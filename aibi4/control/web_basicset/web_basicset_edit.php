<?php include("../includes/head.php"); ?>
<?php
	$web_basicset_id = 1;
	$action = $_POST["action"] ? $_POST["action"] : $_GET["action"];
	
	//編輯
	if ($action=="Edit") {
		//$debug = true;
		$TableName = "web_basicset";
		$AccurateAction = "Get";
		require("../includes/accurate.php");
		
		if ($Init_WebTitle=="") RunAlert("請輸入網站標題");
		//if ($Init_Address=="") RunAlert("請輸入地址或座標");
		//if ($Init_Email=="") RunAlert("請輸入聯絡我們電子信箱");
		//if ($Init_OrderEmail=="") RunAlert("請輸入線上購物電子信箱");
		if ($Init_NewebOther_Duedate<=0) $Init_NewebOther_Duedate = 3;
		
		//$ForbidData = array("Init_Counter");	//不更新的欄位
		$AccurateAction = "Update";
		require("../includes/accurate.php");
	} 

	$sql = "Select * From web_basicset Where web_basicset_id = '".$web_basicset_id."' ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_edit($row[$_key]);
		$Init_Charming = $row["Init_Charming"];	//加入會員的好處
		$Init_Contact = $row["Init_Contact"];	//聯繫資料
		$Init_Down = $row["Init_Down"];	//底部文字
		$Init_Gift = $row["Init_Gift"];	//贈品說明
		$Init_Freight = $row["Init_Freight"];	//運費說明
		$Init_Shopping = $row["Init_Shopping"];	//購物說明
		$Init_Freight2 = $row["Init_Freight2"];	//運費說明
	}
?>
<script language="javascript">
<!--
function chkform() {
	var msg = "";
	if (document.form.Init_WebTitle.value == "") { msg = msg + "網站標題\n"; }
	if (document.form.Init_Address.value == "") { msg = msg + "地址或座標\n"; }
	if (document.form.Init_Email.value == "") { msg = msg + "聯絡我們電子信箱\n"; }
	if (document.form.Init_OrderEmail.value == "") { msg = msg + "線上購物電子信箱\n"; }
	
	if (msg!="") {
		alert("請輸入以下欄位\n\n" + msg);
		return false;
	}
	return true;
}
//-->
</script>
</head>

<body>
<?php 
  //include("../includes/left.php");
  include("../multi-menu/left.php"); 
?>
<h1>基本設定</h1>
<div id="nav">目前位置：<?php echo $nav_title; ?> > 基本設定 > 編輯</div>

<form name="form" method="post" action="web_basicset_edit.php">
  <table class="Edit_form">
<?php
	if(0){
?>	  
    <tr>
      <th>網站標題：<span class="star">*</span></th>
      <td><input type="text" name="Init_WebTitle" value="<?php echo $Init_WebTitle; ?>" class="fill" maxlength="250" /></td>
    </tr>
    <tr>
      <th>地址或座標：<span class="star">*</span></th>
      <td><input type="text" name="Init_Address" value="<?php echo $Init_Address; ?>" class="fill" maxlength="250" /></td>
    </tr>
    <tr>
      <th>聯絡我們電子信箱：<span class="star">*</span></th>
      <td><input type="text" name="Init_Email" value="<?php echo $Init_Email; ?>" class="fill" maxlength="250" />如有多筆信箱請以逗號隔開，如：<span class="font_red">test@yahoo.com,abc@gmail.com</span></td>
    </tr>
    <tr>
      <th>線上購物電子信箱：<span class="star">*</span></th>
      <td><input type="text" name="Init_OrderEmail" value="<?php echo $Init_OrderEmail; ?>" class="fill" maxlength="250" />如有多筆信箱請以逗號隔開，如：<span class="font_red">test@yahoo.com,abc@gmail.com</span></td>
    </tr>
    <tr>
      <td colspan="2" class="Coffee">SMTP</td>
    </tr>
    <tr>
      <th>SMTP Host：</th>
      <td><input type="text" name="Init_SMTP_Host" value="<?php echo $Init_SMTP_Host; ?>" class="fill" maxlength="100" /></td>
    </tr>
    <tr>
      <th>SMTP Port：</th>
      <td><input type="text" name="Init_SMTP_Port" value="<?php echo $Init_SMTP_Port; ?>" size="10" maxlength="3" /></td>
    </tr>
    <tr>
      <th>SMTP 帳號：</th>
      <td><input type="text" name="Init_SMTP_Email" value="<?php echo $Init_SMTP_Email; ?>" class="fill" maxlength="250" /></td>
    </tr>
    <tr>
      <th>SMTP 密碼：</th>
      <td><input type="text" name="Init_SMTP_Pw" value="<?php echo $Init_SMTP_Pw; ?>" class="fill" maxlength="100" /></td>
    </tr>
    <tr>
      <th>SMTP 連線加密方式：</th>
      <td><input type="text" name="Init_SMTP_Secure" value="<?php echo $Init_SMTP_Secure; ?>" size="10" maxlength="10" /></td>
    </tr>
    <tr>
      <td colspan="2" class="Coffee">連結</td>
    </tr>
    <!--
    <tr>
      <th>Google+：</th>
      <td><input type="text" name="Init_Google" value="<?php echo $Init_Google; ?>" class="fill" maxlength="250" /></td>
    </tr>
    <tr>
      <th>Youtube：</th>
      <td><input type="text" name="Init_Youtube" value="<?php echo $Init_Youtube; ?>" class="fill" maxlength="250" /></td>
    </tr>
    -->
    <tr>
      <th>相關連結：</th>
      <td><input type="text" name="Init_Other_Web" value="<?php echo $Init_Other_Web; ?>" class="fill" maxlength="250" /></td>
    </tr>
    <tr>
      <td colspan="2" class="Coffee">SEO</td>
    </tr>
    <tr>
      <th>SEO：</th>
      <td><textarea name="Init_Metadata"><?php echo $Init_Metadata; ?></textarea></td>
    </tr>
    <tr>
      <th>頁面關鍵字：</th>
      <td>
      <input name="Init_Keywords" id="mySingleField" value="<?php echo $Init_Keywords; ?>" type="hidden">
        <ul id="myTags">
        </ul>   
      </td>
    </tr>
    <tr > 
      <th>頁面描述 :</th>
      <td><textarea class="formfield" id="description" name="Init_Description" rows="2" cols="30"><?php echo $Init_Description; ?></textarea></td>
    </tr>
    <tr style="display:none">
      <td colspan="2" class="Coffee">上稿</td>
    </tr>
    <tr style="display:">
      <th>會員條款：</th>
      <td><textarea name="Init_Member"><?php echo $Init_Member; ?></textarea></td>
    </tr>
    <tr style="display:none">
      <th>加入會員的好處：</th>
      <td><textarea name="Init_Charming" id="Init_Charming" class="ckeditor"><?php echo $Init_Charming; ?></textarea></td>
    </tr>
    <tr style="display:">
      <th>聯繫資料：</th>
      <td><textarea name="Init_Contact" id="Init_Contact" class="ckeditor"><?php echo $Init_Contact; ?></textarea></td>
    </tr>
    <tr style="display:">
      <th>底部文字：</th>
      <td><textarea name="Init_Down" id="Init_Down" class="ckeditor"><?php echo $Init_Down; ?></textarea></td>
    </tr>
    <tr style="display:">
      <th>底部文字 - 手機版：</th>
      <td><textarea name="Init_Down_Phone" id="Init_Down_Phone" class="ckeditor"><?php echo $Init_Down_Phone; ?></textarea></td>
    </tr>
    <tr style="display:">
      <th>版權列連結：</th>
      <td><textarea name="Init_Copyright_Link" id="Init_Copyright_Link" class="ckeditor"><?php echo $Init_Copyright_Link; ?></textarea></td>
    </tr>
    <tr>
      <td colspan="2" class="Coffee">購物</td>
    </tr>
    <tr style="display:none">
      <th>贈品說明：</th>
      <td><textarea name="Init_Gift" id="Init_Gift" class="ckeditor"><?php echo $Init_Gift; ?></textarea></td>
    </tr>
    <tr style="display:none">
      <th>運費說明：</th>
      <td><textarea name="Init_Freight" id="Init_Freight" class="ckeditor"><?php echo $Init_Freight; ?></textarea></td>
    </tr>
    <tr style="display:none">
      <th>購物說明：</th>
      <td><textarea name="Init_Shopping" id="Init_Shopping" class="ckeditor"><?php echo $Init_Shopping; ?></textarea></td>
    </tr>
    <tr style="display:">
      <th>到貨通知(回覆)：</th>
      <td><textarea name="Init_productNoticeReplay" id="Init_productNoticeReplay" class="ckeditor"><?php echo $Init_productNoticeReplay; ?></textarea></td>
    </tr>
    <tr>
      <th>購物底下<br>運費說明：</th>
      <td><textarea name="Init_Freight2" id="Init_Freight2" class="ckeditor"><?php echo $Init_Freight2; ?></textarea></td>
    </tr>
    <tr>
      <th>付款方式：</th>
      <td>
        <table class="Border_form">
          <tr>
            <td width="25%">貨到付款：</td>
            <td>
              <input type="radio" name="Init_Payment_CashOnDelivery" value="1" <?php if ($Init_Payment_CashOnDelivery=="1") echo "checked=\"checked\""; ?>/>啟用
              <input type="radio" name="Init_Payment_CashOnDelivery" value="0" <?php if ($Init_Payment_CashOnDelivery=="0") echo "checked=\"checked\""; ?>/>停用
            </td>
          </tr>
          <tr>
            <td width="25%">7-11超商取貨付款：</td>
            <td>
              <input type="radio" name="Init_Payment_CashOnDelivery_711" value="1" <?php if ($Init_Payment_CashOnDelivery_711=="1") echo "checked=\"checked\""; ?>/>啟用
              <input type="radio" name="Init_Payment_CashOnDelivery_711" value="0" <?php if ($Init_Payment_CashOnDelivery_711=="0") echo "checked=\"checked\""; ?>/>停用
            </td>
          </tr>
          <tr>
            <td width="25%">其他超商取貨付款：</td>
            <td>
              <input type="radio" name="Init_Payment_CashOnDelivery_Other" value="1" <?php if ($Init_Payment_CashOnDelivery_Other=="1") echo "checked=\"checked\""; ?>/>啟用
              <input type="radio" name="Init_Payment_CashOnDelivery_Other" value="0" <?php if ($Init_Payment_CashOnDelivery_Other=="0") echo "checked=\"checked\""; ?>/>停用
              <b>總額限制:</b><input type="text" name="Init_Payment_CashOnDelivery_Other_limit" value="<?php echo $Init_Payment_CashOnDelivery_Other_limit; ?>" size="20" maxlength="20" />
            </td>
          </tr>
          <tr>
            <td>線上刷卡：</td>
            <td>
              <input type="radio" name="Init_Payment_Card" value="1" <?php if ($Init_Payment_Card=="1") echo "checked=\"checked\""; ?>/>啟用
              <input type="radio" name="Init_Payment_Card" value="0" <?php if ($Init_Payment_Card=="0") echo "checked=\"checked\""; ?>/>停用
            </td>
          </tr>
          <tr>
            <td>ATM虛擬帳號：</td>
            <td>
              <input type="radio" name="Init_Payment_ATM" value="1" <?php if ($Init_Payment_ATM=="1") echo "checked=\"checked\""; ?>/>啟用
              <input type="radio" name="Init_Payment_ATM" value="0" <?php if ($Init_Payment_ATM=="0") echo "checked=\"checked\""; ?>/>停用
            </td>
          </tr>
          <tr>
            <td>Web ATM：</td>
            <td>
              <input type="radio" name="Init_Payment_WEBATM" value="1" <?php if ($Init_Payment_WEBATM=="1") echo "checked=\"checked\""; ?>/>啟用
              <input type="radio" name="Init_Payment_WEBATM" value="0" <?php if ($Init_Payment_WEBATM=="0") echo "checked=\"checked\""; ?>/>停用
            </td>
          </tr>
          <tr style="display: none">
            <td>超商代收：</td>
            <td>
              <input type="radio" name="Init_Payment_CS" value="1" <?php if ($Init_Payment_CS=="1") echo "checked=\"checked\""; ?>/>啟用
              <input type="radio" name="Init_Payment_CS" value="0" <?php if ($Init_Payment_CS=="0") echo "checked=\"checked\""; ?>/>停用
            </td>
          </tr>
          <tr>
            <td>超商代碼：</td>
            <td>
              <input type="radio" name="Init_Payment_MMK" value="1" <?php if ($Init_Payment_MMK=="1") echo "checked=\"checked\""; ?>/>啟用
              <input type="radio" name="Init_Payment_MMK" value="0" <?php if ($Init_Payment_MMK=="0") echo "checked=\"checked\""; ?>/>停用
            </td>
          </tr>
        </table>
    </td>
    </tr>
    <tr>
      <th>配送方式：</th>
      <td>
        <table class="Border_form">
          <tr>
            <td width="25%">宅配：</td>
            <td>
              <input type="radio" name="Init_Transport_Delivery" value="1" <?php if ($Init_Transport_Delivery=="1") echo "checked=\"checked\""; ?>/>啟用
              <input type="radio" name="Init_Transport_Delivery" value="0" <?php if ($Init_Transport_Delivery=="0") echo "checked=\"checked\""; ?>/>停用
            </td>
          </tr>
          <tr>
            <td>郵局：</td>
            <td>
              <input type="radio" name="Init_Transport_PostOffice" value="1" <?php if ($Init_Transport_PostOffice=="1") echo "checked=\"checked\""; ?>/>啟用
              <input type="radio" name="Init_Transport_PostOffice" value="0" <?php if ($Init_Transport_PostOffice=="0") echo "checked=\"checked\""; ?>/>停用
            </td>
          </tr>
          <tr>
            <td>超商取貨：</td>
            <td>
              <input type="radio" name="Init_Transport_CS" value="1" <?php if ($Init_Transport_CS=="1") echo "checked=\"checked\""; ?>/>啟用
              <input type="radio" name="Init_Transport_CS" value="0" <?php if ($Init_Transport_CS=="0") echo "checked=\"checked\""; ?>/>停用
            </td>
          </tr>
        </table>
	  </td>
    </tr>
    <!--
    <tr>
      <td colspan="2" class="Coffee">藍新信用卡</td>
    </tr>
    <tr>
      <th>傳送網址：</th>
      <td><input type="text" name="Init_NewebCard_Url" value="<?php echo $Init_NewebCard_Url; ?>" class="fill" maxlength="250" /></td>
    </tr>
    <tr>
      <th>商店編號：</th>
      <td><input type="text" name="Init_NewebCard_MerchantNumber" value="<?php echo $Init_NewebCard_MerchantNumber; ?>" size="20" maxlength="20" /></td>
    </tr>
    <tr>
      <th>商店密碼：</th>
      <td><input type="text" name="Init_NewebCard_Code" value="<?php echo $Init_NewebCard_Code; ?>" size="20" maxlength="20" /></td>
    </tr>
    <tr>
      <th>授權指標：</th>
	  <td>
		<input type="radio" name="Init_NewebCard_ApproveFlag" value="1" <?php if ($Init_NewebCard_ApproveFlag=="1") echo "checked=\"checked\""; ?>/>自動授權
		<input type="radio" name="Init_NewebCard_ApproveFlag" value="0" <?php if ($Init_NewebCard_ApproveFlag=="0") echo "checked=\"checked\""; ?>/>手動授權
	  </td>
    </tr>
    <tr>
      <th>請款指標：</th>
	  <td>
		<input type="radio" name="Init_NewebCard_DepositFlag" value="1" <?php if ($Init_NewebCard_DepositFlag=="1") echo "checked=\"checked\""; ?>/>自動請款
		<input type="radio" name="Init_NewebCard_DepositFlag" value="0" <?php if ($Init_NewebCard_DepositFlag=="0") echo "checked=\"checked\""; ?>/>手動請款
	  </td>
    </tr>
    
    <tr>
      <td colspan="2" class="Coffee">藍新非信用卡</td>
    </tr>
    <tr>
      <th>傳送網址：</th>
      <td><input type="text" name="Init_NewebOther_Url" value="<?php echo $Init_NewebOther_Url; ?>" class="fill" maxlength="250" /></td>
    </tr>
    <tr>
      <th>商店編號：</th>
      <td><input type="text" name="Init_NewebOther_MerchantNumber" value="<?php echo $Init_NewebOther_MerchantNumber; ?>" size="20" maxlength="20" /></td>
    </tr>
    <tr>
      <th>商店密碼：</th>
      <td><input type="text" name="Init_NewebOther_Code" value="<?php echo $Init_NewebOther_Code; ?>" size="20" maxlength="20" /></td>
    </tr>
    -->
    <tr>
      <td colspan="2" class="Coffee">紅陽金流</td>
    </tr>
    <tr>
      <th>傳送網址：</th>
      <td><input type="text" name="Init_Esafe_Url" value="<?php echo $Init_Esafe_Url; ?>" class="fill" maxlength="250" /></td>
    </tr>
    <tr>
      <th>線上刷卡：</th>
      <td><input type="text" name="Init_Esafe_BuySafe" value="<?php echo $Init_Esafe_BuySafe; ?>" size="20" maxlength="20" /></td>
    </tr>
    <tr>
      <th>ATM虛擬帳號：</th>
      <td><input type="text" name="Init_Esafe_ATM" value="<?php echo $Init_Esafe_ATM; ?>" size="20" maxlength="20" /></td>
    </tr>
    <tr>
      <th>Web ATM：</th>
      <td><input type="text" name="Init_Esafe_WEBATM" value="<?php echo $Init_Esafe_WEBATM; ?>" size="20" maxlength="20" /></td>
    </tr>
    <tr>
      <th>超商代收：</th>
      <td><input type="text" name="Init_Esafe_24Pay" value="<?php echo $Init_Esafe_24Pay; ?>" size="20" maxlength="20" /></td>
    </tr>
    <tr>
      <th>超商代碼：</th>
      <td><input type="text" name="Init_Esafe_PayCode" value="<?php echo $Init_Esafe_PayCode; ?>" size="20" maxlength="20" /></td>
    </tr>
    <tr>
      <th>7-11付款取貨：</th>
      <td><input type="text" name="Init_Esafe_711" value="<?php echo $Init_Esafe_711; ?>" size="20" maxlength="20" /></td>
    </tr>
    <tr>
      <th>商店密碼：</th>
      <td><input type="text" name="Init_Esafe_Code" value="<?php echo $Init_Esafe_Code; ?>" size="20" maxlength="20" /></td>
    </tr>
    <tr>
      <th>繳費期限：<span class="star">*</span></th>
      <td>訂購者在<input type="text" name="Init_NewebOther_Duedate" value="<?php echo $Init_NewebOther_Duedate; ?>" size="10" maxlength="2" />日內須繳費，請輸入數字</td>
    </tr>
    <!--
    <tr>
      <td colspan="2" class="Coffee">歐付寶物流</td>
    </tr>
    <tr>
      <th>HashKey：</th>
      <td><input type="text" name="Init_HashKey" value="<?php echo $Init_HashKey; ?>" size="20" maxlength="20" /></td>
    </tr>
    <tr>
      <th>HashIV：</th>
      <td><input type="text" name="Init_HashIV" value="<?php echo $Init_HashIV; ?>" size="20" maxlength="20" /></td>
    </tr>
    <tr>
      <th>商店代號：</th>
      <td><input type="text" name="Init_MerchantID" value="<?php echo $Init_MerchantID; ?>" size="20" maxlength="20" /></td>
    </tr>
    -->
    <!--
    <tr>
      <td colspan="2" class="Coffee">金財通</td>
    </tr>
    <tr>
      <th>HOST：</th>
      <td><input type="text" name="Init_EcHost" value="<?php echo $Init_EcHost; ?>" size="20" maxlength="20" /></td>
    </tr>
    <tr>
      <th>帳號：</th>
      <td><input type="text" name="Init_EcUsername" value="<?php echo $Init_EcUsername; ?>" size="20" maxlength="20" /></td>
    </tr>
    <tr>
      <th>密碼：</th>
      <td><input type="text" name="Init_EcPassword" value="<?php echo $Init_EcPassword; ?>" size="20" maxlength="20" /></td>
    </tr>
    <tr>
      <th>發票捐贈單位：</th>
      <td><input type="text" name="Init_InvoiceDonate" value="<?php echo $Init_InvoiceDonate; ?>" size="30" maxlength="20" /></td>
    </tr>
    <tr>
      <th>發票捐贈單位編號：</th>
      <td><input type="text" name="Init_InvoiceDonateNo" value="<?php echo $Init_InvoiceDonateNo; ?>" size="20" maxlength="20" /></td>
    </tr>
    -->
<?php
	}
?>	
    <tr>
      <td colspan="2" class="Coffee">LINE@</td>
    </tr>
<?php
	if(0) {
?>	
    <tr>
      <th>Channel ID：</th>
      <td><input type="text" name="Channel_ID" value="<?php echo $Channel_ID; ?>" size="20" maxlength="20" /></td>
    </tr>
    <tr>
      <th>Channel Secret：</th>
      <td><input type="text" name="Channel_Secret" value="<?php echo $Channel_Secret; ?>" size="100" /></td>
    </tr>
    <tr>
      <th>Channel Access Token：</th>
      <td><textarea name="Channel_Token"><?php echo $Channel_Token; ?></textarea></td>
    </tr>
<?php
	}
?>	
	<tr>
		<th>取消密碼：</th>
		<td>
			<input type="hidden" name="Channel_ID" value="<?php echo $Channel_ID; ?>" size="20" maxlength="20" />
			<input type="hidden" name="Channel_Secret" value="<?php echo $Channel_Secret; ?>" size="100" />
			<input type="hidden" name="Channel_Token" value="<?php echo $Channel_Token; ?>" />
			<input type="text" name="Init_cancelPwd" value="<?php echo $Init_cancelPwd; ?>" size="20" maxlength="20" />
		</td>
    </tr>
	<tr>
      <th>Instagram：</th>
      <td><input type="text" name="Init_Instagram" value="<?php echo $Init_Instagram; ?>" class="fill" maxlength="250" /></td>
    </tr>
	<tr>
      <th>Facebook：</th>
      <td><input type="text" name="Init_Facebook" value="<?php echo $Init_Facebook; ?>" class="fill" maxlength="250" /></td>
    </tr>
	<tr>
      <th>作品牆：</th>
      <td><input type="text" name="Init_Other_Web" value="<?php echo $Init_Other_Web; ?>" class="fill" maxlength="250" /></td>
    </tr>
	<tr>
      <th>推播功能：</th>
      <td>
        <table class="Border_form">
          <tr>
            <td width="10%">預約推播：</td>
            <td>
              <input type="radio" name="Init_Register_Push" value="1" <?php if ($Init_Register_Push=="1") echo "checked=\"checked\""; ?>/>啟用
              <input type="radio" name="Init_Register_Push" value="0" <?php if ($Init_Register_Push=="0") echo "checked=\"checked\""; ?>/>停用
            </td>
          </tr>
          <tr>
            <td>票券推播：</td>
            <td>
              <input type="radio" name="Init_Coupon_Push" value="1" <?php if ($Init_Coupon_Push=="1") echo "checked=\"checked\""; ?>/>啟用
              <input type="radio" name="Init_Coupon_Push" value="0" <?php if ($Init_Coupon_Push=="0") echo "checked=\"checked\""; ?>/>停用
            </td>
          </tr>
          <tr>
            <td>儲值推播：</td>
            <td>
              <input type="radio" name="Init_Stored_Push" value="1" <?php if ($Init_Stored_Push=="1") echo "checked=\"checked\""; ?>/>啟用
              <input type="radio" name="Init_Stored_Push" value="0" <?php if ($Init_Stored_Push=="0") echo "checked=\"checked\""; ?>/>停用
            </td>
          </tr>
		  <tr>
            <td>兌換推播：</td>
            <td>
              <input type="radio" name="Init_Bonus_Push" value="1" <?php if ($Init_Bonus_Push=="1") echo "checked=\"checked\""; ?>/>啟用
              <input type="radio" name="Init_Bonus_Push" value="0" <?php if ($Init_Bonus_Push=="0") echo "checked=\"checked\""; ?>/>停用
            </td>
          </tr>
		  <tr>
            <td>諮詢推播：</td>
            <td>
              <input type="radio" name="Init_Chat_Push" value="1" <?php if ($Init_Chat_Push=="1") echo "checked=\"checked\""; ?>/>啟用
              <input type="radio" name="Init_Chat_Push" value="0" <?php if ($Init_Chat_Push=="0") echo "checked=\"checked\""; ?>/>停用
            </td>
          </tr>
		  
        </table>
	  </td>
    </tr>
	<tr>
	  <th>生日禮金：</th>
	  <td><input type="text" name="Init_Birthday_Money" value="<?php echo $Init_Birthday_Money; ?>" size="5" /> 元(如為0代表不發送生日禮金)</td>
    </tr>
	<tr>
	  <th>全館消費使用折扣：</th>
	  <td><input type="text" name="Init_All_Discount" value="<?php echo $Init_All_Discount; ?>" size="2" maxlength="2" /> %折( 99-0 如為0代表無折扣)</td>
    </tr>
<?php
	if(0) {
?>	
    <tr>
      <td colspan="2" class="Coffee">顯示筆數</td>
    </tr>
    <tr>
      <th>前台列表顯示筆數：<span class="star">*</span></th>
      <td><input type="text" name="Init_Page" value="<?php echo $Init_Page; ?>" size="10" maxlength="5" /> 請輸入數字</td>
    </tr>
    <tr>
      <th>後台顯示筆數：<span class="star">*</span></th>
      <td><input type="text" name="Init_ControlPage" value="<?php echo $Init_ControlPage; ?>" size="10" maxlength="5" /> 請輸入數字</td>
    </tr>
    <tr>
      <th>最大訂購數量：<span class="star">*</span></th>
      <td><input type="text" name="Init_Max" value="<?php echo $Init_Max; ?>" size="10" maxlength="5" /> 請輸入數字</td>
    </tr>
    <tr>
      <th>產品安全庫存量：<span class="star">*</span></th>
      <td><input type="text" name="Init_stockLimit" value="<?php echo $Init_stockLimit; ?>" size="10" maxlength="5" /> 請輸入數字</td>
    </tr>
    <!--<tr>
      <th>瀏覽人次：<span class="star">*</span></th>
      <td><input type="text" name="Init_Counter" value="<?php echo $Init_Counter; ?>" size="10" maxlength="8" /> 請輸入數字</td>
    </tr>-->
<?php
	}
?>	
	<input type="hidden" name="Init_WebTitle" value="<?php echo $Init_WebTitle; ?>" class="fill" maxlength="250" />
	<input type="hidden" name="Init_Page" value="<?php echo $Init_Page; ?>" size="10" maxlength="5" />
	<input type="hidden" name="Init_ControlPage" value="<?php echo $Init_ControlPage; ?>" size="10" maxlength="5" />
	
  </table>
  <div class="btn">
    <input name="submit" type="submit" value="確定送出" onClick="return chkform();" />
    <input type="hidden" name="action" value="Edit" />
    <input type="hidden" name="web_basicset_id" value="<?php echo $web_basicset_id; ?>" />
  </div>
</form>
<?php include("../includes/footer.php"); ?>
<script>
  $(function() {
    $('#myTags').tagit({
      tagLimit: 10,
      singleField: true,
      singleFieldNode: $('#mySingleField')
    });

    $("#description").maxlength({
      maxCharacters: 100,
      slider: true
    });
  })
</script>    
</body>
</html>