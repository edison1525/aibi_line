<?php
	include("../includes/includes.php");
	
	$action = $_POST["action"] ? $_POST["action"] : $_GET["action"];

	//新增編輯
	if ($action=="Edit") {
		//$debug = true;
		$TableName = "web_x_qrcode";
		$AccurateAction = "Get";
		require("../includes/accurate.php");
		
		$web_x_qrcode_id = intval($_POST["web_x_qrcode_id"]);	//分類名稱
		$subject = ($_POST["subject"]);	//分類名稱
		
		if ($subject=="") RunAlert("請輸入標題");
		
		if ($web_x_qrcode_id>0) {
			$AccurateAction = "Update";
			require("../includes/accurate.php");
		} else {
			$AccurateAction = "Insert";
			require("../includes/accurate.php");
		}
	} 
	
	//刪除
	if ($action=="Delete") {
		$DeleteBox = $_POST["DeleteBox"];
		if (!is_array($DeleteBox)) die();
		
		foreach ($DeleteBox as $value) {
			$sql = "Delete From web_x_qrcode Where web_x_qrcode_id = '".$value."' ";
			$rs = ConnectDB($DB, $sql);
		}
		
		$sql = "Select count(*) as counter From web_x_qrcode ";
		$rs = ConnectDB($DB, $sql);
		if (mysql_result($rs, 0, "counter")==0) {
			$sql = "Truncate table web_x_qrcode";
			$rs = ConnectDB($DB, $sql);
		}
	}
	
	RunJs("web_x_qrcode_list.php?page=".intval($_POST["page"])."&field=".$_POST["field"]."&keyword=".urlencode($_POST["keyword"]));
?>