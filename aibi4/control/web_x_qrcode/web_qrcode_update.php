<?php
	include("../includes/includes.php");

	$web_x_qrcode_id = $_POST["web_x_qrcode_id"] ? floatval($_POST["web_x_qrcode_id"]) : floatval($_GET["web_x_qrcode_id"]);
	$sql = "Select subject From web_x_qrcode Where web_x_qrcode_id = '".$web_x_qrcode_id."'";
	$rs = ConnectDB($DB, $sql);
	if (mysql_num_rows($rs)==0) RunJs("web_x_qrcode_list.php");
	
	$action = $_POST["action"] ? $_POST["action"] : $_GET["action"];

	//新增編輯
	if ($action=="Edit") {
		
		
		/********重組陣列小到大*********/
		foreach($_POST[lineInfo] as $key => $postLineInfo) {
			$explodeRow = explode("@#@", $postLineInfo);
			$lineAsoft = ($explodeRow[0]) ? urldecode($explodeRow[0]) : null;
			$lineSubject = ($explodeRow[1]) ? ($explodeRow[1]) : 'tags'.$key;
			$lineLink = ($explodeRow[2]) ? ($explodeRow[2]) : null;
			$lineNote = ($explodeRow[3]) ? ($explodeRow[3]) : null;
			$_POST[lineInfo2][$lineSubject."@#@".$lineLink."@#@".$lineNote] = $lineAsoft;
		}

		asort($_POST[lineInfo2]);
		
		$_POST[lineInfo] = array();
		foreach($_POST[lineInfo2] as $key => $postLineInfo2) {
			$explodeRow = explode("@#@", $key);
			$lineSubject = ($explodeRow[0]) ? urldecode($explodeRow[0]) : 'tags'.$key;
			$lineLink = ($explodeRow[1]) ? ($explodeRow[1]) : null;
			$lineNote = ($explodeRow[2]) ? ($explodeRow[2]) : null;
			$_POST[lineInfo][] = $postLineInfo2."@#@".$lineSubject."@#@".$lineLink."@#@".$lineNote;
		}
		
		/*****************/
		//lineInfo
		$lineInfo = implode('#####', $_POST["lineInfo"]);
		$_POST["lineInfo"] = $lineInfo;
		
		//$debug = true;
		$TableName = "web_qrcode";
		$AccurateAction = "Get";
		require("../includes/accurate.php");
		foreach($_POST as $_key => $_value) $$_key = str_front($_POST[$_key]);
		
		//標籤 
		$multiple_value2 = array_values(array_filter(explode(",", $_POST["multiple_value2"])));
		$_POST['tags'] = implode(",", array_unique($multiple_value2));
		
		$CoverAction = "Update";
		require("../includes/cover.php");
		$FileAction = "Update";
		require("../includes/files.php");
		$_POST["Files"] = $Files;
		

		$web_x_qrcode_id = intval($_POST["new_web_x_qrcode_id"]);	//分類名稱
		$subject = ($_POST["subject"]);	//分類名稱
		$web_qrcode_id = intval($_POST["web_qrcode_id"]);	//分類名稱
		//$cdate = date("Y-m-d");	//發表日期
		//$edate = date("Y-m-d");	//結束時間
		
		$lineInfoArray = explode('#####', $_POST["lineInfo"]);
		
		if ($subject=="") RunAlert("請輸入標題");
		
		if ($_POST['store']=="" && $_POST['tags']=="" && ($_POST["lineInfo"] == '@#@tags0@#@@#@' || $_POST["lineInfo"] == '')) RunAlert("請輸入建立資料");

		if ($web_qrcode_id>0) {
			$ForbidData = array("hits");	//不更新的欄位
			//$debug = true;
			$AccurateAction = "Update";
			require("../includes/accurate.php");
		} else {
			$hits = 0;	//瀏覽人數
			$AccurateAction = "Insert";
			require("../includes/accurate.php");
			$web_qrcode_id = $insterId; 
			
		}
		
		$mergeLink = null;
		$mergeLink .= str_replace('?', '', $content).'?';
		/*
		foreach(explode(',', $tags) as $tagKey => $tagsRow) {
			$tagAry[] = 'tag'.$tagKey.'='.$tagsRow;
		}
		$mergeLink .= implode('&', $tagAry);
		*/
		$mergeLink .= 'tags='.$_POST['tags'];
		if($lineInfoArray) {
			foreach($lineInfoArray as $linInfoKey => $infoRow) {
				$infoRowAry = explode('@#@', $infoRow);
				foreach($infoRowAry as $infoKey => $info) {
					if(!$infoRowAry[1]) {
						continue;
					}
					$infoAry[$linInfoKey] = ($infoRowAry[1]).'='.($infoRowAry[2]);
				}
			}
			
		}
		if($store) {
			$mergeLink .= '&store='.$store;
		}
		if($infoAry) {
			$mergeLink .= '&'.implode('&', $infoAry);
		}	
		$mergeLink .= '&web_qrcode_id='.$web_qrcode_id;
		
		$url = "https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=AIzaSyAoR8p0dcPP5_4krh4-B4n31BKfg1gF7To";
		$data = array(
			"dynamicLinkInfo" => array(
				"domainUriPrefix" => "https://aibi4.page.link",
				"link" => $mergeLink
			),
			"suffix" => array(
				"option"=> "SHORT"
			)
		);
		$headers = array(
			"Content-Type: application/json",
		);
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data, JSON_UNESCAPED_UNICODE ));
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		//如果失敗可以印出 $result 看錯誤訊息
		$result = curl_exec($ch);
		
		$shotLinkAry = json_decode($result, true);
		$_POST['shortLink'] = $shotLinkAry['shortLink'];
		
		$sql = "Update web_qrcode set shortLink = '".$_POST['shortLink']."' Where web_qrcode_id = '".$web_qrcode_id."' ";
		$rs = ConnectDB($DB, $sql);
	} 
	
	//刪除
	if ($action=="Delete") {
		$DeleteBox = $_POST["DeleteBox"];
		if (!is_array($DeleteBox)) die();
		
		foreach ($DeleteBox as $value) {

			//產品代表圖
			$sql = "Select Covers From web_qrcode Where web_qrcode_id = '".$value."' ";
			$rs = ConnectDB($DB, $sql);
			$CoverAction = "Del";
			require("../includes/cover.php");
			
			//內頁圖片
			$sql = "Select Files From web_qrcode Where web_qrcode_id = '".$value."' ";
			$rs = ConnectDB($DB, $sql);
			$FileAction = "Del";
			require("../includes/files.php");
			
			$sql = "Delete From web_qrcode Where web_qrcode_id = '".$value."' ";
			$rs = ConnectDB($DB, $sql);
		}
		
		$sql = "Select count(*) as counter From web_qrcode ";
		$rs = ConnectDB($DB, $sql);
		if (mysql_result($rs, 0, "counter")==0) {
			$sql = "Truncate table web_qrcode";
			$rs = ConnectDB($DB, $sql);
		}
	}
	
	RunJs("web_qrcode_list.php?page=".intval($_POST["page"])."&field=".$_POST["field"]."&keyword=".urlencode($_POST["keyword"])."&web_x_qrcode_id=".$web_x_news_id);
?>