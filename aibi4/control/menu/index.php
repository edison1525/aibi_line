<?php 
include("../includes/head.php"); 
$dayAry = array();
/*
$sql = "
	Select 
		a.*,
		b.subject as classSubjct,
		c.subject as teamSubject,
		c.restDay,
		d.subject as timeSubject
	From 
		web_shift a
	Left Join 
		web_class b ON b.web_class_id = a.web_class_id 		
	Left Join 
		web_team c ON c.web_team_id = a.web_team_id
	Left Join 
		web_time d ON d.web_time_id = a.subject	
	order by 
		d.web_time_id ASC, a.web_shift_id ASC
";
$rs = ConnectDB($DB, $sql);
for ($i=0; $i<mysql_num_rows($rs); $i++) {
	$row = mysql_fetch_assoc($rs);
	$haveDayAry = explode(',', $row['content']);

	foreach($haveDayAry as $key => $haveDay) {
		$dayAry[$haveDay][] = $row;
	}
	
}	
*/
$isSql = "
	SELECT 
		main.*,
		xteam.subject as xteamSubject,
		xteam.time as xteamTime,
		class.subject as classSubject,
		class.lineInfo as classLineInfo
	from 
		web_x_register as main
	left join
		web_x_team xteam
	on
		xteam.web_x_team_id = main.web_x_team_id
	left join
		web_class class
	on
		class.web_class_id = main.web_time_id	
	WHERE 
		main.registerDate >= '".date('Y-m-d', strtotime(date('Y-m-d')." - 1 month"))."'
	AND
		main.registerDate <= '".date('Y-m-d', strtotime(date('Y-m-d')." + 1 month"))."'
	AND
		main.paymentstatus != '取消' 
";
$rs = ConnectDB($DB, $isSql);
for ($i=0; $i<mysql_num_rows($rs); $i++) {
	$row = mysql_fetch_assoc($rs);
	$dayAry[$row['registerDate']][date('N', strtotime($row['registerDate']))][] = $row;
}	

?>
<script>

	document.addEventListener('DOMContentLoaded', function() {
		var initialLocaleCode = 'zh-tw';
		var calendarEl = document.getElementById('calendar');
		var today = moment().format("YYYY-MM-DD");
		var calendar = new FullCalendar.Calendar(calendarEl, {
			plugins: [ 'interaction', 'dayGrid' ],
			header: {
				left: 'prevYear,prev,next,nextYear today',
				center: 'title',
				right: 'dayGridMonth,dayGridWeek,dayGridDay'
			},
			defaultDate: today,
			//defaultView: 'dayGridWeek',
			eventLimit: true, // allow "more" link when too many events
			locale: initialLocaleCode,
			editable: false,
			navLinks: true, // can click day/week names to navigate views
			events: [
			<?php
				
				foreach($dayAry as $key => $dayRow) {	
					$dayName = date('N', strtotime($key));
					$days = $key;
					if(count($dayAry[$days][$dayName])) {
						foreach($dayAry[$days][$dayName] as $key2 => $dayVal) {
							$timeAry = explode('~', substr($dayVal['subject'], 16));
							
							if($dayVal['xteamTime'] > 1) {
								for($i = 2; $i <= $dayVal['xteamTime']; $i++) {
									$timeAry[1] = substr($timeAry[1], 0, 2) + ($i - 1).":".substr($timeAry[1], 3, 2);
								}	
							}
			?>		
						{
							title: '<?php echo $dayVal['classSubject'].$dayVal['xteamSubject']; ?>',
							start: '<?php echo $days; ?> <?php echo $timeAry[0]; ?>',
							end: '<?php echo $days; ?> <?php echo $timeAry[1]; ?>'
						},
			<?php			
						}
					}
				}			
			?>
			]
		});

		calendar.render();
	});

</script>
<style>
	#calendar {
		max-width: 950px;
		margin: 20px auto;
	}
	.fc-content {
		font-size: 11px;
	}
</style>  
</head>
<body>
<?php 
	//include("../includes/left.php");
	include("../multi-menu/left.php"); 
?>
<h1>管理首頁</h1>
<div id="nav">目前位置：管理首頁</div>
<ul><span class="font_orange"><?php echo date('Y-m-d', strtotime(date('Y-m-d')." -1 month"))." ～ ".date('Y-m-d', strtotime(date('Y-m-d')." +1 month"))." 預約表"; ?></span>
  <li>
	<div id='calendar'></div>
  </li>
</ul>
<br class="clear">
<?php include("../includes/footer.php"); ?>
</body>
</html>