<?php
	$list_search[sql_sub] = " Where 1 = 1 ";
	$list_search[link] = "";
	$title_array = array();
	
	//分店
	$search_store = $_POST["search_store"] ? str_filter($_POST["search_store"]) : str_filter($_GET["search_store"]);
	if ($search_store!="") {
		$list_search[sql_sub] .= " and web_x_convert.store_id = '".$search_store."' ";
		$list_search[link] .= "&search_store=".urlencode($search_store);
		$title_array[] = "訂單分店：".$search_store;
	}
	
	//種類
	$search_orderType = $_POST["search_orderType"] ? str_filter($_POST["search_orderType"]) : str_filter($_GET["search_orderType"]);
	if ($search_orderType != '') {
		$search_orderType = ($search_orderType == '-1') ? '0' : $search_orderType;
		$list_search[sql_sub] .= " and web_x_convert.order_type = '".$search_orderType."' ";
		$list_search[link] .= "&search_orderType=".urlencode($search_orderType);
		$title_array[] = "訂單種類：".$search_orderType;
		$search_orderType = ($search_orderType == '0') ? '-1' : $search_orderType;
	}
	
	//訂單狀態
	$search_states = $_POST["search_states"] ? str_filter($_POST["search_states"]) : str_filter($_GET["search_states"]);
	if ($search_states!="") {
		$list_search[sql_sub] .= " and web_x_convert.states like '".$search_states."' ";
		$list_search[link] .= "&search_states=".urlencode($search_states);
		$title_array[] = "訂單狀態：".$search_states;
	}

	//發票狀態
	$search_ecStatus = $_POST["search_ecStatus"] ? str_filter($_POST["search_ecStatus"]) : str_filter($_GET["search_ecStatus"]);
	if ($search_ecStatus!="") {
		$list_search[sql_sub] .= " and web_x_convert.ecStatus like '".$search_ecStatus."' ";
		$list_search[link] .= "&search_ecStatus=".urlencode($search_ecStatus);
		$title_array[] = "發票狀態：".$search_ecStatus;
	}
	
	//付款狀態
	$search_paymentstatus = $_POST["search_paymentstatus"] ? str_filter($_POST["search_paymentstatus"]) : str_filter($_GET["search_paymentstatus"]);
	if ($search_paymentstatus!="") {
		$list_search[sql_sub] .= " and web_x_convert.paymentstatus like '".$search_paymentstatus."' ";
		$list_search[link] .= "&search_paymentstatus=".urlencode($search_paymentstatus);
		$title_array[] = "付款狀態：".$search_paymentstatus;
	}
	
	//付款方式
	$search_payment = $_POST["search_payment"] ? str_filter($_POST["search_payment"]) : str_filter($_GET["search_payment"]);
	if ($search_payment!="") {
		$list_search[sql_sub] .= " and web_x_convert.payment like '".$search_payment."' ";
		$list_search[link] .= "&search_payment=".urlencode($search_payment);
		$title_array[] = "付款方式：".$search_payment;
	}
	
	//配送方式
	$search_transport = $_POST["search_transport"] ? str_filter($_POST["search_transport"]) : str_filter($_GET["search_transport"]);
	if ($search_transport!="") {
		$list_search[sql_sub] .= " and web_x_convert.transport like '".$search_transport."' ";
		$list_search[link] .= "&search_transport=".urlencode($search_transport);
		$title_array[] = "配送方式：".$search_transport;
	}
	
	//超商門市
	/*
	$search_store = $_POST["search_store"] ? str_filter($_POST["search_store"]) : str_filter($_GET["search_store"]);
	if ($search_store!="") {
		$list_search[sql_sub] .= " and web_x_order.store like '".$search_store."' ";
		$list_search[link] .= "&search_store=".urlencode($search_store);
		$title_array[] = "超商門市：".$search_store;
	}
	*/
	//關鍵字
	$keyword = $_POST["keyword"] ? str_filter($_POST["keyword"]) : str_filter($_GET["keyword"]);
	if ($keyword!="") {
		$keywords = explode(" ", $keyword);
		for ($i=0; $i<sizeof($keywords); $i++) {
			if ($keywords[$i]!="") $list_search[sql_sub] .= " and (ordernum like '%".$keywords[$i]."%' or accept_time like '%".$keywords[$i]."%' or order_name like '%".$keywords[$i]."%' or order_tel like '%".$keywords[$i]."%' or order_mobile like '%".$keywords[$i]."%' or accept_name like '%".$keywords[$i]."%' or accept_tel like '%".$keywords[$i]."%' or accept_mobile like '%".$keywords[$i]."%' or accept_city like '%".$keywords[$i]."%' or accept_area like '%".$keywords[$i]."%' or accept_zip like '%".$keywords[$i]."%' or accept_address like '%".$keywords[$i]."%' or remark like '%".$keywords[$i]."%' or IP like '%".$keywords[$i]."%' or adminremark like '%".$keywords[$i]."%' or ordernum IN (select web_x_order_ordernum from web_convert where subject like '%".$keywords[$i]."%')) ";
		}
		$list_search[link] .= "&keyword=".urlencode($keyword);
		$title_array[] = "關鍵字：".$keyword;
	}
	
	//訂購時間-開始日期
	$search_sdate = $_POST["search_sdate"] ? str_filter($_POST["search_sdate"]) : str_filter($_GET["search_sdate"]);
	if ($search_sdate!="") {
		$list_search[sql_sub] .= " and web_x_convert.cdate >= '".$search_sdate." 00:00:00' ";
		$list_search[link] .= "&search_sdate=".urlencode($search_sdate);
		$title_array[] = "自".$search_sdate;
	}
	
	//訂購時間-結束日期
	$search_edate = $_POST["search_edate"] ? str_filter($_POST["search_edate"]) : str_filter($_GET["search_edate"]);
	if ($search_edate!="") {
		$list_search[sql_sub] .= " and web_x_convert.cdate <= '".$search_edate." 23:59:59' ";
		$list_search[link] .= "&search_edate=".urlencode($search_edate);
		$title_array[] = "至".$search_edate;
	}
	if(in_array($search_paymentstatus, array('付款成功', '付款失敗', '付款金額錯誤'))) {
		//付款時間-開始日期
		$search_paySdate = $_POST["search_paySdate"] ? str_filter($_POST["search_paySdate"]) : str_filter($_GET["search_paySdate"]);
		if ($search_paySdate!="") {
			$list_search[sql_sub] .= " and web_x_convert.successPayDate >= '".$search_paySdate." 00:00:00' ";
			$list_search[link] .= "&search_paySdate=".urlencode($search_paySdate);
			$title_array[] = "自".$search_paySdate;
		}
		
		//付款時間-結束日期
		$search_payEdate = $_POST["search_payEdate"] ? str_filter($_POST["search_payEdate"]) : str_filter($_GET["search_payEdate"]);
		if ($search_payEdate!="") {
			$list_search[sql_sub] .= " and web_x_convert.successPayDate <= '".$search_payEdate." 23:59:59' ";
			$list_search[link] .= "&search_payEdate=".urlencode($search_payEdate);
			$title_array[] = "至".$search_payEdate;
		}
	}
	//訂單金額-開始金額
	$search_samount = $_POST["search_samount"] ? str_filter($_POST["search_samount"]) : str_filter($_GET["search_samount"]);
	if ($search_samount!="") {
		$list_search[sql_sub] .= " and web_x_convert.total >= '".$search_samount."' ";
		$list_search[link] .= "&search_samount=".urlencode($search_samount);
		$title_array[] = "自$".number_format($search_samount);
	}
	
	//訂單金額-結束金額
	$search_eamount = $_POST["search_eamount"] ? str_filter($_POST["search_eamount"]) : str_filter($_GET["search_eamount"]);
	if ($search_eamount!="") {
		$list_search[sql_sub] .= " and web_x_convert.total <= '".$search_eamount."' ";
		$list_search[link] .= "&search_eamount=".urlencode($search_eamount);
		$title_array[] = "至$".number_format($search_eamount);
	}

	$search_title = implode("　", $title_array);
?>
