<?php
	$list_search[sql_sub] = " Where 1 = 1 ";
	$list_search[link] = "";
	$title_array = array();
	
	//分店
	$search_store = $_POST["search_store"] ? str_filter($_POST["search_store"]) : str_filter($_GET["search_store"]);
	if ($search_store!="") {
		$list_search[sql_sub] .= " and e.web_x_class_id = '".$search_store."' ";
		$list_search[link] .= "&search_store=".urlencode($search_store);
		$title_array[] = "預約分店：".$search_store;
	}
	
	//預約狀態
	$search_states = $_POST["search_states"] ? str_filter($_POST["search_states"]) : str_filter($_GET["search_states"]);
	if ($search_states!="") {
		$list_search[sql_sub] .= " and a.paymentstatus like '".$search_states."' ";
		$list_search[link] .= "&search_states=".urlencode($search_states);
		$title_array[] = "預約狀態：".$search_states;
	}
	
	//設計師
	$search_payment = $_POST["search_payment"] ? str_filter($_POST["search_payment"]) : str_filter($_GET["search_payment"]);
	if ($search_payment!="") {
		$list_search[sql_sub] .= " and c.subject like '".$search_payment."' ";
		$list_search[link] .= "&search_payment=".urlencode($search_payment);
		$title_array[] = "設計師：".$search_payment;
	}
	
	//服務項目
	$search_transport = $_POST["search_transport"] ? str_filter($_POST["search_transport"]) : str_filter($_GET["search_transport"]);
	if ($search_transport!="") {
		$list_search[sql_sub] .= " and d.subject like '".$search_transport."' ";
		$list_search[link] .= "&search_transport=".urlencode($search_transport);
		$title_array[] = "服務項目：".$search_transport;
	}
	
	//關鍵字
	$keyword = $_POST["keyword"] ? str_filter($_POST["keyword"]) : str_filter($_GET["keyword"]);
	if ($keyword!="") {
		$keywords = explode(" ", $keyword);
		for ($i=0; $i<sizeof($keywords); $i++) {
			if ($keywords[$i]!="") $list_search[sql_sub] .= " and (ordernum like '%".$keywords[$i]."%' or order_name like '%".$keywords[$i]."%' or order_tel like '%".$keywords[$i]."%' or order_mobile like '%".$keywords[$i]."%' or accept_name like '%".$keywords[$i]."%' or accept_tel like '%".$keywords[$i]."%' or accept_mobile like '%".$keywords[$i]."%' or accept_city like '%".$keywords[$i]."%' or accept_area like '%".$keywords[$i]."%' or accept_zip like '%".$keywords[$i]."%' or accept_address like '%".$keywords[$i]."%' or remark like '%".$keywords[$i]."%' or IP like '%".$keywords[$i]."%' or adminremark like '%".$keywords[$i]."%') ";
		}
		$list_search[link] .= "&keyword=".urlencode($keyword);
		$title_array[] = "關鍵字：".$keyword;
	}
	
	//預約時間-開始日期
	$search_sdate = $_POST["search_sdate"] ? str_filter($_POST["search_sdate"]) : str_filter($_GET["search_sdate"]);
	if ($search_sdate!="") {
		$list_search[sql_sub] .= " and a.registerDate >= '".$search_sdate." 00:00:00' ";
		$list_search[link] .= "&search_sdate=".urlencode($search_sdate);
		$title_array[] = "自".$search_sdate;
	}
	
	//預約時間-結束日期
	$search_edate = $_POST["search_edate"] ? str_filter($_POST["search_edate"]) : str_filter($_GET["search_edate"]);
	if ($search_edate!="") {
		$list_search[sql_sub] .= " and a.registerDate <= '".$search_edate." 23:59:59' ";
		$list_search[link] .= "&search_edate=".urlencode($search_edate);
		$title_array[] = "至".$search_edate;
	}
	
	$search_title = implode("　", $title_array);
?>
