<?php include("../includes/head.php"); ?>
<?php
  include("web_x_register_serach.php");
?>
</head>

<body>
<?php 
  //include("../includes/left.php");
  include("../multi-menu/left.php"); 
?>
<h1>預約</h1>
<div id="nav">目前位置：<?php echo $nav_title; ?> > <a href="web_x_register_list.php">預約</a> > 列表</div>

<!--搜尋開始-->
<form name="formSearch" method="post" action="web_x_register_list.php" id="searchForm">
  <div id="search">
	分店：<select name="search_store">
    <option value="">請選擇</option>
    <?php
            $store_list = "";
			if($loginUserLevelInfo['store_id'] != '-1') {
				$storeSql = "SELECT subject, web_x_class_id FROM `web_x_class` WHERE ifShow = '1' AND web_x_class_id = '".$loginUserLevelInfo['store_id']."' ORDER BY asort ASC";
			} else {
				$storeSql = "SELECT subject, web_x_class_id FROM `web_x_class` WHERE ifShow = '1' ORDER BY asort ASC";
			}
			$storeRs = ConnectDB($DB, $storeSql); 
			for ($i=0; $i<mysql_num_rows($storeRs); $i++) {
				$stroeRow = mysql_fetch_assoc($storeRs);
                $store_list .= "<option value=\"".$stroeRow['web_x_class_id']."\"";
                if ($search_store==$stroeRow['web_x_class_id']) $store_list .= " selected=\"selected\"";
                $store_list .= ">".$stroeRow['subject']."</option>";
            }
            echo $store_list;
        ?>
    </select>
    預約狀態：<select name="search_states">
    <option value="">請選擇</option>
    <?php
		$states_list = "";
		foreach ($states_array as $value) {
			$states_list .= "<option value=\"".$value."\"";
			if ($search_states==$value) $states_list .= " selected=\"selected\"";
			$states_list .= ">".$value."</option>";
		}
		echo $states_list;
	?>
    </select>
    服務項目：<select name="search_transport" style="margin-bottom: 3px">
    <option value="">請選擇</option>
	<?php
		$sql2 = "Select * From web_x_team Where ifShow = '1' order by asort ASC";
		$rs2 = ConnectDB($DB, $sql2);
		for ($i=0; $i<mysql_num_rows($rs2); $i++) {
			$row2 = mysql_fetch_assoc($rs2);		
	?>
		<option value="<?php echo $row2['subject']; ?>"<?php if ($search_payment==$row2['subject']) echo " selected=\"selected\""; ?>><?php echo $row2['subject']; ?></option>
	<?php
		}	
	?>	
    </select>
	設計師：<select name="search_payment">
    <option value="">請選擇</option>
	<?php
		$sql2 = "Select * From web_class Where ifShow = '1' order by asort ASC";
		$rs2 = ConnectDB($DB, $sql2);
		for ($i=0; $i<mysql_num_rows($rs2); $i++) {
			$row2 = mysql_fetch_assoc($rs2);		
	?>
		<option value="<?php echo $row2['subject']; ?>"<?php if ($search_payment==$row2['subject']) echo " selected=\"selected\""; ?>><?php echo $row2['subject']; ?></option>
	<?php
		}	
	?>
    </select>
    關鍵字：<input name="keyword" type="text" value="<?php echo $keyword; ?>" maxlength="100" placeholder="請輸入關鍵字" style="width: 230px;" /></br>
    <span id="orderDate">
   預約時間：<input name="search_sdate" id="search_sdate" type="text" value="<?php echo $search_sdate; ?>" maxlength="10" placeholder="開始日期" style="width: 100px" />~<input name="search_edate" id="search_edate" type="text" value="<?php echo $search_edate; ?>" maxlength="10" placeholder="結束日期" style="width: 100px" />
    </span>
    <input id="searchBtn" name="submit" type="submit" value="搜尋" />
    <div id="new">
<?php
	if($loginUserLevelInfo['export']) {
?>	
      <a href="web_x_register_erp_list.php?page=1<?php echo $list_search[link]; ?>" title="<?php echo $search_title; ?>">匯出<?php echo ($list_search[link]=="") ? "全部" : "搜尋"; ?>
      <!--ERP--></a>
<?php
	}
?>	
      <!--<a href="web_x_order_output_list.php?page=1<?php echo $list_search[link]; ?>" title="<?php echo $search_title; ?>">匯出訂單</a>-->
    </div>
  </div>
  <br class="clear">
</form>
<!--搜尋結束-->

<form name="form" method="post" action="web_x_register_update.php">
  <table class="List_form">
    <tr>
      <th width="5%"><input type="checkbox" name="all" onClick="CheckAll(this, 'DeleteBox[]')" /></th>
      <th width="5%">編號</th>
      <th width="12%" style="display:none;">預約編號</th>
	  <th width="12%" style="display:none;">報到時間</br>【預估】</th>
	  <th width="12%">分店</th>
	  <th width="12%">服務項目</th>
      <th width="12%">服務狀態</th>
      <th width="10%">預約人姓名</th>
      <th width="10%">消費人姓名</th>
      <th width="15%">消費時間</th>
      <th width="15%">預約時間</th>
      <!--<th width="5%"><a class="edit">改</a></th>-->
    </tr>
  <?php
	$sql = "
		Select 
			SQL_CALC_FOUND_ROWS 
			a.*,
			c.subject as classSubject,
			d.subject as teamSubject,
			d.time as teamTime,
			(select subject from web_x_class where web_x_class.web_x_class_id = c.web_x_class_id) as xClassSubject
		From 
			web_x_register a
		Left Join 
			web_class c ON c.web_class_id = a.web_time_id 
		Left Join 
			web_x_class e ON e.web_x_class_id = c.web_x_class_id 	
		Left Join 
			web_x_team d ON d.web_x_team_id = a.web_x_team_id
		".$list_search[sql_sub]."";
	if($loginUserLevelInfo['store_id'] != '-1') {
		$web_x_class_id = $loginUserLevelInfo['store_id'];
		$sql .= " AND e.web_x_class_id = '".$web_x_class_id."'";
	}	
	$sql .= "	
		order by 
			a.registerDate DESC, a.ordernum ASC
	";
	
    //$sql = "Select SQL_CALC_FOUND_ROWS * From web_x_register ".$list_search[sql_sub]." order by cdate desc, web_x_register_id desc ";
	$list_paging = list_paging_sql($page, $Init_ControlPage, $sql);
    $sql .= " limit ".($page-1) * $Init_ControlPage.", ".$Init_ControlPage;
	//echo $sql;
    $rs = ConnectDB($DB, $sql);
    //$list_paging = list_paging($page, $Init_ControlPage);
	
    for ($i=0; $i<mysql_num_rows($rs); $i++) {
      $row = mysql_fetch_assoc($rs);
      foreach($row as $_key=>$_value) $$_key = $row[$_key];
      
      if ($web_member_id>0) {
        if($order_name) {
			$link = "<a href=\"../web_member/web_member_edit.php?web_member_id=".$web_member_id."\" target=\"_blank\">".$order_name."</a>";
		} else {
			$link = "系統代預約";
		}
      } else {
        $link = $order_name;
      }
	  		

    ?>
    <tr style="<?php echo $style; ?>">
      <td align="center"><input type="checkbox" name="DeleteBox[]" value="<?php echo $web_x_register_id; ?>" /></td>
      <td align="center"><?php echo $i+1; ?></td>
      <td align="center" style="display:none;"><a target="_blank"><span class="font_grayred"><?php echo $ordernum; ?></span></a></td>
	  <td align="center" style="display:none;"><a target="_blank"><span class="font_grayred"><?php if($person) { echo date('H:i', $estimate); } ?></span></a></td>
	  <td align="center"><?php echo $xClassSubject; ?></td>
	  <td align="center"><?php echo $teamSubject."-".$teamTime."HR"; ?></br>【<?php echo $classSubject; ?>】</td>
      <td align="center">
      <?php echo $paymentstatus; ?>
      </td>
      <td><?php echo $link; ?></td>
      <td><?php echo $accept_name; ?></td>
      <td align="center">
		<?php 
			if($teamTime > 1) {
				echo substr($subject,0 ,15)."</br>".substr($subject,16, 6).ceil(substr($subject,22, 2)+($teamTime-1)).substr($subject,24); 
			} else {
				echo substr($subject,0 ,15)."</br>".substr($subject,16); 
			}	
		?>
      </td>
      <td align="center"><?php echo str_replace(" ", "<br />", $cdate); ?></td>
      <!--<td align="center"><a href="web_x_register_edit.php?web_x_register_id=<?php echo $web_x_register_id."&page=".$page.$list_search[link]; ?>" class="edit">改</a></td>-->
    </tr>
  <?php } ?>
  </table>
  <div id="delete">
    <select name="action" id="action">
      <option value="">請選擇</option>
	  <option value="未報到">未報到</option>
	  <option value="取消">取消</option>
      <option value="已報到">已報到</option>
	  <option value="已服務">已服務</option>
      <option value="完成">完成</option>
    </select> 
    <input type="submit" name="submit" value="執行" onClick="return _CheckDel();" />
  </div>
  <?php list_page("web_x_register_list.php", $page, $list_paging[pages], $list_paging[records], $list_search[hidden], $list_search[link]); ?>
  <!--<input type="hidden" name="action" value="Delete" />-->
</form>
<?php include("../includes/footer.php"); ?>
<script type="text/javascript">
<!--
var comma_separator_number_step = $.animateNumber.numberStepFactories.separator(',')
$('#totalsales').animateNumber({
    number: <?php echo $totalsales = ($totalsales) ? $totalsales : 0; ?>,
    numberStep: comma_separator_number_step
});
$(function() {
  //console.log($("select[name='search_paymentstatus']").val());

  $('#orderAmount').on('keydown', '#search_samount, #search_eamount', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});

  $("#searchBtn").on('click', function(e) {
    var samount = parseInt($('#search_samount').val());
    var eamount = parseInt($('#search_eamount').val());
    //e.preventDefault();
    console.log(samount,eamount);
    if(samount || eamount) {
        //console.log(Math.samonut,Math.eamonut);
        if((samount > eamount) || !samount || !eamount) {
          alert('輸入正確金額');
          e.preventDefault();
        }
    } else {
      $("#searchForm").submit();
    }

  }); 

 

})
//-->
</script>
</body>
</html>