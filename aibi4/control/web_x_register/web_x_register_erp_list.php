<?php 
  include("../includes/head.php"); 
  include("web_x_register_serach.php");
  if ($search_title!="") $search_title = "（".$search_title."）";
?>
<body>
<?php 
  //include("../includes/left.php");
  include("../multi-menu/left.php"); 
?>
<h1>匯出列表</h1>
<div id="nav">目前位置：<?php echo $nav_title; ?> > <a href="web_x_register_list.php">預約</a> > 列表</div>
  <table class="List_form">
    <tr>
  	  <th width="5%">匯出檔案</th> 	  
    </tr>
	<?php
    $FieldArray = array(
		"a.web_x_register_id" => "編號",
		"(select subject from web_x_class where web_x_class.web_x_class_id = c.web_x_class_id) as xClassSubject" => "分店",
		"d.subject as web_x_team_subject" => "服務項目",
		"d.time" => "服務項目時間",
		"c.subject as web_class_subject" => "設計師",
		"a.paymentstatus" => "服務狀態",
		"CONCAT(a.order_name,'',a.order_sex) as order_name" => "預約人姓名",
		"a.order_mobile" => "訂購者電話",
		"CONCAT(a.accept_name,'',a.accept_sex) as accept_name" => "消費人姓名",
		"a.subject as web_x_register_subject" => "消費時間",
		"a.cdate" => "預約時間",
    );
  
    $i = 0;
    $sql_field = "";
    foreach ($FieldArray as $_key=>$_value) {
      //echo $_key."：".$_value."<br />";    //欄位英文名：中文名
      $sql_field .= $_key.", ";
      $i++;
    }
    $sql_field = trim($sql_field, ", ");
    
    $j = 2;
    $sql = "
		Select 
			".$sql_field." 
		From 
			web_x_register a
		Left Join 
			web_class c ON c.web_class_id = a.web_time_id 
		Left Join 
			web_x_class e ON e.web_x_class_id = c.web_x_class_id 	
		Left Join 
			web_x_team d ON d.web_x_team_id = a.web_x_team_id 
			".$list_search[sql_sub]." 
		order by 
			a.cdate desc, a.web_x_register_id desc 
	";
    //echo $sql;
    $rs = ConnectDB($DB, $sql);
    $allCount = mysql_num_rows($rs);
    
     
    echo "共: ".$allCount." 筆資料";
    $evnRow = 1500;
    $loopRows = ($allCount > $evnRow) ? ceil($allCount / $evnRow) : 1;
    if($allCount) {
    for($i=1; $i<=($loopRows); $i++) {
  ?>
    <tr>
      <td align="center"><a href="web_x_register_erp.php?page=<?php echo ($i); ?><?php echo $list_search[link]; ?>" title="<?php echo $search_title; ?>"><?php echo $Init_WebTitle."_預約資料".$search_title."_".date("YmdHis")."(".($i).").xls"; ?></a></td>
    </tr>  
	<?php } } ?>
  </table>
<?php include("../includes/footer.php"); ?>
</body>
</html>