<?php
	ignore_user_abort(true);
	//for ERP
	include("../includes/includes.php");
	
	require "../class/excel/Classes/PHPExcel.php";
	require "../class/excel/Classes/PHPExcel/IOFactory.php";

	include("web_x_register_serach.php");
	if ($search_title!="") $search_title = "（".$search_title."）";

	$objPHPExcel = new PHPExcel();
	
	$objPHPExcel->getProperties()
				->setCreator("Orders")
				->setLastModifiedBy("Orders")
				->setTitle("Orders")
				->setSubject("Orders")
				->setDescription("Orders")
				->setKeywords("Orders")
				->setCategory("Orders");
	
	$CellArray = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ");

	$FieldArray = array(
		"a.web_x_register_id" => "編號",
		"(select subject from web_x_class where web_x_class.web_x_class_id = c.web_x_class_id) as xClassSubject" => "分店",
		"d.subject as web_x_team_subject" => "服務項目",
		"d.time" => "服務項目時間",
		"c.subject as web_class_subject" => "設計師",
		"a.paymentstatus" => "服務狀態",
		"CONCAT(a.order_name,'',a.order_sex) as order_name" => "預約人姓名",
		"a.order_mobile" => "訂購者電話",
		"CONCAT(a.accept_name,'',a.accept_sex) as accept_name" => "消費人姓名",
		"a.subject as web_x_register_subject" => "消費時間",
		"a.cdate" => "預約時間",
    );
	
	$i = 0;
	$sql_field = "";
	foreach ($FieldArray as $_key=>$_value) {
		//echo $_key."：".$_value."<br />";		//欄位英文名：中文名
		if($_key != 'discountTotal' && $_key != 'CVSstatus') 
			$sql_field .= $_key.", ";
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($CellArray[$i]."1", $_value);
		$i++;
	}
	$sql_field = trim($sql_field, ", ");
	
    /*
    echo "<pre>";
    print_r($web_x_gift_content_count);
    echo "</pre>";
    exit;
	*/
	$j = 2;
	$evnRow = 1500;
	$sql = "
		Select 
			".$sql_field." 
		From 
			web_x_register a
		Left Join 
			web_class c ON c.web_class_id = a.web_time_id 
		Left Join 
			web_x_class e ON e.web_x_class_id = c.web_x_class_id 	
		Left Join 
			web_x_team d ON d.web_x_team_id = a.web_x_team_id 
			".$list_search[sql_sub]." 
		order by 
			a.cdate desc, a.web_x_register_id desc 
	";
	//$sql = "Select ".$sql_field." from web_x_register JOIN (web_order) ON (web_x_register.ordernum=web_order.web_x_register_ordernum) ".$list_search[sql_sub]." order by web_x_register.web_x_register_id desc";
	if($page)
		$sql .= " limit ".($page-1) * ($evnRow - $giftRow).", ".($evnRow - $giftRow);	
	//echo $sql;
	$rs = ConnectDB($DB, $sql);
	
	if(1) {
		//for ($i=($x*$evnRow); $i<mysql_num_rows($rs); $i++) {
		for ($i=0; $i<mysql_num_rows($rs); $i++) {	
			$row = mysql_fetch_assoc($rs);
			$k = 0;
			foreach($row as $_key=>$_value) {
				$$_key = $row[$_key];
				
				switch ($_key) {
					case "order_name":
						$$_key = ($$_key) ? ($$_key) : '系統代預約';
						break;		
					
					case "time":
						$$_key = $$_key." HR";
						break;
						
					default:
				}
				
				//輸出
				switch ($_key) {
					case "cdate":
						$objPHPExcel->getActiveSheet()->setCellValue($CellArray[$k].$j, $$_key);
						$objPHPExcel->getActiveSheet()->getStyle($CellArray[$k].$j)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);
						break;
					default:
						$objPHPExcel->getActiveSheet()->getCell($CellArray[$k].$j)->setValueExplicit($$_key, PHPExcel_Cell_DataType::TYPE_STRING);
				}
				
				$k++;
			}	
			$j++;
			
		}
		
			
	}
	//exit;
	$objPHPExcel->getActiveSheet()->setTitle("預約資料");
	//$objPHPExcel->getActiveSheet()->setAutoFilter($objPHPExcel->getActiveSheet()->calculateWorksheetDimension());
	$objPHPExcel->setActiveSheetIndex(0);
	
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="'.iconv("UTF-8", "Big5", $Init_WebTitle."_預約資料".$search_title)."_".date("YmdHis").'('.$page.').xls"');
	header('Cache-Control: max-age=0');
	
	//}
	try {
		//for($x=1; $i<=2; $i++) {
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		//echo PHPExcel_Shared_File::sys_get_temp_dir(); 
		//echo $objWriter->getTempDir();
		//$objWriter->save('test.xls'); 
		$objWriter->save('php://output');
		//}
	} catch(Exception $e) {
		echo $e->getMessage();
	}
	
	
	//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	//$objWriter->save('php://output'); 
	//$objWriter->save('test.xls');
	//echo '../../uploadfiles/'.iconv("UTF-8", "Big5", $Init_WebTitle."_訂單資料".$search_title)."_".date("YmdHis").'.xls';
	//phpinfo();

	exit;	
?>