<?php include("../includes/head.php"); ?>
<?php
	$web_epaper_id = intval($_GET["web_epaper_id"]);
	$sql = "Select * From web_epaper Where web_epaper_id = '".$web_epaper_id."' ";
	$rs = ConnectDB($DB, $sql);
	if (mysql_num_rows($rs)==0) {
		$action = "Add";
		$ifShow = 1;	//是否顯示
		$cdate = date("Y-m-d");	//發表日期
		$hits = 0;	//瀏覽人數
	} else {
		$action = "Edit";
		for ($i=0; $i<mysql_num_rows($rs); $i++) {
			$row = mysql_fetch_assoc($rs);
			foreach($row as $_key=>$_value) $$_key = str_edit($row[$_key]);
			$content = $row["content"];	//內文
		}
	}
?>
<script language="javascript">
<!--
function chkform() {
	var msg = "";
	if (document.form.subject.value == "") { msg = msg + "標題\n"; }
	
	if (msg!="") {
		alert("請輸入以下欄位\n\n" + msg);
		return false;
	}
	return true;
}
//-->
</script>
</head>

<body>
<?php 
	//include("../includes/left.php");
	include("../multi-menu/left.php"); 
?>
<h1>電子報</h1>
<div id="nav">目前位置：<?php echo $nav_title; ?> > <a href="web_epaper_list.php">電子報</a> > <?php echo ($action=="Add") ? "新增" : "編輯"; ?></div>

<form name="form" method="post" action="web_epaper_update.php" enctype="multipart/form-data">
  <table class="Edit_form">
	<tr>
	  <th>是否顯示：<span class="star">*</span></th>
	  <td>
		<input type="radio" name="ifShow" value="1" <?php if ($ifShow=="1") echo "checked=\"checked\""; ?>/>是
		<input type="radio" name="ifShow" value="0" <?php if ($ifShow=="0") echo "checked=\"checked\""; ?>/>否
	  </td>
	</tr>
	<tr>
	  <th>期數：</th>
	  <td>第<input type="text" name="period" value="<?php echo $period; ?>" size="10" maxlength="10" />期</td>
	</tr>
	<tr>
	  <th>標題：<span class="star">*</span></th>
	  <td><input type="text" name="subject" value="<?php echo $subject; ?>" class="fill" maxlength="100" /></td>
	</tr>
	<tr>
	  <th>發表日期：<span class="star">*</span></th>
	  <td><input type="text" name="cdate" id="cdate" value="<?php echo $cdate; ?>" size="10" maxlength="10" readonly /></td>
	</tr>
	<tr>
	  <th>內文：</th>
	  <td>圖片請使用絕對路徑，以免訂戶無法瀏覽<br /><textarea name="content" id="content" class="ckeditor"><?php echo $content; ?></textarea></td>
	</tr>
    <?php if ($action=="Edit" && 1==2) { ?>
	<tr>
	  <th>瀏覽人數：</th>
	  <td><?php echo $hits; ?></td>
	</tr>
    <?php } ?>
  </table>
  <div class="btn">
    <input name="submit" type="submit" value="確定送出" onClick="return chkform();" />
    <input name="button" type="button" value="回上一頁" onClick="history.go(-1);" />
    <input type="hidden" name="action" value="Edit" />
    <input type="hidden" name="web_epaper_id" value="<?php echo $web_epaper_id; ?>" />
    <input type="hidden" name="page" value="<?php echo (intval($_GET["page"])==0) ? 1 : intval($_GET["page"]); ?>" />
    <input type="hidden" name="field" value="<?php echo str_filter($_GET["field"]); ?>" />
    <input type="hidden" name="keyword" value="<?php echo str_filter($_GET["keyword"]); ?>" />
  </div>
</form>
<?php include("../includes/footer.php"); ?>
</body>
</html>