<?php 
  include("../includes/head.php"); 
?>
</head>

<body>
<?php 
	//include("../includes/left.php");
	include("../multi-menu/left.php"); 
?>
<h1>會員專區</h1>
<div id="nav">目前位置：<?php echo $nav_title; ?> > <a href="web_member_list.php">會員專區</a> > 列表</div>

<!--搜尋開始-->
<form name="formSearch" method="post" action="web_member_list.php">	
  <div id="search">
  <?php 
    $list_search = list_search_by_time(array("uname" => "姓名", "mobile" => "手機號碼", "tags" => "標籤")); 
  ?>
    <div id="new">
      <?php 
		if($loginUserLevelInfo['export']) {
        if(isset($list_search[link])) {
      ?>
      <a href="web_member_output_list.php?web_member_id=<?php echo $web_member_id.$list_search[link]; ?>">匯出</a>
      <?php } else { ?>
      <a href="web_member_output_list.php">匯出</a>
		<?php } } ?>
      <!--<a href="web_member_edit.php">新增資料</a>-->
    </div>	
  </div>
  <a class="tagEdit" href="#tagSelect"></a>
  <div style="display: none;">
		<div id="tagSelect">
			<select name="bestsellers3[]" id="bestsellers3" multiple="multiple" style="width: 45%">
			<?php
				$web_tag_list3 = "";
				$sql_sub3 = "";
				
				//已被選取的產品	
				foreach ($_POST['bestsellers3'] as $value) {
					$sql = "Select web_tag.web_tag_id, web_tag.web_x_tag_id, web_tag.subject as web_tag, web_x_tag.subject as web_x_tag From web_tag JOIN (web_x_tag) ON (web_x_tag.web_x_tag_id = web_tag.web_x_tag_id) Where web_tag.web_tag_id = '".$value."' ";
					$rs = ConnectDB($DB, $sql);
					for ($i=0; $i<mysql_num_rows($rs); $i++) {
						$row = mysql_fetch_assoc($rs);
						$sql_sub3 .= " web_tag.web_tag_id != '".$row["web_tag_id"]."' and ";
						
						$web_tag_list3 .= "<optgroup label=\"".$row["web_x_tag"]."\">";	
						$web_tag_list3 .= "<option value=\"".$row["web_tag_id"]."\" selected>".$row["web_tag"]."</option>\n";
						$web_tag_list3 .= "</optgroup>";
					}
				}
				if ($sql_sub3!="") $sql_sub3 = " and (".trim($sql_sub3, " and ").")";

				$sql = "Select web_tag.web_tag_id, web_tag.web_x_tag_id, web_tag.subject as web_tag, web_x_tag.subject as web_x_tag From web_tag JOIN (web_x_tag) ON (web_x_tag.web_x_tag_id = web_tag.web_x_tag_id) Where web_tag.ifShow = '1' ".$sql_sub3." order by web_tag.asort asc";

				$rs = ConnectDB($DB, $sql);
				for ($i=0; $i<mysql_num_rows($rs); $i++) {
					$row = mysql_fetch_assoc($rs);
					
					$web_tag_list3 .= "<optgroup label=\"".$row["web_x_tag"]."\">";	
					$web_tag_list3 .= "<option value=\"".$row["web_tag_id"]."\" ".$disabled.">".$row["web_tag"]."</option>\n";
					$web_tag_list3 .= "</optgroup>";
				}
				echo $web_tag_list3;
			?>
			</select>
			<input name="multiple_value3" size="100" type="hidden" id="multiple_value3" value="<?php echo $tags ?>"  />
		  
		</div>
  </div>
</form>
<!--搜尋結束-->
<?php
  $orderTotalFlag = 0;
  if($list_search[link]) {
    $keyword = array(
      'search_sdate',
      'search_edate',
      'field',
      'keyword',
      '=',
    );
    $_link = str_replace($keyword, '', $list_search[link]);
    $checkVal = explode('&', $_link);
    $orderTotalFlag = (in_array('memberCode', $checkVal) || in_array('cdate', $checkVal)) ? 1 : 0;
  }

?>      
<form name="form" method="post" action="web_member_update.php">
  <table class="List_form">
    <tr>
      <th width="5%"><input type="checkbox" name="all" onClick="CheckAll(this, 'DeleteBox[]')" /></th>
      <!--<th width="10%">等級</th>-->
      <!--<th>帳號</th>-->
	  <!--<th>LINE</th>-->
	  <th width="10%">LINE ID</th>
      <th width="15%">姓名</th>
      <th width="15%">手機號碼</th>
      <th width="25%">註冊日期</th>
    <?php
      //if($orderTotalFlag) {
    ?>  
      <!--<th width="15%">訂單總額</th>-->
      <th width="15%">預約次數</th>
    <?php
      //}
    ?>
      <!--<th width="5%"><a class="edit">改</a></th>-->
    </tr>
  <?php
        //$orderTotalSql = '0'; 
		
        $search_sdate = $_POST["search_sdate"] ? str_filter($_POST["search_sdate"]) : str_filter($_GET["search_sdate"]);  //開始時間
        $search_edate = $_POST["search_edate"] ? str_filter($_POST["search_edate"]) : str_filter($_GET["search_edate"]);  //結束時間
        
        if($search_sdate && $search_edate) {
          $dateSearchSql = " AND (web_x_register.cdate >= '".$search_sdate." 00:00:00'  AND web_x_register.cdate <= '".$search_edate." 23:59:59')".$list_search["sql_sub2"]."";
        }

        if(in_array('memberCode', $checkVal)) {
          //$orderTotalSql = "(SELECT SUM(total) AS TotalItemsOrdered FROM web_x_order WHERE web_member.web_member_id = web_member_id AND web_x_order.paymentstatus = '付款成功') as TotalItemsOrdered";

          $orderCountSql = ", (SELECT COUNT(*) AS CountItemsOrdered FROM web_x_register WHERE web_member.web_member_id = web_member_id".$dateSearchSql.") as CountItemsOrdered";

        } else if(in_array('cdate', $checkVal)) {

          $orderCountSql = ", (SELECT COUNT(*) AS CountItemsOrdered FROM web_x_register WHERE web_x_register.web_member_id = web_member.web_member_id".$dateSearchSql.") as CountItemsOrdered";

        } else {

          $orderCountSql = ", (SELECT COUNT(*) AS CountItemsOrdered FROM web_x_register WHERE web_x_register.web_member_id = web_member.web_member_id".$dateSearchSql.") as CountItemsOrdered";
        }

        $sql = "
          Select SQL_CALC_FOUND_ROWS 
            web_member_id, 
            loginID,
			lineID,
            level, 
            uname, 
            nickname, 
            tel, 
            mobile,
			ifService,
            cdate
            ".$orderTotalSql."
            ".$orderCountSql."
          From 
            web_member ".$list_search[sql_sub]." 
          order by 
            cdate desc, 
            web_member_id desc 
        ";
		$list_paging = list_paging_sql($page, $Init_ControlPage, $sql);
        $sql .= " limit ".($page-1) * $Init_ControlPage.", ".$Init_ControlPage;
        
        $rs = ConnectDB($DB, $sql);
        //$list_paging = list_paging($page, $Init_ControlPage);
        $memberOpenAry = array(
          'Facebook',
          'Google',
          'Yahoo',
        );
        $memberOpenid = 0;
        $memberManual = 0;
        $TotalItemsOrdered = 0;
        $CountItemsOrdered = 0;
        $memberOpen = array();
        for ($i=0; $i<mysql_num_rows($rs); $i++) {
          $row = mysql_fetch_assoc($rs);
          foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
          
          $Member = getMember($web_member_id);
          
          preg_match_all("/.*?@/i",$loginID, $matches, PREG_SET_ORDER);
          if(!$matches) {
            preg_match_all("/.*?_/i",$loginID, $matches2);
            $openPath = str_replace("_", "", $matches2[0][0]);
            if(in_array($openPath, $memberOpenAry)) {
              $memberOpen[$openPath] ++;
            }
            $memberOpenid++;
            
          } else {
            preg_match_all("/.*?_/i",$matches[0][0], $matches2);
            $openPath = str_replace("_", "", $matches2[0][0]);
            if(in_array($openPath, $memberOpenAry)) {
              $memberOpen[$openPath] ++;
              //continue;
            } else {
              $memberManual++;
            } 
          }
          $TotalItemsOrdered = ($TotalItemsOrdered) ? $TotalItemsOrdered : 0;
          $CountItemsOrdered = ($CountItemsOrdered) ? $CountItemsOrdered : 0;
    ?>
    <tr>
      <td align="center"><input type="checkbox" class="chk" name="DeleteBox[]" value="<?php echo $web_member_id; ?>" /></td>
      <!--<td align="center"><?php echo $Member[level_subject]; ?></td>-->
      <!--<td><?php echo $loginID; ?></td>-->
	  <!--<td><?php echo $lineID; ?></td>-->
	  <td align="center">
	  <?php 
			if($lineID)
				echo "有";
			else 
				echo "否";
	  ?>		
	  </td>
      <td align="center">
		<?php 
			if($ifService)
				echo "★ ";
			echo "<a href=\"web_member_edit.php?web_member_id=".$web_member_id."&page=".$page.$list_search[link]."\" class=\"\">".$uname."</a>"; 
		?>
	  </td>
      <td align="center"><?php echo $mobile; ?></td>
      <td><?php echo $cdate; ?></td>
    <?php
      //if($orderTotalFlag) {
    ?>  
      <!--<td align="center"><?php echo number_format($TotalItemsOrdered); ?>元</td>-->
      <td align="center"><?php echo number_format($CountItemsOrdered); ?></td>
    <?php
      //}
    ?>
      <!--<td align="center"><a href="web_member_edit.php?web_member_id=<?php echo $web_member_id."&page=".$page.$list_search[link]; ?>" class="edit">改</a></td>-->
    </tr>
  <?php 
    }
    foreach($memberOpen as $key => $row) {
      echo $key." => ".$row." ";
    }
    //echo "一般 => ".$memberManual." ";
  ?>
  </table>
  <div id="delete">
	<a class='ajax' href="./web_line_MulitEdit.php?web_member_id=33" title="LINE@" style="display: none">LINE@</a>	
	<select name="action" id="action">
		<option value="">請選擇</option>
		<!--<option value="EditLevel">修改等級</option>-->
		<option value="Delete">刪除</option>
		<option value="Line">LINE@</option>
		</select> 
    <select name="level" id="level" style="display:none">
      <option value="">請選擇</option>
    <?php
      $sql = "Select * From web_memberlevel order by asort asc, web_memberlevel_id desc ";
      $rs = ConnectDB($DB, $sql);
      for ($i=0; $i<mysql_num_rows($rs); $i++) { 
        $row = mysql_fetch_assoc($rs);
        echo "<option value=\"".$row["web_memberlevel_id"]."\"";
        echo ">".$row["subject"]."（小計滿".number_format($row["srange"])."元可打".$row["discount"]."折）</option>\n";
      }
    ?>
    </select>
    <input type="submit" name="submit" value="執行" onClick="return _CheckDel2();" /> 
    <!--<input type="submit" name="submit" value="刪除" onClick="return CheckDel();" />-->
  </div>
  <?php list_page("web_member_list.php", $page, $list_paging[pages], $list_paging[records], $list_search[hidden], $list_search[link]); ?>
  <!--<input type="hidden" name="action" value="Delete" />-->
</form>
<?php include("../includes/footer.php"); ?>
<script>
$(function() {
	
	$('#myTags2').on('click', function(){
		$('.tagEdit').trigger('click');
	});
	
	$('#myTags2').tagit({
		//tagLimit: 10,
		//availableTags: sampleTags,
		singleField: true,
		singleFieldNode: $('#mySingleField'),
		readOnly: true
	});
	
    $("select[name='field']").on('change', function() {
      //$('#search_sdate, #search_edate, #search_paySdate, #search_payEdate').val('');
      if($(this).val() == 'TotalItemsOrdered') {
        $("input[name='keyword']").val(null).hide();
        $('#orderAmount').show();
		$("#myTags2").tagit("removeAll");
		$('form[name="form"]').css({'padding-top': '0px'});
		$('#tagsBar').hide();
	  } else if($(this).val() == 'tags') {
		  $('input[name="keyword"]').val(null).hide();	
	<?php
		if($_POST['tags']) {
			foreach(explode(',', $_POST['tags']) as $tag) {
	?>		
				$("#myTags2").tagit("createTag", '<?php echo $tag; ?>');
	<?php
			}
		}
	?>
		  $('#tagsBar').show();
		  $('form[name="form"]').css({'padding-top': '60px'});
		  $('.tagEdit').trigger('click');	
      } else {
		$("#myTags2").tagit("removeAll");  
		$('form[name="form"]').css({'padding-top': '0px'});
		$('#tagsBar').hide();  
        $('#orderAmount').hide();
        $("input[name='keyword']").val(null).show();
      }

    });
	
	<?php
		if($_POST['submit'] == '搜尋' && $_POST['field'] == 'tags') {
	?>
		$('input[name="keyword"]').val(null).hide();	
		<?php
			if($_POST['tags']) {
				foreach(explode(',', $_POST['tags']) as $tag) {
		?>		
					$("#myTags2").tagit("createTag", '<?php echo $tag; ?>');
		<?php
				}
			}
		?>	
		$('#tagsBar').show();
		$('form[name="form"]').css({'padding-top': '60px'});
		$('#tagsBar').show();
	<?php
		}
	?>	
    $("select[name='action']").on('change', function() {
      if($(this).val() == 'EditLevel') {
        $('#level').show();
      } else {
        $('#level').hide();
      }

    }).change();

    $('#orderAmount').on('keydown', '#search_samount, #search_eamount', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});

    $("#searchBtn").on('click', function(e) {
      var samount = parseInt($('#search_samount').val());
      var eamount = parseInt($('#search_eamount').val());
      //e.preventDefault();
      console.log(samount,eamount);
      if(samount || eamount) {
          //console.log(Math.samonut,Math.eamonut);
          if((samount > eamount) || !samount || !eamount) {
            alert('輸入正確金額');
            e.preventDefault();
          }
      } else {
        $("#searchForm").submit();
      }

    });
	
	$('#bestsellers3').multiSelect({
        keepOrder: true,
        //selectableHeader: "<div align='center' style=\"background-color:#CCC\">請選擇標籤　<a href=\"javascript:;\" id=\"select-all2\"><span class=\"font_red\">全選</span></a></div>",
		selectableHeader: "<div align='center' style=\"background-color:#CCC\">請選擇標籤</div>",
        //selectionHeader: "<div align='center' style=\"background-color:#CCC\">已被選取的標籤　<a href=\"javascript:;\" id=\"deselect-all2\"><span class=\"font_green\">清除</span></a></div>",
		selectionHeader: "<div align='center' style=\"background-color:#CCC\">已被選取的標籤</div>",
        afterSelect: function(value) {
            var get_val = $("#multiple_value3").val();
            var hidden_val = (get_val != "") ? get_val + "," : get_val;
			var val_text;
            $("#multiple_value3").val(hidden_val + "" + value);
			
			$('#bestsellers3').find('option').each(function(k,e) {
				if($(e).val() == value) {
					val_text = $(e).text();
				}
				//console.log(val_text);
			});
			$("#myTags2").tagit("createTag", val_text);
        },
        afterDeselect: function(value) {
            var get_val = $("#multiple_value3").val();
            var new_val = get_val.replace(value, "");
			var val_text;
            $("#multiple_value3").val(new_val);
			$('#bestsellers3').find('option').each(function(k,e) {
				if($(e).val() == value) {
					val_text = $(e).text();
				}
				//console.log(val_text);
			});
			$("#myTags2").tagit("removeTagByLabel", val_text);
        }
    });
})
function _CheckDel2() {
    if (confirm("確定要執行？")) {
    	var _action = $("select[name='action']").find('option:selected').val();
        if(_action == 'Line' ) {
        	var id = new Array();
	        $('.chk:checked').each(function(k,v) {
	        	//console.log($(v).val());
	        	id.push($(v).val());
	        });
	        //alert(id.length);	
	        if(id.length) {
	        	var idStr = id.join();
	        	$('.ajax').attr('href', './web_line_MulitEdit.php?web_member_id='+idStr+'&action='+_action).trigger('click');
	        } else {
	        	alert('請勾選要傳訊之會員');
	        }
	        return false;
        } else {
        	return true;
        }	
    } else {
        return false;
    }
}
</script>
</body>
</html>