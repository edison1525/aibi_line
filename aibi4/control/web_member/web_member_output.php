<?php
include("../includes/includes.php");

require "../class/excel/Classes/PHPExcel.php";
require "../class/excel/Classes/PHPExcel/IOFactory.php";

$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()
			->setCreator("Data")
			->setLastModifiedBy("Data")
			->setTitle("Data")
			->setSubject("Data")
			->setDescription("Data")
			->setKeywords("Data")
			->setCategory("Data");

$CellArray = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ");

$FieldArray = array(
	//"loginID" => "帳號",
	//"web_member_id" => "會員等級",
	"uname" => "姓名",
	//"sex" => "性別",
	//"nickname" => "暱稱",
	"birthday" => "生日",
	//"tel" => "聯絡電話",
	"mobile" => "手機號碼",
	//"email" => "電子信箱",
	//"city" => "縣市",
	//"area" => "鄉鎮市區",
	//"zip" => "郵遞區號",
	//"address" => "通訊地址",
	//"ifEpaper" => "訂閱電子報",
	//"howknow" => "如何得知".$Init_WebTitle."？",
	//"howknow_other" => "如何得知".$Init_WebTitle."的說明",
	//"memberCode" => "推薦代碼",
	"cdate" => "註冊日期"
);

$i = 0;
$sql_field = "";
foreach ($FieldArray as $_key=>$_value) {
	//echo $_key."：".$_value."<br />";		//欄位英文名：中文名
	$sql_field .= $_key.", ";
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue($CellArray[$i]."1", $_value);
	$i++;
}
$sql_field = trim($sql_field, ", ");

$sql_sub = " Where 1 = 1 ";
$field = $_POST["field"] ? str_filter($_POST["field"]) : str_filter($_GET["field"]);	//欄位
$keyword = $_POST["keyword"] ? str_filter($_POST["keyword"]) : str_filter($_GET["keyword"]);	//關鍵字
$search_sdate = $_POST["search_sdate"] ? str_filter($_POST["search_sdate"]) : str_filter($_GET["search_sdate"]);	//開始時間
$search_edate = $_POST["search_edate"] ? str_filter($_POST["search_edate"]) : str_filter($_GET["search_edate"]);	//結束時間
$search_samount = $_POST["search_samount"] ? str_filter($_POST["search_samount"]) : str_filter($_GET["search_samount"]);  //開始金額
$search_eamount = $_POST["search_eamount"] ? str_filter($_POST["search_eamount"]) : str_filter($_GET["search_eamount"]);  //結束金額
$field2 = ($field != 'cdate') ? $field : 'memberCode';
if($field == 'TotalItemsOrdered') { 
  $sql_sub = " Having 1 = 1 ";
  $sql_sub .= " and (TotalItemsOrdered >= '".$search_samount."'  AND TotalItemsOrdered <= '".$search_eamount."') ";
  $list_search[link] .= "&search_sdate=".$search_sdate."&search_edate=".$search_edate."&search_samount=".$search_samount."&search_eamount=".$search_eamount;
}
if ($keyword!="") {
	$keywords = explode(" ", $keyword);
	for ($i=0; $i<sizeof($keywords); $i++) {
		if($field == 'memberCode') {
          	if ($keywords[$i]!="") $sql_sub .= " and (".$field2." like '%".$keywords[$i]."%') ";
        } else if($field == 'CountItemsOrdered') { 
          	$sql_sub = " Having 1 = 1 ";
          	if ($keywords[$i]!="") $sql_sub .= " and (".$field2." = '".$keywords[$i]."') ";
          	//if ($keywords[$i]!="") $list_search[sql_sub] .= " and (".$field2." = '".$keywords[$i]."') ";
        } else {
          	if ($keywords[$i]!="") $sql_sub .= " and (".$field2." like '".$keywords[$i]."') ";
        }
		$list_search[sql_sub] = $sql_sub;
	}
	if($search_sdate != "" && $search_edate != "") {
		if($search_sdate > $search_edate){
			echo "開始時間不能大於結束時間";
			return;
		} else {
			if($field == 'pincode') {
	            $sql2 = "SELECT web_x_order_ordernum FROM `web_order` ".$sql_sub." AND (cdate >= '".$search_sdate." 00:00:00'  AND cdate <= '".$search_edate." 23:59:59')";
	            $rs2 = ConnectDB($DB, $sql2);
	            for ($i=0; $i<mysql_num_rows($rs2); $i++) {
	              $row2 = mysql_fetch_assoc($rs2);
	              $web_x_order_ordernum[] = "'".str_front($row2[web_x_order_ordernum])."'";
	            }
	            $web_x_order_ordernumRow = implode(',', $web_x_order_ordernum); 
	            $sql_sub2 = " AND web_x_order.ordernum IN (".$web_x_order_ordernumRow.")";

	            $sql = "SELECT web_member_id FROM `web_x_order` WHERE 1 ".$sql_sub2." AND web_x_order.paymentstatus = '付款成功' AND web_x_order.states = '出貨' AND web_x_order.shipdate <= DATE_SUB(DATE(now()), INTERVAL 7 DAY) AND (cdate >= '".$search_sdate." 00:00:00'  AND cdate <= '".$search_edate." 23:59:59')";
	            $rs = ConnectDB($DB, $sql);
	            for ($i=0; $i<mysql_num_rows($rs); $i++) {
	              $row = mysql_fetch_assoc($rs);
	              $member[] = str_front($row[web_member_id]);
	            }
	            $memberIdRow = implode(',', $member); 
	            $sql_sub = " Where 1 = 1 ";
	            $sql_sub .= " and (web_member_id IN (".$memberIdRow.")) ";   

          	} else {
	            $sql_sub .= " and (cdate >= '".$search_sdate." 00:00:00'  AND cdate <= '".$search_edate." 23:59:59') ";
          	}
		}
	} else {
		if($field == 'pincode') {
            $sql2 = "SELECT web_x_order_ordernum FROM `web_order` ".$sql_sub."";
            $rs2 = ConnectDB($DB, $sql2);
            for ($i=0; $i<mysql_num_rows($rs2); $i++) {
              $row2 = mysql_fetch_assoc($rs2);
              $web_x_order_ordernum[] = "'".str_front($row2[web_x_order_ordernum])."'";
            }
            $web_x_order_ordernumRow = implode(',', $web_x_order_ordernum); 
            $sql_sub2 = " AND web_x_order.ordernum IN (".$web_x_order_ordernumRow.")";

            $sql = "SELECT web_member_id FROM `web_x_order` WHERE 1 ".$sql_sub2." AND web_x_order.paymentstatus = '付款成功' AND web_x_order.states = '出貨' AND web_x_order.shipdate <= DATE_SUB(DATE(now()), INTERVAL 7 DAY) ";
            $rs = ConnectDB($DB, $sql);
            for ($i=0; $i<mysql_num_rows($rs); $i++) {
              $row = mysql_fetch_assoc($rs);
              $member[] = str_front($row[web_member_id]);
            }
            $memberIdRow = implode(',', $member); 
            $sql_sub = " Where 1 = 1 ";
            $sql_sub .= " and (web_member_id IN (".$memberIdRow.")) ";   

        }
	}
} else if($search_sdate != "" && $search_edate != "")  {
	if($search_sdate > $search_edate){
		echo "開始時間不能大於結束時間";
		return;
	} else {
		$sql_sub .= " and (cdate >= '".$search_sdate." 00:00:00'  AND cdate <= '".$search_edate." 23:59:59') ";
	}
} else if($field == 'tags') {
	$list_search[sql_sub] = " Where 1 = 1 ";
	$list_search[sql_sub] .= " and (web_member_id IN (".$_GET['_web_member_id'].")) "; 
	$list_search[link] .= "&field=".$field."&web_member_id=".$_GET['_web_member_id'];
}

if($search_sdate && $search_edate) {
  $dateSearchSql = " AND (web_x_order.cdate >= '".$search_sdate." 00:00:00'  AND web_x_order.cdate <= '".$search_edate." 23:59:59')".$list_search["sql_sub2"]."";
}

if(in_array('memberCode', $checkVal)) {
  //$orderTotalSql = "(SELECT SUM(total) AS TotalItemsOrdered FROM web_x_order WHERE web_member.web_member_id = web_member_id AND web_x_order.paymentstatus = '付款成功') as TotalItemsOrdered";

  //出貨付款成功且出貨滿7天
  $orderTotalSql = ", (SELECT SUM(total) AS TotalItemsOrdered FROM web_x_order WHERE web_member.web_member_id = web_member_id AND web_x_order.paymentstatus = '付款成功' AND web_x_order.states = '出貨' AND web_x_order.shipdate <= DATE_SUB(DATE(now()), INTERVAL 7 DAY)".$dateSearchSql.") as TotalItemsOrdered";

  //出貨付款成功且出貨滿7天
  $orderCountSql = ", (SELECT COUNT(*) AS CountItemsOrdered FROM web_x_order WHERE web_member.web_member_id = web_member_id AND web_x_order.paymentstatus = '付款成功' AND web_x_order.states = '出貨' AND web_x_order.shipdate <= DATE_SUB(DATE(now()), INTERVAL 7 DAY)".$dateSearchSql.") as CountItemsOrdered";

} else if(in_array('cdate', $checkVal)) {
  //$orderTotalSql = ", (SELECT SUM(total) AS TotalItemsOrdered FROM web_x_order WHERE web_x_order.web_member_id = web_member.web_member_id AND web_x_order.paymentstatus = '付款成功') as TotalItemsOrdered";
  
  //出貨付款成功且出貨滿7天
  $orderTotalSql = ", (SELECT SUM(total) AS TotalItemsOrdered FROM web_x_order WHERE web_x_order.web_member_id = web_member.web_member_id AND web_x_order.paymentstatus = '付款成功' AND web_x_order.states = '出貨' AND web_x_order.shipdate <= DATE_SUB(DATE(now()), INTERVAL 7 DAY)".$dateSearchSql.") as TotalItemsOrdered";

  //出貨付款成功且出貨滿7天
  $orderCountSql = ", (SELECT COUNT(*) AS CountItemsOrdered FROM web_x_order WHERE web_x_order.web_member_id = web_member.web_member_id AND web_x_order.paymentstatus = '付款成功' AND web_x_order.states = '出貨' AND web_x_order.shipdate <= DATE_SUB(DATE(now()), INTERVAL 7 DAY)".$dateSearchSql.") as CountItemsOrdered";

} else {

  //出貨付款成功且出貨滿7天
  $orderTotalSql = ", (SELECT SUM(total) AS TotalItemsOrdered FROM web_x_order WHERE web_x_order.web_member_id = web_member.web_member_id AND web_x_order.paymentstatus = '付款成功' AND web_x_order.states = '出貨' AND web_x_order.shipdate <= DATE_SUB(DATE(now()), INTERVAL 7 DAY)".$dateSearchSql.") as TotalItemsOrdered";

  //出貨付款成功且出貨滿7天
  $orderCountSql = ", (SELECT COUNT(*) AS CountItemsOrdered FROM web_x_order WHERE web_x_order.web_member_id = web_member.web_member_id AND web_x_order.paymentstatus = '付款成功' AND web_x_order.states = '出貨' AND web_x_order.shipdate <= DATE_SUB(DATE(now()), INTERVAL 7 DAY)".$dateSearchSql.") as CountItemsOrdered";
}

$j = 2;
$evnRow = 1500;
//$sql = "Select ".$sql_field.$orderTotalSql.$orderCountSql." From web_member ".$sql_sub." order by cdate desc, web_member_id desc ";
$sql = "Select ".$sql_field." From web_member ".$list_search[sql_sub]." order by cdate desc, web_member_id desc ";
if($page)
	$sql .= " limit ".($page-1) * $evnRow.", ".$evnRow;
//echo $sql;			
//exit;
$rs = ConnectDB($DB, $sql);
for ($i=0; $i<mysql_num_rows($rs); $i++) {
	$row = mysql_fetch_assoc($rs);
	$k = 0;
	foreach($row as $_key=>$_value) {
		$$_key = $row[$_key];
		
		switch ($_key) {
			case "web_member_id":	//會員等級
				$Member = getMember($web_member_id);
				$$_key = $Member[level_subject];
				unset($Member);
				break;
			case "birthday":
				$$_key = ($$_key=="0000-00-00") ? "" : $$_key;
				break;
			case "ifEpaper":
				$$_key = ($$_key==1) ? "是" : "否";
				break;
			default:
		}
		
		$objPHPExcel->getActiveSheet()->getCell($CellArray[$k].$j)->setValueExplicit($$_key, PHPExcel_Cell_DataType::TYPE_STRING);
		
		$k++;
	}
	
	$j++;
}

$objPHPExcel->getActiveSheet()->setTitle("會員資料");
$objPHPExcel->setActiveSheetIndex(0);

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.iconv("UTF-8", "Big5", $Init_WebTitle."_會員資料_").date("YmdHis").'.xls"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
$objWriter->save('php://output'); 
//$objWriter->save("../../uploadfiles/".iconv("UTF-8", "Big5", $Init_WebTitle."_會員資料_").date("YmdHis").'.xls');
exit;	
?>