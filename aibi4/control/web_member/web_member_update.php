<?php
	include("../includes/includes.php");
	
	$action = $_POST["action"] ? $_POST["action"] : $_GET["action"];
	$adminInfo = getAdmin($_SESSION[Member][ID]);
	$ip = $_SERVER["HTTP_VIA"] ? $_SERVER["HTTP_X_FORWARDED_FOR"] : $_SERVER["REMOTE_ADDR"];	//IP
	//操作履歷
	if ($action == "Edit") {
		$TableName = "web_member";
		$sql = "SHOW FULL FIELDS FROM `".$TableName."`";
		$rs = ConnectDB($DB, $sql);
		$tabeInfoAry = array();
		for ($i=0; $i<mysql_num_rows($rs); $i++) {
			$row = mysql_fetch_assoc($rs);
			$tabeInfoAry[$row[Field]] = $row[Comment];
		}

		$sql2 = "Select * From web_member Where web_member_id like '".$_POST[web_member_id]."' ";
		$rs2 = ConnectDB($DB, $sql2);	
		$inDbInfo = array();
		for ($i=0; $i<mysql_num_rows($rs2); $i++) {
			$row2 = mysql_fetch_assoc($rs2);
			$inDbInfo = $row2;
		}
		$_inDbInfo = array();
		$postAry = array();
		foreach($_POST as $key => $postVal) {
			//echo $key.": ".$tabeInfoAry[$key].": ".$postVal."</br>";
			$_inDbInfo[$tabeInfoAry[$key]] = $inDbInfo[$key];
			$postAry[$tabeInfoAry[$key]] = $postVal;
		}
		//exit;
		$result = array_diff_assoc($postAry,$_inDbInfo);
		$nameEditFlag = 0;
		//查看是否有更改
		if(count($result)) {
			$recLogAry = array();
			$_recLogAry = array();
			foreach($result as $key => $postVal) {
				if($key == "姓名") {
					$nameEditFlag = 1;
				}
				//echo $key.'原資料:'.$_inDbInfo[$key]."</br>";
				//$_recLogAry[$key] = $_inDbInfo[$key]." -> ".$postVal;
				$recLogAry[urlencode($key)] = urlencode($_inDbInfo[$key])." -> ".urlencode($postVal);
			}
			$recLogAry[urlencode(操作日期)] = date('Y-m-d H:i:s');
			$recLogAry[urlencode(操作者)] = $adminInfo[loginID]."[".urlencode($adminInfo[uname])."]";
			$recLogAry[IP] = $ip;
			$recLog = json_encode($recLogAry);

			if(count($recLogAry)) {
				//$sql3 = "Update web_x_order set log = '".$recLog."' Where web_x_order_id = '".$_POST[web_x_order_id]."'";
				$sql3 = "
					Insert into 
						web_memberlog 
							(
								web_member_id, 
								log, 
								uname, 
								IP, 
								date
							) 
						values 
							(
								'".$inDbInfo[web_member_id]."', 
								'$recLog', 
								'".$adminInfo[uname]."', 
								'$ip', 
								'".date('Y-m-d H:i:s')."'
							) 
				";	
				$rs3 = ConnectDB($DB, $sql3);
			}
		}

	}

	//新增編輯
	if ($action=="Edit") {
		//$debug = true;
		$TableName = "web_member";
		$AccurateAction = "Get";
		require("../includes/accurate.php");
		
		$web_member_id = intval($_POST["web_member_id"]);
		$uname = ($_POST["uname"]);
		$mobile = ($_POST["mobile"]);
		
		//標籤 
		$multiple_value2 = array_values(array_filter(explode(",", $_POST["multiple_value2"])));
		$_POST['tags'] = implode(",", array_unique($multiple_value2));

		if ($uname=="") RunAlert("請輸入姓名");
		
		//if ($tel=="") RunAlert("請輸入聯絡電話（區碼+電話號碼，格式如04-XXXXXXXX）");
		if (!preg_match("/^09[0-9]{8}$/", $mobile)) RunAlert("請輸入正確的手機號碼（格式為09XXXXXXXX）");
		
		//if (!preg_match("/^[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/", $email)) RunAlert("請輸入正確的電子信箱");

		if ($web_member_id>0) {
			$ForbidData = array();
			array_push($ForbidData, "loginID", "age", "wishlist", "cdate", "ifAccept");
			//$debug = true;
			$AccurateAction = "Update";
			require("../includes/accurate.php");
			
			//姓名有修改
			if($nameEditFlag) {
				
				$sql = "Update web_x_register set order_name = '".$uname."' Where web_member_id = '".$web_member_id."' and self = 0 ";
				$rs = ConnectDB($DB, $sql);
				
				$sql = "Update web_x_register set order_name = '".$uname."', accept_name = '".$uname."' Where web_member_id = '".$web_member_id."' and self = 1 ";
				$rs = ConnectDB($DB, $sql);
				
				$sql = "Update web_x_order set order_name = '".$uname."', accept_name = '".$uname."' Where web_member_id = '".$web_member_id."'";
				$rs = ConnectDB($DB, $sql);
			}
			
			
			
		} else {
			$cdate = date("Y-m-d H:i:s");	//註冊日期
			$AccurateAction = "Insert";
			require("../includes/accurate.php");
		}
	} 
	
	//刪除
	if ($action=="Delete") {
		$DeleteBox = $_POST["DeleteBox"];
		if (!is_array($DeleteBox)) die();
		
		foreach ($DeleteBox as $value) {
			

			//訂單詳細
			$sql = "Select ordernum From web_x_order Where web_member_id = '".$value."' ";
			$rs = ConnectDB($DB, $sql);
			for ($i=0; $i<mysql_num_rows($rs); $i++) {
				$row = mysql_fetch_assoc($rs);

				$sql2 = "Delete From web_order Where web_x_order_ordernum like '".$row["ordernum"]."' ";
				$rs2 = ConnectDB($DB, $sql2);
			}
			
			//訂單
			$sql = "
				Delete From 
					web_x_order
				INNER JOIN 
					web_order ON web_x_order.ordernum = web_order.web_x_order_ordernum		
				Where 
					web_x_order.web_member_id = '".$value."' ";
			$rs = ConnectDB($DB, $sql);
			
			$sql = "Delete From web_member Where web_member_id = '".$value."' ";
			$rs = ConnectDB($DB, $sql);
			
			$sql = "Delete From web_bonus Where web_member_id = '".$value."' ";
			$rs = ConnectDB($DB, $sql);
			
			$sql = "Delete From web_member_money Where web_member_id = '".$value."' ";
			$rs = ConnectDB($DB, $sql);
			
			$sql = "Update web_x_register set paymentstatus = '取消' Where web_member_id = '".$value."' ";
			$rs = ConnectDB($DB, $sql);
		}
		
		$sql = "Select count(*) as counter From web_member ";
		$rs = ConnectDB($DB, $sql);
		if (mysql_result($rs, 0, "counter")==0) {
			$sql = "Truncate table web_member";
			$rs = ConnectDB($DB, $sql);
		}
		/*
		$sql = "ALTER TABLE web_member AUTO_INCREMENT = 1 ";
		$rs = ConnectDB($DB, $sql);
		*/
	} else if($action=="EditLevel") {		//修改會員等級

		$DeleteBox = $_POST["DeleteBox"];
		if (!is_array($DeleteBox)) die();
		
		$level = $_POST["level"];
		if (!$level) 
			RunJs("web_member_list.php?page=".intval($_POST["page"])."&field=".$_POST["field"]."&keyword=".urlencode($_POST["keyword"]));

		foreach ($DeleteBox as $value) {
			//訂單
			$sql = "Update web_member set level = '$level' Where web_member_id = '".$value."' ";
			//echo "</br>";
			$rs = ConnectDB($DB, $sql);
		}	

	}
	
	RunJs("web_member_list.php?page=".intval($_POST["page"])."&field=".$_POST["field"]."&keyword=".urlencode($_POST["keyword"]));
?>