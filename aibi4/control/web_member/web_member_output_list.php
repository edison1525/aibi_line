<?php 
  include("../includes/head.php"); 
  if ($search_title!="") $search_title = "（".$search_title."）";
?>
<body>
<?php 
  //include("../includes/left.php");
  include("../multi-menu/left.php"); 
?>
<h1>匯出列表</h1>
<div id="nav">目前位置：<?php echo $nav_title; ?> > <a href="web_member_list.php">會員</a> > 列表</div>
<form name="formSearch" method="post" action="web_member_output_list.php">
  <div id="search">
  <?php 
	//$list_search = list_search_by_time(array("uname" => "姓名", "mobile" => "手機號碼", "tags" => "標籤")); 
  ?>  
  </div>
  <a class="tagEdit" href="#tagSelect"></a>
  <div style="display: none;">
		<div id="tagSelect">
			<select name="bestsellers3[]" id="bestsellers3" multiple="multiple" style="width: 45%">
			<?php
				$web_tag_list3 = "";
				$sql_sub3 = "";
				
				//已被選取的產品	
				foreach ($_POST['bestsellers3'] as $value) {
					$sql = "Select web_tag.web_tag_id, web_tag.web_x_tag_id, web_tag.subject as web_tag, web_x_tag.subject as web_x_tag From web_tag JOIN (web_x_tag) ON (web_x_tag.web_x_tag_id = web_tag.web_x_tag_id) Where web_tag.web_tag_id = '".$value."' ";
					$rs = ConnectDB($DB, $sql);
					for ($i=0; $i<mysql_num_rows($rs); $i++) {
						$row = mysql_fetch_assoc($rs);
						$sql_sub3 .= " web_tag.web_tag_id != '".$row["web_tag_id"]."' and ";
						
						$web_tag_list3 .= "<optgroup label=\"".$row["web_x_tag"]."\">";	
						$web_tag_list3 .= "<option value=\"".$row["web_tag_id"]."\" selected>".$row["web_tag"]."</option>\n";
						$web_tag_list3 .= "</optgroup>";
					}
				}
				if ($sql_sub3!="") $sql_sub3 = " and (".trim($sql_sub3, " and ").")";

				$sql = "Select web_tag.web_tag_id, web_tag.web_x_tag_id, web_tag.subject as web_tag, web_x_tag.subject as web_x_tag From web_tag JOIN (web_x_tag) ON (web_x_tag.web_x_tag_id = web_tag.web_x_tag_id) Where web_tag.ifShow = '1' ".$sql_sub3." order by web_tag.asort asc";

				$rs = ConnectDB($DB, $sql);
				for ($i=0; $i<mysql_num_rows($rs); $i++) {
					$row = mysql_fetch_assoc($rs);
					
					$web_tag_list3 .= "<optgroup label=\"".$row["web_x_tag"]."\">";	
					$web_tag_list3 .= "<option value=\"".$row["web_tag_id"]."\" ".$disabled.">".$row["web_tag"]."</option>\n";
					$web_tag_list3 .= "</optgroup>";
				}
				echo $web_tag_list3;
			?>
			</select>
			<input name="multiple_value3" size="100" type="hidden" id="multiple_value3" value="<?php echo $tags ?>"  />
		  
		</div>
  </div>
</form>  
  <table class="List_form">
    <tr>
  	  <th width="5%">匯出檔案</th> 	  
    </tr>
	<?php  
    $FieldArray = array(
      "loginID" => "帳號",
      "web_member_id" => "會員等級",
      "uname" => "姓名",
      "sex" => "性別",
      "nickname" => "暱稱",
      "birthday" => "生日",
      "tel" => "聯絡電話",
      "mobile" => "手機號碼",
      "email" => "電子信箱",
      "city" => "縣市",
      "area" => "鄉鎮市區",
      "zip" => "郵遞區號",
      "address" => "通訊地址",
      "ifEpaper" => "訂閱電子報",
      "howknow" => "如何得知".$Init_WebTitle."？",
      "howknow_other" => "如何得知".$Init_WebTitle."的說明",
      "memberCode" => "推薦代碼",
      "cdate" => "註冊日期"
    );
  
    $i = 0;
    $sql_field = "";
    foreach ($FieldArray as $_key=>$_value) {
      //echo $_key."：".$_value."<br />";    //欄位英文名：中文名
      $sql_field .= $_key.", ";
      $i++;
    }
    $sql_field = trim($sql_field, ", ");
    
    $sql_sub = " Where 1 = 1 ";
    $field = $_POST["field"] ? str_filter($_POST["field"]) : str_filter($_GET["field"]);  //欄位
    $keyword = $_POST["keyword"] ? str_filter($_POST["keyword"]) : str_filter($_GET["keyword"]);  //關鍵字
    $search_sdate = $_POST["search_sdate"] ? str_filter($_POST["search_sdate"]) : str_filter($_GET["search_sdate"]);  //開始時間
    $search_edate = $_POST["search_edate"] ? str_filter($_POST["search_edate"]) : str_filter($_GET["search_edate"]);  //結束時間
    $search_samount = $_POST["search_samount"] ? str_filter($_POST["search_samount"]) : str_filter($_GET["search_samount"]);  //開始金額
    $search_eamount = $_POST["search_eamount"] ? str_filter($_POST["search_eamount"]) : str_filter($_GET["search_eamount"]);  //結束金額
    $field2 = ($field != 'cdate') ? $field : 'memberCode';
    //$list_search[link] = "&field=".$field;
    $list_search[link] = "&field=".$field;
    
    if ($keyword!="") {
      $keywords = explode(" ", $keyword);
      for ($i=0; $i<sizeof($keywords); $i++) {
        if($field == 'memberCode') {
          if ($keywords[$i]!="") $sql_sub .= " and (".$field2." like '%".$keywords[$i]."%') ";
        } else if($field == 'CountItemsOrdered') { 
          $sql_sub = " Having 1 = 1 ";
          if ($keywords[$i]!="") $sql_sub .= " and (".$field2." = '".$keywords[$i]."') ";
          //if ($keywords[$i]!="") $list_search[sql_sub] .= " and (".$field2." = '".$keywords[$i]."') ";
        } else {
          if ($keywords[$i]!="") $sql_sub .= " and (".$field2." like '".$keywords[$i]."') ";
		  
        }
		$list_search[sql_sub] = $sql_sub;
      }
      if($search_sdate != "" && $search_edate != "") {
        if($search_sdate > $search_edate){
          echo "開始時間不能大於結束時間";
          return;
        } else {
          if($field == 'pincode') {
            $sql2 = "SELECT web_x_order_ordernum FROM `web_order` ".$sql_sub." AND (cdate >= '".$search_sdate." 00:00:00'  AND cdate <= '".$search_edate." 23:59:59')";
            $rs2 = ConnectDB($DB, $sql2);
            for ($i=0; $i<mysql_num_rows($rs2); $i++) {
              $row2 = mysql_fetch_assoc($rs2);
              $web_x_order_ordernum[] = "'".str_front($row2[web_x_order_ordernum])."'";
            }
            $web_x_order_ordernumRow = implode(',', $web_x_order_ordernum); 
            $sql_sub2 = " AND web_x_order.ordernum IN (".$web_x_order_ordernumRow.")";

            $sql = "SELECT web_member_id FROM `web_x_order` WHERE 1 ".$sql_sub2." AND web_x_order.paymentstatus = '付款成功' AND web_x_order.states = '出貨' AND web_x_order.shipdate <= DATE_SUB(DATE(now()), INTERVAL 7 DAY) AND (cdate >= '".$search_sdate." 00:00:00'  AND cdate <= '".$search_edate." 23:59:59')";
            $rs = ConnectDB($DB, $sql);
            for ($i=0; $i<mysql_num_rows($rs); $i++) {
              $row = mysql_fetch_assoc($rs);
              $member[] = str_front($row[web_member_id]);
            }
            $memberIdRow = implode(',', $member); 
            $sql_sub = " Where 1 = 1 ";
            $sql_sub .= " and (web_member_id IN (".$memberIdRow.")) ";     
            $list_search[link] = "&field=pincode&keyword=".urlencode($keyword)."&search_sdate=".$search_sdate."&search_edate=".$search_edate;

          } else {
            $sql_sub .= " and (cdate >= '".$search_sdate." 00:00:00'  AND cdate <= '".$search_edate." 23:59:59') ";
          }  
        }
      } else {
        if($field == 'pincode') {
            $sql2 = "SELECT web_x_order_ordernum FROM `web_order` ".$sql_sub."";
            $rs2 = ConnectDB($DB, $sql2);
            for ($i=0; $i<mysql_num_rows($rs2); $i++) {
              $row2 = mysql_fetch_assoc($rs2);
              $web_x_order_ordernum[] = "'".str_front($row2[web_x_order_ordernum])."'";
            }
            $web_x_order_ordernumRow = implode(',', $web_x_order_ordernum); 
            $sql_sub2 = " AND web_x_order.ordernum IN (".$web_x_order_ordernumRow.")";

            $sql = "SELECT web_member_id FROM `web_x_order` WHERE 1 ".$sql_sub2." AND web_x_order.paymentstatus = '付款成功' AND web_x_order.states = '出貨' AND web_x_order.shipdate <= DATE_SUB(DATE(now()), INTERVAL 7 DAY) ";
            $rs = ConnectDB($DB, $sql);
            for ($i=0; $i<mysql_num_rows($rs); $i++) {
              $row = mysql_fetch_assoc($rs);
              $member[] = str_front($row[web_member_id]);
            }
            $memberIdRow = implode(',', $member); 
            $sql_sub = " Where 1 = 1 ";
            $sql_sub .= " and (web_member_id IN (".$memberIdRow.")) ";   

        }
        $list_search[link] .= "&field=".$field."&keyword=".urlencode($keyword);
      }
    } else if($search_sdate != "" && $search_edate != "")  {
      if($search_sdate > $search_edate){
        echo "開始時間不能大於結束時間";
        return;
      } else {
        $sql_sub .= " and (cdate >= '".$search_sdate." 00:00:00'  AND cdate <= '".$search_edate." 23:59:59') ";
      }
    } else if($field == 'tags') {
		$list_search[sql_sub] = " Where 1 = 1 ";
		$list_search[sql_sub] .= " and (web_member_id IN (".$_GET['_web_member_id'].")) "; 
		$list_search[link] .= "&field=".$field."&_web_member_id=".$_GET['_web_member_id'];
	}

    if($search_sdate && $search_edate) {
      $dateSearchSql = " AND (web_x_order.cdate >= '".$search_sdate." 00:00:00'  AND web_x_order.cdate <= '".$search_edate." 23:59:59')".$list_search["sql_sub2"]."";
    }

    $j = 2;
    //$sql = "Select ".$sql_field." From web_x_order ".$list_search[sql_sub]." order by cdate desc, web_x_order_id desc ";
    //$sql = "Select ".$sql_field.$orderTotalSql.$orderCountSql." From web_member ".$sql_sub." order by cdate desc, web_member_id desc ";
	$sql = "Select ".$sql_field." From web_member ".$list_search[sql_sub]." order by cdate desc, web_member_id desc ";
    $rs = ConnectDB($DB, $sql);

    $allCount = mysql_num_rows($rs);
    echo "共: ".$allCount." 筆資料";
    $evnRow = 1500;
    $loopRows = ($allCount > $evnRow) ? ceil($allCount / $evnRow) : 1;
    if($allCount) {
    for($i=1; $i<=($loopRows); $i++) {
  ?>
    <tr>
      <td align="center"><a href="web_member_output.php?page=<?php echo ($i); ?><?php echo $list_search[link]; ?>" title="<?php echo $search_title; ?>"><?php echo $Init_WebTitle."_會員資料".$search_title."_".date("YmdHis")."(".($i).").xls"; ?></a></td>
    </tr>  
	<?php } } ?>
  </table>
<?php include("../includes/footer.php"); ?>
<script>
  $(function() {
    $('#myTags2').on('click', function(){
		$('.tagEdit').trigger('click');
	});
	
	$('#myTags2').tagit({
		//tagLimit: 10,
		//availableTags: sampleTags,
		singleField: true,
		singleFieldNode: $('#mySingleField'),
		readOnly: true
	});
	
    $("select[name='field']").on('change', function() {
      //$('#search_sdate, #search_edate, #search_paySdate, #search_payEdate').val('');
      if($(this).val() == 'TotalItemsOrdered') {
        $("input[name='keyword']").val(null).hide();
        $('#orderAmount').show();
		$("#myTags2").tagit("removeAll");
		$('form[name="form"]').css({'padding-bottom': '0px'});
		$('#tagsBar').hide();
	  } else if($(this).val() == 'tags') {
		  $('input[name="keyword"]').val(null).hide();	
	<?php
		if($_POST['tags']) {
			foreach(explode(',', $_POST['tags']) as $tag) {
	?>		
				$("#myTags2").tagit("createTag", '<?php echo $tag; ?>');
	<?php
			}
		}
	?>
		  $('#tagsBar').show();
		  $('form[name="formSearch"]').css({'padding-bottom': '70px'});
		  $('.tagEdit').trigger('click');	
      } else {
		$("#myTags2").tagit("removeAll");  
		$('form[name="formSearch"]').css({'padding-bottom': '0px'});
		$('#tagsBar').hide();  
        $('#orderAmount').hide();
        $("input[name='keyword']").val(null).show();
      }

    });
	
	<?php
		if($_POST['submit'] == '搜尋' && $_POST['field'] == 'tags') {
	?>
		$('input[name="keyword"]').val(null).hide();	
		<?php
			if($_POST['tags']) {
				foreach(explode(',', $_POST['tags']) as $tag) {
		?>		
					$("#myTags2").tagit("createTag", '<?php echo $tag; ?>');
		<?php
				}
			}
		?>	
		$('#tagsBar').show();
		$('form[name="formSearch"]').css({'padding-bottom': '70px'});
		$('#tagsBar').show();
	<?php
		}
	?>

    $('#orderAmount').on('keydown', '#search_samount, #search_eamount', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});

    $("#searchBtn").on('click', function(e) {
      var samount = parseInt($('#search_samount').val());
      var eamount = parseInt($('#search_eamount').val());
      //e.preventDefault();
      console.log(samount,eamount);
      if(samount || eamount) {
          //console.log(Math.samonut,Math.eamonut);
          if((samount > eamount) || !samount || !eamount) {
            alert('輸入正確金額');
            e.preventDefault();
          }
      } else {
        $("#searchForm").submit();
      }

    });
	
	$('#bestsellers3').multiSelect({
        keepOrder: true,
        //selectableHeader: "<div align='center' style=\"background-color:#CCC\">請選擇標籤　<a href=\"javascript:;\" id=\"select-all2\"><span class=\"font_red\">全選</span></a></div>",
		selectableHeader: "<div align='center' style=\"background-color:#CCC\">請選擇標籤</div>",
        //selectionHeader: "<div align='center' style=\"background-color:#CCC\">已被選取的標籤　<a href=\"javascript:;\" id=\"deselect-all2\"><span class=\"font_green\">清除</span></a></div>",
		selectionHeader: "<div align='center' style=\"background-color:#CCC\">已被選取的標籤</div>",
        afterSelect: function(value) {
            var get_val = $("#multiple_value3").val();
            var hidden_val = (get_val != "") ? get_val + "," : get_val;
			var val_text;
            $("#multiple_value3").val(hidden_val + "" + value);
			
			$('#bestsellers3').find('option').each(function(k,e) {
				if($(e).val() == value) {
					val_text = $(e).text();
				}
				//console.log(val_text);
			});
			$("#myTags2").tagit("createTag", val_text);
        },
        afterDeselect: function(value) {
            var get_val = $("#multiple_value3").val();
            var new_val = get_val.replace(value, "");
			var val_text;
            $("#multiple_value3").val(new_val);
			$('#bestsellers3').find('option').each(function(k,e) {
				if($(e).val() == value) {
					val_text = $(e).text();
				}
				//console.log(val_text);
			});
			$("#myTags2").tagit("removeTagByLabel", val_text);
        }
    });
  })
</script>
</body>
</html>