<?php 
	include("../includes/head.php"); 
?>
</head>

<body>
<?php 
	//include("../includes/left.php");
	include("../multi-menu/left.php"); 
?>
<h1>產品</h1>
<div id="nav">目前位置：<?php echo $nav_title; ?> > <a href="web_product_list.php">產品</a> > 列表</div>

<!--搜尋開始-->
<form name="formSearch" method="post" action="web_product_list.php">
  <div id="search">
	<?php $list_search = list_search_product(array("subject" => "名稱", /*"serialnumber" => "型號", "info" => "簡介", "content" => "商品介紹", "standard" => "詳細規格", "youtube" => "Youtube", "stock" => "庫存數量小於", "series" => "系列"*/), web_product); ?>
    <div id="new"><a href="web_product_edit.php">新增資料</a></div>
	
  </div>
</form>

<!--搜尋結束-->

<form name="form" method="post" action="web_product_update.php">
<?php
	$sql = "Select web_x_product_id, web_x_product.subject as web_x_product, web_xx_product.subject as web_xx_product, web_xx_product.web_xx_product_id as web_xx_product_id From web_x_product JOIN (web_xx_product) ON (web_x_product.web_xx_product_id=web_xx_product.web_xx_product_id) group by web_xx_product.web_xx_product_id order by web_xx_product.web_xx_product_id DESC";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysql_num_rows($rs); $i++) {
		$row = mysql_fetch_assoc($rs);
?>				
  <div style="display: block inline; margin-left: 6px; padding: 3px 8px; color: #FFF; background: #c1697e; border-radius: 5px; float: right; margin-top: 3px;"><a href="web_product_list.php?catalog2=<?php echo $row['web_xx_product_id']; ?>"><?php echo $row['web_xx_product']; ?></a></div>
<?php
	}
?>	
  </br>
  <table class="List_form">
    <tr>
      <th width="5%"><input type="checkbox" name="all" onClick="CheckAll(this, 'DeleteBox[]')" /></th>
	  <th width="10%">是否顯示</th>
	  <th width="5%">排序</th>
	  <th width="20%">類別</th>
	  <th>名稱／型號</th>
	  <th width="15%">產品圖<!--問與答--></th>
      <th width="5%"><a class="edit">改</a></th>
    </tr>
	<?php
		$web_xxxx_product_id = intval($_GET["_catalog"]);
		$web_xxx_product_id = intval($_GET["catalog"]);
		$web_xx_product_id = intval($_GET["catalog2"]);
		$web_x_product_id = intval($_GET["kind"]);
		$web_x_sale_id = intval($_GET["saleId"]);

		$_list_search = ($web_xxxx_product_id) ? " AND web_xxx_product.web_xxxx_product_id = ".$web_xxxx_product_id : null;
		$list_search2 = ($web_xxx_product_id) ? " AND web_xx_product.web_xxx_product_id = ".$web_xxx_product_id : null;
		$list_search3 = ($web_xx_product_id) ? " AND web_x_product.web_xx_product_id = ".$web_xx_product_id : null;
		$list_search4 = ($web_x_product_id) ? " AND web_product.web_x_product_id = ".$web_x_product_id : null;
		$list_search5 = ($web_x_sale_id) ? " AND web_product.web_x_sale_id = ".$web_x_sale_id : null;
		if($web_xxxx_product_id) {
			$list_search[link] = "&_catalog=".$web_xxxx_product_id;
		}
		if($web_xxx_product_id) {
			$list_search[link] = "&catalog=".$web_xxx_product_id;
		}
		if($web_xx_product_id) {
			$list_search[link] = "&catalog2=".$web_xx_product_id;
		}
		if($web_x_product_id) {
			$list_search[link] = "&kind=".$web_x_product_id;
		}
		if($web_x_sale_id) {
			$list_search[link] = "&saleId=".$web_x_sale_id;
		}
		//echo $list_search[sql_sub]."<br/>";
		$orderByItem = $_POST["orderByItem"] ? str_filter($_POST["orderByItem"]) : str_filter($_GET["orderByItem"]);	//
		$orderBy = $_POST["orderBy"] ? str_filter($_POST["orderBy"]) : str_filter($_GET["orderBy"]);	//
		$ControlPage = $_POST["ControlPage"] ? str_filter($_POST["ControlPage"]) : str_filter($_GET["ControlPage"]);	//
		$orderByItem = ($orderByItem) ? str_filter($orderByItem) : 'displayorder';	//
		$orderBy = ($orderBy) ? str_filter($orderBy) : 'ASC';	//
		$Init_ControlPage = ($ControlPage) ? str_filter($ControlPage) : $Init_ControlPage;	//

        $sql = "
        	Select 
        		SQL_CALC_FOUND_ROWS 
        		web_product.web_product_id, 
        		web_product.web_x_product_id,
        		web_product.web_x_sale_id, 
        		web_product.ifShow,
        		web_product.ifTop,
        		web_product.ifGeneral,
        		web_product.subject,
        		web_product.serialnumber, 
        		web_product.stock, 
        		web_product.Covers, 
        		web_product.displayorder,
				web_x_product.web_xx_product_id,
				web_x_product.subject as web_x_subject,
				web_xx_product.web_xx_product_id,
				web_xx_product.subject as web_xx_subject,
				web_xxx_product.web_xxx_product_id,
				web_xxx_product.subject as web_xxx_subject,
				web_xxxx_product.web_xxxx_product_id,
				web_xxxx_product.subject as web_xxxx_subject
        	From 
        		web_product
			Join
				(web_x_product,web_xx_product,web_xxx_product,web_xxxx_product) 
			On
				(
				web_x_product.web_x_product_id = web_product.web_x_product_id 
				And web_xx_product.web_xx_product_id = web_x_product.web_xx_product_id 
				And web_xxx_product.web_xxx_product_id = web_xx_product.web_xxx_product_id
				And web_xxxx_product.web_xxxx_product_id = web_xxx_product.web_xxxx_product_id
				)			
        	".$list_search[sql_sub].$_list_search.$list_search2.$list_search3.$list_search4.$list_search5." 
        	AND
				web_product.web_x_product_id != '8'
        ";

        $sql .= " order by web_xx_product.web_xx_product_id ASC, web_product.".$orderByItem." ".$orderBy." ";
		$list_paging = list_paging_sql($page, $Init_ControlPage, $sql);
        $sql .= " limit ".($page-1) * $Init_ControlPage.", ".$Init_ControlPage;
        //echo $sql;
        $rs = ConnectDB($DB, $sql);
		//$list_paging = list_paging($page, $Init_ControlPage);
		for ($i=0; $i<mysql_num_rows($rs); $i++) {
			$row = mysql_fetch_assoc($rs);
			foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
			
			$Product = getProduct($web_product_id);	//取得產品相關資訊

			$trclass = " class=\"strikethrough\"";
			if ($Product[ifStop]==0) $trclass = "";

			$web_x_product_list = "";

			$_Categories = _getCategories($web_xxx_product_id);
			$Categories = getCategories($web_x_product_id);
			
			if ($Categories[web_x_product]) $web_x_product_list = "<a href='web_product_list.php?catalog2=".$Categories[web_xx_product_id]."'>".$Categories[web_xx_product]."</a> > <a href='web_product_list.php?kind=".$Categories[web_x_product_id]."'>".$Categories[web_x_product]."</a>";

			if($web_x_sale_id) {
				$sql = "Select web_x_sale_id, ifDefault, subject, sdate, edate From web_x_sale WHERE web_x_sale_id = '".$web_x_sale_id."'";
				$rs2 = ConnectDB($DB, $sql);
				$saleSubject = mysql_result($rs2, 0, "subject");
				$saleSdate = mysql_result($rs2, 0, "sdate");
				$saleEdate = mysql_result($rs2, 0, "edate");
				$web_x_product_list = $web_x_product_list."</br><a href='web_product_list.php?saleId=".$web_x_sale_id."'><b style='color:red'>".$saleSubject."</b></a>";
			}	
			
			$pic = ShowPic($Covers, "../../uploadfiles/s/", "../../uploadfiles/no_image.jpg");
			
			$sql2 = "Select count(*) as counter From web_productqa Where web_product_id = '".$web_product_id."' ";
			$rs2 = ConnectDB($DB, $sql2);
			$counter = mysql_result($rs2, 0, "counter");

			$sql3 = "Select count(*) as counter From web_productNotice Where web_product_id = '".$web_product_id."' ";
			$rs3 = ConnectDB($DB, $sql3);
			$counter3 = mysql_result($rs3, 0, "counter");
			
			$sql4 = "Select count(*) as counter From web_productNotice Where web_product_id = '".$web_product_id."' and ifSend = '0' ";
			$rs4 = ConnectDB($DB, $sql4);
			$counter4 = mysql_result($rs4, 0, "counter");
    ?>
    <tr<?php echo $trclass; ?>>
      <td align="center"><input type="checkbox" class="chk" name="DeleteBox[]" value="<?php echo $web_product_id; ?>" /></td>
	  <td align="center"><?php echo ($ifShow==1) ? "是" : "否"; ?></td>
	  <td align="center"><input class="_displayOrder" type="text" size="6" data-block="<?php echo $row["displayorder"]; ?>" data-id="<?php echo $web_product_id; ?>" data-act="web_order" name="displayorder[<?php echo $row["displayorder"]; ?>]" value="<?php echo $row["displayorder"]; ?>" /></td>
	  <td><?php echo $web_x_product_list; ?></td>
	  <td><?php echo ($ifTop==1) ? "<font class=\"font_pink\">【置頂】</font>" : ""; ?><?php echo "【".$GeneralArray[$ifGeneral]."】".$subject."<br />".$serialnumber; ?>
	<?php
		if(0) {
	?>
	  <br />
      庫存：<a href="#stock_tip_<?php echo $web_product_id; ?>" class="inline"><?php echo $stock; ?></a>
      <div style="display: none;">
        <div id="stock_tip_<?php echo $web_product_id; ?>">
          <table class="None_form" style="width: 100%">
            <tr>
              <td>目前庫存：</td>
              <td><?php echo $stock; ?></td>
              <td rowspan="2" align="center"><input type="button" onClick="ChangeNum(this, <?php echo $web_product_id; ?>);" value="確定送出"></td>
            </tr>
            <tr>
              <td>增減數量：</td>
              <td><input type="text" id="new_stock_<?php echo $web_product_id; ?>" value="" size="20" maxlength="3" /></td>
            </tr>
            <tr>
              <td colspan="3">
                增加庫存請輸入大於0的數量，如：<span class="font_red">10</span><br />
                減少庫存請輸入負數，如：<span class="font_green">-5</span>
              </td>
            </tr>
          </table>
        </div>
      </div>
	<?php
		}
	?>	
      </td>
	  <td align="center">
	  <img src="<?php echo $pic; ?>" width="80" /><br/>
	<!--	
	  <a href="web_productQA_list.php?web_product_id=<?php echo $web_product_id; ?>"><img src="<?php echo $pic; ?>" width="80" /><br /><?php echo $counter; ?> 則問與答</a>
	-->
	  <!--<br />-->	
	  <!--
	  <a href="web_productNotice_list.php?web_product_id=<?php echo $web_product_id; ?>"><?php echo $counter3; ?> 則到貨通知 <br/> <?php echo $counter4; ?> 則未通知 </a>
	  -->	
	  </td>
      <td align="center"><a href="web_product_edit.php?web_product_id=<?php echo $web_product_id."&page=".$page.$list_search[link]; ?>" class="edit">改</a></td>
    </tr>
	<?php } ?>
  </table>
  <div id="delete"> 
  	<a class='ajax' href="./web_product_MulitEdit.php?web_product_id=33" title="批次修改" style="display: none">批次修改</a>
  	<select name="action" id="action">
      <option value="">請選擇</option>
      <option value="EditIfShow">上下架狀態</option>
      <!--<option value="EditInfo">批次修正商品資料</option>
      <option value="EditStock">批次修正商品庫存</option>
      <option value="EditSummary">批次修正商品簡介</option>-->
      <option value="Delete">刪除</option>
    </select> 
    <select name="ifShow" id="ifShow" style="display:none">
      <option value="">請選擇</option>
      <option value="1">上架</option>
      <option value="0">下架</option>
    </select>
    <input type="submit" id="send" name="submit" value="執行" onClick="return _CheckDel2();" /> 
    <!--<input type="submit" name="submit" value="刪除" onClick="return CheckDel();" />-->
  </div>
  <?php list_page("web_product_list.php", $page, $list_paging[pages], $list_paging[records], $list_search[hidden], $list_search[link]); ?>
  <!--<input type="hidden" name="action" value="Delete" />-->
</form>
<?php include("../includes/footer.php"); ?>
<script language="javascript">
<!--
$(function() {
	$("input[name='ControlPage']").on('keydown',  function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});

	$("select[name='action']").on('change', function() {
	  $('#send').show();	
      if($(this).val() == 'EditIfShow') {
        $('#ifShow').show();  
      } else {
        $('#ifShow').hide();
      }

    }).click();
	$("#mulitSend").click(function() {
		alert(111111);
	})

})
function _CheckDel2() {
    if (confirm("確定要執行？")) {
    	var _action = $("select[name='action']").find('option:selected').val();
        if(_action == 'EditInfo' || _action == 'EditStock' || _action == 'EditSummary') {
        	var id = new Array();
	        $('.chk:checked').each(function(k,v) {
	        	//console.log($(v).val());
	        	id.push($(v).val());
	        });
	        //alert(id.length);	
	        if(id.length) {
	        	var idStr = id.join();
	        	$('.ajax').attr('href', './web_product_MulitEdit.php?web_product_id='+idStr+'&action='+_action).trigger('click');
	        } else {
	        	alert('請勾選要修改之產品');
	        }
	        return false;
        } else {
        	return true;
        }	
    } else {
        return false;
    }
}
function ChangeNum(obj, id) {
	var num = 0;	
	if ($('#new_stock_' + id).length > 0) {
		num = $('#new_stock_' + id).val();
		if (num=="" || isNaN(num)) num = 0;
	}
	num = parseInt(num);
	if (num==0) {
		alert('增減數量不得留空或為0');
		return;
	}
	$(obj).attr('disabled', 'disabled');	//停用

	ajaxobj = new AJAXRequest;
	ajaxobj.method = "POST";
	ajaxobj.url = "web_product_update.php";
	ajaxobj.content = "action=ChangeStock&id=" + id + "&num=" + num;
	ajaxobj.callback = function (xmlobj) {
		//var response = xmlobj.responseText;
		//alert(response);
		window.location.reload();
	};
	ajaxobj.send();
}
//-->
</script>
</body>
</html>