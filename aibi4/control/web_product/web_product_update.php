<?php
	include("../includes/includes.php");
	
	$action = $_POST["action"] ? $_POST["action"] : $_GET["action"];
	$adminInfo = getAdmin($_SESSION[Member][ID]);
	$ip = $_SERVER["HTTP_VIA"] ? $_SERVER["HTTP_X_FORWARDED_FOR"] : $_SERVER["REMOTE_ADDR"];	//IP
	//操作履歷
	if ($action == "Edit") {
		$TableName = "web_product";
		$sql = "SHOW FULL FIELDS FROM `".$TableName."`";
		$rs = ConnectDB($DB, $sql);
		$tabeInfoAry = array();
		for ($i=0; $i<mysql_num_rows($rs); $i++) {
			$row = mysql_fetch_assoc($rs);
			$tabeInfoAry[$row[Field]] = $row[Comment];
		}

		$sql2 = "Select * From web_product Where web_product_id like '".$_POST[web_product_id]."' ";
		$rs2 = ConnectDB($DB, $sql2);	
		$inDbInfo = array();
		for ($i=0; $i<mysql_num_rows($rs2); $i++) {
			$row2 = mysql_fetch_assoc($rs2);
			$inDbInfo = $row2;
		}
		$_inDbInfo = array();
		$postAry = array();
		foreach($_POST as $key => $postVal) {
			//echo $key.": ".$tabeInfoAry[$key].": ".$postVal."</br>";
			$_inDbInfo[$tabeInfoAry[$key]] = $inDbInfo[$key];
			$postAry[$tabeInfoAry[$key]] = $postVal;
		}
		//exit;
		$result = array_diff_assoc($postAry,$_inDbInfo);
		//查看是否有更改
		if(count($result)) {
			$recLogAry = array();
			$_recLogAry = array();
			foreach($result as $key => $postVal) {
				//echo '原資料:'.$_inDbInfo[$key]."</br>";
				//$_recLogAry[$key] = $_inDbInfo[$key]." -> ".$postVal;
				$recLogAry[urlencode($key)] = urlencode($_inDbInfo[$key])." -> ".urlencode($postVal);
			}
			$recLogAry[urlencode(操作日期)] = date('Y-m-d H:i:s');
			$recLogAry[urlencode(操作者)] = $adminInfo[loginID]."[".urlencode($adminInfo[uname])."]";
			$recLogAry[IP] = $ip;
			$recLog = json_encode($recLogAry);

			if(count($recLogAry)) {
				//$sql3 = "Update web_x_order set log = '".$recLog."' Where web_x_order_id = '".$_POST[web_x_order_id]."'";
				$sql3 = "
					Insert into 
						web_productlog 
							(
								web_product_id, 
								log, 
								uname, 
								IP, 
								date
							) 
						values 
							(
								'".$inDbInfo[web_product_id]."', 
								'$recLog', 
								'".$adminInfo[uname]."', 
								'$ip', 
								'".date('Y-m-d H:i:s')."'
							) 
				";	
				$rs3 = ConnectDB($DB, $sql3);
			}
		}
	}
	//新增編輯
	if ($action=="Edit") {
		//$debug = true;
		$TableName = "web_product";
		$AccurateAction = "Get";
		require("../includes/accurate.php");
		foreach($_POST as $_key => $_value) $$_key = str_front($_POST[$_key]);
		//允許會員等級
		//$memberLevel = implode(",", $_POST["memberlevel_multiple"]);

		//相關產品
		$multiple_value = array_values(array_filter(explode(",", $_POST["multiple_value"])));
		$bestsellers = implode(",", $multiple_value);

		//加購產品
		$multiple_value2 = array_values(array_filter(explode(",", $_POST["multiple_value2"])));
		$bestsellers2 = implode(",", $multiple_value2);
		
		//標籤 
		$multiple_value3 = array_values(array_filter(explode(",", $_POST["multiple_value3"])));
		$_POST['tags'] = implode(",", array_unique($multiple_value3));


		$discountstime = str_filter($_POST["discountstime"]);	//折扣開始時間
		$discountetime = str_filter($_POST["discountetime"]);	//折扣結束時間
		$stime = str_filter($_POST["stime"]);	//上架時間
		$etime = str_filter($_POST["etime"]);	//下架時間
		$useEtime = str_filter($_POST["useEtime"]);	//下架時間
		$additionalstime = str_filter($_POST["additionalstime"]);	//加購開始時間
		$additionaletime = str_filter($_POST["additionaletime"]);	//加購結束時間
		$dimension = trim($dimension, ",");
		$tags = implode(",", $_POST["tags"]);	//標籤
		if ($tags!="") $tags = ",".$tags.",";
		
		if ($subject=="") RunAlert("請輸入名稱");
		if ($pincode) {
			$sql = "Select count(*) as counter From web_product Where pincode like '".$pincode."' ";
			if ($web_product_id>0) $sql .= " and web_product_id != '".$web_product_id."' ";
			$rs = ConnectDB($DB, $sql);
			if (mysql_result($rs, 0, "counter")>0) RunAlert("此料號已被使用，請重新輸入");
		}
		//if (!preg_match("/^(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9])(:([0-5]?[0-9]))?$/", $discountstime)) RunAlert("請輸入正確的折扣開始時間");
		//if (!preg_match("/^(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9])(:([0-5]?[0-9]))?$/", $discountetime)) RunAlert("請輸入正確的折扣結束時間");
		if (!preg_match("/^(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9])(:([0-5]?[0-9]))?$/", $additionalstime)) RunAlert("請輸入正確的加購開始時間");
		if (!preg_match("/^(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9])(:([0-5]?[0-9]))?$/", $additionalstime)) RunAlert("請輸入正確的加購結束時間");
		if (!preg_match("/^(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9])(:([0-5]?[0-9]))?$/", $stime)) RunAlert("請輸入正確的上架時間");
		if (!preg_match("/^(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9])(:([0-5]?[0-9]))?$/", $etime)) RunAlert("請輸入正確的下架時間");

		$discountsdate = $discountsdate." ".$discountstime;	//折扣開始時間
		$discountedate = $discountedate." ".$discountetime;	//折扣結束時間
		$sdate = $sdate." ".$stime;	//上架時間
		$edate = $edate." ".$etime;	//下架時間
		$useEdate = $useEdate." ".$useEtime;	//下架時間
		$_POST['useEdate'] = $useEdate;
		$additionalsdate = $additionalsdate." ".$additionalstime;	//加購開始時間
		$additionaledate = $additionaledate." ".$additionaletime;	//加購結束時間
		
		
		//$debug = true;
		$CoverAction = "Update";
		require("../includes/cover.php");
		$FileAction = "Update";
		require("../includes/files.php");
		
		$_POST["Covers"] = $Covers;
		$_POST["Files"] = $Files;
		$_POST["cdate"] = date('U-m-d H:i:s');
		$_POST['price_public'] = $price_cost;
		
		if ($web_product_id>0) {
			$ForbidData = array("hits", "cdate");	//不更新的欄位
			//$debug = "true";
			$AccurateAction = "Update";
			require("../includes/accurate.php");
		} else {
			$hits = 0;	//瀏覽人數
			$cdate = date("Y-m-d H:i:s");	//建立時間
			$_POST["cdate"] = $cdate;
			$AccurateAction = "Insert";
			require("../includes/accurate.php");
		}
	} 
	
	if ($action=="ChangeStock") {
		$web_product_id = floatval($_POST["id"]);
		$num = floatval($_POST["num"]);
		
		$sql = "Update web_product set stock = stock + '".$num."' Where web_product_id = '".$web_product_id."' ";
		$rs = ConnectDB($DB, $sql);

		if($num >= $Init_stockLimit) {
			$sql2 = "Delete From web_stockLimitLog Where web_product_id = '".$web_product_id."' ";
			$rs2 = ConnectDB($DB, $sql2);
		}
		
	}
	
	//刪除
	if ($action=="Delete") {
		$DeleteBox = $_POST["DeleteBox"];
		if (!is_array($DeleteBox)) die();
		
		foreach ($DeleteBox as $value) {
			//產品問與答
			$sql = "Delete From web_productqa Where web_product_id = '".$value."' ";
			$rs = ConnectDB($DB, $sql);
			
			//產品代表圖
			$sql = "Select Covers From web_product Where web_product_id = '".$value."' ";
			$rs = ConnectDB($DB, $sql);
			$CoverAction = "Del";
			require("../includes/cover.php");
			
			//內頁圖片
			$sql = "Select Files From web_product Where web_product_id = '".$value."' ";
			$rs = ConnectDB($DB, $sql);
			$FileAction = "Del";
			require("../includes/files.php");
			
			//產品
			$sql = "Delete From web_product Where web_product_id = '".$value."' ";
			$rs = ConnectDB($DB, $sql);
		}
		
		$sql = "Select count(*) as counter From web_productqa ";
		$rs = ConnectDB($DB, $sql);
		if (mysql_result($rs, 0, "counter")==0) {
			$sql = "Truncate table web_productqa";
			$rs = ConnectDB($DB, $sql);
		}

		$sql = "Select count(*) as counter From web_product ";
		$rs = ConnectDB($DB, $sql);
		if (mysql_result($rs, 0, "counter")==0) {
			$sql = "Truncate table web_product";
			$rs = ConnectDB($DB, $sql);
		}
	} else if($action=="EditIfShow") {
		$DeleteBox = $_POST["DeleteBox"];
		if (!is_array($DeleteBox)) die();
		
		$ifShow = $_POST["ifShow"];
		
		foreach ($DeleteBox as $value) {
			//訂單
			$sql = "Update web_product set ifShow = '$ifShow' Where web_product_id = '".$value."' ";
			//echo "</br>";
			$rs = ConnectDB($DB, $sql);
		}
	}
	
	RunJs("web_product_list.php?page=".intval($_POST["page"])."&field=".$_POST["field"]."&keyword=".urlencode($_POST["keyword"]));
?>