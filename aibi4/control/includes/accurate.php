<?php
	if ($TableName=="") die("未選擇資料表");
	$_POST['bestsellers'] = $bestsellers;
	$_POST['bestsellers2'] = $bestsellers2;
	//取資料
	if ($AccurateAction=="Get") {
		$j = 0;
		$sql = "SHOW FULL FIELDS FROM `".$TableName."`";
		$rs = ConnectDB($DB, $sql);
		while ($row=mysqli_fetch_row($rs)) {
			
			for ($i=0; $i<mysqli_num_fields($rs); $i++) {
				switch ($i) {
					case 0:
						$DataField[$j] = $row[$i];
						break;
					case 1:
						$DataType[$j] = $row[$i];
						$PartType = explode("(", $DataType[$j]);
						$DataPartType[$j] = $PartType[0];
						break;
					case 5:
						$DataDefault[$j] = $row[$i];
						break;
					case 8:
						if ($j==0 && $row[$i]=="") $row[$i] = "流水號";
						$DataComment[$j] = $row[$i];
						break;
					default:						
				}
			}
			$j++;
		}
		

		$table_list = "
		<table class=\"List_form\">
		  <th width=\"15%\">中文名稱</th>
		  <th width=\"20%\">欄位名</th>
		  <th>接收值</th>
		  <th width=\"15%\">型態</th>
		  <th width=\"10%\">預設值</th>
		";
		if (is_array($ForbidData)) $DataField = array_values(array_diff($DataField, $ForbidData));
		for ($i=0; $i<sizeof($DataField); $i++) {
			switch ($DataPartType[$i]) {
				case "int":
				case "tinyint":
					$$DataField[$i] = intval($_POST[$DataField[$i]]);
					break;
				case "float":
					$$DataField[$i] = floatval($_POST[$DataField[$i]]);
					break;
				default:
					$$DataField[$i] = str_filter($_POST[$DataField[$i]]);
			}
			
			$table_list .= "
			<tr>
			  <td>".$DataComment[$i]."</td>
			  <td>".$DataField[$i]."</td>
			  <td>".$$DataField[$i]."</td>
			  <td align=\"center\">".$DataType[$i]."</td>
			  <td align=\"center\">".$DataDefault[$i]."</td>
			</tr>";
		}
		$table_list .= "</table>";
		
		$UpdateData = array();
		$InsertField = array();
		$InsertValue = array();	
		
		if ($debug) {
			for ($i=1; $i<sizeof($DataField); $i++) $UpdateData[] = "`".$DataField[$i]."` = '\$".$DataField[$i]."'";
			$sql = "Update `".$TableName."` set ".implode(", ", $UpdateData)." Where ".$DataField[0]." = '\$".$DataField[0]."' ";
			$debug_update = "\$sql = \"Update `".$TableName."` set ".implode(", ", $UpdateData)." Where ".$DataField[0]." = '\$".$DataField[0]."'\"; ";
		
			for ($i=1; $i<sizeof($DataField); $i++) {
				$InsertField[] = "`".$DataField[$i]."`";
				$InsertValue[] = "'\$".$DataField[$i]."'";
			}
			$debug_insert = "\$sql = \"Insert into `".$TableName."` (".implode(", ", $InsertField).") values (".implode(", ", $InsertValue).")\"; ";
			
			unset($UpdateData);
			unset($InsertField);
			unset($InsertValue);
			
			if ($$DataField[0]>0) {
				for ($i=1; $i<sizeof($DataField); $i++) $UpdateData[] = "`".$DataField[$i]."` = '".$$DataField[$i]."'";
				
				$debug_real = "Update `".$TableName."` set ".implode(", ", $UpdateData)." Where ".$DataField[0]." = '".$$DataField[0]."' ";
			} else {
				for ($i=1; $i<sizeof($DataField); $i++) {
					$InsertField[] = "`".$DataField[$i]."`";
					$InsertValue[] = "'".$$DataField[$i]."'";
				}
	
				$debug_real = "Insert into `".$TableName."` (".implode(", ", $InsertField).") values (".implode(", ", $InsertValue).") ";
			}
			
			echo "
			<title>資料表".$TableName."除錯</title>
			<link href=\"../css/layout.css\" rel=\"stylesheet\" type=\"text/css\" />
			<body style=\"background: #FFF;\">
			  <div id=\"debug\">
				".$table_list."
				<h1>新增</h1>
				".$debug_insert."
				<h1>編輯</h1>
				".$debug_update."
				<h1>執行</h1>
				".$debug_real."
			  </div>
			</body>";
			die();
		}
	}

	//新增
	if ($AccurateAction=="Insert") {
		$InsertField = array();
		$InsertValue = array();	
		for ($i=1; $i<sizeof($DataField); $i++) {
			$InsertField[] = "`".$DataField[$i]."`";
			//$InsertValue[] = "'".$$DataField[$i]."'";
			$InsertValue[] = "'".$_POST[$DataField[$i]]."'";
		}

		$sql = "Insert into `".$TableName."` (".implode(", ", $InsertField).") values (".implode(", ", $InsertValue).") ";
		if ($debug) die($sql);
		//$rs = ConnectDB($DB, $sql);
		
		$insterId = ConnectDBInsterId($DB, $sql);
	}

	//編輯
	if ($AccurateAction=="Update") {
		
		//$ForbidData = array("hits");	//不更新的欄位
		$UpdateData = array();
		if (is_array($ForbidData)) $DataField = array_values(array_diff($DataField, $ForbidData));
		for ($i=1; $i<sizeof($DataField); $i++) {
			//echo "`".$DataField[$i]."` = '".$_POST[$DataField[$i]]."' \n";
			//$UpdateData[] = "`".$DataField[$i]."` = '".$$DataField[$i]."'";
			$UpdateData[] = "`".$DataField[$i]."` = '".$_POST[$DataField[$i]]."'";
		}
	
		$sql = "Update `".$TableName."` set ".implode(", ", $UpdateData)." Where ".$DataField[0]." = '".$$DataField[0]."' ";
		if ($debug) die($sql);
		$rs = ConnectDB($DB, $sql);
		//exit;
	}
?>