<?php
	/*
	2014-02-13	1.移除Add
				2.檢查真正的副檔名
	2014-02-21	1.編排格式
				2.加入檔案說明
	2014-02-25	1.加入前置詞
	2014-02-27	1.整理檔案對應的說明
	2014-05-12	1.png和gif縮圖，gif縮小後不會動，原圖不要縮即可$CoverResize = false;
	2015-02-26	1.改版
	*/

	$CoverMaxNum = intval($_POST["CoverMaxNum"]) ? intval($_POST["CoverMaxNum"]) : intval($CoverMaxNum);
	if ($_POST["CoverTableName"]) $CoverTableName = str_filter($_POST["CoverTableName"]);
	if (intval($CoverMaxNum)==0) $CoverMaxNum = 1; //上傳最大數量
	$CoverResize = flase; //true:原圖要縮圖 false:原圖不縮圖
	$CoverShowMeno = false;	//檔案說明
	
	$CoverName = "";
	$CoverPreName = "";	//前置詞
	$CoverUploadDir = "../../uploadfiles/l/";	//上傳檔案的路徑
	$CoverUploadWidth = 1200;
	$CoverUploadResizeDir = array("../../uploadfiles/m/", "../../uploadfiles/s/");	//上傳檔案的路徑(縮圖)
	$CoverUploadResizeWidth = array(450, 80);	//縮圖的尺寸，輸入數字
	$CoverLimit = "jpg, png, ";					//上傳檔案格式
	//$CoverLimit = "gif, jpg, png, ";
	//$CoverLimit = "doc, docx, gif, jpg, jpeg, pdf, png, rar, txt, xls, xlsx, zip, ";
	//輪播-------------------------------------------------------------
	if ($CoverTableName=="web_banner_1") {	//首頁大輪播
		$CoverUploadWidth = 1200;
		$CoverUploadResizeWidth = array(360, 180,);	//縮圖的尺寸，輸入數字
	}
	
	if ($CoverTableName=="web_banner_2") {	//首頁中間三格
		$CoverUploadWidth = 245;
		$CoverUploadResizeWidth = array(245, 180);	//縮圖的尺寸，輸入數字
	}
	
	if ($CoverTableName=="web_banner_3") {	//首頁下方兩格
		$CoverUploadWidth = 440;
		$CoverUploadResizeWidth = array(440, 80);	//縮圖的尺寸，輸入數字
	}
	
	if ($CoverTableName=="web_banner_4") {	//內頁側邊一格
		$CoverUploadWidth = 1220;
		$CoverUploadResizeWidth = array(586, 100);	//縮圖的尺寸，輸入數字
	}

	if ($CoverTableName=="web_banner_5") {	//首頁下方四格
		$CoverUploadWidth = 205;
		$CoverUploadResizeWidth = array(74, 74);	//縮圖的尺寸，輸入數字
	}

	if ($CoverTableName=="web_xxxx_product") {	//產品分類-導覽列圖片
		$CoverUploadWidth = 208;
		$CoverUploadResizeWidth = array(80, 80);	//縮圖的尺寸，輸入數字
	}

	if ($CoverTableName=="web_xxx_product") {	//產品分類-導覽列圖片
		$CoverUploadWidth = 251;
		$CoverUploadResizeWidth = array(80, 80);	//縮圖的尺寸，輸入數字
	}
	
	if ($CoverTableName=="web_product") {	//產品-產品代表圖
		$CoverUploadWidth = 550;
		$CoverUploadResizeWidth = array(274, 87);	//縮圖的尺寸，輸入數字
	}

	if ($CoverTableName=="web_gift") {	//贈品-代表圖
		$CoverUploadWidth = 468;
		$CoverUploadResizeWidth = array(274, 87);	//縮圖的尺寸，輸入數字
	}

	if ($CoverTableName=="web_news") {	//產品-產品代表圖
		$CoverUploadWidth = 242;
		$CoverUploadResizeWidth = array(242, 80);	//縮圖的尺寸，輸入數字
	}

	if ($CoverTableName=="web_x_sale") {	//產品-產品代表圖
		$CoverUploadWidth = 1000;
		$CoverUploadResizeWidth = array(274, 100);	//縮圖的尺寸，輸入數字
	}

	$CoverLimit = strtolower($CoverLimit);

//Edit編輯-----------------------------------------------------------------------------------------------------------------------------
	if ($CoverAction=="Edit") {
		$CoverShowList = "<table class=\"File_form\">\n";
		if ($Covers!="") {	//如果有檔案
			$CoverSay_array = explode("#####", $CoverSay);	//檔案說明
			$Covers_Array = explode("/", $Covers);
			for ($i=0; $i<$CoverMaxNum; $i++) {
				$CoverSerialNum = ($CoverMaxNum>1) ? ($i+1)."." : "‧";
				if ($CoverShowMeno) $CoverSayList = "檔案說明：<input name=\"CoverSay_".$i."\" type=\"text\" maxlength=\"90\" value=\"".$CoverSay_array[$i]."\" /><br />";	//檔案說明
				if ($Covers_Array[$i]!="") {
					$eCover = explode(".", $Covers_Array[$i]);
					foreach ($eCover as $value) $Cover_format = $value;
					$Cover_format = strtolower($Cover_format);
					if ($Cover_format=="jpg" || $Cover_format=="jpeg" || $Cover_format=="gif" || $Cover_format=="png") {	//圖
						if (file_exists($CoverUploadResizeDir[sizeof($CoverUploadResizeDir)-1].$Covers_Array[$i]))
							$CoverShowWay = "<a href=\"".$CoverUploadDir.$Covers_Array[$i]."\" data-lightbox=\"roadtrip\" title=\"".$Covers_Array[$i]."\"><img src=\"".$CoverUploadResizeDir[sizeof($CoverUploadResizeDir)-1].$Covers_Array[$i]."\"/></a>";	//有小圖秀小圖
						else {
							if (file_exists($CoverUploadDir.$Covers_Array[$i]))
								$CoverShowWay = "<a href=\"".$CoverUploadDir.$Covers_Array[$i]."\" data-lightbox=\"roadtrip\" data-title=\"".$Covers_Array[$i]."\"><img src=\"".$CoverUploadDir.$Covers_Array[$i]."\"/></a>";	//原圖
							else
								$CoverShowWay = "<img src=\"../../uploadfiles/no_image.jpg\"/>";	//沒圖
						}
					} else {
						$CoverShowWay = "<a href=\"".$CoverUploadDir.$Covers_Array[$i]."\" target=\"_blank\" title=\"Download\"><img src=\"../images/download.gif\" alt=\"Download\" /></a>";
					}
					
					$CoverShowList .= "
					  <tr>
						<td>".$CoverSerialNum."</td>
						<td>
						  <input type=\"checkbox\" name=\"DeleteCovers_".$i."\" value=\"".$Covers_Array[$i]."\" />刪除檔案<br />
						  ".$CoverSayList."
						  重新上傳：<input type=\"file\" name=\"Covers[]\" />
						  <input type=\"hidden\" name=\"OldCover_".$i."\" value=\"".$Covers_Array[$i]."\" />
						</td>
						<th>".$CoverShowWay."</th>
					  </tr>";
				} else {
					$CoverShowList .= "<tr><td>".$CoverSerialNum."</td><td>";
					if ($CoverShowMeno) $CoverShowList .= "檔案說明：<input name=\"CoverSay_".$i."\" type=\"text\" maxlength=\"90\" /><br />";	//檔案說明
					$CoverShowList .= "上傳檔案：<input type=\"file\" name=\"Covers[]\" /></td></tr>";
				}
			}
		} else {
			for ($i=0; $i<$CoverMaxNum; $i++) {
				$CoverSerialNum = ($CoverMaxNum>1) ? ($i+1)."." : "‧";
				$CoverShowList .= "<tr><td>".$CoverSerialNum."</td><td>";
				if ($CoverShowMeno) $CoverShowList .= "檔案說明：<input name=\"CoverSay_".$i."\" type=\"text\" maxlength=\"90\" /><br />";	//檔案說明
				$CoverShowList .= "上傳檔案：<input type=\"file\" name=\"Covers[]\" /></td></tr>";
			}
		}
		$CoverShowList .= "</table>\n";
		if ($CoverLimit) $CoverShowList .= "上傳檔案格式：<span class=\"font_pink\">".trim(strtoupper($CoverLimit), ", ")."</span><br />\n";
		$CoverShowList .= "<input type=\"hidden\" name=\"CoverTableName\" value=\"".$CoverTableName."\" />\n";
		$CoverShowList .= "<input type=\"hidden\" name=\"CoverMaxNum\" value=\"".$CoverMaxNum."\" />\n";
		echo $CoverShowList;
	}
	
	
//Update編輯---------------------------------------------------------------------------------------------------------------------------
	if ($CoverAction=="Update") {
		
		$NewUploadCover_Array = array();	//新上傳
		$CoverSay_array = array();	//檔案說明
		for ($i=0; $i<$CoverMaxNum; $i++) {
			$NewUploadCover_Array[$i] = $_POST["OldCover_".$i];
			$CoverSay_array[$i] = $_POST["CoverSay_".$i];
			$CoverSay_array[$i].": ".$NewUploadCover_Array[$i]."<br />";
		}
		
		
		for ($i=0; $i<$CoverMaxNum; $i++) {
			if ($_POST["DeleteCovers_".$i]!="")	{ //如果刪除檔案被勾選
				$NewUploadCover_Array[$i] = "";
				$CoverSay_array[$i] = "";
				if (file_exists($CoverUploadDir.$_POST["DeleteCovers_".$i])) {	//如果檔案存在
					unlink($CoverUploadDir.$_POST["DeleteCovers_".$i]);	//移除大圖
				}
				foreach ($CoverUploadResizeDir as $value) {	//移除縮圖
					if (file_exists($value.$_POST["DeleteCovers_".$i])) unlink($value.$_POST["DeleteCovers_".$i]);
				}
			}

			if ($_FILES["Covers"]["name"]!="") {	//如果有上傳檔案
				$CoverRename = array();
				$CoverSubName = explode(".", $_FILES["Covers"]["name"][$i]);	//分割檔名
				foreach ($CoverSubName as $value) $CoverRename[1] = $value;
				$CoverRename[0] = str_replace(".".$CoverRename[1], "", $_FILES["Covers"]["name"][$i]);
				if ($CoverRename[0]=="" || $CoverRename[1]=="") unset($CoverRename);
				//$CoverRename = explode(".", $_FILES["Covers"]["name"][$i], 2);	//分割檔名

				if (sizeof($CoverRename)==2) {	//如果有副檔名
					//檢查檔案格式
					$CoverRename[1] = strtolower($CoverRename[1]);
					if (strpos($CoverLimit, $CoverRename[1])===false) {
						if (file_exists($CoverUploadDir.$_FILES["Covers"]["name"][$i])) unlink($CoverUploadDir.$_FILES["Covers"]["name"][$i]);
						RunAlert("上傳檔案格式應為：".trim($CoverLimit, ", "));
					}
					
					$NewCover = $CoverPreName.date("YmdHis")."_".getPwd(5).".".$CoverRename[1];	//新檔名:年月日_亂數
					$UploadCover = $CoverUploadDir.$NewCover;					
					if (move_uploaded_file($_FILES["Covers"]["tmp_name"][$i], $UploadCover)) {
						$NewUploadCover_Array[$i] = $NewCover;
						$CoverSay_array[$i] = $_POST["CoverSay_".$i];
						if (strtolower($CoverRename[1])=="jpg" || strtolower($CoverRename[1])=="jpeg" || strtolower($CoverRename[1])=="gif" || strtolower($CoverRename[1])=="png") {						
							if ($CoverResize) {	//原圖要縮圖
								$picSize = getimagesize($UploadCover);	//寬:$picSize[0] 高:$picSize[1]
								$CoverUploadHeight = intval($CoverUploadWidth * $picSize[1] / $picSize[0]);	//修正高度
								ImageResize($UploadCover, $UploadCover, $CoverUploadWidth, $CoverUploadHeight);	//縮圖
							}
	
							$j = 0;
							foreach ($CoverUploadResizeDir as $value) {
								$picSize = getimagesize($UploadCover);
								$CoverUploadHeight = intval($CoverUploadResizeWidth[$j] * $picSize[1] / $picSize[0]);
								ImageResize($UploadCover, $value.$NewCover, $CoverUploadResizeWidth[$j], $CoverUploadHeight);	//縮圖
								$j++;
							}
						}
						
						//如果有上傳新檔則刪除舊檔
						if ($_POST["OldCover_".$i]!="") {	//若原本有檔案						
							$OldCover = $CoverUploadDir.$_POST["OldCover_".$i];
							if ($UploadCover!=$OldCover) {	//新檔名與舊檔名不同則刪除舊檔							
								if (file_exists($OldCover)) unlink($OldCover);
								foreach ($CoverUploadResizeDir as $value) {	//移除縮圖
									if (file_exists($value.$_POST["OldCover_".$i])) unlink($value.$_POST["OldCover_".$i]);
								}
							}
						}
					} else {						
						$NewUploadCover_Array[$i] = $_POST["OldCover_".$i];
						$CoverSay_array[$i] = $_POST["CoverSay_".$i];
						if ($_POST["DeleteCovers_".$i]!="") $NewUploadCover_Array[$i] = "";
						$CoverShowList .= "檔案上傳失敗，錯誤代碼：".$_FILES["Covers"]["error"];
					}
				}
			}
		}
		$Covers = "";
		for ($i=0; $i<$CoverMaxNum; $i++) {
			if ($NewUploadCover_Array[$i]!="") {
				//echo $CoverSay_array[$i].": ".$NewUploadCover_Array[$i]."<br />";
				$Covers .= $NewUploadCover_Array[$i]."/";
				$CoverSay .= $CoverSay_array[$i]."#####";
			}
		}
		$CoverSay = trim($CoverSay, "#####");
		//echo $Covers;
		//die();
	}

	
//SWFUpload大量上傳---------------------------------------------------------------------------------------------------------------------------
	if ($CoverAction=="SWFUpload") {
		$NewUploadCover_Array = array();	//新上傳
		$CoverSay_array = array();	//檔案說明
	    foreach ($_FILES as $Field => $v){			
			if ($_FILES[$Field]["size"]){				
				if ($_FILES[$Field]["error"] > 0){
					echo "Return Code: " . $_FILES[$Field]["error"];
					exit();
				} else {
					$CoverRename = array();
					$CoverSubName = explode(".", $_FILES[$Field]["name"]);	//分割檔名
					foreach ($CoverSubName as $value) $CoverRename[1] = $value;
					$CoverRename[0] = str_replace(".".$CoverRename[1], "", $_FILES[$Field]["name"]);
					$CoverRename[1] = strtolower($CoverRename[1]);
					$NewCover = $CoverPreName.date("YmdHis")."_".getPwd(5).".".$CoverRename[1];	//新檔名:年月日_亂數
					$UploadCover = $CoverUploadDir.$NewCover;
					if (move_uploaded_file($_FILES[$Field]["tmp_name"], $UploadCover)) {
						$NewUploadCover_Array[$i] = $NewCover;
						$CoverSay_array[$i] = $CoverRename[0];
						if (strtolower($CoverRename[1])=="jpg" || strtolower($CoverRename[1])=="jpeg" || strtolower($CoverRename[1])=="gif" || strtolower($CoverRename[1])=="png") {						
							if ($CoverResize) {	//原圖要縮圖
								$picSize = getimagesize($UploadCover);	//寬:$picSize[0] 高:$picSize[1]
								$CoverUploadHeight = intval($CoverUploadWidth * $picSize[1] / $picSize[0]);	//修正高度
								ImageResize($UploadCover, $UploadCover, $CoverUploadWidth, $CoverUploadHeight);	//縮圖
							}
	
							$j = 0;
							foreach ($CoverUploadResizeDir as $value) {
								$picSize = getimagesize($UploadCover);
								$CoverUploadHeight = intval($CoverUploadResizeWidth[$j] * $picSize[1] / $picSize[0]);
								ImageResize($UploadCover, $value.$NewCover, $CoverUploadResizeWidth[$j], $CoverUploadHeight);	//縮圖
								$j++;
							}
						}
					}
				}
			}
		}
		$Covers = implode("/", $NewUploadCover_Array);
		$CoverSay = implode("#####", $CoverSay_array);
	}

	
//Update刪除---------------------------------------------------------------------------------------------------------------------------
	if ($CoverAction=="Del") {
		for ($i=0; $i<mysql_num_rows($rs); $i++) {
			$row = mysql_fetch_assoc($rs);
			$DeleteCovers = explode("/", $row["Covers"]);
			
			//刪除文件時一併刪除檔案
			if (is_array($DeleteCovers)) {
				for ($j=0; $j<sizeof($DeleteCovers); $j++) {
					if ($DeleteCovers[$j]!="") {
						if (file_exists($CoverUploadDir.$DeleteCovers[$j])) unlink($CoverUploadDir.$DeleteCovers[$j]);
						foreach ($CoverUploadResizeDir as $CoverValue) {	//移除縮圖
							if (file_exists($CoverValue.$DeleteCovers[$j])) unlink($CoverValue.$DeleteCovers[$j]);
						}
					}
				}
			}
		}
	}
?>