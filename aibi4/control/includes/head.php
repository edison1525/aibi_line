<?php include("../includes/includes.php"); ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $Init_WebTitle; ?>後台管理系統</title>
<link href="../css/layout.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script type="text/javascript" src="../../../js/jquery-2.1.0.min.js"></script>
<script type="text/javascript" src="../../../js/jquery-ui-1.9.2.min.js"></script>
<!--ajax-->
<script type="text/javascript" src="../js/ajax.js"></script>
<!--ckeditor-->
<?php
if($uploadfilesSize > '93') {
?>	
<script type="text/javascript" src="../../ckeditor/ckeditor.js"></script>
<?php
} else {
?>
<script type="text/javascript" src="../../ckeditor/ckeditor.js"></script>
<?php
}
?>
<!--datepick-->
<link rel="stylesheet" type="text/css" href="../js/datepick/humanity.datepick.css"> 
<script type="text/javascript" src="../js/datepick/jquery.plugin.min.js"></script> 
<script type="text/javascript" src="../js/datepick/jquery.datepick.js"></script>
<script type="text/javascript" src="../js/datepick/jquery.datepick-zh-TW.js"></script>
<!--twzipcode-->
<script type="text/javascript" src="../js/twzipcode/jquery.twzipcode-1.6.7.js"></script>
<!--lightbox-->
<link rel="stylesheet" type="text/css" href="../js/lightbox/lightbox.css"> 
<script type="text/javascript" src="../js/lightbox/lightbox.min.js"></script> 
<!--colorbox-->
<link rel="stylesheet" type="text/css" href="../js/colorbox/colorbox.css"> 
<script type="text/javascript" src="../js/colorbox/jquery.colorbox-min.js"></script> 
<!--multiselect-->
<link rel="stylesheet" type="text/css" href="../js/multiselect/multi-select.css"> 
<script type="text/javascript" src="../js/multiselect/jquery.multi-select.js"></script>
<!--minicolors-->
<link rel="stylesheet" href="../js/minicolors/jquery.minicolors.css">
<script type="text/javascript" src="../js/minicolors/jquery.minicolors.js"></script>
<!--animatenumber-->
<script type="text/javascript" src="../js/animatenumber/jquery.animateNumber.min.js"></script>

<script src="../../../js/aehlke-tag-it-6ccd2de/tag-it.js" type="text/javascript" charset="utf-8"></script>
<link href="../../../js/aehlke-tag-it-6ccd2de/jquery.tagit.css" rel="stylesheet" type="text/css">
<link href="../../../js/aehlke-tag-it-6ccd2de/tagit.ui-zendesk.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css" href="../../../js/multiple-select-master/multiple-select.css"/>
<script src="../../../js/multiple-select-master/jquery.multiple.select.js"></script>

<script src="../../../js/jquery.maxlength.js"></script>

<!--colorPicker-->
<script src="../../../js/colorpicker.js"></script>

<script src="../../../js/jquery.validate.js" type="text/javascript"></script>

<!--angular js-->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<link href='../js/fullcalendar-4.1.0/packages/core/main.css' rel='stylesheet' />
<link href='../js/fullcalendar-4.1.0/packages/daygrid/main.css' rel='stylesheet' />
<script src='../js/fullcalendar-4.1.0/packages/core/main.js'></script>
<script src='../js/fullcalendar-4.1.0/packages/interaction/main.js'></script>
<script src='../js/fullcalendar-4.1.0/packages/daygrid/main.js'></script>
<script src='../js/fullcalendar-4.1.0/packages/core/locales-all.js'></script>

<script type="text/javascript">
<!--
$(function() {


    $('#cdate').datepick();
    $('#sdate').datepick();
    $('#edate').datepick();
	$('#useEdate').datepick();
	$('#shipdate').datepick();
	$('#ecdate').datepick({
		minDate: -0
	});
	$('#discountsdate').datepick();
	$('#discountedate').datepick();
	$('#startdate').datepick();
	$('#enddate').datepick();
	$('#additionalsdate').datepick();
	$('#additionaledate').datepick();	
    $('#birthday').datepick({
        maxDate: 0
    });
    $('#search_sdate').datepick({
        maxDate: 7
    });
    $('#search_edate').datepick({
        maxDate: 7
    });
    $('#search_paySdate').datepick({
        maxDate: 0
    });
    $('#search_payEdate').datepick({
        maxDate: 0
    });
    $('#search_Sdate').datepick({
        maxDate: 0
    });
    $('#search_Edate').datepick({
        maxDate: 0
    });
    /*
    $('.saleList').datepick( {
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'yyyy-mm',
        onClose: function(dateText, inst) { 
            $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
        }
    });
    */
    $('.memberStatistics').click(function(e) {
    	e.preventDefault();
    	var search_sdate = $('#search_sdate').val();
    	var search_edate = $('#search_edate').val();
    	var href_old = $(this).attr('href');
    	var href_new = href_old + '&search_sdate=' + search_sdate + '&search_edate=' + search_edate;
    	$(this).attr({'href' : href_new})
    	var type = $(this).attr('data-type');
    	$("input[name='type']").val(type);
    	//console.log($(this).attr('href'));
    	$( "form:first" ).submit();
    })

    //$("input[type='text']:first", document.forms[0]).focus();
	//$("input:first", document.forms[0]).focus();
	/*if ($("form[name^='formSearch']").length > 0) {
		$("input:first", document.forms[0]).focus();
	}*/

	$(window).load(function() {
	    $(window).bind('scroll resize', function() {
	        var $this = $(this);
	        var $this_Top = $this.scrollTop();

	        if ($this_Top < 35) {
				$('#gotop').hide();
				$('#top_left').stop().animate({
	                top: "-65px"
	            });
	        } else {
				//$('#gotop').show();
	            $('#top_left').stop().animate({
	                top: "0px"
	            });
			}
	    }).scroll();
	});
	
	$(".colorbox").colorbox	({rel:'group4', slideshow:false});
	$(".iframe").colorbox	({iframe:true, width:"80%", height:"80%"});
	$(".inline").colorbox	({inline:true, width:"50%", height:"80%", overlayClose:false});
	$(".ajax").colorbox({iframe:true, width:"50%", height:"80%", overlayClose:false});
	$(".youtube").colorbox ({iframe:true, innerWidth:853, innerHeight:480});
	$(".tagEdit").colorbox ({inline:true, width:"50%", height:"60%", overlayClose:false})

	$('#gotop').click(function(){
		$('html,body').animate({scrollTop: '0px'}, 800);
	});

	$('.minicolors').minicolors();
	
	$(".star").html('※');


	$('._displayOrder').blur(function(e) {
		var _id = $(this).attr('data-id');
		var _old_value = $(this).attr('data-block');
		var _act = $(this).attr('data-act');
		var _new_value = $(this).val();
		var _displayorder = parseInt($(this).val());
		var _this = $(this);
		if(!_act && !_id && !_displayorder || (_old_value == _displayorder)){
			return;	
		}
		$.ajax({
			method: "POST",
			url: "../includes/displayOrder.php",
			data: { web_id: _id, displayorder: _displayorder, act: _act }
		}).done(function( msg ) {
			_this.attr('data-block', _new_value);
			alert(msg);
		});
		return false;
	});


	$('.colorCode').colorPicker(); // that's it

});

//全選
function CheckAll(obj, cName) {
    var checkboxs = document.getElementsByName(cName);
    for (var i = 0; i < checkboxs.length; i++) checkboxs[i].checked = obj.checked;
}

function CheckDel() {
    if (confirm("確定要刪除資料？"))
        return true;
    else
        return false;
}


//跳頁
function MM_jumpMenu(targ, selObj, restore) {
    eval(targ + ".location='" + selObj.options[selObj.selectedIndex].value + "'");
    if (restore) selObj.selectedIndex = 0;
}
//-->
</script>
<link rel="manifest" href="../../../manifest.json">
<!--
<script src="https://www.gstatic.com/firebasejs/4.8.1/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.8.1/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.8.1/firebase-messaging.js"></script>
<script>
	var messaging = null;
		
	var notification = null;
	function ShowNotification(title, body) {
		notification = new Notification(title, {
			//icon: '/icon/ms-icon-310x310.png',
			body: body,
			onclick: function (event) {
				//event.preventDefault(); // prevent the browser from focusing the Notification's tab
				//window.open('https://airconcept.aibitechcology.com/admin/web_chat/web_chat_list.php', '_blank');
				parent.focus();
				window.focus(); //just in case, older browsers
				this.close();
			}
		})
	}
	
	var config = {
		apiKey: "AIzaSyBm38DF2hS12huWXLbp2RmRVDOyEuzrr7s",
		authDomain: "line-bot-1565254439912.firebaseapp.com",
		databaseURL: "https://line-bot-1565254439912.firebaseio.com",
		projectId: "line-bot-1565254439912",
		storageBucket: "line-bot-1565254439912.appspot.com",
		messagingSenderId: "444561611261",
		appId: "1:444561611261:web:57fdbcb7128fa3f58f6194",
		measurementId: "G-EQSNVJT8NQ"

	};
	firebase.initializeApp(config);
	
	messaging = firebase.messaging();
	
	//收到訊息後的處理
	messaging.onMessage(function (payload) {
		
		//如果可以顯示通知就做顯示通知
		if (Notification.permission === 'granted') {
			ShowNotification(payload.notification.title, payload.notification.body);
			//三秒後自動關閉
			setTimeout(notification.close.bind(notification), 3000);
		}
	});	
</script>
-->