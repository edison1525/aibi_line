<?php
session_start();
header("Cache-control:private");
header("Content-Type: text/html; charset=utf-8"); 
ini_set("date.timezone", "Asia/Taipei");
//echo date("Y-m-d H:i:s");;
error_reporting(0);
/*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
*/
include("connect.php");
include_once('helper.php');
$encrypt_key = $_SERVER['HTTP_HOST'];   
/*
//echo date('Y-m-d H:i:s',strtotime('2017/05/12'));
if(time() <= strtotime('2017/05/12')) {
	$ip = ($_SERVER["HTTP_VIA"]) ? $_SERVER["HTTP_X_FORWARDED_FOR"] : $_SERVER["REMOTE_ADDR"];
	$passIp = array(
		'118.163.128.78', 
		'114.46.215.154', 
		'175.183.34.102',
		'49.213.134.238',
		'111.242.107.208',
		'116.241.213.94',
		'111.82.170.3',
		'1.160.145.234',
	);
	//if (strpos($ip, "118.99.141.")!==false || strpos($ip, "192.168.")!==false) {
	if(in_array($ip, $passIp)) {
	} else {
		header('Location: http://www.motherk.com.tw/maintain.html');
		die();
	}
}
*/
//性別
$sexArray = array(
	'小姐',
	'先生',
	'無性別',
);
//產品----------------------------------------------------------------------------------------------------------------
//產品-類型
$GeneralArray = array(
	1 => "一般",
	9 => "加購"
);

//產品-標註
$TipArray = array(
	0 => "無",
	1 => "HOT",
	2 => "SALE"
);

//會員----------------------------------------------------------------------------------------------------------------
//如何得知我們
$howknow_array = array("Facebook", "網路搜尋", "部落客推薦", "親友推薦", "婦幼商店");

//紅利----------------------------------------------------------------------------------------------------------------
$BonusKindArray = array(
	"shopping" => "會員消費",
	//"share" => "會員拉會員",
	//"joinmember" => "加入會員",
	/*"birthday" => "會員生日"*/
);

//購物----------------------------------------------------------------------------------------------------------------
//方便到貨時間
$accept_time_array = array("皆可", "上午（12時前）", "下午（12~17時）", "夜間（17~20時）");

//訂單狀態
$states_array = array("取消", "未報到", "已報到", "已服務");

//付款狀態
$paymentstatus_array = array("尚未收款", "付款成功", /*"付款失敗", "付款金額錯誤"*/);

//訂單狀態
$states_array2 = array("訂單成立", "取消");

$dayName = array(
	1 => '一', 2 => '二', 3 => '三', 4 => '四', 5 => '五', 6 => '六', 7 => '日',
);
/*
$_timeAry = array(
	'10:30~11:30',
	'11:30~12:30',
	'12:30~13:30',
	'13:30~14:30',
	'14:30~15:30',
	'15:30~16:30',
	'16:30~17:30',
	'17:30~18:30',
	'18:30~19:30',
);
*/
$_timeAry = array(
	'10:30~11:00',
	'11:00~11:30',
	'11:30~12:00',
	'12:00~12:30',
	'12:30~13:00',
	'13:00~13:30',
	'13:30~14:00',
	'14:00~14:30',
	'14:30~15:00',
	'15:00~15:30',
	'15:30~16:00',
	'16:00~16:30',
	'16:30~17:00',
	'17:00~17:30',
	'17:30~18:00',
	'18:00~18:30',
	'18:30~19:00',
	'19:00~19:30',
);

//分隔線----------------------------------------------------------------------------------------------------------------
$sql = "Select * From web_basicset Where web_basicset_id = '1' ";
$rs = ConnectDB($DB, $sql);

$sql = "Select * From web_basicset Where web_basicset_id = '1' ";
$rs = ConnectDB($DB, $sql);
for ($i=0; $i<mysqli_num_rows($rs); $i++) {
	$row = mysqli_fetch_assoc($rs);
	foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
}
$page = (intval($_GET["page"])<=0) ? 1 : intval($_GET["page"]);	//目前在第幾頁

//分店
$sql2 = "
	Select 
		web_x_class.*
	From 
		web_x_class 
	Where 
		web_x_class.ifShow = '1'
	Order by
		web_x_class.asort ASC
";
$pdo = $pdoDB->prepare($sql2);
$pdo->execute();
$xClassRow = $pdo->fetchAll(PDO::FETCH_ASSOC);

//時段
$sql2 = "
	Select 
		web_time.*
	From 
		web_time 
	Where 
		web_time.ifShow = '1'
	Order by
		web_time.asort ASC
";
$pdo = $pdoDB->prepare($sql2);
$pdo->execute();
$timeRow = $pdo->fetchAll(PDO::FETCH_ASSOC);

//科目
$sql = "
	Select 
		web_class.*
	From 
		web_class 
	JOIN 
		(web_x_class) 
	ON 
		(
			web_class.web_x_class_id = web_x_class.web_x_class_id 
		) 
	Where 
		web_x_class.ifShow = '1' 
	And 
		web_class.ifShow = '1'
	Order by
		web_class.web_x_class_id ASC, web_class.asort ASC
";
//$rs = ConnectDB($DB, $sql);
$pdo = $pdoDB->prepare($sql);
$pdo->execute();
$classRow = $pdo->fetchAll(PDO::FETCH_ASSOC);

//
/*
$sql = "
	Select 
		web_team.*,
		web_x_team.subject as web_x_team_subject
	From 
		web_team 
	JOIN 
		(web_x_team) 
	ON 
		(
			web_team.web_x_team_id = web_x_team.web_x_team_id  and web_x_team.web_x_team_id IN(1,2)
		) 
	Where 
		web_x_team.ifShow = '1' 
	And 
		web_team.ifShow = '1'
	Order by
		web_team.web_x_team_id ASC, web_team.asort ASC
";
*/
$sql = "
	Select 
		web_x_team.*
	From 
		web_x_team 
	Where 
		web_x_team.ifShow = '1' 
	Order by
		web_x_team.asort ASC
";
//$rs = ConnectDB($DB, $sql);
$pdo = $pdoDB->prepare($sql);
$pdo->execute();
$teamRow = $pdo->fetchAll(PDO::FETCH_ASSOC);


function ConnectDB($DB, $sql) {
	global $DB;
	//$conn = mysql_connect($DB["HostName"], $DB["UserName"], $DB["Password"]); 
	$conn = mysqli_connect($DB["HostName"], $DB["UserName"], $DB["Password"], $DB["Database"]);
	mysqli_query($conn, 'SET NAMES utf8');
	if (!$conn) die("無法連接，請確認帳號及密碼是否正確");
	if (!mysqli_select_db($conn, $DB["Database"])) die("資料庫連接失敗");
	$rs = mysqli_query($conn, $sql);
	//mysqli_query("SET NAMES 'utf8'");
	return $rs;
}

function ConnectDBInsterId($DB, $sql) {
	global $DB;
	//$conn = mysql_connect($DB["HostName"], $DB["UserName"], $DB["Password"]); 
	$conn = mysqli_connect($DB["HostName"], $DB["UserName"], $DB["Password"], $DB["Database"]);
	mysqli_query($conn, 'SET NAMES utf8');
	if (!$conn) die("無法連接，請確認帳號及密碼是否正確");
	if (!mysqli_select_db($conn, $DB["Database"])) die("資料庫連接失敗");
	$rs = mysqli_query($conn, $sql);
	$insterId = mysqli_insert_id($conn); 
	//mysqli_query("SET NAMES 'utf8'");
	return $insterId;
}

function mysql_result($res, $row, $field=0) { 
    $res->data_seek($row); 
    $datarow = $res->fetch_array(); 
    return $datarow[$field]; 
}

function mysql_num_rows($res) { 
    $res = mysqli_num_rows($res);
    return $res; 
}

function mysql_fetch_assoc($res) { 
    $res = mysqli_fetch_assoc($res);
    return $res; 
}

function setWeb() {	//上線更改
	$location = ($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : "http://www.weba.com.tw";
	return "http://".$location;
}

function setWebs() {	//上線更改
	$location = ($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : "https://www.weba.com.tw";
	return "https://".$location;
}

//上稿轉成絕對路徑
function OnlineRoot($str) {
	$str = str_replace("/uploadfiles/", setWeb()."/uploadfiles/", $str);
	return $str;
}

function RunJs($url, $msg) {
	echo "<script language=\"javascript\">";
	if ($msg!="") echo "alert('".$msg."');";
	if ($url!="") echo "location.href='".$url."';";
	echo "</script>";
	die();
}

function RunAlert($msg) {
	die("<script language=\"javascript\">alert('".$msg."');window.history.go(-1);</script>");
	//die("<script language=\"javascript\">alert('".$msg."');</script>");
}

function RunAlertOnlyMsg($msg) {
	//die("<script language=\"javascript\">alert('".$msg."');window.history.go(-1);</script>");
	echo "<script language=\"javascript\">alert('".$msg."');</script>";
}

//編輯
function str_edit($str) {
	$str = str_replace(array("&acute;", "\"", "\&quot;", "&lt;", "&gt;"), array("'", "&quot;", "&quot;", "<", ">"), $str);
	$str = trim($str);
	return $str;
}

//過濾
function str_filter($str, $flag=false) {
	$str = str_replace(array("'", "\"", "<", ">"), array("&acute;", "&quot;", "&lt;", "&gt;"), $str);

	$PageURL = htmlentities($_SERVER["REQUEST_URI"]);
	if (strpos($PageURL, "control")!==false) {
		$filter = false;	//後台不過濾
		if ($flag==true) $filter = true;	//某些狀況需要過濾, ex:登入
		/*
			如果後台要過濾應該這樣寫
			$str = str_filter($str, true);
		*/
	} else {
		$filter = true;		//前台過濾
		if ($flag==true) $filter = false;	//後台和前台寫法顛倒, 某些狀況不過濾, ex:金流
		/*
			如果前台不過濾應該這樣寫
			$str = str_filter($str, true);
		*/
	}
	
	if ($filter) {	//過濾敏感字串
		/*$str = str_ireplace("?", "？", $str);
		$str = str_ireplace("%", "％", $str);
		$str = str_ireplace("(", "（", $str);
		$str = str_ireplace(")", "）", $str);
		$str = str_ireplace("{", "｛", $str);
		$str = str_ireplace("}", "｝", $str);
		$str = str_ireplace("=", "＝", $str);
		$str = str_ireplace(":", "：", $str);
		$str = str_ireplace("/", "／", $str);
		$str = str_ireplace("\\", "＼", $str);*/
		
		//$str = str_ireplace("alert", "ＡＬＥＲＴ", $str);
		//$str = str_ireplace("cookie", "ＣＯＯＫＩＥ", $str);
		//$str = str_ireplace("document", "ＤＯＣＵＭＥＮＴ", $str);
		//$str = str_ireplace("http", "ＨＴＴＰ", $str);
		//$str = str_ireplace("frame", "ＦＲＡＭＥ", $str);
		//$str = str_ireplace("javascript", "ＪＡＶＡＳＣＲＩＰＴ", $str);
		//$str = str_ireplace("script", "ＳＣＲＩＰＴ", $str);
		//$str = str_ireplace("session", "ＳＥＳＳＩＯＮ", $str);
		//$str = str_ireplace("src", "ＳＲＣ", $str);
		//$str = str_ireplace("style", "ＳＴＹＬＥ", $str);
	}
	
	$str = trim($str);
	return $str;
}

//顯示
function str_front($str) {
	$str = str_replace(array("&acute;", "&quot;", "&lt;", "&gt;"), array("'", "\"", "<", ">"), $str);
	$str = trim($str);
	return $str;
}

function getPwd($password_len = 8) {
    //$word = "abcdefghijkmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ0123456789";
	$word = "0123456789";
    $len = strlen($word);
    for ($i=0; $i<$password_len; $i++) $password .= $word[rand() % $len];
    return $password;
}

function getAllPwd($password_len = 10) {
    $word = "abcdefghijkmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ23456789";
    $len = strlen($word);
    for ($i=0; $i<$password_len; $i++) $password .= $word[rand() % $len];
    return $password;
}

//列表頁搜尋->時間
function list_search_by_time($field_array) {
	$list_search[sql_sub] = " Where 1 = 1 ";
	$field = $_POST["field"] ? str_filter($_POST["field"]) : str_filter($_GET["field"]);	//欄位
	$keyword = $_POST["keyword"] ? str_filter($_POST["keyword"]) : str_filter($_GET["keyword"]);	//關鍵字
	$search_sdate = $_POST["search_sdate"] ? str_filter($_POST["search_sdate"]) : str_filter($_GET["search_sdate"]);	//開始時間
	$search_edate = $_POST["search_edate"] ? str_filter($_POST["search_edate"]) : str_filter($_GET["search_edate"]);	//結束時間
	$search_samount = $_POST["search_samount"] ? str_filter($_POST["search_samount"]) : str_filter($_GET["search_samount"]);	//開始金額
	$search_eamount = $_POST["search_eamount"] ? str_filter($_POST["search_eamount"]) : str_filter($_GET["search_eamount"]);	//結束金額
	$field2 = ($field != 'cdate') ? $field : 'memberCode';
	if($field == 'TotalItemsOrdered') { 
		$list_search[sql_sub] = " Having 1 = 1 ";
		$list_search[sql_sub] .= " and (TotalItemsOrdered >= '".$search_samount."'  AND TotalItemsOrdered <= '".$search_eamount."') ";
		$list_search[link] .= "&search_sdate=".$search_sdate."&search_edate=".$search_edate."&search_samount=".$search_samount."&search_eamount=".$search_eamount;
	}				
	if ($keyword != "") {
		$keywords = explode(" ", $keyword);
		//if($field != 'cdate') {
			for ($i=0; $i<sizeof($keywords); $i++) {
				if($field == 'memberCode') {
					if ($keywords[$i]!="") $list_search[sql_sub] .= " and (".$field2." like '%".$keywords[$i]."%') ";
				} else if($field == 'CountItemsOrdered') { 
					$list_search[sql_sub] = " Having 1 = 1 ";
					if ($keywords[$i]!="") $list_search[sql_sub] .= " and (".$field2." = '".$keywords[$i]."') ";
					//if ($keywords[$i]!="") $list_search[sql_sub] .= " and (".$field2." = '".$keywords[$i]."') ";
				} else {
					if ($keywords[$i]!="") $list_search[sql_sub] .= " and (".$field2." like '".$keywords[$i]."') ";
				}
			}
		//}	
		if($search_sdate != "" && $search_edate != "")  {
			if($field == 'cdate') {
				$sql = "SELECT web_member_id FROM `web_x_order` WHERE 1 and (cdate >= '".$search_sdate." 00:00:00'  AND cdate <= '".$search_edate." 23:59:59')";
				$rs = ConnectDB($DB, $sql);
				for ($i=0; $i<mysqli_num_rows($rs); $i++) {
					$row = mysqli_fetch_assoc($rs);
					$member[] = str_front($row[web_member_id]);
				}
				$member = array_unique($member);
				$memberIdRow = implode(',', $member);	
				$list_search[sql_sub] .= " and (web_member_id IN (".$memberIdRow.")) ";
				$list_search[link] = "&field=cdate&keyword=".urlencode($keyword)."&search_sdate=".$search_sdate."&search_edate=".$search_edate;	
			
			} else if($field == 'pincode') {
				$sql2 = "SELECT web_x_order_ordernum FROM `web_order` ".$list_search[sql_sub]." AND (cdate >= '".$search_sdate." 00:00:00'  AND cdate <= '".$search_edate." 23:59:59')";
				$rs2 = ConnectDB($DB, $sql2);
				for ($i=0; $i<mysqli_num_rows($rs2); $i++) {
					$row2 = mysqli_fetch_assoc($rs2);
					$web_x_order_ordernum[] = "'".str_front($row2[web_x_order_ordernum])."'";
				}
				$web_x_order_ordernumRow = implode(',', $web_x_order_ordernum);	
				$list_search[sql_sub2] = " AND web_x_order.ordernum IN (".$web_x_order_ordernumRow.")";

				$sql = "SELECT web_member_id FROM `web_x_order` WHERE 1 ".$list_search[sql_sub2]." AND web_x_order.paymentstatus = '付款成功' AND web_x_order.states = '出貨' AND web_x_order.shipdate <= DATE_SUB(DATE(now()), INTERVAL 7 DAY) AND (cdate >= '".$search_sdate." 00:00:00'  AND cdate <= '".$search_edate." 23:59:59')";
				$rs = ConnectDB($DB, $sql);
				for ($i=0; $i<mysqli_num_rows($rs); $i++) {
					$row = mysqli_fetch_assoc($rs);
					$member[] = str_front($row[web_member_id]);
				}
				$memberIdRow = implode(',', $member);	
				$list_search[sql_sub] = " Where 1 = 1 ";
				$list_search[sql_sub] .= " and (web_member_id IN (".$memberIdRow.")) ";		
				$list_search[link] = "&field=pincode&keyword=".urlencode($keyword)."&search_sdate=".$search_sdate."&search_edate=".$search_edate;	

			} else {

				if($search_sdate > $search_edate){
					echo "開始時間不能大於結束時間";
					return;
				} else {
					$list_search[sql_sub] .= " and (cdate >= '".$search_sdate." 00:00:00'  AND cdate <= '".$search_edate." 23:59:59') ";
				}
				$list_search[link] .= "&search_sdate=".$search_sdate."&search_edate=".$search_edate;


			}
			
		} else {
			if($field == 'pincode') {
				$sql2 = "SELECT web_x_order_ordernum FROM `web_order` ".$list_search[sql_sub]."";
				$rs2 = ConnectDB($DB, $sql2);
				for ($i=0; $i<mysqli_num_rows($rs2); $i++) {
					$row2 = mysqli_fetch_assoc($rs2);
					$web_x_order_ordernum[] = "'".str_front($row2[web_x_order_ordernum])."'";
				}
				$web_x_order_ordernumRow = implode(',', $web_x_order_ordernum);	
				$list_search[sql_sub2] = " AND web_x_order.ordernum IN (".$web_x_order_ordernumRow.")";

				$sql = "SELECT web_member_id FROM `web_x_order` WHERE 1 ".$list_search[sql_sub2]." AND web_x_order.paymentstatus = '付款成功' AND web_x_order.states = '出貨' AND web_x_order.shipdate <= DATE_SUB(DATE(now()), INTERVAL 7 DAY)";
				$rs = ConnectDB($DB, $sql);
				for ($i=0; $i<mysqli_num_rows($rs); $i++) {
					$row = mysqli_fetch_assoc($rs);
					$member[] = str_front($row[web_member_id]);
				}
				$memberIdRow = implode(',', $member);	
				$list_search[sql_sub] = " Where 1 = 1 ";
				$list_search[sql_sub] .= " and (web_member_id IN (".$memberIdRow.")) ";		

			}
			$list_search[link] .= "&field=".$field."&keyword=".urlencode($keyword);
		}	
	} else if($search_sdate != "" && $search_edate != "")  {
		if($field != 'cdate') {
			if($search_sdate > $search_edate){
				echo "開始時間不能大於結束時間";
				return;
			} else {
				$list_search[sql_sub] .= " and (cdate >= '".$search_sdate." 00:00:00'  AND cdate <= '".$search_edate." 23:59:59') ";
			}

			$list_search[link] = "&search_sdate=".$search_sdate."&search_edate=".$search_edate;
		} else {
		
			$sql = "SELECT web_member_id FROM `web_x_order` WHERE 1 and (cdate >= '".$search_sdate." 00:00:00'  AND cdate <= '".$search_edate." 23:59:59')";
			$rs = ConnectDB($DB, $sql);
			for ($i=0; $i<mysqli_num_rows($rs); $i++) {
				$row = mysqli_fetch_assoc($rs);
				$member[] = str_front($row[web_member_id]);
			}
			$member = array_unique($member);
			$memberIdRow = implode(',', $member);	
			$list_search[sql_sub] .= " and (web_member_id IN (".$memberIdRow.")) ";
			$list_search[link] = "&field=cdate&search_sdate=".$search_sdate."&search_edate=".$search_edate;
		}	
	} else if($field == 'tags') {
		
		$sql = "SELECT *, MATCH (`tags`) AGAINST ('".implode(' ', $_POST['bestsellers3'])."' IN BOOLEAN MODE) score FROM web_member having score>0";
		$rs = ConnectDB($DB, $sql);
		for ($i=0; $i<mysqli_num_rows($rs); $i++) {
			$row = mysqli_fetch_assoc($rs);
			$member[] = str_front($row[web_member_id]);
		}
		$memberIdRow = implode(',', $member);	
		$list_search[sql_sub] = " Where 1 = 1 ";
		$list_search[sql_sub] .= " and (web_member_id IN (".$memberIdRow.")) ";	
		$list_search[link] = "&field=tags&_web_member_id=".$memberIdRow;
	}

	foreach($field_array as $_key=>$_value) {
		$field_list .= "\n\t  <option value=\"".$_key."\"";
		if ($field==$_key) $field_list .= " selected";
		$field_list .= ">".$_value."</option>";
	}
		
	echo "
    <select name=\"field\">".$field_list."
    </select>
	<input name=\"keyword\" type=\"text\" value=\"".$keyword."\" maxlength=\"100\" placeholder=\"請輸入關鍵字\" />
    ";
	echo "
		<div id=\"tagsBar\" style=\"display:none; padding-top:5px; padding-bottom:5px;\">
			<input name=\"tags\" id=\"mySingleField\" value=\"\" type=\"hidden\">
			<ul id=\"myTags2\">
			</ul>
		</div>
	";
   	echo "
   	<span id=\"orderAmount\" style=\"display:none\">
   	金額：<input name=\"search_samount\" id=\"search_samount\" type=\"text\" value=\"".$search_samount."\" maxlength=\"7\" placeholder=\"開始金額\" style=\"width: 100px\" />~<input name=\"search_eamount\" id=\"search_eamount\" type=\"text\" value=\"".$search_eamount."\" maxlength=\"7\" placeholder=\"結束金額\" style=\"width: 100px\" />
	</span>
   	";
    
    echo "
   	時間：<input name=\"search_sdate\" id=\"search_sdate\" type=\"text\" value=\"".$search_sdate."\" maxlength=\"10\" placeholder=\"開始日期\" style=\"width: 100px\" />~<input name=\"search_edate\" id=\"search_edate\" type=\"text\" value=\"".$search_edate."\" maxlength=\"10\" placeholder=\"結束日期\" style=\"width: 100px\" />
    <input name=\"submit\" id=\"searchBtn\" type=\"submit\" value=\"搜尋\" />
    
";

	$list_search[hidden] = "
  <input type=\"hidden\" name=\"field\" value=\"".$field."\" />
  <input type=\"hidden\" name=\"keyword\" value=\"".$keyword."\" />
";
			
	return $list_search;
}

//列表頁搜尋
function list_search($field_array) {
	$list_search[sql_sub] = " Where 1 = 1 ";
	$field = $_POST["field"] ? str_filter($_POST["field"]) : str_filter($_GET["field"]);	//欄位
	$keyword = $_POST["keyword"] ? str_filter($_POST["keyword"]) : str_filter($_GET["keyword"]);	//關鍵字
	if ($keyword!="") {
		$keywords = explode(" ", $keyword);
		for ($i=0; $i<sizeof($keywords); $i++) {
			if ($keywords[$i]!="") $list_search[sql_sub] .= " and (".$field." like '%".$keywords[$i]."%') ";
		}
		$list_search[link] = "&field=".$field."&keyword=".urlencode($keyword);
	}

	foreach($field_array as $_key=>$_value) {
		$field_list .= "\n\t  <option value=\"".$_key."\"";
		if ($field==$_key) $field_list .= " selected";
		$field_list .= ">".$_value."</option>";
	}
		
	echo "<input name=\"keyword\" type=\"text\" value=\"".$keyword."\" maxlength=\"100\" placeholder=\"請輸入關鍵字\" />
    <select name=\"field\">".$field_list."
    </select>
    <input name=\"submit\" type=\"submit\" value=\"搜尋\" />
";

	$list_search[hidden] = "
  <input type=\"hidden\" name=\"field\" value=\"".$field."\" />
  <input type=\"hidden\" name=\"keyword\" value=\"".$keyword."\" />
";
			
	return $list_search;
}

//列表頁搜尋
function list_search_code($field_array) {
	$list_search[sql_sub] = " Where 1 = 1 ";
	$field = $_POST["field"] ? str_filter($_POST["field"]) : str_filter($_GET["field"]);	//欄位
	$keyword = $_POST["keyword"] ? str_filter($_POST["keyword"]) : str_filter($_GET["keyword"]);	//關鍵字
	$search_stotal = $_POST["search_Stotal"] ? str_filter($_POST["search_Stotal"]) : str_filter($_GET["search_Stotal"]);	//金額
	$search_etotal = $_POST["search_Etotal"] ? str_filter($_POST["search_Etotal"]) : str_filter($_GET["search_Etotal"]);	//金額
	$search_sdate = $_POST["search_Sdate"] ? str_filter($_POST["search_Sdate"]) : str_filter($_GET["search_Sdate"]);	//開始時間
	$search_edate = $_POST["search_Edate"] ? str_filter($_POST["search_Edate"]) : str_filter($_GET["search_Edate"]);	//結束時間
	
	$outputFlag = true;

	if($field == 'usedate') {
		if($search_sdate != "" && $search_edate != "")  {
			if($search_sdate > $search_edate){
				echo "開始時間不能大於結束時間";
				$outputFlag = false;
				return;
			} else {
				$list_search[sql_sub] .= " and (usedate >= '".$search_sdate." 00:00:00'  AND usedate <= '".$search_edate." 23:59:59') ";
			}
			$list_search[link] .= "&field=".$field."&search_sdate=".$search_sdate."&search_edate=".$search_edate;
		}
	} else if($field == 'total') {
		//echo 111111;
		$search_stotal = ($search_stotal) ? $search_stotal : 0;
		if($search_etotal != "")  {

			if($search_stotal > $search_etotal){
				echo "開始金額不能大於結束金額";
				$outputFlag = false;
				return;
			} else {
				$list_search2[sql_sub] .= "Where 1 = 1 And (total > '".$search_stotal."'  AND total <= '".$search_etotal."') And web_code_id != '0' And web_x_order.paymentstatus = '付款成功'";

				$sql = "Select SQL_CALC_FOUND_ROWS web_member_id From web_x_order ".$list_search2[sql_sub]." GROUP BY web_member_id ";
	            $rs = ConnectDB($DB, $sql);
	            if(mysqli_num_rows($rs)) {
	              for ($i=0; $i<mysqli_num_rows($rs); $i++) {
	                $row = mysqli_fetch_assoc($rs);
	                $web_member_ary[] = $row['web_member_id'];
	              }
	              $list_search[sql_sub] = "Where 1 = 1 and (web_member_id IN (".implode(',', $web_member_ary)."))";  
	            } else {
	              echo '無查詢資料';
	              $outputFlag = false;
	            }

			}
			$list_search[link] .= "&field=".$field."&search_stotal=".$search_stotal."&search_etotal=".$search_etotal;
		}
	} else if($field == 'uname') {
		if ($keyword!="") {
			$keywords = explode(",", $keyword);
			for ($i=0; $i<sizeof($keywords); $i++) {
				if($i == 0) {
					if ($keywords[$i]!="") 
						$list_search2[sql_sub] .= "AND (".$field." like '%".$keywords[$i]."%') ";
				} else {
					if ($keywords[$i]!="") 
						$list_search2[sql_sub] .= "OR (".$field." like '%".$keywords[$i]."%') ";
				}
			}
			$list_search[link] = "&field=".$field."&keyword=".urlencode($keyword);
		}
		$sql = "Select SQL_CALC_FOUND_ROWS web_member_id From web_member Where 1 = 1 ".$list_search2[sql_sub]." GROUP BY web_member_id ";
        $rs = ConnectDB($DB, $sql);
        if(mysqli_num_rows($rs)) {
          for ($i=0; $i<mysqli_num_rows($rs); $i++) {
            $row = mysqli_fetch_assoc($rs);
            $web_member_ary[] = $row['web_member_id'];
          }
          $list_search[sql_sub] = "Where 1 = 1  and (web_member_id IN (".implode(',', $web_member_ary)."))";  
        } else {
          echo '無查詢資料';
          $outputFlag = false;
        }
		
	} else {
		if ($keyword!="") {
			$keywords = explode(" ", $keyword);
			for ($i=0; $i<sizeof($keywords); $i++) {
				if ($keywords[$i]!="") $list_search[sql_sub] .= " and (".$field." like '%".$keywords[$i]."%') ";
			}
			$list_search[link] = "&field=".$field."&keyword=".urlencode($keyword);
		}

	}
	
	$list_search[outputFlag] = ($outputFlag) ? true : false;
	
	foreach($field_array as $_key=>$_value) {
		$field_list .= "\n\t  <option value=\"".$_key."\"";
		if ($field==$_key) $field_list .= " selected";
		$field_list .= ">".$_value."</option>";
	}
		
	echo "<input name=\"keyword\" type=\"text\" value=\"".$keyword."\" maxlength=\"100\" placeholder=\"請輸入關鍵字\" />
    <select name=\"field\">".$field_list."
    </select>
    <input name=\"submit\" type=\"submit\" value=\"搜尋\" />
";

	$list_search[hidden] = "
  <input type=\"hidden\" name=\"field\" value=\"".$field."\" />
  <input type=\"hidden\" name=\"keyword\" value=\"".$keyword."\" />
";
			
	return $list_search;
}

//列表頁搜尋-產品
function list_search_product($field_array, $dbTable) {
	$list_search[sql_sub] = " Where 1 = 1 ";
	$field = $_POST["field"] ? str_filter($_POST["field"]) : str_filter($_GET["field"]);	//欄位
	$keyword = $_POST["keyword"] ? str_filter($_POST["keyword"]) : str_filter($_GET["keyword"]);	//關鍵字
	$orderByItem = $_POST["orderByItem"] ? str_filter($_POST["orderByItem"]) : str_filter($_GET["orderByItem"]);	//
	$orderBy = $_POST["orderBy"] ? str_filter($_POST["orderBy"]) : str_filter($_GET["orderBy"]);	//
	$ControlPage = $_POST["ControlPage"] ? str_filter($_POST["ControlPage"]) : str_filter($_GET["ControlPage"]);	//

	$list_search[link] = "&orderByItem=".$orderByItem."&orderBy=".$orderBy."&ControlPage=".$ControlPage;
	if ($keyword!="") {
		$keywords = explode(" ", $keyword);
		for ($i=0; $i<sizeof($keywords); $i++) {
			if ($keywords[$i]!="") { 
				if(!in_array($field, array('stock'))) {
					if(!in_array($field, array('series'))) {
						$list_search[sql_sub] .= " and (".$dbTable.".".$field." like '%".$keywords[$i]."%') ";
					} else {
						$list_search[sql_sub] .= " and (web_xxxx_product.subject like '%".$keywords[$i]."%' || web_xxx_product.subject like '%".$keywords[$i]."%' || web_xx_product.subject like '%".$keywords[$i]."%' || web_x_product.subject like '%".$keywords[$i]."%') ";
					}	
				} else {
					$list_search[sql_sub] .= " and (".$dbTable.".".$field." <= '".$keywords[$i]."') ";
				}	
			}
		}
		$list_search[link] = "&orderByItem=".$orderByItem."&orderBy=".$orderBy."&ControlPage=".$ControlPage."&field=".$field."&keyword=".urlencode($keyword);
	}

	foreach($field_array as $_key=>$_value) {
		$field_list .= "\n\t  <option value=\"".$_key."\"";
		if ($field==$_key) $field_list .= " selected";
		$field_list .= ">".$_value."</option>";
	}

	$orderByItem_array = array(
		'displayorder' => '排序碼',
		'cdate' => '建立時間',
		'price_cost' => '價格',
		'hits' => '點閱率',
	);

	$orderBy_array = array(
		'ASC' 	=> '遞增',
		'DESC' 	=> '遞減',
	);

	foreach($orderByItem_array as $_key=>$_value) {
		$orderByItem_list .= "\n\t  <option value=\"".$_key."\"";
		if ($orderByItem==$_key) $orderByItem_list .= " selected";
		$orderByItem_list .= ">".$_value."</option>";
	}

	foreach($orderBy_array as $_key=>$_value) {
		$orderBy_list .= "\n\t  <option value=\"".$_key."\"";
		if ($orderBy==$_key) $orderBy_list .= " selected";
		$orderBy_list .= ">".$_value."</option>";
	}
		
	echo "<input name=\"keyword\" type=\"text\" value=\"".$keyword."\" maxlength=\"100\" placeholder=\"請輸入關鍵字\" />
    <select name=\"field\">".$field_list."
    </select>
    排序項目：
    <select name=\"orderByItem\" id=\"orderByItem\">".$orderByItem_list."
    </select>
    <select name=\"orderBy\" id=\"orderBy\">".$orderBy_list."
    </select>
    顯示筆數：<input name=\"ControlPage\" type=\"text\" value=\"".$ControlPage."\" maxlength=\"5\" size=\"6\" placeholder=\"請輸入數字\" style=\"width:90px\" />
    <input name=\"submit\" type=\"submit\" value=\"搜尋\" />
";

	$list_search[hidden] = "
  <input type=\"hidden\" name=\"field\" value=\"".$field."\" />
  <input type=\"hidden\" name=\"keyword\" value=\"".$keyword."\" />
";
			
	return $list_search;
}

//列表頁取得總頁數和總紀錄數
function list_paging($page, $record_per_page) {
	$sql = "SELECT FOUND_ROWS() as counter";	//取得紀錄數
	$rs = ConnectDB($DB, $sql);
	$total_records = mysql_result($rs, 0, "counter");
	
	if ($record_per_page<=0) $record_per_page = 1;			//每頁幾筆
	$total_pages = ceil($total_records/$record_per_page);	//計算總頁數
	if ($page>$total_pages) $page = $total_pages;			//修正輸入過大的頁數
	
	$list_paging[pages] = $total_pages;		//總頁數
	$list_paging[records] = $total_records;	//總紀錄數
	
	return $list_paging;
}

//列表頁取得總頁數和總紀錄數
function list_paging_sql($page, $record_per_page, $sql) {
	$sql = $sql;	//取得紀錄數
	$rs = ConnectDB($DB, $sql);
	$total_records = mysql_num_rows($rs);
	
	if ($record_per_page<=0) $record_per_page = 1;			//每頁幾筆
	$total_pages = ceil($total_records/$record_per_page);	//計算總頁數
	if ($page>$total_pages) $page = $total_pages;			//修正輸入過大的頁數
	
	$list_paging[pages] = $total_pages;		//總頁數
	$list_paging[records] = $total_records;	//總紀錄數
	
	return $list_paging;
}

//列表頁分頁
function list_page($url, $page, $total_pages, $total_records, $hidden, $search) {
	/*
	$url: 連結目標
	$page: 目前頁數
	$total_pages: 總頁數
	$total_records: 總紀錄數
	*/
	
	$symbol = (strpos($url, "?")!==false) ? "&" : "?";
	if ($search) $search = "&".trim($search, "&");
	
	if ($total_pages>=100) {
		$s_page = $page - 3;
		$e_page = $page + 3;
	} else {
		$s_page = $page - 5;
		$e_page = $page + 5;
	}
	if ($s_page < 1) $s_page = 1;
	if ($e_page > $total_pages) $e_page = $total_pages;

	if ($total_pages>1) {
		$page_list = "<!--分頁開始-->\n  <div id=\"page\">\n\t<div id=\"num\">\n";
		if ($page>1) {
			$page_list .= "\t  <a href=\"".$url.$symbol."page=1".$search."\">«</a>\n\t  <a href=\"".$url.$symbol."page=".($page-1).$search."\">‹</a>\n";
		}
		
		for ($i=$s_page; $i<=$e_page; $i++) {
			if ($i==$page) {
				$page_list .= "\t  <a class=\"stay\">".$i."</a>\n";
			} else {
				$page_list .= "\t  <a href=\"".$url.$symbol."page=".$i.$search."\">".$i."</a>\n";
			}
		}
		
		if ($page<$total_pages) {
			$page_list .= "\t  <a href=\"".$url.$symbol."page=".($page+1).$search."\">›</a>\n\t  <a href=\"".$url.$symbol."page=".$total_pages.$search."\">»</a>\n";
		}
		$page_list .= "\t</div>\n";
		
		//跳頁
		$page_list .= "\t<div id=\"statistics\">\n";
		$page_list .= "\t  <select name=\"jump\" onchange=\"MM_jumpMenu('self', this, 0)\">\n";
		if ($total_pages>=100) {	//大於100頁
			for ($i=1; $i<=$total_pages; $i=$i+10) {
				$page_list .= "\t  <option value=\"".$url.$symbol."page=".$i.$search."\"";
				if ($page==$i) $page_list .= " selected";
				$page_list .= ">第 ".$i." 頁</option>\n";
			}
			
			//最後一頁
			if (($i-10)!=$total_pages) {
				$i = $total_pages;
				$page_list .= "\t  <option value=\"".$url.$symbol."page=".$i.$search."\"";
				if ($page==$i) $page_list .= " selected";
				$page_list .= ">第 ".$i." 頁</option>\n";
			}
		} else {
			for ($i=1; $i<=$total_pages; $i++) {
				$page_list .= "\t  <option value=\"".$url.$symbol."page=".$i.$search."\"";
				if ($page==$i) $page_list .= " selected";
				$page_list .= ">第 ".$i." 頁</option>\n";
			}
		}
		$page_list .= "\t</select>共".number_format($total_records)."筆／共".number_format($total_pages)."頁\n\t</div>\n  </div>\n  <!--分頁結束-->\n";
	}
	$page_list .= "  <input type=\"hidden\" name=\"page\" value=\"".$page."\" />".$hidden;
	
	echo $page_list;
}

//前台分頁
function front_page($url, $page, $total_pages, $total_records, $search) {
	/*
	$url: 連結目標
	$page: 目前頁數
	$total_pages: 總頁數
	$total_records: 總紀錄數
	*/
	
	$symbol = (strpos($url, "?")!==false) ? "&" : "?";
	if ($search) $search = "&".trim($search, "&");
	
	$s_page = $page - 5;
	$e_page = $page + 5;
	if ($s_page < 1) $s_page = 1;
	if ($e_page > $total_pages) $e_page = $total_pages;
	
	if ($total_pages>1) {
		$page_list = "
		<div class=\"row clearfix m_xs_bottom_30\">
            <div class=\"col-md-6 col-xs-4 text-left\">
              <p class=\"d_inline_middle f_size_medium\">共".number_format($total_records)."筆資料</p>
            </div>
            <div class=\"col-md-6 col-xs-8 text-right\"> 
		";
		if ($page>1) {
			$page_list .= "
			<a href=\"".$url.$symbol."page=1".$search."\" title=\"第一頁\" role=\"button\" class=\"f_size_large button_type_10 color_dark d_inline_middle bg_cs_hover bg_light_color_1 t_align_c tr_delay_hover r_corners box_s_none\"><i class=\"fa fa-angle-double-left\"></i></a>
			<a href=\"".$url.$symbol."page=".($page-1).$search."\" title=\"上一頁\" role=\"button\" class=\"f_size_large button_type_10 color_dark d_inline_middle bg_cs_hover bg_light_color_1 t_align_c tr_delay_hover r_corners box_s_none\"><i class=\"fa fa-angle-left\"></i></a>
			";
		}
		
		$page_list .= "<ul class=\"horizontal_list clearfix d_inline_middle f_size_medium m_left_10\">";
		for ($i=$s_page; $i<=$e_page; $i++) {
			if ($i==$page) {
				$page_list .= "<li class=\"m_right_10\"><a title=\"第".$i."頁\" class=\"scheme_color\">".$i."</a></li>";
			} else {
				$page_list .= "<li class=\"m_right_10\"><a href=\"".$url.$symbol."page=".$i.$search."\" title=\"第".$i."頁\" class=\"color_dark\">".$i."</a></li>";
			}
		}
		$page_list .= "</ul>";
		
		if ($page<$total_pages) {
			$page_list .= "
			<a href=\"".$url.$symbol."page=".($page+1).$search."\" title=\"下一頁\" role=\"button\"><i class=\"fa fa-angle-right\"></i></a>
            <a href=\"".$url.$symbol."page=".$total_pages.$search."\" title=\"最後一頁\" role=\"button\"><i class=\"fa fa-angle-double-right\"></i></a>
			";
		}
		$page_list .= "</div></div>";
	}
	
	echo $page_list;
}

//前台分頁(Ajax)
function ajax_page($item, $page, $total_pages, $total_records, $id, $search) {
	/*
	$url: 連結目標
	$page: 目前頁數
	$total_pages: 總頁數
	$total_records: 總紀錄數
	*/
	
	$symbol = (strpos($url, "?")!==false) ? "&" : "?";
	if ($search) $search = "&".trim($search, "&");
	
	$s_page = $page - 5;
	$e_page = $page + 5;
	if ($s_page < 1) $s_page = 1;
	if ($e_page > $total_pages) $e_page = $total_pages;
	
	if ($total_pages>1) {
		$page_list = "
		<div class=\"row clearfix m_xs_bottom_30\">
            <div class=\"col-lg-7 col-md-7 col-sm-7 col-xs-5\">
              <p class=\"d_inline_middle f_size_medium\">共".number_format($total_records)."筆資料</p>
            </div>
            <div class=\"col-lg-5 col-md-5 col-sm-5 col-xs-7 t_align_r\"> 
		";
		if ($page>1) {
			$page_list .= "
			<a href=\"javascript:;\" onClick=\"ChangePage('".$item."', '0', '1', '".$id."');\" title=\"第一頁\" role=\"button\" class=\"f_size_large button_type_10 color_dark d_inline_middle bg_cs_hover bg_light_color_1 t_align_c tr_delay_hover r_corners box_s_none\"><i class=\"fa fa-angle-double-left\"></i></a>
			<a href=\"javascript:;\" onClick=\"ChangePage('".$item."', '0', '".$i."', '".$id."');\" title=\"上一頁\" role=\"button\" class=\"f_size_large button_type_10 color_dark d_inline_middle bg_cs_hover bg_light_color_1 t_align_c tr_delay_hover r_corners box_s_none\"><i class=\"fa fa-angle-left\"></i></a>
			";
		}
		
		$page_list .= "<ul class=\"horizontal_list clearfix d_inline_middle f_size_medium m_left_10\">";
		for ($i=$s_page; $i<=$e_page; $i++) {
			if ($i==$page) {
				$page_list .= "<li class=\"m_right_10\"><a href=\"javascript:;\" title=\"第".$i."頁\" class=\"scheme_color\">".$i."</a></li>";
			} else {
				$page_list .= "<li class=\"m_right_10\"><a href=\"javascript:;\" onClick=\"ChangePage('".$item."', '0', '".$i."', '".$id."');\" title=\"第".$i."頁\" class=\"color_dark\">".$i."</a></li>";
			}
		}
		$page_list .= "</ul>";
		
		if ($page<$total_pages) {
			$page_list .= "
			<a href=\"javascript:;\" onClick=\"ChangePage('".$item."', '0', '".($page+1)."', '".$id."');\" title=\"下一頁\" role=\"button\" class=\"f_size_large button_type_10 color_dark d_inline_middle bg_cs_hover bg_light_color_1 t_align_c tr_delay_hover r_corners box_s_none\"><i class=\"fa fa-angle-right\"></i></a>
            <a href=\"javascript:;\" onClick=\"ChangePage('".$item."', '0', '".$total_pages."', '".$id."');\" title=\"最後一頁\" role=\"button\" class=\"f_size_large button_type_10 color_dark d_inline_middle bg_cs_hover bg_light_color_1 t_align_c tr_delay_hover r_corners box_s_none\"><i class=\"fa fa-angle-double-right\"></i></a>
			";
		}
		$page_list .= "</div></div>";
	}
	
	return $page_list;
}

//上一則下一則	
function LeftRight($sql, $id) {
	if ($sql=="" || $id==0) return;
	$page_array = array();	
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		$page_array[$i] = $row["id"];
		$subject_array[$i] = $row["subject"];
	}

	$page = array_search($id, $page_array);
	$LeftRight[left] = intval($page_array[$page-1]);
	$LeftRight[left_subject] = $subject_array[$page-1];
	$LeftRight[right] = intval($page_array[$page+1]);
	$LeftRight[right_subject] = $subject_array[$page+1];
	
	return $LeftRight;
}

function CuttingStr($str, $strlen)  {
	//把' '先轉成空白
	$str = str_replace(' ', ' ', $str);
	
	$output_str_len = 0; //累計要輸出的擷取字串長度
	$output_str = ''; //要輸出的擷取字串
	
	//逐一讀出原始字串每一個字元
	for($i=0; $i<$strlen; $i++) { //擷取字數已達到要擷取的字串長度，跳出回圈
		if($output_str_len >= $strlen) break;
		
		//取得目前字元的ASCII碼
		$str_bit = ord(substr($str, $i, 1));
		if ($str_bit < 128) {	//ASCII碼小於 128 為英文或數字字符
			$output_str_len += 1; //累計要輸出的擷取字串長度，英文字母算一個字數
			$output_str .= substr($str, $i, 1); //要輸出的擷取字串
		} elseif($str_bit > 191 && $str_bit < 224)  {	//第一字節為落於192~223的utf8的中文字(表示該中文為由2個字節所組成utf8中文字)
			$output_str_len += 2; //累計要輸出的擷取字串長度，中文字需算二個字數
			$output_str .= substr($str, $i, 2); //要輸出的擷取字串
			$i++;	
		} elseif($str_bit > 223 && $str_bit < 240) {	//第一字節為落於223~239的utf8的中文字(表示該中文為由3個字節所組成的utf8中文字)
			$output_str_len += 2; //累計要輸出的擷取字串長度，中文字需算二個字數
			$output_str .= substr($str, $i, 3); //要輸出的擷取字串
			$i+=2;	
		} elseif($str_bit > 239 && $str_bit < 248) {	//第一字節為落於240~247的utf8的中文字(表示該中文為由4個字節所組成的utf8中文字)
			$output_str_len += 2; //累計要輸出的擷取字串長度，中文字需算二個字數
			$output_str .= substr($str, $i, 4); //要輸出的擷取字串
			$i+=3;
		}
	}
	
	if ($str!=$output_str)	//要輸出的擷取字串為空白時，輸出原始字串
		return ($output_str == '') ? $str : $output_str."...";
	else
		return ($output_str == '') ? $str : $output_str;
}

//顯示兩個日期差異
function DateDiff($stime, $etime) {
	$timeDiff = strtotime($etime) - strtotime($stime);
	//return floor($timeDiff / 60);		//分
	//return floor($timeDiff / (60 * 60));	//時
	return floor($timeDiff / (60 * 60 * 24));	//日
}

//顯示兩個時間差異
function TimeDiff($stime, $etime) {
	return strtotime($etime) - strtotime($stime);
}

//圖片
function ShowPic($pic, $root, $null) {
	$pic_list = "";
	$file_array = explode("/", $pic);	
	$Files = $file_array[0];
	if ($Files!="" && file_exists($root.$Files)) {
		$eFile = explode(".", $Files);
		foreach ($eFile as $value) $format = strtolower($value);
		if ($format == "jpg" || $format == "jpeg" || $format == "gif" || $format == "png") $pic_list = $root.$Files;
	}
	if ($pic_list=="") $pic_list = $null;
	
	return $pic_list;
}

// 取得youtube圖檔
function _parse_youtube_url($id, $return='hqthumb', $width=500, $height=369){
    //parse_str( parse_url( $url, PHP_URL_QUERY ) );
    //$id = empty($v)?NULL:trim($v);
    if($return == 'embed'){
        return '<iframe width="'.$width.'" height="'.$height.'" src="http://www.youtube.com/embed/'.$id.'" frameborder="0" allowfullscreen></iframe>';
    }
    //return normal thumb
    else if($return == 'thumb'){
        return 'http://i1.ytimg.com/vi/'.$id.'/default.jpg';
    }
    //return hqthumb
    else if($return == 'hqthumb'){
        return 'http://i1.ytimg.com/vi/'.$id.'/hqdefault.jpg';
    }
    else if($return == 'mqthumb'){
        return 'http://i1.ytimg.com/vi/'.$id.'/mqdefault.jpg';
    }
    // else return id
    else{
        return $id;
    }
}

//檔案
function ShowFile($filename, $root) {
	$file_array = explode("/", trim($filename, "/"));	
	$Files = $file_array[0];	//第幾個
	if ($Files!="" && file_exists($root.$Files))
		return $root.$Files;
	else
		return "#";
}

// ref http://plog.longwin.com.tw/programming/2007/08/20/php_image_resize_2007
function ImageResize($from_filename, $save_filename, $in_width=800, $in_height=600, $quality=100) {
    $allow_format = array("jpeg", "png", "gif");
    $sub_name = $t = "";

    $img_info = getimagesize($from_filename);

    $width    = $img_info["0"];
    $height   = $img_info["1"];
    $imgtype  = $img_info["2"];
    $imgtag   = $img_info["3"];
    $bits     = $img_info["bits"];
    $channels = $img_info["channels"];
    $mime     = $img_info["mime"];
	
    list($t, $sub_name) = explode("/", $mime);
	
    if ($sub_name == "jpg") {
        $sub_name = "jpeg";
    }
	

    if (!in_array($sub_name, $allow_format)) return false;

    $percent = getResizePercent($width, $height, $in_width, $in_height);
    $new_width  = $width * $percent;
    $new_height = $height * $percent;

	$image_new = imagecreatetruecolor($new_width, $new_height);
	
	if (strpos($from_filename, ".jpeg")!==false || strpos($from_filename, ".jpg")!==false) {
		$image = imagecreatefromjpeg($from_filename);
		imagecopyresampled($image_new, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
		return imagejpeg($image_new, $save_filename, $quality);
	}

	if (strpos($from_filename, ".gif")!==false) {
		$image = imagecreatefromgif($from_filename);
		imagecopyresampled($image_new, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
		return imagegif($image_new, $save_filename, $quality);
	}

	if (strpos($from_filename, ".png")!==false) {
		$image = imagecreatefrompng($from_filename);
		imagealphablending($image_new, false);
		imagesavealpha($image_new, true);
		imagecopyresampled($image_new, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
		return imagepng($image_new, $save_filename, (int)$quality/10-1);	//quality 0~9之間
	}
}

// ref http://plog.longwin.com.tw/programming/2007/08/20/php_image_resize_2007
function getResizePercent($source_w, $source_h, $inside_w, $inside_h) {
    if ($source_w < $inside_w && $source_h < $inside_h) return 1;
    $w_percent = $inside_w / $source_w;
    $h_percent = $inside_h / $source_h;
    return ($w_percent > $h_percent) ? $h_percent : $w_percent;
}

//寄信
function SendMail($MailInfo) {
	$sql = "Select Init_SMTP_Host, Init_SMTP_Port, Init_SMTP_Email, Init_SMTP_Pw, Init_SMTP_Secure From web_basicset ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_edit($row[$_key]);
	}

	include_once($MailInfo[root]."class/phpmailer/class.phpmailer.php");
	
	$mail = new PHPMailer();
	$mail->CharSet = "utf-8";			//設定語言編碼
	$mail->Encoding = "base64";			//設定內容編碼方式
	$mail->IsSMTP();					//send via SMTP (是否使用smtp寄信方式) 
	$mail->SMTPAuth = true;				//是否需要 smtp 驗證
	if ($Init_SMTP_Secure!="") {		//gmail的SMTP主機需要使用SSL
		$mail->SMTPSecure = $Init_SMTP_Secure;
	}
	$mail->IsHTML(true);				//信件內容是否使用HTML方式編寫
	
	$mail->Host = $Init_SMTP_Host;		//SMTP Host
	$mail->Port = $Init_SMTP_Port;		//SMTP Port
	$mail->Username = $Init_SMTP_Email;	//SMTP 帳號
	$mail->Password = $Init_SMTP_Pw;	//SMTP 登入密碼
	
	$mail->From = $Init_SMTP_Email;		//寄件人信箱
	//$mail->From = $MailInfo[FromMail];			//寄件人信箱
	$mail->FromName = $MailInfo[FromName];	//寄件人名稱

	//收件人
	$MailInfo[ToMail] = str_replace(";", ",", $MailInfo[ToMail]);
	$mail_array = explode(",", $MailInfo[ToMail]);
	$name_array = explode(",", $MailInfo[ToName]);
	
	for ($i=0; $i<sizeof($mail_array); $i++) {
		if ($mail_array[$i]!="") {
			if ($name_array[0]!="")  $name_array[$i] = $name_array[$i];
			//if ($name_array[0]!="")  $name_array[$i] = $name_array[0];
			if ($name_array[$i]=="") $name_array[$i] = $mail_array[$i];
			$mail->AddAddress($mail_array[$i], $name_array[$i]);
		}
	}
	
	//密件副本
	$MailInfo[BccMail] = str_replace(";", ",", $MailInfo[BccMail]);
	if ($MailInfo[BccMail]!="") {
		$bcc_array = explode(",", $MailInfo[BccMail]);
		for ($i=0; $i<sizeof($bcc_array); $i++) {
			if ($bcc_array[$i]!="") $mail->AddBCC($bcc_array[$i]);
		}
	}
	
	//回信人
	$MailInfo[ReplyMail] = str_replace(";", ",", $MailInfo[ReplyMail]);
	if ($MailInfo[ReplyMail]!="") $mail->AddReplyTo($MailInfo[ReplyMail], $MailInfo[ReplyName]);

	$mail->Subject = $MailInfo[subject];	//郵件主旨
	//$MailInfo[body] = "＊提醒您，本信件由系統自動發送，請勿直接回覆，謝謝＊<br /><br />".$MailInfo[body];
	$mail->Body = $MailInfo[body];	//郵件內容

	if (!$mail->Send()) {
		//改用mail()
		//$MailInfo[body] = "＊".$MailInfo[body];
		$subjects = "=?UTF-8?B?".base64_encode($MailInfo[subject])."?=";	//郵件主旨
		$body = iconv("UTF-8", "Big5", $MailInfo[body]);	//郵件內容

		//寄件人
		$mail_from_array = explode(",", $MailInfo[FromMail]);
		
		//收件人
		$mail_array = explode(",", $MailInfo[ToMail], 2);
		$name_array = explode(",", $MailInfo[ToName]);

		$headers  = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=Big5\r\n";
		$headers .= "From: ".$MailInfo[FromName]."<".$mail_from_array[0].">\r\n";
		$headers .= "To: ".$name_array[0]."<".$mail_array[0].">\r\n";
		if ($mail_array[1]!="") $headers .= "Cc: ".$mail_array[1]."\r\n";
		if ($MailInfo[BccMail]!="") $headers .= "Bcc: ".$MailInfo[BccMail]."\r\n";
		$headers = iconv("UTF-8", "Big5", $headers);
	
		if (mail($mail_array[0], $subjects, $body, $headers))
			$message = "信件已寄出";
		else
			$message = "信件無法寄出，錯誤訊息：".$mail->ErrorInfo;
	} else {
		$message = "信件成功寄出";
	}
	$mail->ClearReplyTos();
	$mail->ClearAllRecipients();
	$mail->ClearAttachments();
	
	return $message;
}

//大量寄信
function SendMultiMail($MailInfo) {
	$sql = "Select Init_SMTP_Host, Init_SMTP_Port, Init_SMTP_Email, Init_SMTP_Pw, Init_SMTP_Secure From web_basicset ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_edit($row[$_key]);
	}

	require($MailInfo[root]."class/phpmailer/class.phpmailer.php");
	
	$mail = new PHPMailer();
	$mail->CharSet = "utf-8";			//設定語言編碼
	$mail->Encoding = "base64";			//設定內容編碼方式
	$mail->IsSMTP();					//send via SMTP (是否使用smtp寄信方式) 
	$mail->SMTPAuth = true;				//是否需要 smtp 驗證
	if ($Init_SMTP_Secure!="") {		//gmail的SMTP主機需要使用SSL
		$mail->SMTPSecure = $Init_SMTP_Secure;
	}
	$mail->IsHTML(true);				//信件內容是否使用HTML方式編寫
	
	$mail->Host = $Init_SMTP_Host;		//SMTP Host
	$mail->Port = $Init_SMTP_Port;		//SMTP Port
	$mail->Username = $Init_SMTP_Email;	//SMTP 帳號
	$mail->Password = $Init_SMTP_Pw;	//SMTP 登入密碼
	
	$mail->From = $Init_SMTP_Email;		//寄件人信箱
	//$mail->From = $MailInfo[FromMail];			//寄件人信箱
	$mail->FromName = $MailInfo[FromName];	//寄件人名稱

	//收件人
	$MailInfo[ToMail] = str_replace(";", ",", $MailInfo[ToMail]);
	$mail_array = explode(",", $MailInfo[ToMail]);
	$name_array = explode(",", $MailInfo[ToName]);

	$mail->Subject = $MailInfo[subject];	//郵件主旨
	$MailInfo[body] = "＊提醒您，本信件由系統自動發送，請勿直接回覆，謝謝＊<br /><br />".$MailInfo[body];
	$mail->Body = $MailInfo[body];	//郵件內容
	
	$message = "success";
	$times = 5;	//一次寄多少筆
	for ($i=0; $i<sizeof($mail_array); $i++) {
		if ($name_array[0]!="")  $name_array[$i] = $name_array[0];
		if ($name_array[$i]=="") $name_array[$i] = $mail_array[$i];
		$mail->AddBCC($mail_array[$i]);
		if ($i%$times==0) {
			if (!$mail->Send()) {
				$message = "fail";
				//echo "信件無法寄出，錯誤訊息：".$mail->ErrorInfo;
				//return $message;
			}
			$mail->ClearBCCs();
		}
		
		sleep($MailInfo[TimeSleep]);
	}

	//寄給剩下的收件者
	$ifLast = false;	//是否寄給剩下的收件者
	$remainder = sizeof($mail_array) - (sizeof($mail_array) % $times);
	for ($i=($remainder+1); $i<sizeof($mail_array); $i++) {
		$ifLast = true;
		$mail->AddBCC($mail_array[$i]);
	}
	if ($ifLast) {
		if(!$mail->Send()) {
			$message = "fail";
			//echo "信件無法寄出，錯誤訊息：".$mail->ErrorInfo;
			//return $message;
		}
	}
	$mail->SmtpClose();
	
	return $message;
}

//連結
function getLink($url, $target, $subject) {
	$subject = strip_tags($subject);
	if ($url=="#") $url = "";
	if ($url!="") {
		$link = " href=\"".$url."\"";
		if ($target=="_blank") {
			$link .= " title=\"".$subject."（另開新視窗）\" target=\"_blank\"";
		} else {
			$link .= " title=\"".$subject."\"";
		}
	} else {
		$link = " href=\"javascript:;\" title=\"".$subject."\"";
	}
	
	return $link;
}

//取得產品分類
function __getCategories($web_xx_product_id) {
	
	$sql = "Select web_xx_product.web_xx_product_id, web_xx_product.web_xxx_product_id, web_xx_product.subject as web_xx_product_subject, web_xxx_product.web_xxx_product_id as web_xxx_product_id, web_xxx_product.subject as web_xxx_product_subject From web_xx_product Left Join web_xxx_product On web_xxx_product.web_xxx_product_id = web_xx_product.web_xxx_product_id Where web_xx_product.web_xx_product_id = '".$web_xx_product_id."'";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) {
			$Categories[$_key] = str_front($row[$_key]);
			//echo $_key.":".$Categories[$_key]."<br />";
		}
	}
	return $Categories;
}

//取得產品分類
function _getCategories($web_xxx_product_id) {
	
	$sql = "Select web_xxx_product.web_xxx_product_id, web_xxx_product.web_xxxx_product_id, web_xxx_product.subject as web_xxx_product_subject, web_xxxx_product.web_xxxx_product_id as web_xxxx_product_id, web_xxxx_product.subject as web_xxxx_product_subject, web_xxxx_product.title1, web_xxxx_product.content1 From web_xxx_product Left Join web_xxxx_product On web_xxxx_product.web_xxxx_product_id = web_xxx_product.web_xxxx_product_id Where web_xxx_product.web_xxx_product_id = '".$web_xxx_product_id."'";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) {
			$Categories[$_key] = str_front($row[$_key]);
			//echo $_key.":".$Categories[$_key]."<br />";
		}
	}
	return $Categories;
}

//取得產品分類
function getCategories($web_x_product_id) {
	$sql = "Select web_xxx_product.web_xxx_product_id as web_xxx_product_id, web_xxx_product.subject as web_xxx_product, web_xx_product.web_xx_product_id as web_xx_product_id, web_xx_product.subject as web_xx_product, web_x_product_id, web_x_product.subject as web_x_product, memberLevel From web_x_product JOIN (web_xx_product, web_xxx_product) ON (web_x_product.web_xx_product_id=web_xx_product.web_xx_product_id and web_xx_product.web_xxx_product_id=web_xxx_product.web_xxx_product_id) Where web_x_product_id = '".$web_x_product_id."'";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) {
			$Categories[$_key] = str_front($row[$_key]);
			//echo $_key.":".$Categories[$_key]."<br />";
		}
	}
	return $Categories;
}

//取得會員資訊
function getMember($web_member_id) {
	$Member[nickname] = "訪客"; //$Member[nickname] = "該會員不存在";
	$sql = "Select web_member_id, loginID, level, nickname, uname, email, birthday From web_member Where web_member_id = '".$web_member_id."'";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) {
			$row[birthday] = ($row[birthday] == '0000-00-00') ? '' : $row[birthday]; 
			$Member[$_key] = str_front($row[$_key]);
			//echo $_key.":".$Member[$_key]."<br />";
		}
	}

	$sql = "Select count(*) as counter From web_memberlevel Where web_memberlevel_id = '".$Member[level]."' ";
	$rs = ConnectDB($DB, $sql);
	$counter = mysql_result($rs, 0, "counter");
	if ($counter==0) {	//給予預設值
		$sql = "Update web_member set level = '1' Where web_member_id = '".$web_member_id."'";
		$rs = ConnectDB($DB, $sql);
		
		$Member[level] = 1;
	}

	$sql = "Select subject as level_subject, info as level_info, srange as level_srange, discount as level_discount, ifAllowBonus as level_ifAllowBonus From web_memberlevel Where web_memberlevel_id = '".$Member[level]."' ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) {
			$Member[$_key] = str_front($row[$_key]);
			//echo $_key.":".$Member[$_key]."<br />";
		}
	}
	
	return $Member;
}

//取得後台會員資訊
function getAdmin($web_adminuser_id) {
	
	$sql = "Select web_adminuser_id, loginID, uname From web_adminuser Where web_adminuser_id = '".$web_adminuser_id."'";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) {
			$Admin[$_key] = str_front($row[$_key]);
			//echo $_key.":".$Member[$_key]."<br />";
		}
	}
	
	return $Admin;
}

//確認會員資訊
function chkMember($web_member_id) {
	$sql = "Select * From web_member Where web_member_id = '".$web_member_id."'";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key => $_value) {
			if(empty($_value)) {
				continue;
			}
			$chkMember[$_key] = str_front($row[$_key]);
			//echo $_key.":".$Member[$_key]."<br />";
		}
	}
	
	return count($chkMember);
}

//是否已登入會員
function MemberLogin() {
	$ifLogin = false;
	$web_member_id = intval($_SESSION["MyMember"]["ID"]);
	if ($web_member_id>0) {
		$sql = "Select * From web_member Where web_member_id = '".$web_member_id."' ";
		$rs = ConnectDB($DB, $sql);
		if (mysqli_num_rows($rs)>0) $ifLogin = true;
	}
	return $ifLogin;
}

//取得產品相關資訊
function getProduct($web_product_id) {
	$Product[ifStop] = 1;	//一律先視為暫停銷售
	$sql = "
		Select 
			web_product_id, 
			web_x_product_id, 
			web_x_sale_id, 
			ifTop, 
			ifStop, 
			ifGeneral, 
			ifTip, 
			outlet,
			series, 
			subject, 
			serialnumber, 
			pincode,
			exp, 
			stock, 
			price_cost, 
			price_public, 
			price_member,
			price_today,			
			ifAllowMemberDiscount, 
			discountsdate, 
			discountedate, 
			discountprice, 
			discountnum, 
			dimension, 
			info, 
			title1, 
			content1, 
			tags, 
			Covers, 
			Files, 
			bestsellers, 
			additionalTag,
			showPlace, 
			additionalsdate, 
			additionaledate, 
			additionalprice, 
			bestsellers2 
		From 
			web_product 
		Where 
			web_product_id = '".$web_product_id."' 
		And 
			ifShow = '1' 
		And 
			sdate <= '".date("Y-m-d H:i:s")."' 
		And 
			edate >= '".date("Y-m-d H:i:s")."' 
	";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) {
			$Product[$_key] = str_front($row[$_key]);
			//echo $_key.":".$Product[$_key]."<br />";
		}
		
		$Product[link] = getLink("product".$web_product_id.".html", "_self", $Product[subject]);
		
		$Product[ifTop] = ($Product[ifTop]==1) ? "<span class=\"scheme_color\">【置頂】</span>" : "";
		
		//標註 HOT or SALE--------------------------------------------------------------------------------------------
		switch ($Product[ifTip]) {
			case 1:
				$Product[ifTip] = "<span class=\"sppoint\"><img src=\"images/sppoint2.png\" alt=\"".$TipArray[1]."\"></span>";
				break;
			case 2:
				$Product[ifTip] = "<span class=\"sppoint\"><img src=\"images/sppoint1.png\" alt=\"".$TipArray[2]."\"></span>";
				break;
			default:
				$Product[ifTip] = "";
		}
		
		//價格--------------------------------------------------------------------------------------------------------
		$Product[price] = $Product[price_public];	//尚未登入
		$ifLogin = MemberLogin();
		//if ($ifLogin) $Product[price] = $Product[price_member];	//登入：一般會員價
		if ($ifLogin) {	//已登入
			$Member = getMember(intval($_SESSION["MyMember"]["ID"]));
			if ($Member[level_discount]==100) {	//一般會員
				$Product[price] = $Product[price_member];	//登入：一般會員價
			} else {
				//20170615改折扣會員也是用一般會員價
				$Product[price] = $Product[price_member];	//登入：一般會員價
				//$Product[price] = $Product[price_cost];		//折扣會員：用原價
			}
		}
		//echo $Product[web_x_sale_id]."</br>";
		$saleTitle = null;
		//銷售組合
		$_saleTag[$Product[web_product_id]] = 0;
		if($Product[web_x_sale_id]) {
			$Sale = getSale($Product[web_x_sale_id]);
			//取是否為同一組合全部數量
			//$SaleShoppingCarNum[$Product[web_product_id]] = SearchMultidimensionalArray($_SESSION["MyShopping"], 'saleId', $Product[web_x_sale_id]);			
			if(count($Sale)) {
				//if($SaleShoppingCarNum[$Product[web_product_id]] >= $Sale[rows]) {
					$_saleTag[$Product[web_product_id]] = 1;
					$saleTitle = $Sale[subject];
					$Product[saleTitle] = $saleTitle;
				//}
			}

		}
		
		//echo $_saleTag[$Product[web_product_id]];
		if($_saleTag[$Product[web_product_id]] == 0 && $Member[level_discount] == 100) {    //先看是否在組合銷售中 和是一般會員
			//是否落在折扣區間
			$now = date("Y-m-d H:i:s");
			//if (TimeDiff($Product[discountsdate], $now)>=0 && TimeDiff($now, $Product[discountedate])>=0 && $Product[discountnum]<100) {
			if (TimeDiff($Product[discountsdate], $now)>=0 && TimeDiff($now, $Product[discountedate])>=0) {
				//優先順序  折扣價 > 折扣數
				if($Product[discountprice]) {
					$Product[price] = $Product[discountprice];
					
					//$Product[price_discount_tip] = "<b style='font-size:10pt; color: #CC0066; display: block; background: #efefef; margin: 5px; padding: 7px;'>".substr($Product[discountsdate], 0, 10)." ~ ".substr($Product[discountedate], 0, 10)." 原價".$Product[discountprice]." </b>";	//折扣說明
					$Product[price_discount_tip] = "<b style='font-size:10pt; color: #CC0066; display: block; background: #efefef; margin: 5px; padding: 7px;'>".substr($Product[discountsdate], 0, 10)." ~ ".substr($Product[discountedate], 0, 10)."</b>";	//折扣說明

				} else if($Product[discountnum] < 100 && !$Product[discountprice]) { 	

					$Product[price] = $Product[price_cost];  //2016/10/26有折數用原價去算
					$Product[price] = round($Product[price] * $Product[discountnum] / 100);
					
					$Product[price_discount_tip] = "<b style='font-size:10pt; color: #CC0066; display: block; background: #efefef; margin: 5px; padding: 7px;'>".substr($Product[discountsdate], 0, 10)." ~ ".substr($Product[discountedate],0 , 10)." 原價".($Product[discountnum] / 10)."折 </b>";	//折扣說明
				}

				/*
					round($n)  四捨五入
					ceil($n)   無條件進位
					floor($n)  無條件捨去
				*/
			}
		}
		//會員等級折扣
		if ($Product[ifAllowMemberDiscount]==1) {
			$Product[price_memberlevel_tip] = "<mark> 本產品已列入會員等級折扣 </mark>";	//折扣說明
		}
		
		$Product[price_cost] = number_format($Product[price_cost]);	//原價
		//$Product[price] = number_format($Product[price]);	//售價

		if ($Product[price]<=0) $Product[ifStop] = 1;	//若價格為0也視為暫停銷售
		//價格--------------------------------------------------------------------------------------------------------
		
		//規格--------------------------------------------------------------------------------------------------------
		$dimension_array = explode(",", $Product[dimension]);
		$dimension_list = "";
		$dimension_default = "";
		for ($j=0; $j<sizeof($dimension_array); $j++) {
			if ($dimension_array[$j]!="") {
				if ($dimension_default=="") $dimension_default = $dimension_array[$j];
				$dimension_list .= "<option value=\"".$dimension_array[$j]."\">".$dimension_array[$j]."</option>";
			}
		}
		if ($dimension_list!="") {
			$Product[dimension] = "
			  <select name=\"dimension_".$web_product_id."\" id=\"dimension_".$web_product_id."\">
				".$dimension_list."
			  </select>";
			/*$Product[dimension] = "
                <div class=\"custom_select f_size_medium relative d_inline_middle\">
                  <div class=\"select_title r_corners relative color_dark\">".$dimension_default."</div>
                  <ul class=\"select_list d_none\"></ul>
                  <select name=\"dimension_".$web_product_id."\" id=\"dimension_".$web_product_id."\">
                    ".$dimension_list."
                  </select>
                </div>";*/
		}
		//規格--------------------------------------------------------------------------------------------------------

		$Product[info] = nl2br($Product[info]);	//簡介
		
		//數量--------------------------------------------------------------------------------------------------------
		$Init_Max = 1;	//最大購買量
		$sql2 = "Select Init_Max From web_basicset Where web_basicset_id = '1' ";
		$rs2 = ConnectDB($DB, $sql2);
		if (mysqli_num_rows($rs2)>0) $Init_Max = mysql_result($rs2, 0, "Init_Max");

		$Categories = getCategories($Product[web_x_product_id]);
		$_Categories = _getCategories($Categories[web_xxx_product_id]);
		$web_xxxx_product_id = ($web_xxxx_product_id) ? $web_xxxx_product_id : $_Categories[web_xxxx_product_id];

		$dimension_list = "";
		$dimension_default = "";
		if ($Product[stock]<=0) {	//無庫存
			$Product[ifStop] = 1;	//暫停銷售
		} else {
			if ($Product[stock]<=$Init_Max) $Init_Max = $Product[stock];	//$Product[stock]為庫存量
			if($web_xxxx_product_id == '5') $Init_Max = 1;
			for ($j=1; $j<=$Init_Max; $j++) {
				$num_list .= "<option value=\"".$j."\">".$j."</option>";
			}
		}
		if ($Product[ifStop]!=1) $Product[num] = "
		  <select name=\"num_".$web_product_id."\" id=\"num_".$web_product_id."\" class=\"\">
			".$num_list."
		  </select>";
		/*if ($Product[ifStop]!=1) $Product[num] = "
			<div class=\"custom_select f_size_medium relative d_inline_middle\">
			  <div class=\"select_title r_corners relative color_dark\">1</div>
			  <ul class=\"select_list d_none\"></ul>
			  <select name=\"num_".$web_product_id."\" id=\"num_".$web_product_id."\">
				".$num_list."
			  </select>
			</div>";*/
		//允許之會員等級	
		if($Categories[memberLevel]) {
			$memberLevelArray = explode(",", $Categories["memberLevel"]);	//加購產品
			if($_SESSION[MyMember][level]) {
				if(in_array($_SESSION[MyMember][level], $memberLevelArray)) {
					$Product[memberLevelFlag] = 1;
				} else {
					$Product[memberLevelFlag] = 0;
				}
			}
		} else {
			$Product[memberLevelFlag] = 1;
		}
	


		//數量--------------------------------------------------------------------------------------------------------
		//相關產品----------------------------------------------------------------------------------------------------	
		if($Product[bestsellers]) {
			$bestsellersRow = array_values(array_filter(explode(",", $Product[bestsellers])));
			if(is_array($bestsellersRow)) {
				foreach($bestsellersRow as $row) {
					$sql = "Select web_product_id, subject From web_product Where web_product_id = '".$row."' ";
					$rs = ConnectDB($DB, $sql);
					$row = mysqli_fetch_assoc($rs);

					$subject2 = ($row['subject']) ? explode('-', $row['subject']) : null;
					//$row['subject2'] = ($subject2[1]) ? trim(preg_replace('/[0-9]+/', '',$subject2[1])) : null; 
					$row['subject2'] = ($subject2[1]) ? trim($subject2[1]) : null;
					$num_list2 .= "<option value=\"".$row['web_product_id']."\">".$row['subject2']."</option>";


				}
				$Product[bestsellers] = "
					<select class=\"otherProduct\">
						<option value=\"\">--其他色號--</option>
						".$num_list2."
					</select>";

			}	
		}
		//相關產品----------------------------------------------------------------------------------------------------
		//標籤--------------------------------------------------------------------------------------------------------
		$Product[tags] = trim($Product[tags], ",");
		if ($Product[tags]) {
			$web_tag_array = array();
			$web_tag_array2 = array();
			$sql2 = "Select web_tag_id, subject as web_tag, colorCode, colorCode2 From web_tag Where web_tag_id in (".$Product[tags].") and ifShow = '1' ";
			$rs2 = ConnectDB($DB, $sql2);
			for ($j=0; $j<mysqli_num_rows($rs2); $j++) {
				$row2 = mysqli_fetch_assoc($rs2);
				foreach($row2 as $_key=>$_value) $$_key = str_front($row2[$_key]);
				
				//$web_tag_array[] = "<a href=\"product_list.php?tags=".urlencode($web_tag)."&action=search\" title=\"".$web_tag."\" class=\"color_dark\">".$web_tag."</a>";
				if(strlen($web_tag) > 6)
					$web_tag_array[] = "<div class=\"sale_icon mar_l_5 text-center itemright\" style=\"background-color:".$colorCode2."; color:".$colorCode."\">".$web_tag."</div>";
				else
					$web_tag_array[] = "<div class=\"sale_icon mar_l_5 text-center itemright icon3_p \" style=\"background-color:".$colorCode2."; color:".$colorCode."\">".$web_tag."</div>";

				$web_tag_array2[] = "<div class=\"sale_icon mar_l_5 text-center itemleft pt_sale \" style=\"background-color:".$colorCode2."; color:".$colorCode."\">".$web_tag."</div>";
			}
			//if (mysqli_num_rows($rs2)>0) $Product[tags] = "標籤：".implode("、", $web_tag_array);
			if (mysqli_num_rows($rs2)>0) {
				$Product[tags] = $web_tag_array;
				$Product[tags2] = $web_tag_array2;
			}

		}
		

	}

	//標籤2
	$sql3 = "Select web_hot.web_hot_id, web_hot.web_x_hot_id, web_hot.bestsellers, web_hot.asort, web_x_hot.subject From web_hot Join(web_x_hot) On (web_x_hot.web_x_hot_id = web_hot.web_x_hot_id and web_x_hot.ifShow = '1') Where web_hot.web_x_hot_id != '1' and web_hot.ifShow = '1' ";
	$rs3 = ConnectDB($DB, $sql3);
	for ($j=0; $j<mysqli_num_rows($rs3); $j++) {
		$row3 = mysqli_fetch_assoc($rs3);
		$weekend = date('w');
		if($weekend == "0")
			$weekend = '7';
		//每日必看
		if($row3[web_x_hot_id] == '2'){
			//顯示當天
			if($row3[asort] == $weekend) {
				$bestsellersAry[$row3[web_hot_id]] = explode(",", $row3[bestsellers]);
				$bestsellersTextAry[$row3[web_hot_id]] = ($row3[web_x_hot_id] > 4) ? '熱銷商品' : $row3[subject];
			}
		} else {
			$bestsellersAry[$row3[web_hot_id]] = explode(",", $row3[bestsellers]);
			$bestsellersTextAry[$row3[web_hot_id]] = ($row3[web_x_hot_id] > 4) ? '熱銷商品' : $row3[subject];
		}	
	}
	
	if(count($bestsellersAry)) {
		foreach($bestsellersAry as $key => $row) {
			//echo $web_product_id;
			if(in_array($web_product_id, $row)){
				$newTag[] = ($bestsellersTextAry[$key]) ? $bestsellersTextAry[$key] : null;
			}
		}
	}
	$newTag = array_unique($newTag);
	sort($newTag);
	//每日必看-售價
	if(in_array('每日必看', $newTag)) {
		$Product[price] = ($Product[price_today]) ? $Product[price_today] : $Product[price];
	}
	$Product[newTag] = (count($newTag)) ? $newTag : null;
	$Product[newTagString] = (count($newTag)) ? implode(",", $newTag) : null;
	
	//是否已登入會員
	$ifLogin = MemberLogin();
	if ($ifLogin) {
		$Member = getMember(intval($_SESSION["MyMember"]["ID"]));
		if ($Member[level_discount]==100) $Product[price_memberlevel_tip] = "";
		unset($Member);
	}

	return $Product;
}

//取得產品相關資訊
function getProduct2($web_product_id) {
	$Product[ifStop] = 1;	//一律先視為暫停銷售
	$sql = "
		Select 
			web_product_id, 
			web_x_product_id, 
			web_x_sale_id, 
			ifTop, 
			ifStop, 
			ifGeneral, 
			ifTip, 
			outlet,
			series, 
			subject, 
			serialnumber, 
			pincode,
			exp, 
			stock, 
			price_cost, 
			price_public, 
			price_member,
			price_today,			
			ifAllowMemberDiscount, 
			discountsdate, 
			discountedate, 
			discountprice, 
			discountnum, 
			dimension, 
			info, 
			title1, 
			content1, 
			tags, 
			Covers, 
			Files, 
			bestsellers, 
			additionalTag,
			showPlace, 
			additionalsdate, 
			additionaledate, 
			additionalprice, 
			bestsellers2 
		From 
			web_product 
		Where 
			web_product_id = '".$web_product_id."'  
		And 
			sdate <= '".date("Y-m-d H:i:s")."' 
		And 
			edate >= '".date("Y-m-d H:i:s")."' 
	";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) {
			$Product[$_key] = str_front($row[$_key]);
			//echo $_key.":".$Product[$_key]."<br />";
		}
		
		$Product[link] = getLink("product".$web_product_id.".html", "_self", $Product[subject]);
		
		$Product[ifTop] = ($Product[ifTop]==1) ? "<span class=\"scheme_color\">【置頂】</span>" : "";
		
		//標註 HOT or SALE--------------------------------------------------------------------------------------------
		switch ($Product[ifTip]) {
			case 1:
				$Product[ifTip] = "<span class=\"sppoint\"><img src=\"images/sppoint2.png\" alt=\"".$TipArray[1]."\"></span>";
				break;
			case 2:
				$Product[ifTip] = "<span class=\"sppoint\"><img src=\"images/sppoint1.png\" alt=\"".$TipArray[2]."\"></span>";
				break;
			default:
				$Product[ifTip] = "";
		}
		
		//價格--------------------------------------------------------------------------------------------------------
		$Product[price] = $Product[price_public];	//尚未登入
		$ifLogin = MemberLogin();
		//if ($ifLogin) $Product[price] = $Product[price_member];	//登入：一般會員價
		if ($ifLogin) {	//已登入
			$Member = getMember(intval($_SESSION["MyMember"]["ID"]));
			if ($Member[level_discount]==100) {	//一般會員
				$Product[price] = $Product[price_member];	//登入：一般會員價
			} else {
				$Product[price] = $Product[price_cost];		//折扣會員：用原價
			}
		}
		//echo $Product[web_x_sale_id]."</br>";
		$saleTitle = null;
		//銷售組合
		$_saleTag[$Product[web_product_id]] = 0;
		if($Product[web_x_sale_id]) {
			$Sale = getSale($Product[web_x_sale_id]);
			//取是否為同一組合全部數量
			//$SaleShoppingCarNum[$Product[web_product_id]] = SearchMultidimensionalArray($_SESSION["MyShopping"], 'saleId', $Product[web_x_sale_id]);			
			if(count($Sale)) {
				//if($SaleShoppingCarNum[$Product[web_product_id]] >= $Sale[rows]) {
					$_saleTag[$Product[web_product_id]] = 1;
					$saleTitle = $Sale[subject];
					$Product[saleTitle] = $saleTitle;
				//}
			}

		}
		
		//echo $_saleTag[$Product[web_product_id]];
		if($_saleTag[$Product[web_product_id]] == 0 && $Member[level_discount] == 100) {    //先看是否在組合銷售中 和是一般會員
			//是否落在折扣區間
			$now = date("Y-m-d H:i:s");
			//if (TimeDiff($Product[discountsdate], $now)>=0 && TimeDiff($now, $Product[discountedate])>=0 && $Product[discountnum]<100) {
			if (TimeDiff($Product[discountsdate], $now)>=0 && TimeDiff($now, $Product[discountedate])>=0) {
				//優先順序  折扣價 > 折扣數
				if($Product[discountprice]) {
					$Product[price] = $Product[discountprice];
					
					//$Product[price_discount_tip] = "<b style='font-size:10pt; color: #CC0066; display: block; background: #efefef; margin: 5px; padding: 7px;'>".substr($Product[discountsdate], 0, 10)." ~ ".substr($Product[discountedate], 0, 10)." 原價".$Product[discountprice]." </b>";	//折扣說明
					$Product[price_discount_tip] = "<b style='font-size:10pt; color: #CC0066; display: block; background: #efefef; margin: 5px; padding: 7px;'>".substr($Product[discountsdate], 0, 10)." ~ ".substr($Product[discountedate], 0, 10)."</b>";	//折扣說明

				} else if($Product[discountnum] < 100 && !$Product[discountprice]) { 	

					$Product[price] = $Product[price_cost];  //2016/10/26有折數用原價去算
					$Product[price] = round($Product[price] * $Product[discountnum] / 100);
					
					$Product[price_discount_tip] = "<b style='font-size:10pt; color: #CC0066; display: block; background: #efefef; margin: 5px; padding: 7px;'>".substr($Product[discountsdate], 0, 10)." ~ ".substr($Product[discountedate],0 , 10)." 原價".($Product[discountnum] / 10)."折 </b>";	//折扣說明
				}

				/*
					round($n)  四捨五入
					ceil($n)   無條件進位
					floor($n)  無條件捨去
				*/
			}
		}
		//會員等級折扣
		if ($Product[ifAllowMemberDiscount]==1) {
			$Product[price_memberlevel_tip] = "<mark> 本產品已列入會員等級折扣 </mark>";	//折扣說明
		}
		
		$Product[price_cost] = number_format($Product[price_cost]);	//原價
		//$Product[price] = number_format($Product[price]);	//售價

		if ($Product[price]<=0) $Product[ifStop] = 1;	//若價格為0也視為暫停銷售
		//價格--------------------------------------------------------------------------------------------------------
		
		//規格--------------------------------------------------------------------------------------------------------
		$dimension_array = explode(",", $Product[dimension]);
		$dimension_list = "";
		$dimension_default = "";
		for ($j=0; $j<sizeof($dimension_array); $j++) {
			if ($dimension_array[$j]!="") {
				if ($dimension_default=="") $dimension_default = $dimension_array[$j];
				$dimension_list .= "<option value=\"".$dimension_array[$j]."\">".$dimension_array[$j]."</option>";
			}
		}
		if ($dimension_list!="") {
			$Product[dimension] = "
			  <select name=\"dimension_".$web_product_id."\" id=\"dimension_".$web_product_id."\">
				".$dimension_list."
			  </select>";
			/*$Product[dimension] = "
                <div class=\"custom_select f_size_medium relative d_inline_middle\">
                  <div class=\"select_title r_corners relative color_dark\">".$dimension_default."</div>
                  <ul class=\"select_list d_none\"></ul>
                  <select name=\"dimension_".$web_product_id."\" id=\"dimension_".$web_product_id."\">
                    ".$dimension_list."
                  </select>
                </div>";*/
		}
		//規格--------------------------------------------------------------------------------------------------------

		$Product[info] = nl2br($Product[info]);	//簡介
		
		//數量--------------------------------------------------------------------------------------------------------
		$Init_Max = 1;	//最大購買量
		$sql2 = "Select Init_Max From web_basicset Where web_basicset_id = '1' ";
		$rs2 = ConnectDB($DB, $sql2);
		if (mysqli_num_rows($rs2)>0) $Init_Max = mysql_result($rs2, 0, "Init_Max");

		$Categories = getCategories($Product[web_x_product_id]);
		$_Categories = _getCategories($Categories[web_xxx_product_id]);
		$web_xxxx_product_id = ($web_xxxx_product_id) ? $web_xxxx_product_id : $_Categories[web_xxxx_product_id];

		$dimension_list = "";
		$dimension_default = "";
		if ($Product[stock]<=0) {	//無庫存
			$Product[ifStop] = 1;	//暫停銷售
		} else {
			if ($Product[stock]<=$Init_Max) $Init_Max = $Product[stock];	//$Product[stock]為庫存量
			if($web_xxxx_product_id == '5') $Init_Max = 1;
			for ($j=1; $j<=$Init_Max; $j++) {
				$num_list .= "<option value=\"".$j."\">".$j."</option>";
			}
		}
		if ($Product[ifStop]!=1) $Product[num] = "
		  <select name=\"num_".$web_product_id."\" id=\"num_".$web_product_id."\" class=\"form-control\">
			".$num_list."
		  </select>";
		/*if ($Product[ifStop]!=1) $Product[num] = "
			<div class=\"custom_select f_size_medium relative d_inline_middle\">
			  <div class=\"select_title r_corners relative color_dark\">1</div>
			  <ul class=\"select_list d_none\"></ul>
			  <select name=\"num_".$web_product_id."\" id=\"num_".$web_product_id."\">
				".$num_list."
			  </select>
			</div>";*/
		//允許之會員等級	
		if($Categories[memberLevel]) {
			$memberLevelArray = explode(",", $Categories["memberLevel"]);	//加購產品
			if($_SESSION[MyMember][level]) {
				if(in_array($_SESSION[MyMember][level], $memberLevelArray)) {
					$Product[memberLevelFlag] = 1;
				} else {
					$Product[memberLevelFlag] = 0;
				}
			}
		} else {
			$Product[memberLevelFlag] = 1;
		}
	


		//數量--------------------------------------------------------------------------------------------------------
		//相關產品----------------------------------------------------------------------------------------------------	
		if($Product[bestsellers]) {
			$bestsellersRow = array_values(array_filter(explode(",", $Product[bestsellers])));
			if(is_array($bestsellersRow)) {
				foreach($bestsellersRow as $row) {
					$sql = "Select web_product_id, subject From web_product Where web_product_id = '".$row."' ";
					$rs = ConnectDB($DB, $sql);
					$row = mysqli_fetch_assoc($rs);

					$subject2 = ($row['subject']) ? explode('-', $row['subject']) : null;
					//$row['subject2'] = ($subject2[1]) ? trim(preg_replace('/[0-9]+/', '',$subject2[1])) : null; 
					$row['subject2'] = ($subject2[1]) ? trim($subject2[1]) : null;
					$num_list2 .= "<option value=\"".$row['web_product_id']."\">".$row['subject2']."</option>";


				}
				$Product[bestsellers] = "
					<select class=\"otherProduct\">
						<option value=\"\">--其他色號--</option>
						".$num_list2."
					</select>";

			}	
		}
		//相關產品----------------------------------------------------------------------------------------------------
		//標籤--------------------------------------------------------------------------------------------------------
		$Product[tags] = trim($Product[tags], ",");
		if ($Product[tags]) {
			$web_tag_array = array();
			$web_tag_array2 = array();
			$sql2 = "Select web_tag_id, subject as web_tag, colorCode, colorCode2 From web_tag Where web_tag_id in (".$Product[tags].") and ifShow = '1' ";
			$rs2 = ConnectDB($DB, $sql2);
			for ($j=0; $j<mysqli_num_rows($rs2); $j++) {
				$row2 = mysqli_fetch_assoc($rs2);
				foreach($row2 as $_key=>$_value) $$_key = str_front($row2[$_key]);
				
				//$web_tag_array[] = "<a href=\"product_list.php?tags=".urlencode($web_tag)."&action=search\" title=\"".$web_tag."\" class=\"color_dark\">".$web_tag."</a>";
				if(strlen($web_tag) > 6)
					$web_tag_array[] = "<div class=\"sale_icon mar_l_5 text-center itemright\" style=\"background-color:".$colorCode2."; color:".$colorCode."\">".$web_tag."</div>";
				else
					$web_tag_array[] = "<div class=\"sale_icon mar_l_5 text-center itemright icon3_p \" style=\"background-color:".$colorCode2."; color:".$colorCode."\">".$web_tag."</div>";

				$web_tag_array2[] = "<div class=\"sale_icon mar_l_5 text-center itemleft pt_sale \" style=\"background-color:".$colorCode2."; color:".$colorCode."\">".$web_tag."</div>";
			}
			//if (mysqli_num_rows($rs2)>0) $Product[tags] = "標籤：".implode("、", $web_tag_array);
			if (mysqli_num_rows($rs2)>0) {
				$Product[tags] = $web_tag_array;
				$Product[tags2] = $web_tag_array2;
			}

		}
		

	}

	//標籤2
	$sql3 = "Select web_hot.web_hot_id, web_hot.web_x_hot_id, web_hot.bestsellers, web_hot.asort, web_x_hot.subject From web_hot Join(web_x_hot) On (web_x_hot.web_x_hot_id = web_hot.web_x_hot_id and web_x_hot.ifShow = '1') Where web_hot.web_x_hot_id != '1' and web_hot.ifShow = '1' ";
	$rs3 = ConnectDB($DB, $sql3);
	for ($j=0; $j<mysqli_num_rows($rs3); $j++) {
		$row3 = mysqli_fetch_assoc($rs3);
		$weekend = date('w');
		if($weekend == "0")
			$weekend = '7';
		//每日必看
		if($row3[web_x_hot_id] == '2'){
			//顯示當天
			if($row3[asort] == $weekend) {
				$bestsellersAry[$row3[web_hot_id]] = explode(",", $row3[bestsellers]);
				$bestsellersTextAry[$row3[web_hot_id]] = ($row3[web_x_hot_id] > 4) ? '熱銷商品' : $row3[subject];
			}
		} else {
			$bestsellersAry[$row3[web_hot_id]] = explode(",", $row3[bestsellers]);
			$bestsellersTextAry[$row3[web_hot_id]] = ($row3[web_x_hot_id] > 4) ? '熱銷商品' : $row3[subject];
		}	
	}
	
	if(count($bestsellersAry)) {
		foreach($bestsellersAry as $key => $row) {
			//echo $web_product_id;
			if(in_array($web_product_id, $row)){
				$newTag[] = ($bestsellersTextAry[$key]) ? $bestsellersTextAry[$key] : null;
			}
		}
	}
	$newTag = array_unique($newTag);
	sort($newTag);
	//每日必看-售價
	if(in_array('每日必看', $newTag)) {
		$Product[price] = ($Product[price_today]) ? $Product[price_today] : $Product[price];
	}
	$Product[newTag] = (count($newTag)) ? $newTag : null;
	$Product[newTagString] = (count($newTag)) ? implode(",", $newTag) : null;
	
	//是否已登入會員
	$ifLogin = MemberLogin();
	if ($ifLogin) {
		$Member = getMember(intval($_SESSION["MyMember"]["ID"]));
		if ($Member[level_discount]==100) $Product[price_memberlevel_tip] = "";
		unset($Member);
	}

	return $Product;
}

//取得加購產品相關資訊
function getProductAdditional($web_product_id) {
	$Product[ifStop] = 1;	//一律先視為暫停銷售
	$sql = "Select web_product_id, web_x_product_id, ifTop, ifStop, ifGeneral, ifTip, subject, serialnumber, pincode, stock, price_cost, price_public, price_member, ifAllowMemberDiscount, discountsdate, discountedate, discountprice, discountnum, dimension, info, tags, Covers, Files, bestsellers, additionalTag, additionalsdate, additionaledate, additionalprice, bestsellers2 From web_product Where web_product_id = '".$web_product_id."' and additionalTag = '1' and sdate <= '".date("Y-m-d H:i:s")."' and edate >= '".date("Y-m-d H:i:s")."' ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) {
			$Product[$_key] = str_front($row[$_key]);
			//echo $_key.":".$Product[$_key]."<br />";
		}
		
		$Product[link] = getLink("product_detail.php?id=".$web_product_id, "_self", $Product[subject]);
		
		$Product[ifTop] = ($Product[ifTop]==1) ? "<span class=\"scheme_color\">【置頂】</span>" : "";
		
		//標註 HOT or SALE--------------------------------------------------------------------------------------------
		switch ($Product[ifTip]) {
			case 1:
				$Product[ifTip] = "<span class=\"hot_stripe\"><img src=\"images/hot_product.png\" alt=\"".$TipArray[1]."\"></span>";
				break;
			case 2:
				$Product[ifTip] = "<span class=\"hot_stripe\"><img src=\"images/sale_product.png\" alt=\"".$TipArray[2]."\"></span>";
				break;
			default:
				$Product[ifTip] = "";
		}
		
		//價格--------------------------------------------------------------------------------------------------------
		$Product[price] = $Product[price_public];	//尚未登入
		$ifLogin = MemberLogin();
		//if ($ifLogin) $Product[price] = $Product[price_member];	//登入：一般會員價
		if ($ifLogin) {	//已登入
			$Member = getMember(intval($_SESSION["MyMember"]["ID"]));
			if ($Member[level_discount]==100) {	//一般會員
				$Product[price] = $Product[price_member];	//登入：一般會員價
			} else {
				$Product[price] = $Product[price_cost];		//折扣會員：用原價
			}
		}
		
		//是否落在折扣區間
		$now = date("Y-m-d H:i:s");
		//if (TimeDiff($Product[discountsdate], $now)>=0 && TimeDiff($now, $Product[discountedate])>=0 && $Product[discountnum]<100) {
		if (TimeDiff($Product[discountsdate], $now)>=0 && TimeDiff($now, $Product[discountedate])>=0) {
			//優先順序  折扣價 > 折扣數
			if($Product[discountprice]) {
				$Product[price] = $Product[discountprice];
				
				//$Product[price_discount_tip] = "<mark> ".$Product[discountsdate]." ~ ".$Product[discountedate]." 特價".$Product[discountprice]." </mark>";	//折扣說明

			} else if($Product[discountnum] < 100 && !$Product[discountprice]) { 	
				$Product[price] = round($Product[price] * $Product[discountnum] / 100);
				
				//$Product[price_discount_tip] = "<mark> ".$Product[discountsdate]." ~ ".$Product[discountedate]." 特價".$Product[discountnum]."折 </mark>";	//折扣說明
			}

			/*
				round($n)  四捨五入
				ceil($n)   無條件進位
				floor($n)  無條件捨去
			*/
		}
		
		//會員等級折扣
		if ($Product[ifAllowMemberDiscount]==1) {
			$Product[price_memberlevel_tip] = "<mark> 本產品已列入會員等級折扣 </mark>";	//折扣說明
		}
		
		$Product[price_cost] = number_format($Product[price_cost]);	//原價
		//$Product[price] = number_format($Product[price]);	//售價
		
		if ($Product[price]<=0) $Product[ifStop] = 1;	//若價格為0也視為暫停銷售
		//價格--------------------------------------------------------------------------------------------------------
		
		//規格--------------------------------------------------------------------------------------------------------
		$dimension_array = explode(",", $Product[dimension]);
		$dimension_list = "";
		$dimension_default = "";
		for ($j=0; $j<sizeof($dimension_array); $j++) {
			if ($dimension_array[$j]!="") {
				if ($dimension_default=="") $dimension_default = $dimension_array[$j];
				$dimension_list .= "<option value=\"".$dimension_array[$j]."\">".$dimension_array[$j]."</option>";
			}
		}
		if ($dimension_list!="") {
			$Product[dimension] = "
				<select name=\"dimension_".$web_product_id."\" id=\"dimension_".$web_product_id."\">
				".$dimension_list."
				</select>";
			/*$Product[dimension] = "
                <div class=\"custom_select f_size_medium relative d_inline_middle\">
                  <div class=\"select_title r_corners relative color_dark\">".$dimension_default."</div>
                  <ul class=\"select_list d_none\"></ul>
                  <select name=\"dimension_".$web_product_id."\" id=\"dimension_".$web_product_id."\">
                    ".$dimension_list."
                  </select>
                </div>";*/
		}
		//規格--------------------------------------------------------------------------------------------------------

		$Product[info] = nl2br($Product[info]);	//簡介
		
		//數量--------------------------------------------------------------------------------------------------------
		$Init_Max = 1;	//最大購買量
		$sql2 = "Select Init_Max From web_basicset Where web_basicset_id = '1' ";
		$rs2 = ConnectDB($DB, $sql2);
		if (mysqli_num_rows($rs2)>0) $Init_Max = mysql_result($rs2, 0, "Init_Max");

		$dimension_list = "";
		$dimension_default = "";
		if ($Product[stock]<=0) {	//無庫存
			$Product[ifStop] = 1;	//暫停銷售
		} else {
			if ($Product[stock]<=$Init_Max) $Init_Max = $Product[stock];	//$Product[stock]為庫存量
			for ($j=1; $j<=$Init_Max; $j++) {
				$num_list .= "<option value=\"".$j."\">".$j."</option>";
			}
		}
		if ($Product[ifStop]!=1) $Product[num] = "
		  <select name=\"num_".$web_product_id."\" id=\"num_".$web_product_id."\">
			".$num_list."
		  </select>";
		/*if ($Product[ifStop]!=1) $Product[num] = "
			<div class=\"custom_select f_size_medium relative d_inline_middle\">
			  <div class=\"select_title r_corners relative color_dark\">1</div>
			  <ul class=\"select_list d_none\"></ul>
			  <select name=\"num_".$web_product_id."\" id=\"num_".$web_product_id."\">
				".$num_list."
			  </select>
			</div>";*/
		//數量--------------------------------------------------------------------------------------------------------
		//相關產品----------------------------------------------------------------------------------------------------	
		if($Product[bestsellers]) {
			$bestsellersRow = array_values(array_filter(explode(",", $Product[bestsellers])));
			if(is_array($bestsellersRow)) {
				foreach($bestsellersRow as $row) {
					$sql = "Select web_product_id, subject From web_product Where web_product_id = '".$row."' ";
					$rs = ConnectDB($DB, $sql);
					$row = mysqli_fetch_assoc($rs);

					$subject2 = ($row['subject']) ? explode('-', $row['subject']) : null;
					//$row['subject2'] = ($subject2[1]) ? trim(preg_replace('/[0-9]+/', '',$subject2[1])) : null; 
					$row['subject2'] = ($subject2[1]) ? trim($subject2[1]) : null;
					$num_list2 .= "<option value=\"".$row['web_product_id']."\">".$row['subject2']."</option>";


				}
				$Product[bestsellers] = "
					  <select class=\"otherProduct\">
					    <option value=\"\">--其他色號--</option>
						".$num_list2."
					  </select>";

			}	
		}
		//相關產品----------------------------------------------------------------------------------------------------
		//標籤--------------------------------------------------------------------------------------------------------
		$Product[tags] = trim($Product[tags], ",");
		if ($Product[tags]) {
			$web_tag_array = array();
			$sql2 = "Select web_tag_id, subject as web_tag From web_tag Where web_tag_id in (".$Product[tags].") and ifShow = '1' ";
			$rs2 = ConnectDB($DB, $sql2);
			for ($j=0; $j<mysqli_num_rows($rs2); $j++) {
				$row2 = mysqli_fetch_assoc($rs2);
				foreach($row2 as $_key=>$_value) $$_key = str_front($row2[$_key]);
				
				$web_tag_array[] = "<a href=\"product_list.php?tags=".urlencode($web_tag)."&action=search\" title=\"".$web_tag."\" class=\"color_dark\">".$web_tag."</a>";

			}
			if (mysqli_num_rows($rs2)>0) $Product[tags] = "標籤：".implode("、", $web_tag_array);
		}
	}

	//是否已登入會員
	$ifLogin = MemberLogin();
	if ($ifLogin) {
		$Member = getMember(intval($_SESSION["MyMember"]["ID"]));
		if ($Member[level_discount]==100) $Product[price_memberlevel_tip] = "";
		unset($Member);
	}

	return $Product;
}

//取得組合銷售相關資訊
function getSale($web_x_sale_id) {
	$ifLogin = MemberLogin();
	$Member = getMember(intval($_SESSION["MyMember"]["ID"]));
	//只有一般會員可以有組合
	//mk為皆可
	//if ($Member[level_discount] == 100 || !$ifLogin) {  
		$sql = "Select web_x_sale_id, ifDefault, eventType, subject, rows, price, discountnum, sdate, edate From web_x_sale WHERE web_x_sale_id = '".$web_x_sale_id."'";
		$rs = ConnectDB($DB, $sql);
		for ($i=0; $i<mysqli_num_rows($rs); $i++) {
			$row = mysqli_fetch_assoc($rs);
			
			foreach($row as $_key => $_value) {
				//echo $row[$_key]."</br>";
				if (TimeDiff($row[sdate], date('Y-m-d H:i:s'))>=0 && TimeDiff(date('Y-m-d H:i:s'), $row[edate])>=0) {
				//if (TimeDiff(date('Y-m-d H:i:s'), $row[edate])>=0) {	
					$Sale[$_key] = str_front($row[$_key]);
				}
			}
			
		}
	//}
	
	return $Sale;
}

//取得組合銷售相關資訊(後台預先上資料使用)
function getSaleBackend($web_x_sale_id) {
	$ifLogin = MemberLogin();
	$Member = getMember(intval($_SESSION["MyMember"]["ID"]));
	//只有一般會員可以有組合
	//mk為皆可
	//if ($Member[level_discount] == 100 || !$ifLogin) {  
		$sql = "Select web_x_sale_id, ifDefault, eventType, subject, rows, price, discountnum, sdate, edate From web_x_sale WHERE web_x_sale_id = '".$web_x_sale_id."'";
		$rs = ConnectDB($DB, $sql);
		for ($i=0; $i<mysqli_num_rows($rs); $i++) {
			$row = mysqli_fetch_assoc($rs);
			
			foreach($row as $_key => $_value) {
				//echo $row[$_key]."</br>";
				//if (TimeDiff($row[sdate], date('Y-m-d H:i:s'))>=0 && TimeDiff(date('Y-m-d H:i:s'), $row[edate])>=0) {
				if (TimeDiff(date('Y-m-d H:i:s'), $row[edate])>=0) {	
					$Sale[$_key] = str_front($row[$_key]);
				}
			}
			
		}
	//}
	
	return $Sale;
}

//取得影片相關資訊
function getVideo($web_video_id) {
	$sql = "Select web_video_id, ifTop, subject From web_video Where web_video_id = '".$web_video_id."' and ifShow = '1' ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) {
			$Video[$_key] = str_front($row[$_key]);
			//echo $_key.":".$Product[$_key]."<br />";
		}
		
		//$Product[link] = getLink("product_detail.php?id=".$web_product_id, "_self", $Product[subject]);
		
		//$Product[ifTop] = ($Product[ifTop]==1) ? "<span class=\"scheme_color\">【置頂】</span>" : "";
	}	

	return $Video;
}

//快速檢視
function QuickView($web_product_id) {
	$sql = "Select web_product.* From web_product JOIN (web_x_product, web_xx_product, web_xxx_product) ON (web_product.web_x_product_id=web_x_product.web_x_product_id and web_x_product.web_xx_product_id=web_xx_product.web_xx_product_id and web_xx_product.web_xxx_product_id=web_xxx_product.web_xxx_product_id) Where web_xxx_product.ifShow = '1' and web_xx_product.ifShow = '1' and web_x_product.ifShow = '1' and web_product.ifShow = '1'  and web_product.outlet != '1' and sdate <= '".date("Y-m-d H:i:s")."' and edate >= '".date("Y-m-d H:i:s")."' and web_product_id = '".$web_product_id."' ";
	$rs = ConnectDB($DB, $sql);
	if (mysqli_num_rows($rs)==0) return;
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);

		$Product = getProduct($web_product_id);	//取得產品相關資訊
	}
	
	$link = getLink("product_detail.php?id=".$web_product_id, "_self", $Product[subject]);

	if ($Product[ifGeneral]==1) {	//一般
		//分類名稱
		$Categories = getCategories($Product[web_x_product_id]);
		foreach($Categories as $_key=>$_value) $$_key = $Categories[$_key];
		$web_x_product_list = $web_xxx_product." > ".$web_xx_product." > ".$web_x_product;
		$web_x_product_link = "<a href=\"product_list.php?catalog=".$web_xxx_product_id."\" title=\"".$web_xxx_product."\">".$web_xxx_product."</a> > ".$web_xx_product." > <a href=\"product_list.php?kind=".$web_x_product_id."\" title=\"".$web_x_product_list."\">".$web_x_product."</a>";
	}
	
	//簡介
	if ($Product[info]) {
		$info = "<p class=\"m_bottom_10\">".$Product[info]."</p><hr class=\"divider_type_3 m_bottom_15\">";
	}

	//圖片
	$big = "";
	$pic_list = "";
	$Files = explode("/", $Product[Files]);
	for ($j=0; $j<sizeof($Files); $j++) {
		$pic_m = ShowPic($Files[$j], "uploadfiles/m/", "");
		$pic_s = ShowPic($Files[$j], "uploadfiles/s/", "");
		
		if ($pic_m!="" && $pic_s!="") {
			if ($big=="") $big = $pic_m;	//大圖
			$pic_list .= "<li data-src=\"".$pic_m."\"><img src=\"".$pic_s."\" alt=\"".$Product[subject]."\"></li>";	//小圖
		}
	}
	if ($big=="") $big = ShowPic($Product[Covers], "uploadfiles/l/", "uploadfiles/no_image.jpg");
	if ($big=="") $big = "uploadfiles/logo.jpg";

	$list = "
<div class=\"popup_wrap d_none\" id=\"QuickView".$web_product_id."\">
  <section class=\"popup r_corners shadow\">
    <button class=\"bg_tr color_dark tr_all_hover text_cs_hover close f_size_large\"><i class=\"fa fa-times\"></i></button>
    <div class=\"clearfix\">
      <div class=\"custom_scrollbar\"> 
        <div class=\"f_left half_column\">
          <div class=\"relative d_inline_b m_bottom_10 qv_preview\">
            ".$Product[ifTip]."
            <img src=\"".$big."\" class=\"tr_all_hover\" width=\"360\" alt=\"".$Product[subject]."\"><!--大圖-->
          </div>";
	
	//小圖
	if ($pic_list!="" && 1==2) $list .= "
          <div class=\"relative qv_carousel_wrap m_bottom_20\">
            <button class=\"button_type_11 t_align_c f_size_ex_large bg_cs_hover r_corners d_inline_middle bg_tr tr_all_hover qv_btn_prev\"><i class=\"fa fa-angle-left \"></i></button><!-- < -->
            <ul class=\"qv_carousel d_inline_middle\">
              ".$pic_list."<!--小圖-->
            </ul>
            <button class=\"button_type_11 t_align_c f_size_ex_large bg_cs_hover r_corners d_inline_middle bg_tr tr_all_hover qv_btn_next\"><i class=\"fa fa-angle-right \"></i></button><!-- > -->
          </div>";
	
	//名稱
	$list .= "
        </div>
        <div class=\"f_right half_column\"> 
          <h2 class=\"m_bottom_10\">".$Product[subject]."</h2>
		  <div class=\"f_size_small scheme_color m_bottom_10\">
			".$web_x_product_link."
		  </div>
		  <div class=\"fw_medium m_bottom_10\">
		    ".$Product[serialnumber]."
		  </div>
		  <div class=\"m_bottom_10\">
		    ".$Product[price_discount_tip]."
		  </div>
          <hr class=\"divider_type_3 m_bottom_15\">          
          ".$info."
          <div class=\"m_bottom_15\">
            <s class=\"v_align_b f_size_ex_large\">$".$Product[price_cost]."</s>　
            <span class=\"v_align_b f_size_big m_left_5 scheme_color fw_medium\">$".$Product[price]."</span>
          </div>
          <table class=\"description_table type_2 m_bottom_15\">";

	//規格
	if ($Product[dimension]) $list .= "
            <tr>
              <td class=\"v_align_m\">規格:</td>
              <td class=\"v_align_m\">
                ".$Product[dimension]."
              </td>
            </tr>";

	//數量
	if ($Product[num]) $list .= "
            <tr>
              <td class=\"v_align_m\">數量:</td>
              <td class=\"v_align_m\">
                ".$Product[num]."
              </td>
            </tr>";	
	if ($Product[bestsellers]) $list .= " 
            <tr>
              <td class=\"v_align_m\">相關產品：</td>
              <td class=\"v_align_m\">".$Product[bestsellers]."</td>
            </tr>";	

	if ($Product[ifStop]==1) {	//暫停銷售
		$BuyButton = "<button class=\"button_type_12 r_corners color_dark bg_light_color_2 f_left f_size_large\">暫停銷售</button>";
	} else {
		$BuyButton = "<button class=\"button_type_12 r_corners bg_scheme_color color_light tr_delay_hover f_left f_size_large\" onClick=\"AddShoppingCar(this, ".$web_product_id.");\">我要購買</button>";		
	}

	$list .= "
          </table>
          <div class=\"clearfix m_bottom_15\">
            ".$BuyButton."";
	
	//一般
	if ($Product[ifGeneral]==1) $list .= "
			<button class=\"tr_delay_hover r_corners button_type_12 bg_dark_color bg_cs_hover color_light f_left m_left_5 f_size_large\" onClick=\"location.href='product_detail.php?id=".$Product[web_product_id]."'\">更多資訊</button>
			<button class=\"button_type_12 bg_light_color_2 tr_delay_hover f_left r_corners color_dark m_left_5 p_hr_0\" onClick=\"AddWishlist(this, ".$web_product_id.");\"><i class=\"fa fa-heart-o f_size_big\"></i><span class=\"tooltip tr_all_hover r_corners color_dark f_size_small\"><span class=\"wishlist_tip_".$web_product_id."\">加入最愛</span></span></button>";		

	$list .= "
          </div>
		  <div class=\"m_bottom_20\">
		    ".$Product[tags]."
		  </div>
		  <span class=\"f_size_large scheme_color fw_medium shopping_tip_".$web_product_id."\"></span>
        </div>
      </div>
    </div>
  </section>
</div>";
	return $list;
}

//快速檢視2
function QuickView2($web_product_id) {
	$sql = "Select web_product.* From web_product JOIN (web_x_product, web_xx_product, web_xxx_product) ON (web_product.web_x_product_id=web_x_product.web_x_product_id and web_x_product.web_xx_product_id=web_xx_product.web_xx_product_id and web_xx_product.web_xxx_product_id=web_xxx_product.web_xxx_product_id) Where web_xxx_product.ifShow = '1' and web_xx_product.ifShow = '1' and web_x_product.ifShow = '1' and web_product.ifShow = '1' and sdate <= '".date("Y-m-d H:i:s")."' and edate >= '".date("Y-m-d H:i:s")."' and web_product_id = '".$web_product_id."' ";
	$rs = ConnectDB($DB, $sql);
	if (mysqli_num_rows($rs)==0) return;
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);

		$Product = getProduct($web_product_id);	//取得產品相關資訊
	}
	
	$link = getLink("product_detail.php?id=".$web_product_id, "_self", $Product[subject]);
	if ($Product[ifGeneral]==1) {	//一般
		//分類名稱
		$Categories = getCategories($Product[web_x_product_id]);
		foreach($Categories as $_key=>$_value) $$_key = $Categories[$_key];
		$web_x_product_list = $web_xxx_product." > ".$web_xx_product." > ".$web_x_product;
		$web_x_product_link = "<a href=\"product_list.php?catalog=".$web_xxx_product_id."\" title=\"".$web_xxx_product."\">".$web_xxx_product."</a> > ".$web_xx_product." > <a href=\"product_list.php?kind=".$web_x_product_id."\" title=\"".$web_x_product_list."\">".$web_x_product."</a>";
	}
	
	//簡介
	if ($Product[info]) {
		$info = "<p class=\"m_bottom_10\">".$Product[info]."</p><hr class=\"divider_type_3 m_bottom_15\">";
	}

	//圖片
	$big = "";
	$pic_list = "";
	$Files = explode("/", $Product[Files]);
	for ($j=0; $j<sizeof($Files); $j++) {
		$pic_m = ShowPic($Files[$j], "uploadfiles/m/", "");
		$pic_s = ShowPic($Files[$j], "uploadfiles/s/", "");
		
		if ($pic_m!="" && $pic_s!="") {
			if ($big=="") $big = $pic_m;	//大圖
			$pic_list .= "<li data-src=\"".$pic_m."\"><img src=\"".$pic_s."\" alt=\"".$Product[subject]."\"></li>";	//小圖
		}
	}
	if ($big=="") $big = ShowPic($Product[Covers], "uploadfiles/l/", "uploadfiles/no_image.jpg");
	if ($big=="") $big = "uploadfiles/logo.jpg";

	$list = "
<div class=\"popup_wrap d_none\" id=\"QuickView".$web_product_id."\">
  <section class=\"popup r_corners shadow\">
    <button class=\"bg_tr color_dark tr_all_hover text_cs_hover close f_size_large\"><i class=\"fa fa-times\"></i></button>
    <div class=\"clearfix\">
      <div class=\"custom_scrollbar\"> 
        <div class=\"f_left half_column\">
          <div class=\"relative d_inline_b m_bottom_10 qv_preview\">
            ".$Product[ifTip]."
            <img src=\"".$big."\" class=\"tr_all_hover\" width=\"360\" alt=\"".$Product[subject]."\"><!--大圖-->
          </div>";
	
	//小圖
	if ($pic_list!="" && 1==2) $list .= "
          <div class=\"relative qv_carousel_wrap m_bottom_20\">
            <button class=\"button_type_11 t_align_c f_size_ex_large bg_cs_hover r_corners d_inline_middle bg_tr tr_all_hover qv_btn_prev\"><i class=\"fa fa-angle-left \"></i></button><!-- < -->
            <ul class=\"qv_carousel d_inline_middle\">
              ".$pic_list."<!--小圖-->
            </ul>
            <button class=\"button_type_11 t_align_c f_size_ex_large bg_cs_hover r_corners d_inline_middle bg_tr tr_all_hover qv_btn_next\"><i class=\"fa fa-angle-right \"></i></button><!-- > -->
          </div>";
	
	//名稱
	$list .= "
        </div>
        <div class=\"f_right half_column\"> 
          <h2 class=\"m_bottom_10\">".$Product[subject]."</h2>
		  <div class=\"f_size_small scheme_color m_bottom_10\">
			".$web_x_product_link."
		  </div>
		  <div class=\"fw_medium m_bottom_10\">
		    ".$Product[serialnumber]."
		  </div>
          <hr class=\"divider_type_3 m_bottom_15\">          
          ".$info."
          <div class=\"m_bottom_15\">
            <s class=\"v_align_b f_size_ex_large\">會員價 $".$Product[price]."</s>　
            <span class=\"v_align_b f_size_big m_left_5 scheme_color fw_medium\">加購價 $".$Product[additionalprice]."</span>
          </div>
		  <span class=\"f_size_large scheme_color fw_medium shopping_tip_".$web_product_id."\"></span>
        </div>
      </div>
    </div>
  </section>
</div>";
	return $list;
}

//小計
function CalSubtotal() {
	$total = 0;	//小計
	$allow_total = 0;	//允許會員等級折扣的產品金額小計
	$totalAdditional = 0;	//加購小計

	$saleAllNum = getSaleAllNum();
	$surplusNum = 0;
	for ($i=0; $i<count($_SESSION["MyShopping"]); $i++) {
		$web_product_id = $_SESSION["MyShopping"][$i]["web_product_id"];
		$web_x_sale_id = $_SESSION["MyShopping"][$i]["saleId"];
		$num = $_SESSION["MyShopping"][$i]["num"];
		
		$Product = getProduct($web_product_id);	//取得產品相關資訊

		//$Product[price] = ($_SESSION["MyShopping"][$i]["additional"]) ? $Product[additionalprice] : $Product[price];    //為加購 顯示加購價
		$showFlag = 0;
		//銷售組合
		if($Product[web_x_sale_id] && !$_SESSION["MyShopping"][$i]["additional"]) {
			$Sale = getSale($Product[web_x_sale_id]);
			//echo $Product[web_x_sale_id]."</br>";
			//取是否為同一組合全部數量
			$SaleShoppingCarNum = SearchMultidimensionalArray_2($_SESSION["MyShopping"], 'saleId', $Product[web_x_sale_id]);
			if(count($Sale)) {
				if($Sale[eventType] != '2') {	//是否為免運
					//if($num % $Sale[rows] == 0 || $SaleShoppingCarNum > $Sale[rows] == 0) {
					if($num % $Sale[rows] == 0 || $SaleShoppingCarNum >= $Sale[rows]) {	
						if($Sale[price]) {

							//$Product[price] = floor($Sale[price] / $Sale[rows]);
							$saleTitle = $Sale['subject'];
							$saleTotal[$Product[web_x_sale_id]] += ($Product[price] * $num); 
							$saleNum[$Product[web_x_sale_id]] += $num;
							if($saleNum[$Product[web_x_sale_id]] >= $Sale[rows]) {
								$_minusRow = $saleNum[$Product[web_x_sale_id]] % $Sale[rows];
								$_minus = ($Product[price] * $_minusRow);
							} 

							$saleAry[$Product[web_x_sale_id]] = array(
								'id'			=>	$Product[web_x_sale_id],
								'saleRow'		=>	$Sale[rows],
								'salePrice'		=>	$Sale[price],
								'title'			=>	$saleTitle,
								'num'			=> 	$saleNum[$Product[web_x_sale_id]],
								'total'			=>	$saleTotal[$Product[web_x_sale_id]] - $_minus,

							);

						} else if($Sale[discountnum] < 100 && !$Sale[price]) { 	

							//$Product[price] = round($Product[price] * $Sale[discountnum] / 100);
							//$Product[price] = $Product[price_cost];  //2016/10/26有折數用原價去算
							//$Product[price] = (($Sale[rows] % $num == 0) && ($num >= $Sale[rows])) ? $Product[price_cost] : $Product[price];
							
							//if($SaleShoppingCarNum % $Sale[rows] == 1 && $num % $Sale[rows] == 2) {
							//if($SaleShoppingCarNum % $Sale[rows] == 1 && $num % $Sale[rows] == 2) {
							//if($SaleShoppingCarNum % $Sale[rows] != 0) {
							if(1) {
								

								if($saleAllNum[$Product[web_x_sale_id]]) {
									//echo $Sale[rows].",";
									$numAry2[$Product[web_x_sale_id]][$web_product_id] = $num;
									$SaleRows[$Product[web_x_sale_id]] = $Sale[rows];
									$numAry[] = $num;
									$saleAllNum[$Product[web_x_sale_id]] = ($saleAllNum[$Product[web_x_sale_id]] >= $SaleRows[$Product[web_x_sale_id]]) ? $saleAllNum[$Product[web_x_sale_id]] - ($saleAllNum[$Product[web_x_sale_id]] % $SaleRows[$Product[web_x_sale_id]]) : $saleAllNum[$Product[web_x_sale_id]];
									
									if(array_sum($numAry2[$Product[web_x_sale_id]]) <= $saleAllNum[$Product[web_x_sale_id]]) {
										$lastNum[$Product[web_x_sale_id]] = array_sum($numAry2[$Product[web_x_sale_id]]);
										$Product[slaePriceTotal] = ($Product[price_cost] * ($numAry2[$Product[web_x_sale_id]][$web_product_id]));
										$Product[truePriceTotal] = 0;
									} else {
										$canDoNum[$Product[web_x_sale_id]] = ($saleAllNum[$Product[web_x_sale_id]] - $lastNum[$Product[web_x_sale_id]]);
										$lastNum[$Product[web_x_sale_id]] += $canDoNum[$Product[web_x_sale_id]];
										if($saleAllNum[$Product[web_x_sale_id]] >= $lastNum[$Product[web_x_sale_id]]) {
											$Product[slaePriceTotal] = ($Product[price_cost] * ($canDoNum[$Product[web_x_sale_id]]));
											$Product[truePriceTotal] = ($Product[price] * ($numAry2[$Product[web_x_sale_id]][$web_product_id] - $canDoNum[$Product[web_x_sale_id]]));
										} else {
											$Product[slaePriceTotal] = 0;
											$Product[truePriceTotal] = ($Product[price] * ($numAry2[$Product[web_x_sale_id]][$web_product_id]));
										}	
									}
									
								}
	/*
								$num2 = ($SaleShoppingCarNum % $Sale[rows] == 0) ? $num : $num - ($num % $Sale[rows]);
								$num3 = ($num2) ? $num2 : 1;
								
								$Product[slaePriceTotal] = ($num2) ? ($Product[price_cost] * ($num3)) : 0;
								$Product[truePriceTotal] = ($num2) ? ($Product[price] * ($num - $num3)) : $Product[price] * $num;
	*/							
								$Product[allPriceTotal] = $Product[slaePriceTotal] + $Product[truePriceTotal];
								
							}
							
							$Product[price] = $Product[price_cost];
							$showFlag = 1;

							//$Product[price] = round($Product[price] * $Sale[discountnum] / 100);
							$saleTitle = $Sale['subject'];	
							//$saleTotal[$Product[web_x_sale_id]] += ($Product[price] * $num);
							//$saleTotal[$Product[web_x_sale_id]] += $Product[allPriceTotal];
							$saleTotal[$Product[web_x_sale_id]] += $Product[slaePriceTotal];
							$saleNum[$Product[web_x_sale_id]] += $num;
							if($saleNum[$Product[web_x_sale_id]] >= $Sale[rows]) {
								$_minusRow = $saleNum[$Product[web_x_sale_id]] % $Sale[rows];
								$_minus = ($Product[price] * $_minusRow);
							} 

							$saleAry[$Product[web_x_sale_id]] = array(
								'id'				=>	$Product[web_x_sale_id],
								'saleRow'			=>	$Sale[rows],
								'saleDiscountnum'	=>	$Sale[discountnum],
								'title'				=>	$saleTitle,
								'num'				=> 	$saleNum[$Product[web_x_sale_id]],
								//'total'				=>	$saleTotal[$Product[web_x_sale_id]] - $_minus,
								'total'				=>	$saleTotal[$Product[web_x_sale_id]],

							);
						}
						
					}
				} else {
					$showFlag = 0;
					$saleAry[$Product[web_x_sale_id]] = array(
						'id'			=>	$Product[web_x_sale_id],
						'saleRow'		=>	$Sale[rows],
						'salePrice'		=>	$Sale[price],
						'title'			=>	$Sale['subject'],
						'eventType'		=>  '2'	
					);
					//2017-06-14 改為一般價(取消原價)
					//$Product[price] = $Product[price_cost];  //用原價
				}	
				
			}
		}

		if($_SESSION["MyShopping"][$i]["additional"]) {
			$Product[price] = 0;
			$totalAdditional += $num * $Product[additionalprice];
		} 

		//$total += ($num * $Product[price]);
		if(!$showFlag)
			$total += ($num * $Product[price]);
		else
			$total += $Product[allPriceTotal];
		if ($Product[ifAllowMemberDiscount]==1) {
			//$allow_total += ($num * $Product[price]);	//允許會員等級折扣的產品金額小計

			if(!$showFlag)
				$allow_total += ($num * $Product[price]);	//允許會員等級折扣的產品金額小計
			else
				$allow_total += $Product[allPriceTotal];

		} else {
			//$not_allow_total += ($num * $Product[price]);	//不允許會員等級折扣的產品金額小計

			if(!$showFlag)
				$not_allow_total += ($num * $Product[price]);	//允許會員等級折扣的產品金額小計
			else
				$not_allow_total += $Product[allPriceTotal];

		}
	}
	$minusTotal = 0;
	if(count($saleAry)) {
		foreach($saleAry as $saleRow) {
			/*
			$_saleTotal = ($saleRow[total] >= $saleRow['salePrice']) ? $saleRow[total] : $saleRow['salePrice'];
			$_saleNum = ($saleRow[num] >= $saleRow['saleRow']) ? $saleRow[num] : $saleRow['num'];
			
			$_saleRow =  floor($saleRow[num] / $saleRow['saleRow']);
			$minus = $_saleTotal - ($saleRow['salePrice'] * ($_saleRow));
			*/
			if($saleRow['eventType'] != '2') {
				if($saleRow['salePrice']) {
					$_saleTotal = ($saleRow[total] >= $saleRow['salePrice']) ? $saleRow[total] : $saleRow['salePrice'];
					$_saleNum = ($saleRow[num] >= $saleRow['saleRow']) ? $saleRow[num] : $saleRow['num'];
					$_saleRow =  floor($saleRow[num] / $saleRow['saleRow']);
					$minus = $_saleTotal - ($saleRow['salePrice'] * ($_saleRow));
				} else {
					$_saleTotal = ($saleRow[total] >= $saleRow['salePrice']) ? $saleRow[total] : $saleRow['salePrice'];
					$_saleNum = ($saleRow[num] >= $saleRow['saleRow']) ? $saleRow[num] : $saleRow['num'];
					$_saleRow =  floor($saleRow[num] / $saleRow['saleRow']);
					$minus = $_saleTotal - round(($_saleTotal * $saleRow['saleDiscountnum']) / 100);
				}
			} else {
				$minus = 0;
				//$minus = CalFreightDefault($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney);	//運費=小計-優惠代碼-紅利
				//$minus = 150;
			}	
			
			$shopping_list .= "
			<tr>
				<td colspan=\"4\"><p class=\"fw_medium f_size_large t_align_r t_xs_align_c\">".$saleRow['title']."：</p></td>
				<td colspan=\"1\" class=\"t_align_r\"><p class=\"fw_medium f_size_large color_dark\">-$<span id=\"new_subtotal\">".number_format($minus)."</span></p></td>
			</tr>";
			$minusTotal += $minus;
		}
		//echo $minusTotal += $minus;
		//echo $minusTotal;
	}
	
	//$totalAdditional不可加在折扣裡
	$ifLogin = MemberLogin();
	if ($ifLogin) {
		$Member = getMember(intval($_SESSION["MyMember"]["ID"]));
		$allow_total = $allow_total + $not_allow_total + $totalAdditional - $minusTotal;
		if ($allow_total>=$Member[level_srange]) {	//有超過最低金額
			//$total = round($allow_total * $Member[level_discount] / 100) + $not_allow_total;	//折扣
			//$total = round($allow_total * $Member[level_discount] / 100) + $not_allow_total + $totalAdditional - $minusTotal;	//折扣
			//$total = round(($allow_total + $not_allow_total + $totalAdditional - $minusTotal) * $Member[level_discount] / 100);	//折扣
			$total = round($allow_total * ($Member[level_discount] / 100));	//折扣
			//round($n)  四捨五入
			//ceil($n)   無條件進位
			//floor($n)  無條件捨去
		} else {
			$total = $allow_total;
		}
	} else {
		$total = $total + $totalAdditional - $minusTotal;
	}
	
	return $total;
}

//優惠代碼, 回傳金額
function CalCode() {
	$money = 0;	//優惠金額
	$subtotal = CalSubtotal();	//小計
	
	$sql = "Select web_code_id, srange, money From web_code Where subject like '".$_SESSION["MyCode"]."' and sdate <= '".date("Y-m-d")."' and edate >= '".date("Y-m-d")."' and usedate = '0000-00-00 00:00:00' order by web_code_id ";
	$rs = ConnectDB($DB, $sql);
	if (mysqli_num_rows($rs)==0) $_SESSION["MyCode"] = "";
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);			
		
		if ($subtotal>=$srange) {	//達到金額
			if ($money>=$subtotal) {	//優惠金額>小計
				$_SESSION["MyCode"] = "";
				$money = 0;
			} else {				
				//可使用的
			}
		} else {	//未達金額
			$_SESSION["MyCode"] = "";
			$money = 0;
		}
	}

	//每個優惠代碼每人限用一次
	$ifLogin = MemberLogin();
	if ($ifLogin) {
		$sql = "Select count(*) as counter From web_x_order Where web_member_id = '".intval($_SESSION["MyMember"]["ID"])."' and (paymentstatus like '尚未收款' or paymentstatus like '付款成功') and web_code_subject like '".$_SESSION["MyCode"]."' ";
		$rs = ConnectDB($DB, $sql);
		if (mysql_result($rs, 0, "counter")>0) {	//已使用過此代碼
			$_SESSION["MyCode"] = "";
			$money = 0;
		}
	}

	return $money;
}

//推薦代碼
function CalMemberCode($code) {
	$sql = "Select subject, money From web_x_memberCode Where subject like '".$code."' and sdate <= '".date("Y-m-d")."' and edate >= '".date("Y-m-d")."' order by web_x_memberCode_id ";
	$rs = ConnectDB($DB, $sql);
	if (mysqli_num_rows($rs)>0) $money = mysql_result($rs, 0, "money");

	return $money;
}

//運費
function CalFreight($subtotal) {
	$sql = "Select srange, shipment From web_freight Where web_x_freight_id = '".$_SESSION["MyFreight"]."' order by srange, erange, web_freight_id desc ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
		if ($subtotal>=$srange) $freight = $shipment;	//新運費
	}

	return $freight;
}

//運費
function CalFreightDefault($subtotal) {
	$sql = "Select srange, shipment From web_freight Where web_x_freight_id = '1' order by srange, erange, web_freight_id desc ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
		if ($subtotal>=$srange) $freight = $shipment;	//新運費
	}

	return $freight;
}


//運費區間
function CalFreightRange($subtotal) {
	$sql = "Select srange, shipment From web_freight Where web_x_freight_id = '".$_SESSION["MyFreight"]."' order by srange, erange, web_freight_id desc ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
		
	}

	return $srange;
}

//貨到付款
function CalCashOnDelivery($subtotal) {
	$sql = "Select srange, shipment From web_CashOnDelivery Where web_x_CashOnDelivery_id = '1' order by srange, erange, web_CashOnDelivery_id desc ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
		if ($subtotal>=$srange) $CashOnDelivery = $shipment;	//新運費
	}

	return $CashOnDelivery;
}

//貨到付款區間
function CalCashOnDeliveryRange($subtotal) {
	$sql = "Select srange, shipment From web_CashOnDelivery Where web_x_CashOnDelivery_id = '1' order by srange, erange, web_CashOnDelivery_id desc ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
		
	}

	return $srange;
}

//贈品-舊
function CalGift2($total) {
	//先找預設值	
	$sql = "Select web_x_gift_id, ifDefault as GiftDefault, subject as web_x_gift From web_x_gift Where ifDefault = '1' ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
	}
	
	//有舉行什麼活動嗎
	$sql = "Select web_x_gift_id, ifDefault as GiftDefault, subject as web_x_gift From web_x_gift Where ifDefault = '0' and sdate <= '".date("Y-m-d H:i:s")."' and edate >= '".date("Y-m-d H:i:s")."' order by sdate desc, edate desc, web_x_gift_id desc ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
	}

	$web_gift_array = array();
	$sql = "Select money, subject From web_gift Where web_x_gift_id = '".$web_x_gift_id."' ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);

		if ($total>=$money) $web_gift_array[] = $subject;	//依小計為準
	}
	$web_gift_list = implode("、", $web_gift_array);
	if ($web_gift_list) $web_gift_list = "贈品：".$web_gift_list;
	
	return $web_gift_list;
}

//贈品
function CalGift($total) {
	//先找預設值	
	$sql = "Select web_x_gift_id, ifDefault as GiftDefault, subject as web_x_gift From web_x_gift Where ifDefault = '1' ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
		$web_x_gift_id_aryDefault[] = $web_x_gift_id;
	}
	
	//有舉行什麼活動嗎
	$sql = "Select web_x_gift_id, ifDefault as GiftDefault, subject as web_x_gift From web_x_gift Where ifDefault = '0' and sdate <= '".date("Y-m-d")."' and edate >= '".date("Y-m-d")."' order by sdate desc, edate desc, web_x_gift_id desc ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
		$web_x_gift_id_aryNotDefault[] = $web_x_gift_id;
	}
	
	if($web_x_gift_id_aryDefault && $web_x_gift_id_aryNotDefault) {
		$web_x_gift_id_ary = array_merge($web_x_gift_id_aryDefault, $web_x_gift_id_aryNotDefault);
	} else if($web_x_gift_id_aryDefault) {
		$web_x_gift_id_ary = $web_x_gift_id_aryDefault;
	} else if($web_x_gift_id_aryNotDefault) {
		$web_x_gift_id_ary = $web_x_gift_id_aryNotDefault;
	}

	$web_gift_array = array();
	//$sql = "Select money, subject From web_gift Where web_x_gift_id = '".$web_x_gift_id."' ";
	$sql = "Select money, subject, stock From web_gift Where web_x_gift_id IN (".implode(',', $web_x_gift_id_ary).") ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);

		if ($total>=$money) $web_gift_array[] = $subject;	//依小計為準
	}

	$web_gift_list = implode("、", $web_gift_array);
	if ($web_gift_list) $web_gift_list = "贈品：".$web_gift_list;
	
	return $web_gift_list;
}

//贈品列表
function CalGiftList($total) {
	//先找預設值	
	$sql = "Select web_x_gift_id, ifDefault as GiftDefault, subject as web_x_gift From web_x_gift Where ifDefault = '1' ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
		$web_x_gift_id_aryDefault[] = $web_x_gift_id;
	}
	
	//有舉行什麼活動嗎
	$sql = "Select web_x_gift_id, ifDefault as GiftDefault, subject as web_x_gift From web_x_gift Where ifDefault = '0' and sdate <= '".date("Y-m-d H:i:s")."' and edate >= '".date("Y-m-d H:i:s")."' order by sdate desc, edate desc, web_x_gift_id desc ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
		$web_x_gift_id_aryNotDefault[] = $web_x_gift_id;
	}

	if($web_x_gift_id_aryDefault && $web_x_gift_id_aryNotDefault) {
		$web_x_gift_id_ary = array_merge($web_x_gift_id_aryDefault, $web_x_gift_id_aryNotDefault);
	} else if($web_x_gift_id_aryDefault) {
		$web_x_gift_id_ary = $web_x_gift_id_aryDefault;
	} else if($web_x_gift_id_aryNotDefault) {
		$web_x_gift_id_ary = $web_x_gift_id_aryNotDefault;
	}

	$web_gift_array = array();
	//$sql = "Select money, subject, web_gift_id, Covers From web_gift Where web_x_gift_id = '".$web_x_gift_id."' ";
	$sql = "Select web_gift_id, money, subject, Covers, stock, pincode From web_gift Where web_x_gift_id IN (".implode(',', $web_x_gift_id_ary).") ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);

		if ($total>=$money) $web_gift_array[] = $row;	//依小計為準
	}
	
	
	return $web_gift_array;
}

//滿額加購 -舊
function CalFullAdditional2($total) {
	//先找預設值	
	$sql = "Select web_x_additional_id, ifDefault as AdditionalDefault, subject as web_x_gift From web_x_additional Where ifDefault = '1' ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
	}
	
	//有舉行什麼活動嗎
	$sql = "Select web_x_additional_id, ifDefault as AdditionalDefault, subject as web_x_additional From web_x_additional Where ifDefault = '0' and sdate <= '".date("Y-m-d")."' and edate >= '".date("Y-m-d")."' order by sdate desc, edate desc, web_x_additional_id desc ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
	}

	$web_additional_array = array();
	$sql = "Select money, bestsellers2 From web_additional Where web_x_additional_id = '".$web_x_additional_id."' ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);

		if ($total>=$money && $bestsellers2) $web_additional_array[] = $bestsellers2;	//依小計為準
	}
	//$web_additional_list = implode(",", $web_additional_array);
	//if ($web_additional_list) $web_additional_list = "贈品：".$web_additional_list;
	
	return $web_additional_array;
}

//滿額加購 -新
function CalFullAdditional($total) {
	//先找預設值	
	$sql = "Select web_x_additional_id, ifDefault as AdditionalDefault, subject as web_x_gift From web_x_additional Where ifDefault = '1' ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
		$web_x_additional_id_aryDefault[] = $web_x_additional_id;
	}
	
	//有舉行什麼活動嗎
	$sql = "Select web_x_additional_id, ifDefault as AdditionalDefault, subject as web_x_additional From web_x_additional Where ifDefault = '0' and sdate <= '".date("Y-m-d")."' and edate >= '".date("Y-m-d")."' order by sdate desc, edate desc, web_x_additional_id desc ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
		$web_x_additional_id_aryNotDefault[] = $web_x_additional_id;
	}

	if($web_x_additional_id_aryDefault && $web_x_additional_id_aryNotDefault) {
		$web_x_additional_id_ary = array_merge($web_x_additional_id_aryDefault, $web_x_additional_id_aryNotDefault);
	} else if($web_x_additional_id_aryDefault) {
		$web_x_additional_id_ary = $web_x_additional_id_aryDefault;
	} else if($web_x_additional_id_aryNotDefault) {
		$web_x_additional_id_ary = $web_x_additional_id_aryNotDefault;
	}
	
	$web_additional_array = array();
	//$sql = "Select money, bestsellers2 From web_additional Where web_x_additional_id = '".$web_x_additional_id."' ";
	$sql = "Select money, bestsellers2 From web_additional Where web_x_additional_id IN (".implode(',', $web_x_additional_id_ary).") ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);

		if ($total>=$money && $bestsellers2) $web_additional_array[] = $bestsellers2;	//依小計為準
	}
	$web_additional_list = implode(",", $web_additional_array);

	//if ($web_additional_list) $web_additional_list = "贈品：".$web_additional_list;
	
	return $web_additional_list;
}

//滿額加購-金額區間 -舊
function CalFullAdditionalMoney2($total) {
	//先找預設值	
	$sql = "Select web_x_additional_id, ifDefault as AdditionalDefault, subject as web_x_gift From web_x_additional Where ifDefault = '1' ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
	}
	
	//有舉行什麼活動嗎
	$sql = "Select web_x_additional_id, ifDefault as AdditionalDefault, subject as web_x_additional From web_x_additional Where ifDefault = '0' and sdate <= '".date("Y-m-d")."' and edate >= '".date("Y-m-d")."' order by sdate desc, edate desc, web_x_additional_id desc ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
	}

	$web_additional_money_array = array();
	$sql = "Select money, bestsellers2 From web_additional Where web_x_additional_id = '".$web_x_additional_id."' ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);

		if ($total>=$money && $bestsellers2) $web_additional_money_array[] = $money;	//依小計為準
	}
	
	
	return $web_additional_money_array;
}

//滿額加購-金額區間 -新
function CalFullAdditionalMoney($total) {
	//先找預設值	
	$sql = "Select web_x_additional_id, ifDefault as AdditionalDefault, subject as web_x_gift From web_x_additional Where ifDefault = '1' ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
		$web_x_additional_id_aryDefault[] = $web_x_additional_id;
	}
	
	//有舉行什麼活動嗎
	$sql = "Select web_x_additional_id, ifDefault as AdditionalDefault, subject as web_x_additional From web_x_additional Where ifDefault = '0' and sdate <= '".date("Y-m-d")."' and edate >= '".date("Y-m-d")."' order by sdate desc, edate desc, web_x_additional_id desc ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
		$web_x_additional_id_aryNotDefault[] = $web_x_additional_id;
	}

	if(count($web_x_additional_id_aryDefault) && count($web_x_additional_id_aryNotDefault)) {
		$web_x_additional_id_ary = array_merge($web_x_additional_id_aryDefault, $web_x_additional_id_aryNotDefault);
	} else if(count($web_x_additional_id_aryDefault)) {
		$web_x_additional_id_ary = $web_x_additional_id_aryDefault;
	} else if(count($web_x_additional_id_aryNotDefault)) {
		$web_x_additional_id_ary = $web_x_additional_id_aryNotDefault;
	}
	
	//echo implode(',', $web_x_additional_id_ary);
	$web_additional_money_array = array();
	//$sql = "Select money, bestsellers2 From web_additional Where web_x_additional_id = '".$web_x_additional_id."' ";
	$sql = "Select money, bestsellers2 From web_additional Where web_x_additional_id IN (".implode(',', $web_x_additional_id_ary).") ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);

		if ($total>=$money && $bestsellers2) $web_additional_money_array[] = $money;	//依小計為準
	}
	
	
	return $web_additional_money_array;
}

//是否符合紅利折扣資格
function ___CalUseBonus($subtotal) {
	$bonus = 0;	//可使用的紅利
	$usemoney = 0;	//使用的紅利
	$ifLogin = MemberLogin();
	
	if ($ifLogin) {	//會員
		$web_member_id = intval($_SESSION["MyMember"]["ID"]);
		//$sql = "Select web_bonus_id, kind, web_x_order_from_ordernum, srange, money From web_bonus Where web_member_id = '".$web_member_id."' and sdate <= '".date("Y-m-d")."' and edate >= '".date("Y-m-d")."' and usedate = '0000-00-00 00:00:00' order by money desc";
		//$sql = "Select web_bonus_id, kind, web_x_order_from_ordernum, srange, money, usemoney From web_bonus Where web_member_id = '".$web_member_id."' and sdate <= '".date("Y-m-d")."' and edate >= '".date("Y-m-d")."' and (money - usemoney) != 0 order by money desc";
		$sql = "Select a.web_bonus_id, a.web_x_bonus_id, a.kind, a.web_x_order_from_ordernum, a.web_x_order_to_ordernum, a.money, a.usemoney, b.overflow, b.bonusOverflow From web_bonus a Left Join web_x_bonus b ON b.web_x_bonus_id = a.web_x_bonus_id Where a.web_member_id = '".$web_member_id."' and a.sdate <= '".date("Y-m-d")."' and a.edate >= '".date("Y-m-d")."' order by money desc";
		$rs = ConnectDB($DB, $sql);
		//echo mysqli_num_rows($rs);
		$bonusAry = array();
		for ($i=0; $i<mysqli_num_rows($rs); $i++) {
			$row = mysqli_fetch_assoc($rs);
			if(isset($row[bonusOverflow])) {
				$bonusAry[$row[web_x_bonus_id]][$row[bonusOverflow]] += $row[money];
			}
			//if ($subtotal>=$row["srange"] && $bonus<=$subtotal) {	//小計大於消費滿多少元可使用, 贈送的紅利不得少於小計
			//if ($subtotal>=$row["overflow"]) {	//小計大於消費滿多少元可使用, 贈送的紅利不得少於小計
				switch ($row["kind"]) {
					case "shopping":	//付款成功的訂單才能使用
						$sql2 = "Select count(*) as counter From web_x_order Where ordernum = '".$row["web_x_order_from_ordernum"]."' and paymentstatus like '付款成功' and states not like '取消'";	
						$rs2 = ConnectDB($DB, $sql2);
						$counter = mysql_result($rs2, 0, "counter");
						//if ($counter>0 || $row['usemoney']) {
						if ($counter>0 || $row['usemoney']) {	
							//echo $row["web_x_order_from_ordernum"]."---".$row['money']."---".$row['usemoney']."</br>";
							//echo $bonus += ($row["money"] - $row['usemoney']);	//本次的紅利折扣金額
							//$bonus += ($row["money"]);	//本次的紅利折扣金額
							if($row["money"] && !$row["web_x_order_to_ordernum"]) {
								$web_bonus_id .= $row["web_bonus_id"].",";	//預計扣除的紅利流水號
								$web_bonus_ordernum .= $row["web_x_order_from_ordernum"].",";	//從哪些訂單編號來的紅利
							}	
						} else {
							$bonusAry[$row[web_x_bonus_id]][$row[bonusOverflow]] -= $row[money];
						}
						break;
					
					case "share":	//付款成功的訂單才能使用
						$bonus += $row["money"];	//本次的紅利折扣金額
						$web_bonus_id .= $row["web_bonus_id"].",";	//預計扣除的紅利流水號
						//$Member = getMember($row["web_x_order_from_ordernum"]);
						$web_bonus_ordernum .= $row["web_x_order_from_ordernum"].",";	//從哪些訂單編號來的紅利
						break;	
					
					case "joinmember":	//加入會員
						$bonus += $row["money"];	//本次的紅利折扣金額
						$web_bonus_id .= $row["web_bonus_id"].",";	//預計扣除的紅利流水號
						break;
					case "birthday":	//會員生日
						break;
					default:
						
				}
			//}
		}
		//echo $bonus."===".$subtotal;
		//echo $row["bonusOverflow"];
		ksort($bonusAry);
		
		echo "<pre>";
		print_r($bonusAry);
		echo "</pre>";
		
		foreach($bonusAry as $key => $bonusRow) {
			foreach($bonusRow as $key2 => $bonus) {
				if($bonus >= $key2) {
					$_bonus[$key] += $bonus;
				}
			}
		}
		$bonus = array_sum($_bonus);
		
		//echo implode(", ", array_keys($_bonus));

		$sql2 = "Select a.web_bonus_id, a.web_x_bonus_id, a.kind, a.web_x_order_from_ordernum, a.web_x_order_to_ordernum, a.money, a.usemoney, b.overflow, b.bonusOverflow From web_bonus a Left Join web_x_bonus b ON b.web_x_bonus_id = a.web_x_bonus_id Where a.web_x_bonus_id IN (".implode(", ", array_keys($_bonus)).") and a.web_member_id = '".$web_member_id."' and a.sdate <= '".date("Y-m-d")."' and a.edate >= '".date("Y-m-d")."' order by money desc";
		$rs2 = ConnectDB($DB, $sql2);
		for ($i2=0; $i2<mysqli_num_rows($rs2); $i2++) {
			$row2 = mysqli_fetch_assoc($rs2);
			$web_bonus_id_ary[] = $row2[web_bonus_id];
			$web_bonus_ordernum_ary[] = $row2[web_x_order_from_ordernum];
		}	

		$web_bonus_id = ($web_bonus_id_ary) ? implode(',', $web_bonus_id_ary) : null;
		$web_bonus_ordernum = ($web_bonus_ordernum_ary) ? implode(',', $web_bonus_ordernum_ary) : null;
		
		$bonus = ($bonus > $subtotal) ? $subtotal : $bonus;	
		
		//使用的
		$sql3 = "Select a.web_bonus_id, a.web_x_bonus_id, a.kind, a.web_x_order_from_ordernum, a.web_x_order_to_ordernum, a.money, a.usemoney, b.overflow, b.bonusOverflow From web_bonus a Left Join web_x_bonus b ON b.web_x_bonus_id = a.web_x_bonus_id Where a.web_member_id = '".$web_member_id."' and usemoney != '' order by money desc";
		$rs3 = ConnectDB($DB, $sql3);
		for ($i3=0; $i3<mysqli_num_rows($rs3); $i3++) {
			$row3 = mysqli_fetch_assoc($rs3);

			$usedAry = ($row3[used]) ? explode(',', $row3[used]) : null;
			$diff = array_diff($web_bonus_id_ary, $usedAry);
			//echo count($diff)."====".count($web_bonus_id_ary);
			if(count($diff) == count($web_bonus_id_ary)) {
				continue;
			}

			$usemoney += $row3[usemoney];
			
		}	
	}

	$total = $bonus - $usemoney;
	$bonus = ($total < 0) ? 0 : $total;
	//echo $bonus;
	//紅利累積到設定值才可使用
	if($bonus) { 
		$UseBonus[bonus] = $bonus;	//本次的紅利折扣金額
		$UseBonus[web_bonus_id] = trim($web_bonus_id, ",");		//預計扣除的紅利流水號
		$UseBonus[web_bonus_ordernum] = trim($web_bonus_ordernum, ",");	//從哪些訂單編號來的紅利
	}

	return $UseBonus;
}

//是否符合紅利折扣資格
function CalUseBonus($subtotal) {
	$bonus = 0;	//可使用的紅利
	$usemoney = 0;	//使用的紅利
	$ifLogin = MemberLogin();
	
	if ($ifLogin) {	//會員
		$web_member_id = intval($_SESSION["MyMember"]["ID"]);
		//$sql = "Select web_bonus_id, kind, web_x_order_from_ordernum, srange, money From web_bonus Where web_member_id = '".$web_member_id."' and sdate <= '".date("Y-m-d")."' and edate >= '".date("Y-m-d")."' and usedate = '0000-00-00 00:00:00' order by money desc";
		//$sql = "Select web_bonus_id, kind, web_x_order_from_ordernum, srange, money, usemoney From web_bonus Where web_member_id = '".$web_member_id."' and sdate <= '".date("Y-m-d")."' and edate >= '".date("Y-m-d")."' and (money - usemoney) != 0 order by money desc";
		//$sql = "Select a.web_bonus_id, a.web_x_bonus_id, a.kind, a.web_x_order_from_ordernum, a.web_x_order_to_ordernum, a.money, a.usemoney, b.overflow, b.bonusOverflow From web_bonus a Left Join web_x_bonus b ON b.web_x_bonus_id = a.web_x_bonus_id Where a.web_member_id = '".$web_member_id."' and a.sdate <= '".date("Y-m-d")."' and a.edate >= '".date("Y-m-d")."' order by money desc";
		$sql = "Select a.web_bonus_id, a.web_x_bonus_id, a.kind, a.web_x_order_from_ordernum, a.web_x_order_to_ordernum, a.money, a.usemoney, a.cdate, b.overflow, b.bonusOverflow, b.startdate, b.enddate, b.ifDate, b.days From web_bonus a Left Join web_x_bonus b ON b.web_x_bonus_id = a.web_x_bonus_id Where a.web_member_id = '".$web_member_id."' order by money desc";
		$rs = ConnectDB($DB, $sql);
		//echo mysqli_num_rows($rs);
		$bonusAry = array();
		for ($i=0; $i<mysqli_num_rows($rs); $i++) {
			$row = mysqli_fetch_assoc($rs);

			if($row[ifDate] && $row[days]) {
				$row[startdate] = substr($row["cdate"], 0, 10);
				$row[enddate] = date('Y-m-d', strtotime($row[startdate]. '+'.$row[days].' day'));
			}

			if($row[startdate] <= date("Y-m-d") && $row[enddate] >= date("Y-m-d")) {	
				if(isset($row[bonusOverflow])) {
					$bonusAry[$row[web_x_bonus_id]][$row[bonusOverflow]] += $row[money];
				}
			//if ($subtotal>=$row["srange"] && $bonus<=$subtotal) {	//小計大於消費滿多少元可使用, 贈送的紅利不得少於小計
			//if ($subtotal>=$row["overflow"]) {	//小計大於消費滿多少元可使用, 贈送的紅利不得少於小計
				switch ($row["kind"]) {
					case "shopping":	//付款成功的訂單才能使用
						$sql2 = "Select count(*) as counter From web_x_order Where ordernum = '".$row["web_x_order_from_ordernum"]."' and paymentstatus like '付款成功' and states not like '取消'";	
						$rs2 = ConnectDB($DB, $sql2);
						$counter = mysql_result($rs2, 0, "counter");
						//if ($counter>0 || $row['usemoney']) {
						if ($counter>0 || $row['usemoney']) {	
							//echo $row["web_x_order_from_ordernum"]."---".$row['money']."---".$row['usemoney']."</br>";
							//echo $bonus += ($row["money"] - $row['usemoney']);	//本次的紅利折扣金額
							//$bonus += ($row["money"]);	//本次的紅利折扣金額
							if($row["money"] && !$row["web_x_order_to_ordernum"]) {
								$web_bonus_id .= $row["web_bonus_id"].",";	//預計扣除的紅利流水號
								$web_bonus_ordernum .= $row["web_x_order_from_ordernum"].",";	//從哪些訂單編號來的紅利
							}	
						} else {
							$bonusAry[$row[web_x_bonus_id]][$row[bonusOverflow]] -= $row[money];
						}
						break;
					
					case "share":	//付款成功的訂單才能使用
						$bonus += $row["money"];	//本次的紅利折扣金額
						$web_bonus_id .= $row["web_bonus_id"].",";	//預計扣除的紅利流水號
						//$Member = getMember($row["web_x_order_from_ordernum"]);
						$web_bonus_ordernum .= $row["web_x_order_from_ordernum"].",";	//從哪些訂單編號來的紅利
						break;	
					
					case "joinmember":	//加入會員
						$bonus += $row["money"];	//本次的紅利折扣金額
						$web_bonus_id .= $row["web_bonus_id"].",";	//預計扣除的紅利流水號
						break;
					case "birthday":	//會員生日
						break;
					default:
						
				}
			//}
			}	
		}
		//echo $bonus."===".$subtotal;
		//echo $row["bonusOverflow"];
		ksort($bonusAry);
		/*
		echo "<pre>";
		print_r($bonusAry);
		echo "</pre>";
		*/
		foreach($bonusAry as $key => $bonusRow) {
			foreach($bonusRow as $key2 => $bonus) {
				//累計滿設定值方可使用
				if($bonus >= $key2) {
					$_bonus[$key] += $bonus;
				}
			}
		}
		$bonus = array_sum($_bonus);
		
		//echo implode(", ", array_keys($_bonus));

		$sql2 = "Select a.web_bonus_id, a.web_x_bonus_id, a.kind, a.web_x_order_from_ordernum, a.web_x_order_to_ordernum, a.money, a.usemoney, b.overflow, b.bonusOverflow From web_bonus a Left Join web_x_bonus b ON b.web_x_bonus_id = a.web_x_bonus_id Where a.web_x_bonus_id IN (".implode(", ", array_keys($_bonus)).") and a.web_member_id = '".$web_member_id."' and a.sdate <= '".date("Y-m-d")."' and a.edate >= '".date("Y-m-d")."' order by money desc";
		$rs2 = ConnectDB($DB, $sql2);
		for ($i2=0; $i2<mysqli_num_rows($rs2); $i2++) {
			$row2 = mysqli_fetch_assoc($rs2);
			$web_bonus_id_ary[] = $row2[web_bonus_id];
			$web_bonus_ordernum_ary[] = $row2[web_x_order_from_ordernum];
		}	

		$web_bonus_id = ($web_bonus_id_ary) ? implode(',', $web_bonus_id_ary) : null;
		$web_bonus_ordernum = ($web_bonus_ordernum_ary) ? implode(',', $web_bonus_ordernum_ary) : null;
		
		$bonus = ($bonus > $subtotal) ? $subtotal : $bonus;	
		
		//使用的
		$sql3 = "Select a.web_bonus_id, a.web_x_bonus_id, a.kind, a.web_x_order_from_ordernum, a.web_x_order_to_ordernum, a.money, a.usemoney, b.overflow, b.bonusOverflow From web_bonus a Left Join web_x_bonus b ON b.web_x_bonus_id = a.web_x_bonus_id Where a.web_member_id = '".$web_member_id."' and usemoney != '' order by money desc";
		$rs3 = ConnectDB($DB, $sql3);
		for ($i3=0; $i3<mysqli_num_rows($rs3); $i3++) {
			$row3 = mysqli_fetch_assoc($rs3);

			$usedAry = ($row3[used]) ? explode(',', $row3[used]) : null;
			$diff = array_diff($web_bonus_id_ary, $usedAry);
			//echo count($diff)."====".count($web_bonus_id_ary);
			if(count($diff) == count($web_bonus_id_ary)) {
				continue;
			}

			$usemoney += $row3[usemoney];
			
		}	
	}

	$total = $bonus - $usemoney;
	$bonus = ($total < 0) ? 0 : $total;
	//echo $bonus;
	//紅利累積到設定值才可使用
	if($bonus) { 
		$UseBonus[bonus] = $bonus;	//本次的紅利折扣金額
		$UseBonus[web_bonus_id] = trim($web_bonus_id, ",");		//預計扣除的紅利流水號
		$UseBonus[web_bonus_ordernum] = trim($web_bonus_ordernum, ",");	//從哪些訂單編號來的紅利
	}

	return $UseBonus;
}

//是否符合紅利折扣資格
function CalUseBonus2($subtotal) {
	$bonus = 0;	//可使用的紅利
	$ifLogin = MemberLogin();
	
	if ($ifLogin) {	//會員
		$web_member_id = intval($_SESSION["MyMember"]["ID"]);
		//$sql = "Select web_bonus_id, kind, web_x_order_from_ordernum, srange, money From web_bonus Where web_member_id = '".$web_member_id."' and sdate <= '".date("Y-m-d")."' and edate >= '".date("Y-m-d")."' and usedate = '0000-00-00 00:00:00' order by money desc";
		//$sql = "Select web_bonus_id, kind, web_x_order_from_ordernum, srange, money, usemoney From web_bonus Where web_member_id = '".$web_member_id."' and sdate <= '".date("Y-m-d")."' and edate >= '".date("Y-m-d")."' and (money - usemoney) != 0 order by money desc";
		$sql = "Select a.web_bonus_id, a.web_x_bonus_id, a.kind, a.web_x_order_from_ordernum, a.web_x_order_to_ordernum, a.money, a.usemoney, b.overflow, b.bonusOverflow From web_bonus a Left Join web_x_bonus b ON b.web_x_bonus_id = a.web_x_bonus_id Where a.web_member_id = '".$web_member_id."' and a.sdate <= '".date("Y-m-d")."' and a.edate >= '".date("Y-m-d")."' order by money desc";
		$rs = ConnectDB($DB, $sql);
		//echo mysqli_num_rows($rs);
		for ($i=0; $i<mysqli_num_rows($rs); $i++) {
			$row = mysqli_fetch_assoc($rs);
			//if ($subtotal>=$row["srange"] && $bonus<=$subtotal) {	//小計大於消費滿多少元可使用, 贈送的紅利不得少於小計
			//if ($subtotal>=$row["overflow"]) {	//小計大於消費滿多少元可使用, 贈送的紅利不得少於小計
				switch ($row["kind"]) {
					case "shopping":	//付款成功的訂單才能使用
						$sql2 = "Select count(*) as counter From web_x_order Where ordernum = '".$row["web_x_order_from_ordernum"]."' and paymentstatus like '付款成功' and states not like '取消'";	
						$rs2 = ConnectDB($DB, $sql2);
						$counter = mysql_result($rs2, 0, "counter");
						//if ($counter>0 || $row['usemoney']) {
						if ($counter>0 || $row['usemoney']) {	
							//echo $row["web_x_order_from_ordernum"]."---".$row['money']."---".$row['usemoney']."</br>";
							$bonus += ($row["money"] - $row['usemoney']);	//本次的紅利折扣金額
							//$bonus += ($row["money"]);	//本次的紅利折扣金額
							if($row["money"] && !$row["web_x_order_to_ordernum"]) {
								$web_bonus_id .= $row["web_bonus_id"].",";	//預計扣除的紅利流水號
								$web_bonus_ordernum .= $row["web_x_order_from_ordernum"].",";	//從哪些訂單編號來的紅利
							}	
						}
						break;
					case "share":	//付款成功的訂單才能使用
						$bonus += $row["money"];	//本次的紅利折扣金額
						$web_bonus_id .= $row["web_bonus_id"].",";	//預計扣除的紅利流水號
						//$Member = getMember($row["web_x_order_from_ordernum"]);
						$web_bonus_ordernum .= $row["web_x_order_from_ordernum"].",";	//從哪些訂單編號來的紅利
						break;	
					case "joinmember":	//加入會員
						$bonus += $row["money"];	//本次的紅利折扣金額
						$web_bonus_id .= $row["web_bonus_id"].",";	//預計扣除的紅利流水號
						break;
					case "birthday":	//會員生日
						break;
					default:
						
				}
			//}
		}
		//echo $row["bonusOverflow"];
		if($bonus >= $row["bonusOverflow"]) { 
			//如果贈送的紅利大於小計, 就以小計為主
			$bonus = ($bonus > $subtotal) ? $subtotal : $bonus;	
		} else {
			$bonus = null;
		}
	}
	//紅利累積到設定值才可使用
	if($bonus) { 
		$UseBonus[bonus] = $bonus;	//本次的紅利折扣金額
		$UseBonus[web_bonus_id] = trim($web_bonus_id, ",");		//預計扣除的紅利流水號
		$UseBonus[web_bonus_ordernum] = trim($web_bonus_ordernum, ",");	//從哪些訂單編號來的紅利
	}

	return $UseBonus;
}

//是否符合紅利折扣資格
function _CalUseBonus($subtotal) {
	$bonus = 0;	//可使用的紅利
	$ifLogin = MemberLogin();
	
	if ($ifLogin) {	//會員
		$web_member_id = intval($_SESSION["MyMember"]["ID"]);
		//$sql = "Select web_bonus_id, kind, web_x_order_from_ordernum, srange, money From web_bonus Where web_member_id = '".$web_member_id."' and sdate <= '".date("Y-m-d")."' and edate >= '".date("Y-m-d")."' and usedate = '0000-00-00 00:00:00' order by money desc";
		//$sql = "Select web_bonus_id, kind, web_x_order_from_ordernum, srange, money, usemoney From web_bonus Where web_member_id = '".$web_member_id."' and sdate <= '".date("Y-m-d")."' and edate >= '".date("Y-m-d")."' and (money - usemoney) != 0 order by money desc";
		$sql = "Select web_bonus_id, kind, web_x_order_from_ordernum, srange, money From web_bonus Where web_member_id = '".$web_member_id."' and sdate <= '".date("Y-m-d")."' and edate >= '".date("Y-m-d")."' order by money desc";
		$rs = ConnectDB($DB, $sql);
		for ($i=0; $i<mysqli_num_rows($rs); $i++) {
			$row = mysqli_fetch_assoc($rs);
			
			if ($subtotal>=$row["srange"] && $bonus<=$subtotal) {	//小計大於消費滿多少元可使用, 贈送的紅利不得少於小計
				switch ($row["kind"]) {
					case "shopping":	//付款成功的訂單才能使用
						$sql2 = "Select count(*) as counter From web_x_order Where ordernum = '".$row["web_x_order_from_ordernum"]."' and paymentstatus like '付款成功' ";	
						$rs2 = ConnectDB($DB, $sql2);
						$counter = mysql_result($rs2, 0, "counter");
						if ($counter>0) {
							//$bonus += ($row["money"] - $row['usemoney']);	//本次的紅利折扣金額
							$bonus += ($row["money"]);	//本次的紅利折扣金額
							$web_bonus_id .= $row["web_bonus_id"].",";	//預計扣除的紅利流水號
							$web_bonus_ordernum .= $row["web_x_order_from_ordernum"].",";	//從哪些訂單編號來的紅利
						}
						break;
					case "joinmember":	//加入會員
						$bonus += $row["money"];	//本次的紅利折扣金額
						$web_bonus_id .= $row["web_bonus_id"].",";	//預計扣除的紅利流水號
						break;
					case "birthday":	//會員生日
						break;
					default:
						
				}
			}
		}
		if ($bonus>$subtotal) $bonus = $subtotal;	//如果贈送的紅利大於小計, 就以小計為主
	}

	$UseBonus[bonus] = $bonus;	//本次的紅利折扣金額
	$UseBonus[web_bonus_id] = trim($web_bonus_id, ",");		//預計扣除的紅利流水號
	$UseBonus[web_bonus_ordernum] = trim($web_bonus_ordernum, ",");	//從哪些訂單編號來的紅利
/*	
	echo "<pre>";
	print_r($UseBonus);
	echo "</pre>";
*/
	return $UseBonus;
}

//紅利:本檔活動
function CalSendBonus($subtotal) {	//小計=產品小計-優惠代碼
	//$ifLogin = MemberLogin();
	$ifLogin = 1;
	$web_x_bonus_hidden = 0;	//紅利流水號
	$web_x_bonus_list = "";		//紅利描述
	$web_bonus_content = "";	//贈送的內容(用於成立訂單)
	$sql = "Select * From web_x_bonus Where kind like 'shopping' and sdate <= '".date("Y-m-d")."' and edate >= '".date("Y-m-d")."' order by sdate asc, edate desc, srange ASC ";
 	//$sql = "Select * From web_x_bonus Where kind like 'shopping' and sdate <= '".date("Y-m-d")."' and edate >= '".date("Y-m-d")."' order by sdate desc, edate ASC LIMIT 0 , 1";  //2016/07/13 改可以在同一時段重覆有兩種紅利規 取結束時間較接近者
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
		if($subtotal<$srange) {
			continue;
		}
		$web_x_bonus_title = "<p class=\"fw_medium f_size_large scheme_color t_align_l t_xs_align_c m_bottom_10\">";
		if ($subtotal>=$srange) {
			if ($ifLogin) {
				$Member = getMember(intval($_SESSION["MyMember"]["ID"]));
				
				if ($Member[level_ifAllowBonus]==1) {	//會員等級：允許使用紅利
					$web_x_bonus_title .= "恭喜您！本訂單將贈送點數".(floor($subtotal/$srange)*$money)."點，詳情如下：";
					$web_x_bonus_hidden = $web_x_bonus_id;	//暫時的
				} else {
					if ($Member[level_discount]==100)
						$web_x_bonus_title .= "本訂單暫停累積點數";
					else
						$web_x_bonus_title .= "您的身分為".$Member[level_subject]."，列入會員等級折扣的產品原價累計滿".$Member[level_srange]."元可打".$Member[level_discount]."折，本訂單暫停累積點數";
				}
			} else
				$web_x_bonus_title .= "登入會員可累積點數，詳情如下：";
		} else {
			$web_x_bonus_title .= "本檔點數如下，本訂單尚未符合贈送資格：";
		}
		$web_x_bonus_title .= "</p>";
	
		$web_x_bonus_list = "結帳金額滿".number_format($srange)."元（不含運費及其他優惠）贈送點數".number_format($money)."點，點數累積滿".$bonusOverflow."點即可使用。<br />";
		
		$web_x_bonus_list .= "使用期限：";
		if ($ifDate==1) {
			$web_x_bonus_list .= "自本訂單成立日起".$days."天內有效。";
		} else {
			$web_x_bonus_list .= "自".$startdate."至".$enddate."止。";
		}
		$web_x_bonus_list .= "<br />";
		
		$web_x_bonus_list .= "注意事項：本訂單需經過付款確認才可於日後訂單使用點數，不便之處敬請見諒。";
		$web_x_bonus_list .= "<br />";
		//$web_x_bonus_list .= "※購物金一次最多可折抵消費金額(不含運費)的50%。";
		
		$web_bonus_content = "本訂單贈送點數".(floor($subtotal/$srange)*$money)."點，詳情如下：<br />".str_replace("<br />", "\n", $web_x_bonus_list);	//贈送的內容(用於成立訂單)
		$web_x_bonus_list = $web_x_bonus_title."<p class=\"fw_medium f_size_medium color_dark t_align_l t_xs_align_l\">".$web_x_bonus_list."</p>";
		
		//不允許使用紅利
		if ($ifLogin && $Member[level_ifAllowBonus]==0) {
			$web_x_bonus_list = $web_x_bonus_title;
			$web_bonus_content = $web_x_bonus_title;	//贈送的內容(用於成立訂單)
		}
	}
	unset($Member);
	
	if ($web_x_bonus_hidden==0) {
		$web_bonus_content = "";	//贈送的內容(用於成立訂單)
	}
	
	$SendBonus[web_x_bonus_hidden] = $web_x_bonus_hidden;	//紅利流水號
	$SendBonus[web_x_bonus_list] = $web_x_bonus_list;		//紅利描述
	$SendBonus[web_bonus_content] = $web_bonus_content;		//贈送的內容(用於成立訂單)	

	return $SendBonus;
}

//是有購物金
function CalUseMemberCode($subtotal) {
	$bonus = 0;	//可使用的紅利
	$ifLogin = MemberLogin();
	
	if ($ifLogin) {	//會員
		$web_member_id = intval($_SESSION["MyMember"]["ID"]);
		$sql = "Select web_x_memberCode_id, type, overflow, srange, money, web_member_id, cdate, sdate, edate From web_memberCode Where web_member_id = '".$web_member_id."' and srange = '0' order by srange ASC, cdate DESC";
		$rs = ConnectDB($DB, $sql);
		for ($i=0; $i<mysqli_num_rows($rs); $i++) {
			$row = mysqli_fetch_assoc($rs);
			//echo $row[cdate]."===".date('Y-m-d H:i:s', strtotime( $row[cdate]."+1 month" ))."</br>";
			switch($row["srange"]) {
				case 0 :
					$_money = $row['money'];
					break;
				case 1 :
					$_money = $row['money'];
					break;
				case 999 :
					$_tag = "-";
					$_money = (0- $row['money']);
					break;		
			}
			//echo $_money."</br>";
			
			$Alltotal += $_money;
			
			$dateLimit = date('Y-m-d H:i:s', strtotime($row[cdate]."+1 month"));
			$dateline = strtotime($row[cdate]."+1 month");

			$type = ($row[type]) ? $row[type] : null;
			$overflow = ($row[overflow]) ? $row[overflow] : null;
			if($type == 1) {
				if($row[sdate] > date("Y-m-d") || $row[edate] <= date("Y-m-d")) {
					return;
				}	
			}
		}
		$row = null;
		$sql2 = "Select web_x_memberCode_id, srange, money, web_member_id, cdate From web_memberCode Where web_member_id = '".$web_member_id."' and srange != '0' order by srange ASC, cdate DESC";
		$rs2 = ConnectDB($DB, $sql2);
		//使用
		for ($i=0; $i<mysqli_num_rows($rs2); $i++) {
			$row = mysqli_fetch_assoc($rs2);
			$_money = $row['money'];
			//echo $_money."</br>";
			
			$useTotal += $_money;
		}
		if($type != '1') {
			if(time() < $dateline) {
				$total = (($Alltotal - $useTotal) > 0) ? ($Alltotal - $useTotal) : 0;

				//使用購物金只能用商品總額(不含運費)50%
				if(ceil($subtotal * 0.5) <= $total){
					$memberShoppingMoney = ceil($subtotal * 0.5);
					$memberShoppingMoneySurplus = abs($total) - ceil($subtotal * 0.5);
				} else {
					$memberShoppingMoney = abs($total);
					$memberShoppingMoneySurplus = 0;
				}
				$web_x_memberCode_id = $row[web_x_memberCode_id];
				//if ($bonus>$subtotal) $bonus = $subtotal;	//如果贈送的紅利大於小計, 就以小計為主
			}
		} else {
			
			if(time() < $dateline) {
				$total = (($Alltotal - $useTotal) > 0) ? ($Alltotal - $useTotal) : 0;
				if($subtotal < $overflow) {
					return;
				}

				//使用購物金只能用商品總額(不含運費)50%
				if(ceil($subtotal * 0.5) <= $total){
					$memberShoppingMoney = ceil($subtotal * 0.5);
					$memberShoppingMoneySurplus = abs($total) - ceil($subtotal * 0.5);
				} else {
					$memberShoppingMoney = abs($total);
					$memberShoppingMoneySurplus = 0;
				}
				$web_x_memberCode_id = $row[web_x_memberCode_id];
				//if ($bonus>$subtotal) $bonus = $subtotal;	//如果贈送的紅利大於小計, 就以小計為主
			}
			
		}	
	}

	$UseMemberShoppingMoney[money] = $memberShoppingMoney;	//本次的購物金折扣金額
	$UseMemberShoppingMoney[web_x_memberCode_id] = $web_x_memberCode_id;	//本次的購物金折扣金額
	$UseMemberShoppingMoney[surplus] = $memberShoppingMoneySurplus;	//本次的購物金折扣金額
	$UseMemberShoppingMoney[dateLimit] = ($memberShoppingMoneySurplus) ? $dateLimit : null;	//購物金使用期限
	//$UseMemberShoppingMoney[web_bonus_ordernum] = trim($web_bonus_ordernum, ",");	//從哪些訂單編號來的購物金

	return $UseMemberShoppingMoney;
}

//是有優惠券
function CalUseCoupon($subtotal) {
	$coupon = 0;	//可使用的紅利
	$ifLogin = MemberLogin();
	
	if ($ifLogin) {	//會員
		$web_member_id = intval($_SESSION["MyMember"]["ID"]);
		$sql = "Select a.web_coupon_id, a.web_x_coupon_id, a.web_x_order_ordernum, a.cdate, b.subject, b.srange, b.money, b.sdate, b.edate, b.bestsellers From web_coupon a Left Join web_x_coupon b ON b.web_x_coupon_id = a.web_x_coupon_id Where a.web_member_id = '".$web_member_id."' And (a.web_x_order_ordernum = '' And a.usedate = '0000-00-00 00:00:00') And b.sdate <= '".date("Y-m-d")."' And b.edate >= '".date("Y-m-d")."' order by money desc";
		$rs = ConnectDB($DB, $sql);
		//echo mysqli_num_rows($rs);
		for ($i=0; $i<mysqli_num_rows($rs); $i++) {
			$row = mysqli_fetch_assoc($rs);
			//總額是否符合
			if($row[srange] > $subtotal) {
				continue;
			}
			//$Coupon[edate] = (in_array($web_x_coupon_id, array(1,2))) ? date("Y-m-d", strtotime("+30 day", $_cdate)) : $Coupon[edate];
			if(in_array($row[web_x_coupon_id], array(1,2))) {
				$_cdate = strtotime($row[cdate]);
				$targetTime = strtotime("+30 day", $_cdate);
				//生日好友領取後30日到期
				if(time() >= $targetTime) {
					continue;
				}
				$UseCoupon[] = $row;
			} else {
				$UseCoupon[] = $row;
			} 
			
		}
		
	}
	
	return $UseCoupon;
	
}

//右上角清單
function TopRightProductList() {
	$web_product_list = "";
	for ($x=0; $x<count($_SESSION["MyShopping"]); $x++) {
		//if ($x>=5) break;	//最多顯示幾筆

		$web_product_id = $_SESSION["MyShopping"][$x]["web_product_id"];
		$dimension = $_SESSION["MyShopping"][$x]["dimension"];
		$num = $_SESSION["MyShopping"][$x]["num"];
		$additionalText = ($_SESSION["MyShopping"][$x]["additional"]) ? '<b style="color:#cc0066">【加購】</b>- ' : null;
		$additionalText = ($_SESSION["MyShopping"][$x]["fullAdditional"]) ? '<b style="color:#cc0066">【滿額加購】</b>- ' : $additionalText;
		
		$sql = "Select web_product_id From web_product JOIN (web_x_product, web_xx_product, web_xxx_product) ON (web_product.web_x_product_id=web_x_product.web_x_product_id and web_x_product.web_xx_product_id=web_xx_product.web_xx_product_id and web_xx_product.web_xxx_product_id=web_xxx_product.web_xxx_product_id) Where web_xxx_product.ifShow = '1' and web_xx_product.ifShow = '1' and web_x_product.ifShow = '1' and web_product.ifShow = '1' and sdate <= '".date("Y-m-d H:i:s")."' and edate >= '".date("Y-m-d H:i:s")."' and web_product_id = '".$web_product_id."' ";
		$rs = ConnectDB($DB, $sql);
		for ($i=0; $i<mysqli_num_rows($rs); $i++) {
			$row = mysqli_fetch_assoc($rs);
			
			$Product = getProduct($row["web_product_id"]);	//取得產品相關資訊
			$Categories = getCategories($Product[web_x_product_id]);
			$_Categories = _getCategories($Categories[web_xxx_product_id]);
			
			$saleTitle = null;
			//銷售組合
			if($Product[web_x_sale_id] && !$_SESSION["MyShopping"][$x]["additional"]) {
				$Sale = getSale($Product[web_x_sale_id]);
				//echo $Product[web_x_sale_id]."</br>";
				//取是否為同一組合全部數量
				$SaleShoppingCarNum = SearchMultidimensionalArray_2($_SESSION["MyShopping"], 'saleId', $Product[web_x_sale_id]);
				if(count($Sale)) {
					if($num % $Sale[rows] == 0 || $SaleShoppingCarNum % $Sale[rows] == 0) {
						if($Sale[price]) {

							//$Product[price] = floor($Sale[price] / $Sale[rows]);

						} else if($Sale[discountnum] < 100 && !$Sale[price]) { 	

							$Product[price] = round($Product[price] * $Sale[discountnum] / 100);

						}
						$saleTitle = $Sale['subject'];
						
					}
					$saleTitle = $Sale['subject'];
				}
			}

			$Product[link] = ($additionalText) ? null : $Product[link];    //為加購 不帶連結
			$Product[price] = ($additionalText) ? $Product[additionalprice] : $Product[price];    //為加購 顯示加購價

			$pic = ShowPic($Product[Covers], "uploadfiles/s/", "uploadfiles/no_image.jpg");

			$web_product_list .= "
			<p class=\"cpro\">
        	 	<!--產品代表圖--> 
				<a".$Product[link]." class=\"active\">
					<img class=\"pull-left\" src=\"".$pic."\" alt=\"".$Product[subject]."\" width=\"60px;\">
				</a>
				<a".$Product[link]." class=\"color_dark m_bottom_5 d_block active\">
					<span class=\"pull-left cprot\">".$additionalText.$Product[subject]."</span>
				</a>
				";
			if($saleTitle) 
				$web_product_list .= "        
                    <p class=\"pro_active\" style=\"font-size:14px;\"><span>活動</span>".$saleTitle."</p>";
            $web_product_list .= "		
				<span class=\"pull-left cproc\">".$num."</span>
				<span class=\"pull-left\">X</span>
				<strong class=\"pull-right\">$".number_format($Product[price])."</strong>
			</p>
			";
			/*
			$web_product_list .= " 
			<tr>
                <td>
                    <a".$Product[link]." class=\"pad0\" title=\"".$Product[subject]."\">
                        <img src=\"".$pic."\" alt=\"".$Product[subject]."\" width=\"60px;\">
                    </a>
                </td>
                <td class=\"td_style2\" style=\"text-align:left;\">
                    <a".$Product[link]." class=\"pad0\" style=\"line-height:1.7;\" title=\"".$Product[subject]."\">".$additionalText.$Product[subject]."
                    </a>";
            if($saleTitle) 
				$web_product_list .= "        
                    <p class=\"pro_active\" style=\"font-size:14px;\"><span>活動</span>".$saleTitle."</p>";
            $web_product_list .= "    
                </td>
                <td>".$num." x</td>
                <td>$".number_format($Product[price])."</td>                   
            </tr>";
            */
            /*1051220修改*/
            /*
			if($_Categories[web_xxxx_product_id] == '5')
				$web_product_list .= "<span class=\"promoitem\"><i class=\"i\"></i><a href=\"friend.html\" title=\"".str_front($_Categories[web_xxxx_product_subject])."\">".str_front($_Categories[web_xxxx_product_subject])."</a></span>";
			if($saleTitle) 
				$web_product_list .= "<span class=\"promoitem\"><i class=\"i\">商品適用優惠促銷</i><a href=\"sale_list.php?kind=".str_front($Product["web_x_sale_id"])."\" title=\"".str_front($Product[saleTitle])."\">".$saleTitle."</a></span>";
			$web_product_list .= "
			</p>";
			*/       
		}
	}
	return $web_product_list;
}

function _SendOrder($ordernum, $root) {
	if ($ordernum=="") return;

	$sql = "Select Init_WebTitle, Init_OrderEmail From web_basicset ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_edit($row[$_key]);
	}

	//訂單資訊
	$sql = "Select * From web_x_order Where ordernum like '".$ordernum."' order by web_x_order_id desc limit 1 ";
	$rs = ConnectDB($DB, $sql);
	if (mysqli_num_rows($rs)==0) return;
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
		if ($levelremark!="") $levelremark = "（".$levelremark."）　";
	}

	//訂單詳細
	$web_order_list = "";
	$sql = "
		Select 
			web_order.web_x_sale_id, 
			web_order.subject, 
			web_order.serialnumber, 
			web_order.additional,
			web_order.dimension, 
			web_order.price, 
			web_order.num,
			web_order.masterId,
			web_x_order.saleAry as web_x_order_saleAry 
		From 
			web_order 
		Left Join
			web_x_order
		On
			web_x_order.ordernum = web_order.web_x_order_ordernum	
		Where 
			web_order.web_x_order_ordernum like '".$ordernum."' 
		order by 
			web_order.web_order_id asc";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		$_saleAry = json_decode($row[web_x_order_saleAry], true);
        $__saleAry[$row[web_x_sale_id]] = $_saleAry[$row[web_x_sale_id]];
        $saleTitle = ($_saleAry[$row[web_x_sale_id]]) ? urldecode($_saleAry[$row[web_x_sale_id]]['title']) : null;

		//$saleTitle = ($Sale['subject']) ? $Sale['subject'] : null;
		$web_order_list .= "
  <tr>
    <td>".str_front($row["subject"]);
  	if($saleTitle) 
			$web_order_list .= "<br/><b style='color:red'>".$saleTitle."</b>";  	
   	$web_order_list .= "<br />".str_front($row["serialnumber"]);
	if($row[additional] && $row[masterId]) {
		if(!$row[fullAdditional]) {
			$masterId = json_decode($row[masterId], ture);
			$web_order_list .= "<br/>加購主商品";
			foreach($masterId as $rowMasterId) {
				$_Product = getProduct($rowMasterId);
				$web_order_list .= "<br/>".$_Product[subject];
				//$shopping_list .= "<br/><a".$_Product[link]." style=\"font-size:smaller\">".$_Product[subject]."</a>";
			} 
		}  
    }
    $web_order_list .= "</td>";
    $web_order_list .= "
    <td align=\"center\">".str_front($row["dimension"])."</td>
    <td align=\"center\">$".number_format($row["price"])."</td>
    <td align=\"center\">".number_format($row["num"])."</td>
    <td align=\"center\">$".number_format($row["price"] * $row["num"])."</td>
  </tr>";
	}
	
	//寄信
	$subjects = $Init_WebTitle." - 訂單確認信 - 訂單編號".$ordernum;	//郵件主旨
	$body = "
<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\">
<head>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
<title>訂單確認信</title>
<style type=\"text/css\">
table { border-collapse: collapse; border-spacing: 0;}
body, td, th { margin: 25px; line-height: 1.5; color: #333; }
</style>
</head>

<body>
".$order_name.$order_sex."您好：<br /><br />
感謝您的訂購，訂單編號為".$ordernum."，明細如下：<br /><br />
<table width=\"100%\" border=\"1\" cellpadding=\"5\" cellspacing=\"0\">
  <tr>
    <th>品名</th>
    <th width=\"20%\">規格</th>
    <th width=\"10%\">單價</th>
    <th width=\"10%\">數量</th>
    <th width=\"10%\">價格</th>
  </tr>
  ".$web_order_list."";
if(count($__saleAry)) {
	foreach($__saleAry as $saleRow) {

		$_saleTotal = ($saleRow[total] >= $saleRow['salePrice']) ? $saleRow[total] : $saleRow['salePrice'];
		$_saleNum = ($saleRow[num] >= $saleRow['saleRow']) ? $saleRow[num] : $saleRow['num'];
		$_saleRow = floor($saleRow[num] / $saleRow['saleRow']) ? floor($saleRow[num] / $saleRow['saleRow']) : 1;

		$minus = $_saleTotal - ($saleRow['salePrice'] * ($_saleRow));
		if(!$saleRow['title']) {
			continue;	
		}
		
		$body .= "
		<tr>
			<td colspan=\"4\" align=\"right\"><p class=\"fw_medium f_size_large t_align_r t_xs_align_c\">".urldecode($saleRow['title'])."：</p></td>
			<td align=\"right\"><p class=\"fw_medium f_size_large color_dark\">-$<span id=\"new_subtotal\">".number_format($minus)."</span></p></td>
		</tr>";
	}
	//$minusTotal += $minus;
}
  $body .= "		  
  <tr>
    <td colspan=\"4\" align=\"right\">".$levelremark."小計：</td>
    <td align=\"right\">$".number_format($subtotal)."</td>
  </tr>";

//優惠代碼
if ($web_code_money>0) {
	$body .= "
  <tr>
    <td colspan=\"4\" align=\"right\">優惠代碼（".$web_code_subject."）：</td>
    <td align=\"right\">-$".number_format($web_code_money)."</td>
  </tr>";
}

//使用紅利來源
if ($web_bonus_from_money>0) {
	$body .= "
  <tr>
    <td colspan=\"4\" align=\"right\">紅利折扣：<br />".$web_bonus_from_content."</td>
    <td align=\"right\">-$".number_format($web_bonus_from_money)."</td>
  </tr>";
}
//使用紅利來源
if ($web_member_from_money>0) {
	$body .= "
  <tr>
    <td colspan=\"4\" align=\"right\">購物金抵扣：<br /></td>
    <td align=\"right\">-$".number_format($web_member_from_money)."</td>
  </tr>";
}
//員工不計運費及不可用優惠代碼----------------------------------------------------------------------------------------
if($_SESSION['MyMember']['level'] !== '4') {
	$body .= "
	  <tr>
	    <td colspan=\"4\" align=\"right\">運送地點：".$web_x_freight_subject."　運費：</td>
	    <td align=\"right\">$".number_format($freight)."</td>
	  </tr>
	  <tr>
	    <td colspan=\"4\" align=\"right\">總計：</td>
	    <td align=\"right\">$".number_format($subtotal-$web_code_money-$web_bonus_from_money-$web_member_from_money+$freight)."</td>
	  </tr>";
}
//贈品內容
if ($web_x_gift_content) {
	$body .= "
  <tr>
    <td colspan=\"5\" align=\"left\">".$web_x_gift_content."</td>
  </tr>";
}

//本單贈送紅利內容
if ($web_bonus_content) {
	$body .= "
  <tr>
    <td colspan=\"5\" align=\"left\">".nl2br($web_bonus_content)."</td>
  </tr>";
}


if ($payment=="ATM轉帳") $payment .= "<br />帳號後五碼：".$fiveyards;
if ($invoicetype=="三聯式發票") $invoicetype .= "<br />發票抬頭：".$invoicetitle."<br />統一編號：".$invoicenum;
if ($remark!="") $remark = "<br /><br />備註：<br />".nl2br($remark);
if ($accept_overseas==0) {
	$address = $accept_city.$accept_area.$accept_zip.$accept_address;
} else {
	$address = "（運送至國外）".$accept_foreignaddress;
}

$body .= "
</table>
<br />
<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">
  <tr>
    <td width=\"33%\" align=\"left\" valign=\"top\"><strong>付款方式</strong><br />".$payment."</td>
    <td width=\"34%\" align=\"left\" valign=\"top\"><strong>配送方式</strong><br />".$transport;
if($transport == '超商取貨') {    
$body .=   
    "【".$store."】<br /><span style='color:#da88b1'>".str_replace("|", " ", $CVSStoreIDInfo)."</span></td>";
} else {
$body .=   
    "<br />方便到貨時段：".$accept_time."</td>";	
}    
$body .=  "    
    <td width=\"33%\" align=\"left\" valign=\"top\"><strong>發票資訊</strong><br />".$invoicetype."</td>
  </tr>
</table>
<br /><hr /><br />
<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">
  <tr>
    <td width=\"50%\" align=\"left\" valign=\"top\">
      <strong>訂購人</strong><br />
      姓名：".$order_name."<br />
      聯絡電話：".$order_tel."<br />
      手機號碼：".$order_mobile."<br />
      電子信箱：".$order_email."
    </td>
    <td align=\"left\" valign=\"top\">
      <strong>收件人</strong><br />
      姓名：".$accept_name."<br />
      聯絡電話：".$accept_tel."<br />
      手機號碼：".$accept_mobile."<br />
      地址：".$address."
	  ".$remark."
    </td>
  </tr>
</table>
<br /><hr /><br />
<a href=\"".setWeb()."\">".$Init_WebTitle."</a>敬上
</body>
</html>";

	$Init_OrderEmail_array = explode(",", $Init_OrderEmail, 2);
/*
	require($root."class/Mustache/Autoloader.php");

	$mail_file_location = './Templates/mail_Order.html';
	if(!file_exists($mail_file_location)) {
		
		RunJs("./index.html", "<b>信件發送失敗, 請稍後片刻系統正在執行中......</b>");

	} else {
		echo $tpl = file_get_contents($mail_file_location);
		exit;
		$mailInfo = array(
			'WEBURL' 			=> "http://".$_SERVER['HTTP_HOST'],
			'SENDDATE'			=> date("Y-m-j h:i:s a"),
			'IP'				=> getenv('REMOTE_ADDR'),

			'NAME'				=> $uname,
			'EMAIL'				=> $email,
			'TEXTAREA'			=> ($content) ? nl2br($content) : '',

			'COMPANYNAME'		=> $Init_WebTitle,
			'COMPANYTEL'		=> $info_array[2],
			'COMPANYADDRESS'	=> $info_array[0],
			'COMPANYMAIL'		=> $info_array[1],
			'COMPANYFAX'		=> $info_array[3],
			
		);


		$mailSubject = "".$uname." 線上諮詢";

		Mustache_Autoloader::register();
		$m = new Mustache_Engine;

		//array_push($options['email'], $email);
		//render the template with the set values
		$mailTemplate = $m->render($tpl, $mailInfo);
	}	

	exit;
*/	
	$MailInfo[root] = $root;						//路徑
	$MailInfo[subject] = $subjects;					//信件主旨
	$MailInfo[body] = $body;						//信件內容
	$MailInfo[FromMail] = $Init_OrderEmail_array[0];//寄件人信箱(無作用)
	$MailInfo[FromName] = $Init_WebTitle;			//寄件人名稱
	$MailInfo[ToMail] = $order_email;				//收件人信箱
	$MailInfo[ToName] = $order_name.$order_sex;		//收件人名稱
	$MailInfo[BccMail] = $Init_OrderEmail;			//密件副本
	$MailInfo[ReplyMail] = "";			//回信人信箱
	$MailInfo[ReplyName] = "";			//回信人名稱
	$message = SendMail($MailInfo);
}

function SendOrder($ordernum, $root) {
	if ($ordernum=="") return;
	include_once($root."class/Mustache/Autoloader.php");

	$sql = "Select Init_WebTitle, Init_OrderEmail, Init_Email, Init_Contact From web_basicset ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_edit($row[$_key]);
	}

	//訂單資訊
	$sql = "Select * From web_x_order Where ordernum like '".$ordernum."' order by web_x_order_id desc limit 1 ";
	$rs = ConnectDB($DB, $sql);
	$web_x_order_ary = array();
	$targetAry = array(
		'REMARK', 
		'CVSSTOREIDINFO',
		'ACCEPT_TIME',
		'INVOICETYPE',
		'PAYMENT'
	);
	if (mysqli_num_rows($rs)==0) return;
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key => $_value) {
			if(in_array(strtoupper($_key), $targetAry)) {
				switch(strtoupper($_key)) {
					case 'REMARK':
						$web_x_order_ary[strtoupper($_key)] = ($row[$_key]) ? nl2br($row[$_key]) : '';
					break;
					
					case 'CVSSTOREIDINFO':
						$web_x_order_ary[strtoupper($_key)] = str_replace('|', ' ', $row[$_key]);
					break;

					case 'ACCEPT_TIME':
						$web_x_order_ary[strtoupper($_key)] = ($row[transport] != '宅配') ? '' : $row[$_key];
					break;

					case 'INVOICETYPE':
						$web_x_order_ary[strtoupper($_key)] = $row[$_key];
						/*
						if($row[$_key] == '二聯捐贈發票')
							$web_x_order_ary[strtoupper($_key)] = $row[$_key]."【".$Init_InvoiceDonate."】";
						*/
						$web_x_order_ary[INVOICESHOW] = ($row[$_key] != '個人電子發票(兩聯式)' && $row[$_key] != '二聯電子發票' && $row[$_key] != '二聯捐贈發票') ? 1 : 0;
					break;	

					case 'PAYMENT':
						$web_x_order_ary[strtoupper($_key)] = $row[$_key];
						if($row[$_key] == 'ATM虛擬帳號') {
							$responseInfo = json_decode($responseInfo, true);
							if($responseInfo)
								$web_x_order_ary[ATMCODE] = "台新銀行 812 - ". "【".urldecode($responseInfo[EntityATM])."】";	
						} else if($row[$_key] == '超商代碼') {
							$web_x_order_ary[PAYCODE] = ($payCode) ? "【".$payCode."】" : '';
						}	
					break;
				}	
			} else {
				$web_x_order_ary[strtoupper($_key)] = $row[$_key];
			}
			$$_key = str_front($row[$_key]);
		}
		if ($levelremark!="") $levelremark = "（".$levelremark."）　";
	}
	
	//訂單詳細
	$web_order_list = "";
	$sql = "
		Select 
			web_order.web_x_sale_id,
			web_order.web_product_id,
			web_order.subject, 
			web_order.serialnumber, 
			web_order.fullAdditional,
			web_order.additional,
			web_order.dimension, 
			web_order.price, 
			web_order.num,
			web_order.masterId,
			web_x_order.saleAry as web_x_order_saleAry 
		From 
			web_order 
		Left Join
			web_x_order
		On
			web_x_order.ordernum = web_order.web_x_order_ordernum	
		Where 
			web_order.web_x_order_ordernum like '".$ordernum."' 
		order by 
			web_order.web_order_id asc";
	$rs = ConnectDB($DB, $sql);
	$saleAllNum = getSaleAllNumBackend($ordernum);
	$surplusNum = 0;
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		$_saleAry = json_decode($row[web_x_order_saleAry], true);
        $__saleAry[$row[web_x_sale_id]] = $_saleAry[$row[web_x_sale_id]];
        $saleTitle = ($_saleAry[$row[web_x_sale_id]] && !$row[additional]) ? urldecode($_saleAry[$row[web_x_sale_id]]['title']) : null;
        $__Product = getProduct($row[web_product_id]);
        $Categories = getCategories($__Product[web_x_product_id]);
		$_Categories = _getCategories($Categories[web_xxx_product_id]);
		$_Product = getProduct($row[web_product_id]);
		$showFlag = 0;
        if($__saleAry[$row[web_x_sale_id]][saleDiscountnum] && !$row[additional]) {
          if($saleAllNum[$row[web_x_sale_id]]) {
            $numAry[$row[web_x_sale_id]][$row[web_product_id]] = $row[num];
            $SaleRows[$row[web_x_sale_id]] =  $__saleAry[$row[web_x_sale_id]][saleRow];

            $saleAllNum[$row[web_x_sale_id]] = ($saleAllNum[$row[web_x_sale_id]] >= $SaleRows[$row[web_x_sale_id]]) ? $saleAllNum[$row[web_x_sale_id]] - ($saleAllNum[$row[web_x_sale_id]] % $SaleRows[$row[web_x_sale_id]]) : $saleAllNum[$row[web_x_sale_id]];

            if(array_sum($numAry[$row[web_x_sale_id]]) <= $saleAllNum[$row[web_x_sale_id]]) {
              $lastNum[$row[web_x_sale_id]] = array_sum($numAry[$row[web_x_sale_id]]);
              $test[slaePriceTotal] = ($_Product[price_cost] * ( $numAry[$row[web_x_sale_id]][$row[web_product_id]]));
              $test[truePriceTotal] = 0;
            } else {
              //$lastNum = array_sum($numAry);
              $canDoNum[$row[web_x_sale_id]] = ($saleAllNum[$row[web_x_sale_id]] - $lastNum[$row[web_x_sale_id]]);
              $lastNum[$row[web_x_sale_id]] += $canDoNum[$row[web_x_sale_id]];
              if($saleAllNum[$row[web_x_sale_id]] >= $lastNum[$row[web_x_sale_id]]) {
                $test[slaePriceTotal] = ($_Product[price_cost] * ($canDoNum[$row[web_x_sale_id]]));
                $test[truePriceTotal] = ($_Product[price] * ($numAry[$row[web_x_sale_id]][$row[web_product_id]] - $canDoNum[$row[web_x_sale_id]]));
              } else {
                $test[slaePriceTotal] = 0;
                $test[truePriceTotal] = ($_Product[price] * ($numAry[$row[web_x_sale_id]][$row[web_product_id]]));
              } 
              //echo array_sum($test)."--".$num;
            }
            $showFlag = 1;  
          }
        }

        $test[allPriceTotal] = $test[slaePriceTotal] + $test[truePriceTotal];
		/*
        $IPA =  getenv('REMOTE_ADDR');
		$ipList = array(
			'118.170.45.55',
		);

		if (in_array($IPA, $ipList)){
			echo "<pre>";
			print_r($_Categories);
			echo "</pre>";
			exit;
		}
		*/
        $web_order_list .= "
			<tr bgcolor=\"#ffffff\">
				<td height=\"30\" style=\"font-family: 微軟正黑體;font-size: 13px;color: #666666;line-height: 20px;letter-spacing: 1px;\" align=\"left\">".str_front($row["subject"]);
					if($_Categories[web_xxxx_product_id] == '5')
						$web_order_list .= "<br/><b style='color:red'>".str_front($_Categories[web_xxxx_product_subject])."</b>";
					if($saleTitle) 
						$web_order_list .= "<br/><b style='color:red'>".$saleTitle."</b>";  	
					//$web_order_list .= "<br />".str_front($row["serialnumber"]);
					if($row[additional] && $row[masterId]) {
						if(!$row[fullAdditional]) {
							$masterId = json_decode($row[masterId], ture);
							$web_order_list .= "<br/><span style=\"background: #f3f3f3;border-top: dashed 1px #DA88B1;display: block;font-weight: bold;\">加購主商品</span>";
							foreach($masterId as $rowMasterId) {
								$_Product = getProduct($rowMasterId);
								$web_order_list .= $_Product[subject]."<br/>";
								//$shopping_list .= "<br/><a".$_Product[link]." style=\"font-size:smaller\">".$_Product[subject]."</a>";
							}
  						}
					}	
			$web_order_list .= "</td>"; 		
			$web_order_list .= "
				<td height=\"30\" style=\"font-family: 微軟正黑體;font-size: 13px;color: #666666;line-height: 20px;letter-spacing: 1px;\" align=\"center\">$".number_format($row["price"])."</td>
				<td height=\"30\" style=\"font-family: 微軟正黑體;font-size: 13px;color: #666666;line-height: 20px;letter-spacing: 1px;\" align=\"center\">".number_format($row["num"])."</td>";
			if(!$showFlag) {
				$web_order_list .= "	
					<td height=\"30\" style=\"font-family: 微軟正黑體;font-size: 13px;color: #666666;line-height: 20px;letter-spacing: 1px;\" align=\"center\">$".number_format($row["price"] * $row["num"])."</td>";
			} else {
				$web_order_list .= "	
					<td height=\"30\" style=\"font-family: 微軟正黑體;font-size: 13px;color: #666666;line-height: 20px;letter-spacing: 1px;\" align=\"center\">$".number_format($test[allPriceTotal])."</td>";	
			}	
			$web_order_list .= "
			</tr>
		";
	}
	//贈品內容
	
	if ($web_x_gift_content) {
		//$web_x_gift_content_ary = explode('、', str_replace('贈品：', '', $web_x_gift_content));
		$web_x_gift_content_ary = json_decode($web_x_gift_content, true);
		foreach($web_x_gift_content_ary as $key => $gift) {
			//扣除贈品庫存	
			$giftStock = ($gift[stock]) ? 1 : '贈品發送完畢';
			$web_order_list .= "
				<tr bgcolor=\"#ffffff\">
					<td height=\"30\" style=\"font-family: 微軟正黑體;font-size: 13px;color: #666666;line-height: 20px;letter-spacing: 1px;\" align=\"left\">贈品：".(str_front($gift[subject]));
				$web_order_list .= "</td>"; 		
				$web_order_list .= "
					<td height=\"30\" style=\"font-family: 微軟正黑體;font-size: 13px;color: #666666;line-height: 20px;letter-spacing: 1px;\" align=\"center\">$0</td>
					<td height=\"30\" style=\"font-family: 微軟正黑體;font-size: 13px;color: #666666;line-height: 20px;letter-spacing: 1px;\" align=\"center\">".$giftStock."</td>
					<td height=\"30\" style=\"font-family: 微軟正黑體;font-size: 13px;color: #666666;line-height: 20px;letter-spacing: 1px;\" align=\"center\">$0</td>
				";
				$web_order_list .= "
				</tr>
			";
		}
			
	}	
	
	if(count($__saleAry)) {
		foreach($__saleAry as $saleRow) {
			/*
			$_saleTotal = ($saleRow[total] >= $saleRow['salePrice']) ? $saleRow[total] : $saleRow['salePrice'];
			$_saleNum = ($saleRow[num] >= $saleRow['saleRow']) ? $saleRow[num] : $saleRow['num'];
			$_saleRow = floor($saleRow[num] / $saleRow['saleRow']) ? floor($saleRow[num] / $saleRow['saleRow']) : 1;
			
			$minus = $_saleTotal - ($saleRow['salePrice'] * ($_saleRow));
			*/
			if($saleRow[eventType] != '2') {
				if($saleRow['salePrice']) {
					$_saleTotal = ($saleRow[total] >= $saleRow['salePrice']) ? $saleRow[total] : $saleRow['salePrice'];
					$_saleNum = ($saleRow[num] >= $saleRow['saleRow']) ? $saleRow[num] : $saleRow['num'];
					$_saleRow =  floor($saleRow[num] / $saleRow['saleRow']);
					$minus = $_saleTotal - ($saleRow['salePrice'] * ($_saleRow));
				} else {
					$_saleTotal = ($saleRow[total] >= $saleRow['salePrice']) ? $saleRow[total] : $saleRow['salePrice'];
					$_saleNum = ($saleRow[num] >= $saleRow['saleRow']) ? $saleRow[num] : $saleRow['num'];
					$_saleRow =  floor($saleRow[num] / $saleRow['saleRow']);
					$minus = $_saleTotal - round(($_saleTotal * $saleRow['saleDiscountnum']) / 100);
				}
			} else {
				//$minus = $saleRow[minusFreight];
				$minus = 0;
			}	

			if(!$saleRow['title']) {
				continue;	
			}
			if($saleRow[eventType] != '2') {
				$web_order_list .= "
					<tr bgcolor=\"#ffffff\">
						<td height='30' style='font-family: 微軟正黑體;font-size: 13px;color: #DA88B1;line-height: 20px;letter-spacing: 1px;' align='right' colspan='3'><span style=\"border: 1px solid #e2e2e6;font-size: 13px;padding: 1px;margin-right: 5px;color:#0598A2;font-weight: bold;\">優惠</span>".urldecode($saleRow['title'])."</td>
						<td height='30' style='font-family: 微軟正黑體;font-size: 13px;color: #DA88B1;line-height: 20px;letter-spacing: 1px;' align='right'>-$".number_format($minus)."</td>
					</tr>
				";
			}	
		}
		//$minusTotal += $minus;
	}

	$web_order_list .= "		  
		<tr bgcolor=\"#ffffff\">
			<td height='30' style='font-family: 微軟正黑體;font-size: 13px;color: #666666;line-height: 20px;letter-spacing: 1px;' align='right' colspan='3'>".$levelremark."小計：</td>
			<td height='30' style='font-family: 微軟正黑體;font-size: 13px;color: #666666;line-height: 20px;letter-spacing: 1px;' align='right'>$".number_format($subtotal)."</td>
		</tr>
	";

	//優惠代碼
	if ($web_code_money>0) {
		$web_order_list .= "
			<tr bgcolor=\"#ffffff\">
				<td height='30' style='font-family: 微軟正黑體;font-size: 13px;color: #DA88B1;line-height: 20px;letter-spacing: 1px;' align='right' colspan='3'>優惠代碼（".$web_code_subject."）：</td>
				<td height='30' style='font-family: 微軟正黑體;font-size: 13px;color: #DA88B1;line-height: 20px;letter-spacing: 1px;' align='right'>-$".number_format($web_code_money)."</td>
			</tr>
		";
	}

	//使用紅利來源
	if ($web_bonus_from_money>0) {
		$web_order_list .= "
			<tr bgcolor=\"#ffffff\">
				<td height='30' style='font-family: 微軟正黑體;font-size: 13px;color: #DA88B1;line-height: 20px;letter-spacing: 1px;' align='right' colspan='3'>紅利折扣：<br />".$web_bonus_from_content."</td>
				<td height='30' style='font-family: 微軟正黑體;font-size: 13px;color: #DA88B1;line-height: 20px;letter-spacing: 1px;' align='right'>-$".number_format($web_bonus_from_money)."</td>
			</tr>
		";
	}

	//使用購物金來源
	if ($web_member_from_money>0) {
		$web_order_list .= "
			<tr bgcolor=\"#ffffff\">
				<td height='30' style='font-family: 微軟正黑體;font-size: 13px;color: #DA88B1;line-height: 20px;letter-spacing: 1px;' align='right' colspan='3'>購物金抵扣：<br /></td>
				<td height='30' style='font-family: 微軟正黑體;font-size: 13px;color: #DA88B1;line-height: 20px;letter-spacing: 1px;' align='right'>-$".number_format($web_member_from_money)."</td>
			</tr>
		";
	}

	//使用優惠券來源
	if ($web_coupon_from_money>0) {
		$Coupon = getCoupon($web_x_coupon_id);
		$web_order_list .= "
			<tr bgcolor=\"#ffffff\">
				<td height='30' style='font-family: 微軟正黑體;font-size: 13px;color: #DA88B1;line-height: 20px;letter-spacing: 1px;' align='right' colspan='3'>優惠券【".$Coupon[subject]."】：<br /></td>
				<td height='30' style='font-family: 微軟正黑體;font-size: 13px;color: #DA88B1;line-height: 20px;letter-spacing: 1px;' align='right'>-$".number_format($web_coupon_from_money)."</td>
			</tr>
		";
	}

	//貨到付款
	if ($Init_Payment_CashOnDelivery2_price) {
		$web_order_list .= "
			<tr bgcolor=\"#ffffff\">
				<td height='30' style='font-family: 微軟正黑體;font-size: 13px;color: #DA88B1;line-height: 20px;letter-spacing: 1px;' align='right' colspan='3'>貨到付款：<br /></td>
				<td height='30' style='font-family: 微軟正黑體;font-size: 13px;color: #DA88B1;line-height: 20px;letter-spacing: 1px;' align='right'>$".number_format($Init_Payment_CashOnDelivery2_price)."</td>
			</tr>
		";
	}

	//員工不計運費及不可用優惠代碼----------------------------------------------------------------------------------------
	if($_SESSION['MyMember']['level'] !== '4') {
		$web_order_list .= "
			<tr bgcolor=\"#ffffff\">
				<td height='30' style='font-family: 微軟正黑體;font-size: 13px;color: #666666;line-height: 20px;letter-spacing: 1px;' align='right' colspan='3'>運送地點：".$web_x_freight_subject."　運費：</td>
				<td height='30' style='font-family: 微軟正黑體;font-size: 13px;color: #666666;line-height: 20px;letter-spacing: 1px;' align='right'>$".number_format($freight)."</td>
			</tr>
			<tr bgcolor=\"#ffffff\">
				<td height='30' style='font-family: 微軟正黑體;font-size: 13px;color: #666666;line-height: 20px;letter-spacing: 1px;' align='right' colspan='3'>總計：</td>
				<td height='30' style='font-family: 微軟正黑體;font-size: 13px;color: #666666;line-height: 20px;letter-spacing: 1px;' align='right'>$".number_format($subtotal-$web_code_money-$web_bonus_from_money-$web_member_from_money-$web_coupon_from_money+$freight+$Init_Payment_CashOnDelivery2_price)."</td>
			</tr>";	
	}

	//贈品內容
	/*
	if ($web_x_gift_content) {
		$web_order_list .= "
			<tr bgcolor='#ffffff'>
				<td height='30' style='font-family: 微軟正黑體;font-size: 13px;color: #666666;line-height: 20px;letter-spacing: 1px;' align='left' colspan='4'>".$web_x_gift_content."</td>
			</tr>
		";
	}
	*/
	//本單贈送紅利內容
	if ($web_bonus_content && 0) {
		$web_order_list .= "
			<tr bgcolor='#ffffff'>
				<td height='30' style='font-family: 微軟正黑體;font-size: 13px;color: #666666;line-height: 20px;letter-spacing: 1px;' align='left' colspan='4'>".nl2br($web_bonus_content)."</td>
			</tr>
		";
	}

	$web_x_order_ary[PRODUCTLIST] = $web_order_list;	

	$mail_file_location = './Templates/mail_Order.html';
	if(!file_exists($mail_file_location)) {
		
		//RunJs("./index.html", "<b>信件發送失敗, 請稍後片刻系統正在執行中......</b>");

	} else {
		$tpl = file_get_contents($mail_file_location);

		$contactInfo = explode('<span>', str_replace(array('<p>', '</p>', '</span>'), null, $Init_Contact));
		array_filter($contactInfo);
	
		$info_array[2] = preg_replace('/([\x80-\xff]*)/i','',$contactInfo[2]);
		$info_array[1] = preg_replace('/([\x80-\xff]*)/i','',$contactInfo[3]);

		$mailInfo = array(
			'WEBURL' 			=> "http://".$_SERVER['HTTP_HOST'],
			'SENDDATE'			=> date("Y-m-j h:i:s a"),
			'IP'				=> getenv('REMOTE_ADDR'),

			'DUEDATE'			=> ($payment == 'ATM虛擬帳號' || $payment == '超商代碼') ? $duedate : null,

			'COMPANYNAME'		=> $Init_WebTitle,
			'COMPANYTEL'		=> $info_array[2],
			'COMPANYADDRESS'	=> $info_array[0],
			'COMPANYMAIL'		=> $info_array[1],
			'COMPANYFAX'		=> $info_array[3],
			
		);

		$mailInfo += $web_x_order_ary;

		$mailSubject = "【".$ordernum."】".$order_name." 訂購產品成功通知信";

		Mustache_Autoloader::register();
		$m = new Mustache_Engine;

		//array_push($options['email'], $email);
		//render the template with the set values
		$mailTemplate = $m->render($tpl, $mailInfo);

		$Init_OrderEmail_array = explode(',', $Init_OrderEmail);

		$MailInfo[root] = $root;								//路徑
		$MailInfo[subject] = $mailSubject;						//信件主旨
		$MailInfo[body] = $mailTemplate;						//信件內容
		$MailInfo[FromMail] = $Init_OrderEmail_array[0];		//寄件人信箱(無作用)
		$MailInfo[FromName] = $Init_WebTitle;					//寄件人名稱
		$MailInfo[ToMail] = $order_email;						//收件人信箱
		$MailInfo[ToName] = $order_name.$order_sex;				//收件人名稱
		$MailInfo[BccMail] = $Init_OrderEmail;					//密件副本
		$MailInfo[ReplyMail] = "";								//回信人信箱
		$MailInfo[ReplyName] = "";								//回信人名稱

		$message = SendMail($MailInfo);
	}	

}

function stockLimitSend($prouctAry, $duedate, $root) {
	if (!count($prouctAry)) return;
	include_once($root."class/Mustache/Autoloader.php");

	$sql = "Select Init_WebTitle, Init_OrderEmail, Init_Email, Init_Contact From web_basicset ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_edit($row[$_key]);
	}
	if(!count($prouctAry)) {
		RunJs("./index.html", "<b>信件發送失敗, 請稍後片刻系統正在執行中......</b>");
	}

	foreach($prouctAry as $key => $product) {
		$web_order_list .= "
			<tr bgcolor=\"#ffffff\">
				<td height=\"30\" style=\"font-family: 微軟正黑體;font-size: 13px;color: #666666;line-height: 20px;letter-spacing: 1px;\" align=\"left\">
					".str_front($product["subject"]).
				"</td>
				<td height=\"30\" style=\"font-family: 微軟正黑體;font-size: 13px;color: #666666;line-height: 20px;letter-spacing: 1px;\" align=\"center\">
					".str_front($product["stock"]).
				"</td>
			</tr>	
		";
	}			
	
	$web_x_order_ary[PRODUCTLIST] = $web_order_list;    	

	$mail_file_location = './Templates/mail_StockLimit.html';
	if(!file_exists($mail_file_location)) {
		
		//RunJs("./index.html", "<b>信件發送失敗, 請稍後片刻系統正在執行中......</b>");


	} else {
		$tpl = file_get_contents($mail_file_location);

		$contactInfo = explode('<span>', str_replace(array('<p>', '</p>', '</span>'), null, $Init_Contact));
		array_filter($contactInfo);
	
		$info_array[2] = preg_replace('/([\x80-\xff]*)/i','',$contactInfo[2]);
		$info_array[1] = preg_replace('/([\x80-\xff]*)/i','',$contactInfo[3]);

		$mailInfo = array(
			'WEBURL' 			=> "http://".$_SERVER['HTTP_HOST'],
			'SENDDATE'			=> date("Y-m-j h:i:s a"),
			'IP'				=> getenv('REMOTE_ADDR'),

			'DUEDATE'			=> $duedate,
			'ALLTOTAL'			=> number_format($subtotal - $web_code_money - $web_bonus_from_money - $web_member_from_money + $freight),

			'COMPANYNAME'		=> $Init_WebTitle,
			'COMPANYTEL'		=> $info_array[2],
			'COMPANYADDRESS'	=> $info_array[0],
			'COMPANYMAIL'		=> $info_array[1],
			'COMPANYFAX'		=> $info_array[3],
			
		);

		$mailInfo += $web_x_order_ary;

		$mailSubject = "【".$Init_WebTitle."】 安全庫存量通知";

		Mustache_Autoloader::register();
		$m = new Mustache_Engine;

		//array_push($options['email'], $email);
		//render the template with the set values
		$mailTemplate = $m->render($tpl, $mailInfo);

		$Init_OrderEmail_array = explode(',', $Init_OrderEmail);

		$MailInfo[root] = $root;								//路徑
		$MailInfo[subject] = $mailSubject;						//信件主旨
		$MailInfo[body] = $mailTemplate;						//信件內容
		$MailInfo[FromMail] = $Init_OrderEmail_array[0];		//寄件人信箱(無作用)
		$MailInfo[FromName] = $Init_WebTitle;					//寄件人名稱
		$MailInfo[ToMail] = $Init_OrderEmail;					//收件人信箱
		$MailInfo[ToName] = $Init_WebTitle;						//收件人名稱
		$MailInfo[BccMail] = $Init_OrderEmail;					//密件副本
		$MailInfo[ReplyMail] = "";								//回信人信箱
		$MailInfo[ReplyName] = "";								//回信人名稱
		$message = SendMail($MailInfo);

		return $message;
		
	}	

}		

function DueDateSend($ordernum, $duedate, $root) {
	if ($ordernum=="") return;
	include_once($root."class/Mustache/Autoloader.php");

	$sql = "Select Init_WebTitle, Init_OrderEmail, Init_Email, Init_Contact From web_basicset ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_edit($row[$_key]);
	}

	//訂單資訊
	$sql = "Select * From web_x_order Where ordernum like '".$ordernum."' order by web_x_order_id desc limit 1 ";
	$rs = ConnectDB($DB, $sql);
	$web_x_order_ary = array();
	$targetAry = array(
		'REMARK', 
		'CVSSTOREIDINFO',
		'ACCEPT_TIME',
		'INVOICETYPE',
		'PAYMENT'
	);
	if (mysqli_num_rows($rs)==0) return;
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key => $_value) {
			if(in_array(strtoupper($_key), $targetAry)) {
				switch(strtoupper($_key)) {
					case 'REMARK':
						$web_x_order_ary[strtoupper($_key)] = ($row[$_key]) ? nl2br($row[$_key]) : '';
					break;
					
					case 'CVSSTOREIDINFO':
						$web_x_order_ary[strtoupper($_key)] = str_replace('|', ' ', $row[$_key]);
					break;

					case 'ACCEPT_TIME':
						$web_x_order_ary[strtoupper($_key)] = ($row[transport] != '宅配') ? '' : $row[$_key];
					break;

					case 'INVOICETYPE':
						$web_x_order_ary[strtoupper($_key)] = $row[$_key];
						$web_x_order_ary[INVOICESHOW] = ($row[$_key] != '二聯式發票') ? 1 : 0;
					break;
						
					case 'PAYMENT':
						$web_x_order_ary[strtoupper($_key)] = $row[$_key];
						if($row[$_key] == 'ATM虛擬帳號') {
							$responseInfo = json_decode($responseInfo, true);
							$web_x_order_ary[ATMCODE] = "台新銀行 812 - ". "【".urldecode($responseInfo[EntityATM])."】";	
						} else if($row[$_key] == '超商代碼') {
							$web_x_order_ary[PAYCODE] = ($payCode) ? "【".$payCode."】" : '';
						}	
					break;
				}	
			} else {
				$web_x_order_ary[strtoupper($_key)] = $row[$_key];
			}
			$$_key = str_front($row[$_key]);
		}
		if ($levelremark!="") $levelremark = "（".$levelremark."）　";
	}
	
	//訂單詳細
	$web_order_list = "";
	$sql = "
		Select 
			web_order.web_x_sale_id,
			web_order.web_product_id, 
			web_order.subject, 
			web_order.serialnumber, 
			web_order.fullAdditional,
			web_order.additional,
			web_order.dimension, 
			web_order.price, 
			web_order.num,
			web_order.masterId,
			web_x_order.saleAry as web_x_order_saleAry 
		From 
			web_order 
		Left Join
			web_x_order
		On
			web_x_order.ordernum = web_order.web_x_order_ordernum	
		Where 
			web_order.web_x_order_ordernum like '".$ordernum."' 
		order by 
			web_order.web_order_id asc";
	$rs = ConnectDB($DB, $sql);
	$saleAllNum = getSaleAllNumBackend($ordernum);
  	$surplusNum = 0;
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		$_saleAry = json_decode($row[web_x_order_saleAry], true);
        $__saleAry[$row[web_x_sale_id]] = $_saleAry[$row[web_x_sale_id]];
        $saleTitle = ($_saleAry[$row[web_x_sale_id]] && !$row[additional]) ? urldecode($_saleAry[$row[web_x_sale_id]]['title']) : null;

        $__Product = getProduct($row[web_product_id]);
        $Categories = getCategories($__Product[web_x_product_id]);
        $_Categories = _getCategories($Categories[web_xxx_product_id]);
        $_Product = getProduct($row[web_product_id]);
        $showFlag = 0;
        if($__saleAry[$row[web_x_sale_id]][saleDiscountnum] && !$row[additional]) {
          if($saleAllNum[$row[web_x_sale_id]]) {
            $numAry[$row[web_x_sale_id]][$row[web_product_id]] = $row[num];
            $SaleRows[$row[web_x_sale_id]] =  $__saleAry[$row[web_x_sale_id]][saleRow];

            $saleAllNum[$row[web_x_sale_id]] = ($saleAllNum[$row[web_x_sale_id]] >= $SaleRows[$row[web_x_sale_id]]) ? $saleAllNum[$row[web_x_sale_id]] - ($saleAllNum[$row[web_x_sale_id]] % $SaleRows[$row[web_x_sale_id]]) : $saleAllNum[$row[web_x_sale_id]];

            if(array_sum($numAry[$row[web_x_sale_id]]) <= $saleAllNum[$row[web_x_sale_id]]) {
              $lastNum[$row[web_x_sale_id]] = array_sum($numAry[$row[web_x_sale_id]]);
              $test[slaePriceTotal] = ($_Product[price_cost] * ( $numAry[$row[web_x_sale_id]][$row[web_product_id]]));
              $test[truePriceTotal] = 0;
            } else {
              //$lastNum = array_sum($numAry);
              $canDoNum[$row[web_x_sale_id]] = ($saleAllNum[$row[web_x_sale_id]] - $lastNum[$row[web_x_sale_id]]);
              $lastNum[$row[web_x_sale_id]] += $canDoNum[$row[web_x_sale_id]];
              if($saleAllNum[$row[web_x_sale_id]] >= $lastNum[$row[web_x_sale_id]]) {
                $test[slaePriceTotal] = ($_Product[price_cost] * ($canDoNum[$row[web_x_sale_id]]));
                $test[truePriceTotal] = ($_Product[price] * ($numAry[$row[web_x_sale_id]][$row[web_product_id]] - $canDoNum[$row[web_x_sale_id]]));
              } else {
                $test[slaePriceTotal] = 0;
                $test[truePriceTotal] = ($_Product[price] * ($numAry[$row[web_x_sale_id]][$row[web_product_id]]));
              } 
              //echo array_sum($test)."--".$num;
            }
            $showFlag = 1;  
          }
        }

        $test[allPriceTotal] = $test[slaePriceTotal] + $test[truePriceTotal];
        
        $web_order_list .= "
			<tr bgcolor=\"#ffffff\">
				<td height=\"30\" style=\"font-family: 微軟正黑體;font-size: 13px;color: #666666;line-height: 20px;letter-spacing: 1px;\" align=\"left\">".str_front($row["subject"]);
					if($_Categories[web_xxxx_product_id] == '5') //新朋友限定特價
          				$web_order_list .= "<br/><b style='color:red'>".str_front($_Categories[web_xxxx_product_subject])."</b>";
					if($saleTitle) 
						$web_order_list .= "<br/><b style='color:red'>".$saleTitle."</b>";  	
					//$web_order_list .= "<br />".str_front($row["serialnumber"]);
					if($row[additional] && $row[masterId]) {
						if(!$row[fullAdditional]) {
							$masterId = json_decode($row[masterId], ture);
							$web_order_list .= "<br/><span style=\"background: #f3f3f3;border-top: dashed 1px #DA88B1;display: block;font-weight: bold;\">加購主商品</span>";
							foreach($masterId as $rowMasterId) {
								$_Product = getProduct($rowMasterId);
								$web_order_list .= $_Product[subject]."<br/>";
								//$shopping_list .= "<br/><a".$_Product[link]." style=\"font-size:smaller\">".$_Product[subject]."</a>";
							}
  						}
					}	
			$web_order_list .= "</td>"; 		
			$web_order_list .= "
				<td height=\"30\" style=\"font-family: 微軟正黑體;font-size: 13px;color: #666666;line-height: 20px;letter-spacing: 1px;\" align=\"center\">$".number_format($row["price"])."</td>
				<td height=\"30\" style=\"font-family: 微軟正黑體;font-size: 13px;color: #666666;line-height: 20px;letter-spacing: 1px;\" align=\"center\">".number_format($row["num"])."</td>";
		if(!$showFlag) {
			$web_order_list .= "	
				<td height=\"30\" style=\"font-family: 微軟正黑體;font-size: 13px;color: #666666;line-height: 20px;letter-spacing: 1px;\" align=\"center\">$".number_format($row["price"] * $row["num"])."</td>";
		} else {
			$web_order_list .= "	
				<td height=\"30\" style=\"font-family: 微軟正黑體;font-size: 13px;color: #666666;line-height: 20px;letter-spacing: 1px;\" align=\"center\">$".number_format($test[allPriceTotal])."</td>";	
		}	
			$web_order_list .= "
			</tr>
		";
	}

	//贈品內容
	if ($web_x_gift_content) {
		$web_x_gift_content_ary = explode('、', str_replace('贈品：', '', $web_x_gift_content));
		foreach($web_x_gift_content_ary as $key => $gift) {
			$web_order_list .= "
				<tr bgcolor=\"#ffffff\">
					<td height=\"30\" style=\"font-family: 微軟正黑體;font-size: 13px;color: #666666;line-height: 20px;letter-spacing: 1px;\" align=\"left\">贈品：".str_front($gift);
				$web_order_list .= "</td>"; 		
				$web_order_list .= "
					<td height=\"30\" style=\"font-family: 微軟正黑體;font-size: 13px;color: #666666;line-height: 20px;letter-spacing: 1px;\" align=\"center\">$0</td>
					<td height=\"30\" style=\"font-family: 微軟正黑體;font-size: 13px;color: #666666;line-height: 20px;letter-spacing: 1px;\" align=\"center\">1</td>
					<td height=\"30\" style=\"font-family: 微軟正黑體;font-size: 13px;color: #666666;line-height: 20px;letter-spacing: 1px;\" align=\"center\">$0</td>
				";
				$web_order_list .= "
				</tr>
			";
		}	
	}	

	if(count($__saleAry)) {
		foreach($__saleAry as $saleRow) {
			/*
			$_saleTotal = ($saleRow[total] >= $saleRow['salePrice']) ? $saleRow[total] : $saleRow['salePrice'];
			$_saleNum = ($saleRow[num] >= $saleRow['saleRow']) ? $saleRow[num] : $saleRow['num'];
			$_saleRow = floor($saleRow[num] / $saleRow['saleRow']) ? floor($saleRow[num] / $saleRow['saleRow']) : 1;

			$minus = $_saleTotal - ($saleRow['salePrice'] * ($_saleRow));
			*/

			if($saleRow['salePrice']) {
				$_saleTotal = ($saleRow[total] >= $saleRow['salePrice']) ? $saleRow[total] : $saleRow['salePrice'];
				$_saleNum = ($saleRow[num] >= $saleRow['saleRow']) ? $saleRow[num] : $saleRow['num'];
				$_saleRow =  floor($saleRow[num] / $saleRow['saleRow']);
				$minus = $_saleTotal - ($saleRow['salePrice'] * ($_saleRow));
			} else {
				$_saleTotal = ($saleRow[total] >= $saleRow['salePrice']) ? $saleRow[total] : $saleRow['salePrice'];
				$_saleNum = ($saleRow[num] >= $saleRow['saleRow']) ? $saleRow[num] : $saleRow['num'];
				$_saleRow =  floor($saleRow[num] / $saleRow['saleRow']);
				$minus = $_saleTotal - round(($_saleTotal * $saleRow['saleDiscountnum']) / 100);
			}

			if(!$saleRow['title']) {
				continue;	
			}
			
			$web_order_list .= "
				<tr bgcolor=\"#ffffff\">
					<td height='30' style='font-family: 微軟正黑體;font-size: 13px;color: #DA88B1;line-height: 20px;letter-spacing: 1px;' align='right' colspan='3'><span style=\"border: 1px solid #e2e2e6;font-size: 13px;padding: 1px;margin-right: 5px;color:#0598A2;font-weight: bold;\">優惠</span>".urldecode($saleRow['title'])."</td>
					<td height='30' style='font-family: 微軟正黑體;font-size: 13px;color: #DA88B1;line-height: 20px;letter-spacing: 1px;' align='right'>-$".number_format($minus)."</td>
				</tr>
			";
		}
		//$minusTotal += $minus;
	}

	$web_order_list .= "		  
		<tr bgcolor=\"#ffffff\">
			<td height='30' style='font-family: 微軟正黑體;font-size: 13px;color: #666666;line-height: 20px;letter-spacing: 1px;' align='right' colspan='3'>".$levelremark."小計：</td>
			<td height='30' style='font-family: 微軟正黑體;font-size: 13px;color: #666666;line-height: 20px;letter-spacing: 1px;' align='right'>$".number_format($subtotal)."</td>
		</tr>
	";

	//優惠代碼
	if ($web_code_money>0) {
		$web_order_list .= "
			<tr bgcolor=\"#ffffff\">
				<td height='30' style='font-family: 微軟正黑體;font-size: 13px;color: #DA88B1;line-height: 20px;letter-spacing: 1px;' align='right' colspan='3'>優惠代碼（".$web_code_subject."）：</td>
				<td height='30' style='font-family: 微軟正黑體;font-size: 13px;color: #DA88B1;line-height: 20px;letter-spacing: 1px;' align='right'>-$".number_format($web_code_money)."</td>
			</tr>
		";
	}

	//使用紅利來源
	if ($web_bonus_from_money>0) {
		$web_order_list .= "
			<tr bgcolor=\"#ffffff\">
				<td height='30' style='font-family: 微軟正黑體;font-size: 13px;color: #DA88B1;line-height: 20px;letter-spacing: 1px;' align='right' colspan='3'>紅利折扣：<br />".$web_bonus_from_content."</td>
				<td height='30' style='font-family: 微軟正黑體;font-size: 13px;color: #DA88B1;line-height: 20px;letter-spacing: 1px;' align='right'>-$".number_format($web_bonus_from_money)."</td>
			</tr>
		";
	}

	//使用購物金來源
	if ($web_member_from_money>0) {
		$web_order_list .= "
			<tr bgcolor=\"#ffffff\">
				<td height='30' style='font-family: 微軟正黑體;font-size: 13px;color: #DA88B1;line-height: 20px;letter-spacing: 1px;' align='right' colspan='3'>購物金抵扣：<br /></td>
				<td height='30' style='font-family: 微軟正黑體;font-size: 13px;color: #DA88B1;line-height: 20px;letter-spacing: 1px;' align='right'>-$".number_format($web_member_from_money)."</td>
			</tr>
		";
	}

	//使用優惠券來源
	if ($web_coupon_from_money>0) {
		$Coupon = getCoupon($web_x_coupon_id);
		$web_order_list .= "
			<tr bgcolor=\"#ffffff\">
				<td height='30' style='font-family: 微軟正黑體;font-size: 13px;color: #DA88B1;line-height: 20px;letter-spacing: 1px;' align='right' colspan='3'>優惠券【".$Coupon[subject]."】：<br /></td>
				<td height='30' style='font-family: 微軟正黑體;font-size: 13px;color: #DA88B1;line-height: 20px;letter-spacing: 1px;' align='right'>-$".number_format($web_coupon_from_money)."</td>
			</tr>
		";
	}

	//員工不計運費及不可用優惠代碼----------------------------------------------------------------------------------------
	if($_SESSION['MyMember']['level'] !== '4') {
		$web_order_list .= "
			<tr bgcolor=\"#ffffff\">
				<td height='30' style='font-family: 微軟正黑體;font-size: 13px;color: #666666;line-height: 20px;letter-spacing: 1px;' align='right' colspan='3'>運送地點：".$web_x_freight_subject."　運費：</td>
				<td height='30' style='font-family: 微軟正黑體;font-size: 13px;color: #666666;line-height: 20px;letter-spacing: 1px;' align='right'>$".number_format($freight)."</td>
			</tr>
			<tr bgcolor=\"#ffffff\">
				<td height='30' style='font-family: 微軟正黑體;font-size: 13px;color: #666666;line-height: 20px;letter-spacing: 1px;' align='right' colspan='3'>總計：</td>
				<td height='30' style='font-family: 微軟正黑體;font-size: 13px;color: #666666;line-height: 20px;letter-spacing: 1px;' align='right'>$".number_format($subtotal-$web_code_money-$web_bonus_from_money-$web_member_from_money-$web_coupon_from_money+$freight)."</td>
			</tr>";	
	}

	//贈品內容
	if ($web_x_gift_content) {
		$web_order_list .= "
			<tr bgcolor='#ffffff'>
				<td height='30' style='font-family: 微軟正黑體;font-size: 13px;color: #666666;line-height: 20px;letter-spacing: 1px;' align='left' colspan='4'>".$web_x_gift_content."</td>
			</tr>
		";
	}

	//本單贈送紅利內容
	if ($web_bonus_content) {
		$web_order_list .= "
			<tr bgcolor='#ffffff'>
				<td height='30' style='font-family: 微軟正黑體;font-size: 13px;color: #666666;line-height: 20px;letter-spacing: 1px;' align='left' colspan='4'>".nl2br($web_bonus_content)."</td>
			</tr>
		";
	}

	$web_x_order_ary[PRODUCTLIST] = $web_order_list;    	

	$mail_file_location = './Templates/mail_Order_Payment.html';
	if(!file_exists($mail_file_location)) {
		
		//RunJs("./index.html", "<b>信件發送失敗, 請稍後片刻系統正在執行中......</b>");


	} else {
		$tpl = file_get_contents($mail_file_location);

		$contactInfo = explode('<span>', str_replace(array('<p>', '</p>', '</span>'), null, $Init_Contact));
		array_filter($contactInfo);
	
		$info_array[2] = preg_replace('/([\x80-\xff]*)/i','',$contactInfo[2]);
		$info_array[1] = preg_replace('/([\x80-\xff]*)/i','',$contactInfo[3]);

		$mailInfo = array(
			'WEBURL' 			=> "http://".$_SERVER['HTTP_HOST'],
			'SENDDATE'			=> date("Y-m-j h:i:s a"),
			'IP'				=> getenv('REMOTE_ADDR'),

			'DUEDATE'			=> $duedate,
			'ALLTOTAL'			=> number_format($subtotal - $web_code_money - $web_bonus_from_money - $web_member_from_money + $freight),

			'COMPANYNAME'		=> $Init_WebTitle,
			'COMPANYTEL'		=> $info_array[2],
			'COMPANYADDRESS'	=> $info_array[0],
			'COMPANYMAIL'		=> $info_array[1],
			'COMPANYFAX'		=> $info_array[3],
			
		);

		$mailInfo += $web_x_order_ary;

		$mailSubject = "【".$ordernum."】".$order_name." 繳費通知";

		Mustache_Autoloader::register();
		$m = new Mustache_Engine;

		//array_push($options['email'], $email);
		//render the template with the set values
		$mailTemplate = $m->render($tpl, $mailInfo);
		//exit;

		$Init_OrderEmail_array = explode(',', $Init_OrderEmail);

		$MailInfo[root] = $root;								//路徑
		$MailInfo[subject] = $mailSubject;						//信件主旨
		$MailInfo[body] = $mailTemplate;						//信件內容
		$MailInfo[FromMail] = $Init_OrderEmail_array[0];		//寄件人信箱(無作用)
		$MailInfo[FromName] = $Init_WebTitle;					//寄件人名稱
		$MailInfo[ToMail] = $order_email;						//收件人信箱
		$MailInfo[ToName] = $order_name.$order_sex;				//收件人名稱
		$MailInfo[BccMail] = $Init_OrderEmail;					//密件副本
		$MailInfo[ReplyMail] = "";								//回信人信箱
		$MailInfo[ReplyName] = "";								//回信人名稱
		$message = SendMail($MailInfo);

		return $message;
		
	}	

}	


//購物清單 shopping_car.php
function _ShoppingCarList($ifStop) {
	//現在在哪一頁
	$UrlArray = explode("/", htmlentities($_SERVER["PHP_SELF"]));
	$PageSelfURL = strtolower($UrlArray[sizeof($UrlArray)-1]);

	$ifEditor = ($ifStop==0) ? true : false;
	//$ifEditor = false;
	
	//是否已登入會員
	$ifLogin = MemberLogin();

	
	//最大訂購量
	$sql = "Select Init_Max From web_basicset Where web_basicset_id = '1' ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
	}
	
	//可編輯
	if ($ifEditor) {
		$thDelete = "<th class=\"t_align_c\" width=\"10%\">刪除</th>";
		//$tdDelete = "<td>&nbsp;</td>";
	} else {
		$thDelete = "<th class=\"t_align_c\" width=\"10%\">刪除</th>";
	}
	
	//購物車
	$shopping_list = "
	  <table class=\"table_type_1 responsive_table full_width r_corners wraper shadow t_align_l t_xs_align_c m_bottom_30 \">
		<thead>
		  <tr class=\"f_size_large\"> 
			<th class=\"t_align_c\" width=\"25%\">產品縮圖</th>
			<th class=\"t_align_c\" width=\"35%\">品名</th>
			<th class=\"t_align_c\" width=\"10%\">單價</th>
			<th class=\"t_align_c\" width=\"10%\">數量</th>
			<th class=\"t_align_c\" width=\"10%\">小計</th>
			".$thDelete."
		  </tr>
		</thead>
		<tbody>";
	
	//清單---------------------------------------------------------------------------------------------------------------
	$total = 0;	//小計
	$MyShoppingListAry = array();
	for ($x=0; $x<count($_SESSION["MyShopping"]); $x++) {
		$web_product_id = $_SESSION["MyShopping"][$x]["web_product_id"];
		$dimension = $_SESSION["MyShopping"][$x]["dimension"];
		$num = $_SESSION["MyShopping"][$x]["num"];
		$additionalText = ($_SESSION["MyShopping"][$x]["additional"]) ? '<b style="color:#cc0066">【加購】</b>- ' : null;
		$additionalText = ($_SESSION["MyShopping"][$x]["fullAdditional"]) ? '<b style="color:#cc0066">【滿額加購】</b>- ' : $additionalText;
		$masterId = ($_SESSION["MyShopping"][$x]["masterId"]) ? $_SESSION["MyShopping"][$x]["masterId"] : null;
		$MyShoppingListAry[$x] = $web_product_id;
		$Product = getProduct($web_product_id);	//取得產品相關資訊
		$Product[link] = ($additionalText) ? null : $Product[link];    //為加購 不帶連結

		$saleTitle = null;
		//銷售組合
		if($Product[web_x_sale_id]) {
			$Sale = getSale($Product[web_x_sale_id]);
			//取是否為同一組合全部數量
			$SaleShoppingCarNum = SearchMultidimensionalArray($_SESSION["MyShopping"], 'saleId', $Product[web_x_sale_id]);
			if(count($Sale)) {
				if($num >= $Sale[rows] || $SaleShoppingCarNum >= $Sale[rows]) {
					if($Sale[price]) {

						$Product[price] = ceil($Sale[price] / $Sale[rows]);

					} else if($Sale[discountnum] < 100 && !$Sale[price]) { 	

						$Product[price] = round($Product[price] * $Sale[discountnum] / 100);

					}
					$saleTitle = $Sale['subject'];
				}
			}
		}

		$Product[price] = ($additionalText) ? $Product[additionalprice] : $Product[price];    //為加購 顯示加購價

		$pic = ShowPic($Product[Covers], "uploadfiles/s/", "uploadfiles/no_image.jpg");

		if (!$ifLogin) $Product[price_memberlevel_tip] = "";
		
		if ($ifEditor) {	//可編輯
			$num_list = "<select name=\"num_".$web_product_id."_".$dimension."\" onChange=\"AlterShoppingCar(this, 'price_".$web_product_id."_".$dimension."');\">";
			$Init_Max_Stock = ($Product['stock'] >= $Init_Max) ? $Init_Max : $Product['stock'];
			$Init_Max_Stock = ($additionalText) ? 1 : $Init_Max_Stock;    //為加購 只能購買一個數量

			for ($j=1; $j<=$Init_Max_Stock; $j++) {
				$num_list .= "<option value=\"".$j."\"";
				if ($num==$j) $num_list .= " selected";
				$num_list .= ">".sprintf("%02d", $j)."</option>\n";
			}
			$num_list .= "</select>";
		} else {
			$num_list = "<span class=\"fw_medium f_size_large color_dark\">".$num."</span>";
		}
		
		if (!$ifEditor) $Product[link] = "";
		
		if ($Product[ifGeneral]==9) {	//加購
			$Product[link] = "";
			$Product[subject] = "【加購】".$Product[subject];
		}
		
		$shopping_list .= "
		  <tr id=\"tr_".$web_product_id."_".$dimension."\" class=\"tr_".$masterId."_".$dimension."\">
			<td data-title=\"產品縮圖\" class=\"t_align_c\"><a".$Product[link]."><img src=\"".$pic."\" alt=\"".$Product[subject]."\" class=\"m_md_bottom_5 d_xs_block d_xs_centered\"></a></td>
			<td data-title=\"品名\">
			  <a".$Product[link]." class=\"d_inline_b color_dark\">".$additionalText.$Product[subject]."</a>";
		if($saleTitle) 
			$shopping_list .= "<br/><a href=\"sale_list.php?kind=".str_front($Product["web_x_sale_id"])."\" title=\"".str_front($Product[saleTitle])."\"><b style='color:red'>".$saleTitle."</b></a>";
		$shopping_list .= "	 
			</td>

			<td data-title=\"單價\" class=\"t_align_c\"><span class=\"scheme_color fw_medium f_size_large\"><s>$".$Product[price_cost]."</s><br />$".number_format($Product[price])."</span></td>
			<td data-title=\"數量\" class=\"t_align_c\">".$num_list."</td>
			<td data-title=\"價格\" class=\"t_align_c\"><p class=\"f_size_large fw_medium scheme_color\">$<span id=\"price_".$web_product_id."_".$dimension."\">".number_format($Product[price]*$num)."</span></p></td>";

	if ($ifEditor) $shopping_list .= "<td data-title=\"刪除\" class=\"t_align_c\"><a href=\"javascript:;\" onClick=\"DeleteShoppingCar(this, 'tr_".$web_product_id."_".$dimension."_".$additionalText."');\" class=\"color_dark\"><i class=\"fa fa-times f_size_medium m_right_5\"></i></a></td>";

		$shopping_list .= "</tr>";
	}
	$_SESSION["MyShoppingList"] = $MyShoppingListAry;
	//小計---------------------------------------------------------------------------------------------------------------
	$subtotal = CalSubtotal();	//小計
	$shopping_list .= "
	  <tr>
		<td colspan=\"4\"><p class=\"fw_medium f_size_large t_align_r t_xs_align_c\">小計：</p></td>
		<td colspan=\"1\" class=\"t_align_r\"><p class=\"fw_medium f_size_large color_dark\">$<span id=\"new_subtotal\">".number_format($subtotal)."</span></p></td>
		".$tdDelete."
	  </tr>";
	//員工不計運費及不可用優惠代碼----------------------------------------------------------------------------------------
	if($_SESSION['MyMember']['level'] !== '4') {  
	if(0) {
		//優惠代碼---------------------------------------------------------------------------------------------------------------
		$code = CalCode();	//優惠代碼
		$mycoupon = $_SESSION["MyCode"];
		
		if ($ifEditor) {
			$shopping_list .= "
			  <tr>
				<td colspan=\"4\">
				  <p class=\"fw_medium f_size_large t_align_r t_xs_align_c\">
					<input id=\"mycoupon\" type=\"text\" value=\"".$mycoupon."\" placeholder=\"輸入優惠代碼\" class=\"r_corners f_size_medium\">
					<input type=\"button\" onClick=\"CheckCoupon();\" class=\"button_type_4 r_corners bg_light_color_2 m_left_5 mw_0 tr_all_hover color_dark\" value=\"使用\">
				  </p>
				</td>
				<td colspan=\"1\" class=\"t_align_r\"><p class=\"fw_medium f_size_large scheme_color\"><span id=\"new_coupon\">-$".number_format($code)."</span></p></td>
				".$tdDelete."
			  </tr>";
		} else {
			//有輸入優惠代碼嗎
			if ($mycoupon) $shopping_list .= "
			  <tr>
				<td colspan=\"4\">
				  <p class=\"fw_medium f_size_large t_align_r t_xs_align_c\">優惠代碼（".$mycoupon."）：</p>
				</td>
				<td colspan=\"1\" class=\"t_align_r\"><p class=\"fw_medium f_size_large scheme_color\"><span id=\"new_coupon\">-$".number_format($code)."</span></p></td>
				".$tdDelete."
			  </tr>";
		}
		
		//紅利折扣---------------------------------------------------------------------------------------------------------------
		$memberShoppingMoney = $_SESSION["MyMemberShoppingMoney"];
		$UseBonus = CalUseBonus($subtotal - $code - $memberShoppingMoney);	//是否符合紅利折扣資格
		$_bonus = $UseBonus[bonus];	//本次的紅利折扣金額
		$bonus = $_SESSION["MyBonus"];	//紅利要折扣金額
		if ($ifLogin && $_bonus) {	//有登入會員
			$shopping_list .= "
			  <tr>
			  	<td colspan=\"3\" style=\"text-align:center; line-height:45px;\"><p class=\"fw_medium f_size_large t_align_r t_xs_align_c\">可折抵紅利：$".number_format($_bonus)."</p></td>
				<td colspan=\"1\">
				  <p class=\"fw_medium f_size_large t_align_r t_xs_align_c\">
					<input id=\"mybonus\" type=\"text\" value=\"".$bonus."\" placeholder=\"輸入要抵扣紅利金額\" class=\"r_corners f_size_medium\">
					<input type=\"button\" onClick=\"CheckBonus(".$_bonus.");\" class=\"button_type_4 r_corners bg_light_color_2 m_left_5 mw_0 tr_all_hover color_dark\" value=\"使用\">
				  </p>
				</td>
				<td colspan=\"1\" class=\"t_align_r\"><p class=\"fw_medium f_size_large scheme_color\"><span id=\"new_coupon\">-$".number_format($bonus)."</span></p></td>
				".$tdDelete."
			  </tr>		
			  ";
		} else {
			unset($_SESSION["MyBonus"]);
		}

		//購物金---------------------------------------------------------------------------------------------------------------
		$UseMemberCode = CalUseMemberCode($subtotal - $code - $bonus);	//是否符購物金		
		$_memberShoppingMoney = $UseMemberCode[money];	//本次的購物金折扣金額
		$memberShoppingMoney = $_SESSION["MyMemberShoppingMoney"];	//購物金要折扣金額
		$dateLimit = ($UseMemberCode[dateLimit]) ? "  使用期限  (".$UseMemberCode[dateLimit].")" : null;
		if ($ifLogin && $_memberShoppingMoney) {	//有登入會員
			$shopping_list .= "
			  
			  <tr>
			  	<td colspan=\"3\" style=\"text-align:center; line-height:45px;\"><p class=\"fw_medium f_size_large t_align_r t_xs_align_c\"> 可折抵購物金：$".number_format($_memberShoppingMoney)."</p></td>
				<td>
				  <p class=\"fw_medium f_size_large t_align_r t_xs_align_c\">
					<input id=\"mymembercode\" type=\"text\" value=\"".$memberShoppingMoney."\" placeholder=\"輸入要抵扣購物金額\" class=\"r_corners f_size_medium\">
					<input type=\"button\" onClick=\"CheckMemberShoppingMoney(".$_memberShoppingMoney.");\" class=\"button_type_4 r_corners bg_light_color_2 m_left_5 mw_0 tr_all_hover color_dark\" value=\"使用\">
				  </p>
				</td>
				<td colspan=\"1\" class=\"t_align_r\"><p class=\"fw_medium f_size_large scheme_color\"><span id=\"new_coupon\">-$".number_format($memberShoppingMoney)."</span></p></td>
				".$tdDelete."
			  </tr>	
			  ";
		} else {
			unset($_SESSION["MyMemberShoppingMoney"]);
		}
	}	
		//運送地點---------------------------------------------------------------------------------------------------------------
		//先找運費預設值	
		$sql = "Select web_xx_freight_id, ifDefault as FreightDefault, subject as web_xx_freight From web_xx_freight Where ifDefault = '1' ";
		$rs = ConnectDB($DB, $sql);
		for ($i=0; $i<mysqli_num_rows($rs); $i++) {
			$row = mysqli_fetch_assoc($rs);
			foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
		}
		
		//有舉行什麼活動嗎
		$sql = "Select web_xx_freight_id, ifDefault as FreightDefault, subject as web_xx_freight From web_xx_freight Where ifDefault = '0' and sdate <= '".date("Y-m-d")."' and edate >= '".date("Y-m-d")."' order by sdate desc, edate desc, web_xx_freight_id desc ";
		$rs = ConnectDB($DB, $sql);
		for ($i=0; $i<mysqli_num_rows($rs); $i++) {
			$row = mysqli_fetch_assoc($rs);
			foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
		}

		//列出運送地點
		$web_x_freight_list = "";
		$sql = "Select web_x_freight_id, subject as web_x_freight From web_x_freight Where web_xx_freight_id = '".$web_xx_freight_id."' and ifShow = '1' order by asort asc, web_x_freight_id desc";
		$rs = ConnectDB($DB, $sql);
		for ($i=0; $i<mysqli_num_rows($rs); $i++) {
			$row = mysqli_fetch_assoc($rs);
			foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
			
			if (intval($_SESSION["MyFreight"])==0) $_SESSION["MyFreight"] = $web_x_freight_id;	//預設值

			$web_x_freight_list .= "<option value=\"".$web_x_freight_id."\"";
			if ($_SESSION["MyFreight"]==$web_x_freight_id) {
				$web_x_freight_list .= " selected";
				$default_web_x_freight = $web_x_freight;
			}
			$web_x_freight_list .= ">".$web_x_freight."</option>\n";
		}
		$freight = CalFreight($subtotal - $code - $bonus - $memberShoppingMoney);	//運費=小計-優惠代碼-紅利
	
		if ($ifEditor) {
			$shopping_list .= "
			  <tr>
				<td colspan=\"4\"><p class=\"fw_medium f_size_large t_align_r t_xs_align_c\">運送地點
				<select name=\"freight_id\" onChange=\"ChangeFreight(this);\">
				  ".$web_x_freight_list."
				</select> 運費：</p></td>
				<td colspan=\"1\" class=\"t_align_r\"><p class=\"fw_medium f_size_large color_dark\">$<span id=\"new_freight\">".number_format($freight)."</span></p></td>
				".$tdDelete."
			  </tr>";
		} else {
			$shopping_list .= "
			  <tr>
				<td colspan=\"4\"><p class=\"fw_medium f_size_large t_align_r t_xs_align_c\">運送地點：".$default_web_x_freight."　運費：</p></td>
				<td colspan=\"1\" class=\"t_align_r\"><p class=\"fw_medium f_size_large color_dark\">$<span id=\"new_freight\">".number_format($freight)."</span></p></td>
				".$tdDelete."
			  </tr>";
		}
	}
	//總計---------------------------------------------------------------------------------------------------------------
	$shopping_list .= "
	  <tr>
		<td colspan=\"4\"><p class=\"fw_medium f_size_large scheme_color t_align_r t_xs_align_c\">總計：</p></td>
		<td colspan=\"1\" class=\"t_align_r\"><p class=\"fw_medium f_size_large scheme_color m_xs_bottom_10\">$<span id=\"new_total\">".number_format($subtotal - $code - $bonus - $memberShoppingMoney + $freight)."</span></p></td>
		".$tdDelete."
	  </tr>";
	$_SESSION["MyShoppingSubtobal"] = $subtotal - $code - $bonus - $memberShoppingMoney;
	//贈品---------------------------------------------------------------------------------------------------------------
	if (CalGift($subtotal - $code - $bonus - $memberShoppingMoney)!="") {//if不為空值顯示
	$shopping_list .= "
	  <tr>
		<td colspan=\"5\" class=\"t_align_l\"><p class=\"fw_medium f_size_large scheme_color t_align_l t_xs_align_l\"><span id=\"new_gift\">".CalGift($subtotal - $code - $bonus - $memberShoppingMoney)."</span></p></td>
		".$tdDelete."
	  </tr>";
	  }
	
	//贈送紅利---------------------------------------------------------------------------------------------------------------
	$SendBonus = CalSendBonus($subtotal - $code - $bonus - $memberShoppingMoney);	//紅利:本檔活動
	$web_x_bonus_hidden = $SendBonus[web_x_bonus_hidden];	//紅利流水號
	$web_x_bonus_list = $SendBonus[web_x_bonus_list];		//紅利描述
			
	if ($PageSelfURL!="shopping_send.php") {
		//if (CalGift($web_x_bonus_list)!="") {//if不為空值顯示
		$shopping_list .= "
	  <tr>
		<td colspan=\"5\" class=\"t_align_r\"><span id=\"new_bonus\">".$web_x_bonus_list."</span></td>
		".$tdDelete."
	  </tr>";
		//}
	}
	//說明---------------------------------------------------------------------------------------------------------------
	$sql = "Select Init_Freight2 From web_basicset Where web_basicset_id = '1' ";
	$rs = ConnectDB($DB, $sql);
	$row = mysqli_fetch_assoc($rs);
	$Init_Freight2 = $row['Init_Freight2']; 
	$shopping_list .= "
	  <tr>
		<td colspan=\"6\" class=\"t_align_l memo\">
		<div class=\"alert_box r_corners warning m_bottom_10\">
			<div class=\"row\">
			".str_front($Init_Freight2)."
			</div>
		</div>
		</td>
	 </tr>";
	 
	$shopping_list .= "
            </tbody>
          </table>";
    //$shopping_list .=  ShoppingCarListPhone();     
	
	return $shopping_list;
}

//購物清單 shopping_car.php
function ShoppingCarList($ifStop) {
	//現在在哪一頁
	$UrlArray = explode("/", htmlentities($_SERVER["PHP_SELF"]));
	$PageSelfURL = strtolower($UrlArray[sizeof($UrlArray)-1]);

	$ifEditor = ($ifStop==0) ? true : false;
	//$ifEditor = false;
	
	//是否已登入會員
	$ifLogin = MemberLogin();

	
	//最大訂購量
	$sql = "Select Init_Max, Init_Payment_CashOnDelivery2_price From web_basicset Where web_basicset_id = '1' ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
	}
		
	
	//可編輯
	if ($ifEditor) {
		$thDelete = "<li class=\"b_r_none\">刪除</li>";
		//$tdDelete = "<li>&nbsp;</li>";
	} else {
		$thDelete = "<li class=\"b_r_none\">刪除</li>";
	}	

	//購物車		
	$shopping_list ="
		<div class=\"table2\">
			<div class=\"tab1t\">
				<li>產品縮圖</li>
				<li>品名</li>
				<li>單價</li>
				<li>數量</li>
				<li>小計</li>
				".$thDelete."
			</div>
	";	 
	
	//清單---------------------------------------------------------------------------------------------------------------
	$total = 0;	//小計
	$MyShoppingListAry = array();
	$groupTag = 0;
	$groupTag2 = 0;
	$_j = 0;
	$saleAllNum = getSaleAllNum();
	$surplusNum = 0;
	for ($x=0; $x<count($_SESSION["MyShopping"]); $x++) {
		$web_product_id = $_SESSION["MyShopping"][$x]["web_product_id"];
		$dimension = $_SESSION["MyShopping"][$x]["dimension"];
		$num = $_SESSION["MyShopping"][$x]["num"];
		$additionalText = ($_SESSION["MyShopping"][$x]["additional"]) ? '<b style="color:#cc0066">【加購】</b>- ' : null;
		$additionalText = ($_SESSION["MyShopping"][$x]["fullAdditional"]) ? '<b style="color:#cc0066">【滿額加購】</b>- ' : $additionalText;
		$masterId = ($_SESSION["MyShopping"][$x]["masterId"]) ? $_SESSION["MyShopping"][$x]["masterId"] : null;
		$MyShoppingListAry[$x] = $web_product_id;
		$Product = getProduct($web_product_id);	//取得產品相關資訊
		$Categories = getCategories($Product[web_x_product_id]);
		$_Categories = _getCategories($Categories[web_xxx_product_id]);
		/*
		$IPA =  getenv('REMOTE_ADDR');
		$ipList = array(
			'118.170.45.55',
		);

		if (in_array($IPA, $ipList)){
			echo "<pre>";
			print_r($_Categories);
			echo "</pre>";
		}
		*/
		$Product[link] = ($additionalText) ? null : $Product[link];    //為加購 不帶連結
		if($_SESSION["MyShopping"][$x]["additional"]) {
			continue;
		}
		$saleTitle = null;
		$showFlag = 0;
		//$saleAry = array();
		//銷售組合
		if($Product[web_x_sale_id] && !$_SESSION["MyShopping"][$x]["additional"] && !$_SESSION["MyShopping"][$x]["fullAdditional"]) {
			$Sale = getSale($Product[web_x_sale_id]);
			//echo $Product[web_x_sale_id]."</br>";
			//取是否為同一組合全部數量
			$SaleShoppingCarNum = SearchMultidimensionalArray_2($_SESSION["MyShopping"], 'saleId', $Product[web_x_sale_id]);
			if(count($Sale)) {
				//if($num % $Sale[rows] == 0 || $SaleShoppingCarNum % $Sale[rows] == 0) {
				if($num % $Sale[rows] == 0 || $SaleShoppingCarNum >= $Sale[rows]) {	
					if($Sale[price]) {

						//$Product[price] = floor($Sale[price] / $Sale[rows]);
						$saleTitle = $Sale['subject'];	
						$saleTotal[$Product[web_x_sale_id]] += ($Product[price] * $num);
						$saleNum[$Product[web_x_sale_id]] += $num;
						if($saleNum[$Product[web_x_sale_id]] >= $Sale[rows]) {
							$_minusRow = $saleNum[$Product[web_x_sale_id]] % $Sale[rows];
							$_minus = ($Product[price] * $_minusRow);
						} 

						$saleAry[$Product[web_x_sale_id]] = array(
							'id'			=>	$Product[web_x_sale_id],
							'saleRow'		=>	$Sale[rows],
							'salePrice'		=>	$Sale[price],
							'title'			=>	$saleTitle,
							'num'			=> 	$saleNum[$Product[web_x_sale_id]],
							'total'			=>	$saleTotal[$Product[web_x_sale_id]] - $_minus,

						);

					} else if($Sale[discountnum] < 100 && !$Sale[price]) { 	

						//echo $num % $Sale[rows]; 	
						//echo '-';
						//$Product[price] = (($Sale[rows] % $num == 0) && ($num >= $Sale[rows])) ? $Product[price_cost] : $Product[price];
						//$showFlag = (($Sale[rows] % $num == 0) && ($num >= $Sale[rows])) ? 1 : 0;
						//echo $SaleShoppingCarNum % $Sale[rows].'=='.$num % $Sale[rows]."</br>";
						//echo '_';
						//if($SaleShoppingCarNum % $Sale[rows] == 1 && $num % $Sale[rows] == 2) {
						//if($SaleShoppingCarNum % $Sale[rows] != 0) {
						if(1) {	
							
							if($saleAllNum[$Product[web_x_sale_id]]) {
								//echo $Sale[rows].",";
								$numAry2[$Product[web_x_sale_id]][$web_product_id] = $num;
								$SaleRows[$Product[web_x_sale_id]] = $Sale[rows];
								$numAry[] = $num;
								$saleAllNum[$Product[web_x_sale_id]] = ($saleAllNum[$Product[web_x_sale_id]] >= $SaleRows[$Product[web_x_sale_id]]) ? $saleAllNum[$Product[web_x_sale_id]] - ($saleAllNum[$Product[web_x_sale_id]] % $SaleRows[$Product[web_x_sale_id]]) : $saleAllNum[$Product[web_x_sale_id]];
								
								if(array_sum($numAry2[$Product[web_x_sale_id]]) <= $saleAllNum[$Product[web_x_sale_id]]) {
									$lastNum[$Product[web_x_sale_id]] = array_sum($numAry2[$Product[web_x_sale_id]]);
									$Product[slaePriceTotal] = ($Product[price_cost] * ($numAry2[$Product[web_x_sale_id]][$web_product_id]));
									$Product[truePriceTotal] = 0;
								} else {
									$canDoNum[$Product[web_x_sale_id]] = ($saleAllNum[$Product[web_x_sale_id]] - $lastNum[$Product[web_x_sale_id]]);
									$lastNum[$Product[web_x_sale_id]] += $canDoNum[$Product[web_x_sale_id]];
									if($saleAllNum[$Product[web_x_sale_id]] >= $lastNum[$Product[web_x_sale_id]]) {
										$Product[slaePriceTotal] = ($Product[price_cost] * ($canDoNum[$Product[web_x_sale_id]]));
										$Product[truePriceTotal] = ($Product[price] * ($numAry2[$Product[web_x_sale_id]][$web_product_id] - $canDoNum[$Product[web_x_sale_id]]));
									} else {
										$Product[slaePriceTotal] = 0;
										$Product[truePriceTotal] = ($Product[price] * ($numAry2[$Product[web_x_sale_id]][$web_product_id]));
									}	
								}
								
							}
							
/*
							$num2 = ($SaleShoppingCarNum % $Sale[rows] == 0) ? $num : $num - ($num % $Sale[rows]);
							$num3 = ($num2) ? $num2 : 1;
							
							$Product[slaePriceTotal] = ($num2) ? ($Product[price_cost] * ($num3)) : 0;
							$Product[truePriceTotal] = ($num2) ? ($Product[price] * ($num - $num3)) : $Product[price] * $num;
*/							
							$Product[allPriceTotal] = $Product[slaePriceTotal] + $Product[truePriceTotal];
							
						}	
						$Product[price] = $Product[price_cost];
						$showFlag = 1;

						//$Product[price] = round($Product[price] * $Sale[discountnum] / 100);
						$saleTitle = $Sale['subject'];	
						//$saleTotal[$Product[web_x_sale_id]] += ($Product[price] * $num);
						//$saleTotal[$Product[web_x_sale_id]] += $Product[allPriceTotal];
						$saleTotal[$Product[web_x_sale_id]] += $Product[slaePriceTotal];
						$saleNum[$Product[web_x_sale_id]] += $num;
						if($saleNum[$Product[web_x_sale_id]] >= $Sale[rows]) {
							$_minusRow = $saleNum[$Product[web_x_sale_id]] % $Sale[rows];
							$_minus = ($Product[price] * $_minusRow);
						} 

						$saleAry[$Product[web_x_sale_id]] = array(
							'id'				=>	$Product[web_x_sale_id],
							'saleRow'			=>	$Sale[rows],
							'saleDiscountnum'	=>	$Sale[discountnum],
							'title'				=>	$saleTitle,
							'num'				=> 	$saleNum[$Product[web_x_sale_id]],
							//'total'				=>	$saleTotal[$Product[web_x_sale_id]] - $_minus,
							'total'				=>	$saleTotal[$Product[web_x_sale_id]],

						);
					}

				}
				$saleTitle = $Sale['subject'];
			}
		}

		$Product[price] = ($additionalText) ? $Product[additionalprice] : $Product[price];    //為加購 顯示加購價

		$pic = ShowPic($Product[Covers], "uploadfiles/s/", "uploadfiles/nopic.gif");

		if (!$ifLogin) $Product[price_memberlevel_tip] = "";
		
		if ($ifEditor) {	//可編輯
			$num_list = "<select name=\"num_".$web_product_id."_".$dimension."\" onChange=\"AlterShoppingCar(this, 'price_".$web_product_id."_".$dimension."');\">";
			$Init_Max_Stock = ($Product['stock'] >= $Init_Max) ? $Init_Max : $Product['stock'];
			$Init_Max_Stock = ($additionalText) ? 1 : $Init_Max_Stock;    //為加購 只能購買一個數量

			for ($j=1; $j<=$Init_Max_Stock; $j++) {
				$num_list .= "<option value=\"".$j."\"";
				if ($num==$j) $num_list .= " selected";
				$num_list .= ">".sprintf("%02d", $j)."</option>\n";
			}
			$num_list .= "</select>";
		} else {
			$num_list = "<span class=\"fw_medium f_size_large color_dark\">".$num."</span>";
		}
		
		if (!$ifEditor) $Product[link] = "";
		
		if ($Product[ifGeneral]==9) {	//加購
			$Product[link] = "";
			$Product[subject] = "【加購】".$Product[subject];
		}
		
		$shopping_list .= "		
		<div class=\"tab1b tr_".$masterId."_".$dimension."\" id=\"tr_".$web_product_id."_".$dimension."\">							
			<li class=\"t_bor_top02\"><a".$Product[link]."><img src=\"".$pic."\" alt=\"".$Product[subject]."\"></a></li>
			<li class=\"pro_name t_bor_top02\"><a".$Product[link]." class=\"d_inline_b color_dark\">".$additionalText.$Product[subject]."</a>";
		if($Product[price_discount_tip]) 
			$shopping_list .= "</br>".$Product[price_discount_tip];
		if($_Categories[web_xxxx_product_id] == '5')
			/*1051220修改*/
			$shopping_list .= "<div class=\"promoitem\"><p></p><a href=\"friend.html\" title=\"".str_front($_Categories[web_xxxx_product_subject])."\">".str_front($_Categories[web_xxxx_product_subject])."</a></div>";	 
		if($saleTitle)
			 /*1050706修改備份
			$shopping_list .= "<br/><a href=\"sale_list.php?kind=".str_front($Product["web_x_sale_id"])."\" title=\"".str_front($Product[saleTitle])."\"><b style='color:red'>".$saleTitle."</b></a>";*/
			/*1050706修改*/
			$shopping_list .= "<div class=\"promoitem\"><p>商品適用優惠促銷</p><a href=\"sale_list.php?kind=".str_front($Product["web_x_sale_id"])."\" title=\"".str_front($Product[saleTitle])."\">".$saleTitle."</a></div>";
		$shopping_list .= "
			</li>
			<li>$".number_format($Product[price])."</li>
			<li>數量：".$num_list."</li>
			";
		if(!$showFlag)
			$shopping_list .= "
			<li class=\"point\">$<span id=\"price_".$web_product_id."_".$dimension."\">".number_format($Product[price]*$num)."</span></li>";
		else
			$shopping_list .= "
			<li class=\"point\">$<span id=\"price_".$web_product_id."_".$dimension."\">".number_format($Product[allPriceTotal])."</span></li>";	
	
	
		if ($ifEditor) {
			$shopping_list .= "<li><a href=\"javascript:;\" onClick=\"DeleteShoppingCar(this, 'tr_".$web_product_id."_".$dimension."');\" class=\"color_dark\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i></a></li>";
		} else {
			$shopping_list .= "<li class=\"hidden-xs hidden-sm\">&nbsp;</li>";
		}
		$shopping_list .= "</div>";
	}

	//加購區
	for ($x=0; $x<count($_SESSION["MyShopping"]); $x++) {
		$web_product_id = $_SESSION["MyShopping"][$x]["web_product_id"];
		$dimension = $_SESSION["MyShopping"][$x]["dimension"];
		$num = $_SESSION["MyShopping"][$x]["num"];
		$additionalText = ($_SESSION["MyShopping"][$x]["additional"]) ? '<b style="color:#cc0066">【加購】</b>- ' : null;
		$additionalText = ($_SESSION["MyShopping"][$x]["fullAdditional"]) ? '<b style="color:#cc0066">【滿額加購】</b>- ' : $additionalText;
		$masterId = ($_SESSION["MyShopping"][$x]["masterId"]) ? $_SESSION["MyShopping"][$x]["masterId"] : null;
		$MyShoppingListAry[$x] = $web_product_id;
		$Product = getProduct($web_product_id);	//取得產品相關資訊
		$Product[link] = ($additionalText) ? null : $Product[link];    //為加購 不帶連結
		if(!$_SESSION["MyShopping"][$x]["additional"]) {
			continue;
		}
		$saleTitle = null;
		
		$Product[price] = ($additionalText) ? $Product[additionalprice] : $Product[price];    //為加購 顯示加購價

		$pic = ShowPic($Product[Covers], "uploadfiles/s/", "uploadfiles/nopic.gif");

		if (!$ifLogin) $Product[price_memberlevel_tip] = "";
		
		if ($ifEditor) {	//可編輯
			$num_list = "<select name=\"num_".$web_product_id."_".$dimension."\" onChange=\"AlterShoppingCar(this, 'price_".$web_product_id."_".$dimension."');\">";
			$Init_Max_Stock = ($Product['stock'] >= $Init_Max) ? $Init_Max : $Product['stock'];
			$Init_Max_Stock = ($additionalText) ? 1 : $Init_Max_Stock;    //為加購 只能購買一個數量

			for ($j=1; $j<=$Init_Max_Stock; $j++) {
				$num_list .= "<option value=\"".$j."\"";
				if ($num==$j) $num_list .= " selected";
				$num_list .= ">".sprintf("%02d", $j)."</option>\n";
			}
			$num_list .= "</select>";
		} else {
			$num_list = "<span class=\"fw_medium f_size_large color_dark\">".$num."</span>";
		}
		
		if (!$ifEditor) $Product[link] = "";
		
		if ($Product[ifGeneral]==9) {	//加購
			$Product[link] = "";
			$Product[subject] = "【加購】".$Product[subject];
		}
		
		$shopping_list .= "		
		<div class=\"tab1b tr_".$masterId."_".$dimension."\" id=\"tr_".$web_product_id."_".$dimension."\">							
			<li><a".$Product[link]."><img src=\"".$pic."\" alt=\"".$Product[subject]."\"></a></li>
			<li><a".$Product[link]." class=\"d_inline_b color_dark\">".$additionalText.$Product[subject]."</a>";
		if($saleTitle)
			$shopping_list .= "<br/><a href=\"sale_list.php?kind=".str_front($Product["web_x_sale_id"])."\" title=\"".str_front($Product[saleTitle])."\"><b style='color:red'>".$saleTitle."</b></a>";
		$shopping_list .= "
			</li>
			<li>$".number_format($Product[price])."</li>
			<li>".$num_list."</li>
			<li class=\"tab1t mobile\">小計：</li>
			<li class=\"point\">$<span id=\"price_".$web_product_id."_".$dimension."\">".number_format($Product[price]*$num)."</span></li>	
		";
	
	
		if ($ifEditor) {
			$shopping_list .= "<li><a href=\"javascript:;\" onClick=\"DeleteShoppingCar(this, 'tr_".$web_product_id."_".$dimension."');\" class=\"color_dark\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i></a></li>";
		} else {
			$shopping_list .= "<li>&nbsp;</li>";
		}
		$shopping_list .= "</div>";
	}

	$_SESSION["MyShoppingList"] = $MyShoppingListAry;

	if(($saleAry)) {
		foreach($saleAry as $saleRow) {

			if($saleRow['salePrice']) {
				$_saleTotal = ($saleRow[total] >= $saleRow['salePrice']) ? $saleRow[total] : $saleRow['salePrice'];
				$_saleNum = ($saleRow[num] >= $saleRow['saleRow']) ? $saleRow[num] : $saleRow['num'];
				$_saleRow =  floor($saleRow[num] / $saleRow['saleRow']);
				$minus = $_saleTotal - ($saleRow['salePrice'] * ($_saleRow));
			} else {
				$_saleTotal = ($saleRow[total] >= $saleRow['salePrice']) ? $saleRow[total] : $saleRow['salePrice'];
				$_saleNum = ($saleRow[num] >= $saleRow['saleRow']) ? $saleRow[num] : $saleRow['num'];
				$_saleRow =  floor($saleRow[num] / $saleRow['saleRow']);
				$minus = $_saleTotal - round(($_saleTotal * $saleRow['saleDiscountnum']) / 100);
			}	
			
			$shopping_list .= "
			<div class=\"tab1f\">
				<li class=\"text-right\"><span class=\"promotions\">優惠</span><a href=\"sales".str_front($Product["web_x_sale_id"]).".html\" title=\"".$saleRow['title']."\">".$saleRow['title']."</a></li>
				<li>-$<span id=\"new_subtotal\">".number_format($minus)."</span></li>
			</div>";
		}
		$minusTotal += $minus;
	}

	//小計---------------------------------------------------------------------------------------------------------------
	$subtotal = CalSubtotal();	//小計
	
	$shopping_list .= "  
		<div class=\"tab1f\">
			<li class=\"text-right\">小計</li>
			<li>$<span id=\"new_subtotal\">".number_format($subtotal)."</span></li>
	";		
										
	//員工不計運費及不可用優惠代碼----------------------------------------------------------------------------------------
	if($_SESSION['MyMember']['level'] !== '4') {  
	if(0) {	
		//優惠代碼---------------------------------------------------------------------------------------------------------------
		$code = CalCode();	//優惠代碼
		$mycoupon = $_SESSION["MyCode"];
		
		if ($ifEditor) {
			if(!$mycoupon) {
				$shopping_list .= "  
					<li class=\"text-right\">
						<input id=\"mycoupon\" type=\"text\" value=\"".$mycoupon."\" placeholder=\"輸入優惠代碼\">
						<a class=\"btn3\" style=\"cursor:pointer\" onClick=\"CheckCoupon();\">使用</a>
					</li>
					<li class=\"point\"><span id=\"new_coupon\">-$".number_format($code)."</span></li>
				";
			} else {
				$shopping_list .= "  
					<li class=\"text-right\">
						<input id=\"mycoupon\" type=\"text\" value=\"".$mycoupon."\" placeholder=\"輸入優惠代碼\">
						<a class=\"btn3\" style=\"cursor:pointer\" onClick=\"CancleCoupon();\">取消</a>
					</li>
					<li class=\"point\"><span id=\"new_coupon\">-$".number_format($code)."</span></li>
				";
			}		  
		} else {
			//有輸入優惠代碼嗎
			
			if ($mycoupon) $shopping_list .= "  
				<li class=\"text-right\">
					優惠代碼（".$mycoupon."）：
				</li>
				<li class=\"point lh2\"><span id=\"new_coupon\">-$".number_format($code)."</span></li>
			";  
		}
		//紅利折扣---------------------------------------------------------------------------------------------------------------
		$memberShoppingMoney = $_SESSION["MyMemberShoppingMoney"];
		$memberCouponMoney = $_SESSION["MyCouponMoney"];	//優惠券要折扣金額
		$UseBonus = CalUseBonus($subtotal - $code - $memberShoppingMoney - $memberCouponMoney);	//是否符合紅利折扣資格
		$_bonus = $UseBonus[bonus];	//本次的紅利折扣金額
		$bonus = $_SESSION["MyBonus"];	//紅利要折扣金額
		if ($ifLogin && $_bonus) {	//有登入會員
			
			$shopping_list .= "  
			<li class=\"text-right lh\">
				可折抵紅利：$".number_format($_bonus)."
				<input id=\"mybonus\" type=\"text\" value=\"".$bonus."\" placeholder=\"輸入要抵扣紅利金額\" >
				<a class=\"btn3\" onClick=\"CheckBonus(".$_bonus.");\" style=\"cursor:pointer\">使用</a>
			</li>
			<li class=\"point lh2\"><span id=\"new_coupon\">-$".number_format($bonus)."</span></li>
			"; 
		} else {
			unset($_SESSION["MyBonus"]);
		}

		//購物金---------------------------------------------------------------------------------------------------------------
		$memberCouponMoney = $_SESSION["MyCouponMoney"];	//優惠券要折扣金額
		$UseMemberCode = CalUseMemberCode($subtotal - $code - $bonus - $memberCouponMoney);	//是否符購物金	
		$_memberShoppingMoney = $UseMemberCode[money];	//本次的購物金折扣金額
		$memberShoppingMoney = $_SESSION["MyMemberShoppingMoney"];	//購物金要折扣金額
		$dateLimit = ($UseMemberCode[dateLimit]) ? "  使用期限  (".$UseMemberCode[dateLimit].")" : null;
		if ($ifLogin && $_memberShoppingMoney) {	//有登入會員
			
			$shopping_list .= "  
			<li class=\"text-right lh\">
				可折抵購物金：$".number_format($_memberShoppingMoney)."
				<input id=\"mymembercode\" type=\"text\" value=\"".$memberShoppingMoney."\" placeholder=\"輸入要抵扣購物金額\">
				<a class=\"btn3\" onClick=\"CheckMemberShoppingMoney(".$_memberShoppingMoney.");\" style=\"cursor:pointer\">使用</a>
			</li>
			<li class=\"point lh2\"><span id=\"new_coupon\">-$".number_format($memberShoppingMoney)."</span></li>
			";   
		} else {
			unset($_SESSION["MyMemberShoppingMoney"]);
		}


		//優惠券(只限一般會員) 
		if($_SESSION['MyMember']['level'] === '1') {
			$memberShoppingMoney = $_SESSION["MyMemberShoppingMoney"];	//購物金要折扣金額
			//優惠券---------------------------------------------------------------------------------------------------------------
			$UseCoupon = CalUseCoupon($subtotal - $code - $bonus - $memberShoppingMoney);	//是否符購物金
			//$_memberCouponMoney = $UseMemberCode[money];	//本次的購物金折扣金額
			$memberCouponMoney = $_SESSION["MyCouponMoney"];	//優惠券要折扣金額
			//$dateLimit = ($UseMemberCode[dateLimit]) ? "  使用期限  (".$UseMemberCode[dateLimit].")" : null;
			$_total = $subtotal - $code - $bonus;
			if ($ifLogin && count($UseCoupon)) {	//有登入會員
				$_web_x_coupon_id = ($_SESSION["MyCoupon"]) ? $_SESSION["MyCoupon"] : 0;
				$Coupon = getCoupon($_web_x_coupon_id);
				$web_x_coupon_list .= "<option value=\"\">優惠券</option>";
				$couponShow = false;
				foreach($UseCoupon as $coupon) {
					if($coupon[money] > $_total) {
						continue;
					}
					//可抵扣產品
					$coupon[bestsellers] = ($coupon[bestsellers]) ? explode(',', $coupon[bestsellers]) : '';
					$result = array_intersect($_SESSION[MyShoppingList], $coupon[bestsellers]);
					if(!count($result)) {
						continue;
					} else {
						$couponShow = true;
					}

					$web_x_coupon_list .= "<option value=\"".$coupon[web_x_coupon_id]."\"";
					if ($_SESSION["MyCoupon"] == $coupon[web_x_coupon_id]) {
						$web_x_coupon_list .= " selected";
						//$default_web_x_freight = $web_x_freight;
					}
					$web_x_coupon_list .= ">【".$coupon[subject]."】 抵扣金額: $".number_format($coupon[money])."</option>\n";
				}
				if($couponShow) {
					$shopping_list .= "  	
						<li class=\"text-right\">
							<select name=\"coupon_id\" onChange=\"ChangeCoupon(this);\">
								".$web_x_coupon_list."
							</select>
						</li>
						<li class=\"point\">
							-$<span id=\"new_coupon2\">".number_format($Coupon[money])."</span>
						</li>
					";
				}     
			} else {
				unset($_SESSION["MyCouponMoney"]);
			}
  			

		}
	}	
		//運送地點---------------------------------------------------------------------------------------------------------------
		//先找運費預設值	
		$sql = "Select web_xx_freight_id, ifDefault as FreightDefault, subject as web_xx_freight From web_xx_freight Where ifDefault = '1' ";
		$rs = ConnectDB($DB, $sql);
		for ($i=0; $i<mysqli_num_rows($rs); $i++) {
			$row = mysqli_fetch_assoc($rs);
			foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
		}
		
		//有舉行什麼活動嗎
		$sql = "Select web_xx_freight_id, ifDefault as FreightDefault, subject as web_xx_freight From web_xx_freight Where ifDefault = '0' and sdate <= '".date("Y-m-d")."' and edate >= '".date("Y-m-d")."' order by sdate desc, edate desc, web_xx_freight_id desc ";
		$rs = ConnectDB($DB, $sql);
		for ($i=0; $i<mysqli_num_rows($rs); $i++) {
			$row = mysqli_fetch_assoc($rs);
			foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
		}

		//列出運送地點
		$web_x_freight_list = "";
		$sql = "Select web_x_freight_id, subject as web_x_freight From web_x_freight Where web_xx_freight_id = '".$web_xx_freight_id."' and ifShow = '1' order by asort asc, web_x_freight_id desc";
		$rs = ConnectDB($DB, $sql);
		for ($i=0; $i<mysqli_num_rows($rs); $i++) {
			$row = mysqli_fetch_assoc($rs);
			foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
			
			if (intval($_SESSION["MyFreight"])==0) $_SESSION["MyFreight"] = $web_x_freight_id;	//預設值

			$web_x_freight_list .= "<option value=\"".$web_x_freight_id."\"";
			if ($_SESSION["MyFreight"]==$web_x_freight_id) {
				$web_x_freight_list .= " selected";
				$default_web_x_freight = $web_x_freight;
			}
			$web_x_freight_list .= ">".$web_x_freight."</option>\n";
		}
		$freight = CalFreight($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney);	//運費=小計-優惠代碼-紅利
	
		if ($ifEditor) {
			
			$shopping_list .= "  	
			<li class=\"text-right\">
				<select name=\"freight_id\" onChange=\"ChangeFreight(this);\">
					".$web_x_freight_list."
				</select>
			</li>
			<li>
				$<span id=\"new_freight\">".number_format($freight)."</span>
			</li>
			";  
		} else {
			
			$shopping_list .= "  	
			<li class=\"text-right\">
				運送地點：".$default_web_x_freight."
			</li>
			<li>
				$<span id=\"new_freight\">".number_format($freight)."</span>
			</li>
			";    
		}

		//貨到付款
		if( $_SESSION['Init_Payment_CashOnDelivery2']) {
			$_Init_Payment_CashOnDelivery2_price = CalCashOnDelivery($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney + $freight);
			if($_SESSION[bugFlag]) {
				$_Init_Payment_CashOnDelivery2_price = 0;
			}
			
		    $shopping_list .= "  	
				<li class=\"text-right\">
					貨到付款：
				</li>
				<li>
					$".number_format($_Init_Payment_CashOnDelivery2_price)."
				</li>
			";
		    //$_Init_Payment_CashOnDelivery2_price = $Init_Payment_CashOnDelivery2_price;
		}

	}
	//總計---------------------------------------------------------------------------------------------------------------
	
	$shopping_list .= "  
		<li class=\"sum text-right\">總計</li>
			<li class=\"point\">$<span id=\"new_total\">".number_format($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney + $freight + $_Init_Payment_CashOnDelivery2_price)."</span></li> 
	";
	$shopping_list .= "
            </div>
        </div>    
    ";      

	$_SESSION["MyShoppingSubtobal"] = $subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney;
	//贈品---------------------------------------------------------------------------------------------------------------
	if (CalGift($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney)!="") {//if不為空值顯示
	
		$shopping_list .= "  
			<div class=\"memo\">
				<span id=\"new_gift\">".CalGift($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney)."</span>
			</div>
			</hr>
		";	

  	}
	
	//贈送紅利---------------------------------------------------------------------------------------------------------------
	$SendBonus = CalSendBonus($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney);	//紅利:本檔活動
	$web_x_bonus_hidden = $SendBonus[web_x_bonus_hidden];	//紅利流水號
	$web_x_bonus_list = $SendBonus[web_x_bonus_list];		//紅利描述
			
	if ($PageSelfURL!="shoppingsend.php" && 0) {
		//if (CalGift($web_x_bonus_list)!="") {//if不為空值顯示
	
		$shopping_list .= "  
			<div class=\"memo2\">
				<span id=\"new_bonus\">".$web_x_bonus_list."</span>
			</div>
		";  
		//}
	}
	//說明---------------------------------------------------------------------------------------------------------------
	$sql = "Select Init_Freight2 From web_basicset Where web_basicset_id = '1' ";
	$rs = ConnectDB($DB, $sql);
	$row = mysqli_fetch_assoc($rs);
	$Init_Freight2 = $row['Init_Freight2']; 
	
 	$shopping_list .= "
 	</hr>
	<div class=\"memo2\">								
		".str_front($Init_Freight2)."
	</div>
	"; 
	 
	
	return $shopping_list;
}

//購物清單 shopping_car.php
function ShoppingCarList2($ifStop) {
	//現在在哪一頁
	$UrlArray = explode("/", htmlentities($_SERVER["PHP_SELF"]));
	$PageSelfURL = strtolower($UrlArray[sizeof($UrlArray)-1]);

	$ifEditor = ($ifStop==0) ? true : false;
	//$ifEditor = false;
	
	//是否已登入會員
	$ifLogin = MemberLogin();
	if ($ifLogin) {
		//$web_member_id = intval($_SESSION["MyMember"]["ID"]);	//會員

		//會員等級折扣
		$Member = getMember(intval($_SESSION["MyMember"]["ID"]));
		if ($Member[level_discount]<100) $levelremark = "小計滿".$Member[level_srange]."元打".$Member[level_discount]."折";
	}
	
	//最大訂購量
	$sql = "Select Init_Max, Init_Payment_CashOnDelivery2_price From web_basicset Where web_basicset_id = '1' ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
	}
		
	
	//可編輯
	if ($ifEditor) {
		$thDelete = "<th>刪除</th>";
	} else {	
		//$tdDelete = "<th>&nbsp;</th>";
	}		

	//購物車		
	$shopping_list ="
		<table class=\"table table-striped table_style\">
			<thead>
				<tr class=\"tr_bg tr_br\">
					<th>產品縮圖</th>
					<th>品名</th>
					<th>數量</th>
					<th>單價</th>
					<th>小計</th>
					".$thDelete."
				</tr>
			</thead>
	";		 
	$shopping_list .="<tbody>";	 


	
	//清單---------------------------------------------------------------------------------------------------------------
	$total = 0;	//小計
	$MyShoppingListAry = array();
	$groupTag = 0;
	$groupTag2 = 0;
	$_j = 0;
	$saleAllNum = getSaleAllNum();
	$surplusNum = 0;
	$unFreightFlag = 0; 	//免運費
	$unFreightFlag = 0; 	//免運費
	$_SESSION['unFreightFlag'] = $unFreightFlag;
	for ($x=0; $x<count($_SESSION["MyShopping"]); $x++) {
		$web_product_id = $_SESSION["MyShopping"][$x]["web_product_id"];
		$dimension = $_SESSION["MyShopping"][$x]["dimension"];
		$num = $_SESSION["MyShopping"][$x]["num"];
		$additionalText = ($_SESSION["MyShopping"][$x]["additional"]) ? '<b style="color:#cc0066">【加購】</b>- ' : null;
		$additionalText = ($_SESSION["MyShopping"][$x]["fullAdditional"]) ? '<b style="color:#cc0066">【滿額加購】</b>- ' : $additionalText;
		$masterId = ($_SESSION["MyShopping"][$x]["masterId"]) ? $_SESSION["MyShopping"][$x]["masterId"] : null;
		$MyShoppingListAry[$x] = $web_product_id;
		$Product = getProduct($web_product_id);	//取得產品相關資訊
		$Categories = getCategories($Product[web_x_product_id]);
		$_Categories = _getCategories($Categories[web_xxx_product_id]);
		/*
		$IPA =  getenv('REMOTE_ADDR');
		$ipList = array(
			'118.170.45.55',
		);

		if (in_array($IPA, $ipList)){
			echo "<pre>";
			print_r($_Categories);
			echo "</pre>";
		}
		*/
		$Product[link] = ($additionalText) ? null : $Product[link];    //為加購 不帶連結
		if($_SESSION["MyShopping"][$x]["additional"]) {
			continue;
		}
		$saleTitle = null;
		//$saleAry = array();
		$showFlag = 0;
		//銷售組合
		if($Product[web_x_sale_id] && !$_SESSION["MyShopping"][$x]["additional"] && !$_SESSION["MyShopping"][$x]["fullAdditional"]) {
			$Sale = getSale($Product[web_x_sale_id]);
			//echo $Product[web_x_sale_id]."</br>";
			//取是否為同一組合全部數量
			$SaleShoppingCarNum = SearchMultidimensionalArray_2($_SESSION["MyShopping"], 'saleId', $Product[web_x_sale_id]);
			if(count($Sale)) {
				//if($num % $Sale[rows] == 0 || $SaleShoppingCarNum % $Sale[rows] == 0) {
				if($Sale[eventType] != '2') {	//是否為免運
					if($num % $Sale[rows] == 0 || $SaleShoppingCarNum >= $Sale[rows]) {	
						if($Sale[price]) {

							//$Product[price] = floor($Sale[price] / $Sale[rows]);
							$saleTitle = $Sale['subject'];	
							$saleTotal[$Product[web_x_sale_id]] += ($Product[price] * $num);
							$saleNum[$Product[web_x_sale_id]] += $num;
							if($saleNum[$Product[web_x_sale_id]] >= $Sale[rows]) {
								$_minusRow = $saleNum[$Product[web_x_sale_id]] % $Sale[rows];
								$_minus = ($Product[price] * $_minusRow);
							} 

							$saleAry[$Product[web_x_sale_id]] = array(
								'id'			=>	$Product[web_x_sale_id],
								'saleRow'		=>	$Sale[rows],
								'salePrice'		=>	$Sale[price],
								'title'			=>	$saleTitle,
								'num'			=> 	$saleNum[$Product[web_x_sale_id]],
								'total'			=>	$saleTotal[$Product[web_x_sale_id]] - $_minus,

							);

						} else if($Sale[discountnum] < 100 && !$Sale[price]) { 	

							//echo $num % $Sale[rows]; 	
							//echo '-';
							//$Product[price] = (($Sale[rows] % $num == 0) && ($num >= $Sale[rows])) ? $Product[price_cost] : $Product[price];
							//$showFlag = (($Sale[rows] % $num == 0) && ($num >= $Sale[rows])) ? 1 : 0;
							//echo $SaleShoppingCarNum % $Sale[rows].'=='.$num % $Sale[rows]."</br>";
							//echo '_';
							//if($SaleShoppingCarNum % $Sale[rows] == 1 && $num % $Sale[rows] == 2) {
							//if($SaleShoppingCarNum % $Sale[rows] != 0) {
							if(1) {	
								
								if($saleAllNum[$Product[web_x_sale_id]]) {
									//echo $Sale[rows].",";
									$numAry2[$Product[web_x_sale_id]][$web_product_id] = $num;
									$SaleRows[$Product[web_x_sale_id]] = $Sale[rows];
									$numAry[] = $num;
									$saleAllNum[$Product[web_x_sale_id]] = ($saleAllNum[$Product[web_x_sale_id]] >= $SaleRows[$Product[web_x_sale_id]]) ? $saleAllNum[$Product[web_x_sale_id]] - ($saleAllNum[$Product[web_x_sale_id]] % $SaleRows[$Product[web_x_sale_id]]) : $saleAllNum[$Product[web_x_sale_id]];
									
									if(array_sum($numAry2[$Product[web_x_sale_id]]) <= $saleAllNum[$Product[web_x_sale_id]]) {
										$lastNum[$Product[web_x_sale_id]] = array_sum($numAry2[$Product[web_x_sale_id]]);
										$Product[slaePriceTotal] = ($Product[price_cost] * ($numAry2[$Product[web_x_sale_id]][$web_product_id]));
										$Product[truePriceTotal] = 0;
									} else {
										$canDoNum[$Product[web_x_sale_id]] = ($saleAllNum[$Product[web_x_sale_id]] - $lastNum[$Product[web_x_sale_id]]);
										$lastNum[$Product[web_x_sale_id]] += $canDoNum[$Product[web_x_sale_id]];
										if($saleAllNum[$Product[web_x_sale_id]] >= $lastNum[$Product[web_x_sale_id]]) {
											$Product[slaePriceTotal] = ($Product[price_cost] * ($canDoNum[$Product[web_x_sale_id]]));
											$Product[truePriceTotal] = ($Product[price] * ($numAry2[$Product[web_x_sale_id]][$web_product_id] - $canDoNum[$Product[web_x_sale_id]]));
										} else {
											$Product[slaePriceTotal] = 0;
											$Product[truePriceTotal] = ($Product[price] * ($numAry2[$Product[web_x_sale_id]][$web_product_id]));
										}	
									}
									
								}
								
	/*
								$num2 = ($SaleShoppingCarNum % $Sale[rows] == 0) ? $num : $num - ($num % $Sale[rows]);
								$num3 = ($num2) ? $num2 : 1;
								
								$Product[slaePriceTotal] = ($num2) ? ($Product[price_cost] * ($num3)) : 0;
								$Product[truePriceTotal] = ($num2) ? ($Product[price] * ($num - $num3)) : $Product[price] * $num;
	*/							
								$Product[allPriceTotal] = $Product[slaePriceTotal] + $Product[truePriceTotal];
								
							}	
							$Product[price] = $Product[price_cost];
							$showFlag = 1;

							//$Product[price] = round($Product[price] * $Sale[discountnum] / 100);
							$saleTitle = $Sale['subject'];	
							//$saleTotal[$Product[web_x_sale_id]] += ($Product[price] * $num);
							//$saleTotal[$Product[web_x_sale_id]] += $Product[allPriceTotal];
							$saleTotal[$Product[web_x_sale_id]] += $Product[slaePriceTotal];
							$saleNum[$Product[web_x_sale_id]] += $num;
							if($saleNum[$Product[web_x_sale_id]] >= $Sale[rows]) {
								$_minusRow = $saleNum[$Product[web_x_sale_id]] % $Sale[rows];
								$_minus = ($Product[price] * $_minusRow);
							} 

							$saleAry[$Product[web_x_sale_id]] = array(
								'id'				=>	$Product[web_x_sale_id],
								'saleRow'			=>	$Sale[rows],
								'saleDiscountnum'	=>	$Sale[discountnum],
								'title'				=>	$saleTitle,
								'num'				=> 	$saleNum[$Product[web_x_sale_id]],
								//'total'				=>	$saleTotal[$Product[web_x_sale_id]] - $_minus,
								'total'				=>	$saleTotal[$Product[web_x_sale_id]],

							);
						}

					}
				} else {
					$showFlag = 0;
					$saleAry[$Product[web_x_sale_id]] = array(
						'id'			=>	$Product[web_x_sale_id],
						'saleRow'		=>	$Sale[rows],
						'salePrice'		=>	$Sale[price],
						'title'			=>	$Sale['subject'],
						'eventType'		=>  '2'	
					);
					//2017-06-14 改為一般價(取消原價)
					//$Product[price] = $Product[price_cost];  //用原價
				}	
				$saleTitle = $Sale['subject'];
			}
		}

		$Product[price] = ($additionalText) ? $Product[additionalprice] : $Product[price];    //為加購 顯示加購價

		$pic = ShowPic($Product[Covers], "uploadfiles/s/", "uploadfiles/no_image.jpg");

		if (!$ifLogin) $Product[price_memberlevel_tip] = "";
		
		if ($ifEditor) {	//可編輯
			$num_list = "<select name=\"num_".$web_product_id."_".$dimension."\" onChange=\"AlterShoppingCar(this, 'price_".$web_product_id."_".$dimension."');\">";
			$Init_Max_Stock = ($Product['stock'] >= $Init_Max) ? $Init_Max : $Product['stock'];
			$Init_Max_Stock = ($additionalText) ? 1 : $Init_Max_Stock;    //為加購 只能購買一個數量

			for ($j=1; $j<=$Init_Max_Stock; $j++) {
				$num_list .= "<option value=\"".$j."\"";
				if ($num==$j) $num_list .= " selected";
				$num_list .= ">".sprintf("%02d", $j)."</option>\n";
			}
			$num_list .= "</select>";
		} else {
			$num_list = "<span class=\"fw_medium f_size_large color_dark\">".$num."</span>";
		}
		
		if (!$ifEditor) $Product[link] = "";
		
		if ($Product[ifGeneral]==9) {	//加購
			$Product[link] = "";
			$Product[subject] = "【加購】".$Product[subject];
		}
		$shopping_list .= "	
			<tr class=\"tab1b tr_".$masterId."_".$dimension."\" id=\"tr_".$web_product_id."_".$dimension."\">
		        <td>
		            <a".$Product[link]." title=\"".$Product[subject]."\"><img src=\"".$pic."\" alt=\"".$Product[subject]."\" width=\"100px;\"></a>
		        </td>
		        <td class=\"td_style\" style=\"text-align:left;\"><a".$Product[link]." title=\"".$Product[subject]."\">".$additionalText.$Product[subject]."</a>";
		        if($saleTitle)
		        	$shopping_list .= "
		            <p class=\"pro_active\"><span>活動</span>".$saleTitle."</p>";
		        $shopping_list .= "
		        </td>
		        <td>
		            ".$num_list."
		        </td>
		        <td>$".number_format($Product[price])."</td>
		        <td>";
		    if(!$showFlag)
				$shopping_list .= "
					$<span id=\"price_".$web_product_id."_".$dimension."\">".number_format($Product[price]*$num)."</span>";
			else
				$shopping_list .= "
					$<span id=\"price_".$web_product_id."_".$dimension."\">".number_format($Product[allPriceTotal])."</span>";	
		    $shopping_list .= "</td>";
		    if ($ifEditor) {
				$shopping_list .= "
		        <td><a href=\"javascript:;\" onclick=\"DeleteShoppingCar(this, 'tr_".$web_product_id."_".$dimension."');\"><i class=\"fa  fa-times\" aria-hidden=\"true\"></i></a></td>";
		    } else {
		    	/*
		    	$shopping_list .= "
		        <td>&nbsp;</td>";
		    	*/
		    }    	
		       $shopping_list .= " 
		    </tr>
	    ";
	}

	//加購區
	for ($x=0; $x<count($_SESSION["MyShopping"]); $x++) {
		$web_product_id = $_SESSION["MyShopping"][$x]["web_product_id"];
		$dimension = $_SESSION["MyShopping"][$x]["dimension"];
		$num = $_SESSION["MyShopping"][$x]["num"];
		$additionalText = ($_SESSION["MyShopping"][$x]["additional"]) ? '<b style="color:#cc0066">【加購】</b>- ' : null;
		$additionalText = ($_SESSION["MyShopping"][$x]["fullAdditional"]) ? '<b style="color:#cc0066">【滿額加購】</b>- ' : $additionalText;
		$masterId = ($_SESSION["MyShopping"][$x]["masterId"]) ? $_SESSION["MyShopping"][$x]["masterId"] : null;
		$MyShoppingListAry[$x] = $web_product_id;
		$Product = getProduct($web_product_id);	//取得產品相關資訊
		$Product[link] = ($additionalText) ? null : $Product[link];    //為加購 不帶連結
		if(!$_SESSION["MyShopping"][$x]["additional"]) {
			continue;
		}
		$saleTitle = null;
		$showFlag = 0;
		$Product[price] = ($additionalText) ? $Product[additionalprice] : $Product[price];    //為加購 顯示加購價

		$pic = ShowPic($Product[Covers], "uploadfiles/s/", "uploadfiles/no_image.jpg");

		if (!$ifLogin) $Product[price_memberlevel_tip] = "";
		
		if ($ifEditor) {	//可編輯
			$num_list = "<select name=\"num_".$web_product_id."_".$dimension."\" onChange=\"AlterShoppingCar(this, 'price_".$web_product_id."_".$dimension."');\">";
			$Init_Max_Stock = ($Product['stock'] >= $Init_Max) ? $Init_Max : $Product['stock'];
			$Init_Max_Stock = ($additionalText) ? 1 : $Init_Max_Stock;    //為加購 只能購買一個數量

			for ($j=1; $j<=$Init_Max_Stock; $j++) {
				$num_list .= "<option value=\"".$j."\"";
				if ($num==$j) $num_list .= " selected";
				$num_list .= ">".sprintf("%02d", $j)."</option>\n";
			}
			$num_list .= "</select>";
		} else {
			$num_list = "<span class=\"fw_medium f_size_large color_dark\">".$num."</span>";
		}
		
		if (!$ifEditor) $Product[link] = "";
		
		if ($Product[ifGeneral]==9) {	//加購
			$Product[link] = "";
			$Product[subject] = "【加購】".$Product[subject];
		}
		/*
		$shopping_list .= "		
		<div class=\"tab1b tr_".$masterId."_".$dimension."\" id=\"tr_".$web_product_id."_".$dimension."\">							
			<li><a".$Product[link]."><img src=\"".$pic."\" alt=\"".$Product[subject]."\"></a></li>
			<li><a".$Product[link]." class=\"d_inline_b color_dark\">".$additionalText.$Product[subject]."</a>";
		if($saleTitle)
			$shopping_list .= "<br/><a href=\"sale_list.php?kind=".str_front($Product["web_x_sale_id"])."\" title=\"".str_front($Product[saleTitle])."\"><b style='color:red'>".$saleTitle."</b></a>";
		$shopping_list .= "
			</li>
			<li>$".number_format($Product[price])."</li>
			<li>".$num_list."</li>
			<li class=\"tab1t mobile\">小計：</li>
			<li class=\"point\">$<span id=\"price_".$web_product_id."_".$dimension."\">".number_format($Product[price]*$num)."</span></li>	
		";
	
	
		if ($ifEditor) {
			$shopping_list .= "<li><a href=\"javascript:;\" onClick=\"DeleteShoppingCar(this, 'tr_".$web_product_id."_".$dimension."');\" class=\"color_dark\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i></a></li>";
		} else {
			$shopping_list .= "<li>&nbsp;</li>";
		}
		$shopping_list .= "</div>";
		*/
		$shopping_list .= "	
			<tr class=\"tab1b tr_".$masterId."_".$dimension."\" id=\"tr_".$web_product_id."_".$dimension."\">
		        <td>
		            <a".$Product[link]." title=\"".$Product[subject]."\"><img src=\"".$pic."\" alt=\"".$Product[subject]."\" width=\"100px;\"></a>
		        </td>
		        <td class=\"td_style\" style=\"text-align:left;\"><a".$Product[link]." title=\"".$Product[subject]."\">".$additionalText.$Product[subject]."</a>";
		        if($saleTitle)
		        	$shopping_list .= "
		            <p class=\"pro_active\"><span>活動</span>".$saleTitle."</p>";
		        $shopping_list .= "
		        </td>
		        <td>
		            ".$num_list."
		        </td>
		        <td>$".number_format($Product[price])."</td>
		        <td>";
		    if(!$showFlag)
				$shopping_list .= "
					$<span id=\"price_".$web_product_id."_".$dimension."\">".number_format($Product[price]*$num)."</span>";
			else
				$shopping_list .= "
					$<span id=\"price_".$web_product_id."_".$dimension."\">".number_format($Product[allPriceTotal])."</span>";	
		    $shopping_list .= "</td>";
		    if ($ifEditor) {
				$shopping_list .= "
		        <td><a href=\"javascript:;\" onclick=\"DeleteShoppingCar(this, 'tr_".$web_product_id."_".$dimension."_".$_SESSION["MyShopping"][$x]["fullAdditional"]."');\"><i class=\"fa  fa-times\" aria-hidden=\"true\"></i></a></td>";
		    } else {
		    	/*
		    	$shopping_list .= "
		        <td></td>";
		        */
		    }    	
		       $shopping_list .= " 
		    </tr>
	    ";
	}

	$shopping_list .="
		</tbody>
	</table>	
	";
	$shopping_list .= "
		<table class=\"table table-striped\">
            <tbody>
	";                    
	$_SESSION["MyShoppingList"] = $MyShoppingListAry;

	if(($saleAry)) {
		foreach($saleAry as $saleRow) {

			if($saleRow['eventType'] != '2') {
				if($saleRow['salePrice']) {
					$_saleTotal = ($saleRow[total] >= $saleRow['salePrice']) ? $saleRow[total] : $saleRow['salePrice'];
					$_saleNum = ($saleRow[num] >= $saleRow['saleRow']) ? $saleRow[num] : $saleRow['num'];
					$_saleRow =  floor($saleRow[num] / $saleRow['saleRow']);
					$minus = $_saleTotal - ($saleRow['salePrice'] * ($_saleRow));
				} else {
					$_saleTotal = ($saleRow[total] >= $saleRow['salePrice']) ? $saleRow[total] : $saleRow['salePrice'];
					$_saleNum = ($saleRow[num] >= $saleRow['saleRow']) ? $saleRow[num] : $saleRow['num'];
					$_saleRow =  floor($saleRow[num] / $saleRow['saleRow']);
					$minus = $_saleTotal - round(($_saleTotal * $saleRow['saleDiscountnum']) / 100);
				}
			} else {
				$unFreightFlag = 1;
				$_SESSION['unFreightFlag'] = $unFreightFlag;
				//$minus = CalFreightDefault($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney);	//運費=小計-優惠代碼-紅利
				$minus = 0;
			}

			if($saleRow['eventType'] != '2') {		
				$shopping_list .= "	
					<tr>
		                <td class=\"shop_td\">
		                    <div class=\"big\"><span>活動</span>".$saleRow['title']."</div>
		                </td>
		                <td class=\"shop_td2\"></td>
		                <td class=\"shop_td3\"></td>
		                <td class=\"shop_td4 text-center\">-$<span id=\"new_subtotal\">".number_format($minus)."</span></td>
		                <td class=\"shop_td5\">
		                </td>
		            </tr>
		        ";
		    }

		}
		$minusTotal += $minus;
	}

	//小計---------------------------------------------------------------------------------------------------------------
	$subtotal = CalSubtotal();	//小計
	$shopping_list .= "
		<tr>
	        <td class=\"shop_td\">
	        </td>
	        <td class=\"shop_td2 text-center\">
	            共<span class=\"total_color\">".count($_SESSION["MyShopping"])."</span>件 ".$_SESSION[MyMember][level_subject]."
	        </td>
	        <td class=\"shop_td3 text-right\"> 商品合計：
	        </td>
	        <td class=\"shop_td4 text-center\">$".number_format($subtotal)."</td>
	        <td class=\"shop_td5\">
	        </td>
	    </tr>
	";		
										
	//員工不計運費及不可用優惠代碼----------------------------------------------------------------------------------------
	//if($_SESSION['MyMember']['level'] !== '4') {
	//第二階段改為不限制
	if(1) { 

		//優惠代碼---------------------------------------------------------------------------------------------------------------
		$code = CalCode();	//優惠代碼
		$mycoupon = $_SESSION["MyCode"];    
		if ($ifEditor) {
			if(!$mycoupon) {
				
				$shopping_list .= " 
					<tr>
			            <td colspan=\"2\" class=\"shop_td3 text-right\">可折抵優惠代碼
			            </td>
			            <td class=\"shop_td4 pad0 text-center\">
			                <input id=\"mycoupon\" type=\"text\" value=\"".$mycoupon."\" placeholder=\"輸入優惠代碼\">
			            </td>
			            <td class=\"shop_td4 text-center\">-$".number_format($code)."</td>
			            <td class=\"shop_td5 pad0 text-right\">
			                <div class=\"btn_use\">
			                    <input type=\"checkbox\" id=\"use01\" value=\"使用\" checked onClick=\"CheckCoupon();\">
			                    <label for=\"use01\">使用</label>
			                </div>
			            </td>
			        </tr>
			    ";
			} else {
				
				$shopping_list .= " 
					<tr>
			            <td colspan=\"2\" class=\"shop_td3 text-right\">可折抵優惠代碼
			            </td>
			            <td class=\"shop_td4 pad0 text-center\">
			                <input id=\"mycoupon\" type=\"text\" value=\"".$mycoupon."\" placeholder=\"輸入優惠代碼\">
			            </td>
			            <td class=\"shop_td4 text-center\">-$".number_format($code)."</td>
			            <td class=\"shop_td5 pad0 text-right\">
			                <div class=\"btn_use\">
			                    <input type=\"checkbox\" id=\"use01\" value=\"取消\" checked onClick=\"CancleCoupon();\">
			                    <label for=\"use01\">取消</label>
			                </div>
			            </td>
			        </tr>
			    ";
			}		  
		} else {
			//有輸入優惠代碼嗎
			if ($mycoupon)
			$shopping_list .= " 
					<tr>
			            <td colspan=\"2\" class=\"shop_td3 text-right\">
			            	優惠代碼（".$mycoupon."）
			            </td>
			            <td class=\"shop_td4 pad0 text-center\">
			            </td>
			            <td class=\"shop_td4 text-center\">-$".number_format($code)."</td>
			            <td class=\"shop_td5 pad0 text-right\">
			            </td>
			        </tr>
			    "; 
		}
		//紅利折扣---------------------------------------------------------------------------------------------------------------
		$memberShoppingMoney = $_SESSION["MyMemberShoppingMoney"];
		$memberCouponMoney = $_SESSION["MyCouponMoney"];	//優惠券要折扣金額
		$UseBonus = CalUseBonus($subtotal - $code - $memberShoppingMoney - $memberCouponMoney);	//是否符合紅利折扣資格
		$_bonus = $UseBonus[bonus];	//本次的紅利折扣金額
		$bonus = $_SESSION["MyBonus"];	//紅利要折扣金額
		if ($ifLogin && $_bonus) {	//有登入會員
			if ($ifEditor) {
				if(!$bonus) {
					$shopping_list .= "
					<tr>
                        <td colspan=\"2\" class=\"shop_td3  text-right\">可折抵紅利：$".number_format($_bonus, 0)."</td>
                        <td class=\"shop_td4 pad0 text-center\">
                            <input id=\"mybonus\" class=\"td_input\" type=\"text\" value=\"".$_bonus."\" placeholder=\"請輸入金額\">
                        </td>
                        <td class=\"shop_td4 text-center\">-$".number_format($bonus, 0)."</td>
                        <td class=\"shop_td5 pad0\">
                            <div class=\"btn_use\">
                                <input type=\"checkbox\" id=\"use02\" value=\"使用\" checked onClick=\"CheckBonus(".$_bonus.");\">
                                <label for=\"use02\">使用</label>
                            </div>
                        </td>
                    </tr>
					";
				} else {
					$shopping_list .= "
					<tr>
                        <td colspan=\"2\" class=\"shop_td3  text-right\">可折抵紅利：$".number_format($_bonus, 0)."</td>
                        <td class=\"shop_td4 pad0 text-center\">
                            <input id=\"mybonus\" class=\"td_input\" type=\"text\" value=\"".$_bonus."\" placeholder=\"請輸入金額\">
                        </td>
                        <td class=\"shop_td4 text-center\">-$".number_format($bonus, 0)."</td>
                        <td class=\"shop_td5 pad0\">
                            <div class=\"btn_use\">
                                <input type=\"checkbox\" id=\"use02\" value=\"取消\" checked onClick=\"CancleCheckBonus(".$_bonus.");\">
                                <label for=\"use02\">取消</label>
                            </div>
                        </td>
                    </tr>
					";
				}
			} else {
				$shopping_list .= "
					<tr>
                        <td colspan=\"2\" class=\"shop_td3  text-right\">可折抵紅利：$".number_format($_bonus, 0)."</td>
                        <td class=\"shop_td4 pad0 text-center\">
                        </td>
                        <td class=\"shop_td4 text-center\">-$".number_format($bonus, 0)."</td>
                        <td class=\"shop_td5 pad0\">
                        </td>
                    </tr>
				";
			}	 
		} else {
			unset($_SESSION["MyBonus"]);
		}

		//購物金---------------------------------------------------------------------------------------------------------------
		$memberCouponMoney = $_SESSION["MyCouponMoney"];	//優惠券要折扣金額
		$UseMemberCode = CalUseMemberCode($subtotal - $code - $bonus - $memberCouponMoney);	//是否符購物金	
		$_memberShoppingMoney = $UseMemberCode[money];	//本次的購物金折扣金額
		$memberShoppingMoney = $_SESSION["MyMemberShoppingMoney"];	//購物金要折扣金額
		$dateLimit = ($UseMemberCode[dateLimit]) ? "  使用期限  (".$UseMemberCode[dateLimit].")" : null;
		if ($ifLogin && $_memberShoppingMoney) {	//有登入會員
		//if ($ifLogin) {	//有登入會員	
			if ($ifEditor) {
				if(!$memberShoppingMoney) {
					$shopping_list .= "
					<tr>
                        <td colspan=\"2\" class=\"shop_td3  text-right\">可折抵購物金</td>
                        <td class=\"shop_td4 pad0 text-center\">
                            <input id=\"mymembercode\" class=\"td_input\" type=\"text\" value=\"".$_memberShoppingMoney."\" placeholder=\"輸入要抵扣購物金額\">
                        </td>
                        <td class=\"shop_td4 text-center\">-$".number_format($memberShoppingMoney, 0)."</td>
                        <td class=\"shop_td5 pad0\">
                            <div class=\"btn_use\">
                                <input type=\"checkbox\" id=\"use03\" value=\"使用\" checked onClick=\"CheckMemberShoppingMoney(".$_memberShoppingMoney.");\">
                                <label for=\"use03\">使用</label>
                            </div>
                        </td>
                    </tr>
					";
				} else {
					$shopping_list .= "
					<tr>
                        <td colspan=\"2\" class=\"shop_td3  text-right\">可折抵購物金</td>
                        <td class=\"shop_td4 pad0 text-center\">
                            <input id=\"mymembercode\" class=\"td_input\" type=\"text\" value=\"".$_memberShoppingMoney."\" placeholder=\"輸入要抵扣購物金額\">
                        </td>
                        <td class=\"shop_td4 text-center\">-$".number_format($memberShoppingMoney, 0)."</td>
                        <td class=\"shop_td5 pad0\">
                            <div class=\"btn_use\">
                                <input type=\"checkbox\" id=\"use03\" value=\"取消\" checked onClick=\"CancleCheckMemberShoppingMoney(".$_memberShoppingMoney.");\">
                                <label for=\"use03\">取消</label>
                            </div>
                        </td>
                    </tr>
					";
				}
			} else {
				$shopping_list .= "
					<tr>
                        <td colspan=\"2\" class=\"shop_td3  text-right\">可折抵購物金</td>
                        <td class=\"shop_td4 pad0 text-center\">
                        </td>
                        <td class=\"shop_td4 text-center\">-$".number_format($memberShoppingMoney, 0)."</td>
                        <td class=\"shop_td5 pad0\">
                        </td>
                    </tr>
				";
			}		   
		} else {
			unset($_SESSION["MyMemberShoppingMoney"]);
		}


		//優惠券(只限一般會員) 
		//if($_SESSION['MyMember']['level'] === '1') {
		//改為都可使用二階段修改
		if(1) {	
			$memberShoppingMoney = $_SESSION["MyMemberShoppingMoney"];	//購物金要折扣金額
			//優惠券---------------------------------------------------------------------------------------------------------------
			$UseCoupon = CalUseCoupon($subtotal - $code - $bonus - $memberShoppingMoney);	//是否符購物金
			//$_memberCouponMoney = $UseMemberCode[money];	//本次的購物金折扣金額
			$memberCouponMoney = $_SESSION["MyCouponMoney"];	//優惠券要折扣金額
			//$dateLimit = ($UseMemberCode[dateLimit]) ? "  使用期限  (".$UseMemberCode[dateLimit].")" : null;
			$_total = $subtotal - $code - $bonus;
			if ($ifLogin && count($UseCoupon)) {	//有登入會員
				$_web_x_coupon_id = ($_SESSION["MyCoupon"]) ? $_SESSION["MyCoupon"] : 0;
				$Coupon = getCoupon($_web_x_coupon_id);
				$web_x_coupon_list .= "<option value=\"\">優惠券</option>";
				$couponShow = false;
				foreach($UseCoupon as $coupon) {
					if($coupon[money] > $_total) {
						continue;
					}
					//可抵扣產品
					$coupon[bestsellers] = ($coupon[bestsellers]) ? explode(',', $coupon[bestsellers]) : '';
					$result = array_intersect($_SESSION[MyShoppingList], $coupon[bestsellers]);
					if(!count($result)) {
						continue;
					} else {
						$couponShow = true;
					}

					$web_x_coupon_list .= "<option value=\"".$coupon[web_x_coupon_id]."\"";
					if ($_SESSION["MyCoupon"] == $coupon[web_x_coupon_id]) {
						$web_x_coupon_list .= " selected";
						//$default_web_x_freight = $web_x_freight;
					}
					$web_x_coupon_list .= ">【".$coupon[subject]."】 抵扣金額: $".number_format($coupon[money])."</option>\n";
				}
				if($couponShow) {
					if ($ifEditor) {
						$shopping_list .= "
							<tr>
								<td colspan=\"4\" class=\"text-right form-xs-inline\">
									<select name=\"coupon_id\" onChange=\"ChangeCoupon(this);\">
										".$web_x_coupon_list."
									</select>
									<span class=\"hidden-xs\">優惠券：</span>
								</td>
								<td colspan=\"2\" class=\"text-center hidden-xs\">
									<span class=\"visible-xs pull-left\">優惠券：</span>
									<span class=\"point-text\">
										-$".number_format($Coupon[money], 0)."
									</span>
								</td>
							</tr>

							<tr>
								<td colspan=\"2\" class=\"text-center visible-xs\">
									<span class=\"visible-xs pull-left\">優惠券：</span>
									<span class=\"point-text\">
										-$".number_format($Coupon[money], 0)."
									</span>
								</td>
							</tr>
						";
					} else {
						$shopping_list .= "
							<tr>
								<td colspan=\"4\" class=\"text-right form-xs-inline\">
									<select name=\"coupon_id\" onChange=\"ChangeCoupon(this);\" disabled>
										".$web_x_coupon_list."
									</select>
									<span class=\"hidden-xs\">優惠券：</span>
								</td>
								<td colspan=\"2\" class=\"text-center hidden-xs\">
									<span class=\"visible-xs pull-left\">優惠券：</span>
									<span class=\"point-text\">
										-$".number_format($Coupon[money], 0)."
									</span>
								</td>
							</tr>

							<tr>
								<td colspan=\"2\" class=\"text-center visible-xs\">
									<span class=\"visible-xs pull-left\">優惠券：</span>
									<span class=\"point-text\">
										-$".number_format($Coupon[money], 0)."
									</span>
								</td>
							</tr>
						";
					}	
				}     
			} else {
				unset($_SESSION["MyCouponMoney"]);
			}
  			

		}
		$shopping_list .= " 
			<tr>
	            <td class=\"shop_td\"></td>
	            <td colspan=\"2\" class=\"shop_td3 text-right\">發票金額：</td>
	            <td class=\"shop_td4 text-center\">$".number_format($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney)."</td>
	            <td class=\"shop_td5\"></td>
	        </tr>
	    ";    
		
		//運送地點---------------------------------------------------------------------------------------------------------------
		//先找運費預設值	
		$sql = "Select web_xx_freight_id, ifDefault as FreightDefault, subject as web_xx_freight From web_xx_freight Where ifDefault = '1' ";
		$rs = ConnectDB($DB, $sql);
		for ($i=0; $i<mysqli_num_rows($rs); $i++) {
			$row = mysqli_fetch_assoc($rs);
			foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
		}
		
		//有舉行什麼活動嗎
		$sql = "Select web_xx_freight_id, ifDefault as FreightDefault, subject as web_xx_freight From web_xx_freight Where ifDefault = '0' and sdate <= '".date("Y-m-d")."' and edate >= '".date("Y-m-d")."' order by sdate desc, edate desc, web_xx_freight_id desc ";
		$rs = ConnectDB($DB, $sql);
		for ($i=0; $i<mysqli_num_rows($rs); $i++) {
			$row = mysqli_fetch_assoc($rs);
			foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
		}

		//列出運送地點
		$web_x_freight_list = "";
		$sql = "Select web_x_freight_id, subject as web_x_freight From web_x_freight Where web_xx_freight_id = '".$web_xx_freight_id."' and ifShow = '1' order by asort asc, web_x_freight_id desc";
		$rs = ConnectDB($DB, $sql);
		for ($i=0; $i<mysqli_num_rows($rs); $i++) {
			$row = mysqli_fetch_assoc($rs);
			foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
			
			if (intval($_SESSION["MyFreight"])==0) $_SESSION["MyFreight"] = $web_x_freight_id;	//預設值
			/*
			$web_x_freight_list .= "<option value=\"".$web_x_freight_id."\"";
			if ($_SESSION["MyFreight"]==$web_x_freight_id) {
				$web_x_freight_list .= " selected";
				$default_web_x_freight = $web_x_freight;
			}
			$web_x_freight_list .= ">".$web_x_freight."</option>\n";
			*/
			$web_x_freight_list .= "<input class=\"_freight\" onChange=\"ChangeFreight(this);\" type=\"radio\" id=\"ti".$i."\" value=\"".$web_x_freight_id."\"";
			if ($_SESSION["MyFreight"]==$web_x_freight_id) {
				$web_x_freight_list .= " checked";
				$default_web_x_freight = $web_x_freight;
			}
			$web_x_freight_list .= ">\n<label for=\"ti".$i."\">".$web_x_freight."</label>\n";
		}
		$freight = CalFreight($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney);	//運費=小計-優惠代碼-紅利

		//如果有活動為免運費時
		if($_SESSION['unFreightFlag'] && $freight) {
			$minus = CalFreightDefault($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney);	//運費=小計-優惠代碼-紅利
			$freight = $freight - $minus;
			
		}

		$srange = CalFreightRange($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney);	//運費區間=小計-優惠代碼-紅利

		$loseFreight = ($srange > ($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney)) ? ($srange - ($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney)) : 0;
		
		if($_SESSION[bugFlag]) {
			$freight = 0;
		} else {

			if ($ifEditor) {
		        if($loseFreight && !$unFreightFlag) {
			        $shopping_list .="
				        <tr>
			                <td colspan=\"3\"> <span class=\"tooltip-tip\" style=\"display:none\"><i class=\"fa fa-times-circle _close\" aria-hidden=\"true\" style=\"cursor:pointer; float:right;padding-top:10px;\"></i><img src=\"images/tooltip.jpg\" alt=\"\"><h1>還差<span class=\"color\">$".number_format($loseFreight)."</span>就免運囉!</h1>
			                    <p>下方加購區人氣商品超好湊!</p>
			                    </span>
			                </td>
			                <td class=\"shop_td4\"></td>
			                <td class=\"shop_td5\">
			                </td>
			            </tr>
		            ";
		        }        
				$shopping_list .= "
				<tr class=\"bort0 sh_cr\">
	                <td colspan=\"3\" class=\"sh_tr\">
	                    <div class=\"radio\">
	                        ".$web_x_freight_list."
	                    </div>";
	            if($loseFreight) {
	                $shopping_list .= "    
	                    <span class=\"tooltip-container text-right\">
	                        <span class=\"tooltip-content\"><img src=\"images/i-con.png\" alt=\"\" class=\"_show\" style=\"cursor:pointer; vertical-align: top;padding-top: 5px;\">
	                        </span>
	                    </span>";
	            }
	            	$shopping_list .= "        
	                </td>
	                <td class=\"shop_td4  text-center\" style=\"vertical-align:bottom;line-height:2;\">$<span id=\"new_freight\">".number_format($freight)."</span></td>
	                <td class=\"shop_td5\">
	                </td>
	            </tr>
				";
				/*
				$shopping_list .= "  	
				<li class=\"text-right\">
					<select name=\"freight_id\" onChange=\"ChangeFreight(this);\">
						".$web_x_freight_list."
					</select>
				</li>
				<li>
					$<span id=\"new_freight\">".number_format($freight)."</span>
				</li>
				";
				*/  
			} else {
				
				$shopping_list .= "
				<tr class=\"bort0 sh_cr\">
	                <td colspan=\"3\" class=\"sh_tr\">
	                    運送地點：".$default_web_x_freight."
	                </td>
	                <td class=\"shop_td4  text-center\" style=\"vertical-align:bottom;line-height:2;\">$<span id=\"new_freight\">".number_format($freight)."</span></td>
	                <td class=\"shop_td5\">
	                </td>
	            </tr>
				";    
			}
		}
	}

	//貨到付款
	if( $_SESSION['Init_Payment_CashOnDelivery2']) {
		$_Init_Payment_CashOnDelivery2_price = CalCashOnDelivery($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney + $freight);
		if($_SESSION[bugFlag]) {
			$_Init_Payment_CashOnDelivery2_price = 0;
		}
		$shopping_list .= " 
			<tr>
	            <td class=\"shop_td\"></td>
	            <td colspan=\"2\" class=\"shop_td3 text-right\">貨到付款：</td>
	            <td class=\"shop_td4 text-center\">$".number_format($_Init_Payment_CashOnDelivery2_price)."</td>
	            <td class=\"shop_td5\"></td>
	        </tr>
	    ";
	    //$_Init_Payment_CashOnDelivery2_price = $Init_Payment_CashOnDelivery2_price;
	}    
	//總計---------------------------------------------------------------------------------------------------------------
	/*
	$shopping_list .= "  
		<li class=\"sum text-right\">總計</li>
			<li class=\"point\">$<span id=\"new_total\">".number_format($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney + $freight)."</span></li> 
	";
	$shopping_list .= "
            </div>
        </div>    
    ";
    */

    //贈品---------------------------------------------------------------------------------------------------------------
	if (CalGift($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney)!="") {//if不為空值顯示
		/*
		$shopping_list .= "  
			<div class=\"memo\">
				<span id=\"new_gift\">".CalGift($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney)."</span>
			</div>
			</hr>
		";
		*/
		$giftAry = CalGiftList($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney);
		$giftList = "
			<div style=\"display:none\" class=\"gift\">	
				<table>
					<tbody>
		";
			foreach($giftAry as $key => $giftRow) {
				$pic = ShowPic($giftRow[Covers], "uploadfiles/s/", "uploadfiles/no_image.jpg");
				$giftStock = ($giftRow[stock]) ? 1 : '贈品發送完畢';
				$giftList .= "
					<tr class=\"tab1b\">
				        <td>
				           <img src=\"".$pic."\" alt=\"".$giftRow[subject]."\" width=\"100px;\">
				        </td>
				        <td class=\"td_style\" style=\"text-align:left;\">
				        	<p class=\"pro_active\"><span>贈品：</span>".$giftRow[subject]."</p>
				        </td>
				        <td>
				            ".$giftStock."
				        </td>
				        <td>0</td>
				        <td>
							$<span>0</span>
						</td>
				    </tr>    
				";
			}	
		$giftList .= "
					</tobdy>    
				</table>    
			</div>
		";	    	
		$gifText = 	"<p class=\"text-left\" id=\"new_gift\">".CalGift($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney)."</p>";

  	}
	
	//贈送紅利---------------------------------------------------------------------------------------------------------------
	$SendBonus = CalSendBonus($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney);	//紅利:本檔活動
	$web_x_bonus_hidden = $SendBonus[web_x_bonus_hidden];	//紅利流水號
	$web_x_bonus_list = $SendBonus[web_x_bonus_list];		//紅利描述
			
	if ($PageSelfURL!="shoppingsend.php") {
		//if (CalGift($web_x_bonus_list)!="") {//if不為空值顯示
		/*
		$shopping_list .= "  
			<div class=\"memo\">
				<span id=\"new_bonus\">".$web_x_bonus_list."</span>
			</div>
		";  
		//}
		*/
		$bonusText = "<span id=\"new_bonus\">".$web_x_bonus_list."</span>";
	}
	//說明---------------------------------------------------------------------------------------------------------------
	$sql = "Select Init_Freight2 From web_basicset Where web_basicset_id = '1' ";
	$rs = ConnectDB($DB, $sql);
	$row = mysqli_fetch_assoc($rs);
	$Init_Freight2 = $row['Init_Freight2']; 
	/*
 	$shopping_list .= "
 	</hr>
	<div class=\"memo2\">								
		".str_front($Init_Freight2)."
	</div>
	";
	*/ 


    $shopping_list .= "
	    <tr class=\"sh_cr2\">
		    <td class=\"shop_td\">
		        <!--<p class=\"text-left\">◆請確認您購買的商品款式、顏色、數量。確認後訂單無法進行任何更改喔!</p>
		        <p class=\"text-left\">◆商品售價將以您實際結帳之即時價格為主，商品數量、贈品與優惠等，也將以您實際結帳為準。</p>
		        <p class=\"text-left\">◆請儘早完成結帳以確保享有贈品與優惠。</p>-->
		        ".$giftList.$gifText.$bonusText.str_front($Init_Freight2)."
		    </td>
		    <td colspan=\"2\" class=\"text-right total_font\">TOTAL應付金額：</td>
		    <td class=\"shop_td4 text-center total\">$<span id=\"new_total\">".number_format($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney + $freight + $_Init_Payment_CashOnDelivery2_price)."</span></td>
		    <td class=\"shop_td5\">
		    </td>
		</tr>      
	";
	$_SESSION["MyShoppingSubtobal"] = $subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney;	

	$shopping_list .= "
                </tbody>
		    </table>                
		";
	 
	
	return $shopping_list;
}

//購物組合清單
function ShoppingSaleList($saleId) {
	//現在在哪一頁
	$UrlArray = explode("/", htmlentities($_SERVER["PHP_SELF"]));
	$PageSelfURL = strtolower($UrlArray[sizeof($UrlArray)-1]);

	//是否已登入會員
	$ifLogin = MemberLogin();

	
	//最大訂購量
	$sql = "Select Init_Max From web_basicset Where web_basicset_id = '1' ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
	}
	
	//清單---------------------------------------------------------------------------------------------------------------
	$total = 0;	//小計
	$allNum = 0;
	$MyShoppingListAry = array();
	$groupTag = 0;
	$groupTag2 = 0;
	$_j = 0;
	$saleAllNum = getSaleAllNumSale();
	$surplusNum = 0;
	for ($x=0; $x<count($_SESSION["Sales"]); $x++) {
		if($_SESSION["Sales"][$x]["saleId"] != $saleId) {
			continue;
		}
		$web_product_id = $_SESSION["Sales"][$x]["web_product_id"];
		$dimension = $_SESSION["Sales"][$x]["dimension"];
		$num = $_SESSION["Sales"][$x]["num"];
		$additionalText = ($_SESSION["Sales"][$x]["additional"]) ? '<b style="color:#cc0066">【加購】</b>- ' : null;
		$additionalText = ($_SESSION["Sales"][$x]["fullAdditional"]) ? '<b style="color:#cc0066">【滿額加購】</b>- ' : $additionalText;
		$masterId = ($_SESSION["Sales"][$x]["masterId"]) ? $_SESSION["Sales"][$x]["masterId"] : null;
		$MyShoppingListAry[$x] = $web_product_id;
		$Product = getProduct($web_product_id);	//取得產品相關資訊
		$Product[link] = ($additionalText) ? null : $Product[link];    //為加購 不帶連結
		$saleTitle = null;
		//銷售組合
		if($Product[web_x_sale_id] && !$_SESSION["Sales"][$x]["additional"] && !$_SESSION["Sales"][$x]["fullAdditional"]) {
			$Sale = getSale($Product[web_x_sale_id]);
			if($Sale[eventType] != '2') {	//是否為免運
				//echo $Product[web_x_sale_id]."</br>";
				//取是否為同一組合全部數量
				$SaleShoppingCarNum = SearchMultidimensionalArray_2($_SESSION["Sales"], 'saleId', $Product[web_x_sale_id]);
				if(count($Sale)) {
					//if($num % $Sale[rows] == 0 || $SaleShoppingCarNum % $Sale[rows] == 0) {
					if($num % $Sale[rows] == 0 || $SaleShoppingCarNum >= $Sale[rows]) {	
						if($Sale[price]) {

							//$Product[price] = floor($Sale[price] / $Sale[rows]);
							$saleTitle = $Sale['subject'];	
							$saleTotal[$Product[web_x_sale_id]] += ($Product[price] * $num);
							$saleNum[$Product[web_x_sale_id]] += $num;
							if($saleNum[$Product[web_x_sale_id]] >= $Sale[rows]) {
								$_minusRow = $saleNum[$Product[web_x_sale_id]] % $Sale[rows];
								$_minus = ($Product[price] * $_minusRow);
							} 
							//echo $saleTotal[$Product[web_x_sale_id]];
							$saleAry[$Product[web_x_sale_id]] = array(
								'id'			=>	$Product[web_x_sale_id],
								'saleRow'		=>	$Sale[rows],
								'salePrice'		=>	$Sale[price],
								'title'			=>	$saleTitle,
								'num'			=> 	$saleNum[$Product[web_x_sale_id]],
								'total'			=>	$saleTotal[$Product[web_x_sale_id]] - $_minus,
								'Alltotal'		=>	$saleTotal[$Product[web_x_sale_id]],

							);

						} else if($Sale[discountnum] < 100 && !$Sale[price]) { 	

							//echo $num % $Sale[rows]; 	
							//echo '-';
							//$Product[price] = (($Sale[rows] % $num == 0) && ($num >= $Sale[rows])) ? $Product[price_cost] : $Product[price];
							//$showFlag = (($Sale[rows] % $num == 0) && ($num >= $Sale[rows])) ? 1 : 0;
							//echo $SaleShoppingCarNum % $Sale[rows].'=='.$num % $Sale[rows]."</br>";
							//echo '_';
							//if($SaleShoppingCarNum % $Sale[rows] == 1 && $num % $Sale[rows] == 2) {
							//if($SaleShoppingCarNum % $Sale[rows] != 0) {
							if(1) {	
								
								if($saleAllNum[$Product[web_x_sale_id]]) {
									//echo $Sale[rows].",";
									$numAry2[$Product[web_x_sale_id]][$web_product_id] = $num;
									$SaleRows[$Product[web_x_sale_id]] = $Sale[rows];
									$numAry[] = $num;
									$saleAllNum[$Product[web_x_sale_id]] = ($saleAllNum[$Product[web_x_sale_id]] >= $SaleRows[$Product[web_x_sale_id]]) ? $saleAllNum[$Product[web_x_sale_id]] - ($saleAllNum[$Product[web_x_sale_id]] % $SaleRows[$Product[web_x_sale_id]]) : $saleAllNum[$Product[web_x_sale_id]];
									
									if(array_sum($numAry2[$Product[web_x_sale_id]]) <= $saleAllNum[$Product[web_x_sale_id]]) {
										$lastNum[$Product[web_x_sale_id]] = array_sum($numAry2[$Product[web_x_sale_id]]);
										$Product[slaePriceTotal] = ($Product[price_cost] * ($numAry2[$Product[web_x_sale_id]][$web_product_id]));
										$Product[truePriceTotal] = 0;
									} else {
										$canDoNum[$Product[web_x_sale_id]] = ($saleAllNum[$Product[web_x_sale_id]] - $lastNum[$Product[web_x_sale_id]]);
										$lastNum[$Product[web_x_sale_id]] += $canDoNum[$Product[web_x_sale_id]];
										if($saleAllNum[$Product[web_x_sale_id]] >= $lastNum[$Product[web_x_sale_id]]) {
											$Product[slaePriceTotal] = ($Product[price_cost] * ($canDoNum[$Product[web_x_sale_id]]));
											$Product[truePriceTotal] = ($Product[price] * ($numAry2[$Product[web_x_sale_id]][$web_product_id] - $canDoNum[$Product[web_x_sale_id]]));
										} else {
											$Product[slaePriceTotal] = 0;
											$Product[truePriceTotal] = ($Product[price] * ($numAry2[$Product[web_x_sale_id]][$web_product_id]));
										}	
									}
									
								}
								
	/*
								$num2 = ($SaleShoppingCarNum % $Sale[rows] == 0) ? $num : $num - ($num % $Sale[rows]);
								$num3 = ($num2) ? $num2 : 1;
								
								$Product[slaePriceTotal] = ($num2) ? ($Product[price_cost] * ($num3)) : 0;
								$Product[truePriceTotal] = ($num2) ? ($Product[price] * ($num - $num3)) : $Product[price] * $num;
	*/							
								$Product[allPriceTotal] = $Product[slaePriceTotal] + $Product[truePriceTotal];
								
							}	
							$Product[price] = $Product[price_cost];
							$showFlag = 1;

							//$Product[price] = round($Product[price] * $Sale[discountnum] / 100);
							$saleTitle = $Sale['subject'];	
							//$saleTotal[$Product[web_x_sale_id]] += ($Product[price] * $num);
							$saleAllTotal[$Product[web_x_sale_id]] += $Product[allPriceTotal];
							$saleTotal[$Product[web_x_sale_id]] += $Product[slaePriceTotal];
							$saleNum[$Product[web_x_sale_id]] += $num;
							if($saleNum[$Product[web_x_sale_id]] >= $Sale[rows]) {
								$_minusRow = $saleNum[$Product[web_x_sale_id]] % $Sale[rows];
								$_minus = ($Product[price] * $_minusRow);
							} 

							$saleAry[$Product[web_x_sale_id]] = array(
								'id'				=>	$Product[web_x_sale_id],
								'saleRow'			=>	$Sale[rows],
								'saleDiscountnum'	=>	$Sale[discountnum],
								'title'				=>	$saleTitle,
								'num'				=> 	$saleNum[$Product[web_x_sale_id]],
								//'total'				=>	$saleTotal[$Product[web_x_sale_id]] - $_minus,
								'total'				=>	$saleTotal[$Product[web_x_sale_id]],
								'Alltotal'				=>	$saleAllTotal[$Product[web_x_sale_id]],

							);
						}

					}
					$saleTitle = $Sale['subject'];
				}
			}	
		}

		$Product[price] = ($additionalText) ? $Product[additionalprice] : $Product[price];    //為加購 顯示加購價

		$pic = ShowPic($Product[Covers], "uploadfiles/m/", "uploadfiles/no_image.jpg");

		
		if (!$ifEditor) $Product[link] = "";
		
		if ($Product[ifGeneral]==9) {	//加購
			$Product[link] = "";
			$Product[subject] = "【加購】".$Product[subject];
		}
		
		$sale_list_info .= "	
			<div class=\"pro_effect\" id=\"tr_".$web_product_id."_".$dimension."\" class=\"tr_".$masterId."_".$dimension."\">
	            <figure>
	                <img src=\"".$pic."\" alt=\"".$Product[subject]."\">
	            </figure>
	            <div class=\"item_pro\">
	                <a class=\"wish\" data-tooltip=\"取消\" onClick=\"DeleteSalesCar(this, 'tr_".$web_product_id."_".$dimension."_".$additionalText."', '".$_SESSION["Sales"][$x]["saleId"]."');\">
	                <i class=\"fa fa-trash icon_cart\" aria-hidden=\"true\"></i>
	                </a>
	            </div>
	        
	            <!--<div class=\"itemleft iteminfo\">".$Product[subject]."</div>-->
	           	<!--<div class=\"text-right\">
	                <span><s class=\"gray\">市價$".$Product[price_cost]."</s></span>
	                <div class=\"price\">$".number_format($Product[price])."</div>
	            </div>-->
				<div class=\"text-center\">
					<span class=\"point\">$".number_format($Product[price])."</span>x".$num."
				</div>
	        </div>
	    ";
	    $allNum += $num;
	}

	if(($saleAry)) {
		foreach($saleAry as $saleRow) {

			if($saleRow['salePrice']) {
				$_saleTotal = ($saleRow[total] >= $saleRow['salePrice']) ? $saleRow[total] : $saleRow['salePrice'];
				$_saleAllTotal = $saleRow[Alltotal];
				$_saleNum = ($saleRow[num] >= $saleRow['saleRow']) ? $saleRow[num] : $saleRow['num'];
				$_saleRow =  floor($saleRow[num] / $saleRow['saleRow']);
				$minus = $_saleTotal - ($saleRow['salePrice'] * ($_saleRow));

				//顯示用
				$_saleTotal = $_saleAllTotal;
			} else {
				$_saleTotal = ($saleRow[total] >= $saleRow['salePrice']) ? $saleRow[total] : $saleRow['salePrice'];
				$_saleAllTotal = ($saleRow[Alltotal] >= $saleRow['salePrice']) ? $saleRow[Alltotal] : $saleRow['salePrice'];
				$_saleNum = ($saleRow[num] >= $saleRow['saleRow']) ? $saleRow[num] : $saleRow['num'];
				$_saleRow =  floor($saleRow[num] / $saleRow['saleRow']);
				$minus = $_saleTotal - round(($_saleTotal * $saleRow['saleDiscountnum']) / 100);

				//顯示用
				$_saleTotal = $_saleAllTotal;
			}
				

		}
		$minusTotal += $minus;
	}
	
	$sale_btn = "
		<div id=\"saleBtn\" style=\"display:none\">
			<div class=\"col-md-12 col-sm-12 col-xs-12 pad0\">";
		if($Sale[eventType] != '2') {	//是否為免運
			$sale_btn .= "	    	
				<p class=\"col-md-6 col-sm-12 col-xs-12 price_01 pad0\"><s class=\"pro_p\">原價$".number_format($_saleTotal)."</s></p>
				<p class=\"col-md-6 col-sm-12 col-xs-12 price_02 pad0 m_t_-1\">總額 : <span class=\"pro_member\">$".number_format($_saleTotal-$minusTotal)."</span></p>";
		}	
			$sale_btn .= "
				<div class=\"group mar_10\" style=\"width:100%;\">";
		if($Sale[eventType] != '2') {	//是否為免運	    	

			if($minusTotal)	{
				$sale_btn .= "	
					<button type=\"button\" class=\"btn btn-lg btn-warning btn-warning-1 mar_10 button_a\" onclick=\"salesToShoppingCar(this, '".$saleId."');\">
					<a style=\"cursor: pointer\">加入購物車</a>
					</button>
					<button type=\"button\" class=\"btn btn-lg btn-danger btn-danger-1 mar_10 button_b\" onclick=\"salesToShoppingCar(this, '".$saleId."', 'shopping-cart.html');\">
					<a style=\"cursor: pointer\">立即結帳</a>
					</button>";
			} else {
				$sale_btn .= "	
					<button type=\"button\" class=\"btn btn-lg btn-danger btn-danger-1 mar_10 button_b\">
					<a style=\"cursor: pointer\">未達到活動件</a>
					</button>
				";	
			}
		} else {
			if($allNum >= $Sale[rows]) {
				$sale_btn .= "	
					<button type=\"button\" class=\"btn btn-lg btn-warning btn-warning-1 mar_10 button_a\" onclick=\"salesToShoppingCar(this, '".$saleId."');\">
					<a style=\"cursor: pointer\">加入購物車</a>
					</button>
					<button type=\"button\" class=\"btn btn-lg btn-danger btn-danger-1 mar_10 button_b\" onclick=\"salesToShoppingCar(this, '".$saleId."', 'shopping-cart.html');\">
					<a style=\"cursor: pointer\">立即結帳</a>
					</button>";
			} else {
				$sale_btn .= "	
					<button type=\"button\" class=\"btn btn-lg btn-danger btn-danger-1 mar_10 button_b\">
					<a style=\"cursor: pointer\">未達到活動件</a>
					</button>
				";	
			}
		}			
					
			$sale_btn .= "	
				</div>
			</div>
		</div>
	";

	//購物車
	$sale_list = "	
		<div class=\"sale-top\">
		    已選購<span class=\"point\">".$allNum."</span>件";
	if($Sale[eventType] != '2') {	//是否為免運	    
		$sale_list .= "    
		    ，優惠價<span class=\"point\">".number_format($_saleTotal-$minusTotal)."</span>元";
	} else {
		if($allNum >= $Sale[rows]) {
			$sale_list .= "    
		    	，免運費";    
		}
	}
	$sale_list .= "		
		</div>
		<div class=\"clearfix\"></div>
		<div class=\"sale-slick\">".$sale_list_info."
	";
	$sale_list .= "</div>".$sale_btn;
	
	return $sale_list;
}

//購物清單 shopping_car.php
function ShoppingCarListConfirm($ifStop) {
	//現在在哪一頁
	$UrlArray = explode("/", htmlentities($_SERVER["PHP_SELF"]));
	$PageSelfURL = strtolower($UrlArray[sizeof($UrlArray)-1]);

	$ifEditor = ($ifStop==0) ? true : false;
	//$ifEditor = false;
	
	//是否已登入會員
	$ifLogin = MemberLogin();

	
	//最大訂購量
	$sql = "Select Init_Max, Init_Payment_CashOnDelivery2_price From web_basicset Where web_basicset_id = '1' ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
	}
		
	
	//可編輯
	if ($ifEditor) {
		$thDelete = "<th>刪除</th>";
	} else {	
		//$tdDelete = "<th>&nbsp;</th>";
	}		

	//購物車		
	$shopping_list ="
		<table class=\"table table-striped table_style\">
			<thead>
				<tr class=\"tr_bg tr_br\">
					<th>產品縮圖</th>
					<th>品名</th>
					<th>數量</th>
					<th>單價</th>
					<th>小計</th>
					".$thDelete."
				</tr>
			</thead>
	";		 
	$shopping_list .="<tbody>";	 


	
	//清單---------------------------------------------------------------------------------------------------------------
	$total = 0;	//小計
	$MyShoppingListAry = array();
	$groupTag = 0;
	$groupTag2 = 0;
	$_j = 0;
	$saleAllNum = getSaleAllNum();
	$surplusNum = 0;
	$unFreightFlag = 0;
	$_SESSION['unFreightFlag'] = $unFreightFlag;
	for ($x=0; $x<count($_SESSION["MyShopping"]); $x++) {
		$web_product_id = $_SESSION["MyShopping"][$x]["web_product_id"];
		$dimension = $_SESSION["MyShopping"][$x]["dimension"];
		$num = $_SESSION["MyShopping"][$x]["num"];
		$additionalText = ($_SESSION["MyShopping"][$x]["additional"]) ? '<b style="color:#cc0066">【加購】</b>- ' : null;
		$additionalText = ($_SESSION["MyShopping"][$x]["fullAdditional"]) ? '<b style="color:#cc0066">【滿額加購】</b>- ' : $additionalText;
		$masterId = ($_SESSION["MyShopping"][$x]["masterId"]) ? $_SESSION["MyShopping"][$x]["masterId"] : null;
		$MyShoppingListAry[$x] = $web_product_id;
		$Product = getProduct($web_product_id);	//取得產品相關資訊
		$Categories = getCategories($Product[web_x_product_id]);
		$_Categories = _getCategories($Categories[web_xxx_product_id]);
		/*
		$IPA =  getenv('REMOTE_ADDR');
		$ipList = array(
			'118.170.45.55',
		);

		if (in_array($IPA, $ipList)){
			echo "<pre>";
			print_r($_Categories);
			echo "</pre>";
		}
		*/
		$Product[link] = ($additionalText) ? null : $Product[link];    //為加購 不帶連結
		if($_SESSION["MyShopping"][$x]["additional"]) {
			continue;
		}
		$saleTitle = null;
		$showFlag = 0;
		//$saleAry = array();
		//銷售組合
		if($Product[web_x_sale_id] && !$_SESSION["MyShopping"][$x]["additional"] && !$_SESSION["MyShopping"][$x]["fullAdditional"]) {
			$Sale = getSale($Product[web_x_sale_id]);
			//echo $Product[web_x_sale_id]."</br>";
			//取是否為同一組合全部數量
			$SaleShoppingCarNum = SearchMultidimensionalArray_2($_SESSION["MyShopping"], 'saleId', $Product[web_x_sale_id]);
			if(count($Sale)) {
				if($Sale[eventType] != '2') {	//是否為免運
					//if($num % $Sale[rows] == 0 || $SaleShoppingCarNum % $Sale[rows] == 0) {
					if($num % $Sale[rows] == 0 || $SaleShoppingCarNum >= $Sale[rows]) {	
						if($Sale[price]) {

							//$Product[price] = floor($Sale[price] / $Sale[rows]);
							$saleTitle = $Sale['subject'];	
							$saleTotal[$Product[web_x_sale_id]] += ($Product[price] * $num);
							$saleNum[$Product[web_x_sale_id]] += $num;
							if($saleNum[$Product[web_x_sale_id]] >= $Sale[rows]) {
								$_minusRow = $saleNum[$Product[web_x_sale_id]] % $Sale[rows];
								$_minus = ($Product[price] * $_minusRow);
							} 

							$saleAry[$Product[web_x_sale_id]] = array(
								'id'			=>	$Product[web_x_sale_id],
								'saleRow'		=>	$Sale[rows],
								'salePrice'		=>	$Sale[price],
								'title'			=>	$saleTitle,
								'num'			=> 	$saleNum[$Product[web_x_sale_id]],
								'total'			=>	$saleTotal[$Product[web_x_sale_id]] - $_minus,

							);

						} else if($Sale[discountnum] < 100 && !$Sale[price]) { 	

							//echo $num % $Sale[rows]; 	
							//echo '-';
							//$Product[price] = (($Sale[rows] % $num == 0) && ($num >= $Sale[rows])) ? $Product[price_cost] : $Product[price];
							//$showFlag = (($Sale[rows] % $num == 0) && ($num >= $Sale[rows])) ? 1 : 0;
							//echo $SaleShoppingCarNum % $Sale[rows].'=='.$num % $Sale[rows]."</br>";
							//echo '_';
							//if($SaleShoppingCarNum % $Sale[rows] == 1 && $num % $Sale[rows] == 2) {
							//if($SaleShoppingCarNum % $Sale[rows] != 0) {
							if(1) {	
								
								if($saleAllNum[$Product[web_x_sale_id]]) {
									//echo $Sale[rows].",";
									$numAry2[$Product[web_x_sale_id]][$web_product_id] = $num;
									$SaleRows[$Product[web_x_sale_id]] = $Sale[rows];
									$numAry[] = $num;
									$saleAllNum[$Product[web_x_sale_id]] = ($saleAllNum[$Product[web_x_sale_id]] >= $SaleRows[$Product[web_x_sale_id]]) ? $saleAllNum[$Product[web_x_sale_id]] - ($saleAllNum[$Product[web_x_sale_id]] % $SaleRows[$Product[web_x_sale_id]]) : $saleAllNum[$Product[web_x_sale_id]];
									
									if(array_sum($numAry2[$Product[web_x_sale_id]]) <= $saleAllNum[$Product[web_x_sale_id]]) {
										$lastNum[$Product[web_x_sale_id]] = array_sum($numAry2[$Product[web_x_sale_id]]);
										$Product[slaePriceTotal] = ($Product[price_cost] * ($numAry2[$Product[web_x_sale_id]][$web_product_id]));
										$Product[truePriceTotal] = 0;
									} else {
										$canDoNum[$Product[web_x_sale_id]] = ($saleAllNum[$Product[web_x_sale_id]] - $lastNum[$Product[web_x_sale_id]]);
										$lastNum[$Product[web_x_sale_id]] += $canDoNum[$Product[web_x_sale_id]];
										if($saleAllNum[$Product[web_x_sale_id]] >= $lastNum[$Product[web_x_sale_id]]) {
											$Product[slaePriceTotal] = ($Product[price_cost] * ($canDoNum[$Product[web_x_sale_id]]));
											$Product[truePriceTotal] = ($Product[price] * ($numAry2[$Product[web_x_sale_id]][$web_product_id] - $canDoNum[$Product[web_x_sale_id]]));
										} else {
											$Product[slaePriceTotal] = 0;
											$Product[truePriceTotal] = ($Product[price] * ($numAry2[$Product[web_x_sale_id]][$web_product_id]));
										}	
									}
									
								}
								
	/*
								$num2 = ($SaleShoppingCarNum % $Sale[rows] == 0) ? $num : $num - ($num % $Sale[rows]);
								$num3 = ($num2) ? $num2 : 1;
								
								$Product[slaePriceTotal] = ($num2) ? ($Product[price_cost] * ($num3)) : 0;
								$Product[truePriceTotal] = ($num2) ? ($Product[price] * ($num - $num3)) : $Product[price] * $num;
	*/							
								$Product[allPriceTotal] = $Product[slaePriceTotal] + $Product[truePriceTotal];
								
							}	
							$Product[price] = $Product[price_cost];
							$showFlag = 1;

							//$Product[price] = round($Product[price] * $Sale[discountnum] / 100);
							$saleTitle = $Sale['subject'];	
							//$saleTotal[$Product[web_x_sale_id]] += ($Product[price] * $num);
							//$saleTotal[$Product[web_x_sale_id]] += $Product[allPriceTotal];
							$saleTotal[$Product[web_x_sale_id]] += $Product[slaePriceTotal];
							$saleNum[$Product[web_x_sale_id]] += $num;
							if($saleNum[$Product[web_x_sale_id]] >= $Sale[rows]) {
								$_minusRow = $saleNum[$Product[web_x_sale_id]] % $Sale[rows];
								$_minus = ($Product[price] * $_minusRow);
							} 

							$saleAry[$Product[web_x_sale_id]] = array(
								'id'				=>	$Product[web_x_sale_id],
								'saleRow'			=>	$Sale[rows],
								'saleDiscountnum'	=>	$Sale[discountnum],
								'title'				=>	$saleTitle,
								'num'				=> 	$saleNum[$Product[web_x_sale_id]],
								//'total'				=>	$saleTotal[$Product[web_x_sale_id]] - $_minus,
								'total'				=>	$saleTotal[$Product[web_x_sale_id]],

							);
						}

					}
				} else {
					$showFlag = 0;
					$saleAry[$Product[web_x_sale_id]] = array(
						'id'			=>	$Product[web_x_sale_id],
						'saleRow'		=>	$Sale[rows],
						'salePrice'		=>	$Sale[price],
						'title'			=>	$Sale['subject'],
						'eventType'		=>  '2'	
					);
					//2017-06-14 改為一般價(取消原價)
					//$Product[price] = $Product[price_cost];  //用原價
				}		
				$saleTitle = $Sale['subject'];
			}
		}

		$Product[price] = ($additionalText) ? $Product[additionalprice] : $Product[price];    //為加購 顯示加購價

		$pic = ShowPic($Product[Covers], "uploadfiles/s/", "uploadfiles/no_image.jpg");

		if (!$ifLogin) $Product[price_memberlevel_tip] = "";
		
		if ($ifEditor) {	//可編輯
			$num_list = "<select name=\"num_".$web_product_id."_".$dimension."\" onChange=\"AlterShoppingCar(this, 'price_".$web_product_id."_".$dimension."');\">";
			$Init_Max_Stock = ($Product['stock'] >= $Init_Max) ? $Init_Max : $Product['stock'];
			$Init_Max_Stock = ($additionalText) ? 1 : $Init_Max_Stock;    //為加購 只能購買一個數量

			for ($j=1; $j<=$Init_Max_Stock; $j++) {
				$num_list .= "<option value=\"".$j."\"";
				if ($num==$j) $num_list .= " selected";
				$num_list .= ">".sprintf("%02d", $j)."</option>\n";
			}
			$num_list .= "</select>";
		} else {
			$num_list = "<span class=\"fw_medium f_size_large color_dark\">".$num."</span>";
		}
		
		if (!$ifEditor) $Product[link] = "";
		
		if ($Product[ifGeneral]==9) {	//加購
			$Product[link] = "";
			$Product[subject] = "【加購】".$Product[subject];
		}
		$shopping_list .= "	
			<tr class=\"tab1b tr_".$masterId."_".$dimension."\" id=\"tr_".$web_product_id."_".$dimension."\">
		        <td>
		            <a".$Product[link]." title=\"".$Product[subject]."\"><img src=\"".$pic."\" alt=\"".$Product[subject]."\" width=\"100px;\"></a>
		        </td>
		        <td class=\"td_style\" style=\"text-align:left;\"><a".$Product[link]." title=\"".$Product[subject]."\">".$additionalText.$Product[subject]."</a>";
		        if($saleTitle)
		        	$shopping_list .= "
		            <p class=\"pro_active\"><span>活動</span>".$saleTitle."</p>";
		        $shopping_list .= "
		        </td>
		        <td>
		            ".$num_list."
		        </td>
		        <td>$".number_format($Product[price])."</td>
		        <td>";
		    if(!$showFlag)
				$shopping_list .= "
					$<span id=\"price_".$web_product_id."_".$dimension."\">".number_format($Product[price]*$num)."</span>";
			else
				$shopping_list .= "
					$<span id=\"price_".$web_product_id."_".$dimension."\">".number_format($Product[allPriceTotal])."</span>";	
		    $shopping_list .= "</td>";
		    if ($ifEditor) {
				$shopping_list .= "
		        <td><a href=\"javascript:;\" onclick=\"DeleteShoppingCar(this, 'tr_".$web_product_id."_".$dimension."');\"><i class=\"fa  fa-times\" aria-hidden=\"true\"></i></a></td>";
		    } else {
		    	/*
		    	$shopping_list .= "
		        <td>&nbsp;</td>";
		    	*/
		    }    	
		       $shopping_list .= " 
		    </tr>
	    ";
	}

	//加購區
	for ($x=0; $x<count($_SESSION["MyShopping"]); $x++) {
		$web_product_id = $_SESSION["MyShopping"][$x]["web_product_id"];
		$dimension = $_SESSION["MyShopping"][$x]["dimension"];
		$num = $_SESSION["MyShopping"][$x]["num"];
		$additionalText = ($_SESSION["MyShopping"][$x]["additional"]) ? '<b style="color:#cc0066">【加購】</b>- ' : null;
		$additionalText = ($_SESSION["MyShopping"][$x]["fullAdditional"]) ? '<b style="color:#cc0066">【滿額加購】</b>- ' : $additionalText;
		$masterId = ($_SESSION["MyShopping"][$x]["masterId"]) ? $_SESSION["MyShopping"][$x]["masterId"] : null;
		$MyShoppingListAry[$x] = $web_product_id;
		$Product = getProduct($web_product_id);	//取得產品相關資訊
		$Product[link] = ($additionalText) ? null : $Product[link];    //為加購 不帶連結
		if(!$_SESSION["MyShopping"][$x]["additional"]) {
			continue;
		}
		$saleTitle = null;
		$showFlag = 0;
		$Product[price] = ($additionalText) ? $Product[additionalprice] : $Product[price];    //為加購 顯示加購價

		$pic = ShowPic($Product[Covers], "uploadfiles/s/", "uploadfiles/no_image.jpg");

		if (!$ifLogin) $Product[price_memberlevel_tip] = "";
		
		if ($ifEditor) {	//可編輯
			$num_list = "<select name=\"num_".$web_product_id."_".$dimension."\" onChange=\"AlterShoppingCar(this, 'price_".$web_product_id."_".$dimension."');\">";
			$Init_Max_Stock = ($Product['stock'] >= $Init_Max) ? $Init_Max : $Product['stock'];
			$Init_Max_Stock = ($additionalText) ? 1 : $Init_Max_Stock;    //為加購 只能購買一個數量

			for ($j=1; $j<=$Init_Max_Stock; $j++) {
				$num_list .= "<option value=\"".$j."\"";
				if ($num==$j) $num_list .= " selected";
				$num_list .= ">".sprintf("%02d", $j)."</option>\n";
			}
			$num_list .= "</select>";
		} else {
			$num_list = "<span class=\"fw_medium f_size_large color_dark\">".$num."</span>";
		}
		
		if (!$ifEditor) $Product[link] = "";
		
		if ($Product[ifGeneral]==9) {	//加購
			$Product[link] = "";
			$Product[subject] = "【加購】".$Product[subject];
		}
		/*
		$shopping_list .= "		
		<div class=\"tab1b tr_".$masterId."_".$dimension."\" id=\"tr_".$web_product_id."_".$dimension."\">							
			<li><a".$Product[link]."><img src=\"".$pic."\" alt=\"".$Product[subject]."\"></a></li>
			<li><a".$Product[link]." class=\"d_inline_b color_dark\">".$additionalText.$Product[subject]."</a>";
		if($saleTitle)
			$shopping_list .= "<br/><a href=\"sale_list.php?kind=".str_front($Product["web_x_sale_id"])."\" title=\"".str_front($Product[saleTitle])."\"><b style='color:red'>".$saleTitle."</b></a>";
		$shopping_list .= "
			</li>
			<li>$".number_format($Product[price])."</li>
			<li>".$num_list."</li>
			<li class=\"tab1t mobile\">小計：</li>
			<li class=\"point\">$<span id=\"price_".$web_product_id."_".$dimension."\">".number_format($Product[price]*$num)."</span></li>	
		";
	
	
		if ($ifEditor) {
			$shopping_list .= "<li><a href=\"javascript:;\" onClick=\"DeleteShoppingCar(this, 'tr_".$web_product_id."_".$dimension."');\" class=\"color_dark\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i></a></li>";
		} else {
			$shopping_list .= "<li>&nbsp;</li>";
		}
		$shopping_list .= "</div>";
		*/
		$shopping_list .= "	
			<tr class=\"tab1b tr_".$masterId."_".$dimension."\" id=\"tr_".$web_product_id."_".$dimension."\">
		        <td>
		            <a".$Product[link]." title=\"".$Product[subject]."\"><img src=\"".$pic."\" alt=\"".$Product[subject]."\" width=\"100px;\"></a>
		        </td>
		        <td class=\"td_style\" style=\"text-align:left;\"><a".$Product[link]." title=\"".$Product[subject]."\">".$additionalText.$Product[subject]."</a>";
		        if($saleTitle)
		        	$shopping_list .= "
		            <p class=\"pro_active\"><span>活動</span>".$saleTitle."</p>";
		        $shopping_list .= "
		        </td>
		        <td>
		            ".$num_list."
		        </td>
		        <td>$".number_format($Product[price])."</td>
		        <td>";
		    if(!$showFlag)
				$shopping_list .= "
					$<span id=\"price_".$web_product_id."_".$dimension."\">".number_format($Product[price]*$num)."</span>";
			else
				$shopping_list .= "
					$<span id=\"price_".$web_product_id."_".$dimension."\">".number_format($Product[allPriceTotal])."</span>";	
		    $shopping_list .= "</td>";
		    if ($ifEditor) {
				$shopping_list .= "
		        <td><a href=\"javascript:;\" onclick=\"DeleteShoppingCar(this, 'tr_".$web_product_id."_".$dimension."');\"><i class=\"fa  fa-times\" aria-hidden=\"true\"></i></a></td>";
		    } else {
		    	/*
		    	$shopping_list .= "
		        <td></td>";
		        */
		    }    	
		       $shopping_list .= " 
		    </tr>
	    ";
	}
	$shopping_list .="
		</tbody>
	</table>	
	";
	               
	$_SESSION["MyShoppingList"] = $MyShoppingListAry;

	if(($saleAry)) {
		foreach($saleAry as $saleRow) {

			if($saleRow['eventType'] != '2') {
				if($saleRow['salePrice']) {
					$_saleTotal = ($saleRow[total] >= $saleRow['salePrice']) ? $saleRow[total] : $saleRow['salePrice'];
					$_saleNum = ($saleRow[num] >= $saleRow['saleRow']) ? $saleRow[num] : $saleRow['num'];
					$_saleRow =  floor($saleRow[num] / $saleRow['saleRow']);
					$minus = $_saleTotal - ($saleRow['salePrice'] * ($_saleRow));
				} else {
					$_saleTotal = ($saleRow[total] >= $saleRow['salePrice']) ? $saleRow[total] : $saleRow['salePrice'];
					$_saleNum = ($saleRow[num] >= $saleRow['saleRow']) ? $saleRow[num] : $saleRow['num'];
					$_saleRow =  floor($saleRow[num] / $saleRow['saleRow']);
					$minus = $_saleTotal - round(($_saleTotal * $saleRow['saleDiscountnum']) / 100);
				}
			} else {
				$unFreightFlag = 1;
				$_SESSION['unFreightFlag'] = $unFreightFlag;
				//$minus = CalFreightDefault($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney);	//運費=小計-優惠代碼-紅利
				$minus = 0;
			}
			if($saleRow['eventType'] != '2') {	
		        $shopping_list .= "	
			        <table width=\"100%\" border=\"0\">
		                <tbody>
		                    <tr class=\"tr_bor\">
		                        <td class=\"td_left\" align=\"right\"></td>
		                        <td class=\"td_center\" align=\"right\"><div class=\"big\"><span>活動</span>".$saleRow['title']."</div></td>
		                        <td class=\"td_right\" align=\"center\">-$<span id=\"new_subtotal\">".number_format($minus)."</span></td>
		                    </tr>
		                </tbody>
		            </table>
	            ";
		    }

		}
		$minusTotal += $minus;
	}

	     

	//小計---------------------------------------------------------------------------------------------------------------
	$subtotal = CalSubtotal();	//小計
	
	$shopping_list .= "
		<table width=\"100%\" border=\"0\">
            <tbody>
                <tr class=\"tr_bor\">
                    <td class=\"td_left\" align=\"right\" >共<span class=\"total_color\">".count($_SESSION["MyShopping"])."</span>件 ".$_SESSION[MyMember][level_subject]."</td>
                    <td class=\"td_center\" align=\"right\">商品合計：</td>
                    <td class=\"td_right\" align=\"center\">$".number_format($subtotal)."</td>
                </tr>
            </tbody>
        </table>		
	";
	

	//員工不計運費及不可用優惠代碼----------------------------------------------------------------------------------------
	//if($_SESSION['MyMember']['level'] !== '4') {  
	//第二階段改為不限制
	if(1) {
		//優惠代碼---------------------------------------------------------------------------------------------------------------
		$code = CalCode();	//優惠代碼
		$mycoupon = $_SESSION["MyCode"];    
		if ($ifEditor) {
			if(!$mycoupon) {
				
				$shopping_list .= " 
					<tr>
			            <td colspan=\"2\" class=\"shop_td3 text-right\">可折抵優惠代碼
			            </td>
			            <td class=\"shop_td4 pad0 text-center\">
			                <input id=\"mycoupon\" type=\"text\" value=\"".$mycoupon."\" placeholder=\"輸入優惠代碼\">
			            </td>
			            <td class=\"shop_td4 text-center\">-$".number_format($code)."</td>
			            <td class=\"shop_td5 pad0 text-right\">
			                <div class=\"btn_use\">
			                    <input type=\"checkbox\" id=\"use01\" value=\"使用\" checked onClick=\"CheckCoupon();\">
			                    <label for=\"use01\">使用</label>
			                </div>
			            </td>
			        </tr>
			    ";
			} else {
				
				$shopping_list .= " 
					<tr>
			            <td colspan=\"2\" class=\"shop_td3 text-right\">可折抵優惠代碼
			            </td>
			            <td class=\"shop_td4 pad0 text-center\">
			                <input id=\"mycoupon\" type=\"text\" value=\"".$mycoupon."\" placeholder=\"輸入優惠代碼\">
			            </td>
			            <td class=\"shop_td4 text-center\">-$".number_format($code)."</td>
			            <td class=\"shop_td5 pad0 text-right\">
			                <div class=\"btn_use\">
			                    <input type=\"checkbox\" id=\"use01\" value=\"取消\" checked onClick=\"CancleCoupon();\">
			                    <label for=\"use01\">取消</label>
			                </div>
			            </td>
			        </tr>
			    ";
			}		  
		} else {
			//有輸入優惠代碼嗎
			if ($mycoupon)
			$shopping_list .= "
				<table width=\"100%\" border=\"0\">
		            <tbody>
		                <tr class=\"tr_bor\">
		                    <td class=\"td_left\" align=\"right\" ></td>
		                    <td class=\"td_center\" align=\"right\">優惠代碼（".$mycoupon."）</td>
		                    <td class=\"td_right\" align=\"center\">-$".number_format($code)."</td>
		                </tr>
		            </tbody>
		        </table>		
			";     
		}

		//紅利折扣---------------------------------------------------------------------------------------------------------------
		$memberShoppingMoney = $_SESSION["MyMemberShoppingMoney"];
		$memberCouponMoney = $_SESSION["MyCouponMoney"];	//優惠券要折扣金額
		$UseBonus = CalUseBonus($subtotal - $code - $memberShoppingMoney - $memberCouponMoney);	//是否符合紅利折扣資格
		$_bonus = $UseBonus[bonus];	//本次的紅利折扣金額
		$bonus = $_SESSION["MyBonus"];	//紅利要折扣金額
		if ($ifLogin && $_bonus) {	//有登入會員
			if ($ifEditor) {
				if(!$bonus) {
					$shopping_list .= "
					<tr>
                        <td colspan=\"2\" class=\"shop_td3  text-right\">可折抵紅利：$".number_format($_bonus, 0)."</td>
                        <td class=\"shop_td4 pad0 text-center\">
                            <input id=\"mybonus\" class=\"td_input\" type=\"text\" value=\"".$_bonus."\" placeholder=\"請輸入金額\">
                        </td>
                        <td class=\"shop_td4 text-center\">-$".number_format($bonus, 0)."</td>
                        <td class=\"shop_td5 pad0\">
                            <div class=\"btn_use\">
                                <input type=\"checkbox\" id=\"use02\" value=\"使用\" checked onClick=\"CheckBonus(".$_bonus.");\">
                                <label for=\"use02\">使用</label>
                            </div>
                        </td>
                    </tr>
					";
				} else {
					$shopping_list .= "
					<tr>
                        <td colspan=\"2\" class=\"shop_td3  text-right\">可折抵紅利：$".number_format($_bonus, 0)."</td>
                        <td class=\"shop_td4 pad0 text-center\">
                            <input id=\"mybonus\" class=\"td_input\" type=\"text\" value=\"".$_bonus."\" placeholder=\"請輸入金額\">
                        </td>
                        <td class=\"shop_td4 text-center\">-$".number_format($bonus, 0)."</td>
                        <td class=\"shop_td5 pad0\">
                            <div class=\"btn_use\">
                                <input type=\"checkbox\" id=\"use02\" value=\"取消\" checked onClick=\"CancleCheckBonus(".$_bonus.");\">
                                <label for=\"use02\">取消</label>
                            </div>
                        </td>
                    </tr>
					";
				}
			} else {
				
				$shopping_list .= "
					<table width=\"100%\" border=\"0\">
			            <tbody>
			                <tr class=\"tr_bor\">
			                    <td class=\"td_left\" align=\"right\" ></td>
			                    <td class=\"td_center\" align=\"right\">可折抵紅利：$".number_format($_bonus, 0)."</td>
			                    <td class=\"td_right\" align=\"center\">-$".number_format($bonus, 0)."</td>
			                </tr>
			            </tbody>
			        </table>		
				";
			}	 
		} else {
			unset($_SESSION["MyBonus"]);
		}

		//購物金---------------------------------------------------------------------------------------------------------------
		$memberCouponMoney = $_SESSION["MyCouponMoney"];	//優惠券要折扣金額
		$UseMemberCode = CalUseMemberCode($subtotal - $code - $bonus - $memberCouponMoney);	//是否符購物金	
		$_memberShoppingMoney = $UseMemberCode[money];	//本次的購物金折扣金額
		$memberShoppingMoney = $_SESSION["MyMemberShoppingMoney"];	//購物金要折扣金額
		$dateLimit = ($UseMemberCode[dateLimit]) ? "  使用期限  (".$UseMemberCode[dateLimit].")" : null;
		if ($ifLogin && $_memberShoppingMoney) {	//有登入會員
			if ($ifEditor) {
				if(!$memberShoppingMoney) {
					$shopping_list .= "
					<tr>
                        <td colspan=\"2\" class=\"shop_td3  text-right\">可折抵購物金</td>
                        <td class=\"shop_td4 pad0 text-center\">
                            <input id=\"mymembercode\" class=\"td_input\" type=\"text\" value=\"".$_memberShoppingMoney."\" placeholder=\"輸入要抵扣購物金額\">
                        </td>
                        <td class=\"shop_td4 text-center\">-$".number_format($memberShoppingMoney, 0)."</td>
                        <td class=\"shop_td5 pad0\">
                            <div class=\"btn_use\">
                                <input type=\"checkbox\" id=\"use03\" value=\"使用\" checked onClick=\"CheckMemberShoppingMoney(".$_memberShoppingMoney.");\">
                                <label for=\"use03\">使用</label>
                            </div>
                        </td>
                    </tr>
					";
				} else {
					$shopping_list .= "
					<tr>
                        <td colspan=\"2\" class=\"shop_td3  text-right\">可折抵購物金</td>
                        <td class=\"shop_td4 pad0 text-center\">
                            <input id=\"mymembercode\" class=\"td_input\" type=\"text\" value=\"".$_memberShoppingMoney."\" placeholder=\"輸入要抵扣購物金額\">
                        </td>
                        <td class=\"shop_td4 text-center\">-$".number_format($memberShoppingMoney, 0)."</td>
                        <td class=\"shop_td5 pad0\">
                            <div class=\"btn_use\">
                                <input type=\"checkbox\" id=\"use03\" value=\"取消\" checked onClick=\"CancleCheckMemberShoppingMoney(".$_memberShoppingMoney.");\">
                                <label for=\"use03\">取消</label>
                            </div>
                        </td>
                    </tr>
					";
				}
			} else {
			
				$shopping_list .= "
					<table width=\"100%\" border=\"0\">
			            <tbody>
			                <tr class=\"tr_bor\">
			                    <td class=\"td_left\" align=\"right\" ></td>
			                    <td class=\"td_center\" align=\"right\">可折抵購物金</td>
			                    <td class=\"td_right\" align=\"center\">-$".number_format($memberShoppingMoney, 0)."</td>
			                </tr>
			            </tbody>
			        </table>		
				";
			}		   
		} else {
			unset($_SESSION["MyMemberShoppingMoney"]);
		}


		//優惠券(只限一般會員) 
		//if($_SESSION['MyMember']['level'] === '1') {
		//第二階段改為不限制
		if(1) {
			$memberShoppingMoney = $_SESSION["MyMemberShoppingMoney"];	//購物金要折扣金額
			//優惠券---------------------------------------------------------------------------------------------------------------
			$UseCoupon = CalUseCoupon($subtotal - $code - $bonus - $memberShoppingMoney);	//是否符購物金
			//$_memberCouponMoney = $UseMemberCode[money];	//本次的購物金折扣金額
			$memberCouponMoney = $_SESSION["MyCouponMoney"];	//優惠券要折扣金額
			//$dateLimit = ($UseMemberCode[dateLimit]) ? "  使用期限  (".$UseMemberCode[dateLimit].")" : null;
			$_total = $subtotal - $code - $bonus;
			if ($ifLogin && count($UseCoupon)) {	//有登入會員
				$_web_x_coupon_id = ($_SESSION["MyCoupon"]) ? $_SESSION["MyCoupon"] : 0;
				$Coupon = getCoupon($_web_x_coupon_id);
				$web_x_coupon_list .= "<option value=\"\">優惠券</option>";
				$couponShow = false;
				foreach($UseCoupon as $coupon) {
					if($coupon[money] > $_total) {
						continue;
					}
					//可抵扣產品
					$coupon[bestsellers] = ($coupon[bestsellers]) ? explode(',', $coupon[bestsellers]) : '';
					$result = array_intersect($_SESSION[MyShoppingList], $coupon[bestsellers]);
					if(!count($result)) {
						continue;
					} else {
						$couponShow = true;
					}

					$web_x_coupon_list .= "<option value=\"".$coupon[web_x_coupon_id]."\"";
					if ($_SESSION["MyCoupon"] == $coupon[web_x_coupon_id]) {
						$web_x_coupon_list .= " selected";
						//$default_web_x_freight = $web_x_freight;
					}
					$web_x_coupon_list .= ">【".$coupon[subject]."】 抵扣金額: $".number_format($coupon[money])."</option>\n";
				}
				if($couponShow) {
					if ($ifEditor) {
						$shopping_list .= "
							<tr>
								<td colspan=\"4\" class=\"text-right form-xs-inline\">
									<select name=\"coupon_id\" onChange=\"ChangeCoupon(this);\">
										".$web_x_coupon_list."
									</select>
									<span class=\"hidden-xs\">優惠券：</span>
								</td>
								<td colspan=\"2\" class=\"text-center hidden-xs\">
									<span class=\"visible-xs pull-left\">優惠券：</span>
									<span class=\"point-text\">
										-$".number_format($Coupon[money], 0)."
									</span>
								</td>
							</tr>

							<tr>
								<td colspan=\"2\" class=\"text-center visible-xs\">
									<span class=\"visible-xs pull-left\">優惠券：</span>
									<span class=\"point-text\">
										-$".number_format($Coupon[money], 0)."
									</span>
								</td>
							</tr>
						";
					} else {

						$shopping_list .= "
							<table width=\"100%\" border=\"0\">
					            <tbody>
					                <tr class=\"tr_bor\">
					                    <td class=\"td_left\" align=\"right\" ></td>
					                    <td class=\"td_center\" align=\"right\">優惠券：</td>
					                    <td class=\"td_right\" align=\"center\">-$".number_format($Coupon[money], 0)."</td>
					                </tr>
					            </tbody>
					        </table>		
						";
					}	
				}     
			} else {
				unset($_SESSION["MyCouponMoney"]);
			}
  			

		}

	    $shopping_list .= "
			<table width=\"100%\" border=\"0\">
	            <tbody>
	                <tr class=\"tr_bor\">
	                    <td class=\"td_left\" align=\"right\" ></td>
	                    <td class=\"td_center\" align=\"right\">發票金額：</td>
	                    <td class=\"td_right\" align=\"center\">$".number_format($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney)."</td>
	                </tr>
	            </tbody>
	        </table>		
		";

		
		
		//運送地點---------------------------------------------------------------------------------------------------------------
		//先找運費預設值	
		$sql = "Select web_xx_freight_id, ifDefault as FreightDefault, subject as web_xx_freight From web_xx_freight Where ifDefault = '1' ";
		$rs = ConnectDB($DB, $sql);
		for ($i=0; $i<mysqli_num_rows($rs); $i++) {
			$row = mysqli_fetch_assoc($rs);
			foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
		}
		
		//有舉行什麼活動嗎
		$sql = "Select web_xx_freight_id, ifDefault as FreightDefault, subject as web_xx_freight From web_xx_freight Where ifDefault = '0' and sdate <= '".date("Y-m-d")."' and edate >= '".date("Y-m-d")."' order by sdate desc, edate desc, web_xx_freight_id desc ";
		$rs = ConnectDB($DB, $sql);
		for ($i=0; $i<mysqli_num_rows($rs); $i++) {
			$row = mysqli_fetch_assoc($rs);
			foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
		}

		//列出運送地點
		$web_x_freight_list = "";
		$sql = "Select web_x_freight_id, subject as web_x_freight From web_x_freight Where web_xx_freight_id = '".$web_xx_freight_id."' and ifShow = '1' order by asort asc, web_x_freight_id desc";
		$rs = ConnectDB($DB, $sql);
		for ($i=0; $i<mysqli_num_rows($rs); $i++) {
			$row = mysqli_fetch_assoc($rs);
			foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
			
			if (intval($_SESSION["MyFreight"])==0) $_SESSION["MyFreight"] = $web_x_freight_id;	//預設值
			/*
			$web_x_freight_list .= "<option value=\"".$web_x_freight_id."\"";
			if ($_SESSION["MyFreight"]==$web_x_freight_id) {
				$web_x_freight_list .= " selected";
				$default_web_x_freight = $web_x_freight;
			}
			$web_x_freight_list .= ">".$web_x_freight."</option>\n";
			*/
			$web_x_freight_list .= "<input class=\"_freight\" onChange=\"ChangeFreight(this);\" type=\"radio\" id=\"ti".$i."\" value=\"".$web_x_freight_id."\"";
			if ($_SESSION["MyFreight"]==$web_x_freight_id) {
				$web_x_freight_list .= " checked";
				$default_web_x_freight = $web_x_freight;
			}
			$web_x_freight_list .= ">\n<label for=\"ti".$i."\">".$web_x_freight."</label>\n";
		}
		$freight = CalFreight($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney);	//運費=小計-優惠代碼-紅利

		//如果有活動為免運費時
		if($_SESSION['unFreightFlag'] && $freight) {
			$minus = CalFreightDefault($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney);	//運費=小計-優惠代碼-紅利
			$freight = $freight - $minus;
			
		}

		$srange = CalFreightRange($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney);	//運費區間=小計-優惠代碼-紅利

		$loseFreight = ($srange > ($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney)) ? ($srange - ($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney)) : 0;
		
		if($_SESSION[bugFlag]) {
			$freight = 0;
		} else {
			if ($ifEditor) {
		        if($loseFreight) {
			        $shopping_list .="
				        <tr>
			                <td colspan=\"3\"> <span class=\"tooltip-tip\" style=\"display:none\"><i class=\"fa fa-times-circle _close\" aria-hidden=\"true\" style=\"cursor:pointer; float:right;padding-top:10px;\"></i><img src=\"images/tooltip.jpg\" alt=\"\"><h1>還差<span class=\"color\">$".number_format($loseFreight)."</span>就免運囉!</h1>
			                    <p>下方加購區人氣商品超好湊!</p>
			                    </span>
			                </td>
			                <td class=\"shop_td4\"></td>
			                <td class=\"shop_td5\">
			                </td>
			            </tr>
		            ";
		        }        
				$shopping_list .= "
				<tr class=\"bort0 sh_cr\">
	                <td colspan=\"3\" class=\"sh_tr\">
	                    <div class=\"radio\">
	                        ".$web_x_freight_list."
	                    </div>";
	            if($loseFreight) {
	                $shopping_list .= "    
	                    <span class=\"tooltip-container text-right\">
	                        <span class=\"tooltip-content\"><img src=\"images/i-con.png\" alt=\"\" class=\"_show\" style=\"cursor:pointer; vertical-align: top;padding-top: 5px;\">
	                        </span>
	                    </span>";
	            }
	            	$shopping_list .= "        
	                </td>
	                <td class=\"shop_td4  text-center\" style=\"vertical-align:bottom;line-height:2;\">$<span id=\"new_freight\">".number_format($freight)."</span></td>
	                <td class=\"shop_td5\">
	                </td>
	            </tr>
				";
				/*
				$shopping_list .= "  	
				<li class=\"text-right\">
					<select name=\"freight_id\" onChange=\"ChangeFreight(this);\">
						".$web_x_freight_list."
					</select>
				</li>
				<li>
					$<span id=\"new_freight\">".number_format($freight)."</span>
				</li>
				";
				*/  
			} else {
				
				$shopping_list .= "
					<table width=\"100%\" border=\"0\">
			            <tbody>
			                <tr class=\"tr_bor\">
			                    <td class=\"td_left\" align=\"right\">配送地區<span class=\"total_color\">".$default_web_x_freight."</span></td>
			                    <td class=\"td_center\" align=\"right\">運費：</td>
			                    <td class=\"td_right\" align=\"center\">$<span id=\"new_freight\">".number_format($freight)."</span></td>
			                </tr>
			            </tbody>
			        </table>		
				";   
			}
		}
	}

	if( $_SESSION['Init_Payment_CashOnDelivery2']) {
		$_Init_Payment_CashOnDelivery2_price = CalCashOnDelivery($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney + $freight);
		
		if($_SESSION[bugFlag]) {
			$_Init_Payment_CashOnDelivery2_price = 0;
		}
			
	    $shopping_list .= "
			<table width=\"100%\" border=\"0\">
	            <tbody>
	                <tr class=\"tr_bor\">
	                    <td class=\"td_left\" align=\"right\"></td>
	                    <td class=\"td_center\" align=\"right\">貨到付款：</td>
	                    <td class=\"td_right\" align=\"center\">$".number_format($_Init_Payment_CashOnDelivery2_price)."</td>
	                </tr>
	            </tbody>
	        </table>		
		";
	    //$_Init_Payment_CashOnDelivery2_price = $Init_Payment_CashOnDelivery2_price;
	}

	$shopping_list .= "
		<table width=\"100%\" border=\"0\">
            <tr class=\"tr_bor2\">
                <td class=\"td_left2\" align=\"left\">
                </td>
                <td class=\"td_center2\" align=\"right\">TOTAL應付金額：</td>
                <td class=\"td_right2\" align=\"center\"><span class=\"shop_td4 text-center2 total\">$".number_format($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney + $freight + $_Init_Payment_CashOnDelivery2_price)."</span></td>
            </tr>
            </tbody>
        </table>		
	";

	$shopping_list .= "
		<table class=\"table table-striped\">
            <tbody>
	";

	//總計---------------------------------------------------------------------------------------------------------------
	/*
	$shopping_list .= "  
		<li class=\"sum text-right\">總計</li>
			<li class=\"point\">$<span id=\"new_total\">".number_format($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney + $freight)."</span></li> 
	";
	$shopping_list .= "
            </div>
        </div>    
    ";
    */

    //贈品---------------------------------------------------------------------------------------------------------------
	if (CalGift($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney)!="") {//if不為空值顯示
		/*
		$shopping_list .= "  
			<div class=\"memo\">
				<span id=\"new_gift\">".CalGift($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney)."</span>
			</div>
			</hr>
		";
		*/
		$giftAry = CalGiftList($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney);
		$giftList = "
			<div style=\"display:none\" class=\"gift\">	
				<table>
					<tbody>
		";
			foreach($giftAry as $key => $giftRow) {
				$pic = ShowPic($giftRow[Covers], "uploadfiles/s/", "uploadfiles/no_image.jpg");
				$giftStock = ($giftRow[stock]) ? 1 : '贈品發送完畢';
				$giftList .= "
					<tr class=\"tab1b\">
				        <td>
				           <img src=\"".$pic."\" alt=\"".$giftRow[subject]."\" width=\"100px;\">
				        </td>
				        <td class=\"td_style\" style=\"text-align:left;\">
				        	<p class=\"pro_active\"><span>贈品：</span>".$giftRow[subject]."</p>
				        </td>
				        <td>
				            ".$giftStock."
				        </td>
				        <td>0</td>
				        <td>
							$<span>0</span>
						</td> 
				    </tr>    
				";
			}	
		$giftList .= "
					</tobdy>    
				</table>    
			</div>
		";
		$gifText = 	"<span id=\"new_gift\">".CalGift($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney)."</span>";

  	}
	
	//贈送紅利---------------------------------------------------------------------------------------------------------------
	$SendBonus = CalSendBonus($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney);	//紅利:本檔活動
	$web_x_bonus_hidden = $SendBonus[web_x_bonus_hidden];	//紅利流水號
	$web_x_bonus_list = $SendBonus[web_x_bonus_list];		//紅利描述
			
	if ($PageSelfURL!="shoppingsend.php") {
		//if (CalGift($web_x_bonus_list)!="") {//if不為空值顯示
		/*
		$shopping_list .= "  
			<div class=\"memo\">
				<span id=\"new_bonus\">".$web_x_bonus_list."</span>
			</div>
		";  
		//}
		*/
		$bonusText = "<span id=\"new_bonus\">".$web_x_bonus_list."</span>";
	}
	//說明---------------------------------------------------------------------------------------------------------------
	$sql = "Select Init_Freight2 From web_basicset Where web_basicset_id = '1' ";
	$rs = ConnectDB($DB, $sql);
	$row = mysqli_fetch_assoc($rs);
	$Init_Freight2 = $row['Init_Freight2']; 
	/*
 	$shopping_list .= "
 	</hr>
	<div class=\"memo2\">								
		".str_front($Init_Freight2)."
	</div>
	";
	*/ 


    $shopping_list .= "
	    <tr class=\"sh_cr2\">
		    <td class=\"shop_td\">
		        <!--<p class=\"text-left\">◆請確認您購買的商品款式、顏色、數量。確認後訂單無法進行任何更改喔!</p>
		        <p class=\"text-left\">◆商品售價將以您實際結帳之即時價格為主，商品數量、贈品與優惠等，也將以您實際結帳為準。</p>
		        <p class=\"text-left\">◆請儘早完成結帳以確保享有贈品與優惠。</p>-->
		        ".$giftList.$gifText.$bonusText.str_front($Init_Freight2)."
		    </td>
		    <td colspan=\"2\" class=\"text-right total_font\"></td>
		    <td class=\"shop_td4 text-center total\"></td>
		    <td class=\"shop_td5\">
		    </td>
		</tr>      
	";
	$_SESSION["MyShoppingSubtobal"] = $subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney;	

	$shopping_list .= "
                </tbody>
		    </table>                
		";
	 
	
	return $shopping_list;
}

//購物清單 shopping_car.php
function _ShoppingCarListConfirm($ifStop) {
	//現在在哪一頁
	$UrlArray = explode("/", htmlentities($_SERVER["PHP_SELF"]));
	$PageSelfURL = strtolower($UrlArray[sizeof($UrlArray)-1]);

	$ifEditor = ($ifStop==0) ? true : false;
	//$ifEditor = false;
	
	//是否已登入會員
	$ifLogin = MemberLogin();
	
	//最大訂購量
	$sql = "Select Init_Max From web_basicset Where web_basicset_id = '1' ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
	}
	
	//可編輯
	if ($ifEditor) {
		$thDelete = "<th class=\"t_align_c\" width=\"10%\">刪除</th>";
		$tdDelete = "<td>&nbsp;</td>";
	}
	
	//購物車
	$shopping_list = "
	  <table class=\"table_type_1 responsive_table full_width r_corners wraper shadow t_align_l t_xs_align_c m_bottom_30 \">
		<thead>
		  <tr class=\"f_size_large\"> 
			<th class=\"t_align_c\" width=\"25%\">產品縮圖</th>
			<th class=\"t_align_c\" width=\"35%\">品名</th>
			<th class=\"t_align_c\" width=\"10%\">單價</th>
			<th class=\"t_align_c\" width=\"10%\">數量</th>
			<th class=\"t_align_c\" width=\"10%\">小計</th>
			".$thDelete."
		  </tr>
		</thead>
		<tbody>";
	
	//清單---------------------------------------------------------------------------------------------------------------
	$total = 0;	//小計
	$MyShoppingListAry = array();
	for ($x=0; $x<count($_SESSION["MyShopping"]); $x++) {
		$web_product_id = $_SESSION["MyShopping"][$x]["web_product_id"];
		$dimension = $_SESSION["MyShopping"][$x]["dimension"];
		$num = $_SESSION["MyShopping"][$x]["num"];
		$additionalText = ($_SESSION["MyShopping"][$x]["additional"]) ? '<b style="color:#cc0066">【加購】</b>- ' : null;
		$additionalText = ($_SESSION["MyShopping"][$x]["fullAdditional"]) ? '<b style="color:#cc0066">【滿額加購】</b>- ' : $additionalText;
		$masterId = ($_SESSION["MyShopping"][$x]["masterId"]) ? $_SESSION["MyShopping"][$x]["masterId"] : null;
		$MyShoppingListAry[$x] = $web_product_id;
		$Product = getProduct($web_product_id);	//取得產品相關資訊
		$Product[link] = ($additionalText) ? null : $Product[link];    //為加購 不帶連結
		$saleTitle = null;
		//銷售組合
		if($Product[web_x_sale_id]) {
			$Sale = getSale($Product[web_x_sale_id]);
			//取是否為同一組合全部數量
			$SaleShoppingCarNum = SearchMultidimensionalArray($_SESSION["MyShopping"], 'saleId', $Product[web_x_sale_id]);
			if(count($Sale)) {
				if($num >= $Sale[rows] || $SaleShoppingCarNum >= $Sale[rows]) {
					if($Sale[price]) {

						$Product[price] = ceil($Sale[price] / $Sale[rows]);

					} else if($Sale[discountnum] < 100 && !$Sale[price]) { 	

						$Product[price] = round($Product[price] * $Sale[discountnum] / 100);

					}
					$saleTitle = $Sale['subject'];
				}
			}
		}

		$Product[price] = ($additionalText) ? $Product[additionalprice] : $Product[price];    //為加購 顯示加購價

		$pic = ShowPic($Product[Covers], "uploadfiles/s/", "uploadfiles/no_image.jpg");

		if (!$ifLogin) $Product[price_memberlevel_tip] = "";
		
		if ($ifEditor) {	//可編輯
			$num_list = "<select name=\"num_".$web_product_id."_".$dimension."\" onChange=\"AlterShoppingCar(this, 'price_".$web_product_id."_".$dimension."');\">";
			$Init_Max_Stock = ($Product['stock'] >= $Init_Max) ? $Init_Max : $Product['stock'];
			$Init_Max_Stock = ($additionalText) ? 1 : $Init_Max_Stock;    //為加購 只能購買一個數量

			for ($j=1; $j<=$Init_Max_Stock; $j++) {
				$num_list .= "<option value=\"".$j."\"";
				if ($num==$j) $num_list .= " selected";
				$num_list .= ">".sprintf("%02d", $j)."</option>\n";
			}
			$num_list .= "</select>";
		} else {
			$num_list = "<span class=\"fw_medium f_size_large color_dark\">".$num."</span>";
		}

		$num_list = "<span class=\"fw_medium f_size_large color_dark\">".$num."</span>";

		if (!$ifEditor) $Product[link] = "";
		
		if ($Product[ifGeneral]==9) {	//加購
			$Product[link] = "";
			$Product[subject] = "【加購】".$Product[subject];
		}
		
		$shopping_list .= "
		  <tr id=\"tr_".$web_product_id."_".$dimension."\" class=\"tr_".$masterId."_".$dimension."\">
			<td data-title=\"產品縮圖\" class=\"t_align_c\"><a".$Product[link]."><img src=\"".$pic."\" alt=\"".$Product[subject]."\" class=\"m_md_bottom_5 d_xs_block d_xs_centered\"></a></td>
			<td data-title=\"品名\">
			  <a".$Product[link]." class=\"d_inline_b color_dark\">".$additionalText.$Product[subject]."</a>";
		if($saleTitle) 
			$shopping_list .= "<br/><a href=\"sale_list.php?kind=".str_front($Product["web_x_sale_id"])."\" title=\"".str_front($Product[saleTitle])."\"><b style='color:red'>".$saleTitle."</b></a>";
		$shopping_list .= "	 	 
			 
			</td>

			<td data-title=\"單價\" class=\"t_align_c\"><span class=\"scheme_color fw_medium f_size_large\"><s>$".$Product[price_cost]."</s><br />$".number_format($Product[price])."</span></td>
			<td data-title=\"數量\" class=\"t_align_c\">".$num_list."</td>
			<td data-title=\"價格\" class=\"t_align_c\"><p class=\"f_size_large fw_medium scheme_color\">$<span id=\"price_".$web_product_id."_".$dimension."\">".number_format($Product[price]*$num)."</span></p></td>";

	if ($ifEditor && $_SESSION["MyShopping"][$x]["fullAdditional"]) $shopping_list .= "<td data-title=\"刪除\" class=\"t_align_c\"><a href=\"javascript:;\" onClick=\"DeleteShoppingCar(this, 'tr_".$web_product_id."_".$dimension."');\" class=\"color_dark\"><i class=\"fa fa-times f_size_medium m_right_5\"></i></a></td>";

		$shopping_list .= "</tr>";
	}
	$_SESSION["MyShoppingList"] = $MyShoppingListAry;
	//小計---------------------------------------------------------------------------------------------------------------
	$subtotal = CalSubtotal();	//小計
	$shopping_list .= "
	  <tr>
		<td colspan=\"4\"><p class=\"fw_medium f_size_large t_align_r t_xs_align_c\">小計：</p></td>
		<td colspan=\"1\" class=\"t_align_r\"><p class=\"fw_medium f_size_large color_dark\">$<span id=\"new_subtotal\">".number_format($subtotal)."</span></p></td>
		".$tdDelete."
	  </tr>";
	//員工不計運費及不可用優惠代碼----------------------------------------------------------------------------------------
	if($_SESSION['MyMember']['level'] !== '4') {  

		//優惠代碼---------------------------------------------------------------------------------------------------------------
		$code = CalCode();	//優惠代碼
		$mycoupon = $_SESSION["MyCode"];
		
		//if ($ifEditor) {
		if (0) {
			$shopping_list .= "
			  <tr>
				<td colspan=\"4\">
				  <p class=\"fw_medium f_size_large t_align_r t_xs_align_c\">
					<input id=\"mycoupon\" type=\"text\" value=\"".$mycoupon."\" placeholder=\"輸入優惠代碼\" class=\"r_corners f_size_medium\">
					<input type=\"button\" onClick=\"CheckCoupon();\" class=\"button_type_4 r_corners bg_light_color_2 m_left_5 mw_0 tr_all_hover color_dark\" value=\"使用\">
				  </p>
				</td>
				<td colspan=\"1\" class=\"t_align_r\"><p class=\"fw_medium f_size_large scheme_color\"><span id=\"new_coupon\">-$".number_format($code)."</span></p></td>
				".$tdDelete."
			  </tr>";
		} else {
			//有輸入優惠代碼嗎
			if ($mycoupon) $shopping_list .= "
			  <tr>
				<td colspan=\"4\">
				  <p class=\"fw_medium f_size_large t_align_r t_xs_align_c\">優惠代碼（".$mycoupon."）：</p>
				</td>
				<td colspan=\"1\" class=\"t_align_r\"><p class=\"fw_medium f_size_large scheme_color\"><span id=\"new_coupon\">-$".number_format($code)."</span></p></td>
				".$tdDelete."
			  </tr>";
		}
		
		//紅利折扣---------------------------------------------------------------------------------------------------------------
		$UseBonus = CalUseBonus($subtotal - $code);	//是否符合紅利折扣資格
		//$bonus = $UseBonus[bonus];	//本次的紅利折扣金額
		$bonus = $_SESSION["MyBonus"];	//本次的紅利折扣金額
		if ($ifLogin && $bonus) {	//有登入會員
			$shopping_list .= "
			  <tr>
				<td colspan=\"4\"><p class=\"fw_medium f_size_large t_align_r t_xs_align_c\">紅利折扣：</p></td>
				<td colspan=\"1\" class=\"t_align_r\"><p class=\"fw_medium f_size_large scheme_color\"><span id=\"new_use_bonus\">-$".number_format($bonus)."</span></p></td>
				".$tdDelete."
			  </tr>";
		}

		//購物金---------------------------------------------------------------------------------------------------------------
		$UseMemberCode = CalUseMemberCode($subtotal - $code - $bonus);	//是否符購物金		
		$_memberShoppingMoney = $UseMemberCode[money];	//本次的購物金折扣金額
		$memberShoppingMoney = $_SESSION["MyMemberShoppingMoney"];	//本次的購物金折扣金額
		$UseMemberCode[surplus] = abs(ceil($_memberShoppingMoney - $memberShoppingMoney));
		$dateLimit = ($UseMemberCode[dateLimit]) ? "  使用期限  (".$UseMemberCode[dateLimit].")" : null;
		if ($ifLogin && $memberShoppingMoney) {	//有登入會員
			$shopping_list .= "
			  <tr>
				<td colspan=\"4\"><p class=\"fw_medium f_size_large t_align_r t_xs_align_c\">".$dateLimit."  已扣除購物金：</p></td>
				<td colspan=\"1\" class=\"t_align_r\"><p class=\"fw_medium f_size_large scheme_color\"><span id=\"new_use_bonus\">-$".number_format($memberShoppingMoney)."</span></p></td>
				".$tdDelete."
			  </tr>";
		}
	
		//運送地點---------------------------------------------------------------------------------------------------------------
		//先找運費預設值	
		$sql = "Select web_xx_freight_id, ifDefault as FreightDefault, subject as web_xx_freight From web_xx_freight Where ifDefault = '1' ";
		$rs = ConnectDB($DB, $sql);
		for ($i=0; $i<mysqli_num_rows($rs); $i++) {
			$row = mysqli_fetch_assoc($rs);
			foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
		}
		
		//有舉行什麼活動嗎
		$sql = "Select web_xx_freight_id, ifDefault as FreightDefault, subject as web_xx_freight From web_xx_freight Where ifDefault = '0' and sdate <= '".date("Y-m-d")."' and edate >= '".date("Y-m-d")."' order by sdate desc, edate desc, web_xx_freight_id desc ";
		$rs = ConnectDB($DB, $sql);
		for ($i=0; $i<mysqli_num_rows($rs); $i++) {
			$row = mysqli_fetch_assoc($rs);
			foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
		}

		//列出運送地點
		$web_x_freight_list = "";
		$sql = "Select web_x_freight_id, subject as web_x_freight From web_x_freight Where web_xx_freight_id = '".$web_xx_freight_id."' and ifShow = '1' order by asort asc, web_x_freight_id desc";
		$rs = ConnectDB($DB, $sql);
		for ($i=0; $i<mysqli_num_rows($rs); $i++) {
			$row = mysqli_fetch_assoc($rs);
			foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
			
			if (intval($_SESSION["MyFreight"])==0) $_SESSION["MyFreight"] = $web_x_freight_id;	//預設值

			$web_x_freight_list .= "<option value=\"".$web_x_freight_id."\"";
			if ($_SESSION["MyFreight"]==$web_x_freight_id) {
				$web_x_freight_list .= " selected";
				$default_web_x_freight = $web_x_freight;
			}
			$web_x_freight_list .= ">".$web_x_freight."</option>\n";
		}
		$freight = CalFreight($subtotal - $code - $bonus - $memberShoppingMoney);	//運費=小計-優惠代碼-紅利
	
		//if ($ifEditor) {
		if (0) {	
			$shopping_list .= "
			  <tr>
				<td colspan=\"4\"><p class=\"fw_medium f_size_large t_align_r t_xs_align_c\">運送地點
				<select name=\"freight_id\" onChange=\"ChangeFreight(this);\">
				  ".$web_x_freight_list."
				</select> 運費：</p></td>
				<td colspan=\"1\" class=\"t_align_r\"><p class=\"fw_medium f_size_large color_dark\">$<span id=\"new_freight\">".number_format($freight)."</span></p></td>
				".$tdDelete."
			  </tr>";
		} else {
			$shopping_list .= "
			  <tr>
				<td colspan=\"4\"><p class=\"fw_medium f_size_large t_align_r t_xs_align_c\">運送地點：".$default_web_x_freight."　運費：</p></td>
				<td colspan=\"1\" class=\"t_align_r\"><p class=\"fw_medium f_size_large color_dark\">$<span id=\"new_freight\">".number_format($freight)."</span></p></td>
				".$tdDelete."
			  </tr>";
		}
	}
	//總計---------------------------------------------------------------------------------------------------------------
	$shopping_list .= "
	  <tr>
		<td colspan=\"4\"><p class=\"fw_medium f_size_large scheme_color t_align_r t_xs_align_c\">總計：</p></td>
		<td colspan=\"1\" class=\"t_align_r\"><p class=\"fw_medium f_size_large scheme_color m_xs_bottom_10\">$<span id=\"new_total\">".number_format($subtotal - $code - $bonus - $memberShoppingMoney + $freight)."</span></p></td>
		".$tdDelete."
	  </tr>";
	$_SESSION["MyShoppingSubtobal"] = $subtotal - $code - $bonus - $memberShoppingMoney;
	//贈品---------------------------------------------------------------------------------------------------------------
	if (CalGift($subtotal - $code - $bonus - $memberShoppingMoney)!="") {//if不為空值顯示
	$shopping_list .= "
	  <tr>
		<td colspan=\"5\" class=\"t_align_l\"><p class=\"fw_medium f_size_large scheme_color t_align_l t_xs_align_l\"><span id=\"new_gift\">".CalGift($subtotal - $code - $bonus - $memberShoppingMoney)."</span></p></td>
		".$tdDelete."
	  </tr>";
	  }
	
	//贈送紅利---------------------------------------------------------------------------------------------------------------
	$SendBonus = CalSendBonus($subtotal - $code - $bonus - $memberShoppingMoney);	//紅利:本檔活動
	$web_x_bonus_hidden = $SendBonus[web_x_bonus_hidden];	//紅利流水號
	$web_x_bonus_list = $SendBonus[web_x_bonus_list];		//紅利描述
			
	if ($PageSelfURL!="shopping_send.php") {
		//if (CalGift($web_x_bonus_list)!="") {//if不為空值顯示
		$shopping_list .= "
	  <tr>
		<td colspan=\"5\" class=\"t_align_r\"><span id=\"new_bonus\">".$web_x_bonus_list."</span></td>
		".$tdDelete."
	  </tr>";
		//}
	}
	//說明---------------------------------------------------------------------------------------------------------------
	$sql = "Select Init_Freight2 From web_basicset Where web_basicset_id = '1' ";
	$rs = ConnectDB($DB, $sql);
	$row = mysqli_fetch_assoc($rs);
	$Init_Freight2 = $row['Init_Freight2']; 
	$shopping_list .= "
	  <tr>
		<td colspan=\"6\" class=\"t_align_l memo\">
		<div class=\"alert_box r_corners warning m_bottom_10\">
			<div class=\"row\">
			".str_front($Init_Freight2)."
			</div>
		</div>
		</td>
	 </tr>";
	 
	$shopping_list .= "
            </tbody>
          </table>";
    //$shopping_list .=  ShoppingCarListPhone();     
	
	return $shopping_list;
}

//購物清單 shopping_car.php
function ShoppingCarListPhone($ifStop) {
	//現在在哪一頁
	$UrlArray = explode("/", htmlentities($_SERVER["PHP_SELF"]));
	$PageSelfURL = strtolower($UrlArray[sizeof($UrlArray)-1]);

	$ifEditor = ($ifStop==0) ? true : false;
	//$ifEditor = false;
	
	//是否已登入會員
	$ifLogin = MemberLogin();

	
	//最大訂購量
	$sql = "Select Init_Max From web_basicset Where web_basicset_id = '1' ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
	}
		
	
	//可編輯
	if ($ifEditor) {
		$thDelete = "<th>刪除</th>";
	} else {	
		//$tdDelete = "<th>&nbsp;</th>";
	}		

	//購物車		
	$shopping_list ="
		<table class=\"table table_moile car\">
            <tbody>
	";		 
	
	//清單---------------------------------------------------------------------------------------------------------------
	$total = 0;	//小計
	$MyShoppingListAry = array();
	$groupTag = 0;
	$groupTag2 = 0;
	$_j = 0;
	$saleAllNum = getSaleAllNum();
	$surplusNum = 0;
	$unFreightFlag = 0;
	$_SESSION['unFreightFlag'] = $unFreightFlag;
	for ($x=0; $x<count($_SESSION["MyShopping"]); $x++) {
		$web_product_id = $_SESSION["MyShopping"][$x]["web_product_id"];
		$dimension = $_SESSION["MyShopping"][$x]["dimension"];
		$num = $_SESSION["MyShopping"][$x]["num"];
		$additionalText = ($_SESSION["MyShopping"][$x]["additional"]) ? '<b style="color:#cc0066">【加購】</b>- ' : null;
		$additionalText = ($_SESSION["MyShopping"][$x]["fullAdditional"]) ? '<b style="color:#cc0066">【滿額加購】</b>- ' : $additionalText;
		$masterId = ($_SESSION["MyShopping"][$x]["masterId"]) ? $_SESSION["MyShopping"][$x]["masterId"] : null;
		$MyShoppingListAry[$x] = $web_product_id;
		$Product = getProduct($web_product_id);	//取得產品相關資訊
		$Categories = getCategories($Product[web_x_product_id]);
		$_Categories = _getCategories($Categories[web_xxx_product_id]);
		/*
		$IPA =  getenv('REMOTE_ADDR');
		$ipList = array(
			'118.170.45.55',
		);

		if (in_array($IPA, $ipList)){
			echo "<pre>";
			print_r($_Categories);
			echo "</pre>";
		}
		*/
		$Product[link] = ($additionalText) ? null : $Product[link];    //為加購 不帶連結
		if($_SESSION["MyShopping"][$x]["additional"]) {
			continue;
		}
		$saleTitle = null;
		$showFlag = 0;
		//$saleAry = array();
		//銷售組合
		if($Product[web_x_sale_id] && !$_SESSION["MyShopping"][$x]["additional"] && !$_SESSION["MyShopping"][$x]["fullAdditional"]) {
			$Sale = getSale($Product[web_x_sale_id]);
			//echo $Product[web_x_sale_id]."</br>";
			//取是否為同一組合全部數量
			$SaleShoppingCarNum = SearchMultidimensionalArray_2($_SESSION["MyShopping"], 'saleId', $Product[web_x_sale_id]);
			if(count($Sale)) {
				if($Sale[eventType] != '2') {	//是否為免運
					//if($num % $Sale[rows] == 0 || $SaleShoppingCarNum % $Sale[rows] == 0) {
					if($num % $Sale[rows] == 0 || $SaleShoppingCarNum >= $Sale[rows]) {	
						if($Sale[price]) {

							//$Product[price] = floor($Sale[price] / $Sale[rows]);
							$saleTitle = $Sale['subject'];	
							$saleTotal[$Product[web_x_sale_id]] += ($Product[price] * $num);
							$saleNum[$Product[web_x_sale_id]] += $num;
							if($saleNum[$Product[web_x_sale_id]] >= $Sale[rows]) {
								$_minusRow = $saleNum[$Product[web_x_sale_id]] % $Sale[rows];
								$_minus = ($Product[price] * $_minusRow);
							} 

							$saleAry[$Product[web_x_sale_id]] = array(
								'id'			=>	$Product[web_x_sale_id],
								'saleRow'		=>	$Sale[rows],
								'salePrice'		=>	$Sale[price],
								'title'			=>	$saleTitle,
								'num'			=> 	$saleNum[$Product[web_x_sale_id]],
								'total'			=>	$saleTotal[$Product[web_x_sale_id]] - $_minus,

							);

						} else if($Sale[discountnum] < 100 && !$Sale[price]) { 	

							//echo $num % $Sale[rows]; 	
							//echo '-';
							//$Product[price] = (($Sale[rows] % $num == 0) && ($num >= $Sale[rows])) ? $Product[price_cost] : $Product[price];
							//$showFlag = (($Sale[rows] % $num == 0) && ($num >= $Sale[rows])) ? 1 : 0;
							//echo $SaleShoppingCarNum % $Sale[rows].'=='.$num % $Sale[rows]."</br>";
							//echo '_';
							//if($SaleShoppingCarNum % $Sale[rows] == 1 && $num % $Sale[rows] == 2) {
							//if($SaleShoppingCarNum % $Sale[rows] != 0) {
							if(1) {	
								
								if($saleAllNum[$Product[web_x_sale_id]]) {
									//echo $Sale[rows].",";
									$numAry2[$Product[web_x_sale_id]][$web_product_id] = $num;
									$SaleRows[$Product[web_x_sale_id]] = $Sale[rows];
									$numAry[] = $num;
									$saleAllNum[$Product[web_x_sale_id]] = ($saleAllNum[$Product[web_x_sale_id]] >= $SaleRows[$Product[web_x_sale_id]]) ? $saleAllNum[$Product[web_x_sale_id]] - ($saleAllNum[$Product[web_x_sale_id]] % $SaleRows[$Product[web_x_sale_id]]) : $saleAllNum[$Product[web_x_sale_id]];
									
									if(array_sum($numAry2[$Product[web_x_sale_id]]) <= $saleAllNum[$Product[web_x_sale_id]]) {
										$lastNum[$Product[web_x_sale_id]] = array_sum($numAry2[$Product[web_x_sale_id]]);
										$Product[slaePriceTotal] = ($Product[price_cost] * ($numAry2[$Product[web_x_sale_id]][$web_product_id]));
										$Product[truePriceTotal] = 0;
									} else {
										$canDoNum[$Product[web_x_sale_id]] = ($saleAllNum[$Product[web_x_sale_id]] - $lastNum[$Product[web_x_sale_id]]);
										$lastNum[$Product[web_x_sale_id]] += $canDoNum[$Product[web_x_sale_id]];
										if($saleAllNum[$Product[web_x_sale_id]] >= $lastNum[$Product[web_x_sale_id]]) {
											$Product[slaePriceTotal] = ($Product[price_cost] * ($canDoNum[$Product[web_x_sale_id]]));
											$Product[truePriceTotal] = ($Product[price] * ($numAry2[$Product[web_x_sale_id]][$web_product_id] - $canDoNum[$Product[web_x_sale_id]]));
										} else {
											$Product[slaePriceTotal] = 0;
											$Product[truePriceTotal] = ($Product[price] * ($numAry2[$Product[web_x_sale_id]][$web_product_id]));
										}	
									}
									
								}
								
	/*
								$num2 = ($SaleShoppingCarNum % $Sale[rows] == 0) ? $num : $num - ($num % $Sale[rows]);
								$num3 = ($num2) ? $num2 : 1;
								
								$Product[slaePriceTotal] = ($num2) ? ($Product[price_cost] * ($num3)) : 0;
								$Product[truePriceTotal] = ($num2) ? ($Product[price] * ($num - $num3)) : $Product[price] * $num;
	*/							
								$Product[allPriceTotal] = $Product[slaePriceTotal] + $Product[truePriceTotal];
								
							}	
							$Product[price] = $Product[price_cost];
							$showFlag = 1;

							//$Product[price] = round($Product[price] * $Sale[discountnum] / 100);
							$saleTitle = $Sale['subject'];	
							//$saleTotal[$Product[web_x_sale_id]] += ($Product[price] * $num);
							//$saleTotal[$Product[web_x_sale_id]] += $Product[allPriceTotal];
							$saleTotal[$Product[web_x_sale_id]] += $Product[slaePriceTotal];
							$saleNum[$Product[web_x_sale_id]] += $num;
							if($saleNum[$Product[web_x_sale_id]] >= $Sale[rows]) {
								$_minusRow = $saleNum[$Product[web_x_sale_id]] % $Sale[rows];
								$_minus = ($Product[price] * $_minusRow);
							} 

							$saleAry[$Product[web_x_sale_id]] = array(
								'id'				=>	$Product[web_x_sale_id],
								'saleRow'			=>	$Sale[rows],
								'saleDiscountnum'	=>	$Sale[discountnum],
								'title'				=>	$saleTitle,
								'num'				=> 	$saleNum[$Product[web_x_sale_id]],
								//'total'				=>	$saleTotal[$Product[web_x_sale_id]] - $_minus,
								'total'				=>	$saleTotal[$Product[web_x_sale_id]],

							);
						}

					}
				} else {
					$showFlag = 0;
					$saleAry[$Product[web_x_sale_id]] = array(
						'id'			=>	$Product[web_x_sale_id],
						'saleRow'		=>	$Sale[rows],
						'salePrice'		=>	$Sale[price],
						'title'			=>	$Sale['subject'],
						'eventType'		=>  '2'	
					);
					//2017-06-14 改為一般價(取消原價)
					//$Product[price] = $Product[price_cost];  //用原價
				}	
				$saleTitle = $Sale['subject'];
			}
		}

		$Product[price] = ($additionalText) ? $Product[additionalprice] : $Product[price];    //為加購 顯示加購價

		$pic = ShowPic($Product[Covers], "uploadfiles/s/", "uploadfiles/no_image.jpg");

		if (!$ifLogin) $Product[price_memberlevel_tip] = "";
		
		if ($ifEditor) {	//可編輯
			$num_list = "<select name=\"num_".$web_product_id."_".$dimension."\" onChange=\"AlterShoppingCar(this, 'Mprice_".$web_product_id."_".$dimension."');\">";
			$Init_Max_Stock = ($Product['stock'] >= $Init_Max) ? $Init_Max : $Product['stock'];
			$Init_Max_Stock = ($additionalText) ? 1 : $Init_Max_Stock;    //為加購 只能購買一個數量

			for ($j=1; $j<=$Init_Max_Stock; $j++) {
				$num_list .= "<option value=\"".$j."\"";
				if ($num==$j) $num_list .= " selected";
				$num_list .= ">".sprintf("%02d", $j)."</option>\n";
			}
			$num_list .= "</select>";
		} else {
			$num_list = "<span class=\"fw_medium f_size_large color_dark\"> x ".$num."</span>";
		}
		
		if (!$ifEditor) $Product[link] = "";
		
		if ($Product[ifGeneral]==9) {	//加購
			$Product[link] = "";
			$Product[subject] = "【加購】".$Product[subject];
		}

		$shopping_list .= "	
			<tr class=\"tab1b Mtr_".$masterId."_".$dimension."\" id=\"Mtr_".$web_product_id."_".$dimension."\">
	            <td>
	                <div class=\"col-sm-3 col-xs-3 padd0\">
	                    <a".$Product[link]." title=\"".$Product[subject]."\">
	                        <img src=\"".$pic."\" alt=\"".$Product[subject]."\" width=\"100px;\">
	                    </a>
	                </div>
	                <div class=\"col-sm-9 col-xs-9 padd0\">
	                    <div class=\"ol-sm-9 col-xs-9 text-left\">
	                        <a".$Product[link]." title=\"".$Product[subject]."\">".$Product[subject]."</a>
	                    </div>
	                    <div class=\"col-sm-3 col-xs-3 padd0 text-right\">$".number_format($Product[price])."</div>";
	            if($saleTitle)
		        	$shopping_list .= "    
	                    <div class=\"col-sm-12 col-xs-12 sale_name text-left p_r_0\">
	                        <p class=\"pro_active text-left\" style=\"line-height:1.7;\"><span>活動</span>".$saleTitle."</p>
	                    </div>";
					$shopping_list .= "
	                    <div class=\"col-sm-12 col-xs-12 padd0\">
	                        <div class=\"col-sm-5 col-xs-4 p_r_0 text-left\">
	                            ".$num_list."
	                        </div>
	                        <div class=\"col-sm-3 col-xs-3 padd0 text-center\">
	                            <div style=\"line-height: 2\">小計：</div>
	                        </div>
	                        <div class=\"col-sm-3 col-xs-4 text-center\" style=\"padding:0;\">
	                            <div style=\"line-height:2; padding:0;\">";
							if(!$showFlag)
								$shopping_list .= "
									$<span id=\"Mprice_".$web_product_id."_".$dimension."\">".number_format($Product[price]*$num)."</span>";
							else
								$shopping_list .= "
									$<span id=\"Mprice_".$web_product_id."_".$dimension."\">".number_format($Product[allPriceTotal])."</span>";
	                            $shopping_list .= "
	                            </div>
	                        </div>
	                        <div class=\"col-sm-1 col-xs-1 text-center padd0\">";
	                    if ($ifEditor) {
							$shopping_list .= "
	                        	<a href=\"javascript:;\" onclick=\"DeleteShoppingCar(this, 'Mtr_".$web_product_id."_".$dimension."');\"><i class=\"fa fa-trash\" aria-hidden=\"true\"></i></a>";
	                    }
	                    $shopping_list .= "
	                        </div>
	                    </div>
	                </div>
	            </td>
	        </tr>
		";
	}

	//加購區
	for ($x=0; $x<count($_SESSION["MyShopping"]); $x++) {
		$web_product_id = $_SESSION["MyShopping"][$x]["web_product_id"];
		$dimension = $_SESSION["MyShopping"][$x]["dimension"];
		$num = $_SESSION["MyShopping"][$x]["num"];
		$additionalText = ($_SESSION["MyShopping"][$x]["additional"]) ? '<b style="color:#cc0066">【加購】</b>- ' : null;
		$additionalText = ($_SESSION["MyShopping"][$x]["fullAdditional"]) ? '<b style="color:#cc0066">【滿額加購】</b>- ' : $additionalText;
		$masterId = ($_SESSION["MyShopping"][$x]["masterId"]) ? $_SESSION["MyShopping"][$x]["masterId"] : null;
		$MyShoppingListAry[$x] = $web_product_id;
		$Product = getProduct($web_product_id);	//取得產品相關資訊
		$Product[link] = ($additionalText) ? null : $Product[link];    //為加購 不帶連結
		if(!$_SESSION["MyShopping"][$x]["additional"]) {
			continue;
		}
		$saleTitle = null;
		$showFlag = 0;
		$Product[price] = ($additionalText) ? $Product[additionalprice] : $Product[price];    //為加購 顯示加購價

		$pic = ShowPic($Product[Covers], "uploadfiles/s/", "uploadfiles/no_image.jpg");

		if (!$ifLogin) $Product[price_memberlevel_tip] = "";
		
		if ($ifEditor) {	//可編輯
			$num_list = "<select name=\"num_".$web_product_id."_".$dimension."\" onChange=\"AlterShoppingCar(this, 'Mprice_".$web_product_id."_".$dimension."');\">";
			$Init_Max_Stock = ($Product['stock'] >= $Init_Max) ? $Init_Max : $Product['stock'];
			$Init_Max_Stock = ($additionalText) ? 1 : $Init_Max_Stock;    //為加購 只能購買一個數量

			for ($j=1; $j<=$Init_Max_Stock; $j++) {
				$num_list .= "<option value=\"".$j."\"";
				if ($num==$j) $num_list .= " selected";
				$num_list .= ">".sprintf("%02d", $j)."</option>\n";
			}
			$num_list .= "</select>";
		} else {
			$num_list = "<span class=\"fw_medium f_size_large color_dark\"> x ".$num."</span>";
		}
		
		if (!$ifEditor) $Product[link] = "";
		
		if ($Product[ifGeneral]==9) {	//加購
			$Product[link] = "";
			$Product[subject] = "【加購】".$Product[subject];
		}
		/*
		$shopping_list .= "		
		<div class=\"tab1b tr_".$masterId."_".$dimension."\" id=\"tr_".$web_product_id."_".$dimension."\">							
			<li><a".$Product[link]."><img src=\"".$pic."\" alt=\"".$Product[subject]."\"></a></li>
			<li><a".$Product[link]." class=\"d_inline_b color_dark\">".$additionalText.$Product[subject]."</a>";
		if($saleTitle)
			$shopping_list .= "<br/><a href=\"sale_list.php?kind=".str_front($Product["web_x_sale_id"])."\" title=\"".str_front($Product[saleTitle])."\"><b style='color:red'>".$saleTitle."</b></a>";
		$shopping_list .= "
			</li>
			<li>$".number_format($Product[price])."</li>
			<li>".$num_list."</li>
			<li class=\"tab1t mobile\">小計：</li>
			<li class=\"point\">$<span id=\"price_".$web_product_id."_".$dimension."\">".number_format($Product[price]*$num)."</span></li>	
		";
	
	
		if ($ifEditor) {
			$shopping_list .= "<li><a href=\"javascript:;\" onClick=\"DeleteShoppingCar(this, 'tr_".$web_product_id."_".$dimension."');\" class=\"color_dark\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i></a></li>";
		} else {
			$shopping_list .= "<li>&nbsp;</li>";
		}
		$shopping_list .= "</div>";
		*/
		$shopping_list .= "	
			<tr class=\"tab1b Mtr_".$masterId."_".$dimension."\" id=\"Mtr_".$web_product_id."_".$dimension."\">
	            <td>
	                <div class=\"col-sm-3 col-xs-3 padd0\">
	                    <a".$Product[link]." title=\"".$Product[subject]."\">
	                        <img src=\"".$pic."\" alt=\"".$Product[subject]."\" width=\"100px;\">
	                    </a>
	                </div>
	                <div class=\"col-sm-9 col-xs-9 padd0\">
	                    <div class=\"ol-sm-9 col-xs-9 text-left\">
	                        <a".$Product[link]." title=\"".$Product[subject]."\">".$additionalText.$Product[subject]."</a>
	                    </div>
	                    <div class=\"col-sm-3 col-xs-3 padd0 text-right\">$".number_format($Product[price])."</div>";
	            if($saleTitle)
		        	$shopping_list .= "    
	                    <div class=\"col-sm-12 col-xs-12 sale_name text-left p_r_0\">
	                        <p class=\"pro_active text-left\" style=\"line-height:1.7;\"><span>活動</span>".$saleTitle."</p>
	                    </div>";
					$shopping_list .= "
	                    <div class=\"col-sm-12 col-xs-12 padd0\">
	                        <div class=\"col-sm-5 col-xs-4 p_r_0 text-left\">
	                            ".$num_list."
	                        </div>
	                        <div class=\"col-sm-3 col-xs-3 padd0 text-center\">
	                            <div style=\"line-height: 2\">小計：</div>
	                        </div>
	                        <div class=\"col-sm-3 col-xs-4 text-center\" style=\"padding:0;\">
	                            <div style=\"line-height:2; padding:0;\">";
							if(!$showFlag)
								$shopping_list .= "
									$<span id=\"Mprice_".$web_product_id."_".$dimension."\">".number_format($Product[price]*$num)."</span>";
							else
								$shopping_list .= "
									$<span id=\"Mprice_".$web_product_id."_".$dimension."\">".number_format($Product[allPriceTotal])."</span>";
	                            $shopping_list .= "
	                            </div>
	                        </div>
	                        <div class=\"col-sm-1 col-xs-1 text-center padd0\">";
	                    if ($ifEditor) {
							$shopping_list .= "
	                        	<a href=\"javascript:;\" onclick=\"DeleteShoppingCar(this, 'Mtr_".$web_product_id."_".$dimension."_".$_SESSION["MyShopping"][$x]["fullAdditional"]."');\"><i class=\"fa fa-trash\" aria-hidden=\"true\"></i></a>";
	                    }
	                    $shopping_list .= "
	                        </div>
	                    </div>
	                </div>
	            </td>
	        </tr>
		";
	}
                   
	$_SESSION["MyShoppingList"] = $MyShoppingListAry;

	if(($saleAry)) {
		foreach($saleAry as $saleRow) {

			if($saleRow['eventType'] != '2') {
				if($saleRow['salePrice']) {
					$_saleTotal = ($saleRow[total] >= $saleRow['salePrice']) ? $saleRow[total] : $saleRow['salePrice'];
					$_saleNum = ($saleRow[num] >= $saleRow['saleRow']) ? $saleRow[num] : $saleRow['num'];
					$_saleRow =  floor($saleRow[num] / $saleRow['saleRow']);
					$minus = $_saleTotal - ($saleRow['salePrice'] * ($_saleRow));
				} else {
					$_saleTotal = ($saleRow[total] >= $saleRow['salePrice']) ? $saleRow[total] : $saleRow['salePrice'];
					$_saleNum = ($saleRow[num] >= $saleRow['saleRow']) ? $saleRow[num] : $saleRow['num'];
					$_saleRow =  floor($saleRow[num] / $saleRow['saleRow']);
					$minus = $_saleTotal - round(($_saleTotal * $saleRow['saleDiscountnum']) / 100);
				}
			} else {
				$unFreightFlag = 1;
				$_SESSION['unFreightFlag'] = $unFreightFlag;
				//$minus = CalFreightDefault($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney);	//運費=小計-優惠代碼-紅利
				$minus = 0;
			}
			if($saleRow['eventType'] != '2') {
		$shopping_list .= "
			<tr>
	            <td>
	            	<div class=\"col-lg-9 col-md-9 col-sm-9 col-xs-9 padd0 text-right\"> 活動".$saleRow['title']."：</div>
	                <div class=\"col-lg-3 col-md-3 col-sm-3 col-xs-3 padd0  text-right\">-$<span id=\"new_subtotal\">".number_format($minus)."</span></div>
	            </td>
	        </tr>
	    ";
	    	}    	
		/*	
			$shopping_list .= "	
				<tr>
	                <td class=\"shop_td\">
	                    <div class=\"big\"><span>活動</span>".$saleRow['title']."</div>
	                </td>
	                <td class=\"shop_td2\"></td>
	                <td class=\"shop_td3\"></td>
	                <td class=\"shop_td4 text-center\">-$<span id=\"new_subtotal\">".number_format($minus)."</span></td>
	                <td class=\"shop_td5\">
	                </td>
	            </tr>
	        "; 
	    */       
		}
		$minusTotal += $minus;
	}



	//小計---------------------------------------------------------------------------------------------------------------
	$subtotal = CalSubtotal();	//小計
	$shopping_list .= "		
		<tr>
	        <td>
	        	<div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-4 padd0 text-right\">共<span class=\"total_color\">".count($_SESSION["MyShopping"])."</span>件 ".$_SESSION[MyMember][level_subject]."</div>
	        	<div class=\"col-lg-5 col-md-5 col-sm-5 col-xs-5 padd0 text-right\">商品合計：</div>
	            <div class=\"col-lg-3 col-md-3 col-sm-3 col-xs-3 padd0 text-right\">$".number_format($subtotal)."</div>
	        </td>
	    </tr>
	";    
										
	//員工不計運費及不可用優惠代碼----------------------------------------------------------------------------------------
	//if($_SESSION['MyMember']['level'] !== '4') { 
	//第二階段改為不限制
	if(1) { 

		//優惠代碼---------------------------------------------------------------------------------------------------------------
		$code = CalCode();	//優惠代碼
		$mycoupon = $_SESSION["MyCode"];    
		if ($ifEditor) {
			if(!$mycoupon) {
				$shopping_list .= " 
					<tr>
	                    <td>
	                        <table width=\"100%\">
	                            <tr>
	                                <td class=\"text-right\" width=\"75%\">
	                                    <input id=\"mycoupon\" type=\"text\" value=\"".$mycoupon."\" placeholder=\"輸入優惠代碼\" style=\"width:67%;margin-bottom:5px;\">
	                                    <button class=\"buylist_btn\" onClick=\"CheckCoupon();\">使用</button>
	                                </td>
	                                <td class=\"text-right\" width=\"25%\">-$".number_format($code)."</td>
	                            </tr>
	                        </table>
	                    </td>
	                </tr>
				";
				
			} else {
			    $shopping_list .= " 
					<tr>
	                    <td>
	                        <table width=\"100%\">
	                            <tr>
	                                <td class=\"text-right\" width=\"75%\">
	                                    <input id=\"mycoupon\" type=\"text\" value=\"".$mycoupon."\" placeholder=\"輸入優惠代碼\" style=\"width:67%;margin-bottom:5px;\">
	                                    <button class=\"buylist_btn\" onClick=\"CancleCoupon();\">取消</button>
	                                </td>
	                                <td class=\"text-right\" width=\"25%\">-$".number_format($code)."</td>
	                            </tr>
	                        </table>
	                    </td>
	                </tr>
				";
			}		  
		} else {
			//有輸入優惠代碼嗎
			if ($mycoupon)
			    $shopping_list .= " 
					<tr>
	                    <td>
	                        <table width=\"100%\">
	                            <tr>
	                                <td class=\"text-right\" width=\"75%\">
										優惠代碼（".$mycoupon."）
	                                </td>
	                                <td class=\"text-right\" width=\"25%\">-$".number_format($code)."</td>
	                            </tr>
	                        </table>
	                    </td>
	                </tr>
				"; 
		}
		//紅利折扣---------------------------------------------------------------------------------------------------------------
		$memberShoppingMoney = $_SESSION["MyMemberShoppingMoney"];
		$memberCouponMoney = $_SESSION["MyCouponMoney"];	//優惠券要折扣金額
		$UseBonus = CalUseBonus($subtotal - $code - $memberShoppingMoney - $memberCouponMoney);	//是否符合紅利折扣資格
		$_bonus = $UseBonus[bonus];	//本次的紅利折扣金額
		$bonus = $_SESSION["MyBonus"];	//紅利要折扣金額
		if ($ifLogin && $_bonus) {	//有登入會員
			if ($ifEditor) {
				if(!$bonus) {
					$shopping_list .= " 
					<tr>
	                    <td>
	                        <table width=\"100%\">
	                            <tr>
	                                <td class=\"text-right\" width=\"75%\">
	                                    <input id=\"mybonus\" type=\"text\" value=\"".$_bonus."\" placeholder=\"輸入可折抵紅利金額\" style=\"width:67%;margin-bottom:5px;\">
	                                    <button class=\"buylist_btn\" onClick=\"CheckBonus(".$_bonus.");\">使用</button>
	                                </td>
	                                <td class=\"text-right\" width=\"25%\">-$".number_format($bonus, 0)."</td>
	                            </tr>
	                        </table>
	                    </td>
	                </tr>
					";
				} else {
					$shopping_list .= " 
					<tr>
	                    <td>
	                        <table width=\"100%\">
	                            <tr>
	                                <td class=\"text-right\" width=\"75%\">
	                                    <input id=\"mybonus\" type=\"text\" value=\"".$_bonus."\" placeholder=\"輸入可折抵紅利金額\" style=\"width:67%;margin-bottom:5px;\">
	                                    <button class=\"buylist_btn\" onClick=\"CancleCheckBonus(".$_bonus.");\">取消</button>
	                                </td>
	                                <td class=\"text-right\" width=\"25%\">-$".number_format($bonus, 0)."</td>
	                            </tr>
	                        </table>
	                    </td>
	                </tr>
					";
				}
			} else {
				$shopping_list .= " 
					<tr>
	                    <td>
	                        <table width=\"100%\">
	                            <tr>
	                                <td class=\"text-right\" width=\"75%\">
	                                    可折抵紅利：$".number_format($_bonus, 0)."
	                                </td>
	                                <td class=\"text-right\" width=\"25%\">-$".number_format($bonus, 0)."</td>
	                            </tr>
	                        </table>
	                    </td>
	                </tr>
				";
			}	 
		} else {
			unset($_SESSION["MyBonus"]);
		}

		//購物金---------------------------------------------------------------------------------------------------------------
		$memberCouponMoney = $_SESSION["MyCouponMoney"];	//優惠券要折扣金額
		$UseMemberCode = CalUseMemberCode($subtotal - $code - $bonus - $memberCouponMoney);	//是否符購物金	
		$_memberShoppingMoney = $UseMemberCode[money];	//本次的購物金折扣金額
		$memberShoppingMoney = $_SESSION["MyMemberShoppingMoney"];	//購物金要折扣金額
		$dateLimit = ($UseMemberCode[dateLimit]) ? "  使用期限  (".$UseMemberCode[dateLimit].")" : null;
		if ($ifLogin && $_memberShoppingMoney) {	//有登入會員
			if ($ifEditor) {
				if(!$memberShoppingMoney) {
					$shopping_list .= " 
					<tr>
	                    <td>
	                        <table width=\"100%\">
	                            <tr>
	                                <td class=\"text-right\" width=\"75%\">
	                                    <input id=\"mymembercode\" type=\"text\" value=\"".$_memberShoppingMoney."\" placeholder=\"輸入可折抵購物金\" style=\"width:67%;margin-bottom:5px;\">
	                                    <button class=\"buylist_btn\" onClick=\"CheckMemberShoppingMoney(".$memberShoppingMoney.");\">使用</button>
	                                </td>
	                                <td class=\"text-right\" width=\"25%\">-$".number_format($memberShoppingMoney, 0)."</td>
	                            </tr>
	                        </table>
	                    </td>
	                </tr>
					";
				} else {
					$shopping_list .= " 
					<tr>
	                    <td>
	                        <table width=\"100%\">
	                            <tr>
	                                <td class=\"text-right\" width=\"75%\">
	                                    <input id=\"mymembercode\" type=\"text\" value=\"".$_memberShoppingMoney."\" placeholder=\"輸入可折抵購物金\" style=\"width:67%;margin-bottom:5px;\">
	                                    <button class=\"buylist_btn\" onClick=\"CancleCheckMemberShoppingMoney(".$memberShoppingMoney.");\">取消</button>
	                                </td>
	                                <td class=\"text-right\" width=\"25%\">-$".number_format($memberShoppingMoney, 0)."</td>
	                            </tr>
	                        </table>
	                    </td>
	                </tr>
					";
				}
			} else {
				$shopping_list .= " 
					<tr>
	                    <td>
	                        <table width=\"100%\">
	                            <tr>
	                                <td class=\"text-right\" width=\"75%\">
	                                	可折抵購物金   
	                                </td>
	                                <td class=\"text-right\" width=\"25%\">-$".number_format($memberShoppingMoney, 0)."</td>
	                            </tr>
	                        </table>
	                    </td>
	                </tr>
				";
			}		   
		} else {
			unset($_SESSION["MyMemberShoppingMoney"]);
		}


		//優惠券(只限一般會員) 
		//if($_SESSION['MyMember']['level'] === '1') {
		//第二階段改為不限制
		if(1) { 	
			$memberShoppingMoney = $_SESSION["MyMemberShoppingMoney"];	//購物金要折扣金額
			//優惠券---------------------------------------------------------------------------------------------------------------
			$UseCoupon = CalUseCoupon($subtotal - $code - $bonus - $memberShoppingMoney);	//是否符購物金
			//$_memberCouponMoney = $UseMemberCode[money];	//本次的購物金折扣金額
			$memberCouponMoney = $_SESSION["MyCouponMoney"];	//優惠券要折扣金額
			//$dateLimit = ($UseMemberCode[dateLimit]) ? "  使用期限  (".$UseMemberCode[dateLimit].")" : null;
			$_total = $subtotal - $code - $bonus;
			if ($ifLogin && count($UseCoupon)) {	//有登入會員
				$_web_x_coupon_id = ($_SESSION["MyCoupon"]) ? $_SESSION["MyCoupon"] : 0;
				$Coupon = getCoupon($_web_x_coupon_id);
				$web_x_coupon_list .= "<option value=\"\">優惠券</option>";
				$couponShow = false;
				foreach($UseCoupon as $coupon) {
					if($coupon[money] > $_total) {
						continue;
					}
					//可抵扣產品
					$coupon[bestsellers] = ($coupon[bestsellers]) ? explode(',', $coupon[bestsellers]) : '';
					$result = array_intersect($_SESSION[MyShoppingList], $coupon[bestsellers]);
					if(!count($result)) {
						continue;
					} else {
						$couponShow = true;
					}

					$web_x_coupon_list .= "<option value=\"".$coupon[web_x_coupon_id]."\"";
					if ($_SESSION["MyCoupon"] == $coupon[web_x_coupon_id]) {
						$web_x_coupon_list .= " selected";
						//$default_web_x_freight = $web_x_freight;
					}
					$web_x_coupon_list .= ">【".$coupon[subject]."】 抵扣金額: $".number_format($coupon[money])."</option>\n";
				}
				if($couponShow) {
					if ($ifEditor) {
						$shopping_list .= " 
							<tr>
			                    <td>
			                        <table width=\"100%\">
			                            <tr>
			                                <td class=\"text-right\" width=\"75%\">
			                                    <select name=\"coupon_id\" onChange=\"ChangeCoupon(this);\">
													".$web_x_coupon_list."
												</select>
												<span class=\"hidden-xs\">優惠券：</span>
			                                </td>
			                                <td class=\"text-right\" width=\"25%\">-$".number_format($Coupon[money], 0)."</td>
			                            </tr>
			                        </table>
			                    </td>
			                </tr>
						";
					} else {
						$shopping_list .= " 
							<tr>
			                    <td>
			                        <table width=\"100%\">
			                            <tr>
			                                <td class=\"text-right\" width=\"75%\">
			                                    <select name=\"coupon_id\" onChange=\"ChangeCoupon(this);\">
													".$web_x_coupon_list."
												</select>
												<span class=\"hidden-xs\">優惠券：</span>
			                                </td>
			                                <td class=\"text-right\" width=\"25%\">-$".number_format($Coupon[money], 0)."</td>
			                            </tr>
			                        </table>
			                    </td>
			                </tr>
						";
					}	
				}     
			} else {
				unset($_SESSION["MyCouponMoney"]);
			}
  			

		}
	    $shopping_list .= " 
		    <tr>
	            <td>
	            	<div class=\"col-lg-9 col-md-9 col-sm-9 col-xs-9 padd0 text-right\"> 發票金額：</div>
	                <div class=\"col-lg-3 col-md-3 col-sm-3 col-xs-3 padd0 text-right\"> $".number_format($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney)."</div>
	            </td>
	        </tr>  
	    ";      
		
		//運送地點---------------------------------------------------------------------------------------------------------------
		//先找運費預設值	
		$sql = "Select web_xx_freight_id, ifDefault as FreightDefault, subject as web_xx_freight From web_xx_freight Where ifDefault = '1' ";
		$rs = ConnectDB($DB, $sql);
		for ($i=0; $i<mysqli_num_rows($rs); $i++) {
			$row = mysqli_fetch_assoc($rs);
			foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
		}
		
		//有舉行什麼活動嗎
		$sql = "Select web_xx_freight_id, ifDefault as FreightDefault, subject as web_xx_freight From web_xx_freight Where ifDefault = '0' and sdate <= '".date("Y-m-d")."' and edate >= '".date("Y-m-d")."' order by sdate desc, edate desc, web_xx_freight_id desc ";
		$rs = ConnectDB($DB, $sql);
		for ($i=0; $i<mysqli_num_rows($rs); $i++) {
			$row = mysqli_fetch_assoc($rs);
			foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
		}

		//列出運送地點
		$web_x_freight_list = "";
		$sql = "Select web_x_freight_id, subject as web_x_freight From web_x_freight Where web_xx_freight_id = '".$web_xx_freight_id."' and ifShow = '1' order by asort asc, web_x_freight_id desc";
		$rs = ConnectDB($DB, $sql);
		for ($i=0; $i<mysqli_num_rows($rs); $i++) {
			$row = mysqli_fetch_assoc($rs);
			foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
			
			if (intval($_SESSION["MyFreight"])==0) $_SESSION["MyFreight"] = $web_x_freight_id;	//預設值
			/*
			$web_x_freight_list .= "<option value=\"".$web_x_freight_id."\"";
			if ($_SESSION["MyFreight"]==$web_x_freight_id) {
				$web_x_freight_list .= " selected";
				$default_web_x_freight = $web_x_freight;
			}
			$web_x_freight_list .= ">".$web_x_freight."</option>\n";
			*/
			$web_x_freight_list .= "<input class=\"_freight\" onChange=\"ChangeFreight(this);\" type=\"radio\" id=\"Mti".$i."\" value=\"".$web_x_freight_id."\"";
			if ($_SESSION["MyFreight"]==$web_x_freight_id) {
				$web_x_freight_list .= " checked";
				$default_web_x_freight = $web_x_freight;
			}
			$web_x_freight_list .= ">\n<label for=\"Mti".$i."\">".$web_x_freight."</label>\n";
		}
		$freight = CalFreight($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney);	//運費=小計-優惠代碼-紅利
		
		//如果有活動為免運費時
		if($_SESSION['unFreightFlag'] && $freight) {
			$minus = CalFreightDefault($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney);	//運費=小計-優惠代碼-紅利
			$freight = $freight - $minus;
			
		}

		$srange = CalFreightRange($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney);	//運費區間=小計-優惠代碼-紅利

		$loseFreight = ($srange > ($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney)) ? ($srange - ($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney)) : 0;
		if($_SESSION[bugFlag]) {
			$freight = 0;
		} else {
			if ($ifEditor) {
				$shopping_list .= "
				<tr>
	                <td>  
		                <div class=\"col-lg-9 col-md-9 col-sm-9 col-xs-9 padd0\">";
		            if ($loseFreight) {
						$shopping_list .= "    
		                    <div class=\"total_color\">
		                        <img src=\"images/shock.jpg\" alt=\"\" width=\"20px\">還差$".number_format($loseFreight)."就免運囉
		                    </div>
		                    <p style=\"font-size: 12px;\">下方加購 區人氣商品超好湊!</p>";
		            }   
		            	$shopping_list .= "     
		                    <div class=\"radio\">
		                        ".$web_x_freight_list."
		                    </div>
		                </div>   
		                <div class=\"col-lg-3 col-md-3 col-sm-3 col-xs-3 padd0 text-right\"> 
		                    <label class=\"mar_t15\">$<span id=\"new_freight\">".number_format($freight)."</span></label>
		                </div>
	                </td>
	            </tr>
	            ";  
			} else {
				
				$shopping_list .= " 
			    <tr>
		            <td>
		            	<div class=\"col-lg-9 col-md-9 col-sm-9 col-xs-9 padd0 text-right\"> 運送地點：".$default_web_x_freight."</div>
		                <div class=\"col-lg-3 col-md-3 col-sm-3 col-xs-3 padd0 text-right\"> $<span id=\"new_freight\">".number_format($freight)."</span></div>
		            </td>
		        </tr>  
		    	";     
			}
		}	
	}

	//貨到付款
	if( $_SESSION['Init_Payment_CashOnDelivery2']) {
		$_Init_Payment_CashOnDelivery2_price = CalCashOnDelivery($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney + $freight);
		if($_SESSION[bugFlag]) {
			$_Init_Payment_CashOnDelivery2_price = 0;
		}
	    $shopping_list .= " 
		    <tr>
	            <td>
	            	<div class=\"col-lg-9 col-md-9 col-sm-9 col-xs-9 padd0 text-right\">貨到付款：</div>
	                <div class=\"col-lg-3 col-md-3 col-sm-3 col-xs-3 padd0 text-right\"> $".number_format($_Init_Payment_CashOnDelivery2_price)."</div>
	            </td>
	        </tr>  
	    ";
	    //$_Init_Payment_CashOnDelivery2_price = $Init_Payment_CashOnDelivery2_price;
	}
	//總計---------------------------------------------------------------------------------------------------------------
	/*
	$shopping_list .= "  
		<li class=\"sum text-right\">總計</li>
			<li class=\"point\">$<span id=\"new_total\">".number_format($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney + $freight)."</span></li> 
	";
	$shopping_list .= "
            </div>
        </div>    
    ";
    */
    $shopping_list .= "
	    <tr>
		    <td>
		    	<div class=\"col-lg-9 col-md-9 col-sm-9 col-xs-9 padd0 text-right\"> TOTAL應付金額：</div>
		        <div class=\"col-lg-3 col-md-3 col-sm-3 col-xs-3 padd0 text-right\"> $<span id=\"new_total\">".number_format($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney + $freight + $_Init_Payment_CashOnDelivery2_price)."</span></div>
		    </td>
		</tr>
	";

	$shopping_list .= "
                </tbody>
		    </table>
		";
	$shopping_list .="
		<table class=\"table table_moile\">
            <tbody>
	";
		
    //贈品---------------------------------------------------------------------------------------------------------------
	if (CalGift($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney)!="") {//if不為空值顯示
			
		$giftAry = CalGiftList($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney);
		$giftList = "
			<div style=\"display:none\" class=\"gift\">	
				<table>
					<tbody>
		";
			foreach($giftAry as $key => $giftRow) {
				$pic = ShowPic($giftRow[Covers], "uploadfiles/s/", "uploadfiles/no_image.jpg");
				$giftStock = ($giftRow[stock]) ? 1 : '贈品發送完畢';
				$giftList .= "
					<tr class=\"tab1b Mtr__\">
			            <td>
			                <div class=\"col-sm-3 col-xs-3 padd0\">
			                    <img src=\"".$pic."\" alt=\"".$giftRow[subject]."\" width=\"100px;\">
			                </div>
			                <div class=\"col-sm-9 col-xs-9 padd0\">
			                    <div class=\"ol-sm-9 col-xs-9 text-left\">
			                        <span>贈品：</span>".$giftRow[subject]."
			                    </div>
			                    <div class=\"col-sm-3 col-xs-3 padd0 text-right\">$0</div>
			                    <div class=\"col-sm-12 col-xs-12 padd0\">
			                        <div class=\"col-sm-5 col-xs-4 p_r_0 text-left\">
			                        ".$giftStock."
			                        </div>
			                    </div>
			                </div>
			            </td>
			        </tr>
		        ";
			}	
		$giftList .= "
					</tobdy>    
				</table>    
			</div>
		";
		$gifText = 	"<p class=\"text-left\" id=\"new_gift\">".CalGift($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney)."</span>";

  	}
	
	//贈送紅利---------------------------------------------------------------------------------------------------------------
	$SendBonus = CalSendBonus($subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney);	//紅利:本檔活動
	$web_x_bonus_hidden = $SendBonus[web_x_bonus_hidden];	//紅利流水號
	$web_x_bonus_list = $SendBonus[web_x_bonus_list];		//紅利描述
			
	if ($PageSelfURL!="shoppingsend.php") {
		
		$bonusText = "<span id=\"new_bonus\">".$web_x_bonus_list."</span>";
	}
	//說明---------------------------------------------------------------------------------------------------------------
	$sql = "Select Init_Freight2 From web_basicset Where web_basicset_id = '1' ";
	$rs = ConnectDB($DB, $sql);
	$row = mysqli_fetch_assoc($rs);
	$Init_Freight2 = $row['Init_Freight2']; 	
	$shopping_list .= "
		<tr>
	        <td class=\"tel_bg\">
	            注意事項
	        </td>
	    </tr>
	    <tr>
	        <td class=\"sh_cr2\">
	            <!--<p class=\"text-left\">◆請確認您購買的商品款式、顏色、數量。確認後訂單無法進行任何更改喔!</p>
	            <p class=\"text-left\">◆商品售價將以您實際結帳之即時價格為主，商品數量、贈品與優惠等，也將以您實際結帳為準。</p>
	            <p class=\"text-left\">◆請儘早完成結帳以確保享有贈品與優惠。</p>-->
	            ".$giftList.$gifText.$bonusText.str_front($Init_Freight2)."
	        </td>
	    </tr>
	";    
	$_SESSION["MyShoppingSubtobal"] = $subtotal - $code - $bonus - $memberShoppingMoney - $memberCouponMoney;
	

	$shopping_list .= "
                </tbody>
		    </table>
		";
	 
	
	return $shopping_list;
}

//將訂單資料更新到會員資料裡
function UpdateMember($ordernum) {
	$sql = "Select web_member_id, order_name, order_sex, order_tel, order_mobile, order_email, accept_overseas, accept_city, accept_area, accept_zip, accept_address, accept_foreignaddress From web_x_order Where ordernum like '".$ordernum."' limit 1 ";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = $row[$_key];
	}
	
	if ($web_member_id>0) {
		$sql = "Select uname, sex, tel, mobile, email, ifOverseas, city, area, zip, address, foreignaddress From web_member Where web_member_id = '".$web_member_id."' ";
		$rs = ConnectDB($DB, $sql);
		for ($i=0; $i<mysqli_num_rows($rs); $i++) {
			$row = mysqli_fetch_assoc($rs);
			foreach($row as $_key=>$_value) $$_key = trim($row[$_key]);
		}
		
		if ($uname=="") $uname = $order_name;	//姓名
		if ($sex=="") $sex = $order_sex;	//性別
		if ($tel=="") $tel = $order_tel;	//聯絡電話
		if ($mobile=="") $mobile = $order_mobile;	//手機號碼
		if ($email=="") $email = $order_email;	//電子信箱
		$ifOverseas = $accept_overseas;
		if ($accept_overseas==0) {
			if ($city=="") $city = $accept_city;	//縣市
			if ($area=="") $area = $accept_area;	//鄉鎮市區
			if ($zip=="") $zip = $accept_zip;	//郵遞區號
			if ($address=="") $address = $accept_address;	//通訊地址
		} else {
			if ($foreignaddress=="") $foreignaddress = $accept_foreignaddress;	//國外地址
		}
		
		$sql = "Update web_member set uname = '$uname', sex = '$sex', tel = '$tel', mobile = '$mobile', email = '$email', ifOverseas = '$ifOverseas', city = '$city', area = '$area', zip = '$zip', address = '$address', foreignaddress = '$foreignaddress' Where web_member_id = '".$web_member_id."' ";
		$rs = ConnectDB($DB, $sql);
	}
}

//舊 單一
function fullAdditonalList2() {

	$additionalShow = 'none';
	if(isset($_SESSION['MyShoppingSubtobal'])) {
		$fullAdditionalAry = CalFullAdditional($_SESSION['MyShoppingSubtobal']);
		
		//滿額區間-價格
		$fullAdditionalMoneyAry = CalFullAdditionalMoney($_SESSION['MyShoppingSubtobal']);

		//加購
		if(count($fullAdditionalAry)) {
			//是否落在加購區間
			$additionalShow = null;
			$bestsellersRow2 = explode(',', $fullAdditionalAry[0]);
			$x = 1;
			$filter_class = "filter_class_1";
			$figureclass = "t_align_l r_corners photoframe shadow relative d_inline_b d_md_block d_xs_inline_b tr_all_hover";
			$fullAdditionalSubtotal = CalSubtotal();
			foreach($bestsellersRow2 as $web_product_id) {
				if(in_array($web_product_id, $_SESSION['MyShoppingList'])) {
					continue;
				}
				//require("includes/product_fullAdditional.php");
				
				$Product = getProduct($web_product_id);	//取得產品相關資訊
				if($Product[additionalTag] == '0') {
					continue;
				}
				$now = date("Y-m-d H:i:s");
				//echo $Product[web_product_id].':'.$Product[additionalsdate]."-->".TimeDiff($Product[additionalsdate], $now)."----".$Product[additionaledate]."-->".TimeDiff($Product[additionaledate], $now)."</br>";
				if (TimeDiff($Product[additionalsdate], $now) >= 0 && TimeDiff($now, $Product[additionaledate]) >= 0) {

					$parseSubjectRow = explode('-', $Product[subject]); 
					$Product['subject2'] = ($parseSubjectRow[1]) ? trim(preg_replace('/[0-9]+/', '', $parseSubjectRow[1])) : null;
					
					//產品列表
					$pic = ShowPic($Product[Covers], "uploadfiles/l/", "uploadfiles/no_image.jpg");
					
					if ($Product[ifStop]==1) {	//暫停銷售
						$BuyButton = "<center><button class=\"btn-a additionalBut\" onclick=\"location.href='product_detail.php?id=".$Product[web_product_id]."'\">已售完補貨中</button></center>";
					} else {
						$BuyButton = "<center><button class=\"btn-a additionalBut fullAdditionalBut\" onclick=\"AddShoppingCarFullAdditional(this, ".$Product[web_product_id].", ".$fullAdditionalMoneyAry[0].");\" data-id=\"".$Product[web_product_id]."\" >我要加購</button></center>";	
					}


					//$QuickViewArray2[$web_product_id] = QuickView2($Product[web_product_id]);	//快速檢視
					$web_product_array[] = "
					<a>
						<div class=\"item\">
							<img src=\"".$pic."\" alt=\"".$Product[subject]."\">
							<p class=\"btnlist\"><span style=\"background: #bc121f;\">新品上市</span></p>
							<p class=\"titleh\">".$Product[ifTop].$Product[subject]."</p>
							<p class=\"price\"><span class=\"pull-left\">$".$Product[additionalprice]."</span><s class=\"pull-right\">原價：$".number_format($Product[price])."</s></p>
								<div class=\"text-center\">
									".$BuyButton."
								</div>
						</div>
					</a>	
					";
				}	


				$x++;
			}
			$additionalShow = (count($web_product_array)) ? null : 'none';
		}
	}
	if(empty($additionalShow)) {
		$fullAdditional_list = "
		<h3 class=\"title3\"><span>滿額加購商品</span></h3>
		<div class=\"owl-carousel3\">
			
			".implode("", $web_product_array)."

		</div>";
	}
	return $fullAdditional_list;	
}

//新 -綜合
function fullAdditonalList() {

	$additionalShow = 'none';
	if(isset($_SESSION['MyShoppingSubtobal'])) {
		$fullAdditionalAry = CalFullAdditional($_SESSION['MyShoppingSubtobal']);
		//滿額區間-價格
		$fullAdditionalMoneyAry = CalFullAdditionalMoney($_SESSION['MyShoppingSubtobal']);

		//加購
		if(($fullAdditionalAry)) {
			//是否落在加購區間
			$additionalShow = null;
			$bestsellersRow2 = explode(',', $fullAdditionalAry);

			$x = 1;
			$filter_class = "filter_class_1";
			$figureclass = "t_align_l r_corners photoframe shadow relative d_inline_b d_md_block d_xs_inline_b tr_all_hover";
			$fullAdditionalSubtotal = CalSubtotal();
			foreach($bestsellersRow2 as $web_product_id) {
				if(in_array($web_product_id, $_SESSION['MyShoppingList'])) {
					//continue;
				}
				//是否已加購
				$_fullAdditonalList = SearchMultidimensionalAry_4($_SESSION['MyShopping'], 'web_product_id', 'web_product_id', 'fullAdditional', $web_product_id);
				
				//require("includes/product_fullAdditional.php");
				$Product = getProduct($web_product_id);	//取得產品相關資訊
				if($Product[additionalTag] == '0') {
					continue;
				}
				$now = date("Y-m-d H:i:s");
				//echo $Product[web_product_id].':'.$Product[additionalsdate]."-->".TimeDiff($Product[additionalsdate], $now)."----".$Product[additionaledate]."-->".TimeDiff($Product[additionaledate], $now)."</br>";
				if (TimeDiff($Product[additionalsdate], $now) >= 0 && TimeDiff($now, $Product[additionaledate]) >= 0) {

					$parseSubjectRow = explode('-', $Product[subject]); 
					$Product['subject2'] = ($parseSubjectRow[1]) ? trim(preg_replace('/[0-9]+/', '', $parseSubjectRow[1])) : null;
					
					//產品列表
					$pic = ShowPic($Product[Covers], "uploadfiles/m/", "uploadfiles/no_image.jpg");
					
					if ($Product[ifStop]==1) {	//暫停銷售
						$BuyButton = "<a href=\"#\" class=\"btn7\">已售完補貨中</a>";
					} else {
						if(in_array($web_product_id, $_fullAdditonalList)) {
							$BuyButton = "<a class=\"btn7\" data-id=\"".$Product[web_product_id]."\" >已加購</a>";	
						} else {
							$BuyButton = "<a class=\"btn7 fullAdditionalBut\" onclick=\"AddShoppingCarFullAdditional(this, ".$Product[web_product_id].", ".$fullAdditionalMoneyAry[0].");\" data-id=\"".$Product[web_product_id]."\" >我要加購</a>";
						}		
					}


					//$QuickViewArray2[$web_product_id] = QuickView2($Product[web_product_id]);	//快速檢視
					/*
					$web_product_array[] = "
					<a>
						<div class=\"item\">
							<img src=\"".$pic."\" alt=\"".$Product[subject]."\">
							<p class=\"btnlist\"><span style=\"background: #bc121f;\">新品上市</span></p>
							<p class=\"titleh\">".$Product[ifTop].$Product[subject]."</p>
							<p class=\"price\"><span class=\"pull-left\">$".$Product[additionalprice]."</span><s class=\"pull-right\">原價：$".number_format($Product[price])."</s></p>
								<div class=\"text-center\">
									".$BuyButton."
								</div>
						</div>
					</a>	
					";
					*/
					
					$web_product_array[] = "

						<div class=\"col-lg-2 col-md-2 col-sm-6 col-xs-6 text-center p_l1_r1 m_b_20\">
							<a ><img src=\"".$pic."\" alt=\"".$Product[subject]."\">
								<p class=\"cart_pro\">
									<span>".$Product[ifTop].$Product[subject]."</span>
									</br>
									<span style=\"color:#f00;\">$".$Product[additionalprice]."</span>
								</p>
							</a>
							".$BuyButton."
						</div>

	
	                ";
				}	


				$x++;
			}
			$additionalShow = (count($web_product_array)) ? null : 'none';
		}
	}
	if(empty($additionalShow)) {
		
		$fullAdditional_list = "
		<div class=\"pro_title\">
	        <h3 class=\"cart_color\">加購專區</h3>
	        <p class=\"text\">PURCHASE MORE</p>
	    </div>
	    <div class=\"row mar_tb20\">
	    	".implode("", $web_product_array)."
	    </div>";	
	}
	return $fullAdditional_list;	
}

function SearchMultidimensionalArray($ary, $key, $search) {
	$_num = 0;
    foreach($ary as $index => $row) {
        if($row[$key] == $search) {
        	$_num++;
        }
    }
    return $_num;
}

function SearchMultidimensionalArray_2($ary, $key, $search) {
	$_num = 0;
    foreach($ary as $index => $row) {
        if($row[$key] == $search) {
        	$_num += $row[num];
        }
    }
    return $_num;
}

function SearchMultidimensionalAry($ary, $key, $id_key, $search) {
	$array = array();
    foreach($ary as $index => $row) {
        if($row[$key] == $search) {
        	$array = array(
        		$row[$key] => $row[$id_key]
        	);
        }
    }
 	return $array;
}

function SearchMultidimensionalAry_3($ary, $key, $id_key, $id_key2, $search) {
	$array = array();
    foreach($ary as $index => $row) {
        if($row[$key] == $search && !$row[$id_key2]) {
        	$array = array(
        		$row[$key] => $row[$id_key]
        	);
        }
    }
 	return $array;
}

function SearchMultidimensionalAry_4($ary, $key, $id_key, $id_key2, $search) {
	$array = array();
    foreach($ary as $index => $row) {
        if($row[$key] == $search && $row[$id_key2]) {
        	$array = array(
        		$row[$key] => $row[$id_key]
        	);
        }
    }
 	return $array;
}

function SearchMultidimensionalAry_5($ary, $key, $search) {

	$array = array();
    foreach($ary as $index => $row) {
        if($row[$key] == $search) {
        	$array[] = $ary[$index];
        }
    }
 	return $array;
}

function _tagClass($row) {
	switch($row) {
		case "每日必看":
			$_class = 'b1';
			break;
		case "限時特價":
			$_class = 'b2';
			break;
		case "熱銷商品":
			$_class = 'b3';
			break;
		case "新品上市":
			$_class = 'b4';
			break;
		case "銷售排行":
			$_class = 'b5';
			break;					
	}

	return $_class;
}

//取得優惠券
function getCoupon($web_x_coupon_id) {

	$sql = "Select * From web_x_coupon Where web_x_coupon_id = '".$web_x_coupon_id."'";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysqli_num_rows($rs); $i++) {
		$row = mysqli_fetch_assoc($rs);
		foreach($row as $_key=>$_value) {
			$Coupon[$_key] = str_front($row[$_key]);
			//echo $_key.":".$Member[$_key]."<br />";
		}
	}

	
	return $Coupon;
}

//確認會員是否有訂單
function memberHaveOrder($web_member_id) {
	$sql = "Select * From web_x_order Where web_member_id = '".$web_member_id."'";
	$rs = ConnectDB($DB, $sql);
	/*
	$chkVal = 0;
	if(mysqli_num_rows($rs)) {
		$chkVal = 1;
	}
	*/
	$chkVal = (mysqli_num_rows($rs)) ? 1 : 0;
	return $chkVal;
}

function multineedle_stripos($haystack, $needles, $offset=0) {
	    
    foreach($needles as $needle) {
    	if(stripos($haystack, $needle, $offset) === 0) {
        	$found[$needle] = stripos($haystack, $needle, $offset);
        }		
    }
    return $found;
}


function _str_replace($str) {
	return $str = str_replace(    
	    array('!', '"', '#', '$', '%', '&', '\'', '(', ')', '*',    
	        '+', ', ', '-', '.', '/', ':', ';', '<', '=', '>',    
	        '?', '@', '[', '\\', ']', '^', '_', '`', '{', '|',    
	        '}', '~', '；', '﹔', '︰', '﹕', '：', '，', '﹐', '、',    
	        '．', '﹒', '˙', '·', '。', '？', '！', '～', '‥', '‧',    
	        '′', '〃', '〝', '〞', '‵', '‘', '’', '『', '』', '「',    
	        '」', '“', '”', '…', '❞', '❝', '﹁', '﹂', '﹃', '﹄'),    
	    '',    
	    $str);
}

function getSaleAllNum() {
	//$num = 0;
	foreach($_SESSION['MyShopping'] as $key => $row) {
		if(!$row[saleId]) {
			continue;
		}
		//$test[$row[saleId]] += $row[num];
		$num[$row[saleId]] += $row[num];
		$saleAllNum[$row[saleId]] = $num[$row[saleId]];
	}

	return $saleAllNum;
}

function getSaleAllNumSale() {
	//$num = 0;
	foreach($_SESSION['Sales'] as $key => $row) {
		if(!$row[saleId]) {
			continue;
		}
		//$test[$row[saleId]] += $row[num];
		$num[$row[saleId]] += $row[num];
		$saleAllNum[$row[saleId]] = $num[$row[saleId]];
	}

	return $saleAllNum;
}

function getSaleAllNumBackend($ordernum) {
	if(!$ordernum) {
		return;
	}
	//$num = 0;
	$sql = "Select web_order.*, web_x_order.saleAry as web_x_order_saleAry From web_order Left Join (web_x_order) On (web_x_order.ordernum = web_order.web_x_order_ordernum) Where web_order.web_x_order_ordernum like '".$ordernum."' AND web_order.additional != '1' order by web_order.web_order_id ";
    $rs = ConnectDB($DB, $sql);
    for ($i=0; $i<mysqli_num_rows($rs); $i++) {
    	$row = mysqli_fetch_assoc($rs);
    	if(!$row[web_x_sale_id]) {
			continue;
		}
		$num[$row[web_x_sale_id]] += $row[num];
		$saleAllNum[$row[web_x_sale_id]] = $num[$row[web_x_sale_id]];

    }

    return $saleAllNum;
}

//列表頁搜尋->時間
function list_search_by_time_statistics($field_array) {
	$list_search[sql_sub] = " Where 1 = 1 ";
	$field = $_POST["field"] ? str_filter($_POST["field"]) : str_filter($_GET["field"]);	//欄位
	$keyword = $_POST["keyword"] ? str_filter($_POST["keyword"]) : str_filter($_GET["keyword"]);	//關鍵字
	$search_sdate = $_POST["search_sdate"] ? str_filter($_POST["search_sdate"]) : str_filter($_GET["search_sdate"]);	//開始時間
	$search_edate = $_POST["search_edate"] ? str_filter($_POST["search_edate"]) : str_filter($_GET["search_edate"]);	//結束時間
    $orderBy = $_POST["orderBy"] ? str_filter($_POST["orderBy"]) : str_filter($_GET["orderBy"]);	//結束時間

	$field2 = ($field != 'cdate') ? $field : 'memberCode';
	$field2 = ($field == 'serialnumber') ? 'pincode' : $field2;

	$display = ($field == 'serialnumber') ? '' : 'display:none;';
	if ($keyword != "") {
		$keywords = explode(" ", $keyword);
		//if($field == 'serialnumber') {
			for ($i=0; $i<sizeof($keywords); $i++) {
				if($field != 'memberCode') {
					if ($keywords[$i]!="") $list_search[sql_sub] .= " and (".$field2." like '%".$keywords[$i]."%') ";
				} else {
					if ($keywords[$i]!="") $list_search[sql_sub] .= " and (".$field2." like '".$keywords[$i]."') ";
				}
			}
		//}	
		if($search_sdate != "" && $search_edate != "")  {
			if($field != 'serialnumber') {

				//$sql = "SELECT web_member_id FROM `web_x_order` WHERE 1 AND web_x_order.paymentstatus = '付款成功' AND web_x_order.states = '出貨' AND web_x_order.shipdate <= DATE_SUB(DATE(now()), INTERVAL 7 DAY) AND (cdate >= '".$search_sdate." 00:00:00'  AND cdate <= '".$search_edate." 23:59:59')";

				$sql = "SELECT web_member_id FROM `web_x_order` WHERE 1 AND web_x_order.paymentstatus = '付款成功' AND web_x_order.states = '出貨' AND (cdate >= '".$search_sdate." 00:00:00'  AND cdate <= '".$search_edate." 23:59:59')";

				$rs = ConnectDB($DB, $sql);
				for ($i=0; $i<mysqli_num_rows($rs); $i++) {
					$row = mysqli_fetch_assoc($rs);
					$member[] = str_front($row[web_member_id]);
				}
				$memberIdRow = implode(',', $member);	
				$list_search[sql_sub] .= " and (web_member_id IN (".$memberIdRow.")) ";
			} else {
				
				$sql2 = "SELECT web_x_order_ordernum FROM `web_order` ".$list_search[sql_sub]." AND (cdate >= '".$search_sdate." 00:00:00'  AND cdate <= '".$search_edate." 23:59:59')";
				$rs2 = ConnectDB($DB, $sql2);
				for ($i=0; $i<mysqli_num_rows($rs2); $i++) {
					$row2 = mysqli_fetch_assoc($rs2);
					$web_x_order_ordernum[] = "'".str_front($row2[web_x_order_ordernum])."'";
				}
				$web_x_order_ordernumRow = implode(',', $web_x_order_ordernum);	
				$list_search[sql_sub2] = " AND web_x_order.ordernum IN (".$web_x_order_ordernumRow.")";

				//$sql = "SELECT web_member_id FROM `web_x_order` WHERE 1 ".$list_search[sql_sub2]." AND web_x_order.paymentstatus = '付款成功' AND web_x_order.states = '出貨' AND web_x_order.shipdate <= DATE_SUB(DATE(now()), INTERVAL 7 DAY) AND (cdate >= '".$search_sdate." 00:00:00'  AND cdate <= '".$search_edate." 23:59:59')";

				$sql = "SELECT web_member_id FROM `web_x_order` WHERE 1 ".$list_search[sql_sub2]." AND web_x_order.paymentstatus = '付款成功' AND web_x_order.states = '出貨' AND (cdate >= '".$search_sdate." 00:00:00'  AND cdate <= '".$search_edate." 23:59:59')";

				$rs = ConnectDB($DB, $sql);
				for ($i=0; $i<mysqli_num_rows($rs); $i++) {
					$row = mysqli_fetch_assoc($rs);
					$member[] = str_front($row[web_member_id]);
				}
				$memberIdRow = implode(',', $member);	
				$list_search[sql_sub] = " Where 1 = 1 ";
				$list_search[sql_sub] .= " and (web_member_id IN (".$memberIdRow.")) ";

			}	
			
			$list_search[link] = "&field=".$field."&orderBy=".$orderBy."&keyword=".urlencode($keyword)."&search_sdate=".$search_sdate."&search_edate=".$search_edate;	
		} else {


			$sql2 = "SELECT web_x_order_ordernum FROM `web_order` ".$list_search[sql_sub]."";
			$rs2 = ConnectDB($DB, $sql2);
			for ($i=0; $i<mysqli_num_rows($rs2); $i++) {
				$row2 = mysqli_fetch_assoc($rs2);
				$web_x_order_ordernum[] = "'".str_front($row2[web_x_order_ordernum])."'";
			}
			$web_x_order_ordernumRow = implode(',', $web_x_order_ordernum);
			$list_search[sql_sub2] = " AND web_x_order.ordernum IN (".$web_x_order_ordernumRow.")";

			//$sql = "SELECT web_member_id FROM `web_x_order` WHERE 1 ".$list_search[sql_sub2]." AND web_x_order.paymentstatus = '付款成功' AND web_x_order.states = '出貨' AND web_x_order.shipdate <= DATE_SUB(DATE(now()), INTERVAL 7 DAY)";

			$sql = "SELECT web_member_id FROM `web_x_order` WHERE 1 ".$list_search[sql_sub2]." AND web_x_order.paymentstatus = '付款成功' AND web_x_order.states = '出貨'";

			$rs = ConnectDB($DB, $sql);
			for ($i=0; $i<mysqli_num_rows($rs); $i++) {
				$row = mysqli_fetch_assoc($rs);
				$member[] = str_front($row[web_member_id]);
			}
			$memberIdRow = implode(',', $member);
			$list_search[sql_sub] = " Where 1 = 1 ";
			$list_search[sql_sub] .= " and (web_member_id IN (".$memberIdRow.")) ";


			$list_search[link] .= "&field=".$field."&orderBy=".$orderBy."&keyword=".urlencode($keyword);
		}	
	} else if($search_sdate != "" && $search_edate != "")  {
		if($search_sdate > $search_edate){
			echo "開始時間不能大於結束時間";
			return;
		} else {
			if($field != 'serialnumber') {
				//$sql = "SELECT web_member_id FROM `web_x_order` WHERE 1 AND web_x_order.paymentstatus = '付款成功' AND web_x_order.states = '出貨' AND web_x_order.shipdate <= DATE_SUB(DATE(now()), INTERVAL 7 DAY) AND (cdate >= '".$search_sdate." 00:00:00'  AND cdate <= '".$search_edate." 23:59:59')";

				$sql = "SELECT web_member_id FROM `web_x_order` WHERE 1 AND web_x_order.paymentstatus = '付款成功' AND web_x_order.states = '出貨' AND (cdate >= '".$search_sdate." 00:00:00'  AND cdate <= '".$search_edate." 23:59:59')";


				$rs = ConnectDB($DB, $sql);
				for ($i=0; $i<mysqli_num_rows($rs); $i++) {
					$row = mysqli_fetch_assoc($rs);
					$member[] = str_front($row[web_member_id]);
				}
				$memberIdRow = implode(',', $member);	
				$list_search[sql_sub] .= " and (web_member_id IN (".$memberIdRow.")) ";
			} 
			$list_search[link] = "&field=".$field."&orderBy=".$orderBy."&search_sdate=".$search_sdate."&search_edate=".$search_edate;
		}	
	} else {
		$list_search[link] = "&field=".$field."&orderBy=".$orderBy;
	}	

	foreach($field_array as $_key=>$_value) {
		$field_list .= "\n\t  <option value=\"".$_key."\"";
		if ($field==$_key) $field_list .= " selected";
		$field_list .= ">".$_value."</option>";
	}

	$orderBy_array = array(
		'DESC' => '多到少', 
		'ASC' => '少到多',
	);
	foreach($orderBy_array as $_key => $_value) {
		$orderBy_list .= "\n\t  <option value=\"".$_key."\"";
		if ($orderBy==$_key) $orderBy_list .= " selected";
		$orderBy_list .= ">".$_value."</option>";
	}
		
	echo "
    <select name=\"field\" id=\"field\">".$field_list."
    </select>
    <input name=\"keyword\" id=\"keyword\" type=\"text\" value=\"".$keyword."\" maxlength=\"100\" placeholder=\"請輸入料號\" style=\"".$display."\" />
    排序：<select name=\"orderBy\">
    	".$orderBy_list."
    </select>
   	時間：<input name=\"search_sdate\" id=\"search_sdate\" type=\"text\" value=\"".$search_sdate."\" maxlength=\"10\" placeholder=\"開始日期\" style=\"width: 100px\" />~<input name=\"search_edate\" id=\"search_edate\" type=\"text\" value=\"".$search_edate."\" maxlength=\"10\" placeholder=\"結束日期\" style=\"width: 100px\" />
    <input name=\"submit\" type=\"submit\" value=\"搜尋\" />
    
";

	$list_search[hidden] = "
  <input type=\"hidden\" name=\"field\" value=\"".$field."\" />
  <input type=\"hidden\" name=\"keyword\" value=\"".$keyword."\" />
";
			
	return $list_search;
}


function getLevel($web_permission_id) {
	if(!$web_permission_id) {
		return;
	}

	$sql = "
		Select 
			web_permission.*
		From 
			web_permission 
		Where 
			web_permission.web_permission_id = '".$web_permission_id."' 
	";
    $rs = ConnectDB($DB, $sql);
    for ($i=0; $i<mysqli_num_rows($rs); $i++) {
    	$row = mysqli_fetch_assoc($rs);
    	
		$LevelInfo = $row;

    }

    return $LevelInfo;
}


function getUserLevel($userid) {
	if(!$userid) {
		return;
	}
	//$num = 0;
	$sql = "
		Select 
			web_adminuser.*,
			web_permission.subject as web_permission_subject ,
			web_permission.pid as pid 
		From 
			web_adminuser 
		Left Join 
			(web_permission) On (web_permission.pid = web_adminuser.level && web_permission.web_permission_id = web_adminuser.web_permission_id) 
		Where 
			web_adminuser.web_adminuser_id = '".$userid."' 
	";
    $rs = ConnectDB($DB, $sql);
    for ($i=0; $i<mysqli_num_rows($rs); $i++) {
    	$row = mysqli_fetch_assoc($rs);
    	
		$userLevelInfo = $row;

    }

    return $userLevelInfo;
}

function CalcDirectorySize($DirectoryPath) {
 
    // I reccomend using a normalize_path function here
    // to make sure $DirectoryPath contains an ending slash
    // (-> http://www.jonasjohn.de/snippets/php/normalize-path.htm)
 
    // To display a good looking size you can use a readable_filesize
    // function.
    // (-> http://www.jonasjohn.de/snippets/php/readable-filesize.htm)
 
    $Size = 0;
 
    $Dir = opendir($DirectoryPath);
 
    if (!$Dir)
        return -1;
 
    while (($File = readdir($Dir)) !== false) {
 
        // Skip file pointers
        if ($File[0] == '.') continue;
 
        // Go recursive down, or add the file size
        if (is_dir($DirectoryPath . $File))
            $Size += CalcDirectorySize($DirectoryPath . $File . DIRECTORY_SEPARATOR);
        else
            $Size += filesize($DirectoryPath . $File);
    }
 
    closedir($Dir);
 
    return $Size;
}

function encrypt($string,$operation,$key=''){   
    $key=md5($key);   
    $key_length=strlen($key);   
      $string=$operation=='D'?base64_decode($string):substr(md5($string.$key),0,8).$string;   
    $string_length=strlen($string);   
    $rndkey=$box=array();   
    $result='';   
    for($i=0;$i<=255;$i++){   
           $rndkey[$i]=ord($key[$i%$key_length]);   
        $box[$i]=$i;   
    }   
    for($j=$i=0;$i<256;$i++){   
        $j=($j+$box[$i]+$rndkey[$i])%256;   
        $tmp=$box[$i];   
        $box[$i]=$box[$j];   
        $box[$j]=$tmp;   
    }   
    for($a=$j=$i=0;$i<$string_length;$i++){   
        $a=($a+1)%256;   
        $j=($j+$box[$a])%256;   
        $tmp=$box[$a];   
        $box[$a]=$box[$j];   
        $box[$j]=$tmp;   
        $result.=chr(ord($string[$i])^($box[($box[$a]+$box[$j])%256]));   
    }   
    if($operation=='D'){   
        if(substr($result,0,8)==substr(md5(substr($result,8).$key),0,8)){   
            return substr($result,8);   
        }else{   
            return'';   
        }   
    }else{   
        return str_replace('=','',base64_encode($result));   
    }   
}   


?>