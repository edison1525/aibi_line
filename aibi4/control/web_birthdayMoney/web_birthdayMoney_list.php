<?php include("../includes/head.php"); ?>
<?php
  include("web_birthdayMoney_serach.php");
  
?>
</head>

<body>
<?php 
  //include("../includes/left.php");
  include("../multi-menu/left.php"); 
?>
<h1>生日禮兌換</h1>
<div id="nav">目前位置：<?php echo $nav_title; ?> > <a href="web_birthdayMoney_list.php">兌換記錄</a> > 列表</div>

<!--搜尋開始-->
<form name="formSearch" method="post" action="web_birthdayMoney_list.php" id="searchForm">
  <div id="search" style="display:none;">
	分店：<select name="search_store">
    <option value="">請選擇</option>
    <?php
            $store_list = "";
			if($loginUserLevelInfo['store_id'] != '-1') {
				$storeSql = "SELECT subject, web_x_class_id FROM `web_x_class` WHERE ifShow = '1' AND web_x_class_id = '".$loginUserLevelInfo['store_id']."' ORDER BY asort ASC";
			} else {
				$storeSql = "SELECT subject, web_x_class_id FROM `web_x_class` WHERE ifShow = '1' ORDER BY asort ASC";
			}
			$storeRs = ConnectDB($DB, $storeSql); 
			for ($i=0; $i<mysql_num_rows($storeRs); $i++) {
				$stroeRow = mysql_fetch_assoc($storeRs);
                $store_list .= "<option value=\"".$stroeRow['web_x_class_id']."\"";
                if ($search_store==$stroeRow['web_x_class_id']) $store_list .= " selected=\"selected\"";
                $store_list .= ">".$stroeRow['subject']."</option>";
            }
            echo $store_list;
        ?>
    </select>
	
    狀態：<select name="search_states">
    <option value="">請選擇</option>
    <?php
            $states_list = "";
			$states_array2 = array("兌換成立", "取消");
            foreach ($states_array2 as $value) {
                $states_list .= "<option value=\"".$value."\"";
                if ($search_states==$value) $states_list .= " selected=\"selected\"";
                $states_list .= ">".$value."</option>";
            }
            echo $states_list;
        ?>
    </select>
    領取狀態：<select name="search_paymentstatus">
    <option value="">請選擇</option>
    <?php
            $paymentstatus_list = "";
			$paymentstatus_array = array("未兌換", "已兌換", /*"付款失敗", "付款金額錯誤"*/);
            foreach ($paymentstatus_array as $value) {
                $paymentstatus_list .= "<option value=\"".$value."\"";
                if ($search_paymentstatus==$value) $paymentstatus_list .= " selected=\"selected\"";
                $paymentstatus_list .= ">".$value."</option>";
            }
            echo $paymentstatus_list;
        ?>
    </select>

    關鍵字：<input name="keyword" type="text" value="<?php echo $keyword; ?>" maxlength="100" placeholder="請輸入關鍵字" style="width: 230px;" />
    <span id="orderDate">
    兌換時間：<input name="search_sdate" id="search_sdate" type="text" value="<?php echo $search_sdate; ?>" maxlength="10" placeholder="開始日期" style="width: 100px" />~<input name="search_edate" id="search_edate" type="text" value="<?php echo $search_edate; ?>" maxlength="10" placeholder="結束日期" style="width: 100px" />
    </span>
    <span id="payDate" style="display:none">
    付款時間：<input name="search_paySdate" id="search_paySdate" type="text" value="<?php echo $search_paySdate; ?>" maxlength="10" placeholder="開始日期" style="width: 100px" />~<input name="search_payEdate" id="search_payEdate" type="text" value="<?php echo $search_payEdate; ?>" maxlength="10" placeholder="結束日期" style="width: 100px" /></span>
    <span id="orderAmount" style="display:none">
    訂單金額：<input name="search_samount" id="search_samount" type="text" value="<?php echo $search_samount; ?>" maxlength="7" placeholder="開始金額" style="width: 100px" />~<input name="search_eamount" id="search_eamount" type="text" value="<?php echo $search_eamount; ?>" maxlength="7" placeholder="結束金額" style="width: 100px" />
    </span>
   
    <input id="searchBtn" name="submit" type="submit" value="搜尋" /><!--總銷售額-->
    <div id="new" style="margin-top: 5px;">
      <!--<a href="web_x_order_erp.php?page=1<?php echo $list_search[link]; ?>" title="<?php echo $search_title; ?>">匯出<?php echo ($list_search[link]=="") ? "全部" : "搜尋"; ?>-->
      <!--<a href="web_x_order_erp_list.php?page=1<?php echo $list_search[link]; ?>" title="<?php echo $search_title; ?>">匯出<?php echo ($list_search[link]=="") ? "全部" : "搜尋"; ?>-->
      <!--ERP--></a>
      <!--<a href="web_x_order_output_list.php?page=1<?php echo $list_search[link]; ?>" title="<?php echo $search_title; ?>">匯出訂單</a>-->
      <!--<a title="取回電子發票狀態" id="ec" style="cursor: pointer">取回電子發票狀態</a>-->
      <!--<a href="web_x_order_sum_list.php?page=1<?php echo $list_search[link]; ?>" title="<?php echo $search_title; ?>">綜合統計</a>-->
    </div>
  </div>
  <br class="clear">
</form>
<!--搜尋結束-->

<form name="form" method="post" action="web_x_order_update.php">
  <table class="List_form" style="font-size:12px;">
    <tr>
      <th width="2%" style="display:none;"><input type="checkbox" name="all" onClick="CheckAll(this, 'DeleteBox[]')" /></th>
      <th width="4%">編號</th>
	  <th width="10%">分店</th>
	  <!--<th width="5%">種類</th>-->
      <th width="5%">狀態</th>
      <!--<th width="5%">發票</th>
      <th width="10%">配送方式</th>-->
      <th width="10%">兌換人姓名</th>
      <th width="10%">使用時間</th>
      <th width="10%">兌換時間</th>
      <th width="10%">金額</th>
      <th width="5%" style="display:none;"><a class="edit">改</a></th>
    </tr>
  <?php
    $sql = "
		Select 
			SQL_CALC_FOUND_ROWS *,
			(select subject from web_x_class where web_x_class_id = web_member_money.store_id) as store_subject,
			(select uname from web_member where web_member_id = web_member_money.web_member_id) as order_name			
		From 
			web_member_money ".$list_search[sql_sub]." 
		order by 
			cdate desc, web_member_money_id desc 
	";
	$list_paging = list_paging_sql($page, $Init_ControlPage, $sql);
    $sql .= " limit ".($page-1) * $Init_ControlPage.", ".$Init_ControlPage;
    //echo $sql;
    $rs = ConnectDB($DB, $sql);
    //$list_paging = list_paging($page, $Init_ControlPage);
    for ($i=0; $i<mysql_num_rows($rs); $i++) {
      $row = mysql_fetch_assoc($rs);
      foreach($row as $_key=>$_value) $$_key = $row[$_key];
      
      if ($web_member_id>0) {
        //$link = "<a href=\"../../admin_member.php?id=".$web_member_id."\" target=\"_blank\">".$order_name.$order_sex."</a>";
        $link = "<a href=\"../web_member/web_member_edit.php?web_member_id=".$web_member_id."\" target=\"_blank\">".$order_name.$order_sex."</a>";
      } else {
        $link = $order_name.$order_sex;
      }
      
      //UpdateMember($ordernum);
      //$sql2 = "Update web_order set cdate = '".$cdate."' Where web_x_order_ordernum like '".$ordernum."' ";
      //$rs2 = ConnectDB($DB, $sql2);
	  $style = null;
      switch($kind) {
        case '2' :
          $style = "background:#F4E1E1; color:#666666";
          break;
      }
      

    ?>
    <tr style="<?php echo $style; ?>">
      <td align="center" style="display:none;"><input type="checkbox" name="DeleteBox[]" value="<?php echo $ordernum; ?>" /></td>
      <td align="center"><?php echo $i+1; ?></td>
	  <td align="center">
	  <?php echo $store_subject; ?>
	  </td>
	  <td align="center">
        <?php 
          switch($kind) {
				case 0:
					$status = '未兌換';
				break;
				case 1:
					$status = '已兌換';
				break;
				case 2:
					$status = '已取消';
				break;
				case 3:
					$status = '已使用';
				break;
			}	
			echo $status;
        ?>
      </td>
      <td align="center" style="display:none;">
      
      </td>
      <td align="center" style="<?php echo $style2; ?>; display:none;">
	  </td> 
      <td align="center"><?php echo $link; ?></td>
      <td align="center">
		<?php 
			if($kind == '3') {
				echo "<a href=\"../web_x_order/web_x_order_list.php?keyword=".$web_x_order_to_ordernum."\" target=\"_blank\">".$web_x_order_to_ordernum."</a></br>";
				echo $usedate;
			}	
		?>
	  </td>
      <td align="center"><?php echo str_replace(" ", "<br />", $cdate); ?></td>
      <td align="center"><span class="font_grayred">$<?php echo number_format($money); ?> </span></td>
      <td align="center" style="display:none;"><a href="web_birthdayMoney_edit.php?web_member_money_id=<?php echo $web_member_money_id."&page=".$page.$list_search[link]; ?>" class="edit">改</a></td>
    </tr>
  <?php } ?>
  </table>
  <div id="delete">
    <select name="action" id="action" style="display:none;">
      <option value="">請選擇</option>
      <!--<option value="訂單成立">訂單成立</option>
      <option value="收款">收款</option>-->
      <!--<option value="上傳超商訂單">上傳超商訂單</option>-->
      <!--<option value="出貨">出貨</option>-->
      <!--<option value="取消">取消</option>
	  <option value="付款成功">付款成功</option>-->
      <!--<option value="退貨">退貨</option>-->
      <!--<option value="完成">完成</option>-->
      <option value="Delete">刪除</option>
	  <!--
      <option value="上傳電子發票">上傳電子發票</option>
      <option value="刪除電子發票">刪除電子發票</option>-->
    </select> 
    <input type="submit" name="submit" value="執行" onClick="return _CheckDel();" style="display:none;" />
    <span style="display: none;" id="ecInfoCreateTime">
      <input type="text" name="ecdate" id="ecdate" value="" size="10" maxlength="10" placeholder="電子發票開立日期"/>
      <input type="submit" name="submit" value="執行" onClick="return _CheckDel2();" />
    </span>
  </div>
  <?php list_page("web_birthdayMoney_list.php", $page, $list_paging[pages], $list_paging[records], $list_search[hidden], $list_search[link]); ?>
  <!--<input type="hidden" name="action" value="Delete" />-->
</form>
<?php include("../includes/footer.php"); ?>
<script type="text/javascript">
<!--
var comma_separator_number_step = $.animateNumber.numberStepFactories.separator(',')
$('#totalsales').animateNumber({
    number: <?php echo $totalsales; ?>,
    numberStep: comma_separator_number_step
});
$(function() {
  //console.log($("select[name='search_paymentstatus']").val());
  if($("select[name='search_paymentstatus']").val() == '付款成功' || $("select[name='search_paymentstatus']").val() == '付款失敗'  || $("select[name='search_paymentstatus']").val() == '付款金額錯誤') {
    $('#payDate').show();
    $('#orderDate').hide();
    //$('#search_sdate, #search_edate, #search_paySdate, #search_payEdate').val('');
  } else {
    $('#payDate').hide();
    $('#orderDate').show();
  }


  $("select[name='search_paymentstatus']").on('click', function() {
    //$('#search_sdate, #search_edate, #search_paySdate, #search_payEdate').val('');
    if($(this).val() == '付款成功' || $(this).val() == '付款失敗' || $(this).val() == '付款金額錯誤') {
      $('#payDate').show();
      $('#orderDate').hide();
    } else {
      $('#payDate').hide();
      $('#orderDate').show();
    }

  });

  $('#orderAmount').on('keydown', '#search_samount, #search_eamount', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});

  $("#searchBtn").on('click', function(e) {
    var samount = parseInt($('#search_samount').val());
    var eamount = parseInt($('#search_eamount').val());
    //e.preventDefault();
    console.log(samount,eamount);
    if(samount || eamount) {
        //console.log(Math.samonut,Math.eamonut);
        if((samount > eamount) || !samount || !eamount) {
          alert('輸入正確金額');
          e.preventDefault();
        }
    } else {
      $("#searchForm").submit();
    }

  }); 

  $('#ec').on('click', function(e) {

    e.preventDefault();
    ajaxobj = new AJAXRequest;
    ajaxobj.method = "POST";
    ajaxobj.url = "../../ecDownload.php";
    //ajaxobj.content = "action=../../ecDownload.php";
    ajaxobj.callback = function (xmlobj) {
      var response = xmlobj.responseText;
      if(response != '連線失敗') {
        if(response != '0') {
          alert(response+'筆資料回傳');
      location.reload();
        } else {
          alert('無電子發票回傳');
    } 
        
      } else {
        alert(response);
      }  
    };
    ajaxobj.send();  

    return false;
  }) 

})
//-->
</script>
</body>
</html>