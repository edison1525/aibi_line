<?php include("../includes/head.php"); ?>
</head>

<body>
<?php 
  //include("../includes/left.php");
  include("../multi-menu/left.php"); 
?>
<h1>QR Code</h1>
<div id="nav">目前位置：<?php echo $nav_title; ?> > <a href="web_x_qrcode_list.php">QR Code</a> > 列表</div>

<!--搜尋開始-->
<form name="formSearch" method="post" action="web_x_qrcode_list.php">
  <div id="search">
	<?php $list_search = list_search(array("subject" => "標題")); ?>
    <div id="new" style="display: "><a href="web_x_qrcode_edit.php">新增資料</a></div>
  </div>
</form>
<!--搜尋結束-->

<form name="form" method="post" action="web_x_qrcode_update.php">
  <table class="List_form">
    <tr>
      <th width="5%" style="display: none"><input type="checkbox" name="all" onClick="CheckAll(this, 'DeleteBox[]')" /></th>
  	  <th width="10%">是否顯示</th>
  	  <th width="10%">排序</th>
  	  <th>標題</th>
  	  <th width="15%">項目</th>
      <th width="5%"><a class="edit">改</a></th>
    </tr>
	<?php
        $sql = "Select SQL_CALC_FOUND_ROWS web_x_qrcode_id, ifShow, asort, subject, (Select count(*) From web_qrcode Where web_x_qrcode_id = web_x_qrcode.web_x_qrcode_id) as counter From web_x_qrcode ".$list_search[sql_sub]." order by asort asc, web_x_qrcode_id desc ";
        $sql .= " limit ".($page-1) * $Init_ControlPage.", ".$Init_ControlPage;
        $rs = ConnectDB($DB, $sql);
		$list_paging = list_paging($page, $Init_ControlPage);
		for ($i=0; $i<mysql_num_rows($rs); $i++) {
			$row = mysql_fetch_assoc($rs);
			foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
    ?>
    <tr>
      <td align="center" style="display: none"><?php if ($counter==0) { ?><input type="checkbox" name="DeleteBox[]" value="<?php echo $web_x_qrcode_id; ?>" /><?php } else { echo "X"; } ?></td>
  	  <td align="center"><?php echo ($ifShow==1) ? "是" : "否"; ?></td>
  	  <td align="center"><?php echo $asort; ?></td>
  	  <td><?php echo $subject; ?></td>
  	  <td align="center"><a href="web_qrcode_list.php?web_x_qrcode_id=<?php echo $row["web_x_qrcode_id"]; ?>"><?php echo $counter; ?> 個項目</a></td>
      <td align="center"><a href="web_x_qrcode_edit.php?web_x_qrcode_id=<?php echo $web_x_qrcode_id."&page=".$page.$list_search[link]; ?>" class="edit">改</a></td>
    </tr>
	<?php } ?>
  </table>
  <div id="delete" style="display: none"> 
    <input type="submit" name="submit" value="刪除" onClick="return CheckDel();" />
  </div>
  <?php list_page("web_x_qrcode_list.php", $page, $list_paging[pages], $list_paging[records], $list_search[hidden], $list_search[link]); ?>
  <input type="hidden" name="action" value="Delete" />
</form>
<?php include("../includes/footer.php"); ?>
</body>
</html>