<?php include("../includes/head.php"); ?>
<?php
  include("web_x_order_serach.php");
  //總銷售額
  $sql = "
	Select 
		web_x_order.total as totalsales,
		web_xx_product.subject,
		web_order.web_x_order_ordernum	
	From 
		web_x_order
	Left Join	
		web_order web_order
	On
		web_order.web_x_order_ordernum = web_x_order.ordernum		
	Left Join
		web_product web_product
	On
		web_product.web_product_id = web_order.web_product_id	
	Left Join
		web_x_product web_x_product
	On
		web_x_product.web_x_product_id = web_product.web_x_product_id
	Left Join
		web_xx_product web_xx_product
	On
		web_xx_product.web_xx_product_id = web_x_product.web_xx_product_id	
	".$list_search[sql_sub]."	
    Group By web_order.web_x_order_ordernum	
  ";
  
  $rs = ConnectDB($DB, $sql);
  for ($i=0; $i<mysql_num_rows($rs); $i++) {
	//$totalsales = mysql_result($rs, 0, "totalsales");
	$_totalsales = mysql_fetch_assoc($rs);
	$totalsales += $_totalsales['totalsales'];
  }
  
?>
</head>

<body>
<?php 
  //include("../includes/left.php");
  include("../multi-menu/left.php"); 
?>
<h1>訂單</h1>
<div id="nav">目前位置：<?php echo $nav_title; ?> > <a href="web_x_order_list.php">訂單</a> > 列表</div>

<!--搜尋開始-->
<form name="formSearch" method="post" action="web_x_order_list.php" id="searchForm">
  <div id="search">
	分店：<select name="search_store">
    <option value="">請選擇</option>
    <?php
            $store_list = "";
			if($loginUserLevelInfo['store_id'] != '-1') {
				$storeSql = "SELECT subject, web_x_class_id FROM `web_x_class` WHERE ifShow = '1' AND web_x_class_id = '".$loginUserLevelInfo['store_id']."' ORDER BY asort ASC";
			} else {
				$storeSql = "SELECT subject, web_x_class_id FROM `web_x_class` WHERE ifShow = '1' ORDER BY asort ASC";
			}
			$storeRs = ConnectDB($DB, $storeSql); 
			for ($i=0; $i<mysql_num_rows($storeRs); $i++) {
				$stroeRow = mysql_fetch_assoc($storeRs);
                $store_list .= "<option value=\"".$stroeRow['web_x_class_id']."\"";
                if ($search_store==$stroeRow['web_x_class_id']) $store_list .= " selected=\"selected\"";
                $store_list .= ">".$stroeRow['subject']."</option>";
            }
            echo $store_list;
        ?>
    </select>
	種類：<select name="search_orderType">
		<option value="">請選擇</option>
		<option value="-1"<?php if ($search_orderType=="-1") echo " selected=\"selected\""; ?>>票券購買</option>
		<option value="-2"<?php if ($search_orderType=="-2") echo " selected=\"selected\""; ?>>儲值購買</option>
		<option value="1"<?php if ($search_orderType=="1") echo " selected=\"selected\""; ?>>票券使用</option>
		<option value="2"<?php if ($search_orderType=="2") echo " selected=\"selected\""; ?>>儲值使用</option>
    </select>
    訂單狀態：<select name="search_states">
    <option value="">請選擇</option>
    <?php
            $states_list = "";
            foreach ($states_array2 as $value) {
                $states_list .= "<option value=\"".$value."\"";
                if ($search_states==$value) $states_list .= " selected=\"selected\"";
                $states_list .= ">".$value."</option>";
            }
            echo $states_list;
        ?>
    </select>
    付款狀態：<select name="search_paymentstatus">
    <option value="">請選擇</option>
    <?php
            $paymentstatus_list = "";
            foreach ($paymentstatus_array as $value) {
                $paymentstatus_list .= "<option value=\"".$value."\"";
                if ($search_paymentstatus==$value) $paymentstatus_list .= " selected=\"selected\"";
                $paymentstatus_list .= ">".$value."</option>";
            }
            echo $paymentstatus_list;
        ?>
    </select>

    關鍵字：<input name="keyword" type="text" value="<?php echo $keyword; ?>" maxlength="100" placeholder="請輸入關鍵字" style="width: 230px;" />
	</br>
	</br>
    <span id="orderDate">
    訂購時間：<input name="search_sdate" id="search_sdate" type="text" value="<?php echo $search_sdate; ?>" maxlength="10" placeholder="開始日期" style="width: 100px" />~<input name="search_edate" id="search_edate" type="text" value="<?php echo $search_edate; ?>" maxlength="10" placeholder="結束日期" style="width: 100px" />
    </span>
    <span id="payDate" style="display:none">
    付款時間：<input name="search_paySdate" id="search_paySdate" type="text" value="<?php echo $search_paySdate; ?>" maxlength="10" placeholder="開始日期" style="width: 100px" />~<input name="search_payEdate" id="search_payEdate" type="text" value="<?php echo $search_payEdate; ?>" maxlength="10" placeholder="結束日期" style="width: 100px" /></span>
    <span id="orderAmount">
    訂單金額：<input name="search_samount" id="search_samount" type="text" value="<?php echo $search_samount; ?>" maxlength="7" placeholder="開始金額" style="width: 100px" />~<input name="search_eamount" id="search_eamount" type="text" value="<?php echo $search_eamount; ?>" maxlength="7" placeholder="結束金額" style="width: 100px" />
    </span>
   
    <input id="searchBtn" name="submit" type="submit" value="搜尋" />
    <div style="float: right; margin-top: 5px; margin-left: 13px;"><span class="font_grayred" style="font-size: 17px;">$<span id="totalsales"><?php echo number_format($totalsales); ?></span></span></div><!--總銷售額-->
    <div id="new" style="margin-top: 5px;">
      <!--<a href="web_x_order_erp.php?page=1<?php echo $list_search[link]; ?>" title="<?php echo $search_title; ?>">匯出<?php echo ($list_search[link]=="") ? "全部" : "搜尋"; ?>-->
<?php
	if($loginUserLevelInfo['export']) {
?>      
	  <a href="web_x_order_erp_list.php?page=1<?php echo $list_search[link]; ?>" title="<?php echo $search_title; ?>">匯出<?php echo ($list_search[link]=="") ? "全部" : "搜尋"; ?>
      <!--ERP--></a>
<?php
	}
?>	
      <!--<a href="web_x_order_output_list.php?page=1<?php echo $list_search[link]; ?>" title="<?php echo $search_title; ?>">匯出訂單</a>-->
      <!--<a title="取回電子發票狀態" id="ec" style="cursor: pointer">取回電子發票狀態</a>-->
      <!--<a href="web_x_order_sum_list.php?page=1<?php echo $list_search[link]; ?>" title="<?php echo $search_title; ?>">綜合統計</a>-->
    </div>
  </div>
  <br class="clear">
</form>
<!--搜尋結束-->

<form name="form" method="post" action="web_x_order_update.php">
  <table class="List_form" style="font-size:12px;">
    <tr>
      <th width="2%"><input type="checkbox" name="all" onClick="CheckAll(this, 'DeleteBox[]')" /></th>
      <th width="4%">編號</th>
      <th width="15%">訂單編號</th>
	  <th width="10%">分店</th>
	  <th width="5%">種類</th>
      <th width="5%">狀態</th>
      <th width="12%">付款狀態</th>
      <!--<th width="5%">發票</th>
      <th width="10%">配送方式</th>-->
      <th width="10%">訂購人姓名</th>
      <th width="10%">品名</th>
      <th width="15%">付款時間</th>
      <th width="15%">訂購時間</th>
      <th width="5%">總計</th>
      <th width="5%"><a class="edit">改</a></th>
    </tr>
  <?php
    $sql = "
		Select 
			SQL_CALC_FOUND_ROWS 
			web_x_order.web_x_order_id, 
			web_x_order.ordernum, 
			web_x_order.states, 
			web_x_order.order_type, 
			web_x_order.shipdate, 
			web_x_order.paymentstatus, 
			web_x_order.successPayDate, 
			web_x_order.neweb_feedback, 
			web_x_order.neweb_result, 
			web_x_order.neweb_writeoff, 
			web_x_order.total, 
			web_x_order.payment, 
			web_x_order.transport, 
			web_x_order.store, 
			web_x_order.reqCvsStatus, 
			web_x_order.ecStatus, 
			web_x_order.CvsError, 
			web_x_order.store_id, 
			(select subject from web_x_class where web_x_class_id = web_x_order.store_id) as store_subject,
			web_x_order.web_member_id, 
			web_x_order.order_name, 
			web_x_order.order_sex, 
			web_x_order.accept_name, 
			web_x_order.accept_sex, 
			web_x_order.order_sex, 
			web_x_order.cdate,
			web_xx_product.subject
		From 
			web_x_order 
		Left Join	
			web_order web_order
		On
			web_order.web_x_order_ordernum = web_x_order.ordernum		
		Left Join
			web_product web_product
		On
			web_product.web_product_id = web_order.web_product_id	
		Left Join
			web_x_product web_x_product
		On
			web_x_product.web_x_product_id = web_product.web_x_product_id
		Left Join
			web_xx_product web_xx_product
		On
			web_xx_product.web_xx_product_id = web_x_product.web_xx_product_id	
		".$list_search[sql_sub]." 
		group by 
			web_order.web_x_order_ordernum	
		order by 
			web_x_order.cdate desc, web_x_order.web_x_order_id desc 
	";
	$list_paging = list_paging_sql($page, $Init_ControlPage, $sql);
    $sql .= " limit ".($page-1) * $Init_ControlPage.", ".$Init_ControlPage;
    //echo $sql;
	$rs = ConnectDB($DB, $sql);
    //$list_paging = list_paging($page, $Init_ControlPage);
    for ($i=0; $i<mysql_num_rows($rs); $i++) {
      $row = mysql_fetch_assoc($rs);
      foreach($row as $_key=>$_value) $$_key = $row[$_key];
      
      if ($web_member_id>0) {
        //$link = "<a href=\"../../admin_member.php?id=".$web_member_id."\" target=\"_blank\">".$order_name.$order_sex."</a>";
        $link = "<a href=\"../web_member/web_member_edit.php?web_member_id=".$web_member_id."\" target=\"_blank\">".$order_name.$order_sex."</a>";
      } else {
        $link = $order_name.$order_sex;
      }
      
      if(!$successPayDate) {

        $successTextAry = ($neweb_feedback) ? explode("<br />", $neweb_feedback) : explode("<br />", $neweb_writeoff);
        $successText = str_replace('訊息時間：', '', $successTextAry[0]);

        //訂單回傳網址訊息 寫回訂單
        $sqlUpdate = "Update web_x_order set successPayDate = '$successText' Where ordernum like '".$ordernum."' ";
        $rsUpdate = ConnectDB($DB, $sqlUpdate);

      } else {
        $successText = $successPayDate;
      }  
      //UpdateMember($ordernum);
      //$sql2 = "Update web_order set cdate = '".$cdate."' Where web_x_order_ordernum like '".$ordernum."' ";
      //$rs2 = ConnectDB($DB, $sql2);

       switch($payment) {
        case '線上刷卡' :
          $style = "background:#E1FED8; color:#666666";
          break;
        case '超商代碼' :
          $style = "background:#F4EFF5; color:#666666";
          break;
        case 'ATM虛擬帳號' :
          $style = "background:#E6F4FF; color:#666666";
          break;
        case 'Web ATM' :
          $style = "background:#E6F4FF; color:#666666";
          break;
        default :
          $style = "";
          break;  
                
      }
      switch($states) {
        case '取消' :
          $style = "background:#F4E1E1; color:#666666";
          break;
      }
      $style2 = null;
      switch($transport) {
        case '超商取貨' :
          if($paymentstatus == '付款成功' && !$reqCvsStatus) {
            $style = "-moz-animation: bg2 1.5s infinite; -webkit-animation: bg2 1.5s infinite;";
          }
          if($CvsError) {
            $style2 = "background: #FFF000";
          }
          break;
      }

    ?>
    <tr style="<?php echo $style; ?>">
      <td align="center"><input type="checkbox" name="DeleteBox[]" value="<?php echo $ordernum; ?>" /></td>
      <td align="center"><?php echo $i+1; ?></td>
      <td align="center"><!--<a href="../../admin_order_detail.php?id=<?php echo $ordernum; ?>" target="_blank">--><span class="font_grayred"><?php echo $ordernum; ?></span><!--</a>--></td>
	  <td align="center">
	  <?php echo $store_subject; ?>
	  </td>
      <td align="center">
        <?php 
          switch($order_type) {
			case '0' :
				$sql3 = "
					Select 
						(CASE c.subject WHEN '儲值' THEN '儲值購買' WHEN '票券' THEN '票券購買' END) as subject_text
					From 
						web_order a
					Left Join
						web_product d
					On
						d.web_product_id = a.web_product_id	
					Left Join
						web_x_product b
					On
						b.web_x_product_id = d.web_x_product_id
					Left Join
						web_xx_product c
					On
						c.web_xx_product_id = b.web_xx_product_id	
					Where 
						a.web_x_order_ordernum like '".$ordernum."'
				";
				$rs3 = ConnectDB($DB, $sql3);
				//$web_xx_Subject = mysql_result($rs3, 0, "csubject");
				$web_xx_Subject= mysql_result($rs3, 0, "subject_text");
				$orderTypeText = $web_xx_Subject;
			  break;
			case '1' :
			  $orderTypeText = "票券使用";
			  break;
			case '2' :
			  $orderTypeText = "儲值使用";
			  break;
					
		  } 
		  echo $orderTypeText
        ?>
      </td>
	  <td align="center">
        <?php 
          echo $states; 
          if($states === '出貨' && $shipdate)
            echo "</br>".$shipdate; 
        ?>
      </td>
      <td align="center">
      <?php //echo $payment; ?>
      <!--<br />-->
      <?php if ($neweb_result) { ?>
        <a href="#neweb_result_<?echo $ordernum; ?>" class="inline"><?php echo $paymentstatus; ?></a>
        <div style="display: none;">
          <div id="neweb_result_<?echo $ordernum; ?>"><?php echo $neweb_result; ?></div>
        </div>
      <?php } else if($neweb_writeoff) { ?>
        <a href="#neweb_writeoff_<?echo $ordernum; ?>" class="inline"><?php echo $paymentstatus; ?></a>
        <div style="display: none;">
          <div id="neweb_writeoff_<?echo $ordernum; ?>"><?php echo $neweb_writeoff; ?></div>
        </div>
      <?php } else if($neweb_feedback) { ?>
        <a href="#neweb_feedback<?echo $ordernum; ?>" class="inline"><?php echo $paymentstatus; ?></a>
        <div style="display: none;">
          <div id="neweb_feedback<?echo $ordernum; ?>"><?php echo $neweb_feedback; ?></div>
        </div>  
      <?php } else { ?>
		<?php
		
			switch($payment) {
				case "cash":
					$paymentText = "現金</br>";
				break;
				case "credit_card":
					$paymentText = "信用卡</br>";
				break;
				default:
					$paymentText = "";
				break;	
			}
			echo $paymentText;
		?>
        <?php echo $paymentstatus; ?>
      <?php } ?>  
      </td>
      <td align="center" style="display:none;">
      <?php 
        if($ecStatus) { 
      ?>
        <a href="#ecInfo_<?echo $ordernum; ?>" class="inline"><?php echo $ecStatus; ?></a>
        <div style="display: none;">
          <div id="ecInfo_<?echo $ordernum; ?>">
            <?php 
              //echo $ecInfo;
              echo "<hr>"; 
              $sql2 = "Select * From web_eclog Where ordernum like '".$ordernum."'";
              $rs2 = ConnectDB($DB, $sql2);
              for ($i2=0; $i2<mysql_num_rows($rs2); $i2++) {
                $row2 = mysql_fetch_assoc($rs2);
                foreach($row2 as $_key2 => $_value2) $$_key2 = str_front($row2[$_key2]);

                echo "訂單編號:".$ordernum."</br>";
                echo "檔案名稱:".$fileName."</br>";
                echo "錯誤訊息:".$error."</br>";
                echo "發票資訊:".$InvStatus."</br>";
                echo "日    期:".$cdate;
                echo "<hr>";

              }  

            ?>
            </div>
        </div>
      <?php 
        }
      ?>
      </td>
      <td align="center" style="<?php echo $style2; ?>; display:none;">
      <?php echo $transport; ?>
     <?php if ($transport == '超商取貨') { ?>
      <br />
      <?php echo $store; ?>
      <?php } ?>  
	  </td> 
      <td><?php echo $link; ?></td>
      <td>
		<?php 
			$sql3 = "Select GROUP_CONCAT(DISTINCT subject SEPARATOR ', ') AS subjectAry From web_order Where web_x_order_ordernum like '".$ordernum."'";
            $rs3 = ConnectDB($DB, $sql3);
			$prodSubject = mysql_result($rs3, 0, "subjectAry");
			echo $prodSubject;
		?>
	  </td>
      <td align="center"><?php echo $shipdate; ?></td>
      <td align="center"><?php echo str_replace(" ", "<br />", $cdate); ?></td>
      <td align="right"><span class="font_grayred">$<?php echo number_format($total); ?></span></td>
      <td align="center"><a href="web_x_order_edit.php?web_x_order_id=<?php echo $web_x_order_id."&page=".$page.$list_search[link]; ?>" class="edit">改</a></td>
    </tr>
  <?php } ?>
  </table>
  <div id="delete">
    <select name="action" id="action">
      <option value="">請選擇</option>
      <!--<option value="訂單成立">訂單成立</option>
      <option value="收款">收款</option>-->
      <!--<option value="上傳超商訂單">上傳超商訂單</option>-->
      <!--<option value="出貨">出貨</option>-->
      <!--<option value="取消">取消</option>
	  <option value="付款成功">付款成功</option>-->
      <!--<option value="退貨">退貨</option>-->
      <!--<option value="完成">完成</option>-->
      <option value="Delete">刪除</option>
	  <!--
      <option value="上傳電子發票">上傳電子發票</option>
      <option value="刪除電子發票">刪除電子發票</option>-->
    </select> 
    <input type="submit" name="submit" value="執行" onClick="return _CheckDel();" />
    <span style="display: none;" id="ecInfoCreateTime">
      <input type="text" name="ecdate" id="ecdate" value="" size="10" maxlength="10" placeholder="電子發票開立日期"/>
      <input type="submit" name="submit" value="執行" onClick="return _CheckDel2();" />
    </span>
  </div>
  <?php list_page("web_x_order_list.php", $page, $list_paging[pages], $list_paging[records], $list_search[hidden], $list_search[link]); ?>
  <!--<input type="hidden" name="action" value="Delete" />-->
</form>
<?php include("../includes/footer.php"); ?>
<script type="text/javascript">
<!--
var comma_separator_number_step = $.animateNumber.numberStepFactories.separator(',')
$('#totalsales').animateNumber({
    number: <?php echo $totalsales; ?>,
    numberStep: comma_separator_number_step
});
$(function() {
  //console.log($("select[name='search_paymentstatus']").val());
  /*
  if($("select[name='search_paymentstatus']").val() == '付款成功' || $("select[name='search_paymentstatus']").val() == '付款失敗'  || $("select[name='search_paymentstatus']").val() == '付款金額錯誤') {
    $('#payDate').show();
    $('#orderDate').hide();
    //$('#search_sdate, #search_edate, #search_paySdate, #search_payEdate').val('');
  } else {
    $('#payDate').hide();
    $('#orderDate').show();
  }


  $("select[name='search_paymentstatus']").on('click', function() {
    //$('#search_sdate, #search_edate, #search_paySdate, #search_payEdate').val('');
    if($(this).val() == '付款成功' || $(this).val() == '付款失敗' || $(this).val() == '付款金額錯誤') {
      $('#payDate').show();
      $('#orderDate').hide();
    } else {
      $('#payDate').hide();
      $('#orderDate').show();
    }

  });
  */
  $('#orderAmount').on('keydown', '#search_samount, #search_eamount', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});

  $("#searchBtn").on('click', function(e) {
    var samount = parseInt($('#search_samount').val());
    var eamount = parseInt($('#search_eamount').val());
    //e.preventDefault();
    console.log(samount,eamount);
    if(samount || eamount) {
        //console.log(Math.samonut,Math.eamonut);
        if((samount > eamount) || !samount || !eamount) {
          alert('輸入正確金額');
          e.preventDefault();
        }
    } else {
      $("#searchForm").submit();
    }

  }); 

  $('#ec').on('click', function(e) {

    e.preventDefault();
    ajaxobj = new AJAXRequest;
    ajaxobj.method = "POST";
    ajaxobj.url = "../../ecDownload.php";
    //ajaxobj.content = "action=../../ecDownload.php";
    ajaxobj.callback = function (xmlobj) {
      var response = xmlobj.responseText;
      if(response != '連線失敗') {
        if(response != '0') {
          alert(response+'筆資料回傳');
      location.reload();
        } else {
          alert('無電子發票回傳');
    } 
        
      } else {
        alert(response);
      }  
    };
    ajaxobj.send();  

    return false;
  }) 

})
//-->
</script>
</body>
</html>