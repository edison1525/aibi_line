<?php 
  include("../includes/head.php"); 
  include("web_x_order_serach.php");
  if ($search_title!="") $search_title = "（".$search_title."）";
?>
<body>
<?php 
  //include("../includes/left.php");
  include("../multi-menu/left.php"); 
?>
<h1>匯出列表</h1>
<div id="nav">目前位置：<?php echo $nav_title; ?> > <a href="web_x_order_list.php">訂單</a> > 列表</div>
  <table class="List_form">
    <tr>
  	  <th width="5%">匯出檔案</th> 	  
    </tr>
	<?php
    $FieldArray = array(
      "web_x_order.ordernum" => "訂單編號",
      "web_x_order.cdate" => "訂購日期",
      //"web_order.serialnumber" => "商品編號",
      "web_order.pincode" => "商品編號",
      "web_order.subject" => "商品名稱",
      "web_order.price" => "原價",
      "web_order.num" => "購買數",
      "web_order_id as subtotal" => "小計",
      "web_order_id as amountofgoods" => "商品金額",
      "web_order_id as afterdiscount" => "折扣後金額",
      //"web_x_order.web_code_subject" => "商品含稅額",
      "web_x_order.freight" => "加收運費",
      "web_x_order.web_x_freight_subject" => "貨到付款手續費",
      "web_x_order.total" => "訂單金額",
      "web_x_order.web_bonus_from_money" => "使用紅利點數",
      "(web_x_order.total - web_x_order.web_bonus_from_money)" => "實際總金額",
      "web_x_order.order_name" => "會員姓名",
      "web_x_order.order_mobile" => "訂購者手機",
      "web_x_order.order_tel" => "訂購者電話",
      "web_x_order.order_email" => "訂購者E-mail",
      "web_x_order.web_x_gift_content" => "贈品內容",
      "CONCAT(accept_zip, ' ', accept_city, accept_area, accept_address) as accept_address1" => "訂購者地址",
      "web_x_order.remark" => "訂購者備註",
      "web_x_order.accept_name" => "收件人",
      "web_x_order.accept_mobile" => "收件人手機",
      "web_x_order.accept_tel" => "收件人電話",
      "web_x_order.accept_city" => "寄送區域",
      "CONCAT(accept_zip, ' ', accept_city, accept_area, accept_address) as accept_address2" => "收件地址",
      "web_x_order.payment" => "付款方式",
      "web_x_freight_subject as pickupway" => "運送地點",
      "web_x_freight_subject as pickuptime" => "取貨時間",
      //"transport" => "配送方式",
      "web_x_order.accept_time" => "方便到貨時段",
      "web_x_order.invoicetype" => "發票型式",
      "web_x_order.invoicetitle" => "發票抬頭",
      "web_x_order.invoicenum" => "統一編號",
      "web_x_order.states" => "訂單狀態",
      "web_x_order.adminremark" => "訂單備註",
	  "web_xx_product.subject" => "訂單類別2",
    );
  
    $i = 0;
    $sql_field = "";
    foreach ($FieldArray as $_key=>$_value) {
      //echo $_key."：".$_value."<br />";    //欄位英文名：中文名
      $sql_field .= $_key.", ";
      $i++;
    }
    $sql_field = trim($sql_field, ", ");
    
    $j = 2;
    //$sql = "Select ".$sql_field." From web_x_order ".$list_search[sql_sub]." order by cdate desc, web_x_order_id desc ";
    $sql = "
		Select 
			".$sql_field." 
		from 
			web_x_order 
		Left Join	
			web_order web_order
		On
			web_order.web_x_order_ordernum = web_x_order.ordernum		
		Left Join
			web_product web_product
		On
			web_product.web_product_id = web_order.web_product_id	
		Left Join
			web_x_product web_x_product
		On
			web_x_product.web_x_product_id = web_product.web_x_product_id
		Left Join
			web_xx_product web_xx_product
		On
			web_xx_product.web_xx_product_id = web_x_product.web_xx_product_id	
			".$list_search[sql_sub]." 
		group by 
			web_order.web_x_order_ordernum	
		order by 
			web_x_order.web_x_order_id desc
	";
    //echo $sql;
    $rs = ConnectDB($DB, $sql);
    $allCount = mysql_num_rows($rs);
    
    //贈品
	if(0){
		$sql2 = "
			Select web_x_gift_content from web_x_order ".$list_search[sql_sub]." order by web_x_order.web_x_order_id desc";
		$rs2 = ConnectDB($DB, $sql2);
		  $giftRow = 0;
		for ($i2=0; $i2<mysql_num_rows($rs2); $i2++) {
		  $row2 = mysql_fetch_assoc($rs2);
		  if(!$row2[web_x_gift_content]) {
			continue;
		  }
		  $web_x_gift_content_ary = json_decode(str_front($row2[web_x_gift_content]), true);
			$giftRow += count($web_x_gift_content_ary);
		} 
	}	

    $allCount += $giftRow;
     
    echo "共: ".$allCount." 筆資料";
    $evnRow = 1500;
    $loopRows = ($allCount > $evnRow) ? ceil($allCount / $evnRow) : 1;
    if($allCount) {
    for($i=1; $i<=($loopRows); $i++) {
  ?>
    <tr>
      <td align="center"><a href="web_x_order_erp.php?page=<?php echo ($i); ?><?php echo $list_search[link]; ?>" title="<?php echo $search_title; ?>"><?php echo $Init_WebTitle."_訂單資料".$search_title."_".date("YmdHis")."(".($i).").xls"; ?></a></td>
    </tr>  
	<?php } } ?>
  </table>
<?php include("../includes/footer.php"); ?>
</body>
</html>