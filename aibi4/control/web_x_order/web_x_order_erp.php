<?php
	ignore_user_abort(true);
	//for ERP
	include("../includes/includes.php");
	
	require "../class/excel/Classes/PHPExcel.php";
	require "../class/excel/Classes/PHPExcel/IOFactory.php";

	include("web_x_order_serach.php");
	if ($search_title!="") $search_title = "（".$search_title."）";

	$objPHPExcel = new PHPExcel();
	
	$objPHPExcel->getProperties()
				->setCreator("Orders")
				->setLastModifiedBy("Orders")
				->setTitle("Orders")
				->setSubject("Orders")
				->setDescription("Orders")
				->setKeywords("Orders")
				->setCategory("Orders");
	
	$CellArray = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ");

	$FieldArray = array(
		"web_x_order.ordernum" => "訂單編號",
		"(select subject from web_x_class where web_x_class_id = web_x_order.store_id) as store_subject" => "分店",
		"web_x_order.order_type" => "種類",
		"web_x_order.cdate" => "訂購日期",
		//"web_order.serialnumber" => "商品編號",
		//"web_order.pincode" => "商品編號",
		"web_order.subject" => "商品名稱",
		"web_order.price" => "原價",
		"web_order.num" => "購買數",
		"web_order_id as subtotal" => "小計",
		//"web_x_order.saleAry" => "組合折扣金額",
		//"web_x_order.web_code_money" => "優惠代碼",
		//"web_x_order.web_bonus_from_money" => "紅利折扣",
		//"web_x_order.web_member_from_money" => "購物金",
		//"web_x_order.web_coupon_from_money" => "優惠券",
		//"discountTotal" => "折扣加總",
		//"web_order_id as amountofgoods" => "商品金額",
		//"web_order_id as afterdiscount" => "折扣後金額",
		//"web_x_order.web_code_subject" => "商品含稅額",
		//"web_x_order.freight" => "加收運費",
		//"web_x_order.web_x_freight_subject" => "貨到付款手續費",
		"web_x_order.total" => "訂單金額",
		//"web_x_order.web_bonus_from_money" => "使用紅利點數",
		//"(web_x_order.total - web_x_order.web_bonus_from_money)" => "實際總金額",
		"web_x_order.total as web_x_order_total" => "實際總金額",
		"web_x_order.order_name" => "會員姓名",
		"web_x_order.order_mobile" => "訂購者手機",
		//"web_x_order.order_tel" => "訂購者電話",
		//"web_x_order.order_email" => "訂購者E-mail",
		//"CONCAT(accept_zip, ' ', accept_city, accept_area, accept_address) as accept_address1" => "訂購者地址",
		//"remark" => "訂購者備註",
		//"accept_name" => "收件人",
		//"accept_mobile" => "收件人手機",
		//"accept_tel" => "收件人電話",
		//"accept_city" => "寄送區域",
		//"CONCAT(accept_zip, ' ', accept_city, accept_area, accept_address) as accept_address2" => "收件地址",
		//"payment" => "付款方式",
		//"web_x_freight_subject as pickupway" => "運送地點",
		//"web_x_freight_subject as pickuptime" => "取貨時間",
		//"transport" => "配送方式",
		//"store" => "物流廠商",
		//"CVSstatus" => "超取(不)付款",
		//"CVSStoreIDInfo" => "收件門市",
		//"CVSStoreID" => "門市店號",
		//"accept_time" => "方便到貨時段",
		//"invoicetype" => "發票型式",
		//"invoicetitle" => "發票抬頭",
		//"invoicenum" => "統一編號",
		"web_x_order.paymentstatus" => "付款狀態",
		"web_x_order.states" => "訂單狀態",
		"web_x_order.adminremark" => "訂單備註",
		"web_xx_product.subject as web_xx_product_subject" => "訂單類別2",
		//"web_x_order.successPayDate" => "付款日期",
	);
	
	$i = 0;
	$sql_field = "";
	foreach ($FieldArray as $_key=>$_value) {
		//echo $_key."：".$_value."<br />";		//欄位英文名：中文名
		if($_key != 'discountTotal' && $_key != 'CVSstatus') 
			$sql_field .= $_key.", ";
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($CellArray[$i]."1", $_value);
		$i++;
	}
	$sql_field = trim($sql_field, ", ");
	
	//贈品
	if(0){
		$sql2 = "Select web_x_gift_content, ordernum from web_x_order ".$list_search[sql_sub]." order by web_x_order.web_x_order_id desc";
		$rs2 = ConnectDB($DB, $sql2);

		for ($i2=0; $i2<mysql_num_rows($rs2); $i2++) {
		  $row2 = mysql_fetch_assoc($rs2);
		  if(!$row2[web_x_gift_content]) {
			continue;
		  }
		  $web_x_gift_content_ary[$row2[ordernum]] = json_decode(str_front($row2[web_x_gift_content]), true);
		  $web_x_gift_content_count = json_decode(str_front($row2[web_x_gift_content]), true);
		  $giftRow += count($web_x_gift_content_count);
		}
	}	
    /*
    echo "<pre>";
    print_r($web_x_gift_content_count);
    echo "</pre>";
    exit;
	*/
	$j = 2;
	$evnRow = 1500;
	//$sql = "Select ".$sql_field." From web_x_order ".$list_search[sql_sub]." order by cdate desc, web_x_order_id desc ";
	$sql = "
		Select 
			".$sql_field." 
		from 
			web_x_order 
		Left Join	
			web_order web_order
		On
			web_order.web_x_order_ordernum = web_x_order.ordernum		
		Left Join
			web_product web_product
		On
			web_product.web_product_id = web_order.web_product_id	
		Left Join
			web_x_product web_x_product
		On
			web_x_product.web_x_product_id = web_product.web_x_product_id
		Left Join
			web_xx_product web_xx_product
		On
			web_xx_product.web_xx_product_id = web_x_product.web_xx_product_id 
		".$list_search[sql_sub]." 
		group by 
			web_order.web_x_order_ordernum
		order by 
			web_x_order.web_x_order_id desc
	";
	if($page)
		$sql .= " limit ".($page-1) * ($evnRow - $giftRow).", ".($evnRow - $giftRow);	
	//echo $sql;
	$rs2 = ConnectDB($DB, $sql);
	//找贈品位置
	for ($i2=0; $i2<mysql_num_rows($rs2); $i2++) {	
		$row2 = mysql_fetch_assoc($rs2);
		$ary[$i2] = $row2[ordernum];
	}		
	
	foreach($web_x_gift_content_ary as $key => $_gift) {
		$_giftKey = array_keys($ary, $key);

		$_printKey[end($_giftKey)] = $key;
	}

	//找贈品位置
	$rs = ConnectDB($DB, $sql);


	//for($x=0; $x<=($loopRows-1); $x++) {
	$_orderIdAry = array();
	if(1) {
		//for ($i=($x*$evnRow); $i<mysql_num_rows($rs); $i++) {
		for ($i=0; $i<mysql_num_rows($rs); $i++) {	
			$row = mysql_fetch_assoc($rs);
			$k = 0;
			foreach($row as $_key=>$_value) {
				$$_key = $row[$_key];
				
				switch ($_key) {
					case "cdate":	//訂購日期
						$cdate_array = explode(" ", $$_key);
						$$_key = str_replace("-", "/", $cdate_array[0]);
						break;
					case "subtotal":	//小計
						$web_order_id = $$_key;
						
						//真正的小計
						$sql2 = "Select (price * num) as subtotal From web_order Where web_order_id = '".$web_order_id."' ";
						$rs2 = ConnectDB($DB, $sql2);
						$$_key = mysql_result($rs2, 0, "subtotal");
						
						$sql2 = "Select web_code_money, levelremark From web_x_order Where ordernum like '".$ordernum."' ";
						$rs2 = ConnectDB($DB, $sql2);
						$code_money = mysql_result($rs2, 0, "web_code_money");
						$levelremark = mysql_result($rs2, 0, "levelremark");
						
						if ($code_money>0 || $levelremark!="") {	//使用優惠代碼產生優惠金額, 會員等級折扣
							$sql2 = "Select count(*) as counter From web_order Where web_x_order_ordernum like '".$ordernum."' ";
							$rs2 = ConnectDB($DB, $sql2);
							$counter = mysql_result($rs2, 0, "counter");
							
							if ($counter==1) {
								$sql2 = "Select subtotal, web_code_money From web_x_order Where ordernum like '".$ordernum."' ";
								$rs2 = ConnectDB($DB, $sql2);
								for ($x=0; $x<mysql_num_rows($rs2); $x++) {
									$row2 = mysql_fetch_assoc($rs2);
									//改寫 小計=實際總金額-加收運費(此處不用考慮紅利)
									$$_key = $row2["subtotal"] - $row2["web_code_money"];
								}
							} else {
								//會員等級折扣
								if ($levelremark) {
									//整筆訂單中含有會員等級折扣
									$levelremark_array = explode("元打", trim($levelremark, "折"));
									$discount = $levelremark_array[1];
									/*if ($discount>0 && $discount<100) $$_key = round($$_key * $discount / 100);*/
									
									//產品詳細的各自折扣
									$sql2 = "Select discount From web_order Where web_x_order_ordernum like '".$ordernum."' and web_order_id = '".$web_order_id."' ";
									$rs2 = ConnectDB($DB, $sql2);
									if (mysql_num_rows($rs)>0) $discount = mysql_result($rs2, 0, "discount");
									
									//如果有預期外的值則設為不折扣
									if ($discount<=0 && $discount>100) $discount = 100;
									
									//小計
									$$_key = round($$_key * intval($discount) / 100);
								}
								
								//使用優惠代碼產生優惠金額
								if ($code_money>0) {
									//將優惠代碼產生優惠金額加入第一筆
									$sql2 = "Select web_order_id From web_order Where web_x_order_ordernum like '".$ordernum."' order by web_order_id asc limit 1 ";
									$rs2 = ConnectDB($DB, $sql2);
									if (mysql_result($rs2, 0, "web_order_id")==$web_order_id) {	//將折扣加入第一筆
										$$_key = $$_key - $code_money;
									}
								}
								//echo $ordernum."----".$web_order_id."----".$discount."</br>";
							
								//這裡可能有錯誤
								if ($discount<100) {
									/*
									//修正+-1元的錯誤, 補在最後一筆
									$sql2 = "Select web_order_id From web_order Where web_x_order_ordernum like '".$ordernum."' order by web_order_id desc limit 1 ";
									$rs2 = ConnectDB($DB, $sql2);
									
									if (mysql_result($rs2, 0, "web_order_id")==$web_order_id) {	//修正最後一筆
										//先算出總金額(優惠金額已在第一筆扣除, 不必理會)
										$sql3 = "Select subtotal From web_x_order Where ordernum like '".$ordernum."' ";
										$rs3 = ConnectDB($DB, $sql3);
										$web_x_order_subtotal = mysql_result($rs3, 0, "subtotal");
										
										//一一計算之前產生的金額
										$sql3 = "Select (price * num) as subtotal, discount From web_order Where web_x_order_ordernum like '".$ordernum."' and web_order_id <> '".$web_order_id."' order by web_order_id ";
										$rs3 = ConnectDB($DB, $sql3);
										for ($x=0; $x<mysql_num_rows($rs3); $x++) {
											$row3 = mysql_fetch_assoc($rs3);
											$web_x_order_tmp_subtotal += round($row3["subtotal"] * $row3["discount"] / 100);	//產品詳細的各自折扣
										}

										$$_key = $web_x_order_subtotal - $web_x_order_tmp_subtotal;
										
									}
									*/
									$sql2 = "Select web_order_id From web_order Where web_x_order_ordernum like '".$ordernum."' order by web_order_id desc";
									$rs2 = ConnectDB($DB, $sql2);
									for ($x=0; $x<mysql_num_rows($rs2); $x++) {
										$row2 = mysql_fetch_assoc($rs2);
										if(in_array($row2['web_order_id'], $_orderIdAry)) {
											continue;
										}
										//echo $row2['web_order_id']."</br>";
										$_orderIdAry[] = $row2['web_order_id'];

										$sql3 = "Select web_order_id, price, num, discount From web_order Where web_order_id like '".$row2[web_order_id]."'";
										//echo "</br>";
										
										$rs3 = ConnectDB($DB, $sql3);
										for ($x=0; $x<mysql_num_rows($rs3); $x++) {
											$row3 = mysql_fetch_assoc($rs3);
											//echo $row2["price"]."----".$row2["discount"]."</br>";
											//改寫 小計=實際總金額-加收運費(此處不用考慮紅利)
											$$_key = (($row3["price"] * $row3['num']) * $row3["discount"] / 100);
										}
											
									}
									

								}
								
								//echo $$_key."</br>";
							}
						}
						break;
					case "CVSStoreIDInfo":
						//echo $$_key."</br>";
						$_storeAry = explode('|', $$_key);
						$$_key = ($_storeAry) ? $_storeAry[0] : '';
						$objPHPExcel->getActiveSheet()->getCell($CellArray[$k].$j)->setValueExplicit('純配送', PHPExcel_Cell_DataType::TYPE_STRING);
						$k++;
						break;
					case "accept_time":
						$$_key = ($$_key=="0000-00-00") ? "" : $$_key;
						if($$_key == '皆可') {
							$$_key = 4;
						} else if($$_key == '上午（12時前）') {
							$$_key = 1;
						} else if($$_key == '下午（12~17時）') {
							$$_key = 2;
						} else if($$_key == '夜間（17~20時）') {
							$$_key = 3;
						}
						break;
					case "saleAry":
						if($saleAry){
							$saleTotal = 0;
							$_saleAry = array();
							$saleAry = json_decode($saleAry, true);
							if(count($saleAry)) {
								foreach($saleAry as $key => $sale) {
									if(!$sale['id']) {
										continue;
									}

									if($sale['salePrice']) {	
										$_saleTotal = ($sale[total] >= $sale['salePrice']) ? $sale[total] : $sale['salePrice'];
										$_saleNum = ($sale[num] >= $sale['saleRow']) ? $sale[num] : $sale['num'];
										$_saleRow = floor($sale[num] / $sale['saleRow']) ? floor($sale[num] / $sale['saleRow']) : 1;

										//$_saleAry[] = ($_saleTotal - ($sale['salePrice'] * ($_saleRow)));
										$_saleAry2 = ($_saleTotal - ($sale['salePrice'] * ($_saleRow)));
										
										$saleTotal += $_saleAry2;
									} else {
										$_saleTotal = ($sale[total] >= $sale['salePrice']) ? $sale[total] : $sale['salePrice'];
										$_saleNum = ($sale[num] >= $sale['saleRow']) ? $sale[num] : $sale['num'];
										$_saleRow = floor($sale[num] / $sale['saleRow']) ? floor($sale[num] / $sale['saleRow']) : 1;

										//$_saleAry[] = ($_saleTotal - ($sale['salePrice'] * ($_saleRow)));
										//$_saleAry2 = ($_saleTotal - ($sale['salePrice'] * ($_saleRow)));
										$_saleAry2 = $_saleTotal - round(($_saleTotal * $sale['saleDiscountnum']) / 100);
										
										$saleTotal += $_saleAry2;

									}
										
								}
								
							}	
							//$$_key = implode(',', $_saleAry);
							$$_key = (0 + $saleTotal) ? (0 + $saleTotal) : '';
						}	
						break;		
					case "web_code_money":
						$$_key = (0 + $$_key) ? (0 + $$_key) : '';
						break;		
					case "web_bonus_from_money":
						$$_key = (0 + $$_key) ? (0 + $$_key) : '';
						break;		
					case "web_member_from_money":
						$$_key = (0 + $$_key) ? (0 + $$_key) : '';
						break;
					case "web_coupon_from_money":
						$$_key = (0 + $$_key) ? (0 + $$_key) : '';
						break;	
					case "amountofgoods":	//商品金額
						
						$discountTotal = $saleAry + $web_code_money + $web_bonus_from_money + $web_member_from_money + $web_coupon_from_money;
						$discountTotal = ($discountTotal) ? $discountTotal : '';
						$objPHPExcel->getActiveSheet()->getCell($CellArray[$k].$j)->setValueExplicit($discountTotal, PHPExcel_Cell_DataType::TYPE_STRING);
						$k++;

						$web_order_id = $$_key;
						
						//真正的商品金額
						$sql2 = "Select (price * num) as subtotal From web_order Where web_order_id = '".$web_order_id."' ";
						$rs2 = ConnectDB($DB, $sql2);
						$$_key = mysql_result($rs2, 0, "subtotal");
						
						$sql2 = "Select web_code_money, levelremark From web_x_order Where ordernum like '".$ordernum."' ";
						$rs2 = ConnectDB($DB, $sql2);
						$code_money = mysql_result($rs2, 0, "web_code_money");
						$levelremark = mysql_result($rs2, 0, "levelremark");
						
						if ($code_money>0 || $levelremark!="") {	//使用優惠代碼產生優惠金額, 會員等級折扣
							$sql2 = "Select count(*) as counter From web_order Where web_x_order_ordernum like '".$ordernum."' ";
							$rs2 = ConnectDB($DB, $sql2);
							$counter = mysql_result($rs2, 0, "counter");
							
							if ($counter>1) {
								//此處列出此張訂單原價的總計
								$web_order_subtotal = 0;
								$sql2 = "Select (price * num) as web_order_subtotal From web_order Where web_x_order_ordernum like '".$ordernum."' ";
								$rs2 = ConnectDB($DB, $sql2);
								for ($x=0; $x<mysql_num_rows($rs2); $x++) {
									$row2 = mysql_fetch_assoc($rs2);
									$web_order_subtotal += $row2["web_order_subtotal"];
								}
								$$_key = $web_order_subtotal;
							} else {
								//保持原價, 不用處理
							}
						}


						/*$sql2 = "Select web_code_money, web_bonus_from_money From web_x_order Where ordernum like '".$ordernum."' ";
						$rs2 = ConnectDB($DB, $sql2);
						$code_money = mysql_result($rs2, 0, "web_code_money");
						$bonus_from_money = mysql_result($rs2, 0, "web_bonus_from_money");
						
						//使用優惠代碼產生優惠金額, 使用紅利扣除金額
						if ($code_money>0 || $bonus_from_money>0) {	//優惠金額>0 or 使用紅利扣除金額>0
							$sql2 = "Select count(*) as counter From web_order Where web_x_order_ordernum like '".$ordernum."' ";
							$rs2 = ConnectDB($DB, $sql2);
							$counter = mysql_result($rs2, 0, "counter");
							
							if ($counter>1) {
								$sql2 = "Select web_x_order.web_code_money, web_x_order.freight, web_x_order.total From web_x_order Where ordernum like '".$ordernum."' ";
								$rs2 = ConnectDB($DB, $sql2);
								for ($x=0; $x<mysql_num_rows($rs2); $x++) {
									$row2 = mysql_fetch_assoc($rs2);
									//改寫 商品金額=實際總金額-加收運費-優惠金額(不用理會紅利)
									$$_key = $row2["total"] - $row2["freight"] - $row2["web_code_money"];
								}
							} else {
								//不用處理
							}
						}*/
						break;
					case "afterdiscount":	//折扣後金額
						$$_key = "-";

						$sql2 = "Select web_code_money, levelremark From web_x_order Where ordernum like '".$ordernum."' ";
						$rs2 = ConnectDB($DB, $sql2);
						$code_money = mysql_result($rs2, 0, "web_code_money");
						$levelremark = mysql_result($rs2, 0, "levelremark");
						
						$discount = 100;
						if ($levelremark) {
							$levelremark_array = explode("元打", trim($levelremark, "折"));
							$discount = intval($levelremark_array[1]);
							if ($discount<=0 && $discount>=100) $discount = 100;
						}

						$sql2 = "Select web_code_money From web_x_order Where ordernum like '".$ordernum."' ";
						$rs2 = ConnectDB($DB, $sql2);
						$code_money = mysql_result($rs2, 0, "web_code_money");
						
						if ($code_money>0 || $discount<100) {	//使用優惠代碼產生優惠金額, 會員等級折扣
							$sql2 = "Select freight, total From web_x_order Where ordernum like '".$ordernum."' ";
							$rs2 = ConnectDB($DB, $sql2);
							for ($x=0; $x<mysql_num_rows($rs2); $x++) {
								$row2 = mysql_fetch_assoc($rs2);
								//改寫 小計=實際總金額-加收運費(此處不用考慮紅利)
								$$_key = $row2["total"] - $row2["freight"];
							}
						}
						break;
					case "web_code_subject":	//商品含稅額 - 忽略
						$$_key = "-";
						break;
					case "freight":		//加收運費
						$$_key = ($$_key==0) ? "免運費" : $$_key;
						break;
					case "web_x_freight_subject":	//貨到付款手續費 - 忽略
						$$_key = "無";
						break;
					case "web_bonus_from_money":	//使用紅利點數
						$$_key = ($$_key==0) ? "無" : $$_key;
						break;
					case "accept_city":	//寄送區域 - 忽略
						$$_key = "國內";
						break;
					case "pickuptime":	//取貨時間 - 忽略
						$$_key = "";
						break;
					case "order_type":	//寄送區域 - 忽略
						switch($$_key) {
							case '0' :
							  $$_key = "購買";
							  break;
							case '1' :
							  $$_key = "票券使用";
							  break;
							case '2' :
							  $$_key = "儲值使用";
							  break;
									
						}
						break;	
					default:
						break;
				}
				
				//輸出
				switch ($_key) {
					case "cdate":
						$objPHPExcel->getActiveSheet()->setCellValue($CellArray[$k].$j, $$_key);
						$objPHPExcel->getActiveSheet()->getStyle($CellArray[$k].$j)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);
						break;
					default:
						$objPHPExcel->getActiveSheet()->getCell($CellArray[$k].$j)->setValueExplicit($$_key, PHPExcel_Cell_DataType::TYPE_STRING);
				}
				
				$k++;
			}	
			$j++;
			if($_printKey[$i]) {
				foreach($web_x_gift_content_ary[$_printKey[$i]] as $key => $gift) {
					$stock = ($gift[stock]) ? 1 : 0;   //此為贈品庫存
					$objPHPExcel->getActiveSheet()->getCell($CellArray[0].$j)->setValueExplicit($ordernum, PHPExcel_Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->getCell($CellArray[1].$j)->setValueExplicit($cdate, PHPExcel_Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->getCell($CellArray[2].$j)->setValueExplicit($gift[pincode], PHPExcel_Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->getCell($CellArray[3].$j)->setValueExplicit("贈 ".$gift[subject], PHPExcel_Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->getCell($CellArray[4].$j)->setValueExplicit(0, PHPExcel_Cell_DataType::TYPE_STRING);
					//$objPHPExcel->getActiveSheet()->getCell($CellArray[5].$j)->setValueExplicit($gift[stock], PHPExcel_Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->getCell($CellArray[5].$j)->setValueExplicit($stock, PHPExcel_Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->getCell($CellArray[6].$j)->setValueExplicit(0, PHPExcel_Cell_DataType::TYPE_STRING);
					$j++;
				}
			}	
		}
		
			
	}
	//exit;
	$objPHPExcel->getActiveSheet()->setTitle("訂單資料");
	$objPHPExcel->setActiveSheetIndex(0);
	
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="'.iconv("UTF-8", "Big5", $Init_WebTitle."_訂單資料".$search_title)."_".date("YmdHis").'('.$page.').xls"');
	header('Cache-Control: max-age=0');
	
	//}
	try {
		//for($x=1; $i<=2; $i++) {
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		//echo PHPExcel_Shared_File::sys_get_temp_dir(); 
		//echo $objWriter->getTempDir();
		//$objWriter->save('test.xls'); 
		$objWriter->save('php://output');
		//}
	} catch(Exception $e) {
		echo $e->getMessage();
	}
	
	
	//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	//$objWriter->save('php://output'); 
	//$objWriter->save('test.xls');
	//echo '../../uploadfiles/'.iconv("UTF-8", "Big5", $Init_WebTitle."_訂單資料".$search_title)."_".date("YmdHis").'.xls';
	//phpinfo();

	exit;	
?>