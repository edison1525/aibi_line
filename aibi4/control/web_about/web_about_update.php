<?php
	include("../includes/includes.php");
	
	$action = $_POST["action"] ? $_POST["action"] : $_GET["action"];

	//新增編輯
	if ($action=="Edit") {
		
		$FileAction = "Update";
		require("../includes/files.php");
		
		//$debug = true;
		$TableName = "web_about";
		$AccurateAction = "Get";
		require("../includes/accurate.php");
		
		$subject = ($_POST["subject"]);	//分類名稱
		if ($subject=="") RunAlert("請輸入標題");
		
		$web_about_id = intval($_POST["web_about_id"]);	//分類名稱

		asort($_REQUEST[picAsort]); //排序
		if($_REQUEST[picAsort]) {
			$_picAsort = array();
			$i = 0;
			foreach($_REQUEST[picAsort] as $fileName => $key) {

				if($_REQUEST["DeleteFiles_".$i] != $fileName)  //勾選刪除不在排序內
					$_picAsort[$i] = $fileName;
				
				$i++;
			}
			ksort($_picAsort);
			$_picAsort = implode("/", $_picAsort);
			$Files = ($_REQUEST[picAsort]) ? $_picAsort : $Files;
		}
	
		if ($web_about_id > 0) {
			//$debug = true;
			$AccurateAction = "Update";
			require("../includes/accurate.php");
		} else {
			$AccurateAction = "Insert";
			require("../includes/accurate.php");
		}
	} 
	
	//刪除
	if ($action=="Delete") {
		$DeleteBox = $_POST["DeleteBox"];
		if (!is_array($DeleteBox)) die();
		
		foreach ($DeleteBox as $value) {
			$sql = "Delete From web_about Where web_about_id = '".$value."' ";
			$rs = ConnectDB($DB, $sql);
		}
		
		$sql = "Select count(*) as counter From web_about ";
		$rs = ConnectDB($DB, $sql);
		if (mysql_result($rs, 0, "counter")==0) {
			$sql = "Truncate table web_about";
			$rs = ConnectDB($DB, $sql);
		}
	}
	
	RunJs("web_about_list.php?page=".intval($_POST["page"])."&field=".$_POST["field"]."&keyword=".urlencode($_POST["keyword"]));
?>