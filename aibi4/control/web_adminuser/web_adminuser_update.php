<?php
	include("../includes/includes.php");

	$action = $_POST["action"] ? $_POST["action"] : $_GET["action"];

	//新增編輯
	if ($action=="Edit") {

		$web_adminuser_id = intval($_POST["web_adminuser_id"]);	//分類名稱
		//$cdate = date("Y-m-d");	//發表日期
		//$edate = date("Y-m-d");	//結束時間
		$subject = ($_POST["subject"]);	//分類名稱
		$loginID = $_POST["loginID"];
		$email = $_POST["email"];
		$web_permission_id = intval($_POST["web_permission_id"]);
		$level = getLevel($web_permission_id);
		$_POST['level'] = $level['pid'];
		$_POST["func"] = implode(',', $_POST['func']);
		$func = $_POST['func'];
		
		$export = $_POST['export'];
		$editor = $_POST['editor'];
		
		if ($web_adminuser_id > 0) {
			$ForbidData = array();
			$ForbidData[] = "loginID";	//不更新的欄位
				
			$old_password = str_filter($_POST["old_password"]);	//舊密碼
			$password = str_filter($_POST["password"]);			//新密碼
			$chk_password = str_filter($_POST["chk_password"]);	//確認新密碼

			//if ($old_password!="" || $password!="" || $chk_password!="") {
			if ($password!="" || $chk_password!="") {	
				//if ($old_password=="") RunAlert("請輸入舊密碼");
				if ($password=="") RunAlert("請輸入新密碼");
				if ($chk_password=="") RunAlert("請輸入確認新密碼");
				//if ($old_password==$password) RunAlert("新密碼不得和舊密碼相同");
				if (strlen($password)<4 || !preg_match("/^[a-zA-Z0-9]*$/", $password)) RunAlert("新密碼須為4到20個英數字");
				if ($password!=$chk_password) RunAlert("新密碼與確認新密碼不符");
				/*
				$old_password = md5($old_password);
				$sql = "SELECT web_adminuser_id FROM web_adminuser WHERE web_adminuser_id = '".$web_adminuser_id."' and password = '".$old_password."' ";
				$rs = ConnectDB($DB, $sql);
				if (mysql_num_rows($rs)==0) {
					RunAlert("舊密碼錯誤，請重新輸入正確的密碼");
				} else {
					$password = md5($password);
				}
				*/
				$password = md5($password);
			} else {
				$ForbidData[] = "password";	//不更新的欄位
			}
			if ($email!="" && !preg_match("/^[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/", $email)) RunAlert("請輸入正確的電子信箱");
			
			if ($_SESSION["Member"]["ID"] == $web_adminuser_id) {
				
				$ForbidData[] = "func";	//不更新的欄位
				$ForbidData[] = "web_permission_id";	//不更新的欄位
				$ForbidData[] = "level";	//不更新的欄位
				
			} else {
			
				if ($func=="") RunAlert("請輸入權限設定");
				if ($web_permission_id=="") RunAlert("請輸入身份設定");
				//level
				$levelInfo = getLevel($web_permission_id);
				$level = ($levelInfo[pid]) ? $levelInfo[pid] : '999';    //999為有錯
			}	
			
			
		} else {
			$password = str_filter($_POST["password"]);			//密碼
			$chk_password = str_filter($_POST["chk_password"]);	//確認密碼

			//檢查帳號
			if ($loginID=="") RunAlert("請輸入帳號");
			$sql = "Select count(*) as counter From web_adminuser Where loginID like '".$loginID."' ";
			$rs = ConnectDB($DB, $sql);
			if (mysql_result($rs, 0, "counter")>0) RunAlert("此帳號已被註冊過，請重新輸入");
			
			//密碼
			if (strlen($password)<4 || !preg_match("/^[a-zA-Z0-9]*$/", $password)) RunAlert("密碼請輸入4~20個數字或英文字母");
			if ($password!=$chk_password) RunAlert("密碼與確認密碼不符");
			$password = md5($password);
		}
		if ($email!="" && !preg_match("/^[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/", $email)) RunAlert("請輸入正確的電子信箱");
		if ($_SESSION["Member"]["ID"] == $web_adminuser_id) {
				
			$ForbidData[] = "func";	//不更新的欄位
			$ForbidData[] = "web_permission_id";	//不更新的欄位
			$ForbidData[] = "level";	//不更新的欄位
			
		} else {
		
			if ($func=="") RunAlert("請輸入權限設定");
			if ($web_permission_id=="") RunAlert("請輸入身份設定");
			//level
			$levelInfo = getLevel($web_permission_id);
			$level = ($levelInfo[pid]) ? $levelInfo[pid] : '999';    //999為有錯
		}
			
		$_POST['password'] = $password;
		
		$_POST['export'] = $export;
		$_POST['editor'] = $editor;
	
		//$debug = true;
		$TableName = "web_adminuser";
		$AccurateAction = "Get";
		require("../includes/accurate.php");
		
		//$debug = true;		
		if ($web_adminuser_id>0) {
			/*
			echo "<pre>";
			print_r($_REQUEST);
			echo "</pre>";
			$debug = true;
			*/
			$AccurateAction = "Update";
			require("../includes/accurate.php");
		} else {
			$AccurateAction = "Insert";
			require("../includes/accurate.php");
		}
	} 
	
	//刪除
	if ($action=="Delete") {
		$DeleteBox = $_POST["DeleteBox"];
		
		if(!count($DeleteBox)) {
			die();
		}	
		
		foreach ($DeleteBox as $value) {
			$sql = "Delete From web_adminuser Where web_adminuser_id = '".$value."' ";
			$rs = ConnectDB($DB, $sql);
		}
	}
	
	RunJs("web_adminuser_list.php?page=".intval($_POST["page"])."&field=".$_POST["field"]."&keyword=".urlencode($_POST["keyword"]));
?>