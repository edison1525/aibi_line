<?php include("../includes/head.php"); ?>
<?php
	$web_adminuser_id = intval($_GET["web_adminuser_id"]);

	$sql = "Select * From web_adminuser Where web_adminuser_id = '".$web_adminuser_id."' ";
	$rs = ConnectDB($DB, $sql);
	if (mysqli_num_rows($rs)==0) {
		$action = "Add";
		
		$level = 0;	//身分
		$old_func = array();
	} else {
		$action = "Edit";
		for ($i=0; $i<mysqli_num_rows($rs); $i++) {
			$row = mysqli_fetch_assoc($rs);
			foreach($row as $_key=>$_value) $$_key = str_edit($row[$_key]);
			$old_func = explode(",", $row["func"]);	//權限設定
		}
	}
	
?>
<script language="javascript">
<!--
function chkform() {
	var msg = "";
	var x = 0;
<?php if ($action=="Add") { ?>
	if (document.form.loginID.value == "") { msg = msg + "帳號\n"; }
	if (document.form.password.value.length < 4 || document.form.password.value.search(/^[a-zA-Z0-9]*$/)) { msg = msg + "密碼須為4~20個數字或英文字母\n"; }
	if (document.form.chk_password.value == "") { msg = msg + "確認密碼\n"; }
	if (document.form.password.value != document.form.chk_password.value) { msg = msg + "密碼與確認密碼不符\n"; }
<?php } else { ?>
	//if (document.form.old_password.value != "" || document.form.password.value != "" || document.form.chk_password.value != "") { 
	if (document.form.password.value != "" || document.form.chk_password.value != "") {
		//if (document.form.old_password.value == "") { msg = msg + "舊密碼\n"; }
		if (document.form.password.value.length < 4 || document.form.password.value.search(/^[a-zA-Z0-9]*$/)) { msg = msg + "新密碼須為4~20個數字或英文字母\n"; }
		if (document.form.chk_password.value == "") { msg = msg + "確認新密碼\n"; }
		//if (document.form.old_password.value == document.form.password.value) { msg = msg + "新密碼不得和舊密碼相同\n"; }
		if (document.form.password.value != document.form.chk_password.value) { msg = msg + "新密碼與確認新密碼不符\n"; }
	}
<?php } ?>
	if (document.form.uname.value == "") { msg = msg + "姓名\n"; }
	if (document.form.email.value != "" && document.form.email.value.search(/^[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/)) { msg = msg + "正確的電子信箱\n"; }
	var func = document.form.elements["func[]"];
	var func_msg = "權限設定\n";
	for (x = 0; x < func.length; x++) {
		if (func[x].checked) func_msg = "";
	}
	msg = msg + func_msg;
	
	if (msg!="") {
		alert("請輸入以下欄位\n\n" + msg);
		return false;
	}
	return true;
}
//-->
</script>
</head>

<body>
<?php 
	//include("../includes/left.php");
	include("../multi-menu/left.php"); 
?>
<h1>管理員</h1>
<div id="nav">目前位置：<?php echo $nav_title; ?> > <a href="web_adminuser_list.php">管理員</a> > <?php echo ($action=="Add") ? "新增" : "編輯"; ?></div>

<form name="form" method="post" action="web_adminuser_update.php">
  <table class="Edit_form">
    <!--<tr>
      <th>身分：<span class="star">*</span></th>
      <td>
        <input type="radio" name="level" value="9" <?php if ($level==9) echo "checked=\"checked\""; ?>/>主管
        <input type="radio" name="level" value="0" <?php if ($level==0) echo "checked=\"checked\""; ?>/>職員
      </td>
    </tr>-->
    <?php if ($action == "Add") { ?>
    <tr>
      <th>帳號：<span class="star">*</span></th>
      <td><input type="text" name="loginID" value="<?php echo $loginID; ?>" size="20" maxlength="20" onkeyup="value=value.replace(/[^a-zA-Z0-9]/g,'')" onafterpaste="value=value.replace(/[^a-zA-Z0-9]/g,'')" /></td>
    </tr>
    <tr>
      <th>密碼：</th>
      <td><input type="password" name="password" value="<?php echo $password; ?>" size="20" maxlength="20" /> 請輸入4~20個數字或英文字母</td>
    </tr>
    <tr>
      <th>確認密碼：</th>
      <td><input type="password" name="chk_password" value="<?php echo $chk_password; ?>" size="20" maxlength="20" /> 請輸入4~20個數字或英文字母</td>
    </tr>
    <?php } else { ?>
    <tr>
      <th>帳號：</th>
      <td><?php echo $loginID; ?></td>
    </tr>
    <tr style="display:none;">
      <th>舊密碼：</th>
      <td><input type="password" name="old_password" value="<?php echo $old_password; ?>" size="20" maxlength="20" /></td>
    </tr>
    <tr>
      <th>新密碼：</th>
      <td><input type="password" name="password" value="" size="20" maxlength="20" /> 請輸入4~20個數字或英文字母</td>
    </tr>
    <tr>
      <th>確認新密碼：</th>
      <td><input type="password" name="chk_password" value="" size="20" maxlength="20" /> 請輸入4~20個數字或英文字母</td>
    </tr>
	<?php } ?>
    <tr>
      <th>姓名：<span class="star">*</span></th>
      <td><input type="text" name="uname" value="<?php echo $uname; ?>" size="20" maxlength="20" /></td>
    </tr>
    <tr>
      <th>電子信箱：</th>
      <td><input type="text" name="email" value="<?php echo $email; ?>" class="fill" maxlength="250" /></td>
    </tr>
<?php
	if($_SESSION["Member"]["ID"] != $web_adminuser_id || $_SESSION["Member"]["ID"] == '1') {
?>
	<tr style="display:;">
	  <th>身份：<span class="star">*</span></th>
	  <td>
	  	<select name="web_permission_id">
		<?php
			$sql = "Select * From web_permission order by web_permission_id ASC";
			$rs = ConnectDB($DB, $sql);
			for ($i=0; $i<mysql_num_rows($rs); $i++) { 
				$row = mysql_fetch_assoc($rs);
				
				if($loginUserLevelInfo[pid] <= 1) {
					if($loginUserLevelInfo[web_permission_id] > $row[pid]) {	
						continue;
					}
				}	else {
					if($loginUserLevelInfo[web_permission_id] != $row[pid]) {	
						continue;
					}
				}
				
				echo "<option value=\"".$row["web_permission_id"]."\"";
				if ($web_permission_id==$row["web_permission_id"]) echo " selected";
				
				echo ">".$row["subject"]."</option>\n";
			}
		?>
		</select>
	  </td>
	</tr>
	<tr>
      <th>所屬分店：<span class="star">*</span></th>
	  <td>
<?php
	$adimiStoreSql = "SELECT store_id FROM `web_adminuser` WHERE web_adminuser_id = '".$_SESSION['Member']['ID']."'";
	$adimiStoreRs = ConnectDB($DB, $adimiStoreSql); 
	$adimiStore = mysql_result($adimiStoreRs, 0, "store_id");
?>	  
		<select name="store_id">
	<?php
		if($adimiStore == '-1') {
	?>		
			<option value="-1">全部</option>
	<?php
		}
	?>	
<?php	
	$storeSql = "SELECT subject, web_x_class_id FROM `web_x_class` WHERE ifShow = '1' ORDER BY asort ASC";
	$storeRs = ConnectDB($DB, $storeSql); 
	for ($i=0; $i<mysql_num_rows($storeRs); $i++) {
		$stroeRow = mysql_fetch_assoc($storeRs);
		if($adimiStore != '-1') {
			if($stroeRow['web_x_class_id'] != $adimiStore) {
				continue;
			}
		}	
		$selected = ($stroeRow['web_x_class_id'] == $store_id) ? 'selected="selected"' : null;
?>		
			<option value="<?php echo $stroeRow['web_x_class_id']; ?>" <?php echo $selected; ?>><?php echo $stroeRow['subject']; ?></option>
<?php
	}
?>	
		</select>
	  </td>
	</tr>	
    <tr>
      <th>權限設定：<span class="star">*</span></th>
      <td><input type="checkbox" name="all" onClick="CheckAll(this, 'func[]')" />全選
      <table class="Border_form">
		<?php
			$web_worktree_list = "";
			$sql = "SELECT DISTINCT kinds FROM web_worktree ORDER BY gsort ";
			$rs = ConnectDB($DB, $sql);
			for ($i=0; $i<mysqli_num_rows($rs); $i++) {
				$row = mysqli_fetch_assoc($rs);
				$first = false;
		
				$sql2 = "SELECT * FROM web_worktree WHERE kinds like '".$row["kinds"]."' and ifShow = '1' ORDER BY asort";
				$rs2 = ConnectDB($DB, $sql2);
				for ($j=0; $j<mysql_num_rows($rs2); $j++) {
					$row2 = mysql_fetch_assoc($rs2);
					
					if(!in_array($row2["web_worktree_id"], $loginUserFuncAry)) {
						continue;
					}
					
					if (!$first) {
						$web_worktree_list .= "<tr><td width=\"20%\" align=\"center\"><span class=\"font_orange\">".$row["kinds"]."</span></td><td>";
						$first = true;
					}
					
					$web_worktree_list .= "<input type=\"checkbox\" name=\"func[]\" value=\"".$row2["web_worktree_id"]."\"";
					foreach ($old_func as $value) if ($value==$row2["web_worktree_id"]) $web_worktree_list .= " checked=\"checked\" ";
					$web_worktree_list .= "/> ".$row2["subject"]."<br />";
				}
				if ($first) $web_worktree_list .= "</td></tr>";
			}
			echo $web_worktree_list;
		?>
		<tr>
			<td width="20%" align="center">
				<span class="font_orange">會員資料編輯</span>
			</td>
			<td>
				<input type="checkbox" name="editor" value="1" <?php if($editor) echo "checked=\"checked\""; ?>>會員資料編輯<br>
			</td>
		</tr>
		<tr>
			<td width="20%" align="center">
				<span class="font_orange">匯出功能</span>
			</td>
			<td>
				<input type="checkbox" name="export" value="1" <?php if($export) echo "checked=\"checked\""; ?>>匯出功能<br>
			</td>
		</tr>
      </table>
      </td>
    </tr>
<?php
	}
?>	
  </table>
  <div class="btn">
    <input name="submit" type="submit" value="確定送出" onClick="return chkform();" />
    <input name="button" type="button" value="回上一頁" onClick="history.go(-1);" />
    <input type="hidden" name="action" value="Edit" />
    <input type="hidden" name="web_adminuser_id" value="<?php echo $web_adminuser_id; ?>" />
    <input type="hidden" name="page" value="<?php echo (intval($_GET["page"])==0) ? 1 : intval($_GET["page"]); ?>" />
    <input type="hidden" name="field" value="<?php echo str_filter($_GET["field"]); ?>" />
    <input type="hidden" name="keyword" value="<?php echo str_filter($_GET["keyword"]); ?>" />
  </div>
</form>
<?php include("../includes/footer.php"); ?>
</body>
</html>