<?php
	include("../includes/includes.php");
	
	$action = $_POST["action"] ? $_POST["action"] : $_GET["action"];

	//新增編輯
	if ($action=="Edit") {
		
		//$debug = true;
		$TableName = "web_x_tag";
		$AccurateAction = "Get";
		require("../includes/accurate.php");
		foreach($_POST as $_key => $_value) $$_key = str_front($_POST[$_key]);
		if ($subject=="") RunAlert("請輸入標題");
		$CoverAction = "Update";
		require("../includes/cover.php");
		$FileAction = "Update";
		require("../includes/files.php");
		if ($web_x_tag_id>0) {
			$AccurateAction = "Update";
			require("../includes/accurate.php");
		} else {
			$AccurateAction = "Insert";
			require("../includes/accurate.php");
		}
	} 
	
	//刪除
	if ($action=="Delete") {
		$DeleteBox = $_POST["DeleteBox"];
		if (!is_array($DeleteBox)) die();
		
		foreach ($DeleteBox as $value) {

			$sql = "Select Covers From web_x_tag Where web_x_tag_id = '".$value."' ";
			$rs = ConnectDB($DB, $sql);
			$CoverAction = "Del";
			require("../includes/cover.php");

			$sql = "Select Files From web_x_tag Where web_x_tag_id = '".$value."' ";
			$rs = ConnectDB($DB, $sql);
			$FileAction = "Del";
			require("../includes/files.php");

			$sql = "Delete From web_x_tag Where web_x_tag_id = '".$value."' ";
			$rs = ConnectDB($DB, $sql);
		}
		
		$sql = "Select count(*) as counter From web_x_tag ";
		$rs = ConnectDB($DB, $sql);
		if (mysql_result($rs, 0, "counter")==0) {
			$sql = "Truncate table web_x_tag";
			$rs = ConnectDB($DB, $sql);
		}
	}
	
	RunJs("web_x_tag_list.php?page=".intval($_POST["page"])."&field=".$_POST["field"]."&keyword=".urlencode($_POST["keyword"]));
?>