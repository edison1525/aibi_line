<?php include("../includes/head.php"); ?>
</head>

<body>
<?php 
  //include("../includes/left.php");
  include("../multi-menu/left.php"); 
?>
<h1>標籤分類</h1>
<div id="nav">目前位置：<?php echo $nav_title; ?> > <a href="web_x_tag_list.php">標籤分類</a> > 列表</div>

<!--搜尋開始-->
<form name="formSearch" method="post" action="web_x_tag_list.php">
  <div id="search">
	<?php $list_search = list_search(array("subject" => "標題")); ?>
    <div id="new" style="display:"><a href="web_x_tag_edit.php">新增資料</a></div>
  </div>
</form>
<!--搜尋結束-->

<form name="form" method="post" action="web_x_tag_update.php">
  <table class="List_form">
    <tr>
      <th width="5%"><input type="checkbox" name="all" onClick="CheckAll(this, 'DeleteBox[]')" /></th>
	  <th width="10%">是否顯示</th>
	  <th width="10%">排序</th>
	  <th>標題</th>
    <!--<th width="15%">導覽列圖片</th>-->
    <th width="15%">項目</th>
    <th width="5%"><a class="edit">改</a></th>
    </tr>
	  
	<?php
        $sql = "Select SQL_CALC_FOUND_ROWS web_x_tag_id, ifShow, asort, subject, Covers, Files, (Select count(*) From web_tag Where web_x_tag_id = web_x_tag.web_x_tag_id) as counter From web_x_tag ".$list_search[sql_sub]." order by asort asc, web_x_tag_id desc ";
        $list_paging = list_paging_sql($page, $Init_ControlPage, $sql);
		$sql .= " limit ".($page-1) * $Init_ControlPage.", ".$Init_ControlPage;
        $rs = ConnectDB($DB, $sql);
		//$list_paging = list_paging($page, $Init_ControlPage);
		for ($i=0; $i<mysql_num_rows($rs); $i++) {
			$row = mysql_fetch_assoc($rs);
			foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
      $pic_cover = ShowPic($Covers, "../../uploadfiles/s/", "../../uploadfiles/no_image.jpg");
      $pic = ShowPic($Files, "../../uploadfiles/s/", "../../uploadfiles/no_image.jpg");

      $sql2 = "Select count(*) as counter From web_tag Where web_x_tag_id = '".$web_x_tag_id."' ";
      $rs2 = ConnectDB($DB, $sql2);
      $counter = mysql_result($rs2, 0, "counter");
    ?>
    <tr>
      <td align="center">
		<?php if ($counter==0 && !in_array($web_x_tag_id, array(3,4))) { ?>
		<input type="checkbox" name="DeleteBox[]" value="<?php echo $web_x_tag_id; ?>" />
		<?php } else { echo "X"; } ?>
	  </td>
  	  <td align="center"><?php echo ($ifShow==1) ? "是" : "否"; ?></td>
  	  <td align="center"><?php echo $asort; ?></td>
  	  <td><?php echo $subject; ?></td>
      <!--<td align="center"><img src="<?php echo $pic_cover; ?>" width="80" /></td>-->
  	  <td align="center"><a href="../web_tag/web_tag_list.php?web_x_tag_id=<?php echo $row["web_x_tag_id"]; ?>"><!--<img src="<?php echo $pic; ?>" width="80" /><br />--><?php echo $counter; ?> 個項目</a></td>
      <td align="center"><a href="web_x_tag_edit.php?web_x_tag_id=<?php echo $web_x_tag_id."&page=".$page.$list_search[link]; ?>" class="edit">改</a></td>
    </tr>
	<?php } ?>
  </table>
  <div id="delete"> 
    <input type="submit" name="submit" value="刪除" onClick="return CheckDel();" />
  </div>
  <?php list_page("web_x_page_list.php", $page, $list_paging[pages], $list_paging[records], $list_search[hidden], $list_search[link]); ?>
  <input type="hidden" name="action" value="Delete" />
</form>
<?php include("../includes/footer.php"); ?>
</body>
</html>