<?php include("../includes/head.php"); ?>
</head>

<body>
<?php 
  //include("../includes/left.php");
  include("../multi-menu/left.php"); 
?>
<h1>標籤</h1>
<div id="nav">目前位置：<?php echo $nav_title; ?> > <a href="web_tag_list.php">標籤</a> > 列表</div>

<!--搜尋開始-->
<form name="formSearch" method="post" action="web_tag_list.php">
  <div id="search">
	<?php $list_search = list_search(array("subject" => "標題")); ?>
    <div id="new"><a href="web_tag_edit.php">新增資料</a></div>
  </div>
</form>
<!--搜尋結束-->

<form name="form" method="post" action="web_tag_update.php">
  <table class="List_form">
    <tr>
      <th width="5%"><input type="checkbox" name="all" onClick="CheckAll(this, 'DeleteBox[]')" /></th>
	  <th width="10%">是否顯示</th>
	  <th width="10%">排序</th>
	  <th>標題</th>
      <th width="10%">瀏覽人數</th>
      <th width="5%"><a class="edit">改</a></th>
    </tr>
	<?php
        $sql = "Select SQL_CALC_FOUND_ROWS web_tag_id, ifShow, asort, subject, hits From web_tag ".$list_search[sql_sub]." order by asort asc, web_tag_id desc ";
        $list_paging = list_paging_sql($page, $Init_ControlPage, $sql);
		$sql .= " limit ".($page-1) * $Init_ControlPage.", ".$Init_ControlPage;
        $rs = ConnectDB($DB, $sql);
		//$list_paging = list_paging($page, $Init_ControlPage);
		for ($i=0; $i<mysql_num_rows($rs); $i++) {
			$row = mysql_fetch_assoc($rs);
			foreach($row as $_key=>$_value) $$_key = str_front($row[$_key]);
    ?>
    <tr>
      <td align="center"><input type="checkbox" name="DeleteBox[]" value="<?php echo $web_tag_id; ?>" /></td>
	  <td align="center"><?php echo ($ifShow==1) ? "是" : "否"; ?></td>
	  <td align="center"><?php echo $asort; ?></td>
	  <td><?php echo $subject; ?></td>
      <td align="center"><?php echo number_format($hits); ?></td>
      <td align="center"><a href="web_tag_edit.php?web_tag_id=<?php echo $web_tag_id."&page=".$page.$list_search[link]; ?>" class="edit">改</a></td>
    </tr>
	<?php } ?>
  </table>
  <div id="delete"> 
    <input type="submit" name="submit" value="刪除" onClick="return CheckDel();" />
  </div>
  <?php list_page("web_tag_list.php", $page, $list_paging[pages], $list_paging[records], $list_search[hidden], $list_search[link]); ?>
  <input type="hidden" name="action" value="Delete" />
</form>
<?php include("../includes/footer.php"); ?>
</body>
</html>