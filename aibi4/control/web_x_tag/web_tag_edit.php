<?php include("../includes/head.php"); ?>
<?php
	$web_tag_id = intval($_GET["web_tag_id"]);
	$sql = "Select * From web_tag Where web_tag_id = '".$web_tag_id."' ";
	$rs = ConnectDB($DB, $sql);
	if (mysql_num_rows($rs)==0) {
		$action = "Add";
		$ifShow = 1;	//是否顯示
		$asort = 1;	//排序
		$colorCode = '#000000';
		$colorCode2 = '#000000';

		$sql = "Select asort From web_tag order by asort desc limit 1 ";
		$rs = ConnectDB($DB, $sql);
		if (mysql_num_rows($rs)>0) $asort += mysql_result($rs, 0, "asort");

	} else {
		$action = "Edit";
		for ($i=0; $i<mysql_num_rows($rs); $i++) {
			$row = mysql_fetch_assoc($rs);
			foreach($row as $_key=>$_value) $$_key = str_edit($row[$_key]);
			$colorCode = ($colorCode) ? $colorCode : '#000000';
			$colorCode2 = ($colorCode2) ? $colorCode2 : '#000000';
		}
	}
?>
<script language="javascript">
<!--
function chkform() {
	var msg = "";
	if (document.form.subject.value == "") { msg = msg + "標題\n"; }
	
	if (msg!="") {
		alert("請輸入以下欄位\n\n" + msg);
		return false;
	}
	return true;
}
//-->
</script>
</head>

<body>
<?php 
	//include("../includes/left.php");
	include("../multi-menu/left.php"); 
?>
<h1>標籤</h1>
<div id="nav">目前位置：<?php echo $nav_title; ?> > <a href="web_tag_list.php">標籤</a> > <?php echo ($action=="Add") ? "新增" : "編輯"; ?></div>

<form name="form" method="post" action="web_tag_update.php" enctype="multipart/form-data">
  <table class="Edit_form">
	<tr>
	  <th>是否顯示：<span class="star">*</span></th>
	  <td>
		<input type="radio" name="ifShow" value="1" <?php if ($ifShow=="1") echo "checked=\"checked\""; ?>/>是
		<input type="radio" name="ifShow" value="0" <?php if ($ifShow=="0") echo "checked=\"checked\""; ?>/>否
	  </td>
	</tr>
	<tr>
	  <th>排序：<span class="star">*</span></th>
	  <td><input type="text" name="asort" value="<?php echo $asort; ?>" size="10" maxlength="5" /> 請輸入數字</td>
	</tr>
	<tr>
	  <th>標題：<span class="star">*</span></th>
	  <td><input type="text" name="subject" value="<?php echo $subject; ?>" class="fill" maxlength="100" /></td>
	</tr>
	<tr>
	  <th>顏色：<span class="star">*</span></th>
	  <td><input type="text" name="colorCode" class="colorCode" value="<?php echo $colorCode; ?>" size="10" maxlength="10" /></td>
	</tr>
	<tr>
	  <th>顏色2：<span class="star">*</span></th>
	  <td><input type="text" name="colorCode2" class="colorCode" value="<?php echo $colorCode2; ?>" size="10" maxlength="10" /></td>
	</tr>
  </table>
  <div class="btn">
    <input name="submit" type="submit" value="確定送出" onClick="return chkform();" />
    <input name="button" type="button" value="回上一頁" onClick="history.go(-1);" />
    <input type="hidden" name="action" value="Edit" />
    <input type="hidden" name="web_tag_id" value="<?php echo $web_tag_id; ?>" />
    <input type="hidden" name="page" value="<?php echo (intval($_GET["page"])==0) ? 1 : intval($_GET["page"]); ?>" />
    <input type="hidden" name="field" value="<?php echo str_filter($_GET["field"]); ?>" />
    <input type="hidden" name="keyword" value="<?php echo str_filter($_GET["keyword"]); ?>" />
  </div>
</form>
<?php include("../includes/footer.php"); ?>
</body>
</html>