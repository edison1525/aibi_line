<link href="./login/css/bootstrap.min.css" rel="stylesheet">
<link href="./login/css/signin.css" rel="stylesheet">
<div class="signin">
	<div class="signin-head">
		<div class="signin_title"><?php echo $Init_WebTitle; ?><br>後台管理系統</div>
	</div>
	<form name="form" class="form-signin" role="form" action="index.php" method="POST">
		<input type="text" name="username" id="username" value="" onkeyup="value=value.replace(/[^a-zA-Z0-9]/g,'')" onafterpaste="value=value.replace(/[^a-zA-Z0-9]/g,'')" class="form-control" placeholder="登入帳號" required autofocus />
		<input type="password" name="password" id="password" class="form-control" placeholder="登入密碼" required />
		<input name="action" type="hidden" value="Login">
		<div id='html_element'></div>
		<button class="btn btn-lg btn-warning btn-block" type="submit" onClick="return chkform();">登入</button>
	</form>
</div>
<script>
	var onloadCallback = function() {
		grecaptcha.render('html_element', {
			'sitekey' : '6LctRMEUAAAAABY1k1HFXPG1jM-a1LQwfPHxlupw',
		});
	};
</script>