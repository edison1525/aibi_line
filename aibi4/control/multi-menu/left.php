<script src="../multi-menu/js/modernizr.js"></script>
<!--<link rel="stylesheet" href="../multi-menu/css/reset.css">-->
<link rel="stylesheet" href="../multi-menu/css/style.css">
<script src="../multi-menu/js/main.js"></script>
<?php
	$UrlArray = explode("/", htmlentities($_SERVER["REQUEST_URI"]));
	$PageURL = strtolower($UrlArray[sizeof($UrlArray)-1]);
	
	$top_btn_array = array(
		"<a style=\"cursor:default; color:black; font-size:10px; font-weight:bold;\">圖片".$directorySize."MB</a>",
		"<a style=\"cursor:default; color:black; font-weight:bold;\">".$loginUserLevelInfo['web_permission_subject']."</a>",
		//"<a href=\"../../\" target=\"_blank\">網站首頁</a>",
		"<a href=\"../menu/index.php\">管理首頁</a>",
		"<a href=\"../menu/logout.php\">登出系統</a>"
	);
	$top_btn_list = "";
	foreach ($top_btn_array as $value) $top_btn_list .= "<li>".$value."</li>\n";
?>
<div id="wrapper">
	<div id="main" class="clearfix">
		<div id="top">
			<div class="top_logo"><?php echo $Init_WebTitle; ?>後台管理系統</div>
			<ul class="top_btn">
			<?php echo $top_btn_list; ?>
			</ul>
			<br class="clear">
		</div>
		<div id="top_left">
			<ul class="top_btn">
			<?php echo $top_btn_list; ?>
			</ul>
		</div>
		<div id="center">
			<div id="left">
				<div id="menu" style="width:19%;">
					<ul class="cd-accordion-menu animated">
				<?php
					//選單
					$web_worktree_list = "";
					$web_worktree_main_counter = 0;
					$web_worktree_counter = 0;
					$sql = "SELECT DISTINCT kinds FROM web_worktree where ifShow = '1' order by gsort ASC";
					$rs = ConnectDB($DB, $sql);
					for ($i=0; $i<mysqli_num_rows($rs); $i++) {
						$row = mysqli_fetch_assoc($rs);
						$first = false;
						
						$rowAry2 = array();
						$web_worktree_id_ary = array();
						$sql2 = "SELECT * FROM web_worktree WHERE kinds like '".$row["kinds"]."' and ifShow = '1' ORDER BY asort";
						$rs2 = ConnectDB($DB, $sql2);
						for ($j=0; $j<mysqli_num_rows($rs2); $j++) {
							$row2 = mysqli_fetch_assoc($rs2);
							$rowAry2[] = $row2;
							$web_worktree_id_ary[] = $row2[web_worktree_id];
						}	
						
						//權限比對
						$diff = array_diff($web_worktree_id_ary, $loginUserFuncAry);
						if(count($diff) == count($web_worktree_id_ary)) {
							continue;
						}
						
						if(!in_array($row[kinds], array('優惠促銷', '最新消息', '頁面管理', '熱銷商品', '統計中心'))) {
				?>			
						<li class="has-children1">
							<input class="menuClick" type="checkbox" name ="group-<?php echo $i; ?>" id="group-<?php echo $i; ?>">
							<label for="group-<?php echo $i; ?>"><?php echo $row[kinds]; ?></label>

							<ul>
						<?php
						/*
							$sql2 = "SELECT * FROM web_worktree WHERE kinds like '".$row["kinds"]."' and ifShow = '1' ORDER BY asort";
							$rs2 = ConnectDB($DB, $sql2);
							for ($j=0; $j<mysql_num_rows($rs2); $j++) {
								$row2 = mysql_fetch_assoc($rs2);
						*/
							foreach($rowAry2 as $key2 => $row2) {
								if(!in_array($row2[web_worktree_id], $loginUserFuncAry)) {
									continue;
								}
								if($row2["subject_en"] != 'web_xxxx_product') {	
						?>		
								<li><a href="../<?php echo $row2["subject_en"]."/".$row2["subject_en"]."_".$row2["url"].".php".$row2["parameter"]; ?>"><?php echo $row2[subject]; ?></a></li>
				
						<?php
								} else {
						?>				
								<li class="has-children">
									<input type="checkbox" name ="sub-group-2" id="sub-group-2">
									<label for="sub-group-2"><?php echo $row2[subject]; ?></label>
									<ul>
										<li>
											<a href="../<?php echo $row2["subject_en"]."/".$row2["subject_en"]."_".$row2["url"].".php".$row2["parameter"]; ?>"><?php echo $row2[subject]; ?></a>
										</li>
								<?php
									$sql3 = "Select SQL_CALC_FOUND_ROWS web_xxxx_product_id, ifShow, asort, subject, subject_en, Covers, Files From web_xxxx_product order by asort asc, web_xxxx_product_id desc ";
									$rs3 = ConnectDB($DB, $sql3);
									for ($k=0; $k<mysqli_num_rows($rs3); $k++) {
										$row3 = mysqli_fetch_assoc($rs3);
										if($row3[web_xxxx_product_id] == '4') {
											continue;
										}
								?>	
										<li class="has-children2">
											<input class="menuClick2" type="checkbox" name ="_sub-group-<?php echo $row3[web_xxxx_product_id]; ?>" id="_sub-group-<?php echo $row3[web_xxxx_product_id]; ?>">
											<label for="_sub-group-<?php echo $row3[web_xxxx_product_id]; ?>"><?php echo $row3[subject]; ?></label>
											<ul>
											<?php

												$sql4 = "
													SELECT 
														web_xxx_product.web_xxx_product_id,
														web_xxx_product.web_xxxx_product_id, 
														web_xxx_product.subject as web_xxx_product_subject,
														web_xxx_product.subject_en as web_xxx_product_subject_en,  
														web_xxxx_product.web_xxxx_product_id, 
														web_xxxx_product.subject as web_xxxx_product_subject,
														web_xxxx_product.Covers as web_xxxx_product_Covers 
													FROM 
														web_xxx_product 
													LEFT JOIN 
														web_xxxx_product 
													ON 
														web_xxxx_product.web_xxxx_product_id = web_xxx_product.web_xxxx_product_id
													WHERE 
														web_xxxx_product.web_xxxx_product_id = '".$row3['web_xxxx_product_id']."' 
													AND 
														web_xxx_product.web_xxx_product_id != '9999' 
													AND 
														web_xxx_product.ifShow = 1 
													ORDER BY 
														web_xxxx_product.web_xxxx_product_id asc, 
														web_xxx_product.asort asc, 
														web_xxx_product.web_xxx_product_id desc
													";
												$rs4 = ConnectDB($DB, $sql4);
												for ($l=0; $l<mysqli_num_rows($rs4); $l++) {
													$row4 = mysqli_fetch_assoc($rs4);

											?>
												<li class="has-children3">
													<input class="menuClick3" type="checkbox" name ="__sub-group-<?php echo $row4[web_xxx_product_id]; ?>" id="__sub-group-<?php echo $row4[web_xxx_product_id]; ?>">
													<label for="__sub-group-<?php echo $row4[web_xxx_product_id]; ?>"><?php echo $row4[web_xxx_product_subject]; ?></label>
													<ul>
												<?php
													$sql5 = "Select web_xx_product_id, subject as web_xx_product From web_xx_product Where web_xxx_product_id = '".$row4["web_xxx_product_id"]."' and ifShow = '1' order by asort asc, web_xx_product_id desc";
													$rs5 = ConnectDB($DB, $sql5);
													if (mysqli_num_rows($rs5)>0) {
														
														for ($m=0; $m<mysqli_num_rows($rs5); $m++) {
															$row5 = mysqli_fetch_assoc($rs5);
												?>				
														<li class="has-children4">
															<input class="menuClick4" type="checkbox" name ="___sub-group-<?php echo $row5[web_xx_product_id]; ?>" id="___sub-group-<?php echo $row5[web_xx_product_id]; ?>">
															<label for="___sub-group-<?php echo $row5[web_xx_product_id]; ?>"><?php echo $row5[web_xx_product]; ?></label>
															<ul>
														<?php
															$sql6 = "Select web_x_product_id, subject as web_x_product From web_x_product Where web_xx_product_id = '".$row5["web_xx_product_id"]."' and ifShow = '1' order by asort asc, web_x_product_id desc";
															$rs6 = ConnectDB($DB, $sql6);
															if (mysqli_num_rows($rs6)>0) {		
																for ($n=0; $n<mysqli_num_rows($rs6); $n++) {
																$row6 = mysqli_fetch_assoc($rs6);
														?>				
																<li><a href="../web_product/web_product_list.php?kind=<?php echo $row6[web_x_product_id]; ?>"><?php echo $row6[web_x_product]; ?></a></li>
														<?php
																}
															}
														?>	
															</ul>
														</li>
												<?php
														}
													}
												?>			
													</ul>
												</li>
											<?php
												}
											?>		
											</ul>
										</li>
								<?php
									}
								?>			
									</ul>
								</li>
						<?php
								}
							}
						?>
							</ul>
						</li>
						<?php 
							} else { 
								$sql2 = "SELECT * FROM web_worktree WHERE kinds like '".$row["kinds"]."' and ifShow = '1' ORDER BY asort";
								$rs2 = ConnectDB($DB, $sql2);
								for ($j=0; $j<mysqli_num_rows($rs2); $j++) {
									$row2 = mysqli_fetch_assoc($rs2);
									if(!in_array($row2[web_worktree_id], $loginUserFuncAry)) {
										continue;
									}
									if($row2["subject_en"] != 'web_xxxx_product') {		
						?>
						<li>
							<a href="../<?php echo $row2["subject_en"]."/".$row2["subject_en"]."_".$row2["url"].".php".$row2["parameter"]; ?>"><?php echo $row2[subject]; ?></a>
						</li>
					<?php
									}
								}	
							}
						}
					?>	
						
					</ul>
				<!--</div>-->
				<br class="clear">

		</div>
	</div>
	<script>
		$(function() {
			$('.menuClick').on('click', function() {
				//console.log($(this).parents('#menu').css({'overflow-y':'scroll', 'height': '80%'}));
				$(this).parents('#menu').css({'overflow-y':'scroll', 'height': '80%'});
				var id = $(this).attr('id');
				console.log(id);
				$('.has-children1').each(function() {
					//console.log($(this).children('input').attr('id'));
					if(id == $(this).children('input').attr('id')) {
						return;
					}
					if($(this).children('ul').is(':visible')) {
						//console.log($(this).children('input').is(':checked'));
						$(this).children('ul').hide();
						$(this).children('input').attr('checked', false);
					}
				});

			});

			$('.menuClick2').on('click', function() {
				var id2 = $(this).attr('id');
				console.log(id2);
				$('.has-children2').each(function() {
					//console.log($(this).children('input').attr('id'));
					if(id2 == $(this).children('input').attr('id')) {
						return;
					}
					if($(this).children('ul').is(':visible')) {
						//console.log($(this).children('input').is(':checked'));
						$(this).children('ul').hide();
						$(this).children('input').attr('checked', false);
					}
				});

			});

			$('.menuClick3').on('click', function() {
				var id3 = $(this).attr('id');
				//console.log(id3);
				$('.has-children3').each(function() {
					console.log($(this).children('input').attr('id'));
					if(id3 == $(this).children('input').attr('id')) {
						return;
					}
					if($(this).children('ul').is(':visible')) {
						//console.log($(this).children('input').is(':checked'));
						$(this).children('ul').hide();
						$(this).children('input').attr('checked', false);
					}
				});

			});

			$('.menuClick4').on('click', function() {
				var id4 = $(this).attr('id');
				//console.log(id3);
				$('.has-children4').each(function() {
					console.log($(this).children('input').attr('id'));
					if(id4 == $(this).children('input').attr('id')) {
						return;
					}
					if($(this).children('ul').is(':visible')) {
						//console.log($(this).children('input').is(':checked'));
						$(this).children('ul').hide();
						$(this).children('input').attr('checked', false);
					}
				});

			});

		})
	</script>
	<div id="right">	
