<?php include("../includes/head.php"); ?>
<?php
	$web_x_bonus_id = intval($_GET["web_x_bonus_id"]);
	$sql = "Select * From web_x_bonus Where web_x_bonus_id = '".$web_x_bonus_id."' ";
	$rs = ConnectDB($DB, $sql);
	if (mysql_num_rows($rs)==0) {
		$action = "Add";
		$kind = "shopping";	//類型

		$sql = "Select SQL_CALC_FOUND_ROWS web_x_bonus_id, kind, subject, sdate, edate From web_x_bonus where edate >= NOW() ORDER BY edate DESC ";
	    $rs = ConnectDB($DB, $sql);
	    $edate = mysql_result($rs, 0, "edate");

		$sdate = $edate;	//開始日期
		$edate = date("Y-m-d", strtotime($edate."+1 month"));	//結束日期
		$srange = 0;	//結帳時滿多少元贈送點數
		$money = 0;	//點數金額
		$overflow = 0;	//下次消費滿多少元才可使用點數
		$bouonsOverflow = 0;	//點數累積
		$ifDate = 1;	//贈送方式
		$days = 30;	//自訂單確認日起幾天內有效
		//$startdate = date("Y-m-d");	//開始日期
		//$enddate = date("Y-m-d");	//截止日期
	} else {
		$action = "Edit";
		for ($i=0; $i<mysql_num_rows($rs); $i++) {
			$row = mysql_fetch_assoc($rs);
			foreach($row as $_key=>$_value) $$_key = str_edit($row[$_key]);
		}
	}
	if($web_x_bonus_id) {
		$sql2 = "Select count(*) as counter From web_bonus Where web_x_bonus_id = '".$web_x_bonus_id."' ";
		$rs2 = ConnectDB($DB, $sql2);
		$counter = mysql_result($rs2, 0, "counter");
	} 
	
	if ($days=="0") $days = "";	//自訂單成立日起幾天內有效
	if ($startdate=="0000-00-00") $startdate = "";	//開始日期
	if ($enddate=="0000-00-00") $enddate = "";	//截止日期
?>
<script language="javascript">
<!--
function chkform() {
	var msg = "";
	if (document.form.subject.value == "") { msg = msg + "標題\n"; }
	if (document.form.ifDate[0].checked == true) {
		if (document.form.days.value == "") { msg = msg + "自贈送日起幾天內有效\n"; }
	} else {
		if (document.form.startdate.value == "" || document.form.enddate.value == "") { msg = msg + "贈送起迄日\n"; }
	}
	
	if (msg!="") {
		alert("請輸入以下欄位\n\n" + msg);
		return false;
	}
	return true;
}
//-->
</script>
</head>

<body>
<?php 
	//include("../includes/left.php");
	include("../multi-menu/left.php"); 
?>
<h1>點數</h1>
<div id="nav">目前位置：<?php echo $nav_title; ?> > <a href="web_x_bonus_list.php">點數</a> > <?php echo ($action=="Add") ? "新增" : "編輯"; ?></div>

<form name="form" method="post" action="web_x_bonus_update.php" enctype="multipart/form-data">
  <table class="Edit_form">
	<tr>
	  <th>類型：<span class="star">*</span></th>
	  <td>
		<?php
			$kind_list = "";
			foreach ($BonusKindArray as $_key=>$_value) {
				$kind_list .= "<input name=\"kind\" type=\"radio\" value=\"".$_key."\"";
				if ($kind==$_key) $kind_list .= " checked=\"checked\"";
				$kind_list .= " /> ".$_value."　";
			}
			echo $kind_list;
		?>
	  </td>
	</tr>
	<tr>
	  <th>標題：<span class="star">*</span></th>
	  <td><input type="text" name="subject" value="<?php echo $subject; ?>" class="fill" maxlength="100" /></td>
	</tr>
	<tr>
	  <th>開始日期：<span class="star">*</span></th>
	  <td><input type="text" name="sdate" id="sdate" value="<?php echo $sdate; ?>" size="10" maxlength="10" readonly /></td>
	</tr>
	<tr>
	  <th>結束日期：<span class="star">*</span></th>
	  <td><input type="text" name="edate" id="edate" value="<?php echo $edate; ?>" size="10" maxlength="10" readonly /></td>
	</tr>
	<tr>
	  <th>滿額贈：<span class="star">*</span></th>
	  <td>滿<input type="text" name="srange" value="<?php echo $srange; ?>" size="10" maxlength="5" />元贈送點數<input type="text" name="money" value="<?php echo $money; ?>" size="10" maxlength="5" />點<!--，下次消費滿<input type="text" name="overflow" value="<?php echo $overflow; ?>" size="10" maxlength="5" />元及--><!--點數累積滿<input type="text" name="bonusOverflow" value="<?php echo $bonusOverflow; ?>" size="10" maxlength="5" />點才可使用--></td>
	</tr>
	<tr>
	  <th>贈送方式：<span class="star">*</span></th>
	  <td><table class="None_form">
          <tr>
            <td>則該筆訂單需經過管理者變更狀態為<span class="font_red">付款成功</span>後才可使用</td>
          </tr>
          <tr>
            <td>
              <input type="radio" name="ifDate" value="1" <?php if ($ifDate=="1") echo "checked=\"checked\""; ?>/>
              贈送日起<input type="text" name="days" value="<?php echo $days; ?>" size="10" maxlength="5" />天內有效
            </td>
          </tr>
          <tr>
            <td>
              <input type="radio" name="ifDate" value="0" <?php if ($ifDate=="0") echo "checked=\"checked\""; ?>/>
              自<input type="text" name="startdate" id="startdate" value="<?php echo $startdate; ?>" size="10" maxlength="10" readonly />至<input type="text" name="enddate" id="enddate" value="<?php echo $enddate; ?>" size="10" maxlength="10" readonly />可使用
            </td>
          </tr>
        </table>
	  </td>
	</tr>
  </table>
  <div class="btn">
    <?php if ($counter==0) { ?><input name="submit" type="submit" value="確定送出" onClick="return chkform();" /><?php } ?>
    <input name="button" type="button" value="回上一頁" onClick="history.go(-1);" />
    <input type="hidden" name="action" value="Edit" />
    <input type="hidden" name="web_x_bonus_id" value="<?php echo $web_x_bonus_id; ?>" />
    <input type="hidden" name="page" value="<?php echo (intval($_GET["page"])==0) ? 1 : intval($_GET["page"]); ?>" />
    <input type="hidden" name="field" value="<?php echo str_filter($_GET["field"]); ?>" />
    <input type="hidden" name="keyword" value="<?php echo str_filter($_GET["keyword"]); ?>" />
  </div>
</form>
<?php include("../includes/footer.php"); ?>
</body>
</html>