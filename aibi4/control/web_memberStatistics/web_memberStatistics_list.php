<?php include("../includes/head.php"); ?>
<?php
	$type = $_POST["type"] ? $_POST["type"] : $_GET["type"];
	$search_sdate = $_POST["search_sdate"] ? $_POST["search_sdate"] : $_GET["search_sdate"];
	$search_edate = $_POST["search_edate"] ? $_POST["search_edate"] : $_GET["search_edate"];
	if($search_sdate > $search_edate){
		RunAlert("開始時間不能大於結束時間");
	}
	switch ($type) {
		case "age":
			$title = "年齡";
			break;
		case "sex":
			$title = "性別";
			break;
		default:
			$type = "age";
			$title = "年齡";
	}
 
	$timeRange = ($search_sdate && $search_edate) ? '&search_sdate='.$search_sdate.'&search_edate='.$search_edate : null;
?>
</head>

<body>
<?php 
	//include("../includes/left.php");
	include("../multi-menu/left.php"); 
?>
<h1><?php echo $title; ?></h1>
<div id="nav">目前位置：<?php echo $nav_title; ?> > <a href="web_memberStatistics_list.php">統計資料</a> > <?php echo $title; ?></div>

<!--搜尋開始-->
<form name="formSearch" method="post" action="web_memberStatistics_list.php">
  <div id="search">
  	時間：<input name="search_sdate" id="search_sdate" type="text" value="<?php echo $search_sdate; ?>" maxlength="10" placeholder="開始日期" style="width: 100px" />~<input name="search_edate" id="search_edate" type="text" value="<?php echo $search_edate; ?>" maxlength="10" placeholder="結束日期" style="width: 100px" />
    <div id="new">
    	<!--<a class="memberStatistics" data-type="area" href="web_memberStatistics_list.php?type=area<?php echo $timeRange ?>">地區統計</a>-->
    	<a class="memberStatistics" data-type="age" href="web_memberStatistics_list.php?type=age<?php echo $timeRange ?>">年齡統計</a>
    	<!--<a class="memberStatistics" data-type="sex" href="web_memberStatistics_list.php?type=sex<?php echo $timeRange ?>">性別統計</a>-->
    <?php
		if($loginUserLevelInfo['export']) {
	?>		
		<a href="web_memberStatistics_erp.php?type=<?php echo $type.$timeRange ?>" title="<?php echo $search_title; ?>">匯出<?php echo ($list_search[link]=="") ? "全部" : "搜尋"; ?></a>
    <?php
		}
	?>	
		<input type="hidden" value="area" name="type" />
    </div>
  </div>
</form>
<!--搜尋結束-->

<form>
<?php
	$web_member_list = "";
	
	//地區
	if ($type=="area") {
		$web_member_list .= "<table class=\"Edit_form\">";
		if($search_sdate && $search_edate) {		
        	$sql = "Select DISTINCT city, count(city) as counter From web_member WHERE (cdate >= '".$search_sdate." 00:00:00'  AND cdate <= '".$search_edate." 23:59:59') group by city order by counter desc ";
        } else {
        	$sql = "Select DISTINCT city, count(city) as counter From web_member group by city order by counter desc ";
        }
        $rs = ConnectDB($DB, $sql);
		for ($i=0; $i<mysql_num_rows($rs); $i++) {
			$row = mysql_fetch_assoc($rs);
			foreach($row as $_key=>$_value) $$_key = $row[$_key];
			
			if ($city=="") $city = "未填寫地區";
			
			$web_member_list .= "
			<tr>
			  <th>".$city."：</th>
			  <td>".number_format($counter)." 人</td>
			</tr>";
		}
		$web_member_list .= "</table>";
	}
	
	//年齡
	if ($type=="age") {
		//Select web_member_id, now() as today, birthday, year(from_days(datediff(now(), birthday))) as age From web_member
       	$sql = "Select web_member_id, year(from_days(datediff(now(), birthday))) as age From web_member ";	
        $rs = ConnectDB($DB, $sql);
		for ($i=0; $i<mysql_num_rows($rs); $i++) {
			$row = mysql_fetch_assoc($rs);
			foreach($row as $_key=>$_value) $$_key = $row[$_key];
			
			$sql2 = "Update web_member set age = '".intval($age)."' Where web_member_id = '".$web_member_id."' ";
			$rs2 = ConnectDB($DB, $sql2);
		}

		$web_member_list .= "<table class=\"Edit_form\">";
		if($search_sdate && $search_edate) {
        	$sql = "Select DISTINCT age, count(age) as counter From web_member WHERE (cdate >= '".$search_sdate." 00:00:00'  AND cdate <= '".$search_edate." 23:59:59') group by age order by age asc ";
        } else {
        	$sql = "Select DISTINCT age, count(age) as counter From web_member group by age order by age asc ";
        }	
        $rs = ConnectDB($DB, $sql);
		for ($i=0; $i<mysql_num_rows($rs); $i++) {
			$row = mysql_fetch_assoc($rs);
			foreach($row as $_key=>$_value) $$_key = $row[$_key];
			
			if ($age==0)
				$age = "未填寫生日";
			else
				$age .= "歲";
			
			$web_member_list .= "
			<tr>
			  <th>".$age."：</th>
			  <td>".number_format($counter)." 人</td>
			</tr>";
		}
		$web_member_list .= "</table>";
	}
	
	//性別
	if ($type=="sex") {
		$web_member_list .= "<table class=\"Edit_form\">";		
		if($search_sdate && $search_edate) {
        	$sql = "Select DISTINCT sex, count(sex) as counter From web_member WHERE (cdate >= '".$search_sdate." 00:00:00'  AND cdate <= '".$search_edate." 23:59:59') group by sex order by counter desc ";
        } else {
        	$sql = "Select DISTINCT sex, count(sex) as counter From web_member group by sex order by counter desc ";
        }	
        $rs = ConnectDB($DB, $sql);
		for ($i=0; $i<mysql_num_rows($rs); $i++) {
			$row = mysql_fetch_assoc($rs);
			foreach($row as $_key=>$_value) $$_key = $row[$_key];
			
			$web_member_list .= "
			<tr>
			  <th>".$sex."：</th>
			  <td>".number_format($counter)." 人</td>
			</tr>";
		}
		$web_member_list .= "</table>";
	}
	
	echo $web_member_list;
?>
  <div id="delete"> 
    <!--<input type="submit" name="submit" value="刪除" onClick="return CheckDel();" />-->
  </div>
</form>
<?php include("../includes/footer.php"); ?>
</body>
</html>