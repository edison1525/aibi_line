<?php
	include("../includes/includes.php");

	$web_x_class_id = $_POST["web_x_class_id"] ? floatval($_POST["web_x_class_id"]) : floatval($_GET["web_x_class_id"]);
	$sql = "Select subject From web_x_class Where web_x_class_id = '".$web_x_class_id."'";
	$rs = ConnectDB($DB, $sql);
	if (mysql_num_rows($rs)==0) RunJs("web_x_class_list.php");
	
	$action = $_POST["action"] ? $_POST["action"] : $_GET["action"];

	//新增編輯
	if ($action=="Edit") {
		
		$web_x_class_id = intval($_POST["new_web_x_class_id"]);	//分類名稱
		$web_class_id = intval($_POST["web_class_id"]);	//分類名稱
		//$cdate = date("Y-m-d");	//發表日期
		//$edate = date("Y-m-d");	//結束時間
		$subject = ($_POST["subject"]);	//分類名稱
		
		/********重組陣列小到大*********/
		foreach($_POST["lineInfo"] as $key => $postLineInfo) {
			$explodeRow = explode("@#@", $postLineInfo);
			$lineAsoft = ($explodeRow[0]) ? urldecode($explodeRow[0]) : null;
			$lineSubject = ($explodeRow[1]) ? ($explodeRow[1]) : null;
			$lineLink = ($explodeRow[2]) ? ($explodeRow[2]) : null;
			$lineNote = ($explodeRow[3]) ? ($explodeRow[3]) : null;
			$_POST["lineInfo2"][$lineSubject."@#@".$lineLink."@#@".$lineNote] = $lineAsoft;
		}

		asort($_POST["lineInfo2"]);
		
		$_POST["lineInfo"] = array();
		foreach($_POST["lineInfo2"] as $key => $postLineInfo2) {
			$explodeRow = explode("@#@", $key);
			$lineSubject = ($explodeRow[0]) ? urldecode($explodeRow[0]) : null;
			$lineLink = ($explodeRow[1]) ? ($explodeRow[1]) : null;
			$lineNote = ($explodeRow[2]) ? ($explodeRow[2]) : null;
			$_POST[lineInfo][] = $postLineInfo2."@#@".$lineSubject."@#@".$lineLink."@#@".$lineNote;
		}
		
		/*****************/
		//lineInfo
		$lineInfo = implode('#####', $_POST["lineInfo"]);
		$_POST["lineInfo"] = $lineInfo;
		
		$CoverAction = "Update";
		require("../includes/cover.php");
		$FileAction = "Update";
		require("../includes/files.php");
		$_POST["Files"] = $Files;
	
		//$debug = true;
		$TableName = "web_class";
		$AccurateAction = "Get";
		require("../includes/accurate.php");

		if ($subject=="") RunAlert("請輸入標題");
	
		//exit;
		
		if ($web_class_id>0) {
			//$debug = true;
			$ForbidData = array("hits");	//不更新的欄位
			$AccurateAction = "Update";
			require("../includes/accurate.php");
		} else {
			//$debug = true;
			$hits = 0;	//瀏覽人數
			$AccurateAction = "Insert";
			require("../includes/accurate.php");
		}
	} 
	
	//刪除
	if ($action=="Delete") {
		$DeleteBox = $_POST["DeleteBox"];
		if (!is_array($DeleteBox)) die();
		
		foreach ($DeleteBox as $value) {

			//產品代表圖
			$sql = "Select Covers From web_class Where web_class_id = '".$value."' ";
			$rs = ConnectDB($DB, $sql);
			$CoverAction = "Del";
			require("../includes/cover.php");
			
			//內頁圖片
			$sql = "Select Files From web_class Where web_class_id = '".$value."' ";
			$rs = ConnectDB($DB, $sql);
			$FileAction = "Del";
			require("../includes/files.php");
			
			$sql = "Delete From web_class Where web_class_id = '".$value."' ";
			$rs = ConnectDB($DB, $sql);
		}
		
		$sql = "Select count(*) as counter From web_class ";
		$rs = ConnectDB($DB, $sql);
		if (mysql_result($rs, 0, "counter")==0) {
			$sql = "Truncate table web_class";
			$rs = ConnectDB($DB, $sql);
		}
	}
	
	RunJs("web_class_list.php?page=".intval($_POST["page"])."&field=".$_POST["field"]."&keyword=".urlencode($_POST["keyword"])."&web_x_class_id=".$web_x_class_id);
?>