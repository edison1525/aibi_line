<?php
    include_once("./control/includes/function.php");
	ini_set('session.cookie_httponly', 1 );
	session_start();

	if (empty($_SESSION['expire']) || $_SESSION['expire'] < time()) $_SESSION['token'] = md5('aibi' . uniqid(microtime())); // create token (fast/sufficient) 
	$_SESSION['expire'] = time() + 900; // make session valid for next 15 mins
	$_SESSION['visitid'] = $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT'];
	
	$agent = $_SERVER['HTTP_USER_AGENT'];
	if (!preg_match("#\bLine\b#", $agent)) {
		//die('aibi');
	}
	$web_member_id = $_REQUEST['web_member_id'];
	$web_product_id = $_REQUEST['web_product_id'];
	$web_x_order_id = $_REQUEST['web_x_order_id'];
	if(!$web_member_id || !$web_product_id || !$web_x_order_id) {
		die('aibi');
	}
	
	$sql = "
		Select 
			SQL_CALC_FOUND_ROWS a.*,
			b.subject as bsubject,
			b.web_product_id as bproductId,
			b.price as bprice,
			b.num as bnum,
			b.dimension as bdimension,
			c.subject as csubject,
			c.price_cost as cprice_cost,
			c.price_member as cprice_member,
			c.totalCount as ctotalCount,
			c.Covers as cCovers,
			d.web_xx_product_id as dweb_xx_product_id,
			d.subject as dsubject,
			e.subject as esubject,
			(select subject from web_x_class where web_x_class.web_x_class_id = a.store_id) as xClassSubject
		From 
			web_x_order a
		Left Join 
			web_order b ON b.web_x_order_ordernum = a.ordernum
		Left Join 
			web_product c ON c.web_product_id = b.web_product_id
		Left Join 
			web_x_product d ON d.web_x_product_id = c.web_x_product_id
		Left Join 
			web_xx_product e ON e.web_xx_product_id = d.web_xx_product_id	
		Where 
			a.web_x_order_id = '".$web_x_order_id."'
		AND
			a.web_member_id = '".$web_member_id."' 
		AND
			b.web_product_id = '".$web_product_id."'
		AND
			a.states = '訂單成立'
		AND
			a.paymentstatus = '付款成功'
		AND
			d.web_xx_product_id IN (1)
		order by 
			a.web_x_order_id desc 
	";
	$pdo = $pdoDB->prepare($sql);
	$pdo->execute();
	$orderRow = $pdo->fetchAll(PDO::FETCH_ASSOC);
	
	$sql = "
		SELECT 
			web_x_order.*,
			web_order.subject as prodSubjects,
			web_order.web_order_id as web_order_id,
			web_order.web_x_order_ordernum as web_x_order_ordernum,
			web_order.web_product_id as web_order_product_id,
			web_product.web_product_id as web_product_product_id,
			web_product.web_x_product_id as web_product_x_product_id,
			web_product.totalCount as web_product_totalCount,
			web_product.Covers as web_product_Covers,
			web_product.useEdate as web_product_useEdate,
			web_x_product.web_x_product_id as web_x_product_x_product_id,
			web_x_product.web_xx_product_id as web_x_product_xx_product_id,
			(select subject from web_x_class where web_x_class.web_x_class_id = web_x_order.store_id) as xClassSubject
		from 
			web_x_order
		Left Join
			web_order as web_order
		On
			web_order.web_x_order_ordernum = web_x_order.ordernum
		Left Join
			web_product
		On
			web_product.web_product_id = web_order.web_product_id
		Left Join
			web_x_product
		On
			web_x_product.web_x_product_id = web_product.web_x_product_id
		WHERE 
			web_x_order.from_ordernum = :ordernum
		AND
			web_x_order.states = '訂單成立'	
		AND
			web_x_order.paymentstatus = '付款成功'	
		AND
			web_x_order.order_type = 1
		AND
			web_x_product.web_xx_product_id = 1		
	";
	
	$excute = array(
		':ordernum'		=> $orderRow[0]['ordernum'],
	);
	//$debug = new Helper();
	//echo $test = $debug::debugPDO($sql, $excute);
	$pdo = $pdoDB->prepare($sql);
	$pdo->execute($excute);
	$row4 = $pdo->fetchAll(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $Init_WebTitle; ?> 會員專區-療程體驗券</title>
	<script src="./js/jquery-1.10.2.min.js"></script>
    <link rel="stylesheet" href="css/app.css"/>
</head>
<body>
<div class="header">
    <h1>頭皮活化疏導療程</h1> <a class="back" href="coupon.php?web_member_id=<?php echo $web_member_id; ?>"></a>
</div>
<div class="content coupon-record">
    <div class="title">購買/使用 紀錄</div>
	<!--
    <div class="row">
        <div class="col-1">2019/05/12</div>
        <div class="col-2">
            <div><span class="col-name">使用狀況</span><span>2 / 4 (已使用 / 總次數)</span></div>
            <div><span class="col-name">店別</span><span><?php echo store_Name; ?></span></div>
            <div><span class="col-name">經手人</span><span><?php echo $orderVal['editUser']; ?></span></div>
            <span class="use">用</span>
        </div>
    </div>
	-->
<?php
	foreach($orderRow as $key => $orderVal) {
?>	
    <div class="row">
        <div class="col-1"><?php echo date('Y/m/d', strtotime($orderVal['cdate'])); ?></div>
        <div class="col-2">
            <div><span class="col-name">金額</span><span><?php echo number_format($orderVal['total']);?>元</span></div>
            <div><span class="col-name">使用狀況</span><span><?php echo count($row4);?> / <?php echo number_format($orderVal['ctotalCount']);?> (已使用 / 總次數)</span></div>
            <div><span class="col-name">店別</span><span><?php echo $orderVal['xClassSubject']; ?></span></div>
            <div><span class="col-name">經手人</span><span><?php echo $orderVal['editUser']; ?></span></div>
            <span class="buy">購</span>
        </div>
    </div>
<?php
	}
?>
<?php
	foreach($row4 as $key4 => $val4) {
?>	
    <div class="row">
        <div class="col-1"><?php echo date('Y/m/d', strtotime($val4['cdate'])); ?></div>
        <div class="col-2">
            <div><span class="col-name">使用狀況</span><span>1 / <?php echo number_format($val4['web_product_totalCount']);?> (已使用 / 總次數)</span></div>
            <div><span class="col-name">店別</span><span><?php echo $val4['xClassSubject']; ?></span></div>
            <div><span class="col-name">經手人</span><span><?php echo $val4['editUser']; ?></span></div>
            <span class="use">用</span>
        </div>
    </div>
<?php
	}
?>	
</div>
</body>
</html>
