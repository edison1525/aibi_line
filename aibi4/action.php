<?php
	//註解
	include_once("./control/includes/function.php");
	//exit;
	/*
	if (empty($_SESSION['token']) ||  $_SESSION['token'] != $_POST['token'] || $_SESSION['expire'] < time()
		|| $_SESSION['visitid'] != $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT'] || empty($_POST["action"]) ) {
	*/		
	if (empty($_SESSION['token']) ||  $_SESSION['token'] != $_POST['token'] || empty($_POST["action"]) ) {		
		session_destroy();
		$req = array('error'=>'1', 'message'=> '系統維護中');
		die(json_encode($req, JSON_UNESCAPED_UNICODE));
	}
	
	$action = $_POST["action"];
	
	//預約掛號
	if ($action == "register") {
		$person = intval($_POST["person"]);
		$lineID = ($_POST["lineID"]);
		$uname = trim($_POST["uname"]);
		$uname2 = ($person) ? $uname : trim($_POST["uname2"]);
		$sex = trim($_POST["sex"]);
		$sex2 = trim($_POST["sex2"]);
		$birthday = ($_POST["birthday"]);
		$mobile = ($_POST["mobile"]);
		$shiftId = intval($_POST["shiftId"]);
		$subject = trim($_POST["date"]);
		$registerType = trim($_POST["registerType"]);
		$timeId = intval($_POST["timeId"]);
		$teamXid = intval($_POST["teamXid"]);
		
		if($lineID) {
			$sql = "Select * From web_member Where lineID = :lineID ";
			$excute = array(
				':lineID'        => $lineID,
			);
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute($excute);
			$row = $pdo->fetch(PDO::FETCH_ASSOC);
		} else {
			$sql = "Select * From web_member Where uname = :uname AND mobile = :mobile ";
			$excute = array(
				':uname'        => $uname,
				':mobile'        => $mobile,
			);
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute($excute);
			$row = $pdo->fetch(PDO::FETCH_ASSOC);
		}
		
		$web_member_id = $row['web_member_id'];
		$oldUname = $row['uname'];
		$oldBirthday = $row['birthday'];
		$oldMobile = $row['mobile'];
		$oldSex = $row['sex'];
	
		//沒有加入會員
		if(!$web_member_id) {
			/*
			$insertSql = "
				Insert into 
					web_member 
						(
							lineID, 
							uname,
							birthday, 
							mobile, 
							cdate
						) 
					values 
						(
							'$lineID', 
							'$uname',
							'$birthday',
							'$mobile', 
							'".date('Y-m-d H:i:s')."'
						) 
			";	
			
			$web_member_id = ConnectDBInsterId($DB, $insertSql);
			*/
			$insertSql = "
				Insert into 
					web_member 
						(
							lineID,
							store_id,	
							uname,
							sex,
							birthday, 
							mobile, 
							cdate
						) 
					values 
						(
							:lineID,
							:store_id,	
							:uname,
							:sex,
							:birthday,
							:mobile, 
							:cdate
						) 
			";	
			$excute = array(
				':lineID'        => $lineID,
				':store_id'        => store_Id,
				':uname'        => $uname,
				':sex'        => $sex,
				':birthday'        => $birthday,
				':mobile'        => $mobile,
				':cdate'        => date('Y-m-d H:i:s'),
			);
			$pdo = $pdoDB->prepare($insertSql);
			$pdo->execute($excute);
			$web_member_id = $pdoDB->lastInsertId();
			
			$sql = "Select ordernum, web_member_id, lineID, order_name, accept_name From web_x_register Where subject like '%".$subject."%' order by web_x_register_id DESC";
			$rs = ConnectDB($DB, $sql);
			
			$ordernum = mysql_result($rs, 0, "ordernum");
			$ordernum = ($ordernum) ? ($ordernum + 1) : 1;
			
			
			$sql3 = "
				Insert into 
					web_x_register 
						(
							web_shift_id,
							web_time_id,
							web_x_team_id,
							lineID,
							self,
							ordernum,
							subject,
							registerDate,
							registerType,
							states,
							web_member_id,
							order_name,
							order_sex,
							order_mobile,
							accept_name,
							accept_sex,
							accept_birthday,
							cdate,
							date 
						) 
					values 
						(
							'$shiftId',
							'$timeId',
							'$teamXid',
							'$lineID',
							'$person',
							'$ordernum',
							'$subject',
							'".substr($subject, 0, 10)."',
							'$registerType',
							'線上預約',
							'$web_member_id', 
							'$uname', 
							'$sex',
							'$mobile',
							'$uname2',
							'$sex2',
							'$birthday',	
							'".date('Y-m-d H:i:s')."',
							'".date('Y-m-d H:i:s')."'
						) 
			";	
			//$rs3 = ConnectDB($DB, $sql3);
			//$web_x_register_id = ConnectDBInsterId($DB, $sql3);
			//file_put_contents("debug.txt", $sql3."\r\n", FILE_APPEND);
			$pdo = $pdoDB->prepare($sql3);
			$pdo->execute();
			$web_x_register_id = $pdoDB->lastInsertId();
			//file_put_contents("debug.txt", $web_x_register_id."\r\n", FILE_APPEND);
			if($web_x_register_id) {
			
				$sql4 = "
					Select 
						a.*,
						c.subject as classSubject,
						d.subject as teamSubject
					From 
						web_x_register a
					Left Join 
						web_class c ON c.web_class_id = a.web_time_id 		
					Left Join 
						web_x_team d ON d.web_x_team_id = a.web_x_team_id
					Where
						a.web_x_register_id = :web_x_register_id
					order by 
						a.registerDate ASC, a.ordernum ASC
				";
				$excute = array(
					':web_x_register_id'        => $web_x_register_id,
				);
				$pdo = $pdoDB->prepare($sql4);
				$pdo->execute($excute);
				$row4 = $pdo->fetchAll(PDO::FETCH_ASSOC);
				/*
				$rs4 = ConnectDB($DB, $sql4);
				for ($i=0; $i<mysql_num_rows($rs4); $i++) {
					$row4 = mysql_fetch_assoc($rs4);
					$logInfo['error'] = 0;
					$logInfo['ordernum'] = $row4['ordernum'];
					$logInfo['registerId'] = $row4['web_x_register_id'];
					$logInfo['subject'] = $row4['subject'];
					$logInfo['classSubjct'] = $row4['classSubjct'];
					$logInfo['teamSubject'] = $row4['teamSubject'];
				}
				*/
				foreach($row4 as $key4 => $val4) {
					$logInfo['error'] = 0;
					$logInfo['ordernum'] = $val4['ordernum'];
					$logInfo['registerId'] = $val4['web_x_register_id'];
					$logInfo['subject'] = $val4['subject'];
					$logInfo['classSubject'] = $val4['classSubject'];
					$logInfo['teamSubject'] = $val4['teamSubject'];
				}
				die(json_encode($logInfo, JSON_UNESCAPED_UNICODE));
				
			} else {
				$req = array('error'=>'1', 'message'=> '系統維護中');
				die(json_encode($req, JSON_UNESCAPED_UNICODE));
			}	
			
			
			
		} else {
			
			if($uname != $oldUname) {
				$sql = "UPDATE web_member set uname = :uname Where web_member_id = :web_member_id ";
				//$rs = ConnectDB($DB, $sql);
				$excute = array(
					':uname'        => $uname,
					':web_member_id'        => $web_member_id,
				);
				$pdo = $pdoDB->prepare($sql);
				$pdo->execute($excute);	
			}
			
			if($mobile != $oldMobile) {
				$sql = "UPDATE web_member set mobile = :mobile Where web_member_id = :web_member_id ";
				//$rs = ConnectDB($DB, $sql);
				$excute = array(
					':mobile'        => $mobile,
					':web_member_id'        => $web_member_id,
				);
				$pdo = $pdoDB->prepare($sql);
				$pdo->execute($excute);
			}
			
			if($sex != $oldSex) {
				$sql = "UPDATE web_member set sex = :sex Where web_member_id = :web_member_id ";
				//$rs = ConnectDB($DB, $sql);
				$excute = array(
					':sex'        => $sex,
					':web_member_id'        => $web_member_id,
				);
				$pdo = $pdoDB->prepare($sql);
				$pdo->execute($excute);
			}
			/*
			$sql = "
				Select 
					main.lineID, 
					main.web_member_id ,
					xTeam.time as xTeamTime
				From 
					web_x_register as main
				Left Join
					web_x_team xTeam
				ON
					xTeam.web_x_team_id = main.web_x_team_id
				Where 
					main.subject like '%".$subject."%' 
				AND
					main.web_time_id = '".$timeId."'	
				AND 
					main.lineID = '".$lineID."' 			
				AND 
					main.order_name like '%".$uname."%' 
				AND 
					main.accept_name like '%".$uname2."%' 
				AND 
					main.paymentstatus = '未報到' 
				Order by 
					main.web_x_register_id DESC
			";
			*/
			$sql = "
				Select 
					main.lineID, 
					main.web_member_id ,
					xTeam.time as xTeamTime
				From 
					web_x_register as main
				Left Join
					web_x_team xTeam
				ON
					xTeam.web_x_team_id = main.web_x_team_id
				Where 
					main.subject like '%".$subject."%' 
				AND
					main.web_time_id = '".$timeId."'	
				AND 
					main.lineID = '".$lineID."' 
				AND 
					main.paymentstatus = '未報到' 
				Order by 
					main.web_x_register_id DESC
			";
			file_put_contents("debug.txt", $sql."\r\n", FILE_APPEND);
			$rs = ConnectDB($DB, $sql);
			$_web_member_id = mysql_result($rs, 0, "web_member_id");
			if($_web_member_id) {
				$req = array('error'=>'1', 'message'=> '此時段已有預約了，謝謝');
				die(json_encode($req, JSON_UNESCAPED_UNICODE));
			}
			
			/*
			$_timeAry = array(
				'10:30~11:30',
				'11:30~12:30',
				'12:30~13:30',
				'13:30~14:30',
				'14:30~15:30',
				'15:30~16:30',
				'16:30~17:30',
				'17:30~18:30',
				'18:30~19:30',
			);
			*/
			
			//已預約的時段
			$registerTimeRow = array();
			$isSql = "
				SELECT 
					main.*,
					xteam.subject as xteamSubject,
					xteam.time as xteamTime
				from 
					web_x_register as main
				left join
					web_x_team xteam
				on
					xteam.web_x_team_id = main.web_x_team_id
				WHERE 
					main.registerDate = '".substr($subject, 0, 10)."'
				AND
					main.web_time_id != '".$timeId."'	
				AND 
					main.lineID = '".$lineID."' 
				AND 
					main.order_name like '%".$uname."%' 
				AND 
					main.accept_name like '%".$uname2."%' 	
				AND
					main.paymentstatus != '取消' 
				AND
					main.paymentstatus != '已報到'
				AND
					main.paymentstatus != '已服務'	
			";
			//echo $isSql;
			$pdo3 = $pdoDB->prepare($isSql);
			$pdo3->execute();
			$registerRow = $pdo3->fetchAll(PDO::FETCH_ASSOC);

			foreach($registerRow as $registerKey => $registerVal) {

				if(array_search(substr($registerVal['subject'],16), $_timeAry) >= '0') {
					if($_timeAry[array_search(substr($registerVal['subject'],16), $_timeAry)] == substr($registerVal['subject'],16)) {
						$registerTimeRow[$registerVal['registerDate']][] = substr($registerVal['subject'],16);
						if($registerVal['xteamTime'] > 1) {
							for($i = 2; $i <= $registerVal['xteamTime']; $i++) {
								$searchKey = array_search(substr($registerVal['subject'],16), $_timeAry) + ($i - 1);
								$registerTimeRow[$registerVal['registerDate']][] = $_timeAry[$searchKey];
							}	
						}
					}	
					
				}
			}
			
			if(in_array(substr($subject,16), $registerTimeRow[substr($subject, 0, 10)])) {
				$req = array('error'=>'1', 'message'=> '此時段您已經有預約了，謝謝');
				die(json_encode($req, JSON_UNESCAPED_UNICODE));
			}
			//已預約的時段
			
			$sql = "Select ordernum, lineID, order_name, accept_name From web_x_register Where subject like '%".$subject."%' order by web_x_register_id DESC";
			$rs = ConnectDB($DB, $sql);
			
			$ordernum = mysql_result($rs, 0, "ordernum");
			$ordernum = ($ordernum) ? ($ordernum + 1) : 1;
			/*
			$sql3 = "
				Insert into 
					web_x_register 
						(
							web_shift_id,
							lineID,
							ordernum,
							subject,
							registerDate,
							states,
							web_member_id,
							order_name,
							order_mobile,
							accept_name, 
							cdate,
							date 
						) 
					values 
						(
							'$shiftId',
							'$lineID',	
							'$ordernum',
							'$subject',
							'".substr($subject, 0, 10)."',
							'線上預約',
							'$web_member_id', 
							'$uname', 
							'$mobile',
							'$uname2',	
							'".date('Y-m-d H:i:s')."',
							'".date('Y-m-d H:i:s')."'
						) 
			";	
			//$rs3 = ConnectDB($DB, $sql3);
			$web_x_register_id = ConnectDBInsterId($DB, $sql3);
			*/
			
			$sql3 = "
				Insert into 
					web_x_register 
						(
							web_shift_id,
							web_time_id,
							web_x_team_id,
							lineID,
							self,
							ordernum,
							subject,
							registerDate,
							registerType,
							states,
							web_member_id,
							order_name,
							order_sex,
							order_mobile,
							accept_name,
							accept_sex,
							accept_birthday,	
							cdate,
							date 
						) 
					values 
						(
							:shiftId,
							:timeId,
							:teamXid,
							:lineID,
							:self,	
							:ordernum,
							:subject,
							:registerDate,
							:registerType,
							:states,
							:web_member_id, 
							:uname,
							:sex,	
							:mobile,
							:uname2,
							:sex2,
							:birthday,	
							:cdate,
							:date
						) 
			";	
			$excute = array(
				':shiftId'        	=> $shiftId,
				':timeId'        	=> $timeId,
				':teamXid'        	=> $teamXid,
				':lineID'        	=> $lineID,
				':self'        		=> $person,
				':ordernum'        	=> $ordernum,
				':subject'        	=> $subject,
				':registerDate'     => substr($subject, 0, 10),
				':registerType'     => $registerType,
				':states'        	=> '線上預約',
				':web_member_id'    => $web_member_id,
				':uname'        	=> $uname,
				':sex'        		=> $sex,
				':mobile'        	=> $mobile,
				':uname2'        	=> $uname2,
				':sex2'        		=> $sex2,
				':birthday'        	=> $birthday,
				':cdate'        	=> date('Y-m-d H:i:s'),
				':date'        		=> date('Y-m-d H:i:s'),
			);
			$pdo = $pdoDB->prepare($sql3);
			$pdo->execute($excute);
			$web_x_register_id = $pdoDB->lastInsertId();
			
			if($web_x_register_id) {
			
				$sql4 = "
					Select 
						a.*,
						c.subject as classSubject,
						c.web_x_class_id as store_id,
						d.subject as teamSubject,
						e.subject as storeSubject
					From 
						web_x_register a
					Left Join 
						web_class c ON c.web_class_id = a.web_time_id 		
					Left Join 
						web_x_team d ON d.web_x_team_id = a.web_x_team_id
					Left Join 
						web_x_class e ON e.web_x_class_id = c.web_x_class_id	
					Where
						a.web_x_register_id = :web_x_register_id
					order by 
						a.registerDate ASC, a.ordernum ASC
				";
				$excute = array(
					':web_x_register_id'        => $web_x_register_id,
				);
				$pdo = $pdoDB->prepare($sql4);
				$pdo->execute($excute);
				$row4 = $pdo->fetchAll(PDO::FETCH_ASSOC);
				/*
				$rs4 = ConnectDB($DB, $sql4);
				for ($i=0; $i<mysql_num_rows($rs4); $i++) {
					$row4 = mysql_fetch_assoc($rs4);
					$logInfo['error'] = 0;
					$logInfo['ordernum'] = $row4['ordernum'];
					$logInfo['registerId'] = $row4['web_x_register_id'];
					$logInfo['subject'] = $row4['subject'];
					$logInfo['classSubjct'] = $row4['classSubjct'];
					$logInfo['teamSubject'] = $row4['teamSubject'];
				}
				*/
				foreach($row4 as $key4 => $val4) {
					$logInfo['error'] = 0;
					$logInfo['ordernum'] = $val4['ordernum'];
					$logInfo['registerId'] = $val4['web_x_register_id'];
					$logInfo['subject'] = $val4['subject'];
					$logInfo['classSubject'] = $val4['classSubject'];
					$logInfo['teamSubject'] = $val4['teamSubject'];
					$logInfo['store_id'] = $val4['store_id'];
					$logInfo['storeSubject'] = $val4['storeSubject'];
					$logInfo['accept_name'] = $val4['accept_name'];
				}
				//file_put_contents("debug.txt", json_encode($logInfo, JSON_UNESCAPED_UNICODE)."\r\n", FILE_APPEND);
				
				/*
				$sql = "SELECT GROUP_CONCAT(DISTINCT lineID SEPARATOR ',') AS lineIDAry FROM web_member WHERE lineID != '' AND (ifService IN(1) AND ifMessage IN(1) AND store_id = '".$logInfo['store_id']."') OR (store_id = '-1' AND ifService IN(1) AND ifMessage IN(1))";
				*/
				$sql = "SELECT GROUP_CONCAT(DISTINCT lineID SEPARATOR ',') AS lineIDAry FROM web_member WHERE lineID != '' AND pusher_id = '".$timeId."' OR ((ifService IN(1) AND ifMessage IN(1) AND store_id = '".$logInfo['store_id']."') OR (store_id = '-1' AND ifService IN(1) AND ifMessage IN(1)))";
				//file_put_contents("debug.txt", $sql."\r\n", FILE_APPEND);
				$pdo = $pdoDB->prepare($sql);
				$pdo->execute();
				$lineIDAry = $pdo->fetch(PDO::FETCH_ASSOC);
				//file_put_contents("debug.txt", json_encode($lineIDAry, JSON_UNESCAPED_UNICODE)."\r\n", FILE_APPEND);
				if($Init_Register_Push) {
					if(count($lineIDAry) && 1) {
						//$text = "客戶【".$logInfo['accept_name']."】預約".$productRow['csubject'].$productRow['subject'];
						$text = "【預約通知】\n門市：".$logInfo['storeSubject']."\n項目：".$logInfo['teamSubject']."\n設計師：".$logInfo['classSubject']."\n預約時間：".substr($logInfo['subject'],0,15)."\n到店時間：".substr($logInfo['subject'],16,5)."\n消費人：".$logInfo['accept_name'];
						//file_put_contents("debug.txt", $text."\r\n", FILE_APPEND);	
						$_REQUEST['ID'] = $lineIDAry['lineIDAry'];
						$_REQUEST['contnet'] = $text;
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL, "https://".$_SERVER['HTTP_HOST']."/api.php");
						curl_setopt($ch, CURLOPT_HEADER, false);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
						$PostData = $_REQUEST;
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $PostData);
						$temp = curl_exec($ch);
						// 關閉CURL連線
						curl_close($ch);
					}	
				}
				
				die(json_encode($logInfo, JSON_UNESCAPED_UNICODE));
				
				
			} else {
				$req = array('error'=>'1', 'message'=> '系統維護中');
				die(json_encode($req, JSON_UNESCAPED_UNICODE));
			}
			
		}
		
		
	}
	
	//預約掛號
	if ($action == "registerManager") {
		$person = intval($_POST["person"]);
		$lineID = ($_POST["lineID"]);
		$uname = trim($_POST["uname"]);
		$uname2 = ($person) ? $uname : trim($_POST["uname2"]);
		$birthday = ($_POST["birthday"]);
		$mobile = ($_POST["mobile"]);
		$shiftId = intval($_POST["shiftId"]);
		$subject = trim($_POST["date"]);
		$registerType = trim($_POST["registerType"]);
		$timeId = intval($_POST["timeId"]);
		$teamXid = intval($_POST["teamXid"]);
		
		if($lineID) {
			$sql = "Select * From web_member Where lineID = :lineID ";
			$excute = array(
				':lineID'        => $lineID,
			);
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute($excute);
			$row = $pdo->fetch(PDO::FETCH_ASSOC);
		} else {
			$sql = "Select * From web_member Where uname = :uname AND mobile = :mobile ";
			$excute = array(
				':uname'        => $uname,
				':mobile'        => $mobile,
			);
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute($excute);
			$row = $pdo->fetch(PDO::FETCH_ASSOC);
		}
		
		$web_member_id = $row['web_member_id'];
		$oldUname = $row['uname'];
		$oldBirthday = $row['birthday'];
		$oldMobile = $row['mobile'];
	
		//沒有加入會員
		if(!$web_member_id) {
			/*
			$insertSql = "
				Insert into 
					web_member 
						(
							lineID, 
							uname,
							birthday, 
							mobile, 
							cdate
						) 
					values 
						(
							'$lineID', 
							'$uname',
							'$birthday',
							'$mobile', 
							'".date('Y-m-d H:i:s')."'
						) 
			";	
			
			$web_member_id = ConnectDBInsterId($DB, $insertSql);
			*/
			$insertSql = "
				Insert into 
					web_member 
						(
							lineID,
							store_id,	
							uname,
							birthday, 
							mobile, 
							cdate
						) 
					values 
						(
							:lineID,
							:store_id,	
							:uname,
							:birthday,
							:mobile, 
							:cdate
						) 
			";	
			$excute = array(
				':lineID'        => $lineID,
				':store_id'        => store_Id,
				':uname'        => $uname,
				':birthday'        => $birthday,
				':mobile'        => $mobile,
				':cdate'        => date('Y-m-d H:i:s'),
			);
			$pdo = $pdoDB->prepare($insertSql);
			$pdo->execute($excute);
			$web_member_id = $pdoDB->lastInsertId();
			
			$sql = "Select ordernum, web_member_id, lineID, order_name, accept_name From web_x_register Where subject like '%".$subject."%' order by web_x_register_id DESC";
			$rs = ConnectDB($DB, $sql);
			
			$ordernum = mysql_result($rs, 0, "ordernum");
			$ordernum = ($ordernum) ? ($ordernum + 1) : 1;
			
			
			$sql3 = "
				Insert into 
					web_x_register 
						(
							web_shift_id,
							web_time_id,
							web_x_team_id,
							lineID,
							self,
							ordernum,
							subject,
							registerDate,
							registerType,
							states,
							web_member_id,
							order_name,
							order_mobile,
							accept_name,
							accept_birthday,
							cdate,
							date 
						) 
					values 
						(
							'$shiftId',
							'$timeId',
							'$teamXid',
							'$lineID',
							'$person',
							'$ordernum',
							'$subject',
							'".substr($subject, 0, 10)."',
							'$registerType',
							'線上預約',
							'$web_member_id', 
							'$oldUname', 
							'$mobile',
							'$uname2',
							'$birthday',	
							'".date('Y-m-d H:i:s')."',
							'".date('Y-m-d H:i:s')."'
						) 
			";	
			//$rs3 = ConnectDB($DB, $sql3);
			//$web_x_register_id = ConnectDBInsterId($DB, $sql3);
			//file_put_contents("debug.txt", $sql3."\r\n", FILE_APPEND);
			$pdo = $pdoDB->prepare($sql3);
			$pdo->execute();
			$web_x_register_id = $pdoDB->lastInsertId();
			//file_put_contents("debug.txt", $web_x_register_id."\r\n", FILE_APPEND);
			if($web_x_register_id) {
			
				$sql4 = "
					Select 
						a.*,
						c.subject as classSubject,
						d.subject as teamSubject
					From 
						web_x_register a
					Left Join 
						web_class c ON c.web_class_id = a.web_time_id 		
					Left Join 
						web_x_team d ON d.web_x_team_id = a.web_x_team_id
					Where
						a.web_x_register_id = :web_x_register_id
					order by 
						a.registerDate ASC, a.ordernum ASC
				";
				$excute = array(
					':web_x_register_id'        => $web_x_register_id,
				);
				$pdo = $pdoDB->prepare($sql4);
				$pdo->execute($excute);
				$row4 = $pdo->fetchAll(PDO::FETCH_ASSOC);
				/*
				$rs4 = ConnectDB($DB, $sql4);
				for ($i=0; $i<mysql_num_rows($rs4); $i++) {
					$row4 = mysql_fetch_assoc($rs4);
					$logInfo['error'] = 0;
					$logInfo['ordernum'] = $row4['ordernum'];
					$logInfo['registerId'] = $row4['web_x_register_id'];
					$logInfo['subject'] = $row4['subject'];
					$logInfo['classSubjct'] = $row4['classSubjct'];
					$logInfo['teamSubject'] = $row4['teamSubject'];
				}
				*/
				foreach($row4 as $key4 => $val4) {
					$logInfo['error'] = 0;
					$logInfo['ordernum'] = $val4['ordernum'];
					$logInfo['registerId'] = $val4['web_x_register_id'];
					$logInfo['subject'] = $val4['subject'];
					$logInfo['classSubject'] = $val4['classSubject'];
					$logInfo['teamSubject'] = $val4['teamSubject'];
				}
				die(json_encode($logInfo, JSON_UNESCAPED_UNICODE));
				
			} else {
				$req = array('error'=>'1', 'message'=> '系統維護中');
				die(json_encode($req, JSON_UNESCAPED_UNICODE));
			}	
			
			
			
		} else {
			/*
			if($uname != $oldUname) {
				$sql = "UPDATE web_member set uname = :uname Where web_member_id = :web_member_id ";
				//$rs = ConnectDB($DB, $sql);
				$excute = array(
					':uname'        => $uname,
					':web_member_id'        => $web_member_id,
				);
				$pdo = $pdoDB->prepare($sql);
				$pdo->execute($excute);	
			}
			*/
			if($mobile != $oldMobile) {
				$sql = "UPDATE web_member set mobile = :mobile Where web_member_id = :web_member_id ";
				//$rs = ConnectDB($DB, $sql);
				$excute = array(
					':mobile'        => $mobile,
					':web_member_id'        => $web_member_id,
				);
				$pdo = $pdoDB->prepare($sql);
				$pdo->execute($excute);
			}
			
			$sql = "
				Select 
					main.lineID, 
					main.web_member_id ,
					xTeam.time as xTeamTime
				From 
					web_x_register as main
				Left Join
					web_x_team xTeam
				ON
					xTeam.web_x_team_id = main.web_x_team_id
				Where 
					main.subject like '%".$subject."%' 
				AND
					main.web_time_id != '".$timeId."'
				AND 
					main.lineID = '".$lineID."' 
				AND 
					main.order_name like '%".$oldUname."%' 
				AND 
					main.accept_name like '%".$uname2."%' 
				AND 
					main.paymentstatus = '未報到' 
				Order by 
					main.web_x_register_id DESC
			";
			$rs = ConnectDB($DB, $sql);
			$_web_member_id = mysql_result($rs, 0, "web_member_id");
			if($_web_member_id) {
				$req = array('error'=>'1', 'message'=> '此時段您已經有預約了，謝謝');
				die(json_encode($req, JSON_UNESCAPED_UNICODE));
			}
			
			/*
			$_timeAry = array(
				'10:30~11:30',
				'11:30~12:30',
				'12:30~13:30',
				'13:30~14:30',
				'14:30~15:30',
				'15:30~16:30',
				'16:30~17:30',
				'17:30~18:30',
				'18:30~19:30',
			);
			*/
			
			//已預約的時段
			$registerTimeRow = array();
			$isSql = "
				SELECT 
					main.*,
					xteam.subject as xteamSubject,
					xteam.time as xteamTime
				from 
					web_x_register as main
				left join
					web_x_team xteam
				on
					xteam.web_x_team_id = main.web_x_team_id
				WHERE 
					main.registerDate = '".substr($subject, 0, 10)."'
				AND
					main.web_time_id != '".$timeId."'	
				AND 
					main.lineID = '".$lineID."' 
				AND 
					main.order_name like '%".$oldUname."%' 
				AND 
					main.accept_name like '%".$uname2."%' 	
				AND
					main.paymentstatus != '取消' 
				AND
					main.paymentstatus != '已報到'
				AND
					main.paymentstatus != '已服務'	
			";
			//echo $isSql;
			$pdo3 = $pdoDB->prepare($isSql);
			$pdo3->execute();
			$registerRow = $pdo3->fetchAll(PDO::FETCH_ASSOC);

			foreach($registerRow as $registerKey => $registerVal) {

				if(array_search(substr($registerVal['subject'],16), $_timeAry) >= '0') {
					if($_timeAry[array_search(substr($registerVal['subject'],16), $_timeAry)] == substr($registerVal['subject'],16)) {
						$registerTimeRow[$registerVal['registerDate']][] = substr($registerVal['subject'],16);
						if($registerVal['xteamTime'] > 1) {
							for($i = 2; $i <= $registerVal['xteamTime']; $i++) {
								$searchKey = array_search(substr($registerVal['subject'],16), $_timeAry) + ($i - 1);
								$registerTimeRow[$registerVal['registerDate']][] = $_timeAry[$searchKey];
							}	
						}
					}	
					
				}
			}
			
			if(in_array(substr($subject,16), $registerTimeRow[substr($subject, 0, 10)])) {
				$req = array('error'=>'1', 'message'=> '此時段您已經有預約了，謝謝2');
				die(json_encode($req, JSON_UNESCAPED_UNICODE));
			}
			//已預約的時段
			
			$sql = "Select ordernum, lineID, order_name, accept_name From web_x_register Where subject like '%".$subject."%' order by web_x_register_id DESC";
			$rs = ConnectDB($DB, $sql);
			
			$ordernum = mysql_result($rs, 0, "ordernum");
			$ordernum = ($ordernum) ? ($ordernum + 1) : 1;
			/*
			$sql3 = "
				Insert into 
					web_x_register 
						(
							web_shift_id,
							lineID,
							ordernum,
							subject,
							registerDate,
							states,
							web_member_id,
							order_name,
							order_mobile,
							accept_name, 
							cdate,
							date 
						) 
					values 
						(
							'$shiftId',
							'$lineID',	
							'$ordernum',
							'$subject',
							'".substr($subject, 0, 10)."',
							'線上預約',
							'$web_member_id', 
							'$uname', 
							'$mobile',
							'$uname2',	
							'".date('Y-m-d H:i:s')."',
							'".date('Y-m-d H:i:s')."'
						) 
			";	
			//$rs3 = ConnectDB($DB, $sql3);
			$web_x_register_id = ConnectDBInsterId($DB, $sql3);
			*/
			
			$sql3 = "
				Insert into 
					web_x_register 
						(
							web_shift_id,
							web_time_id,
							web_x_team_id,
							lineID,
							self,
							ordernum,
							subject,
							registerDate,
							registerType,
							states,
							web_member_id,
							order_name,
							order_mobile,
							accept_name,
							accept_birthday,	
							cdate,
							date 
						) 
					values 
						(
							:shiftId,
							:timeId,
							:teamXid,
							:lineID,
							:self,	
							:ordernum,
							:subject,
							:registerDate,
							:registerType,
							:states,
							:web_member_id, 
							:uname, 
							:mobile,
							:uname2,
							:birthday,	
							:cdate,
							:date
						) 
			";	
			$excute = array(
				':shiftId'        	=> $shiftId,
				':timeId'        	=> $timeId,
				':teamXid'        	=> $teamXid,
				':lineID'        	=> $lineID,
				':self'        		=> $person,
				':ordernum'        	=> $ordernum,
				':subject'        	=> $subject,
				':registerDate'     => substr($subject, 0, 10),
				':registerType'     => $registerType,
				':states'        	=> '線上預約',
				':web_member_id'    => $web_member_id,
				':uname'        	=> $oldUname,
				':mobile'        	=> $mobile,
				':uname2'        	=> $uname2,
				':birthday'        	=> $birthday,
				':cdate'        	=> date('Y-m-d H:i:s'),
				':date'        		=> date('Y-m-d H:i:s'),
			);
			$pdo = $pdoDB->prepare($sql3);
			$pdo->execute($excute);
			$web_x_register_id = $pdoDB->lastInsertId();
			
			if($web_x_register_id) {
			
				$sql4 = "
					Select 
						a.*,
						c.subject as classSubject,
						c.web_x_class_id as store_id,
						d.subject as teamSubject,
						e.subject as storeSubject
					From 
						web_x_register a
					Left Join 
						web_class c ON c.web_class_id = a.web_time_id 		
					Left Join 
						web_x_team d ON d.web_x_team_id = a.web_x_team_id
					Left Join 
						web_x_class e ON e.web_x_class_id = c.web_x_class_id	
					Where
						a.web_x_register_id = :web_x_register_id
					order by 
						a.registerDate ASC, a.ordernum ASC
				";
				$excute = array(
					':web_x_register_id'        => $web_x_register_id,
				);
				$pdo = $pdoDB->prepare($sql4);
				$pdo->execute($excute);
				$row4 = $pdo->fetchAll(PDO::FETCH_ASSOC);
				/*
				$rs4 = ConnectDB($DB, $sql4);
				for ($i=0; $i<mysql_num_rows($rs4); $i++) {
					$row4 = mysql_fetch_assoc($rs4);
					$logInfo['error'] = 0;
					$logInfo['ordernum'] = $row4['ordernum'];
					$logInfo['registerId'] = $row4['web_x_register_id'];
					$logInfo['subject'] = $row4['subject'];
					$logInfo['classSubjct'] = $row4['classSubjct'];
					$logInfo['teamSubject'] = $row4['teamSubject'];
				}
				*/
				foreach($row4 as $key4 => $val4) {
					$logInfo['error'] = 0;
					$logInfo['ordernum'] = $val4['ordernum'];
					$logInfo['registerId'] = $val4['web_x_register_id'];
					$logInfo['subject'] = $val4['subject'];
					$logInfo['classSubject'] = $val4['classSubject'];
					$logInfo['teamSubject'] = $val4['teamSubject'];
					$logInfo['store_id'] = $val4['store_id'];
					$logInfo['storeSubject'] = $val4['storeSubject'];
					$logInfo['accept_name'] = $val4['accept_name'];
				}
				//file_put_contents("debug.txt", json_encode($logInfo, JSON_UNESCAPED_UNICODE)."\r\n", FILE_APPEND);
				/*
				$sql = "SELECT GROUP_CONCAT(DISTINCT lineID SEPARATOR ',') AS lineIDAry FROM web_member WHERE lineID != '' AND (ifService IN(1) AND ifMessage IN(1) AND store_id = '".$logInfo['store_id']."') OR (store_id = '-1' AND ifService IN(1) AND ifMessage IN(1))";
				*/
				$sql = "SELECT GROUP_CONCAT(DISTINCT lineID SEPARATOR ',') AS lineIDAry FROM web_member WHERE lineID != '' AND pusher_id = '".$timeId."' OR ((ifService IN(1) AND ifMessage IN(1) AND store_id = '".$logInfo['store_id']."') OR (store_id = '-1' AND ifService IN(1) AND ifMessage IN(1)))";
				$pdo = $pdoDB->prepare($sql);
				$pdo->execute();
				$lineIDAry = $pdo->fetch(PDO::FETCH_ASSOC);
				//file_put_contents("debug.txt", json_encode($lineIDAry, JSON_UNESCAPED_UNICODE)."\r\n", FILE_APPEND);
				if($Init_Register_Push) {
					if(count($lineIDAry) && 1) {
						//$text = "客戶【".$logInfo['accept_name']."】預約".$productRow['csubject'].$productRow['subject'];
						$text = "【預約通知】\n門市：".$logInfo['storeSubject']."\n項目：".$logInfo['teamSubject']."\n設計師：".$logInfo['classSubject']."\n預約時間：".substr($logInfo['subject'],0,15)."\n到店時間：".substr($logInfo['subject'],16,5)."\n消費人：".$logInfo['accept_name'];
						//file_put_contents("debug.txt", $text."\r\n", FILE_APPEND);	
						$_REQUEST['ID'] = $lineIDAry['lineIDAry'];
						$_REQUEST['contnet'] = $text;
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL, "https://".$_SERVER['HTTP_HOST']."/api.php");
						curl_setopt($ch, CURLOPT_HEADER, false);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
						$PostData = $_REQUEST;
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $PostData);
						$temp = curl_exec($ch);
						// 關閉CURL連線
						curl_close($ch);
					}
				}
				die(json_encode($logInfo, JSON_UNESCAPED_UNICODE));
				
			} else {
				$req = array('error'=>'1', 'message'=> '系統維護中');
				die(json_encode($req, JSON_UNESCAPED_UNICODE));
			}
			
		}
		
		
	}
	
	if($action == "confirm") {
		$registerId = intval($_POST["id"]);
		
		$sql = "Select * From web_x_register Where web_x_register_id = :web_x_register_id ";
		$excute = array(
			':web_x_register_id'        => $registerId,
		);
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute($excute);
		$row = $pdo->fetch(PDO::FETCH_ASSOC);
		//$rs = ConnectDB($DB, $sql);
		//if(mysql_num_rows($rs)) {
		if(count($row)) {	
			$req = array('error'=>0);	
		} else {
			$req = array('error'=>'1', 'message'=> '此預約已刪除');
		}	
		die(json_encode($req, JSON_UNESCAPED_UNICODE));
		
	}
	
	if($action == "remove") {
		$registerId = intval($_POST["id"]);
		
		$sql = "Delete From web_x_register Where web_x_register_id = :web_x_register_id ";
		$excute = array(
			':web_x_register_id'        => $registerId,
		);
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute($excute);
		//$rs = ConnectDB($DB, $sql);
		$req = array('error'=> 0, 'message'=> '我要預約');
		die(json_encode($req, JSON_UNESCAPED_UNICODE));
		
	}
	
	if($action == "userInfo") {
		$lineID = trim($_POST["lineId"]);
		$collect = trim($_POST["request"]);
		$sql = "Select * From web_member Where lineID = :lineID ";
		$excute = array(
			':lineID'        => $lineID,
		);
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute($excute);
		$row = $pdo->fetch(PDO::FETCH_ASSOC);
		/*
		$rs = ConnectDB($DB, $sql);
		$web_member_id = mysql_result($rs, 0, "web_member_id");
		$oldUname = mysql_result($rs, 0, "uname");
		$oldBirthday = mysql_result($rs, 0, "birthday");
		$oldMobile = mysql_result($rs, 0, "mobile");
		*/
		$web_member_id = $row["web_member_id"];
		$oldIfOverseas = $row['ifOverseas'];
		$oldLoginID = $row['loginID'];
		$oldUname = $row["uname"];
		$oldBirthday = $row["birthday"];
		$oldMobile = $row["mobile"];
		$oldSex = $row["sex"];
		$oldIfAccept = $row['ifAccept'];
		$oldIfUseSelf = $row['ifUseSelf'];
		
		$sql = "
			Select 
				SUM(b.dimension) as sum
			From 
				web_x_order a
			Left Join 
				web_order b ON b.web_x_order_ordernum = a.ordernum
			Left Join 
				web_product c ON c.web_product_id = b.web_product_id
			Left Join 
				web_x_product d ON d.web_x_product_id = c.web_x_product_id
			Left Join 
				web_xx_product e ON e.web_xx_product_id = d.web_xx_product_id	
			Where 
				a.web_member_id = '".$web_member_id."' 
			AND
				a.states = '訂單成立'
			AND
				a.paymentstatus = '付款成功'
			AND
				d.web_xx_product_id IN (2)
			order by 
				a.web_x_order_id desc 
		";
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute();
		$storedTotal = $pdo->fetch(PDO::FETCH_ASSOC);
		
		$sql = "
			Select 
				a.ordernum,
				a.total,
				b.price,
				a.web_birthday_money,
				a.all_discount
			From 
				web_x_order a
			Left Join 
				web_order b ON b.web_x_order_ordernum = a.ordernum
			Left Join 
				web_product c ON c.web_product_id = b.web_product_id
			Left Join 
				web_x_product d ON d.web_x_product_id = c.web_x_product_id
			Left Join 
				web_xx_product e ON e.web_xx_product_id = d.web_xx_product_id	
			Where 
				a.web_member_id = '".$web_member_id."' 
			AND
				a.states = '訂單成立'
			AND
				a.paymentstatus = '付款成功'
			AND
				d.web_xx_product_id IN (3)
			order by 
				a.web_x_order_id desc 
		";
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute();
		$useTotalRow = $pdo->fetchAll(PDO::FETCH_ASSOC);
		$useTotalArr = array();
		foreach($useTotalRow as $key => $useTotal) {
			$useTotalArr[$useTotal['ordernum']] = $useTotal['total'];
		}
		$balance = abs($storedTotal['sum'] - array_sum($useTotalArr));
		
		$sql = "
			Select 
				SUM(a.money) as sum,
				SUM(a.usemoney) as useSum
			From 
				web_bonus a
			Left Join 
				web_x_order b ON b.ordernum = a.web_x_order_from_ordernum
			Where 
				a.web_member_id = '".$web_member_id."'
			AND
				a.sdate <= '".date('Y-m-d')."'
			AND
				a.edate >= '".date('Y-m-d')."'
			AND
				b.states = '訂單成立'
			AND
				b.paymentstatus = '付款成功'
			order by 
				a.web_bonus_id desc 
		";
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute();
		$bonusTotal = $pdo->fetch(PDO::FETCH_ASSOC);
		/*
		$sql = "
			Select 
				SUM(a.subtotal) as sum
			From 
				web_x_convert a
			Where 
				a.web_member_id = '".$web_member_id."'
			AND
				a.states = '兌換成立'
			order by 
				a.web_x_order_id desc 
		";
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute();
		$useBonusTotal = $pdo->fetch(PDO::FETCH_ASSOC);
		*/
		//$bonus = abs($bonusTotal['sum'] - $useBonusTotal['sum']);
		$bonus = abs($bonusTotal['sum'] - $bonusTotal['useSum']);
		
		
		//tag
		if($collect) {
			$collectAry = json_decode($collect, true);
			$newTags['tags'] = array();
			foreach($collectAry as $key => $collect) {
				if($key == 'web_qrcode_id') {
					//寫入掃描記錄－會員
					if($collect && $web_member_id) {
						$sql = "
							Select 
								member
							From 
								web_qrcode a
							Where 
								a.web_qrcode_id = '".$collect."'
						";
						$pdo = $pdoDB->prepare($sql);
						$pdo->execute();
						$qrcodeMemberRow = $pdo->fetch(PDO::FETCH_ASSOC);
						if($qrcodeMemberRow['member']) {
							$qrcodeMemberArr = explode(',', $qrcodeMemberRow['member']);
							if(in_array($web_member_id, $qrcodeMemberArr)) {
								$qrcodeMember = $qrcodeMemberArr;
							} else {	
								$qrcodeMemberArr[count($qrcodeMemberArr)] = $web_member_id;
								$qrcodeMember = $qrcodeMemberArr;
							}
							$sql = "UPDATE `web_qrcode` SET `member` = '".implode(',', $qrcodeMember)."' Where web_qrcode_id = '".$collect."'";
							$pdo = $pdoDB->prepare($sql);
							$pdo->execute();
						} else {
							$sql = "UPDATE `web_qrcode` SET `member` = '".$web_member_id."' Where web_qrcode_id = '".$collect."'";
							$pdo = $pdoDB->prepare($sql);
							$pdo->execute();
						}	
					}
					continue;
				}
				if($key == 'tags') {
					$collectTagAry = explode(',', $collect);
					$oldTagAry = explode(',', $row['tags']);
					$newTagsAry = array_merge($collectTagAry, $oldTagAry);
					$newTags['tags'] = array_unique($newTagsAry);
				} else {
					$sql = "
						Select 
							web_tag_id
						From 
							web_tag a
						Where 
							a.subject LIKE '%".$collect."'
					";
					$pdo = $pdoDB->prepare($sql);
					$pdo->execute();
					$tagHaved = $pdo->fetch(PDO::FETCH_ASSOC);
					if($tagHaved['web_tag_id']) {
						$newTags['other'][] = $tagHaved['web_tag_id'];
					} else {
						if($key != 'store') {
							$insertSql = "
								Insert into 
									web_tag 
										(
											web_x_tag_id, 
											ifShow,
											subject
										) 
									values 
										(
											'3', 
											'1',
											'$collect'
										) 
							";
						} else {
							$insertSql = "
								Insert into 
									web_tag 
										(
											web_x_tag_id, 
											ifShow,
											subject
										) 
									values 
										(
											'4', 
											'1',
											'$collect'
										) 
							";
						}	
						
						$web_tag_id = ConnectDBInsterId($DB, $insertSql);
						$newTags['other'][] = $web_tag_id;
					}
				}
				
				$finalTags = array_merge($newTags['tags'], $newTags['other']);
				
				$sql = "UPDATE web_member set tags = :tags Where lineID = :lineID ";
				$excute = array(
					':lineID'        			=> $lineID,
					':tags'        				=> implode(',', array_unique($finalTags)),
				);
				$pdo = $pdoDB->prepare($sql);
				$pdo->execute($excute);
			}
		}
		
		//生日禮
		if($Init_Birthday_Money && $oldBirthday) {
			$birthdayMonth = date('m', strtotime($oldBirthday));

			if($birthdayMonth == date('m')) {	
				$sql = "
					Select 
						web_member_money_id,
						kind,
						money
					From 
						web_member_money a
					Where 
						a.sdate >= '".date('Y-m-01')."'
					AND
						a.edate >= '".date('Y-m-t')."'
					AND
						a.web_member_id = '".$web_member_id."'
					Order By
						web_member_money_id DESC
				";
				$pdo = $pdoDB->prepare($sql);
				$pdo->execute();
				$memberMoneyHaved = $pdo->fetch(PDO::FETCH_ASSOC);
				$showMemberMoneyTag = ($memberMoneyHaved['web_member_money_id']) ? 0 : 1;
				$birthdayMoney = ($memberMoneyHaved['web_member_money_id']) ? $memberMoneyHaved['money'] : $Init_Birthday_Money;
				$memberMoneyKind = $memberMoneyHaved['kind'];
			} else {
				$showMemberMoneyTag = 0;
			}	
			
		}
		
		//if(mysql_num_rows($rs)) {
		if(count($row)) {	
			if(preg_match('/^[\x7f-\xff]+$/', $oldUname)) {
				$logInfo['error'] = 0;
				$logInfo['web_member_id'] = $web_member_id;
				$logInfo['ifOverseas'] = $oldIfOverseas;
				$logInfo['loginID'] = $oldLoginID;
				$logInfo['uname'] = $oldUname;
				$logInfo['sex'] = $oldSex;
				$logInfo['birthday'] = $oldBirthday;
				$logInfo['mobile'] = $oldMobile;
				$logInfo['balance'] = number_format($balance);
				$logInfo['bonus'] = ($bonus);
				$logInfo['ifAccept'] = $oldIfAccept;
				$logInfo['store_id'] = $row['store_id'];
				$logInfo['ifUseSelf'] = $oldIfUseSelf;
				$logInfo['birthdayMoney'] = $birthdayMoney;
				$logInfo['showMemberMoneyTag'] = $showMemberMoneyTag;
				$logInfo['memberMoneyKind'] = $memberMoneyKind;
				$_SESSION[$_SESSION['token']]['uname'] = $logInfo['uname'];
				$_SESSION[$_SESSION['token']]['birthday'] = $logInfo['birthday'];
				$_SESSION[$_SESSION['token']]['mobile'] = $logInfo['mobile'];
				$_SESSION[$_SESSION['token']]['balance'] = $logInfo['balance'];
				$_SESSION[$_SESSION['token']]['bonus'] = $logInfo['bonus'];
				$_SESSION[$_SESSION['token']]['store_id'] = $row['store_id'];
				$_SESSION[$_SESSION['token']]['birthdayMoney'] = $logInfo['birthdayMoney'];
				$_SESSION[$_SESSION['token']]['showMemberMoneyTag'] = $showMemberMoneyTag;
				$_SESSION[$_SESSION['token']]['memberMoneyKind'] = $memberMoneyKind;
				$_SESSION['store_id'] = $row['store_id'];
				
			} else {
				$logInfo['error'] = 1;
			}	
		}	
		die(json_encode($logInfo, JSON_UNESCAPED_UNICODE));
		
	}
	
	//各店的服務項目
	if($action == "xClassClickBySearch") {
		$xClassVal = trim($_POST["xClassVal"]);
		$teamHaveClass = 0;
		$tpl = "";
		$defaultKey = 0;
		$tpl .= "<input type=\"radio\" id=\"r01\" value=\"-1\" name=\"class\" checked=\"checked\" data-text=\"All\">";
		$tpl .= "<label for=\"r01\">All</label>";
		foreach($classRow as $classKey => $classVal) {
			if($xClassVal != $classVal['web_x_class_id']) {
				continue;
			}
			
			//$defaultChecked = ($defaultKey == 0) ? 'checked=\"checked\"' : null;
			
			$tpl .= "<input type=\"radio\" id=\"r".$classKey."\" value=\"".$classVal['web_class_id']."\" name=\"class\" ". $defaultChecked." data-text=\"".$classVal['subject']."\">";
			$tpl .= "<label for=\"r".$classKey."\">".$classVal['subject']."</label>";
			
			$defaultKey++;
		}
		$logInfo['error'] = 0;
		$logInfo['tpl'] = $tpl;
		die(json_encode($logInfo, JSON_UNESCAPED_UNICODE));
		
	}
	
	//各店的服務項目
	if($action == "xClassClickBySearch2") {
		$xClassVal = trim($_POST["xClassVal"]);
		$teamHaveClass = 0;
		$tpl = "";
		$defaultKey = 0;
		//$tpl .= "<input type=\"radio\" id=\"r01\" value=\"-1\" name=\"class\" checked=\"checked\" data-text=\"All\">";
		//$tpl .= "<label for=\"r01\">All</label>";
		foreach($classRow as $classKey => $classVal) {
			if($xClassVal != $classVal['web_x_class_id']) {
				continue;
			}
			
			$defaultChecked = ($defaultKey == 0) ? 'checked="checked"' : null;
			
			$tpl .= "<input type=\"radio\" id=\"r".$classKey."\" value=\"".$classVal['web_class_id']."\" name=\"class\" ". $defaultChecked." data-text=\"".$classVal['subject']."\">";
			$tpl .= "<label for=\"r".$classKey."\">".$classVal['subject']."</label>";
			
			$defaultKey++;
		}
		$logInfo['error'] = 0;
		$logInfo['tpl'] = $tpl;
		die(json_encode($logInfo, JSON_UNESCAPED_UNICODE));
		
	}
	
	//各科目的看診醫師
	if($action == "classClickBySearch") {
		$classVal = trim($_POST["classVal"]);
		$teamHaveClass = 0;
		$tpl = "";
		$defaultKey = 0;
		//$tpl .= "<input type=\"radio\" id=\"r01_1\" value=\"-1\" name=\"team\" checked=\"checked\" data-text=\"All\">";
		//$tpl .= "<label for=\"r01_1\">All</label>";
		foreach($teamRow as $teamKey => $teamVal) {
			/*
			$lineInfoAry = explode('#####', $teamVal['lineInfo']);
			
			foreach($lineInfoAry as $$lineInfoKey => $lineInfoVal) {
				$lineInfoRowAry[$teamVal['web_team_id']][] = explode('@#@', $lineInfoVal);
				//$listTimeByTeam[$teamVal][]
			}	
			
			$teamHaveClass = SearchMultidimensionalArray($lineInfoRowAry[$teamVal['web_team_id']], 2, $classVal);
			if(!$teamHaveClass) {
				continue;
			}
			*/
			$defaultChecked = ($defaultKey == 0) ? 'checked="checked"' : null;
			
			/*
			$tpl .= "<input type=\"radio\" id=\"r1-".$teamKey."\" value=\"".$teamVal['web_team_id']."\" name=\"team\" ". $defaultChecked." data-text=\"".$teamVal['subject'].$teamVal['web_x_team_subject']."\">";
			$tpl .= "<label for=\"r1-".$teamKey."\">".$teamVal['subject'].$teamVal['web_x_team_subject']."</label>";
			*/
			$tpl .= "<input type=\"radio\" id=\"r1-".$teamKey."\" value=\"".$teamVal['web_x_team_id']."\" name=\"team\" ". $defaultChecked." data-text=\"".$teamVal['subject']."\">";
			$tpl .= "<label for=\"r1-".$teamKey."\">".$teamVal['subject']."</label>";
			
			$defaultKey++;
		}
		$logInfo['error'] = 0;
		$logInfo['tpl'] = $tpl;
		die(json_encode($logInfo, JSON_UNESCAPED_UNICODE));
		
	}
	
	//今日叫號查詢 - 報到確認
	if($action == "todaySearch") {
		$type = trim($_POST["type"]);
		$timeVal = trim($_POST["timeVal"]);
		$classVal = trim($_POST["classVal"]);
		$teamVal = trim($_POST["teamVal"]);
		
		$accept_name = trim($_POST["accept_name"]);
		$accept_birthday = trim($_POST["accept_birthday"]);
		$accept_mobile = trim($_POST["accept_mobile"]);
		
		$sql = "
			SELECT 
				SQL_CALC_FOUND_ROWS a.*,
				c.subject as classSubject,
				e.subject as teamXsubject,
				e.time as teamXtime,
				f.uname as funame,
				f.birthday as fbirthday,
				f.mobile as fmobile	
			FROM 
				web_x_register a
			Left Join 
				web_class c ON c.web_class_id = a.web_time_id		
			Left Join 
				web_x_team e ON e.web_x_team_id = a.web_x_team_id
			Left Join 
				web_member f ON f.uname = a.order_name AND f.mobile = a.order_mobile			
			where 
				a.registerDate = :registerDate  
		";
	if($classVal != '-1') {	
		$sql .= "	
			AND
				a.web_time_id = :web_time_id
		";
	}	
	
	if($teamVal != '-1') {	
		$sql .= "	
			AND
				a.web_x_team_id = :web_x_team_id
		";
	}	
		
	if($accept_name) {	
		$sql .= "
			AND 
				a.accept_name LIKE :accept_name
		";	
	}
	if($accept_birthday) {	
		$sql .= "
			AND 
				a.accept_birthday = :accept_birthday
		";	
	}
	if($accept_mobile) {	
		$sql .= "
			AND 
				a.order_mobile = :order_mobile
		";	
	}
	if($type) {	
		$sql .= "
			AND 
				a.paymentstatus = :paymentstatus
		";
	} else {
		$sql .= "
			AND 
				a.paymentstatus != :paymentstatus
		";
	}	
		$sql .= "
			Group BY
				a.web_x_register_id
			ORDER BY 
				a.subject ASC,
				a.registerDate ASC
		";
		$excute = array(
			':registerDate'			=> date('Y-m-d'),
			':paymentstatus'		=> '未報到',
			//':subject'        		=> $timeVal,
			//':web_time_id'			=> $classVal,
			//':web_x_team_id'		=> $teamVal,
			//':accept_name'			=> ($accept_name) ? ' AND accept_name LIKE %'.$accept_name.'% ' : 1,
			//':accept_birthday'		=> ($accept_birthday) ? ' AND accept_birthday LIKE %'.$accept_birthday.'% ' : ' AND 1 ',
			//':accept_mobile'		=> ($accept_mobile) ? ' AND accept_mobile LIKE %'.$accept_mobile.'% ' : 1,
		);
		
		$excute = ($accept_name) ? $excute+array(':accept_name' => '%'.$accept_name.'%') : $excute;
		$excute = ($accept_birthday) ? $excute+array(':accept_birthday' => $accept_birthday) : $excute;
		$excute = ($accept_mobile) ? $excute+array(':order_mobile' => $accept_mobile) : $excute;
		$excute = ($classVal != '-1') ? $excute+array(':web_time_id' => $classVal) : $excute;
		$excute = ($teamVal != '-1') ? $excute+array(':web_x_team_id' => $teamVal) : $excute;
		
		//$debug = new Helper();
		//echo $test = $debug::debugPDO($sql, $excute);
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute($excute);
		$row3 = $pdo->fetchAll(PDO::FETCH_ASSOC);
		$tpl = '';
		$tpl2 = '';
		$tpl3 = '';
		if(!count($row3)) {
			$tpl = "<center><h2>無預約記錄</h2></center>";
		} else {
			foreach($row3 as $rowKey3 => $rowVal3) {
				$person = ($rowVal3['self']) ? '本人預約' : '非本人預約';
				$dayName = array(
					1 => '一', 2 => '二', 3 => '三', 4 => '四', 5 => '五', 6 => '六', 7 => '日',
				);
				
				$tpl .= "<div class=\"tr\">";
				$tpl .= "    <div class=\"td\">".$rowVal3['ordernum']."</div>";
				$tpl .= "    <div class=\"td\">".$rowVal3['accept_name']."</div>";
				$tpl .= "    <div class=\"td\">".$rowVal3['order_mobile']."</div>";
				$tpl .= "    <!--<div class=\"td color2\">".substr($rowVal3['subject'],16)."</div>-->";
				$tpl .= "    <div class=\"td color2\">".$rowVal3['paymentstatus']."</div>";
				$tpl .= "    <div class=\"td color2\">".$rowVal3['teamXsubject']." ".$rowVal3['teamXtime']."HR</div>";
				$tpl .= "    <div class=\"td color2\">".$rowVal3['classSubject']."</div>";
				$tpl .= "    <div class=\"td right\">";
				$tpl .= "        <a href=\"#\" class=\"btn manager\" data-id=\"".$rowVal3['web_x_register_id']."\">管理</a>";
				$tpl .= "        <a href=\"#\" class=\"btn info\" data-id=\"".$rowVal3['web_x_register_id']."\">詳情</a>";
				$tpl .= "    </div>";
				$tpl .= "</div>";
				
				$tpl2 .= "<div class=\"popup popup_login\" data-id=\"".$rowVal3['web_x_register_id']."\">";
				$tpl2 .= "	<a class=\"close\" href=\"#\"></a>";
				$tpl2 .= "	<h2>報到管理</h2>";
				$tpl2 .= "	<a href=\"#\" class=\"btn items\" data-status=\"已報到\">已報到</a>";
				$tpl2.= "	<a href=\"#\" class=\"btn items\" data-status=\"取消\">預約取消</a>";
				$tpl2 .= "	<a href=\"#\" class=\"btn submit disable\" onClick=\"active(this);\">確認</a>";
				$tpl2 .= "</div>";
				
				
				$tpl2 .= "<div class=\"popup popup_info\" data-id=\"".$rowVal3['web_x_register_id']."\">";
				$tpl2 .= "	<a class=\"close\" href=\"#\"></a>";
				$tpl2 .= "	<h2>詳細資訊</h2>";
				$tpl2 .= "	<div class=\"content\">";
				$tpl2 .= "		<div class=\"info\">";
				$tpl2 .= "			<div class=\"row\">";
				$tpl2 .= "				<div class=\"name\">".$rowVal3['accept_name']."</div>";
				$tpl2 .= "				<div class=\"value\">電話：".$rowVal3['order_mobile']." <br> 生日：".$rowVal3['accept_birthday']."</div>";
				$tpl2 .= "			</div>";
				$tpl2 .= "		</div>";
				$tpl2 .= "		<div class=\"row\">";
				$tpl2 .= "			<div class=\"name\">類型</div>";
				$tpl2 .= "			<div class=\"value\">".$person."</div>";
				$tpl2 .= "		</div>";
				$tpl2 .= "		<div class=\"row\">";
				$tpl2 .= "			<div class=\"name\">項目</div>";
				$tpl2 .= "			<div class=\"value\">".$rowVal3['teamXsubject']." ".$rowVal3['teamXtime']."HR</div>";
				$tpl2 .= "		</div>";
				$tpl2 .= "		<div class=\"row\">";
				$tpl2 .= "			<div class=\"name\">設計師</div>";
				$tpl2 .= "			<div class=\"value\">".$rowVal3['classSubject']."</div>";
				$tpl2 .= "		</div>";
				$tpl2 .= "		<div class=\"row\">";
				$tpl2 .= "			<div class=\"name\">日期</div>";
				$tpl2 .= "			<div class=\"value\">".$rowVal3['registerDate']." (".$dayName[date('N', strtotime(str_replace('-', '/', $rowVal3['registerDate'])))].")</div>";
				$tpl2 .= "		</div>";
				$tpl2 .= "		<div class=\"row\">";
				$tpl2 .= "			<div class=\"name\">時間</div>";
				if($rowVal3['teamXtime'] > 1) {
					$tpl2 .= "			<div class=\"value\">".substr($rowVal3['subject'],16, 6).ceil(substr($rowVal3['subject'],22, 2)+($rowVal3['teamXtime']-1)).substr($rowVal3['subject'],24)."</div>";
				} else {
					$tpl2 .= "			<div class=\"value\">".substr($rowVal3['subject'], 16)."</div>";
				}	
				$tpl2 .= "		</div>";
				/*
				$tpl2 .= "		<div class=\"row\">";
				$tpl2 .= "			<div class=\"name\">看診人姓名</div>";
				$tpl2 .= "			<div class=\"value\">".$rowVal3['accept_name']."</div>";
				$tpl2 .= "		</div>";
				$tpl2 .= "		<div class=\"row\">";
				$tpl2 .= "			<div class=\"name\">看診人生日</div>";
				$tpl2 .= "			<div class=\"value\"></div>";
				$tpl2 .= "		</div>";
				*/
				$tpl2 .= "		<div class=\"row\">";
				$tpl2 .= "			<div class=\"name\">預約方式</div>";
				$tpl2 .= "			<div class=\"value\">LINE</div>";
				$tpl2 .= "		</div>";
				$tpl2 .= "		<div class=\"row\">";
				$tpl2 .= "			<div class=\"name\">狀態</div>";
				$tpl2 .= "			<div class=\"value\">".$rowVal3['paymentstatus']."</div>";
				$tpl2 .= "		</div>";
				$tpl2 .= "	</div>";
				$tpl2 .= "</div>";
			}
		}
		
		//if(mysql_num_rows($rs)) {
		$logInfo['error'] = 0;
		$logInfo['tpl'] = $tpl;
		$logInfo['tpl2'] = $tpl2;
		
		die(json_encode($logInfo, JSON_UNESCAPED_UNICODE));
		
	}
	
	//今日叫號查詢 - 預約下次
	if($action == "todaySearch2") {
		$type = trim($_POST["type"]);
		$timeVal = trim($_POST["timeVal"]);
		$classVal = trim($_POST["classVal"]);
		$teamVal = trim($_POST["teamVal"]);
		
		$accept_name = trim($_POST["accept_name"]);
		$accept_birthday = trim($_POST["accept_birthday"]);
		$accept_mobile = trim($_POST["accept_mobile"]);
		
		$sql = "
			SELECT 
				SQL_CALC_FOUND_ROWS a.*,
				c.subject as classSubject,
				e.subject as teamXsubject,
				e.time as teamXtime,
				f.uname as funame,
				f.birthday as fbirthday,
				f.mobile as fmobile	
			FROM 
				web_x_register a
			Left Join 
				web_class c ON c.web_class_id = a.web_time_id		
			Left Join 
				web_x_team e ON e.web_x_team_id = a.web_x_team_id
			Left Join 
				web_member f ON f.uname = a.order_name AND f.mobile = a.order_mobile			
			where 
				a.registerDate = :registerDate  
		";
		
	if($classVal != '-1') {	
		$sql .= "	
			AND
				a.web_time_id = :web_time_id
		";
	}	
	
	if($teamVal != '-1') {	
		$sql .= "	
			AND
				a.web_x_team_id = :web_x_team_id
		";
	}
	
	if($accept_name) {	
		$sql .= "
			AND 
				a.accept_name LIKE :accept_name
		";	
	}
	if($accept_birthday) {	
		$sql .= "
			AND 
				a.accept_birthday = :accept_birthday
		";	
	}
	if($accept_mobile) {	
		$sql .= "
			AND 
				a.order_mobile = :order_mobile
		";	
	}
	if($type) {	
		$sql .= "
			AND 
				a.paymentstatus = :paymentstatus
		";
	} else {
		$sql .= "
			AND 
				a.paymentstatus != :paymentstatus
		";
	}	
		$sql .= "
			Group BY
				a.web_x_register_id
			ORDER BY 
				a.subject ASC,
				a.registerDate ASC
		";
		$excute = array(
			':registerDate'			=> date('Y-m-d'),
			':paymentstatus'		=> '未報到',
			//':subject'        		=> $timeVal,
			//':web_time_id'			=> $classVal,
			//':web_x_team_id'			=> $teamVal,
			//':accept_name'			=> ($accept_name) ? ' AND accept_name LIKE %'.$accept_name.'% ' : 1,
			//':accept_birthday'		=> ($accept_birthday) ? ' AND accept_birthday LIKE %'.$accept_birthday.'% ' : ' AND 1 ',
			//':accept_mobile'		=> ($accept_mobile) ? ' AND accept_mobile LIKE %'.$accept_mobile.'% ' : 1,
		);
		
		$excute = ($accept_name) ? $excute+array(':accept_name' => '%'.$accept_name.'%') : $excute;
		$excute = ($accept_birthday) ? $excute+array(':accept_birthday' => $accept_birthday) : $excute;
		$excute = ($accept_mobile) ? $excute+array(':order_mobile' => $accept_mobile) : $excute;
		$excute = ($classVal != '-1') ? $excute+array(':web_time_id' => $classVal) : $excute;
		$excute = ($teamVal != '-1') ? $excute+array(':web_x_team_id' => $teamVal) : $excute;
		
		
		//$debug = new Helper();
		//echo $test = $debug::debugPDO($sql, $excute);
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute($excute);
		$row3 = $pdo->fetchAll(PDO::FETCH_ASSOC);
		$tpl = '';
		$tpl2 = '';
		$tpl3 = '';
		if(!count($row3)) {
			$tpl = "<center><h2>無預約記錄</h2></center>";
		} else {
			foreach($row3 as $rowKey3 => $rowVal3) {
				$person = ($rowVal3['self']) ? '本人預約' : '非本人預約';
				$dayName = array(
					1 => '一', 2 => '二', 3 => '三', 4 => '四', 5 => '五', 6 => '六', 7 => '日',
				);
				
				$tpl .= "<div class=\"tr\">";
				//$tpl .= "    <div class=\"td\">".$rowVal3['ordernum']."</div>";
				$tpl .= "    <div class=\"td\">".($rowKey3+1)."</div>";
				$tpl .= "    <div class=\"td\">".$rowVal3['accept_name']."</div>";
				$tpl .= "    <div class=\"td\">".$rowVal3['order_mobile']."</div>";
				$tpl .= "    <div class=\"td color2\">".substr($rowVal3['subject'],16)."</div>";
				$tpl .= "    <div class=\"td color2\" style=\"padding-left:10px\">".$rowVal3['paymentstatus']."</div>";
				$tpl .= "    <div class=\"td color2\">設計師 ".$rowVal3['classSubject']."</div>";
				$tpl .= "    <div class=\"td color2\">".$rowVal3['teamXsubject']." ".$rowVal3['teamXtime']."HR</div>";
				$tpl .= "    <div class=\"td right\">";
				$tpl .= "        <a href=\"appointment.php?id=".$rowVal3['web_x_register_id']."\" class=\"btn\">管理</a>";
				$tpl .= "        <a href=\"appointment4.php?id=".$rowVal3['web_x_register_id']."\" class=\"btn\" data-id=\"".$rowVal3['web_x_register_id']."\">詳情</a>";
				$tpl .= "    </div>";
				$tpl .= "</div>";
				
				$tpl2 .= "<div class=\"popup popup_login\" data-id=\"".$rowVal3['web_x_register_id']."\">";
				$tpl2 .= "	<a class=\"close\" href=\"#\"></a>";
				$tpl2 .= "	<h2>報到管理</h2>";
				$tpl2 .= "	<a href=\"#\" class=\"btn items\" data-status=\"已報到\">已報到</a>";
				$tpl2.= "	<a href=\"#\" class=\"btn items\" data-status=\"取消\">預約取消</a>";
				$tpl2 .= "	<a href=\"#\" class=\"btn submit disable\" onClick=\"active(this);\">確認</a>";
				$tpl2 .= "</div>";
				
				
				$tpl2 .= "<div class=\"popup popup_info\" data-id=\"".$rowVal3['web_x_register_id']."\">";
				$tpl2 .= "	<a class=\"close\" href=\"#\"></a>";
				$tpl2 .= "	<h2>詳細資訊</h2>";
				$tpl2 .= "	<div class=\"content\">";
				$tpl2 .= "		<div class=\"info\">";
				$tpl2 .= "			<div class=\"row\">";
				$tpl2 .= "				<div class=\"name\">".$rowVal3['accept_name']."</div>";
				$tpl2 .= "				<div class=\"value\">電話：".$rowVal3['order_mobile']." <br> 生日：".$rowVal3['accept_birthday']."</div>";
				$tpl2 .= "			</div>";
				$tpl2 .= "		</div>";
				$tpl2 .= "		<div class=\"row\">";
				$tpl2 .= "			<div class=\"name\">類型</div>";
				$tpl2 .= "			<div class=\"value\">".$person."</div>";
				$tpl2 .= "		</div>";
				$tpl2 .= "		<div class=\"row\">";
				$tpl2 .= "			<div class=\"name\">項目</div>";
				$tpl2 .= "			<div class=\"value\">".$rowVal3['teamXsubject']." ".$rowVal3['teamXtime']."HR</div>";
				$tpl2 .= "		</div>";
				$tpl2 .= "		<div class=\"row\">";
				$tpl2 .= "			<div class=\"name\">設計師</div>";
				$tpl2 .= "			<div class=\"value\">".$rowVal3['classSubject']."</div>";
				$tpl2 .= "		</div>";
				$tpl2 .= "		<div class=\"row\">";
				$tpl2 .= "			<div class=\"name\">日期</div>";
				$tpl2 .= "			<div class=\"value\">".$rowVal3['registerDate']." (".$dayName[date('N', strtotime(str_replace('-', '/', $rowVal3['registerDate'])))].")</div>";
				$tpl2 .= "		</div>";
				$tpl2 .= "		<div class=\"row\">";
				$tpl2 .= "			<div class=\"name\">時間</div>";
				if($rowVal3['teamXtime'] > 1) {
					$tpl2 .= "			<div class=\"value\">".substr($rowVal3['subject'],16, 6).ceil(substr($rowVal3['subject'],22, 2)+($rowVal3['teamXtime']-1)).substr($rowVal3['subject'],24)."</div>";
				} else {
					$tpl2 .= "			<div class=\"value\">".substr($rowVal3['subject'], 16)."</div>";
				}	
				$tpl2 .= "		</div>";
				/*
				$tpl2 .= "		<div class=\"row\">";
				$tpl2 .= "			<div class=\"name\">看診人姓名</div>";
				$tpl2 .= "			<div class=\"value\">".$rowVal3['accept_name']."</div>";
				$tpl2 .= "		</div>";
				$tpl2 .= "		<div class=\"row\">";
				$tpl2 .= "			<div class=\"name\">看診人生日</div>";
				$tpl2 .= "			<div class=\"value\"></div>";
				$tpl2 .= "		</div>";
				*/
				$tpl2 .= "		<div class=\"row\">";
				$tpl2 .= "			<div class=\"name\">預約方式</div>";
				$tpl2 .= "			<div class=\"value\">LINE</div>";
				$tpl2 .= "		</div>";
				$tpl2 .= "		<div class=\"row\">";
				$tpl2 .= "			<div class=\"name\">狀態</div>";
				$tpl2 .= "			<div class=\"value\">".$rowVal3['paymentstatus']."</div>";
				$tpl2 .= "		</div>";
				$tpl2 .= "	</div>";
				$tpl2 .= "</div>";
			}
		}
		
		//if(mysql_num_rows($rs)) {
		$logInfo['error'] = 0;
		$logInfo['tpl'] = $tpl;
		$logInfo['tpl2'] = $tpl2;
		
		die(json_encode($logInfo, JSON_UNESCAPED_UNICODE));
		
	}
	
	//今日叫號 - 管理
	if($action == "todayManager") {
		$id = trim($_POST["id"]);
		$status = trim($_POST["status"]);
		$remark = trim($_POST["remark"]);
		$adminUsr = trim($_POST["adminUsr"]);
		$total = trim($_POST["total"]);
		if($status == '已報到') {
			$sql = "UPDATE web_x_register set paymentstatus = :paymentstatus, remark = :remark, total = :total, checkInUsr = :checkInUsr, checkInTime = :checkInTime Where web_x_register_id = :web_x_register_id ";
			$excute = array(
				':web_x_register_id'        => $id,
				':paymentstatus'        	=> $status,
				':remark'					=> $remark,
				':total'					=> $total,
				':checkInUsr'				=> $adminUsr,
				':checkInTime'				=> date('Y-m-d H:i:s'),
			);
		} else {
			$sql = "UPDATE web_x_register set paymentstatus = :paymentstatus, remark = :remark, total = :total, cancelUsr = :cancelUsr, cancelTime = :cancelTime Where web_x_register_id = :web_x_register_id ";
			$excute = array(
				':web_x_register_id'        => $id,
				':paymentstatus'        	=> $status,
				':remark'					=> $remark,
				':total'					=> $total,
				':cancelUsr'				=> $adminUsr,
				':cancelTime'				=> date('Y-m-d H:i:s'),
			);
		}
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute($excute);
		//$rs = ConnectDB($DB, $sql);
		
		//預約取消推播
		if($status == '取消') {
			
			$sql4 = "
				Select 
					a.*,
					c.subject as classSubject,
					c.web_x_class_id as store_id,
					d.subject as teamSubject,
					e.subject as storeSubject
				From 
					web_x_register a
				Left Join 
					web_class c ON c.web_class_id = a.web_time_id 		
				Left Join 
					web_x_team d ON d.web_x_team_id = a.web_x_team_id
				Left Join 
					web_x_class e ON e.web_x_class_id = c.web_x_class_id	
				Where
					a.web_x_register_id = :web_x_register_id
				order by 
					a.registerDate ASC, a.ordernum ASC
			";
			$excute = array(
				':web_x_register_id'        => $id,
			);
			$pdo = $pdoDB->prepare($sql4);
			$pdo->execute($excute);
			$row4 = $pdo->fetchAll(PDO::FETCH_ASSOC);
			
			foreach($row4 as $key4 => $val4) {
				$logInfo['error'] = 0;
				$logInfo['ordernum'] = $val4['ordernum'];
				$logInfo['registerId'] = $val4['web_x_register_id'];
				$logInfo['subject'] = $val4['subject'];
				$logInfo['classSubject'] = $val4['classSubject'];
				$logInfo['teamSubject'] = $val4['teamSubject'];
				$logInfo['store_id'] = $val4['store_id'];
				$logInfo['storeSubject'] = $val4['storeSubject'];
				$logInfo['accept_name'] = $val4['accept_name'];
				$logInfo['web_time_id'] = $val4['web_time_id'];
			}
			//file_put_contents("debug.txt", json_encode($logInfo, JSON_UNESCAPED_UNICODE)."\r\n", FILE_APPEND);
			
			/*
			$sql = "SELECT GROUP_CONCAT(DISTINCT lineID SEPARATOR ',') AS lineIDAry FROM web_member WHERE lineID != '' AND (ifService IN(1) AND ifMessage IN(1) AND store_id = '".$logInfo['store_id']."') OR (store_id = '-1' AND ifService IN(1) AND ifMessage IN(1))";
			*/
			$sql = "SELECT GROUP_CONCAT(DISTINCT lineID SEPARATOR ',') AS lineIDAry FROM web_member WHERE lineID != '' AND pusher_id = '".$logInfo['web_time_id']."' OR ((ifService IN(1) AND ifMessage IN(1) AND store_id = '".$logInfo['store_id']."') OR (store_id = '-1' AND ifService IN(1) AND ifMessage IN(1)))";
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute();
			$lineIDAry = $pdo->fetch(PDO::FETCH_ASSOC);
			//file_put_contents("debug.txt", json_encode($lineIDAry, JSON_UNESCAPED_UNICODE)."\r\n", FILE_APPEND);
			if($Init_Register_Push) {
				if(count($lineIDAry) && 1) {
					//$text = "客戶【".$logInfo['accept_name']."】預約".$productRow['csubject'].$productRow['subject'];
					$text = "【預約取消通知】\n門市：".$logInfo['storeSubject']."\n項目：".$logInfo['teamSubject']."\n設計師：".$logInfo['classSubject']."\n預約時間：".substr($logInfo['subject'],0,15)."\n到店時間：".substr($logInfo['subject'],16,5)."\n消費人：".$logInfo['accept_name'];
					//file_put_contents("debug.txt", $text."\r\n", FILE_APPEND);	
					$_REQUEST['ID'] = $lineIDAry['lineIDAry'];
					$_REQUEST['contnet'] = $text;
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, "https://".$_SERVER['HTTP_HOST']."/api.php");
					curl_setopt($ch, CURLOPT_HEADER, false);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
					$PostData = $_REQUEST;
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $PostData);
					$temp = curl_exec($ch);
					// 關閉CURL連線
					curl_close($ch);
				}
			}
		}
		
		
		
		$req = array('error'=> 0, 'message'=> $status.'成功');
		die(json_encode($req, JSON_UNESCAPED_UNICODE));
	}	
	
	//今日叫號 - 備註
	if($action == "todayManagerRemark") {
		$id = trim($_POST["id"]);
		$remark = trim($_POST["remark"]);
		
		$sql = "UPDATE web_x_register set remark = :remark Where web_x_register_id = :web_x_register_id ";
		$excute = array(
			':web_x_register_id'        => $id,
			':remark'					=> $remark,
		);
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute($excute);
		//$rs = ConnectDB($DB, $sql);
		$req = array('error'=> 0, 'message'=> $status.'成功');
		die(json_encode($req, JSON_UNESCAPED_UNICODE));
	}
	
	//今日叫號 - 金額
	if($action == "todayManagerTotal") {
		$id = trim($_POST["id"]);
		$total = trim($_POST["total"]);
		
		$sql = "UPDATE web_x_register set total = :total Where web_x_register_id = :web_x_register_id ";
		$excute = array(
			':web_x_register_id'        => $id,
			':total'					=> $total,
		);
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute($excute);
		//$rs = ConnectDB($DB, $sql);
		$req = array('error'=> 0, 'message'=> $status.'成功');
		die(json_encode($req, JSON_UNESCAPED_UNICODE));
	}
	
	//時段日曆
	if($action == "calendar") {
		$web_class_id = trim($_POST["classVal"]);
		$web_team_id = trim($_POST["teamVal"]);
		
		$sql2 = "
			Select 
				web_time.*
			From 
				web_time 
			Where 
				web_time.ifShow = '1'
			Order by
				web_time.asort ASC
		";
		$pdo = $pdoDB->prepare($sql2);
		$pdo->execute();
		$row2 = $pdo->fetchAll(PDO::FETCH_ASSOC);
		$timeAry = array();
		foreach($row2 as $key2 => $val2) {
			$timeAry[] = $val2;	
		}
		/*
		$sql3 = "
			SELECT 
				main.web_team_id,
				main.web_class_id,
				main.ifShow,
				main.lineInfo,
				main.content,
				main.web_shift_id,
				teams.subject as teamSubjct,
				teams.restDay as teamRestDay,
				time.web_time_id,
				time.subject
			FROM 
				web_shift as main 
			left join 
				(SELECT * from web_team) as teams
			on 
				main.web_team_id = teams.web_team_id
			left join 
				(SELECT * from web_time) as time
			on 
				main.subject = time.web_time_id	
			where
				main.web_team_id = :web_team_id
			and
				main.web_class_id = :web_class_id
		";
		//$sql3 .= " Group by web_team_id, web_class_id";
		$sql3 .= " order by time.web_time_id ASC , main.web_shift_id ASC";
		$excute = array(
			':web_team_id'        => $web_team_id,
			':web_class_id'        => $web_class_id,
		);
		*/
		$sql3 = "
			SELECT 
				*
			FROM 
				web_class as main 
			where
				main.web_class_id = :web_class_id
		";
		//$sql3 .= " Group by web_team_id, web_class_id";
		//$sql3 .= " order by time.web_time_id ASC , main.web_shift_id ASC";
		$excute = array(
			//':web_team_id'        => $web_team_id,
			':web_class_id'        => $web_class_id,
		);
		//$debug = new Helper();
		//echo $test = $debug::debugPDO($sql3, $excute);
		$pdo = $pdoDB->prepare($sql3);
		$pdo->execute($excute);
		$row3 = $pdo->fetch(PDO::FETCH_ASSOC);
	
		$shiftAry = array();
		$shiftAry2 = array();
		$restDay = array();
		/*
		$_timeAry = array(
			'10:30~11:30',
			'11:30~12:30',
			'12:30~13:30',
			'13:30~14:30',
			'14:30~15:30',
			'15:30~16:30',
			'16:30~17:30',
			'17:30~18:30',
			'18:30~19:30',
		);
		*/
		/*
		foreach($row3 as $rowKey3 => $rowVal3) {
			$restDay[$rowVal3['teamSubjct']] = explode(',', str_replace('-', '/', $rowVal3['teamRestDay']));
			$contentAry = explode(',', $rowVal3['content']);
			$shiftAry[] = $row3;
			for($j=1; $j<=7; $j++) {
				if(!in_array($j, $contentAry)) {
					continue;
				}	
				if(!in_array($rowVal3['teamSubjct'], $shiftAry2[$rowVal3['web_time_id']][$j])) {
					$shiftAry2[$rowVal3['web_time_id']][$j][$rowVal3['web_shift_id']] = $rowVal3['teamSubjct'];
				}	
			}	
		   
		}
		*/
		
		$lineInfoAry = explode('#####', $row3['lineInfo']);
		foreach($lineInfoAry as $lineInfoKey => $lineInfoVal) {
			$lineInfoRowAry[] = explode('@#@', $lineInfoVal);
		}
		
		foreach($lineInfoRowAry as $key => $lineInfoAry) {
			$lineInfo[$lineInfoAry['2']][] = $lineInfoAry['1'];
		}
	
	
		/*
		$tpl = "
			<table class=\"mobile\">
		";	
		*/
		$tpl = "";
		$tpl .= "
				<tr>
					<td></td>
		";			
				//foreach($timeAry as $timeKey => $timeVal) {
				foreach($_timeAry as $_timeKey => $_timeVal) {	
		//$tpl .= "	<td><span>".$timeVal["urlsubject"]." ".$timeVal["subject"]."</span></td>";
		$tpl .= "	<td><span>".$_timeVal."</span></td>";
					
					if($_timeKey == '3' || $_timeKey == '7' || $_timeKey == '11' || $_timeKey == '15' ) {
						$tpl .= "	<td><span></span></td>";
					}
					
				}			
		$tpl .= "
				</tr>
		";
		
		
		$todayDay = date('N');
		//$endDay = strtotime('+'.ceil(14-$todayDay).' day');
		//$startDay = strtotime('-13 day', $endDay);
		//20號前當月，21號當月+下個月
		if(date('d') <= '20') {
			$_endDay = ceil((date('t')- date('d')) + $todayDay); 
			$endDay = strtotime('+'.ceil($_endDay-$todayDay).' day');
			$startDay = strtotime('-'.ceil($_endDay - 1).' day', $endDay);

		} else {
			$nextMonthTotal = date('d', strtotime(date('Y-m-t', strtotime('+1 month'))));
			$_endDay = ceil((date('t')- date('d')) + $todayDay + $nextMonthTotal); 
			$endDay = strtotime('+'.ceil($_endDay-$todayDay).' day');
			$startDay = strtotime('-'.ceil($_endDay - 1).' day', $endDay);
		}
		
		//echo date('Y-m-d (N)', $startDay);
		//echo "</br>";
		//echo date('Y-m-d (N)', $endDay);
		$dayName = array(
			1 => '一', 2 => '二', 3 => '三', 4 => '四', 5 => '五', 6 => '六', 7 => '日',
		);
		
		$registerTimeRow = array();
		$isSql = "
			SELECT 
				main.*,
				xteam.subject as xteamSubject,
				xteam.time as xteamTime
			from 
				web_x_register as main
			left join
				web_x_team xteam
			on
				xteam.web_x_team_id = main.web_x_team_id
			WHERE 
				main.web_time_id = '".$web_class_id."'
			AND
				main.registerDate >= '".date('Y-m-d', $startDay)."'
			AND
				main.paymentstatus != '取消' 
			AND
				main.paymentstatus != '已報到'
			AND
				main.paymentstatus != '已服務'	
		";

		$pdo3 = $pdoDB->prepare($isSql);
		$pdo3->execute();
		$registerRow = $pdo3->fetchAll(PDO::FETCH_ASSOC);
		
		foreach($registerRow as $registerKey => $registerVal) {
			//echo array_search(substr($registerVal['subject'],16), $_timeAry);
			if(array_search(substr($registerVal['subject'],16), $_timeAry) >= '0') {
				if($_timeAry[array_search(substr($registerVal['subject'],16), $_timeAry)] == substr($registerVal['subject'],16)) {
					$registerTimeRow[$registerVal['web_time_id']][$registerVal['registerDate']][] = substr($registerVal['subject'],16);
					if($registerVal['xteamTime'] >= 1) {
						for($i = 2; $i <= $registerVal['xteamTime'] * 2; $i++) {
							$searchKey = array_search(substr($registerVal['subject'],16), $_timeAry) + ($i - 1);
							$registerTimeRow[$registerVal['web_time_id']][$registerVal['registerDate']][] = $_timeAry[$searchKey];
						}	
					}
				}	
				
			}
		}
		
		for($dayS = $startDay; $dayS <= $endDay; $dayS+=86400) {
			$tpl .= "
					<tr>
						<td><span>週".$dayName[date('N', $dayS)]."</span><span>".date('m/d', $dayS)."</span></td>
			";
				//foreach($timeAry as $timeKey => $timeVal) {
					//if($dayS < time() || strtotime(date('Y-m-d', $dayS)." ".substr($timeVal['subject'], 6, 5)) < time()) {	
				foreach($_timeAry as $_timeKey => $_timeVal) {
					if($_timeKey % 4 == '0' && $_timeKey != '0') {
						$tpl .= "
									<td><span>週".$dayName[date('N', $dayS)]."</span><span>".date('m/d', $dayS)."</span></td>
						";
					}
					if($dayS < time() || strtotime(date('Y-m-d', $dayS)." ".substr($_timeVal, 6, 5)) < time()) {		
						if($registerTimeRow[$web_class_id][date('Y-m-d', $dayS)]) {	
						$tpl .= "			
									<td style=\"text-decoration:line-through;\">
						";	
							//foreach($shiftAry2[$timeVal['web_time_id']][date('N', $dayS)] as $_teamKey => $_teamVal) {
							foreach($registerTimeRow[$web_class_id][date('Y-m-d', $dayS)] as $_timeKey2 => $_timeVal2) {
								if($_timeVal2 != $_timeVal) {
									continue;
								}
								
								
								
					$tpl .= "		<span class=\"shiftInfo\" data-shiftId=\"".$_teamKey."\" data-shiftText=\"".date('Y-m-d', $dayS)."(".$dayName[date('N', $dayS)].")-".$_timeVal."\" data-timeId=\"".$timeVal["web_time_id"]."\" data-date=\"".date('Y-m-d', $dayS)."\">已有預約</span>";
							}
						} else {
							$tpl .= "			
									<td style=\"text-decoration:line-through;\">
						";
						}
	
			
					} else {
						
						//設計師休假
						if($lineInfo[date('Y-m-d', $dayS)]) {
							if((in_array('1', $lineInfo[date('Y-m-d', $dayS)]) && $_timeKey < 9) || (in_array('3', $lineInfo[date('Y-m-d', $dayS)]) && $_timeKey < 9) && !$registerTimeRow[$web_class_id][date('Y-m-d', $dayS)]) {
								/*
								$tpl .= "			
									<td style=\"text-decoration:line-through;\">
								";
								*/
								$tpl .= "			
									<td class=\"items\">
								";
								$tpl .= "		<span class=\"shiftInfo\" data-shiftId=\"".$_teamKey."\" data-shiftText=\"".date('Y-m-d', $dayS)."(".$dayName[date('N', $dayS)].")-".$_timeVal."\" data-timeId=\"".$timeVal["web_time_id"]."\" data-date=\"".date('Y-m-d', $dayS)."\">休</span>";
							} else if((in_array('2', $lineInfo[date('Y-m-d', $dayS)]) && $_timeKey >= 9) || (in_array('3', $lineInfo[date('Y-m-d', $dayS)]) && $_timeKey >= 9) && !$registerTimeRow[$web_class_id][date('Y-m-d', $dayS)]) {
								/*
								$tpl .= "			
									<td style=\"text-decoration:line-through;\">
								";
								*/
								$tpl .= "			
									<td class=\"items\">
								";
								$tpl .= "		<span class=\"shiftInfo\" data-shiftId=\"".$_teamKey."\" data-shiftText=\"".date('Y-m-d', $dayS)."(".$dayName[date('N', $dayS)].")-".$_timeVal."\" data-timeId=\"".$timeVal["web_time_id"]."\" data-date=\"".date('Y-m-d', $dayS)."\">休</span>";
							} else {
								//if($shiftAry2[$timeVal['web_time_id']][date('N', $dayS)]) {
								if($registerTimeRow[$web_class_id][date('Y-m-d', $dayS)]) {		
									//foreach($shiftAry2[$timeVal['web_time_id']][date('N', $dayS)] as $_teamKey => $_teamVal) {
									$tag = 0;
									if(in_array($_timeVal, $registerTimeRow[$web_class_id][date('Y-m-d', $dayS)])) {
										$counts = count( array_keys( $registerTimeRow[$web_class_id][date('Y-m-d', $dayS)], $_timeVal ));	
										if($counts >= '2' && 0) {
											$tpl .= "			
														<td>
											";
										} else {
											$tpl .= "			
														<td class=\"items\">
											";
										}
										foreach($registerTimeRow[$web_class_id][date('Y-m-d', $dayS)] as $_timeKey2 => $_timeVal2) {
											if($_timeVal2 == $_timeVal) {
												
								$tpl .= "		<span class=\"shiftInfo\" data-shiftId=\"".$_teamKey."\" data-shiftText=\"".date('Y-m-d', $dayS)."(".$dayName[date('N', $dayS)].")-".$_timeVal."\" data-timeId=\"".$timeVal["web_time_id"]."\" data-date=\"".date('Y-m-d', $dayS)."\">已有預約</span>";
								
											} 
										}
									} else {
										$tpl .= "			
											<td class=\"items\">
								";
								$tpl .= "		<span class=\"shiftInfo\" data-shiftId=\"".$_teamKey."\" data-shiftText=\"".date('Y-m-d', $dayS)."(".$dayName[date('N', $dayS)].")-".$_timeVal."\" data-timeId=\"".$timeVal["web_time_id"]."\" data-date=\"".date('Y-m-d', $dayS)."\">可預約</span>";
									}	
									
								} else {
									$tpl .= "			
											<td class=\"items\">
								";
								$tpl .= "		<span class=\"shiftInfo\" data-shiftId=\"".$_teamKey."\" data-shiftText=\"".date('Y-m-d', $dayS)."(".$dayName[date('N', $dayS)].")-".$_timeVal."\" data-timeId=\"".$timeVal["web_time_id"]."\" data-date=\"".date('Y-m-d', $dayS)."\">可預約</span>";
								}
							}	
							
						} else {
			
							//if($shiftAry2[$timeVal['web_time_id']][date('N', $dayS)]) {
							if($registerTimeRow[$web_class_id][date('Y-m-d', $dayS)]) {		
								//foreach($shiftAry2[$timeVal['web_time_id']][date('N', $dayS)] as $_teamKey => $_teamVal) {
								$tag = 0;
								if(in_array($_timeVal, $registerTimeRow[$web_class_id][date('Y-m-d', $dayS)])) {	
								$counts = count( array_keys( $registerTimeRow[$web_class_id][date('Y-m-d', $dayS)], $_timeVal ));	
								if($counts >= '2' && 0) {
									$tpl .= "			
												<td>
									";
								} else {
									$tpl .= "			
												<td class=\"items\">
									";
								}
								//$tpl .= $counts;
									foreach($registerTimeRow[$web_class_id][date('Y-m-d', $dayS)] as $_timeKey2 => $_timeVal2) {
										if($_timeVal2 == $_timeVal) {
														
							$tpl .= "		<span class=\"shiftInfo\" data-shiftId=\"".$_teamKey."\" data-shiftText=\"".date('Y-m-d', $dayS)."(".$dayName[date('N', $dayS)].")-".$_timeVal."\" data-timeId=\"".$timeVal["web_time_id"]."\" data-date=\"".date('Y-m-d', $dayS)."\">已有預約</span>";
							
										} 
									}
								} else {
									$tpl .= "			
										<td class=\"items\">
							";
							$tpl .= "		<span class=\"shiftInfo\" data-shiftId=\"".$_teamKey."\" data-shiftText=\"".date('Y-m-d', $dayS)."(".$dayName[date('N', $dayS)].")-".$_timeVal."\" data-timeId=\"".$timeVal["web_time_id"]."\" data-date=\"".date('Y-m-d', $dayS)."\">可預約</span>";
								}	
								
							} else {
								$tpl .= "			
										<td class=\"items\">
							";
							$tpl .= "		<span class=\"shiftInfo\" data-shiftId=\"".$_teamKey."\" data-shiftText=\"".date('Y-m-d', $dayS)."(".$dayName[date('N', $dayS)].")-".$_timeVal."\" data-timeId=\"".$timeVal["web_time_id"]."\" data-date=\"".date('Y-m-d', $dayS)."\">可預約</span>";
							}
						}
						
					}
 
			$tpl .= "	</td>
			";
				}
			$tpl .= "
					</tr>
			";
		}
		/*	
		$tpl .= "
			</table>
		";
		*/		
		//echo $tpl;
		
		$logInfo['error'] = 0;
		$logInfo['tpl'] = $tpl;
		//$logInfo['tpl2'] = $tpl2;
		
		die(json_encode($logInfo, JSON_UNESCAPED_UNICODE));
	}
	
	//資料修改
	if($action == "infoEdit") {
		$web_x_register_id = intval($_POST["web_x_register_id"]);
		$uname = trim($_POST["uname"]);
		$birthday = trim($_POST["birthday"]);
		$mobile = trim($_POST["mobile"]);
		
		$sql = "Select * From web_x_register Where web_x_register_id = :web_x_register_id ";
		$excute = array(
			':web_x_register_id'        => $web_x_register_id,
		);
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute($excute);
		$row = $pdo->fetch(PDO::FETCH_ASSOC);
		
		if($row['lineID']) {
			$sql2 = "Select * From web_member Where lineID = :lineID and uname = :uname ";
			$excute2 = array(
				':lineID'        => $lineID,
				':uname'        => $uname,
			);
			$pdo = $pdoDB->prepare($sql2);
			$pdo->execute($excute2);
			$row2 = $pdo->fetch(PDO::FETCH_ASSOC);
			if(count($row2)) {
				$sql3 = "UPDATE web_member set uname = :uname, mobile = :mobile, birthday = :birthday Where web_member_id = :web_member_id ";
				//$rs = ConnectDB($DB, $sql);
				$excute3 = array(
					':uname'        => $uname,
					':mobile'       => $mobile,
					':birthday'     => $birthday,
					':web_member_id'    => $row2['web_member_id'],
				);
				$pdo = $pdoDB->prepare($sql3);
				$pdo->execute($excute3);
			}
			
		}
		
		$sql4 = "UPDATE web_x_register set order_name = :uname, order_mobile = :mobile, accept_name = :accept_name, accept_birthday = :birthday Where web_x_register_id = :web_x_register_id ";
		$excute4 = array(
			':uname'        => $uname,
			':accept_name'  => $uname,
			':mobile'       => $mobile,
			':birthday'     => $birthday,
			':web_x_register_id'    => $web_x_register_id,
		);
		//$debug = new Helper();
		//echo $test = $debug::debugPDO($sql4, $excute4);
		$pdo = $pdoDB->prepare($sql4);
		$pdo->execute($excute4);
		//$rs = ConnectDB($DB, $sql);
		//if(mysql_num_rows($rs)) {
		$logInfo['error'] = 0;
		$logInfo['web_x_register_id'] = $web_x_register_id;
		die(json_encode($logInfo, JSON_UNESCAPED_UNICODE));
		
	}
	
	//資料修改
	if($action == "infoEditMember") {
		$web_member_id = intval($_POST["web_member_id"]);
		$ifOverseas = intval($_POST["ifOverseas"]);
		$loginID = trim($_POST["loginID"]);
		$uname = trim($_POST["uname"]);
		$sex = trim($_POST["sex"]);
		$birthday = trim($_POST["birthday"]);
		$mobile = trim($_POST["mobile"]);
		$ifAccept = intval($_POST["ifAccept"]);
		$ifUseSelf = intval($_POST["ifUseSelf"]);
		
		
		
		if($web_member_id) {
			$sql2 = "Select * From web_member Where web_member_id = :web_member_id ";
			$excute2 = array(
				':web_member_id'        => $web_member_id,
			);
			$pdo = $pdoDB->prepare($sql2);
			$pdo->execute($excute2);
			$row2 = $pdo->fetch(PDO::FETCH_ASSOC);
			if(count($row2)) {
				$sql3 = "UPDATE web_member set uname = :uname, sex = :sex, loginID = :loginID, ifOverseas = :ifOverseas, mobile = :mobile, birthday = :birthday, ifAccept = :ifAccept, ifUseSelf = :ifUseSelf Where web_member_id = :web_member_id ";
				//$rs = ConnectDB($DB, $sql);
				$excute3 = array(
					':loginID'      => $loginID,
					':ifOverseas'   => $ifOverseas,
					':uname'        => $uname,
					':sex'        	=> $sex,
					':mobile'       => $mobile,
					':birthday'     => $birthday,
					':ifAccept'     => ($row2['ifAccept']) ? $row2['ifAccept'] : $ifAccept,
					':ifUseSelf'     => $ifUseSelf,
					':web_member_id'    => $row2['web_member_id'],
				);
				$pdo = $pdoDB->prepare($sql3);
				$pdo->execute($excute3);
			}
			
			$sql4 = "UPDATE web_x_register set order_mobile = :mobile, accept_birthday = :birthday Where lineID = :lineID and order_name = :uname";
			$excute4 = array(
				':uname'        => $uname,
				':mobile'       => $mobile,
				':birthday'     => $birthday,
				':lineID'    	=> $row2['lineID'],
			);
			//$debug = new Helper();
			//echo $test = $debug::debugPDO($sql4, $excute4);
			$pdo = $pdoDB->prepare($sql4);
			$pdo->execute($excute4);
			
		}
		
		//$rs = ConnectDB($DB, $sql);
		//if(mysql_num_rows($rs)) {
		$logInfo['error'] = 0;
		$logInfo['web_member_id'] = $web_member_id;
		die(json_encode($logInfo, JSON_UNESCAPED_UNICODE));
		
	}
	
	//叫號管理-查詢
	if($action == "callManagerSearch") {
		$timeVal = trim($_POST["timeVal"]);
		$classVal = trim($_POST["classVal"]);
		$teamVal = trim($_POST["teamVal"]);
		
		$accept_name = trim($_POST["accept_name"]);
		$accept_birthday = trim($_POST["accept_birthday"]);
		$accept_mobile = trim($_POST["accept_mobile"]);
		
		$sql = "
			SELECT 
				SQL_CALC_FOUND_ROWS a.*,
				b.web_shift_id,
				b.web_class_id,
				b.web_team_id,
				c.subject as classSubjct,
				d.subject as teamSubject,
				e.subject as teamXsubject,
				f.uname as funame,
				f.birthday as fbirthday,
				f.mobile as fmobile	
			FROM 
				web_x_register a
			Left Join 
				web_shift b ON b.web_shift_id = a.web_shift_id
			Left Join 
				web_class c ON c.web_class_id = b.web_class_id 		
			Left Join 
				web_team d ON d.web_team_id = b.web_team_id
			Left Join 
				web_x_team e ON e.web_x_team_id = d.web_x_team_id
			Left Join 
				web_member f ON f.uname = a.order_name AND f.mobile = a.order_mobile	
			where 
				a.registerDate = :registerDate
			AND
				b.subject = :subject
			AND
				b.web_team_id = :web_team_id
			AND
				b.web_class_id = :web_class_id
		";
	if($accept_name) {	
		$sql .= "
			AND 
				a.accept_name LIKE :accept_name
		";	
	}
	if($accept_birthday) {	
		$sql .= "
			AND 
				a.accept_birthday = :accept_birthday
		";	
	}
	if($accept_mobile) {	
		$sql .= "
			AND 
				a.order_mobile = :order_mobile
		";	
	}	
		$sql .= "
			Group BY
				a.web_x_register_id
			ORDER BY 
				a.subject ASC,
				a.registerDate ASC
		";
		$excute = array(
			':registerDate'			=> date('Y-m-d'),
			//':paymentstatus'		=> '已報到',
			':subject'        		=> $timeVal,
			':web_team_id'			=> $teamVal,
			':web_class_id'			=> $classVal,
			//':accept_name'			=> ($accept_name) ? ' AND accept_name LIKE %'.$accept_name.'% ' : 1,
			//':accept_birthday'		=> ($accept_birthday) ? ' AND accept_birthday LIKE %'.$accept_birthday.'% ' : ' AND 1 ',
			//':accept_mobile'		=> ($accept_mobile) ? ' AND accept_mobile LIKE %'.$accept_mobile.'% ' : 1,
		);
		
		$excute = ($accept_name) ? $excute+array(':accept_name' => '%'.$accept_name.'%') : $excute;
		$excute = ($accept_birthday) ? $excute+array(':accept_birthday' => $accept_birthday) : $excute;
		$excute = ($accept_mobile) ? $excute+array(':order_mobile' => $accept_mobile) : $excute;
		
		
		//$debug = new Helper();
		//echo $test = $debug::debugPDO($sql, $excute);
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute($excute);
		$row = $pdo->fetchAll(PDO::FETCH_ASSOC);
		
		
		$nowSql = "
			SELECT 
				SQL_CALC_FOUND_ROWS a.*,
				b.web_shift_id,
				b.web_class_id,
				b.web_team_id,
				c.subject as classSubjct,
				d.subject as teamSubject,
				e.subject as teamXsubject,
				f.accept_name,
				f.order_mobile
			FROM 
				web_call_log a
			Left Join 
				web_shift b ON b.web_shift_id = a.web_shift_id
			Left Join 
				web_class c ON c.web_class_id = b.web_class_id 		
			Left Join 
				web_team d ON d.web_team_id = b.web_team_id
			Left Join 
				web_x_team e ON e.web_x_team_id = d.web_x_team_id
			Left Join 
				web_x_register f ON f.web_x_register_id = a.web_x_register_id	
			where 
				a.registerDate = :registerDate
			AND
				b.subject = :subject
			AND
				b.web_team_id = :web_team_id
			AND
				b.web_class_id = :web_class_id
		";
		$nowSql .= "
			ORDER BY 
				a.cdate DESC
			LIMIT 0, 1	
		";
		$excute = array(
			':registerDate'			=> date('Y-m-d'),
			//':paymentstatus'		=> '已報到',
			':subject'        		=> $timeVal,
			':web_team_id'			=> $teamVal,
			':web_class_id'			=> $classVal,
			//':accept_name'			=> ($accept_name) ? ' AND accept_name LIKE %'.$accept_name.'% ' : 1,
			//':accept_birthday'		=> ($accept_birthday) ? ' AND accept_birthday LIKE %'.$accept_birthday.'% ' : ' AND 1 ',
			//':accept_mobile'		=> ($accept_mobile) ? ' AND accept_mobile LIKE %'.$accept_mobile.'% ' : 1,
		);
		
		
		
		//$debug = new Helper();
		//echo $test = $debug::debugPDO($nowSql, $excute);
		$pdo = $pdoDB->prepare($nowSql);
		$pdo->execute($excute);
		$watingRow = $pdo->fetch(PDO::FETCH_ASSOC);
		
		if($watingRow) {
			$nowTpl = '<span>'.$watingRow['ordernum'].'</span><span>'.$watingRow['accept_name'].'</span><span>'.$watingRow['order_mobile'].'</span>';
		}
		
		$callList = array();
		$viewList = array();
		$watingList = array();
		$callListTpl = '';
		$viewListTpl = '';
		$watingListTpl = '';
		//echo count($row);
		foreach($row as $rowKey => $rowVal) {
			//已報到-未過號
			if($rowVal['paymentstatus'] == '已報到' && ($rowVal['ordernum'] > $watingRow['ordernum'])) {
				$callList = $row;
				$callListTpl .= "
					<input data-id=\"".$rowVal['web_x_register_id']."\" data-ordernum=\"".$rowVal['ordernum']."\" data-shiftid=\"".$rowVal['web_shift_id']."\" type=\"radio\" id=\"r11-".$rowVal['web_x_register_id']."\" name=\"skill\">
                    <label for=\"r11-".$rowVal['web_x_register_id']."\"><span>".$rowVal['ordernum']."</span><span>".$rowVal['accept_name']."</span><span>".$rowVal['order_mobile']."</span></label>
				";
			}
			//已報到-已過號
			if($rowVal['paymentstatus'] == '已報到' && ($rowVal['ordernum'] < $watingRow['ordernum'])) {
				$watingList = $row;
				$watingListTpl .= "
					<input data-id=\"".$rowVal['web_x_register_id']."\" data-ordernum=\"".$rowVal['ordernum']."\" data-shiftid=\"".$rowVal['web_shift_id']."\" type=\"radio\" id=\"r12-".$rowVal['web_x_register_id']."\" name=\"skill\">
                    <label for=\"r12-".$rowVal['web_x_register_id']."\"><span>".$rowVal['ordernum']."</span><span>".$rowVal['accept_name']."</span><span>".$rowVal['order_mobile']."</span></label>
				";
			}
			//已報到-已服務
			if($rowVal['paymentstatus'] == '已服務') {
				$viewList = $row;
				$viewListTpl .= "
					<input data-id=\"".$rowVal['web_x_register_id']."\" data-ordernum=\"".$rowVal['ordernum']."\" data-shiftid=\"".$rowVal['web_shift_id']."\" type=\"radio\" id=\"r13-".$rowVal['web_x_register_id']."\" name=\"skill\">
                    <label for=\"r13-".$rowVal['web_x_register_id']."\"><span>".$rowVal['ordernum']."</span><span>".$rowVal['accept_name']."</span><span>".$rowVal['order_mobile']."</span></label>
				";
			}
			
		}
		
		
		$logInfo['error'] = 0;
		$logInfo['tpl'] = $callListTpl;
		$logInfo['tpl2'] = $watingListTpl;
		$logInfo['tpl3'] = $viewListTpl;
		$logInfo['tpl4'] = $nowTpl;
		
		die(json_encode($logInfo, JSON_UNESCAPED_UNICODE));
		
	}
	
	//叫號管理-叫號
	if($action == "callManager") {
		$web_x_register_id = intval($_POST["id"]);
		$ordernum = intval($_POST["ordernum"]);
		$shiftId = intval($_POST["shiftId"]);
		
		$sql3 = "
			Insert into 
				web_call_log 
					(
						ordernum,
						web_x_register_id,
						web_shift_id,
						registerDate,
						cdate
					) 
				values 
					(
						:ordernum,
						:web_x_register_id,
						:web_shift_id,
						:registerDate,
						:cdate
					) 
		";	
		$excute = array(
			':ordernum'        	=> $ordernum,
			':web_x_register_id' => $web_x_register_id,
			':web_shift_id'		=> $shiftId,
			':registerDate'     => date('Y-m-d'),
			':cdate'        	=> date('Y-m-d H:i:s'),
		);
		$pdo = $pdoDB->prepare($sql3);
		$pdo->execute($excute);
		$web_call_log_id = $pdoDB->lastInsertId();
		
		if(!$web_call_log_id) {
			$req = array('error'=>'1', 'message'=> '系統維護中');
		}
		
		$sql = "UPDATE web_x_register set paymentstatus = :paymentstatus Where web_x_register_id = :web_x_register_id ";
		//$rs = ConnectDB($DB, $sql);
		$excute = array(
			':paymentstatus'        => '已服務',
			':web_x_register_id'    => $web_x_register_id,
		);
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute($excute);	
		
		$req = array('error'=>'0');
		
		die(json_encode($req, JSON_UNESCAPED_UNICODE));
		
	}	
	
	//建立訂單
	if($action == "addOrder") {
		
		$TableName = "web_x_order";
		$AccurateAction = "Get";
		require("control/includes/accurate.php");
		
		$web_member_id = intval($_POST["web_member_id"]);
		$web_product_id = intval($_POST["web_product_id"]);
		$store_id = intval($_POST["store_id"]);
		
		$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM web_member where web_member.web_member_id = :web_member_id";
		$excute = array(
			':web_member_id'       	=> $web_member_id,
		);
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute($excute);
		$memberRow = $pdo->fetch(PDO::FETCH_ASSOC);
		
		$_POST['web_member_id'] = $memberRow['web_member_id'];
		$_POST['order_name'] = $memberRow['uname'];
		$_POST['order_mobile'] = $memberRow['mobile'];
		$_POST['accept_name'] = $memberRow['uname'];
		$_POST['accept_mobile'] = $memberRow['mobile'];
		
		
		$sql = "
			SELECT 
				SQL_CALC_FOUND_ROWS a.*,
				b.web_xx_product_id as bweb_xx_product_id,
				c.subject as csubject
			FROM 
				web_product a
			Left Join 
				web_x_product b ON b.web_x_product_id = a.web_x_product_id
			Left Join 
				web_xx_product c ON c.web_xx_product_id = b.web_xx_product_id		
			where 
				a.ifShow = :ifShow 
			and 
				a.web_product_id = :web_product_id 
			ORDER BY 
				a.displayorder ASC
		";
		$excute = array(
			':ifShow'        			=> 1,
			':web_product_id'       	=> $web_product_id,
		);
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute($excute);
		$productRow = $pdo->fetch(PDO::FETCH_ASSOC);
		if(!$productRow) {
			$req = array('error'=>'1', 'message'=> '系統維護中');
			
		} else {
		
			$ordernum = "";
			$date = substr(date("Y"), 2, 2).date("mdHi");
			$sql = "Select ordernum From web_x_order order by ordernum desc limit 1";
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute();	
			$web_x_order_info = $pdo->fetch(PDO::FETCH_ASSOC);
			if ($web_x_order_info>0) $ordernum = $web_x_order_info['ordernum'];
			if (substr($ordernum, 1, 10) != substr($date, 0, 10))	//尚無訂單編號
				$ordernum = $date."0001";
			else	//已有訂單編號
				$ordernum = $date.sprintf("%04d", substr($ordernum, 11, 4) + 1);

				
			$_POST['ordernum'] = "N".$ordernum;			
			
			$_POST['states'] = "訂單成立";	//訂單狀態
			$_POST['paymentstatus'] = ($payment != '線上刷卡') ? "尚未收款" : "付款失敗";	//付款狀態
			
			$_POST['subtotal'] = $productRow['price_member'];	//小計
			
			$_POST['total'] = $_POST['subtotal'];		//總計=小計-優惠代碼-紅利+運費
			$_POST['store_id'] = $store_id;
			
			$_POST['cdate'] = date("Y-m-d H:i:s");	//訂購時間
			$_POST['IP'] = $_SERVER["HTTP_VIA"] ? $_SERVER["HTTP_X_FORWARDED_FOR"] : $_SERVER["REMOTE_ADDR"];	//IP
			$_POST['agent'] = ($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : null;
			
			//$debug = true;
			$AccurateAction = "Insert";
			require("control/includes/accurate.php");
			
			$sql = "Select web_x_order_id, ordernum From web_x_order Where ordernum like '".$_POST['ordernum']."' order by web_x_order_id desc limit 1 ";
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute();
			$web_x_order_info = $pdo->fetch(PDO::FETCH_ASSOC);
		
			if (!$web_x_order_info) {
				$req = array('error'=>'1', 'message'=> '訂單錯誤');

			} else {
				$web_x_order_ordernum = $web_x_order_info['ordernum'];	//訂單編號
			}
			
			$dimension = ($productRow['bweb_xx_product_id']) ? $productRow['price_cost'] : null;
			$sql = "Insert into web_order (web_x_order_ordernum, web_product_id, web_x_sale_id, ifGeneral, additional, fullAdditional, subject, serialnumber, pincode, dimension, price, num, discount, masterId, cdate) values ('$web_x_order_ordernum', '".$productRow['web_product_id']."', '', '', '', '', '".$productRow['subject']."', '', '', '".$dimension."', '".$productRow['price_member']."', '1', '', '', '".$_POST['cdate']."') ";
			//echo $sql;
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute();	
			
			//扣庫存
			/*
			$sql = "Update web_product set stock = stock - ".$num." Where web_product_id = '".$web_product_id."' ";
			$rs = ConnectDB($DB, $sql);
			*/
			
			$sql = "SELECT GROUP_CONCAT(DISTINCT lineID SEPARATOR ',') AS lineIDAry FROM web_member WHERE lineID != '' AND (ifService IN(1) AND ifMessage IN(1) AND store_id = '".$_POST['store_id']."') OR (store_id = '-1' AND ifService IN(1) AND ifMessage IN(1))";
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute();
			$lineIDAry = $pdo->fetch(PDO::FETCH_ASSOC);
			if($productRow['bweb_xx_product_id'] == '1') {
				if($Init_Coupon_Push) {
					if(count($lineIDAry) && 1) {
						$text = "客戶【".$memberRow['uname']."】購買".$productRow['csubject'].$productRow['subject'];
							
						$_REQUEST['ID'] = $lineIDAry['lineIDAry'];
						$_REQUEST['contnet'] = $text;
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL, "https://".$_SERVER['HTTP_HOST']."/api.php");
						curl_setopt($ch, CURLOPT_HEADER, false);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
						$PostData = $_REQUEST;
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $PostData);
						$temp = curl_exec($ch);
						// 關閉CURL連線
						curl_close($ch);
					}	
				}	
			} else if($productRow['bweb_xx_product_id'] == '2') {
				if($Init_Stored_Push) {
					if(count($lineIDAry) && 1) {
						$text = "客戶【".$memberRow['uname']."】購買".$productRow['csubject'].$productRow['subject'];
							
						$_REQUEST['ID'] = $lineIDAry['lineIDAry'];
						$_REQUEST['contnet'] = $text;
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL, "https://".$_SERVER['HTTP_HOST']."/api.php");
						curl_setopt($ch, CURLOPT_HEADER, false);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
						$PostData = $_REQUEST;
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $PostData);
						$temp = curl_exec($ch);
						// 關閉CURL連線
						curl_close($ch);
					}	
				}
			}	
			
			$req = array('error'=>'0');
			
		}
		die(json_encode($req, JSON_UNESCAPED_UNICODE));
		
	}	
	
	//會員註冊
	if ($action == "memberRegister") {
	
		$lineID = ($_POST["lineID"]);
		$ifOverseas = intval($_POST["ifOverseas"]);
		$loginID = ($_POST["loginID"]);
		$uname = trim($_POST["uname"]);
		$sex = trim($_POST["sex"]);
		$birthday = ($_POST["birthday"]);
		$mobile = ($_POST["mobile"]);
		$ifAccept = intval($_POST["ifAccept"]);
		$ifUseSelf = intval($_POST["ifUseSelf"]);
		
		
		if($lineID) {
			$sql = "Select * From web_member Where lineID = :lineID ";
			$excute = array(
				':lineID'        => $lineID,
			);
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute($excute);
			$row = $pdo->fetch(PDO::FETCH_ASSOC);
		} else {
			$sql = "Select * From web_member Where uname = :uname AND mobile = :mobile ";
			$excute = array(
				':uname'        => $uname,
				':mobile'        => $mobile,
			);
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute($excute);
			$row = $pdo->fetch(PDO::FETCH_ASSOC);
		}
		
		$web_member_id = $row['web_member_id'];
		$oldLoginID = $row['loginID'];
		$oldIfOverseas = $row['ifOverseas'];
		$oldUname = $row['uname'];
		$oldSex = $row['sex'];
		$oldBirthday = $row['birthday'];
		$oldMobile = $row['mobile'];
		$oldIfAccept = $row['ifAccept'];
		$oldIfUseSelf = $row['ifUseSelf'];
	
		//沒有加入會員
		if(!$web_member_id) {
			$loginIDsql = "Select * From web_member Where loginID = :loginID ";
			$excute = array(
				':loginID'        => $loginID,
			);
			$pdo = $pdoDB->prepare($loginIDsql);
			$pdo->execute($excute);
			$loginIDcheck = $pdo->fetch(PDO::FETCH_ASSOC);
			
			if($loginIDcheck) {
				$logInfo = array('error'=>'1', 'message'=> '此身份證字號已註冊使用');
				die(json_encode($logInfo, JSON_UNESCAPED_UNICODE));
			}
			
			/*
			$insertSql = "
				Insert into 
					web_member 
						(
							lineID, 
							uname,
							birthday, 
							mobile, 
							cdate
						) 
					values 
						(
							'$lineID', 
							'$uname',
							'$birthday',
							'$mobile', 
							'".date('Y-m-d H:i:s')."'
						) 
			";	
			
			$web_member_id = ConnectDBInsterId($DB, $insertSql);
			*/
			$insertSql = "
				Insert into 
					web_member 
						(
							lineID,
							loginID,
							ifOverseas,
							store_id,
							uname,
							sex,
							birthday, 
							mobile, 
							cdate,
							ifAccept,
							ifUseSelf
						) 
					values 
						(
							:lineID,
							:loginID,
							:ifOverseas,							
							:store_id, 
							:uname,
							:sex,
							:birthday,
							:mobile, 
							:cdate,
							:ifAccept,
							:ifUseSelf
						) 
			";	
			$excute = array(
				':lineID'        	=> $lineID,
				':loginID'        	=> $loginID,
				':ifOverseas'		=> $ifOverseas,
				':store_id'        	=> store_Id,
				':uname'        	=> $uname,
				':sex'        		=> $sex,
				':birthday'        	=> $birthday,
				':mobile'        	=> $mobile,
				':cdate'        	=> date('Y-m-d H:i:s'),
				':ifAccept'			=> $ifAccept,
				':ifUseSelf'		=> $ifUseSelf
			);
			$pdo = $pdoDB->prepare($insertSql);
			$pdo->execute($excute);
			$web_member_id = $pdoDB->lastInsertId();
			
			$logInfo['error'] = 0;

			die(json_encode($logInfo, JSON_UNESCAPED_UNICODE));	
			
			
			
		} else {
			
			if($uname != $oldUname) {
				$sql = "UPDATE web_member set uname = :uname Where web_member_id = :web_member_id ";
				//$rs = ConnectDB($DB, $sql);
				$excute = array(
					':uname'        => $uname,
					':web_member_id'        => $web_member_id,
				);
				$pdo = $pdoDB->prepare($sql);
				$pdo->execute($excute);	
			}
			
			if($mobile != $oldMobile) {
				$sql = "UPDATE web_member set mobile = :mobile Where web_member_id = :web_member_id ";
				//$rs = ConnectDB($DB, $sql);
				$excute = array(
					':mobile'        => $mobile,
					':web_member_id'        => $web_member_id,
				);
				$pdo = $pdoDB->prepare($sql);
				$pdo->execute($excute);
			}
			
			if($sex != $oldSex) {
				$sql = "UPDATE web_member set sex = :sex Where web_member_id = :web_member_id ";
				//$rs = ConnectDB($DB, $sql);
				$excute = array(
					':sex'        => $sex,
					':web_member_id'        => $web_member_id,
				);
				$pdo = $pdoDB->prepare($sql);
				$pdo->execute($excute);
			}
			
			if($ifOverseas != $oldIfOverseas) {
				$sql = "UPDATE web_member set ifOverseas = :sex Where web_member_id = :web_member_id ";
				//$rs = ConnectDB($DB, $sql);ifOverseas
				$excute = array(
					':ifOverseas'        	=> $ifOverseas,
					':web_member_id'        => $web_member_id,
				);
				$pdo = $pdoDB->prepare($sql);
				$pdo->execute($excute);
			}
			
			if($loginID != $oldUname) {
				$sql = "UPDATE web_member set loginID = :loginID Where web_member_id = :web_member_id ";
				//$rs = ConnectDB($DB, $sql);ifOverseas
				$excute = array(
					':loginID'        		=> $loginID,
					':web_member_id'        => $web_member_id,
				);
				$pdo = $pdoDB->prepare($sql);
				$pdo->execute($excute);
			}
			
			if($birthday != $oldBirthday) {
				$sql = "UPDATE web_member set birthday = :birthday Where web_member_id = :web_member_id ";
				//$rs = ConnectDB($DB, $sql);ifOverseas
				$excute = array(
					':birthday'        		=> $birthday,
					':web_member_id'        => $web_member_id,
				);
				$pdo = $pdoDB->prepare($sql);
				$pdo->execute($excute);
			}
			
			if($ifAccept != $oldIfAccept) {
				$sql = "UPDATE web_member set ifAccept = :ifAccept Where web_member_id = :web_member_id ";
				//$rs = ConnectDB($DB, $sql);ifOverseas
				$excute = array(
					':ifAccept'        		=> $ifAccept,
					':web_member_id'        => $web_member_id,
				);
				$pdo = $pdoDB->prepare($sql);
				$pdo->execute($excute);
			}
			
			if($ifUseSelf != $oldIfUseSelf) {
				$sql = "UPDATE web_member set ifUseSelf = :ifUseSelf Where web_member_id = :web_member_id ";
				//$rs = ConnectDB($DB, $sql);ifOverseas
				$excute = array(
					':ifUseSelf'        		=> $ifUseSelf,
					':web_member_id'        => $web_member_id,
				);
				$pdo = $pdoDB->prepare($sql);
				$pdo->execute($excute);
			}
			
			$logInfo['error'] = 0;

			die(json_encode($logInfo, JSON_UNESCAPED_UNICODE));
		}	
		
		
	}
	
	//會員註冊-收集並未完成填寫
	if ($action == "collectRegister") {
	
		$lineID = ($_POST["lineId"]);
		$collect = trim($_POST["request"]);
		
		
		if($lineID) {
			$sql = "Select * From web_member Where lineID = :lineID ";
			$excute = array(
				':lineID'        => $lineID,
			);
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute($excute);
			$row = $pdo->fetch(PDO::FETCH_ASSOC);
		} 
		
		if($row) {
			$web_member_id = $row['web_member_id'];
			$oldLoginID = $row['loginID'];
			$oldIfOverseas = $row['ifOverseas'];
			$oldUname = $row['uname'];
			$oldSex = $row['sex'];
			$oldBirthday = $row['birthday'];
			$oldMobile = $row['mobile'];
		}
		/*
		if($collect) {
			$collectAry = json_decode($collect, true);
			$newTags['tags'] = array();
			foreach($collectAry as $key => $collect) {
				if($key == 'tags') {
					$collectTagAry = explode(',', $collect);
					$oldTagAry = ($row['tags']) ? explode(',', $row['tags']) : null;
					$newTagsAry = ($oldTagAry) ? array_merge($collectTagAry, $oldTagAry) : $collectTagAry;
					$newTags['tags'] = array_unique($newTagsAry);
					
				} else {
					$sql = "
						Select 
							web_tag_id
						From 
							web_tag a
						Where 
							a.subject LIKE '%".$collect."%'
					";
					$pdo = $pdoDB->prepare($sql);
					$pdo->execute();
					$tagHaved = $pdo->fetch(PDO::FETCH_ASSOC);
					if($tagHaved['web_tag_id']) {
						$newTags['other'][] = $tagHaved['web_tag_id'];
					} else {
						$insertSql = "
							Insert into 
								web_tag 
									(
										web_x_tag_id, 
										ifShow,
										subject
									) 
								values 
									(
										'3', 
										'1',
										'$collect'
									) 
						";	
						
						$web_tag_id = ConnectDBInsterId($DB, $insertSql);
						$newTags['other'][] = $web_tag_id;
					}
				}
				
				$finalTags = array_merge($newTags['tags'], $newTags['other']);
				
			}
		}
		*/
		//tag
		if($collect) {
			$collectAry = json_decode($collect, true);
			$newTags['tags'] = array();
			foreach($collectAry as $key => $collect) {
				if($key == 'web_qrcode_id') {
					$_web_qrcode_id = $collect;
					continue;
				}
				if($key == 'tags') {
					$collectTagAry = explode(',', $collect);
					$oldTagAry = explode(',', $row['tags']);
					$newTagsAry = array_merge($collectTagAry, $oldTagAry);
					$newTags['tags'] = array_unique($newTagsAry);
				} else {
					$sql = "
						Select 
							web_tag_id
						From 
							web_tag a
						Where 
							a.subject LIKE '%".$collect."'
					";
					$pdo = $pdoDB->prepare($sql);
					$pdo->execute();
					$tagHaved = $pdo->fetch(PDO::FETCH_ASSOC);
					if($tagHaved['web_tag_id']) {
						$newTags['other'][] = $tagHaved['web_tag_id'];
					} else {
						if($key != 'store') {
							$insertSql = "
								Insert into 
									web_tag 
										(
											web_x_tag_id, 
											ifShow,
											subject
										) 
									values 
										(
											'3', 
											'1',
											'$collect'
										) 
							";
						} else {
							$insertSql = "
								Insert into 
									web_tag 
										(
											web_x_tag_id, 
											ifShow,
											subject
										) 
									values 
										(
											'4', 
											'1',
											'$collect'
										) 
							";
						}	
						
						$web_tag_id = ConnectDBInsterId($DB, $insertSql);
						$newTags['other'][] = $web_tag_id;
					}
				}
				
				$finalTags = array_merge($newTags['tags'], $newTags['other']);
			}
		}	
		//file_put_contents("debug.txt", json_encode($collectTagAry, JSON_UNESCAPED_UNICODE)."\r\n", FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($oldTagAry, JSON_UNESCAPED_UNICODE)."\r\n", FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($newTags['tags'], JSON_UNESCAPED_UNICODE)."\r\n", FILE_APPEND);
		//沒有加入會員
		if(!$web_member_id) {
			
			
			$insertSql = "
				Insert into 
					web_member 
						(
							lineID,
							cdate,
							tags
						) 
					values 
						(
							:lineID,
							:cdate,
							:tags
						) 
			";	
			$excute = array(
				':lineID'        	=> $lineID,
				':cdate'        	=> date('Y-m-d H:i:s'),
				':tags'				=> implode(',', array_unique($finalTags)),
			);
			
			file_put_contents("debug.txt", json_encode($excute, JSON_UNESCAPED_UNICODE)."\r\n", FILE_APPEND);
			$pdo = $pdoDB->prepare($insertSql);
			$pdo->execute($excute);
			$web_member_id = $pdoDB->lastInsertId();
			
			
			//寫入掃描記錄－會員
			if($_web_qrcode_id && $web_member_id) {
				$sql = "
					Select 
						member
					From 
						web_qrcode a
					Where 
						a.web_qrcode_id = '".$_web_qrcode_id."'
				";
				$pdo = $pdoDB->prepare($sql);
				$pdo->execute();
				$qrcodeMemberRow = $pdo->fetch(PDO::FETCH_ASSOC);
				if($qrcodeMemberRow['member']) {
					$qrcodeMemberArr = explode(',', $qrcodeMemberRow['member']);
					if(in_array($web_member_id, $qrcodeMemberArr)) {
						$qrcodeMember = $qrcodeMemberArr;
					} else {	
						$qrcodeMemberArr[count($qrcodeMemberArr)] = $web_member_id;
						$qrcodeMember = $qrcodeMemberArr;
					}
					$sql = "UPDATE `web_qrcode` SET `member` = '".implode(',', $qrcodeMember)."' Where web_qrcode_id = '".$_web_qrcode_id."'";
					$pdo = $pdoDB->prepare($sql);
					$pdo->execute();
				} else {
					$sql = "UPDATE `web_qrcode` SET `member` = '".$web_member_id."' Where web_qrcode_id = '".$_web_qrcode_id."'";
					$pdo = $pdoDB->prepare($sql);
					$pdo->execute();
				}
			}
			
			$logInfo['error'] = 0;

			die(json_encode($logInfo, JSON_UNESCAPED_UNICODE));	
			
			
			
		} else {

			$sql = "UPDATE web_member set tags = :tags Where lineID = :lineID ";
			$excute = array(
				':lineID'        			=> $lineID,
				':tags'        				=> implode(',', array_unique($finalTags)),
			);
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute($excute);
			
			$logInfo['error'] = 0;

			die(json_encode($logInfo, JSON_UNESCAPED_UNICODE));
		}
		
		
	}
	
	//訂單收款
	if($action == "orderPay") {
		$web_x_order_id = intval($_POST["id"]);
		$status = trim($_POST["status"]);
		$web_member_id = intval($_POST["admin"]);
		$ip = $_SERVER["HTTP_VIA"] ? $_SERVER["HTTP_X_FORWARDED_FOR"] : $_SERVER["REMOTE_ADDR"];	//IP
		
		$sql = "
			SELECT 
				SQL_CALC_FOUND_ROWS a.*,
				b.web_member_id as bweb_member_id,
				b.uname as buname,
				b.birthday as bbirthday,
				b.mobile as bmobile,
				b.lineID as blineID,
				b.tags as btags,
				web_order.subject as prodSubjects,
				web_order.web_order_id as web_order_id,
				web_order.web_x_order_ordernum as web_x_order_ordernum,
				web_order.web_product_id as web_order_product_id,
				web_product.web_product_id as web_product_product_id,
				web_product.web_x_product_id as web_product_x_product_id,
				web_product.totalCount as web_product_totalCount,
				web_product.subject2 as web_product_subject2,
				web_product.tags as web_product_tags,
				web_x_product.web_x_product_id as web_x_product_x_product_id,
				web_x_product.subject as web_x_product_subject,
				web_x_product.web_xx_product_id as web_x_product_xx_product_id
			FROM 
				web_x_order a
			Left Join 
				web_member b 
			ON 
				b.web_member_id = a.web_member_id
			Left Join
				web_order as web_order
			On
				web_order.web_x_order_ordernum = a.ordernum
			Left Join
				web_product
			On
				web_product.web_product_id = web_order.web_product_id
			Left Join
				web_x_product
			On
				web_x_product.web_x_product_id = web_product.web_x_product_id	
			where 
				a.web_x_order_id = :web_x_order_id
		";
		$excute = array(
			':web_x_order_id'        => $web_x_order_id,
		);
		//$debug = new Helper();
		//echo $test = $debug::debugPDO($sql, $excute);
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute($excute);
		$productRow = $pdo->fetch(PDO::FETCH_ASSOC);
		
		if($productRow['web_x_order_id']) {
			
			//$sql = "Select * From web_member Where web_member_id = :web_member_id ";
			$sql = "Select * From web_adminuser Where web_adminuser_id = :web_member_id ";
			$excute = array(
				':web_member_id'        => $web_member_id,
			);
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute($excute);
			$memberRow = $pdo->fetch(PDO::FETCH_ASSOC);
			
			if($status == 'pay' || $status == 'credit') {
				$_POST['paymentstatus'] = '付款成功';
				$_POST['editUser'] = $memberRow['uname'];
				$_POST['editDateTime'] = date('Y-m-d H:i:s');
				$_POST['payment'] = ($status == 'pay') ? 'cash' : 'credit_card';
				//只有儲值
				if($productRow['web_x_product_xx_product_id'] == '2' && $status == 'pay') {
					//本期贈送的紅利
					$SendBonus = CalSendBonus($productRow['total']);	//紅利:本檔活動
					$web_x_bonus_id = $SendBonus[web_x_bonus_hidden];	//紅利流水號
					if ($web_x_bonus_id>0) {
						$web_x_order_from_ordernum = $productRow['ordernum'];	//來自訂單編號
						
						$sql = "Select * From web_x_bonus Where web_x_bonus_id = '".$web_x_bonus_id ."' and sdate <= '".date("Y-m-d")."' and edate >= '".date("Y-m-d")."' order by sdate desc, edate desc, web_x_bonus_id desc ";
						$rs = ConnectDB($DB, $sql);
						for ($i=0; $i<mysql_num_rows($rs); $i++) {
							$row = mysql_fetch_assoc($rs);
							
							$kind = $row["kind"];	//類型

							if ($row["ifDate"]==1) {
								$sdate = date("Y-m-d");	//開始日期
								$edate = date("Y-m-d", strtotime("+".$row["days"]." day"));	//截止日期
							} else {
								$sdate = $row["startdate"];	//開始日期
								$edate = $row["enddate"];	//截止日期
							}

							$srange = $row["overflow"];	//本次消費滿多少元可使用紅利
							$money = round(($productRow['total']) / $row["srange"]) * $row["money"];	//紅利金額
						}
						
						//寫入紅利清單
						$sql = "Insert into web_bonus (web_x_bonus_id, kind, web_x_order_from_ordernum, web_member_id, sdate, edate, srange, money, cdate, web_x_order_to_ordernum, usedate) values ('$web_x_bonus_id', '$kind', '$web_x_order_from_ordernum', '".$productRow['web_member_id']."', '$sdate', '$edate', '$srange', '$money', '".date('Y-m-d H:i:s')."', '$web_x_order_to_ordernum', '$usedate') ";
						//if (strpos($ip, "118.99.141.")!==false || strpos($ip, "192.168.")!==false) echo "1:".$sql."<hr />";
						$rs = ConnectDB($DB, $sql);
						
						$sql = "Select web_bonus_id From web_bonus Where web_x_order_from_ordernum like '".$productRow['ordernum']."' and cdate like '".$cdate."' order by web_bonus_id desc limit 1 ";
						//if (strpos($ip, "118.99.141.")!==false || strpos($ip, "192.168.")!==false) echo "2:".$sql."<hr />";
						$rs = ConnectDB($DB, $sql);
						for ($i=0; $i<mysql_num_rows($rs); $i++) {
							$row = mysql_fetch_assoc($rs);

							$web_bonus_content = $SendBonus[web_bonus_content];		//贈送的內容(用於成立訂單)

							//將紅利流水號及贈送內容寫回訂單
							$sql2 = "Update web_x_order set web_bonus_id = '".$row["web_bonus_id"]."', web_bonus_content = '".$web_bonus_content."' Where ordernum like '".$productRow['ordernum']."' ";
							//if (strpos($ip, "118.99.141.")!==false || strpos($ip, "192.168.")!==false) echo "3:".$sql2."<hr />";
							$rs2 = ConnectDB($DB, $sql2);
						}
					}
				}
				
				if($productRow['btags'] && $productRow['web_product_tags']) {
					
					$productTagAry = explode(',', $productRow['web_product_tags']);
					$memberTagAry = explode(',', $productRow['btags']);
					$newTagsAry = array_merge($productTagAry, $memberTagAry);
					$newTags = array_unique($newTagsAry);
					
					$sql2 = "Update web_member set tags = '".implode(',', $newTags)."' Where web_member_id = '".$productRow['bweb_member_id']."' ";
					$rs2 = ConnectDB($DB, $sql2);
					
				}
				
			} else if($status == 'cancel') {
				$_POST['states'] = '取消';
				$_POST['cancelUser'] = $memberRow['uname'];
				$_POST['cancelDateTime'] = date('Y-m-d H:i:s');
			}
			$_POST['date'] = date('Y-m-d H:i:s');
			$_POST['web_x_order_id'] = $productRow['web_x_order_id'];
			$_POST['shipdate'] = date('Y-m-d');
			
			if (1) {
				$TableName = "web_x_order";
				$sql = "SHOW FULL FIELDS FROM `".$TableName."`";
				$rs = ConnectDB($DB, $sql);
				$tabeInfoAry = array();
				for ($i=0; $i<mysql_num_rows($rs); $i++) {
					$row = mysql_fetch_assoc($rs);
					$tabeInfoAry[$row[Field]] = $row[Comment];
				}

				$sql2 = "Select * From web_x_order Where web_x_order_id like '".$_POST[web_x_order_id]."' ";
				$rs2 = ConnectDB($DB, $sql2);	
				$inDbInfo = array();
				for ($i=0; $i<mysql_num_rows($rs2); $i++) {
					$row2 = mysql_fetch_assoc($rs2);
					$inDbInfo = $row2;
				}
				$_inDbInfo = array();
				$postAry = array();
				foreach($_POST as $key => $postVal) {
					//echo $key.": ".$tabeInfoAry[$key].": ".$postVal."</br>";
					$_inDbInfo[$tabeInfoAry[$key]] = $inDbInfo[$key];
					$postAry[$tabeInfoAry[$key]] = $postVal;
				}
				//exit;
				$result = array_diff_assoc($postAry,$_inDbInfo);
				//查看是否有更改
				if(count($result)) {
					$recLogAry = array();
					$_recLogAry = array();
					foreach($result as $key => $postVal) {
						//echo '原資料:'.$_inDbInfo[$key]."</br>";
						//$_recLogAry[$key] = $_inDbInfo[$key]." -> ".$postVal;
						if(in_array($key, array('accept_time', 'invoicetype'))) {
							continue;
						}
						$recLogAry[urlencode($key)] = urlencode($_inDbInfo[$key])." -> ".urlencode($postVal);
					}
					$recLogAry[urlencode(操作日期)] = $_POST['date'];
					$recLogAry[urlencode(操作者)] = urlencode($memberRow['uname']);
					$recLogAry[IP] = $ip;
					$recLog = json_encode($recLogAry);

					if(count($recLogAry)) {
						//$sql3 = "Update web_x_order set log = '".$recLog."' Where web_x_order_id = '".$_POST[web_x_order_id]."'";
						$sql3 = "
							Insert into 
								web_orderlog 
									(
										ordernum, 
										log, 
										uname, 
										IP, 
										date
									) 
								values 
									(
										'".$inDbInfo[ordernum]."', 
										'$recLog', 
										'".$_POST['editUser']."', 
										'$ip', 
										'".$_POST['date']."'
									) 
						";	
						$rs3 = ConnectDB($DB, $sql3);
					}
				}
			}
			
			if($status == 'pay' || $status == 'credit') {

				$sql4 = "UPDATE web_x_order set paymentstatus = :paymentstatus, payment = :payment, shipdate = :shipdate, editUser = :editUser, editDateTime = :editDateTime, date = :date Where web_x_order_id = :web_x_order_id ";
				$excute4 = array(
					':paymentstatus'        => '付款成功',
					':payment'        		=> $_POST['payment'],
					':editUser'  			=> $_POST['editUser'],
					':date'       			=> $_POST['date'],
					':shipdate'				=> date('Y-m-d'),
					':editDateTime'			=> $_POST['editDateTime'],
					':web_x_order_id'    	=> $web_x_order_id,
				);
				//$debug = new Helper();
				//echo $test = $debug::debugPDO($sql4, $excute4);
				$pdo = $pdoDB->prepare($sql4);
				$pdo->execute($excute4);
				$logInfo['error'] = 0;
				if($productRow['web_x_product_xx_product_id'] == '2') {
					if($status == 'pay') {
						$text = "您已付款成功 ".$productRow['web_x_product_subject'].$productRow['prodSubjects']."\n 付款方式: 現金(".$productRow['web_product_subject2'].")\n 開通人員 ".$_POST['editUser']."\n 開通時間 ".$_POST['editDateTime'];
					} else {
						$text = "您已付款成功 ".$productRow['web_x_product_subject'].$productRow['prodSubjects']."\n 付款方式: 信用卡\n 開通人員 ".$_POST['editUser']."\n 開通時間 ".$_POST['editDateTime'];
					}	
				} else {
					$text = "您已付款成功 ".$productRow['web_x_product_subject'].$productRow['prodSubjects']."\n 開通人員 ".$_POST['editUser']."\n 開通時間 ".$_POST['editDateTime'];
				}	
				
			} else if($status == 'cancel') {

				$sql4 = "UPDATE web_x_order set states = :states, cancelUser = :cancelUser, cancelDateTime = :cancelDateTime, date = :date Where web_x_order_id = :web_x_order_id ";
				$excute4 = array(
					':states'        		=> '取消',
					':cancelUser'  			=> $_POST['cancelUser'],
					':date'       			=> $_POST['date'],
					//':shipdate'				=> date('Y-m-d'),
					':web_x_order_id'    	=> $web_x_order_id,
					':cancelDateTime'		=> $_POST['cancelDateTime'],
				);
				//$debug = new Helper();
				//echo $test = $debug::debugPDO($sql4, $excute4);
				$pdo = $pdoDB->prepare($sql4);
				$pdo->execute($excute4);
				$logInfo['error'] = 0;
				
				$text = "您已成功取消 ".$productRow['web_x_product_subject'].$productRow['prodSubjects']."\n 取消人員 ".$_POST['cancelUser']."\n 取消時間 ".$_POST['cancelDateTime'];
			}
			if($productRow['web_x_product_xx_product_id'] == '1') {
				if($Init_Coupon_Push) {
					if($productRow['blineID'] && 1) {
					
						if($productRow['blineID'] && 1) {
							
								
							$_REQUEST['ID'] = $productRow['blineID'];
							$_REQUEST['contnet'] = $text;
							$ch = curl_init();
							curl_setopt($ch, CURLOPT_URL, "https://".$_SERVER['HTTP_HOST']."/api.php");
							curl_setopt($ch, CURLOPT_HEADER, false);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
							$PostData = $_REQUEST;
							curl_setopt($ch, CURLOPT_POST, 1);
							curl_setopt($ch, CURLOPT_POSTFIELDS, $PostData);
							$temp = curl_exec($ch);
							// 關閉CURL連線
							curl_close($ch);
						}	
					}
				}	
			} else if($productRow['web_x_product_xx_product_id'] == '2') {
				if($Init_Stored_Push) {
					if($productRow['blineID'] && 1) {
					
						if($productRow['blineID'] && 1) {
							
								
							$_REQUEST['ID'] = $productRow['blineID'];
							$_REQUEST['contnet'] = $text;
							$ch = curl_init();
							curl_setopt($ch, CURLOPT_URL, "https://".$_SERVER['HTTP_HOST']."/api.php");
							curl_setopt($ch, CURLOPT_HEADER, false);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
							$PostData = $_REQUEST;
							curl_setopt($ch, CURLOPT_POST, 1);
							curl_setopt($ch, CURLOPT_POSTFIELDS, $PostData);
							$temp = curl_exec($ch);
							// 關閉CURL連線
							curl_close($ch);
						}	
					}
				}	
			}	
			

			
		} else {
			$logInfo = array('error'=>'1', 'message'=> '系統維護中');
		}
		
		
		die(json_encode($logInfo, JSON_UNESCAPED_UNICODE));
		
	}
	
	//取消密碼驗証
	if($action == "cancelPwdCheck") {
		$cancelPwd = ($_POST["cancelPwd"]);
		
		if($cancelPwd == $Init_cancelPwd) {
			$logInfo = array('error'=>'0');
		} else {
			$logInfo = array('error'=>'1', 'message'=> '密碼錯誤');
		}
		
		die(json_encode($logInfo, JSON_UNESCAPED_UNICODE));
	}	
	
	
	//訂單取消
	if($action == "cancelOrder") {
		
		$web_x_order_id = intval($_POST["web_x_order_id"]);
		$cancelReason = trim($_POST["cancelReason"]);
		$cancelRemark = trim($_POST["cancelRemark"]);
		$web_member_id = intval($_POST["admin"]);
		
		$ip = $_SERVER["HTTP_VIA"] ? $_SERVER["HTTP_X_FORWARDED_FOR"] : $_SERVER["REMOTE_ADDR"];	//IP
		
		//$sql = "Select * From web_x_order Where web_x_order_id = :web_x_order_id ";
		$sql = "
			SELECT 
				SQL_CALC_FOUND_ROWS a.*,
				b.web_member_id as bweb_member_id,
				b.uname as buname,
				b.birthday as bbirthday,
				b.mobile as bmobile,
				b.lineID as blineID,
				b.tags as btags,
				web_order.subject as prodSubjects,
				web_order.web_order_id as web_order_id,
				web_order.web_x_order_ordernum as web_x_order_ordernum,
				web_order.web_product_id as web_order_product_id,
				web_product.web_product_id as web_product_product_id,
				web_product.web_x_product_id as web_product_x_product_id,
				web_product.totalCount as web_product_totalCount,
				web_product.tags as web_product_tags,
				web_x_product.web_x_product_id as web_x_product_x_product_id,
				web_x_product.subject as web_x_product_subject,
				web_x_product.web_xx_product_id as web_x_product_xx_product_id
			FROM 
				web_x_order a
			Left Join 
				web_member b 
			ON 
				b.web_member_id = a.web_member_id
			Left Join
				web_order as web_order
			On
				web_order.web_x_order_ordernum = a.ordernum
			Left Join
				web_product
			On
				web_product.web_product_id = web_order.web_product_id
			Left Join
				web_x_product
			On
				web_x_product.web_x_product_id = web_product.web_x_product_id	
			where 
				a.web_x_order_id = :web_x_order_id
		";
		$excute = array(
			':web_x_order_id'        => $web_x_order_id,
		);
		//$debug = new Helper();
		//echo $test = $debug::debugPDO($sql, $excute);
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute($excute);
		$productRow = $pdo->fetch(PDO::FETCH_ASSOC);
		
		if($productRow['web_x_order_id']) {
			
			
			$_POST['date'] = date('Y-m-d H:i:s');
			$_POST['web_x_order_id'] = $productRow['web_x_order_id'];
			$_POST['cancelReason'] = $cancelReason;
			$_POST['cancelRemark'] = $cancelRemark;
			$_POST['cancelDateTime'] = date('Y-m-d H:i:s');
			$_POST['states'] = '取消';
			
			//$sql = "Select * From web_member Where web_member_id = :web_member_id ";
			$sql = "Select * From web_adminuser Where web_adminuser_id = :web_member_id ";
			$excute = array(
				':web_member_id'        => $web_member_id,
			);
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute($excute);
			$memberRow = $pdo->fetch(PDO::FETCH_ASSOC);
			
			$_POST['cancelUser'] = $memberRow['uname'];
			
			if (1) {
				$TableName = "web_x_order";
				$sql = "SHOW FULL FIELDS FROM `".$TableName."`";
				$rs = ConnectDB($DB, $sql);
				$tabeInfoAry = array();
				for ($i=0; $i<mysql_num_rows($rs); $i++) {
					$row = mysql_fetch_assoc($rs);
					$tabeInfoAry[$row[Field]] = $row[Comment];
				}

				$sql2 = "Select * From web_x_order Where web_x_order_id like '".$_POST[web_x_order_id]."' ";
				$rs2 = ConnectDB($DB, $sql2);	
				$inDbInfo = array();
				for ($i=0; $i<mysql_num_rows($rs2); $i++) {
					$row2 = mysql_fetch_assoc($rs2);
					$inDbInfo = $row2;
				}
				$_inDbInfo = array();
				$postAry = array();
				foreach($_POST as $key => $postVal) {
					//echo $key.": ".$tabeInfoAry[$key].": ".$postVal."</br>";
					$_inDbInfo[$tabeInfoAry[$key]] = $inDbInfo[$key];
					$postAry[$tabeInfoAry[$key]] = $postVal;
				}
				//exit;
				$result = array_diff_assoc($postAry,$_inDbInfo);
				//查看是否有更改
				if(count($result)) {
					$recLogAry = array();
					$_recLogAry = array();
					foreach($result as $key => $postVal) {
						//echo '原資料:'.$_inDbInfo[$key]."</br>";
						//$_recLogAry[$key] = $_inDbInfo[$key]." -> ".$postVal;
						if(in_array($key, array('accept_time', 'invoicetype'))) {
							continue;
						}
						$recLogAry[urlencode($key)] = urlencode($_inDbInfo[$key])." -> ".urlencode($postVal);
					}
					$recLogAry[urlencode(操作日期)] = $_POST['date'];
					$recLogAry[urlencode(操作者)] = urlencode($memberRow['uname']);
					$recLogAry[IP] = $ip;
					$recLog = json_encode($recLogAry);

					if(count($recLogAry)) {
						//$sql3 = "Update web_x_order set log = '".$recLog."' Where web_x_order_id = '".$_POST[web_x_order_id]."'";
						$sql3 = "
							Insert into 
								web_orderlog 
									(
										ordernum, 
										log, 
										uname, 
										IP, 
										date
									) 
								values 
									(
										'".$inDbInfo[ordernum]."', 
										'$recLog', 
										'".$_POST['editUser']."', 
										'$ip', 
										'".$_POST['date']."'
									) 
						";	
						$rs3 = ConnectDB($DB, $sql3);
					}
				}
			}
			
			$sql4 = "UPDATE web_x_order set states = :states, cancelUser = :cancelUser, cancelDateTime = :cancelDateTime, date = :date, cancelReason = :cancelReason, cancelRemark = :cancelRemark Where web_x_order_id = :web_x_order_id ";
			$excute4 = array(
				':states'        		=> $_POST['states'],
				':cancelUser'  			=> $_POST['cancelUser'],
				':date'       			=> $_POST['date'],
				':cancelDateTime'		=> $_POST['cancelDateTime'],
				':cancelReason'    		=> $_POST['cancelReason'],
				':cancelRemark'    		=> $_POST['cancelRemark'],
				':web_x_order_id'    	=> $web_x_order_id,
			);
			//$debug = new Helper();
			//echo $test = $debug::debugPDO($sql4, $excute4);
			$pdo = $pdoDB->prepare($sql4);
			$pdo->execute($excute4);
			
			if($productRow['btags'] && $productRow['web_product_tags']) {
					
				$productTagAry = explode(',', $productRow['web_product_tags']);
				$memberTagAry = explode(',', $productRow['btags']);
				
				foreach($productTagAry as $productTagKey => $productTag) {
					if(in_array($productTag, $memberTagAry)) {
						unset($memberTagAry[array_search($productTag,$memberTagAry)]);
					}
				}
				
				$sql2 = "Update web_member set tags = '".implode(',', $memberTagAry)."' Where web_member_id = '".$productRow['bweb_member_id']."' ";
				$rs2 = ConnectDB($DB, $sql2);
				
			}
			
			if($productRow['web_x_product_xx_product_id'] == '1') {
				if($Init_Coupon_Push) {
					if($productRow['blineID'] && 1) {
					
						if($productRow['blineID'] && 1) {
							if($productRow['web_x_product_xx_product_id'] == '3') {
								//服務
								$text = "您已成功取消".$productRow['prodSubjects']."如有需要協助的地方，請洽店員";
							} else {
								//票卷
								$text = "您已成功取消".$productRow['web_x_product_subject']." ".$productRow['prodSubjects']."如有需要協助的地方，請洽店員";
							}	
								
							$_REQUEST['ID'] = $productRow['blineID'];
							$_REQUEST['contnet'] = $text;
							$ch = curl_init();
							curl_setopt($ch, CURLOPT_URL, "https://".$_SERVER['HTTP_HOST']."/api.php");
							curl_setopt($ch, CURLOPT_HEADER, false);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
							$PostData = $_REQUEST;
							curl_setopt($ch, CURLOPT_POST, 1);
							curl_setopt($ch, CURLOPT_POSTFIELDS, $PostData);
							$temp = curl_exec($ch);
							// 關閉CURL連線
							curl_close($ch);
						}	
					}
				}	
			} else if($productRow['web_x_product_xx_product_id'] == '2') {
				if($Init_Stored_Push) {
					if($productRow['blineID'] && 1) {
					
						if($productRow['blineID'] && 1) {
							if($productRow['web_x_product_xx_product_id'] == '3') {
								//服務
								$text = "您已成功取消".$productRow['prodSubjects']."如有需要協助的地方，請洽店員";
							} else {
								//票卷
								$text = "您已成功取消".$productRow['web_x_product_subject']." ".$productRow['prodSubjects']."如有需要協助的地方，請洽店員";
							}	
								
							$_REQUEST['ID'] = $productRow['blineID'];
							$_REQUEST['contnet'] = $text;
							$ch = curl_init();
							curl_setopt($ch, CURLOPT_URL, "https://".$_SERVER['HTTP_HOST']."/api.php");
							curl_setopt($ch, CURLOPT_HEADER, false);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
							$PostData = $_REQUEST;
							curl_setopt($ch, CURLOPT_POST, 1);
							curl_setopt($ch, CURLOPT_POSTFIELDS, $PostData);
							$temp = curl_exec($ch);
							// 關閉CURL連線
							curl_close($ch);
						}	
					}
				}	
			} else if($productRow['web_x_product_xx_product_id'] == '3') {
				if($Init_Coupon_Push) {
					if($productRow['blineID'] && 1) {
					
						if($productRow['blineID'] && 1) {
							if($productRow['web_x_product_xx_product_id'] == '3') {
								//服務
								$text = "您已成功取消".$productRow['prodSubjects']."如有需要協助的地方，請洽店員";
							} else {
								//票卷
								$text = "您已成功取消".$productRow['web_x_product_subject']." ".$productRow['prodSubjects']."如有需要協助的地方，請洽店員";
							}	
								
							$_REQUEST['ID'] = $productRow['blineID'];
							$_REQUEST['contnet'] = $text;
							$ch = curl_init();
							curl_setopt($ch, CURLOPT_URL, "https://".$_SERVER['HTTP_HOST']."/api.php");
							curl_setopt($ch, CURLOPT_HEADER, false);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
							$PostData = $_REQUEST;
							curl_setopt($ch, CURLOPT_POST, 1);
							curl_setopt($ch, CURLOPT_POSTFIELDS, $PostData);
							$temp = curl_exec($ch);
							// 關閉CURL連線
							curl_close($ch);
						}	
					}
				}	
			}	
			
			
			$logInfo['error'] = 0;
			

			
		} else {
			$logInfo = array('error'=>'1', 'message'=> '系統維護中');
		}
		
		
		die(json_encode($logInfo, JSON_UNESCAPED_UNICODE));
		
	}
	
	//消費取消(儲值)
	if($action == "cancelOrder2") {
		
		$web_x_order_id = intval($_POST["web_x_order_id"]);
		$cancelReason = trim($_POST["cancelReason"]);
		$cancelRemark = trim($_POST["cancelRemark"]);
		$web_member_id = intval($_POST["admin"]);
		
		$ip = $_SERVER["HTTP_VIA"] ? $_SERVER["HTTP_X_FORWARDED_FOR"] : $_SERVER["REMOTE_ADDR"];	//IP
		
		//$sql = "Select * From web_x_order Where web_x_order_id = :web_x_order_id ";
		$sql = "
			SELECT 
				SQL_CALC_FOUND_ROWS a.*,
				b.web_member_id as bweb_member_id,
				b.uname as buname,
				b.birthday as bbirthday,
				b.mobile as bmobile,
				b.lineID as blineID,
				web_order.subject as prodSubjects,
				web_order.web_order_id as web_order_id,
				web_order.web_x_order_ordernum as web_x_order_ordernum,
				web_order.web_product_id as web_order_product_id,
				web_product.web_product_id as web_product_product_id,
				web_product.web_x_product_id as web_product_x_product_id,
				web_product.totalCount as web_product_totalCount,
				web_x_product.web_x_product_id as web_x_product_x_product_id,
				web_x_product.subject as web_x_product_subject,
				web_x_product.web_xx_product_id as web_x_product_xx_product_id
			FROM 
				web_x_order a
			Left Join 
				web_member b 
			ON 
				b.web_member_id = a.web_member_id
			Left Join
				web_order as web_order
			On
				web_order.web_x_order_ordernum = a.ordernum
			Left Join
				web_product
			On
				web_product.web_product_id = web_order.web_product_id
			Left Join
				web_x_product
			On
				web_x_product.web_x_product_id = web_product.web_x_product_id	
			where 
				a.web_x_order_id = :web_x_order_id
		";
		$excute = array(
			':web_x_order_id'        => $web_x_order_id,
		);
		//$debug = new Helper();
		//echo $test = $debug::debugPDO($sql, $excute);
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute($excute);
		$productRow = $pdo->fetch(PDO::FETCH_ASSOC);
		
		if($productRow['web_x_order_id']) {
			
			
			$_POST['date'] = date('Y-m-d H:i:s');
			$_POST['web_x_order_id'] = $productRow['web_x_order_id'];
			$_POST['cancelReason'] = $cancelReason;
			$_POST['cancelRemark'] = $cancelRemark;
			$_POST['cancelDateTime'] = date('Y-m-d H:i:s');
			$_POST['states'] = '取消';
			
			//取消前先計算儲值餘額是否足夠
			
			$sql = "
				Select 
					SUM(b.dimension) as sum
				From 
					web_x_order a
				Left Join 
					web_order b ON b.web_x_order_ordernum = a.ordernum
				Left Join 
					web_product c ON c.web_product_id = b.web_product_id
				Left Join 
					web_x_product d ON d.web_x_product_id = c.web_x_product_id
				Left Join 
					web_xx_product e ON e.web_xx_product_id = d.web_xx_product_id	
				Where 
					a.web_member_id = '".$productRow['bweb_member_id']."' 
				AND
					a.states = '訂單成立'
				AND
					a.paymentstatus = '付款成功'
				AND
					d.web_xx_product_id IN (2)
				order by 
					a.web_x_order_id desc 
			";
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute();
			$storedTotal = $pdo->fetch(PDO::FETCH_ASSOC);
			
			$sql = "
				Select 
					a.ordernum,
					a.total,
					b.price,
					a.web_birthday_money,
					a.all_discount
				From 
					web_x_order a
				Left Join 
					web_order b ON b.web_x_order_ordernum = a.ordernum
				Left Join 
					web_product c ON c.web_product_id = b.web_product_id
				Left Join 
					web_x_product d ON d.web_x_product_id = c.web_x_product_id
				Left Join 
					web_xx_product e ON e.web_xx_product_id = d.web_xx_product_id	
				Where 
					a.web_member_id = '".$web_member_id."' 
				AND
					a.states = '訂單成立'
				AND
					a.paymentstatus = '付款成功'
				AND
					d.web_xx_product_id IN (3)
				order by 
					a.web_x_order_id desc 
			";
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute();
			$useTotalRow = $pdo->fetchAll(PDO::FETCH_ASSOC);
			$useTotalArr = array();
			foreach($useTotalRow as $key => $useTotal) {
				$useTotalArr[$useTotal['ordernum']] = $useTotal['total'];
			}
			$balance = abs($storedTotal['sum'] - array_sum($useTotalArr));
			//echo $productRow['total'] ."==". $balance."==".$storedTotal['sum']."==".$useTotal['sum'];
			//exit;
			if($productRow['total'] > $balance) {
				$logInfo = array('error'=>'1', 'message'=> '儲值金額不足');
				die(json_encode($logInfo, JSON_UNESCAPED_UNICODE));
			}
			
			//$sql = "Select * From web_member Where web_member_id = :web_member_id ";
			$sql = "Select * From web_adminuser Where web_adminuser_id = :web_member_id ";
			$excute = array(
				':web_member_id'        => $web_member_id,
			);
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute($excute);
			$memberRow = $pdo->fetch(PDO::FETCH_ASSOC);
			
			$_POST['cancelUser'] = $memberRow['uname'];
			
			if (1) {
				$TableName = "web_x_order";
				$sql = "SHOW FULL FIELDS FROM `".$TableName."`";
				$rs = ConnectDB($DB, $sql);
				$tabeInfoAry = array();
				for ($i=0; $i<mysql_num_rows($rs); $i++) {
					$row = mysql_fetch_assoc($rs);
					$tabeInfoAry[$row[Field]] = $row[Comment];
				}

				$sql2 = "Select * From web_x_order Where web_x_order_id like '".$_POST[web_x_order_id]."' ";
				$rs2 = ConnectDB($DB, $sql2);	
				$inDbInfo = array();
				for ($i=0; $i<mysql_num_rows($rs2); $i++) {
					$row2 = mysql_fetch_assoc($rs2);
					$inDbInfo = $row2;
				}
				$_inDbInfo = array();
				$postAry = array();
				foreach($_POST as $key => $postVal) {
					//echo $key.": ".$tabeInfoAry[$key].": ".$postVal."</br>";
					$_inDbInfo[$tabeInfoAry[$key]] = $inDbInfo[$key];
					$postAry[$tabeInfoAry[$key]] = $postVal;
				}
				//exit;
				$result = array_diff_assoc($postAry,$_inDbInfo);
				//查看是否有更改
				if(count($result)) {
					$recLogAry = array();
					$_recLogAry = array();
					foreach($result as $key => $postVal) {
						//echo '原資料:'.$_inDbInfo[$key]."</br>";
						//$_recLogAry[$key] = $_inDbInfo[$key]." -> ".$postVal;
						if(in_array($key, array('accept_time', 'invoicetype'))) {
							continue;
						}
						$recLogAry[urlencode($key)] = urlencode($_inDbInfo[$key])." -> ".urlencode($postVal);
					}
					$recLogAry[urlencode(操作日期)] = $_POST['date'];
					$recLogAry[urlencode(操作者)] = urlencode($memberRow['uname']);
					$recLogAry[IP] = $ip;
					$recLog = json_encode($recLogAry);

					if(count($recLogAry)) {
						//$sql3 = "Update web_x_order set log = '".$recLog."' Where web_x_order_id = '".$_POST[web_x_order_id]."'";
						$sql3 = "
							Insert into 
								web_orderlog 
									(
										ordernum, 
										log, 
										uname, 
										IP, 
										date
									) 
								values 
									(
										'".$inDbInfo[ordernum]."', 
										'$recLog', 
										'".$_POST['editUser']."', 
										'$ip', 
										'".$_POST['date']."'
									) 
						";	
						$rs3 = ConnectDB($DB, $sql3);
					}
				}
			}
			
			$sql4 = "UPDATE web_x_order set states = :states, cancelUser = :cancelUser, cancelDateTime = :cancelDateTime, date = :date, cancelReason = :cancelReason, cancelRemark = :cancelRemark Where web_x_order_id = :web_x_order_id ";
			$excute4 = array(
				':states'        		=> $_POST['states'],
				':cancelUser'  			=> $_POST['cancelUser'],
				':date'       			=> $_POST['date'],
				':cancelDateTime'		=> $_POST['cancelDateTime'],
				':cancelReason'    		=> $_POST['cancelReason'],
				':cancelRemark'    		=> $_POST['cancelRemark'],
				':web_x_order_id'    	=> $web_x_order_id,
			);
			//$debug = new Helper();
			//echo $test = $debug::debugPDO($sql4, $excute4);
			$pdo = $pdoDB->prepare($sql4);
			$pdo->execute($excute4);
			
			if($Init_Stored_Push) {
				if($productRow['blineID'] && 1) {
				
					if($productRow['blineID'] && 1) {
						
						//$text = "客戶【".$productRow['buname']."】取消".$productRow['web_x_product_subject'].$productRow['prodSubjects']."\r\n 取消原因 ".$_POST['cancelReason']." ".$_POST['cancelRemark']."\r\n 取消人員 ".$_POST['cancelUser']."\r\n 取消時間 ".$_POST['cancelDateTime'];
						
						$text = "您已成功取消消費".$productRow['prodSubjects']."如有需要協助的地方，請洽店員";
							
						$_REQUEST['ID'] = $productRow['blineID'];
						$_REQUEST['contnet'] = $text;
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL, "https://".$_SERVER['HTTP_HOST']."/api.php");
						curl_setopt($ch, CURLOPT_HEADER, false);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
						$PostData = $_REQUEST;
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $PostData);
						$temp = curl_exec($ch);
						// 關閉CURL連線
						curl_close($ch);
					}	
				}
			}
			
			$logInfo['error'] = 0;
			

			
		} else {
			$logInfo = array('error'=>'1', 'message'=> '系統維護中');
		}
		
		
		die(json_encode($logInfo, JSON_UNESCAPED_UNICODE));
		
	}
	
	//建立訂單
	if($action == "useOrder") {
		
		$TableName = "web_x_order";
		$AccurateAction = "Get";
		require("control/includes/accurate.php");
		
		$web_member_id = intval($_POST["web_member_id"]);
		$web_product_id = intval($_POST["web_product_id"]);
		$from_ordernum = trim($_POST['ordernum']);
		$admin = intval($_POST['admin']);
		$shipdate = ($_POST['shipdate']);
		$store_id = intval($_POST['store_id']);
		
		$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM web_member where web_member.web_member_id = :web_member_id";
		$excute = array(
			':web_member_id'       	=> $web_member_id,
		);
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute($excute);
		$memberRow = $pdo->fetch(PDO::FETCH_ASSOC);
		
		$_POST['web_member_id'] = $memberRow['web_member_id'];
		$_POST['order_name'] = $memberRow['uname'];
		$_POST['order_mobile'] = $memberRow['mobile'];
		$_POST['accept_name'] = $memberRow['uname'];
		$_POST['accept_mobile'] = $memberRow['mobile'];
		$_POST['shipdate'] = $shipdate;
		$_POST['from_ordernum'] = $from_ordernum;
		$_POST['order_type'] = 1;
		
		//$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM web_member where web_member.web_member_id = :web_member_id";
		$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM web_adminuser where web_adminuser.web_adminuser_id = :web_member_id";
		$excute = array(
			':web_member_id'       	=> $admin,
		);
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute($excute);
		$editUsrRow = $pdo->fetch(PDO::FETCH_ASSOC);
		$_POST['editUser'] = $editUsrRow['uname'];
		$_POST['editDateTime'] = date('Y-m-d H:i:s');
		
		
		$sql = "
			SELECT 
				SQL_CALC_FOUND_ROWS a.*,
				b.web_xx_product_id as bweb_xx_product_id,
				c.subject as csubject
			FROM 
				web_product a
			Left Join 
				web_x_product b ON b.web_x_product_id = a.web_x_product_id
			Left Join 
				web_xx_product c ON c.web_xx_product_id = b.web_xx_product_id		
			where 
				a.web_product_id = :web_product_id 
			ORDER BY 
				a.displayorder ASC
		";
		$excute = array(
			//':ifShow'        			=> 1,
			':web_product_id'       	=> $web_product_id,
		);
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute($excute);
		$productRow = $pdo->fetch(PDO::FETCH_ASSOC);
		if(!$productRow) {
			$req = array('error'=>'1', 'message'=> '系統維護中');
			
		} else {
			
			
			$sql = "
				SELECT 
					web_x_order.*,
					web_order.subject as prodSubjects,
					web_order.web_order_id as web_order_id,
					web_order.web_x_order_ordernum as web_x_order_ordernum,
					web_order.web_product_id as web_order_product_id,
					web_product.web_product_id as web_product_product_id,
					web_product.web_x_product_id as web_product_x_product_id,
					web_product.totalCount as web_product_totalCount,
					web_product.Covers as web_product_Covers,
					web_product.useEdate as web_product_useEdate,
					web_x_product.web_x_product_id as web_x_product_x_product_id,
					web_x_product.web_xx_product_id as web_x_product_xx_product_id
				from 
					web_x_order
				Left Join
					web_order as web_order
				On
					web_order.web_x_order_ordernum = web_x_order.ordernum
				Left Join
					web_product
				On
					web_product.web_product_id = web_order.web_product_id
				Left Join
					web_x_product
				On
					web_x_product.web_x_product_id = web_product.web_x_product_id
				WHERE 
					web_x_order.from_ordernum = :ordernum
				AND
					web_x_order.states = '訂單成立'	
				AND
					web_x_order.paymentstatus = '付款成功'	
				AND
					web_x_order.order_type = 1
				AND
					web_x_product.web_xx_product_id = 1		
			";
			
			$excute = array(
				':ordernum'		=> $from_ordernum,
			);
			//$debug = new Helper();
			//echo $test = $debug::debugPDO($sql, $excute);
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute($excute);
			$row4 = $pdo->fetchAll(PDO::FETCH_ASSOC);
			
			if(abs($productRow['totalCount'] - count($row4)) <= '0') {
				$req = array('error'=>'1', 'message'=> '票卷已使用完!!!');
				die(json_encode($req, JSON_UNESCAPED_UNICODE));
			}
		
			$ordernum = "";
			$date = substr(date("Y"), 2, 2).date("mdHi");
			$sql = "Select ordernum From web_x_order order by ordernum desc limit 1";
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute();	
			$web_x_order_info = $pdo->fetch(PDO::FETCH_ASSOC);
			if ($web_x_order_info>0) $ordernum = $web_x_order_info['ordernum'];
			if (substr($ordernum, 1, 10) != substr($date, 0, 10))	//尚無訂單編號
				$ordernum = $date."0001";
			else	//已有訂單編號
				$ordernum = $date.sprintf("%04d", substr($ordernum, 11, 4) + 1);

				
			$_POST['ordernum'] = "N".$ordernum;			
			
			$_POST['states'] = "訂單成立";	//訂單狀態
			$_POST['paymentstatus'] = "付款成功";	//訂單狀態
			
			$_POST['subtotal'] = $productRow['price_member'];	//小計
			$_POST['store_id'] = $store_id;
			
			$_POST['total'] = $_POST['subtotal'];		//總計=小計-優惠代碼-紅利+運費
			
			$_POST['cdate'] = date("Y-m-d H:i:s");	//訂購時間
			$_POST['IP'] = $_SERVER["HTTP_VIA"] ? $_SERVER["HTTP_X_FORWARDED_FOR"] : $_SERVER["REMOTE_ADDR"];	//IP
			$_POST['agent'] = ($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : null;
			
			//$debug = true;
			$AccurateAction = "Insert";
			require("control/includes/accurate.php");
			
			$sql = "Select web_x_order_id, ordernum From web_x_order Where ordernum like '".$_POST['ordernum']."' order by web_x_order_id desc limit 1 ";
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute();
			$web_x_order_info = $pdo->fetch(PDO::FETCH_ASSOC);
		
			if (!$web_x_order_info) {
				$req = array('error'=>'1', 'message'=> '訂單錯誤');

			} else {
				$web_x_order_ordernum = $web_x_order_info['ordernum'];	//訂單編號
			}
			
			$dimension = ($productRow['bweb_xx_product_id']) ? $productRow['price_cost'] : null;
			$sql = "Insert into web_order (web_x_order_ordernum, web_product_id, web_x_sale_id, ifGeneral, additional, fullAdditional, subject, serialnumber, pincode, dimension, price, num, discount, masterId, cdate) values ('$web_x_order_ordernum', '".$productRow['web_product_id']."', '', '', '', '', '".$productRow['subject']."', '', '', '".$dimension."', '".$productRow['price_member']."', '1', '', '', '".$_POST['cdate']."') ";
			//echo $sql;
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute();	
			
			//扣庫存
			/*
			$sql = "Update web_product set stock = stock - ".$num." Where web_product_id = '".$web_product_id."' ";
			$rs = ConnectDB($DB, $sql);
			*/
			if($Init_Coupon_Push) {
				if($memberRow['lineID']) {
				
					if($memberRow['lineID'] && 1) {
						//您已成功消費"法朵頭皮養護療程"，使用儲值金500元，剩下金額1500元
						$text = "您已成功消費".$productRow['subject'].$productRow['csubject']."，使用票券1張，剩餘票券".abs($productRow['totalCount'] - (count($row4) + 1))."張";
							
						$_REQUEST['ID'] = $memberRow['lineID'];
						$_REQUEST['contnet'] = $text;
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL, "https://".$_SERVER['HTTP_HOST']."/api.php");
						curl_setopt($ch, CURLOPT_HEADER, false);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
						$PostData = $_REQUEST;
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $PostData);
						$temp = curl_exec($ch);
						// 關閉CURL連線
						curl_close($ch);
					}	
				}
			}	
			$req = array('error'=>'0');
			
		}
		die(json_encode($req, JSON_UNESCAPED_UNICODE));
		
	}
	
	//建立訂單-儲值
	if($action == "useOrder2") {
		
		$TableName = "web_x_order";
		$AccurateAction = "Get";
		require("control/includes/accurate.php");
		
		$web_member_id = intval($_POST["web_member_id"]);
		//$web_product_id = intval($_POST["web_product_id"]);
		$from_ordernum = trim($_POST['ordernum']);
		$admin = intval($_POST['admin']);
		$shipdate = ($_POST['shipdate']);
		$store_id = intval($_POST['store_id']);
		
		if(!$_POST["web_product_id"] && (!$_POST['customName'] && !$_POST['customNamePrice'])) {
			$req = array('error'=>'1', 'message'=> '請選服務項目');
			die(json_encode($req, JSON_UNESCAPED_UNICODE));
		}
		
		$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM web_member where web_member.web_member_id = :web_member_id";
		$excute = array(
			':web_member_id'       	=> $web_member_id,
		);
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute($excute);
		$memberRow = $pdo->fetch(PDO::FETCH_ASSOC);
		
		$_POST['web_member_id'] = $memberRow['web_member_id'];
		$_POST['order_name'] = $memberRow['uname'];
		$_POST['order_mobile'] = $memberRow['mobile'];
		$_POST['accept_name'] = $memberRow['uname'];
		$_POST['accept_mobile'] = $memberRow['mobile'];
		$_POST['shipdate'] = $shipdate;
		$_POST['from_ordernum'] = $from_ordernum;
		$_POST['order_type'] = 2;
		
		//$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM web_member where web_member.web_member_id = :web_member_id";
		$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM web_adminuser where web_adminuser.web_adminuser_id = :web_member_id";
		$excute = array(
			':web_member_id'       	=> $admin,
		);
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute($excute);
		$editUsrRow = $pdo->fetch(PDO::FETCH_ASSOC);
		$_POST['editUser'] = $editUsrRow['uname'];
		$_POST['editDateTime'] = date('Y-m-d H:i:s');
		
		if($_POST['customName'] && $_POST['customNamePrice']) {
			
			$sql = "
				SELECT 
					SQL_CALC_FOUND_ROWS * 
				FROM 
					web_product 
				WHERE 
					web_x_product_id = :web_x_product_id
				AND
					ifShow = :ifShow
				AND
					subject LIKE :subject
				AND
					price_cost = :price_cost
				AND
					price_public = :price_public
				AND
					price_member = :price_member	
			";
			$excute = array(
				':web_x_product_id'        	=> '8',
				':ifShow' 					=> '1',
				':subject'     				=> '%自訂 - '.$_POST['customName'].'%',
				':price_cost'        		=> $_POST['customNamePrice'],
				':price_public'            	=> $_POST['customNamePrice'],
				':price_member'            	=> $_POST['customNamePrice'],
			);
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute($excute);
			$editProduct = $pdo->fetch(PDO::FETCH_ASSOC);
			
			if(!$editProduct) {
				$sql3 = "
					Insert into 
						web_product 
							(
								web_x_product_id,
								ifShow,
								ifGeneral,
								subject,
								stock,
								price_cost,
								price_public,
								price_member,
								sdate,
								edate,
								cdate
							) 
						values 
							(
								:web_x_product_id,
								:ifShow,
								:ifGeneral,
								:subject,
								:stock,
								:price_cost,
								:price_public,
								:price_member,
								:sdate,
								:edate,
								:cdate
							) 
				";	
				$excute = array(
					':web_x_product_id'        	=> '8',
					':ifShow' 					=> '1',
					':ifGeneral'				=> '1',
					':subject'     				=> '自訂 - '.$_POST['customName'],
					':stock'     				=> '99999',
					':price_cost'        		=> $_POST['customNamePrice'],
					':price_public'            	=> $_POST['customNamePrice'],
					':price_member'            	=> $_POST['customNamePrice'],
					':sdate'            		=> date('Y-m-d H:i:S'),
					':edate'            		=> date('Y-m-d H:i:S', strtotime('+5 year')),
					':cdate'            		=> date('Y-m-d H:i:S'),
				);
				$pdo = $pdoDB->prepare($sql3);
				$pdo->execute($excute);
				$web_product_id = $pdoDB->lastInsertId();
				
				if($_POST["web_product_id"]) {
					array_push($_POST["web_product_id"], $web_product_id);
				} else {
					$_POST["web_product_id"][] = $web_product_id;
				}
			} else {
				
				if($_POST["web_product_id"]) {
					array_push($_POST["web_product_id"], $editProduct['web_product_id']);
				} else {
					$_POST["web_product_id"][] = $editProduct['web_product_id'];
				}
			}	
		}
		
		$sql = "
			SELECT 
				SQL_CALC_FOUND_ROWS a.*,
				b.web_xx_product_id as bweb_xx_product_id,
				b.subject as bsubject,
				c.subject as csubject
			FROM 
				web_product a
			Left Join 
				web_x_product b ON b.web_x_product_id = a.web_x_product_id
			Left Join 
				web_xx_product c ON c.web_xx_product_id = b.web_xx_product_id		
			where 
				a.ifShow = :ifShow 
		";
		/*
		$sql .= "		
			and 
				a.web_product_id = :web_product_id 
		";
		*/
		$sql .= "		
			and 
				a.web_product_id IN (".implode(',', $_POST["web_product_id"]).") 
		";
		$sql .= "
			ORDER BY 
				a.displayorder ASC
		";
		$excute = array(
			':ifShow'        			=> 1,
			//':web_product_id'       	=> $web_product_id,
		);
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute($excute);
		$productRow = $pdo->fetchAll(PDO::FETCH_ASSOC);
		
		$sql = "
			SELECT 
				SUM(a.price_member) as subtotal
			FROM 
				web_product a
			Left Join 
				web_x_product b ON b.web_x_product_id = a.web_x_product_id
			Left Join 
				web_xx_product c ON c.web_xx_product_id = b.web_xx_product_id		
			where 
				a.ifShow = :ifShow 
		";
		$sql .= "		
			and 
				a.web_product_id IN (".implode(',', $_POST["web_product_id"]).") 
		";
		$sql .= "
			ORDER BY 
				a.displayorder ASC
		";
		$excute = array(
			':ifShow'        			=> 1,
		);
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute($excute);
		$subtotal = $pdo->fetch(PDO::FETCH_ASSOC);
		
		if(!$productRow) {
			$req = array('error'=>'1', 'message'=> '系統維護中');
			
		} else {
		
			$ordernum = "";
			$date = substr(date("Y"), 2, 2).date("mdHi");
			$sql = "Select ordernum From web_x_order order by ordernum desc limit 1";
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute();	
			$web_x_order_info = $pdo->fetch(PDO::FETCH_ASSOC);
			if ($web_x_order_info>0) $ordernum = $web_x_order_info['ordernum'];
			if (substr($ordernum, 1, 10) != substr($date, 0, 10))	//尚無訂單編號
				$ordernum = $date."0001";
			else	//已有訂單編號
				$ordernum = $date.sprintf("%04d", substr($ordernum, 11, 4) + 1);

				
			$_POST['ordernum'] = "N".$ordernum;			
			
			$_POST['states'] = "訂單成立";	//訂單狀態
			$_POST['paymentstatus'] = "付款成功";	//訂單狀態
			
			$_POST['store_id'] = $store_id;
			
			//生日禮
			if($_POST['web_member_money'] && $_POST['web_member_money_id']) {
				$_POST['web_birthday_money_id'] = $_POST['web_member_money_id'];
				$_POST['web_birthday_money'] = $_POST['web_member_money'];
			}
			
			//$_POST['subtotal'] = ($_POST['web_member_money'] > $subtotal['subtotal']) ? $subtotal['subtotal'] - $subtotal['subtotal'] : $subtotal['subtotal'] - $_POST['web_member_money'];	//小計
			
			$_POST['subtotal'] = $subtotal['subtotal'];	//小計
			
			
			$sql = "
				Select 
					SUM(b.dimension) as sum
				From 
					web_x_order a
				Left Join 
					web_order b ON b.web_x_order_ordernum = a.ordernum
				Left Join 
					web_product c ON c.web_product_id = b.web_product_id
				Left Join 
					web_x_product d ON d.web_x_product_id = c.web_x_product_id
				Left Join 
					web_xx_product e ON e.web_xx_product_id = d.web_xx_product_id	
				Where 
					a.web_member_id = '".$memberRow['web_member_id']."' 
				AND
					a.states = '訂單成立'
				AND
					a.paymentstatus = '付款成功'
				AND
					d.web_xx_product_id IN (2)
				order by 
					a.web_x_order_id desc 
			";
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute();
			$storedTotal = $pdo->fetch(PDO::FETCH_ASSOC);
			
			$sql = "
				Select 
					a.ordernum,
					a.total,
					b.price,
					a.web_birthday_money,
					a.all_discount
				From 
					web_x_order a
				Left Join 
					web_order b ON b.web_x_order_ordernum = a.ordernum
				Left Join 
					web_product c ON c.web_product_id = b.web_product_id
				Left Join 
					web_x_product d ON d.web_x_product_id = c.web_x_product_id
				Left Join 
					web_xx_product e ON e.web_xx_product_id = d.web_xx_product_id	
				Where 
					a.web_member_id = '".$web_member_id."' 
				AND
					a.states = '訂單成立'
				AND
					a.paymentstatus = '付款成功'
				AND
					d.web_xx_product_id IN (3)
				order by 
					a.web_x_order_id desc 
			";
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute();
			$useTotalRow = $pdo->fetchAll(PDO::FETCH_ASSOC);
			$useTotalArr = array();
			foreach($useTotalRow as $key => $useTotal) {
				$useTotalArr[$useTotal['ordernum']] = $useTotal['total'];
			}
			$balance = abs($storedTotal['sum'] - array_sum($useTotalArr));
			
			$_POST['all_discount'] = $_POST['Init_All_Discount'];
			
			if(!$_POST['web_birthday_money']) {
				$_POST['total'] = ($_POST['all_discount'] != '0') ? round($_POST['subtotal'] * ($_POST['all_discount'] / 100)) : $_POST['subtotal'];		//總計=小計-優惠代碼-紅利+運費
			} else {
				$_subtotal = ($_POST['web_member_money'] > $subtotal['subtotal']) ? $subtotal['subtotal'] - $subtotal['subtotal'] : $subtotal['subtotal'] - $_POST['web_member_money'];
				
				$_POST['total'] = ($_POST['all_discount'] != '0') ? round($_subtotal * ($_POST['all_discount'] / 100)) : $_subtotal;		//總計=小計-優惠代碼-紅利+運費
			}	
			
			if($_POST['total'] > $balance) {
				$req = array('error'=>'1', 'message'=> '儲值金額不足');
				die(json_encode($req, JSON_UNESCAPED_UNICODE));	
			}
			
			$_POST['cdate'] = date("Y-m-d H:i:s");	//訂購時間
			$_POST['IP'] = $_SERVER["HTTP_VIA"] ? $_SERVER["HTTP_X_FORWARDED_FOR"] : $_SERVER["REMOTE_ADDR"];	//IP
			$_POST['agent'] = ($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : null;
			
			//$debug = true;
			$AccurateAction = "Insert";
			require("control/includes/accurate.php");
			
			$sql = "Select web_x_order_id, ordernum From web_x_order Where ordernum like '".$_POST['ordernum']."' order by web_x_order_id desc limit 1 ";
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute();
			$web_x_order_info = $pdo->fetch(PDO::FETCH_ASSOC);
		
			if (!$web_x_order_info) {
				$req = array('error'=>'1', 'message'=> '訂單錯誤');

			} else {
				$web_x_order_ordernum = $web_x_order_info['ordernum'];	//訂單編號
			}
			$prodSubjectAry = array();
			foreach($productRow as $key =>$_productRow) {
				
				$dimension = ($_productRow['bweb_xx_product_id']) ? $_productRow['price_cost'] : null;
				$sql = "Insert into web_order (web_x_order_ordernum, web_product_id, web_x_sale_id, ifGeneral, additional, fullAdditional, subject, serialnumber, pincode, dimension, price, num, discount, masterId, cdate) values ('$web_x_order_ordernum', '".$_productRow['web_product_id']."', '', '', '', '', '".$_productRow['subject']."', '', '', '".$dimension."', '".$_productRow['price_member']."', '1', '', '', '".$_POST['cdate']."') ";
				//echo $sql;
				$pdo = $pdoDB->prepare($sql);
				$pdo->execute();
				
				$prodSubjectAry[] = $_productRow['bsubject']."-".$_productRow['subject']." ".$_productRow['price_member'];
			}		
			
			//扣生日禮
			if($_POST['web_member_money'] && $_POST['web_member_money_id']) {
				$sql = "Update web_member_money set kind = '3', web_x_order_to_ordernum = '".$web_x_order_ordernum."', usedate = '".date('Y-m-d H:i:s')."' Where web_member_money_id = '".$_POST['web_member_money_id']."' ";
				$rs = ConnectDB($DB, $sql);
			}
			
			//扣庫存
			/*
			$sql = "Update web_product set stock = stock - ".$num." Where web_product_id = '".$web_product_id."' ";
			$rs = ConnectDB($DB, $sql);
			*/
			if($Init_Stored_Push) {
				if($memberRow['lineID']) {
				
					if($memberRow['lineID'] && 1) {
						$sql = "Select subject From web_x_class Where web_x_class_id = '".$store_id."'";
						$pdo = $pdoDB->prepare($sql);
						$pdo->execute();
						$store_info = $pdo->fetch(PDO::FETCH_ASSOC);
						
						//$text = "客戶【".$memberRow['uname']."】使用".$productRow['csubject'].$productRow['subject'];
						$text = '';
						$text .= "您已成功於【".$store_info['subject']."】消費:\n".implode("\n", $prodSubjectAry);
						if($_POST['web_birthday_money']) {
							
							$text .= "\n生日禮 -".number_format($_POST['web_birthday_money'])."元";
							$text .= "\n總計:".number_format($_subtotal);
							if($_POST['all_discount']) {
								$text .= ",儲值".(($_POST['all_discount']/100)*10)."折:".number_format($_POST['total']);
							}
							
						} else {
							$text .= "\n總計:".number_format($subtotal['subtotal']);
							if($_POST['all_discount']) {
								$text .= ",儲值".(($_POST['all_discount']/100)*10)."折:".number_format($_POST['total']);
							}
						}	
						$text .= "\n使用儲值金".number_format($_POST['total'])."元";
						$text .= "\n剩下金額".number_format($balance - $_POST['total'])."元";
							
						$_REQUEST['ID'] = $memberRow['lineID'];
						$_REQUEST['contnet'] = $text;
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL, "https://".$_SERVER['HTTP_HOST']."/api.php");
						curl_setopt($ch, CURLOPT_HEADER, false);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
						$PostData = $_REQUEST;
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $PostData);
						$temp = curl_exec($ch);
						// 關閉CURL連線
						curl_close($ch);
					}	
				}
			}	
			$req = array('error'=>'0', 'balance' => number_format(abs($balance - $_POST['total'])));
			
		}
		die(json_encode($req, JSON_UNESCAPED_UNICODE));
		
	}
	
	//建立訂單-儲值
	if($action == "useBonus") {
		
		$TableName = "web_x_convert";
		$AccurateAction = "Get";
		require("control/includes/accurate.php");
		
		$web_member_id = intval($_POST["web_member_id"]);
		$web_product_id = intval($_POST["web_product_id"]);
		$from_ordernum = trim($_POST['ordernum']);
		//$admin = intval($_POST['admin']);
		//$shipdate = ($_POST['shipdate']);
		$store_id = intval($_POST['store_id']);
		
		if(!$_POST["web_product_id"]) {
			$req = array('error'=>'1', 'message'=> '請選兌換項目');
			die(json_encode($req, JSON_UNESCAPED_UNICODE));
		}
		
		$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM web_member where web_member.web_member_id = :web_member_id";
		$excute = array(
			':web_member_id'       	=> $web_member_id,
		);
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute($excute);
		$memberRow = $pdo->fetch(PDO::FETCH_ASSOC);
		
		$_POST['web_member_id'] = $memberRow['web_member_id'];
		$_POST['order_name'] = $memberRow['uname'];
		$_POST['order_mobile'] = $memberRow['mobile'];
		$_POST['accept_name'] = $memberRow['uname'];
		$_POST['accept_mobile'] = $memberRow['mobile'];
		//$_POST['shipdate'] = $shipdate;
		$_POST['from_ordernum'] = $from_ordernum;
		$_POST['order_type'] = 0;
		
		$sql = "
			SELECT 
				SQL_CALC_FOUND_ROWS a.*,
				b.web_xx_product_id as bweb_xx_product_id,
				b.subject as bsubject,
				c.subject as csubject
			FROM 
				web_product a
			Left Join 
				web_x_product b ON b.web_x_product_id = a.web_x_product_id
			Left Join 
				web_xx_product c ON c.web_xx_product_id = b.web_xx_product_id		
			where 
				a.ifShow = :ifShow 
		";
		$sql .= "		
			and 
				a.web_product_id = :web_product_id 
		";
		/*
		$sql .= "		
			and 
				a.web_product_id IN (".implode(',', $_POST["web_product_id"]).") 
		";
		*/
		$sql .= "
			ORDER BY 
				a.displayorder ASC
		";
	
		$excute = array(
			':ifShow'        			=> 1,
			':web_product_id'       	=> $web_product_id,
		);
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute($excute);
		$productRow = $pdo->fetchAll(PDO::FETCH_ASSOC);
		
		$sql = "
			SELECT 
				SUM(a.price_member) as subtotal
			FROM 
				web_product a
			Left Join 
				web_x_product b ON b.web_x_product_id = a.web_x_product_id
			Left Join 
				web_xx_product c ON c.web_xx_product_id = b.web_xx_product_id		
			where 
				a.ifShow = :ifShow 
		";
		$sql .= "		
			and 
				a.web_product_id = :web_product_id 
		";
		/*
		$sql .= "		
			and 
				a.web_product_id IN (".implode(',', $_POST["web_product_id"]).") 
		";
		*/
		$sql .= "
			ORDER BY 
				a.displayorder ASC
		";
		$excute = array(
			':ifShow'        			=> 1,
			':web_product_id'       	=> $web_product_id,
		);
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute($excute);
		$subtotal = $pdo->fetch(PDO::FETCH_ASSOC);
	
		if(!$productRow) {
			$req = array('error'=>'1', 'message'=> '系統維護中');
			
		} else {
		
			$ordernum = "";
			$date = substr(date("Y"), 2, 2).date("mdHi");
			$sql = "Select ordernum From web_x_convert order by ordernum desc limit 1";
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute();	
			$web_x_order_info = $pdo->fetch(PDO::FETCH_ASSOC);
			if ($web_x_order_info>0) $ordernum = $web_x_order_info['ordernum'];
			if (substr($ordernum, 1, 10) != substr($date, 0, 10))	//尚無訂單編號
				$ordernum = $date."0001";
			else	//已有訂單編號
				$ordernum = $date.sprintf("%04d", substr($ordernum, 11, 4) + 1);

				
			$_POST['ordernum'] = "N".$ordernum;			
			
			$_POST['states'] = "兌換成立";	//訂單狀態
			$_POST['paymentstatus'] = "未兌換";	//訂單狀態
			
			$_POST['store_id'] = $store_id;
			
			$_POST['subtotal'] = $subtotal['subtotal'];	//小計
			
			
			$sql = "
				Select 
					SUM(a.money) as sum,
					SUM(a.usemoney) as useSum,
					GROUP_CONCAT(DISTINCT CONCAT(a.web_bonus_id, ':', a.money, ':', a.usemoney, ':', a.used) SEPARATOR ',') AS bonusAry
				From 
					web_bonus a
				Left Join 
					web_x_order b ON b.ordernum = a.web_x_order_from_ordernum
				Where 
					a.web_member_id = '".$web_member_id."'
				AND
					a.money > a.usemoney
				AND
					a.sdate <= '".date('Y-m-d')."'
				AND
					a.edate >= '".date('Y-m-d')."'
				AND
					b.states = '訂單成立'
				AND
					b.paymentstatus = '付款成功'
				order by 
					a.web_bonus_id desc 
			";
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute();
			$bonusTotal = $pdo->fetch(PDO::FETCH_ASSOC);
			
			$bonus = abs($bonusTotal['sum'] - $bonusTotal['useSum']);
			
			$_POST['total'] = $_POST['subtotal'];		//總計=小計-優惠代碼-紅利+運費
			
			if($_POST['total'] > $bonus) {
				$req = array('error'=>'1', 'message'=> '兌換點數不足');
				die(json_encode($req, JSON_UNESCAPED_UNICODE));	
			}
	
			$_POST['cdate'] = date("Y-m-d H:i:s");	//訂購時間
			$_POST['IP'] = $_SERVER["HTTP_VIA"] ? $_SERVER["HTTP_X_FORWARDED_FOR"] : $_SERVER["REMOTE_ADDR"];	//IP
			$_POST['agent'] = ($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : null;
			
			//$debug = true;
			$AccurateAction = "Insert";
			require("control/includes/accurate.php");
			
			$sql = "Select web_x_order_id, ordernum From web_x_convert Where ordernum like '".$_POST['ordernum']."' order by web_x_order_id desc limit 1 ";
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute();
			$web_x_order_info = $pdo->fetch(PDO::FETCH_ASSOC);
		
			if (!$web_x_order_info) {
				$req = array('error'=>'1', 'message'=> '兌換單錯誤');

			} else {
				$web_x_order_ordernum = $web_x_order_info['ordernum'];	//訂單編號
			}
			/***********************************點數計算***********************************/
			$_transAry = array();
			$editFlag = 1;
			$recTotalMoney = 0;
			$recTotalUseMoney = 0;
			$lastTotal = array();
			foreach(explode(',', $bonusTotal['bonusAry']) as $key => $bonusList) {
				$_transAry2 = explode(':', $bonusList);
				$_transAry[$_transAry2[0]]['money'] = $_transAry2[1];
				$_transAry[$_transAry2[0]]['usemoney'] = $_transAry2[2];
				$_transAry[$_transAry2[0]]['used'] = $_transAry2[3];
	
				$recTotalMoney += $_transAry2[1];
				$recTotalUseMoney += $_transAry2[2];
			}
	
			//exit;
			$mustPay = $_POST['total'];
			foreach($_transAry as $key2 => $_trnas) {
				if($editFlag) {
					//if(($recTotalMoney - $recTotalUseMoney) >= $_POST['total']) {
					if(($recTotalMoney - $recTotalUseMoney) >= $mustPay) {	
						
						//先找各筆單數是否足夠兌換
						//if(($_trnas['money'] - $_trnas['usemoney']) >= $_POST['total']) {
						if(($_trnas['money'] - $_trnas['usemoney']) >= $mustPay) {	
							unset($usedAry);
							if($_trnas['used']) {
								$usedAry[count($_transAry)] = $_trnas['used'];
							}
							
							//$usedAry[] = $web_x_order_ordernum.'-'.$_POST['total'];
							$usedAry[] = $web_x_order_ordernum.'-'.$mustPay;
							
							
							$sql = "Update web_bonus set usemoney = ".abs($_trnas['usemoney'] + $mustPay).", usedate = '".date('Y-m-d H:i:s')."', used = '".implode(';', $usedAry)."' Where web_bonus_id = '".$key2."' ";
							$rs = ConnectDB($DB, $sql);
							
							$editFlag = 0;
						
						//剩餘加總兌換						
						} else {
							
							if(!$mustPay) {
								$editFlag = 0;
							} else {
								
								if(abs($_trnas['money'] - $_trnas['usemoney']) <= $mustPay) {
									
									//$lastTotal[] = abs($_trnas['money'] - $_trnas['usemoney']);
									unset($usedAry);
									if($_trnas['used']) {
										$usedAry[count($_transAry)] = $_trnas['used'];
									}
									
									$usedAry[] = $web_x_order_ordernum.'-'.abs($_trnas['money'] - $_trnas['usemoney']);
									
									$sql = "Update web_bonus set usemoney = ".abs($_trnas['money']).", usedate = '".date('Y-m-d H:i:s')."', used = '".implode(';', $usedAry)."' Where web_bonus_id = '".$key2."' ";
									$rs = ConnectDB($DB, $sql);
									
									$mustPay = $mustPay - abs($_trnas['money'] - $_trnas['usemoney']);
									
								} else {
									
									//$lastTotal[] = abs($_trnas['money'] - $_trnas['usemoney']);
									//$mustPay -= abs($_trnas['money'] - $_trnas['usemoney']);
									unset($usedAry);
									if($_trnas['used']) {
										$usedAry[count($_transAry)] = $_trnas['used'];
									}
									
									$usedAry[] = $web_x_order_ordernum.'-'.abs($mustPay);
									
									$sql = "Update web_bonus set usemoney = ".abs($_trnas['usemoney'] + $mustPay).", usedate = '".date('Y-m-d H:i:s')."', used = '".implode(';', $usedAry)."' Where web_bonus_id = '".$key2."' ";
									$rs = ConnectDB($DB, $sql);
									
									$mustPay -= $mustPay; 
								}
								
							}
							
							
						}
					
					}		
				}
			}
			/*
			echo "<pre>";
			print_r($lastTotal);
			echo "</pre>";
			echo array_sum($lastTotal)."</br>";
			
			echo "<pre>";
			print_r($_transAry);
			echo "</pre>";
			exit;
			*/
			/***********************************點數計算***********************************/
			
			$prodSubjectAry = array();
			foreach($productRow as $key =>$_productRow) {
				
				$dimension = ($_productRow['bweb_xx_product_id']) ? $_productRow['price_cost'] : null;
				$sql = "Insert into web_convert (web_x_order_ordernum, web_product_id, web_x_sale_id, ifGeneral, additional, fullAdditional, subject, serialnumber, pincode, dimension, price, num, discount, masterId, cdate) values ('$web_x_order_ordernum', '".$_productRow['web_product_id']."', '', '', '', '', '".$_productRow['subject']."', '', '', '".$dimension."', '".$_productRow['price_member']."', '1', '', '', '".$_POST['cdate']."') ";
				//echo $sql;
				$pdo = $pdoDB->prepare($sql);
				$pdo->execute();
				
				$prodSubjectAry[] = $_productRow['subject'];
			}		
			
			//扣庫存
			/*
			$sql = "Update web_product set stock = stock - ".$num." Where web_product_id = '".$web_product_id."' ";
			$rs = ConnectDB($DB, $sql);
			*/
			if($Init_Bonus_Push) {
				if($memberRow['lineID'] && 1) {
				
					if($memberRow['lineID'] && 1) {
						$sql = "Select subject From web_x_class Where web_x_class_id = '".$store_id."'";
						$pdo = $pdoDB->prepare($sql);
						$pdo->execute();
						$store_info = $pdo->fetch(PDO::FETCH_ASSOC);
						
						//$text = "客戶【".$memberRow['uname']."】使用".$productRow['csubject'].$productRow['subject'];
						$text = "您已成功於【".$store_info['subject']."】\n兌換:".implode("\n", $prodSubjectAry)."\n使用點數".number_format($_POST['total'])."點\n剩餘點數".number_format($bonus - $_POST['total'])."點";
							
						$_REQUEST['ID'] = $memberRow['lineID'];
						$_REQUEST['contnet'] = $text;
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL, "https://".$_SERVER['HTTP_HOST']."/api.php");
						curl_setopt($ch, CURLOPT_HEADER, false);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
						$PostData = $_REQUEST;
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $PostData);
						$temp = curl_exec($ch);
						// 關閉CURL連線
						curl_close($ch);
					}	
				}
			}	
			$req = array('error'=>'0', 'balance' => number_format(abs($balance - $_POST['total'])));
			
		}
		die(json_encode($req, JSON_UNESCAPED_UNICODE));
		
	}
	
	//點數兌換
	if($action == "bonusPay") {
		$web_x_order_id = intval($_POST["id"]);
		$status = trim($_POST["status"]);
		$web_member_id = intval($_POST["admin"]);
		$ip = $_SERVER["HTTP_VIA"] ? $_SERVER["HTTP_X_FORWARDED_FOR"] : $_SERVER["REMOTE_ADDR"];	//IP
		
		$sql = "
			SELECT 
				SQL_CALC_FOUND_ROWS a.*,
				b.web_member_id as bweb_member_id,
				b.uname as buname,
				b.birthday as bbirthday,
				b.mobile as bmobile,
				b.lineID as blineID,
				web_convert.subject as prodSubjects,
				web_convert.web_order_id as web_order_id,
				web_convert.web_x_order_ordernum as web_x_order_ordernum,
				web_convert.web_product_id as web_order_product_id,
				web_product.web_product_id as web_product_product_id,
				web_product.web_x_product_id as web_product_x_product_id,
				web_product.totalCount as web_product_totalCount,
				web_x_product.web_x_product_id as web_x_product_x_product_id,
				web_x_product.subject as web_x_product_subject,
				web_x_product.web_xx_product_id as web_x_product_xx_product_id,
				(select subject from web_x_class where web_x_class.web_x_class_id = a.store_id) as xClassSubject
			FROM 
				web_x_convert a
			Left Join 
				web_member b 
			ON 
				b.web_member_id = a.web_member_id
			Left Join
				web_convert as web_convert
			On
				web_convert.web_x_order_ordernum = a.ordernum
			Left Join
				web_product
			On
				web_product.web_product_id = web_convert.web_product_id
			Left Join
				web_x_product
			On
				web_x_product.web_x_product_id = web_product.web_x_product_id	
			where 
				a.web_x_order_id = :web_x_order_id
		";
		$excute = array(
			':web_x_order_id'        => $web_x_order_id,
		);
		//$debug = new Helper();
		//echo $test = $debug::debugPDO($sql, $excute);
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute($excute);
		$productRow = $pdo->fetch(PDO::FETCH_ASSOC);
		
		if($productRow['web_x_order_id']) {
			
			//$sql = "Select * From web_member Where web_member_id = :web_member_id ";
			$sql = "Select * From web_adminuser Where web_adminuser_id = :web_member_id ";
			$excute = array(
				':web_member_id'        => $web_member_id,
			);
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute($excute);
			$memberRow = $pdo->fetch(PDO::FETCH_ASSOC);
			
			if($status == 'pay') {
				$_POST['paymentstatus'] = '已兌換';
				$_POST['editUser'] = $memberRow['uname'];
				$_POST['editDateTime'] = date('Y-m-d H:i:s');
				
			} else if($status == 'cancel') {
				$_POST['states'] = '取消';
				$_POST['cancelUser'] = $memberRow['uname'];
				$_POST['cancelDateTime'] = date('Y-m-d H:i:s');
			}
			$_POST['date'] = date('Y-m-d H:i:s');
			$_POST['web_x_order_id'] = $productRow['web_x_order_id'];
			$_POST['shipdate'] = date('Y-m-d');
			
			if (1) {
				$TableName = "web_x_convert";
				$sql = "SHOW FULL FIELDS FROM `".$TableName."`";
				$rs = ConnectDB($DB, $sql);
				$tabeInfoAry = array();
				for ($i=0; $i<mysql_num_rows($rs); $i++) {
					$row = mysql_fetch_assoc($rs);
					$tabeInfoAry[$row[Field]] = $row[Comment];
				}

				$sql2 = "Select * From web_x_convert Where web_x_order_id like '".$_POST[web_x_order_id]."' ";
				$rs2 = ConnectDB($DB, $sql2);	
				$inDbInfo = array();
				for ($i=0; $i<mysql_num_rows($rs2); $i++) {
					$row2 = mysql_fetch_assoc($rs2);
					$inDbInfo = $row2;
				}
				$_inDbInfo = array();
				$postAry = array();
				foreach($_POST as $key => $postVal) {
					//echo $key.": ".$tabeInfoAry[$key].": ".$postVal."</br>";
					$_inDbInfo[$tabeInfoAry[$key]] = $inDbInfo[$key];
					$postAry[$tabeInfoAry[$key]] = $postVal;
				}
				//exit;
				$result = array_diff_assoc($postAry,$_inDbInfo);
				//查看是否有更改
				if(count($result)) {
					$recLogAry = array();
					$_recLogAry = array();
					foreach($result as $key => $postVal) {
						//echo '原資料:'.$_inDbInfo[$key]."</br>";
						//$_recLogAry[$key] = $_inDbInfo[$key]." -> ".$postVal;
						if(in_array($key, array('accept_time', 'invoicetype'))) {
							continue;
						}
						$recLogAry[urlencode($key)] = urlencode($_inDbInfo[$key])." -> ".urlencode($postVal);
					}
					$recLogAry[urlencode(操作日期)] = $_POST['date'];
					$recLogAry[urlencode(操作者)] = urlencode($memberRow['uname']);
					$recLogAry[IP] = $ip;
					$recLog = json_encode($recLogAry);

					if(count($recLogAry)) {
						//$sql3 = "Update web_x_order set log = '".$recLog."' Where web_x_order_id = '".$_POST[web_x_order_id]."'";
						$sql3 = "
							Insert into 
								web_convertlog 
									(
										ordernum, 
										log, 
										uname, 
										IP, 
										date
									) 
								values 
									(
										'".$inDbInfo[ordernum]."', 
										'$recLog', 
										'".$_POST['editUser']."', 
										'$ip', 
										'".$_POST['date']."'
									) 
						";	
						$rs3 = ConnectDB($DB, $sql3);
					}
				}
			}
			
			$sql = "
				Select 
					SUM(a.money) as sum,
					SUM(a.usemoney) as useSum
				From 
					web_bonus a
				Left Join 
					web_x_order b ON b.ordernum = a.web_x_order_from_ordernum
				Where 
					a.web_member_id = '".$productRow['bweb_member_id']."'
				AND
					a.money > a.usemoney
				AND
					a.sdate <= '".date('Y-m-d')."'
				AND
					a.edate >= '".date('Y-m-d')."'
				AND
					b.states = '訂單成立'
				AND
					b.paymentstatus = '付款成功'
				order by 
					a.web_bonus_id desc 
			";
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute();
			$bonusTotal = $pdo->fetch(PDO::FETCH_ASSOC);
			
			$_bonus = abs($bonusTotal['sum'] - $bonusTotal['useSum']);
			
			if($status == 'pay') {

				$sql4 = "UPDATE web_x_convert set paymentstatus = :paymentstatus, shipdate = :shipdate, editUser = :editUser, editDateTime = :editDateTime, date = :date Where web_x_order_id = :web_x_order_id ";
				$excute4 = array(
					':paymentstatus'        => '已兌換',
					':editUser'  			=> $_POST['editUser'],
					':date'       			=> $_POST['date'],
					':shipdate'				=> date('Y-m-d'),
					':editDateTime'			=> $_POST['editDateTime'],
					':web_x_order_id'    	=> $web_x_order_id,
				);
				//$debug = new Helper();
				//echo $test = $debug::debugPDO($sql4, $excute4);
				$pdo = $pdoDB->prepare($sql4);
				$pdo->execute($excute4);
				$logInfo['error'] = 0;
				
				//$text = "您已兌換成功\n ".$productRow['prodSubjects']." - ".$productRow['total']."點\n 開通人員 ".$_POST['editUser']."\n 開通時間 ".$_POST['editDateTime'];
				
				$text = "您已成功於【".$productRow['xClassSubject']."】\n兌換：".$productRow['prodSubjects']."\n使用點數：".number_format($productRow['total'])."點\n剩餘點數：".number_format($_bonus)."點\n開通人員：".$_POST['editUser']."\n開通時間：".$_POST['editDateTime'];
				
			} else if($status == 'cancel') {
				
				$sql4 = "UPDATE web_x_convert set states = :states, cancelUser = :cancelUser, cancelDateTime = :cancelDateTime, date = :date Where web_x_order_id = :web_x_order_id ";
				$excute4 = array(
					':states'        		=> '取消',
					':cancelUser'  			=> $_POST['cancelUser'],
					':date'       			=> $_POST['date'],
					//':shipdate'				=> date('Y-m-d'),
					':web_x_order_id'    	=> $web_x_order_id,
					':cancelDateTime'		=> $_POST['cancelDateTime'],
				);
				//$debug = new Helper();
				//echo $test = $debug::debugPDO($sql4, $excute4);
				$pdo = $pdoDB->prepare($sql4);
				$pdo->execute($excute4);
				
				/************************************點數復歸************************************/
				//echo $inDbInfo['ordernum'];
				$sql = "
					Select 
						a.web_bonus_id,
						a.used,
						a.usemoney
					From 
						web_bonus a
					Left Join 
						web_x_order b ON b.ordernum = a.web_x_order_from_ordernum
					Where 
						a.web_member_id = '".$productRow['bweb_member_id']."'
					AND
						b.states = '訂單成立'
					AND
						b.paymentstatus = '付款成功'
					order by 
						a.web_bonus_id desc 
				";
				$pdo = $pdoDB->prepare($sql);
				$pdo->execute();
				$bonusRow = $pdo->fetchAll(PDO::FETCH_ASSOC);
				
				$transList = array();
				$usemoneyList = array();
				foreach($bonusRow as $bonusKey => $bonus) {
					if(!$bonus['used']) {
						continue;
					}
					$transList[$bonus['web_bonus_id']] = explode(';', $bonus['used']);
					$usemoneyList[$bonus['web_bonus_id']] = $bonus['usemoney'];
				}
				
				foreach($transList as $transKey => $trans) {
	
					$matches = preg_grep('/'.$inDbInfo['ordernum'].'/i', $trans);
					if(count($matches)) {
						foreach($matches as $keys => $match) {
							$matchExplode = explode('-', $match);
							//echo $transKey."====".$keys."====".$matchExplode[0]."====".$matchExplode[1]."</br>";
							unset($transList[$transKey][$keys]);
							
							
							$sql = "Update web_bonus set usemoney = ".abs($usemoneyList[$transKey] - $matchExplode[1]).", used = '".implode(';', $transList[$transKey])."' Where web_bonus_id = '".$transKey."' ";
							$rs = ConnectDB($DB, $sql);
							
							
						}	
					}
		
				}
				/************************************點數復歸************************************/
				
				$logInfo['error'] = 0;
				
				//$text = "您已成功取消兌換\n ".$productRow['prodSubjects']." - ".$productRow['total']."點\n 取消人員 ".$_POST['cancelUser']."\n 取消時間 ".$_POST['cancelUser'];
				$text = "您已成功於【".$productRow['xClassSubject']."】\n取消兌換：".$productRow['prodSubjects']."\n剩餘點數：".number_format($_bonus+$productRow['total'])."點\n取消人員：".$_POST['cancelUser']."\n取消時間：".$_POST['cancelDateTime'];
			}
			if($Init_Bonus_Push) {
				if($productRow['blineID'] && 1) {
				
					if($productRow['blineID'] && 1) {
						
							
						$_REQUEST['ID'] = $productRow['blineID'];
						$_REQUEST['contnet'] = $text;
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL, "https://".$_SERVER['HTTP_HOST']."/api.php");
						curl_setopt($ch, CURLOPT_HEADER, false);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
						$PostData = $_REQUEST;
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $PostData);
						$temp = curl_exec($ch);
						// 關閉CURL連線
						curl_close($ch);
					}	
				}
			}

			
		} else {
			$logInfo = array('error'=>'1', 'message'=> '系統維護中');
		}
		
		
		die(json_encode($logInfo, JSON_UNESCAPED_UNICODE));
		
	}
	
	//兌換取消
	if($action == "cancelBonus") {
		
		$web_x_order_id = intval($_POST["web_x_order_id"]);
		$cancelReason = trim($_POST["cancelReason"]);
		$cancelRemark = trim($_POST["cancelRemark"]);
		$web_member_id = intval($_POST["admin"]);
		
		$ip = $_SERVER["HTTP_VIA"] ? $_SERVER["HTTP_X_FORWARDED_FOR"] : $_SERVER["REMOTE_ADDR"];	//IP
		
		//$sql = "Select * From web_x_order Where web_x_order_id = :web_x_order_id ";
		$sql = "
			SELECT 
				SQL_CALC_FOUND_ROWS a.*,
				b.web_member_id as bweb_member_id,
				b.uname as buname,
				b.birthday as bbirthday,
				b.mobile as bmobile,
				b.lineID as blineID,
				web_convert.subject as prodSubjects,
				web_convert.web_order_id as web_order_id,
				web_convert.web_x_order_ordernum as web_x_order_ordernum,
				web_convert.web_product_id as web_order_product_id,
				web_product.web_product_id as web_product_product_id,
				web_product.web_x_product_id as web_product_x_product_id,
				web_product.totalCount as web_product_totalCount,
				web_x_product.web_x_product_id as web_x_product_x_product_id,
				web_x_product.subject as web_x_product_subject,
				web_x_product.web_xx_product_id as web_x_product_xx_product_id
			FROM 
				web_x_convert a
			Left Join 
				web_member b 
			ON 
				b.web_member_id = a.web_member_id
			Left Join
				web_convert as web_convert
			On
				web_convert.web_x_order_ordernum = a.ordernum
			Left Join
				web_product
			On
				web_product.web_product_id = web_convert.web_product_id
			Left Join
				web_x_product
			On
				web_x_product.web_x_product_id = web_product.web_x_product_id	
			where 
				a.web_x_order_id = :web_x_order_id
		";
		$excute = array(
			':web_x_order_id'        => $web_x_order_id,
		);
		//$debug = new Helper();
		//echo $test = $debug::debugPDO($sql, $excute);
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute($excute);
		$productRow = $pdo->fetch(PDO::FETCH_ASSOC);
		
		if($productRow['web_x_order_id']) {
			
			
			$_POST['date'] = date('Y-m-d H:i:s');
			$_POST['web_x_order_id'] = $productRow['web_x_order_id'];
			$_POST['cancelReason'] = $cancelReason;
			$_POST['cancelRemark'] = $cancelRemark;
			$_POST['cancelDateTime'] = date('Y-m-d H:i:s');
			$_POST['states'] = '取消';
			
			
			
			//$sql = "Select * From web_member Where web_member_id = :web_member_id ";
			$sql = "Select * From web_adminuser Where web_adminuser_id = :web_member_id ";
			$excute = array(
				':web_member_id'        => $web_member_id,
			);
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute($excute);
			$memberRow = $pdo->fetch(PDO::FETCH_ASSOC);
			
			$_POST['cancelUser'] = $memberRow['uname'];
			
			if (1) {
				$TableName = "web_x_convert";
				$sql = "SHOW FULL FIELDS FROM `".$TableName."`";
				$rs = ConnectDB($DB, $sql);
				$tabeInfoAry = array();
				for ($i=0; $i<mysql_num_rows($rs); $i++) {
					$row = mysql_fetch_assoc($rs);
					$tabeInfoAry[$row[Field]] = $row[Comment];
				}

				$sql2 = "Select * From web_x_convert Where web_x_order_id like '".$_POST[web_x_order_id]."' ";
				$rs2 = ConnectDB($DB, $sql2);	
				$inDbInfo = array();
				for ($i=0; $i<mysql_num_rows($rs2); $i++) {
					$row2 = mysql_fetch_assoc($rs2);
					$inDbInfo = $row2;
				}
				$_inDbInfo = array();
				$postAry = array();
				foreach($_POST as $key => $postVal) {
					//echo $key.": ".$tabeInfoAry[$key].": ".$postVal."</br>";
					$_inDbInfo[$tabeInfoAry[$key]] = $inDbInfo[$key];
					$postAry[$tabeInfoAry[$key]] = $postVal;
				}
				//exit;
				$result = array_diff_assoc($postAry,$_inDbInfo);
				//查看是否有更改
				if(count($result)) {
					$recLogAry = array();
					$_recLogAry = array();
					foreach($result as $key => $postVal) {
						//echo '原資料:'.$_inDbInfo[$key]."</br>";
						//$_recLogAry[$key] = $_inDbInfo[$key]." -> ".$postVal;
						if(in_array($key, array('accept_time', 'invoicetype'))) {
							continue;
						}
						$recLogAry[urlencode($key)] = urlencode($_inDbInfo[$key])." -> ".urlencode($postVal);
					}
					$recLogAry[urlencode(操作日期)] = $_POST['date'];
					$recLogAry[urlencode(操作者)] = urlencode($memberRow['uname']);
					$recLogAry[IP] = $ip;
					$recLog = json_encode($recLogAry);

					if(count($recLogAry)) {
						//$sql3 = "Update web_x_order set log = '".$recLog."' Where web_x_order_id = '".$_POST[web_x_order_id]."'";
						$sql3 = "
							Insert into 
								web_convertlog 
									(
										ordernum, 
										log, 
										uname, 
										IP, 
										date
									) 
								values 
									(
										'".$inDbInfo[ordernum]."', 
										'$recLog', 
										'".$_POST['editUser']."', 
										'$ip', 
										'".$_POST['date']."'
									) 
						";	
						$rs3 = ConnectDB($DB, $sql3);
					}
				}
			}
			
			$sql4 = "UPDATE web_x_convert set states = :states, cancelUser = :cancelUser, cancelDateTime = :cancelDateTime, date = :date, cancelReason = :cancelReason, cancelRemark = :cancelRemark Where web_x_order_id = :web_x_order_id ";
			$excute4 = array(
				':states'        		=> $_POST['states'],
				':cancelUser'  			=> $_POST['cancelUser'],
				':date'       			=> $_POST['date'],
				':cancelDateTime'		=> $_POST['cancelDateTime'],
				':cancelReason'    		=> $_POST['cancelReason'],
				':cancelRemark'    		=> $_POST['cancelRemark'],
				':web_x_order_id'    	=> $web_x_order_id,
			);
			//$debug = new Helper();
			//echo $test = $debug::debugPDO($sql4, $excute4);
			$pdo = $pdoDB->prepare($sql4);
			$pdo->execute($excute4);
			
			/************************************點數復歸************************************/
			//echo $inDbInfo['ordernum'];
			$sql = "
				Select 
					a.web_bonus_id,
					a.used,
					a.usemoney
				From 
					web_bonus a
				Left Join 
					web_x_order b ON b.ordernum = a.web_x_order_from_ordernum
				Where 
					a.web_member_id = '".$productRow['bweb_member_id']."'
				AND
					b.states = '訂單成立'
				AND
					b.paymentstatus = '付款成功'
				order by 
					a.web_bonus_id desc 
			";
			//exit;
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute();
			$bonusRow = $pdo->fetchAll(PDO::FETCH_ASSOC);
			
			$transList = array();
			$usemoneyList = array();
			foreach($bonusRow as $bonusKey => $bonus) {
				if(!$bonus['used']) {
					continue;
				}
				$transList[$bonus['web_bonus_id']] = explode(';', $bonus['used']);
				$usemoneyList[$bonus['web_bonus_id']] = $bonus['usemoney'];
			}
			
			foreach($transList as $transKey => $trans) {

				$matches = preg_grep('/'.$inDbInfo['ordernum'].'/i', $trans);
				if(count($matches)) {
					foreach($matches as $keys => $match) {
						$matchExplode = explode('-', $match);
						//echo $transKey."====".$keys."====".$matchExplode[0]."====".$matchExplode[1]."</br>";
						unset($transList[$transKey][$keys]);
						
						
						$sql = "Update web_bonus set usemoney = ".abs($usemoneyList[$transKey] - $matchExplode[1]).", used = '".implode(';', $transList[$transKey])."' Where web_bonus_id = '".$transKey."' ";
						$rs = ConnectDB($DB, $sql);
						
						
					}	
				}
	
			}
			/************************************點數復歸************************************/
			
			if($Init_Bonus_Push) {
				if($productRow['blineID'] && 1) {
				
					if($productRow['blineID'] && 1) {
						
						//$text = "客戶【".$productRow['buname']."】取消".$productRow['web_x_product_subject'].$productRow['prodSubjects']."\r\n 取消原因 ".$_POST['cancelReason']." ".$_POST['cancelRemark']."\r\n 取消人員 ".$_POST['cancelUser']."\r\n 取消時間 ".$_POST['cancelDateTime'];
						
						$text = "您已成功取消兌換".$productRow['prodSubjects']."如有需要協助的地方，請洽店員";
							
						$_REQUEST['ID'] = $productRow['blineID'];
						$_REQUEST['contnet'] = $text;
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL, "https://".$_SERVER['HTTP_HOST']."/api.php");
						curl_setopt($ch, CURLOPT_HEADER, false);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
						$PostData = $_REQUEST;
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $PostData);
						$temp = curl_exec($ch);
						// 關閉CURL連線
						curl_close($ch);
					}	
				}
			}
			
			$logInfo['error'] = 0;
			

			
		} else {
			$logInfo = array('error'=>'1', 'message'=> '系統維護中');
		}
		
		
		die(json_encode($logInfo, JSON_UNESCAPED_UNICODE));
		
	}
	
	//領取生日禮金
	if($action == "birthdayMoney") {
	
		$web_member_id = intval($_POST["web_member_id"]);
		$store_id = intval($_POST["store_id"]);
		$_POST['sdate'] = date('Y-m-01');
		$_POST['edate'] = date('Y-m-t');

		$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM web_member where web_member.web_member_id = :web_member_id";
		$excute = array(
			':web_member_id'       	=> $web_member_id,
		);
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute($excute);
		$memberRow = $pdo->fetch(PDO::FETCH_ASSOC);
		
		$_POST['web_member_id'] = $memberRow['web_member_id'];
		$_POST['order_name'] = $memberRow['uname'];
		$_POST['order_mobile'] = $memberRow['mobile'];
		$_POST['accept_name'] = $memberRow['uname'];
		$_POST['accept_mobile'] = $memberRow['mobile'];
		
		
		$sql = "SELECT web_member_money_id FROM web_member_money where web_member_money.web_member_id = :web_member_id AND web_member_money.kind != :kind AND web_member_money.sdate = '".$_POST['sdate']."' AND web_member_money.edate = '".$_POST['edate']."'";
		$excute = array(
			':kind'       			=> 2,
			':web_member_id'       	=> $memberRow['web_member_id'],
		);
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute($excute);
		$productRow = $pdo->fetch(PDO::FETCH_ASSOC);
		if($productRow['web_member_money_id']) {
			$req = array('error'=>'1', 'message'=> '已領取');
			
		} else {
			//寫入兑換生日禮金記錄
			$sql = "Insert into web_member_money (web_x_member_money_id, kind, store_id, web_member_id, sdate, edate, srange, money, cdate) values ('1', '0', '".$store_id."', '".$_POST['web_member_id']."', '".$_POST['sdate']."', '".$_POST['edate']."', '0', '$Init_Birthday_Money', '".date('Y-m-d H:i:s')."') ";
			$rs = ConnectDB($DB, $sql);
			
			$sql = "SELECT GROUP_CONCAT(DISTINCT lineID SEPARATOR ',') AS lineIDAry FROM web_member WHERE lineID != '' AND (ifService IN(1) AND ifMessage IN(1) AND store_id = '".$_POST['store_id']."') OR (store_id = '-1' AND ifService IN(1) AND ifMessage IN(1))";
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute();
			$lineIDAry = $pdo->fetch(PDO::FETCH_ASSOC);
			if($Init_Stored_Push) {
				if(count($lineIDAry) && 1) {
					
					$sql = "Select subject From web_x_class Where web_x_class_id = '".$store_id."'";
					$pdo = $pdoDB->prepare($sql);
					$pdo->execute();
					$store_info = $pdo->fetch(PDO::FETCH_ASSOC);
					
					$text = "客戶【".$memberRow['uname']."】於 ".$store_info['subject']." 兌換生日禮金 ".$Init_Birthday_Money."元";
						
					$_REQUEST['ID'] = $lineIDAry['lineIDAry'];
					$_REQUEST['contnet'] = $text;
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, "https://".$_SERVER['HTTP_HOST']."/api.php");
					curl_setopt($ch, CURLOPT_HEADER, false);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
					$PostData = $_REQUEST;
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $PostData);
					$temp = curl_exec($ch);
					// 關閉CURL連線
					curl_close($ch);
				}	
			}	
			
			$req = array('error'=>'0');
			
		}
		die(json_encode($req, JSON_UNESCAPED_UNICODE));
		
	}
	
	//生日禮金兌換
	if($action == "birthdayMoneyPay") {
		
		$web_member_money_id = intval($_POST["id"]);
		$status = trim($_POST["status"]);
		$web_member_id = intval($_POST["admin"]);
		$ip = $_SERVER["HTTP_VIA"] ? $_SERVER["HTTP_X_FORWARDED_FOR"] : $_SERVER["REMOTE_ADDR"];	//IP
		
		$sql = "
			SELECT 
				SQL_CALC_FOUND_ROWS a.*,
				b.web_member_id as bweb_member_id,
				b.uname as buname,
				b.birthday as bbirthday,
				b.mobile as bmobile,
				b.lineID as blineID,
				(select subject from web_x_class where web_x_class.web_x_class_id = a.store_id) as xClassSubject
			FROM 
				web_member_money a
			Left Join 
				web_member b 
			ON 
				b.web_member_id = a.web_member_id	
			where 
				a.web_member_money_id = :web_member_money_id
		";
		$excute = array(
			':web_member_money_id'        => $web_member_money_id,
		);
		//$debug = new Helper();
		//echo $test = $debug::debugPDO($sql, $excute);
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute($excute);
		$productRow = $pdo->fetch(PDO::FETCH_ASSOC);

		if($productRow['web_member_money_id']) {
			
			//$sql = "Select * From web_member Where web_member_id = :web_member_id ";
			$sql = "Select * From web_adminuser Where web_adminuser_id = :web_member_id ";
			$excute = array(
				':web_member_id'        => $web_member_id,
			);
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute($excute);
			$memberRow = $pdo->fetch(PDO::FETCH_ASSOC);
			
			if($status == 'pay') {
				$_POST['paymentstatus'] = '已兌換';
				$_POST['editUser'] = $memberRow['uname'];
				$_POST['editDateTime'] = date('Y-m-d H:i:s');
				
			} else if($status == 'cancel') {
				$_POST['states'] = '取消';
				$_POST['cancelUser'] = $memberRow['uname'];
				$_POST['cancelDateTime'] = date('Y-m-d H:i:s');
			}
			$_POST['date'] = date('Y-m-d H:i:s');
			$_POST['web_member_money_id'] = $productRow['web_member_money_id'];
			$_POST['shipdate'] = date('Y-m-d');
			
			if (0) {
				$TableName = "web_member_money";
				$sql = "SHOW FULL FIELDS FROM `".$TableName."`";
				$rs = ConnectDB($DB, $sql);
				$tabeInfoAry = array();
				for ($i=0; $i<mysql_num_rows($rs); $i++) {
					$row = mysql_fetch_assoc($rs);
					$tabeInfoAry[$row[Field]] = $row[Comment];
				}

				$sql2 = "Select * From web_member_money Where web_member_money_id like '".$_POST['web_member_money_id']."' ";
				$rs2 = ConnectDB($DB, $sql2);	
				$inDbInfo = array();
				for ($i=0; $i<mysql_num_rows($rs2); $i++) {
					$row2 = mysql_fetch_assoc($rs2);
					$inDbInfo = $row2;
				}
				$_inDbInfo = array();
				$postAry = array();
				foreach($_POST as $key => $postVal) {
					//echo $key.": ".$tabeInfoAry[$key].": ".$postVal."</br>";
					$_inDbInfo[$tabeInfoAry[$key]] = $inDbInfo[$key];
					$postAry[$tabeInfoAry[$key]] = $postVal;
				}
				//exit;
				$result = array_diff_assoc($postAry,$_inDbInfo);
				//查看是否有更改
				if(count($result)) {
					$recLogAry = array();
					$_recLogAry = array();
					foreach($result as $key => $postVal) {
						//echo '原資料:'.$_inDbInfo[$key]."</br>";
						//$_recLogAry[$key] = $_inDbInfo[$key]." -> ".$postVal;
						if(in_array($key, array('accept_time', 'invoicetype'))) {
							continue;
						}
						$recLogAry[urlencode($key)] = urlencode($_inDbInfo[$key])." -> ".urlencode($postVal);
					}
					$recLogAry[urlencode(操作日期)] = $_POST['date'];
					$recLogAry[urlencode(操作者)] = urlencode($memberRow['uname']);
					$recLogAry[IP] = $ip;
					$recLog = json_encode($recLogAry);

					if(count($recLogAry)) {
						//$sql3 = "Update web_x_order set log = '".$recLog."' Where web_x_order_id = '".$_POST[web_x_order_id]."'";
						$sql3 = "
							Insert into 
								web_convertlog 
									(
										ordernum, 
										log, 
										uname, 
										IP, 
										date
									) 
								values 
									(
										'".$inDbInfo['ordernum']."', 
										'$recLog', 
										'".$_POST['editUser']."', 
										'$ip', 
										'".$_POST['date']."'
									) 
						";	
						$rs3 = ConnectDB($DB, $sql3);
					}
				}
			}
			
			
			if($status == 'pay') {

				$sql4 = "UPDATE web_member_money set kind = :kind, editUser = :editUser, editDateTime = :editDateTime Where web_member_money_id = :web_member_money_id ";
				$excute4 = array(
					':kind'        			=> '1',
					':editUser'  			=> $_POST['editUser'],
					':editDateTime'			=> $_POST['editDateTime'],
					':web_member_money_id'  => $web_member_money_id,
				);
				//$debug = new Helper();
				//$test = $debug::debugPDO($sql4, $excute4);
				$pdo = $pdoDB->prepare($sql4);
				$pdo->execute($excute4);
				$logInfo['error'] = 0;
				
				//$text = "您已兌換成功\n ".$productRow['prodSubjects']." - ".$productRow['total']."點\n 開通人員 ".$_POST['editUser']."\n 開通時間 ".$_POST['editDateTime'];
				
				$text = "您已成功於【".$productRow['xClassSubject']."】\n兌換：生日禮金".$productRow['money']."\n開通人員：".$_POST['editUser']."\n開通時間：".$_POST['editDateTime'];
				
			} else if($status == 'cancel') {
				
				$sql4 = "UPDATE web_member_money set kind = :kind, cancelUser = :cancelUser, cancelDateTime = :cancelDateTime Where web_member_money_id = :web_member_money_id ";
				$excute4 = array(
					':kind'        			=> '2',
					':cancelUser'  			=> $_POST['cancelUser'],
					':web_member_money_id'  => $web_member_money_id,
					':cancelDateTime'		=> $_POST['cancelDateTime'],
				);
				//$debug = new Helper();
				//echo $test = $debug::debugPDO($sql4, $excute4);
				$pdo = $pdoDB->prepare($sql4);
				$pdo->execute($excute4);
				
				$logInfo['error'] = 0;
				
				//$text = "您已成功取消兌換\n ".$productRow['prodSubjects']." - ".$productRow['total']."點\n 取消人員 ".$_POST['cancelUser']."\n 取消時間 ".$_POST['cancelUser'];
				$text = "您已成功於【".$productRow['xClassSubject']."】\n取消兌換：生日禮金".$productRow['money']."\n取消人員：".$_POST['cancelUser']."\n取消時間：".$_POST['cancelDateTime'];
			}
			if($Init_Bonus_Push) {
				if($productRow['blineID'] && 1) {
				
					if($productRow['blineID'] && 1) {
						
							
						$_REQUEST['ID'] = $productRow['blineID'];
						$_REQUEST['contnet'] = $text;
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL, "https://".$_SERVER['HTTP_HOST']."/api.php");
						curl_setopt($ch, CURLOPT_HEADER, false);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
						$PostData = $_REQUEST;
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $PostData);
						$temp = curl_exec($ch);
						// 關閉CURL連線
						curl_close($ch);
					}	
				}
			}

			
		} else {
			$logInfo = array('error'=>'1', 'message'=> '系統維護中');
		}
		
		
		die(json_encode($logInfo, JSON_UNESCAPED_UNICODE));
		
	}
	
	if($action == "editMemberTag") {
		$tags = trim($_POST["tags"]);
		$web_member_id = intval($_POST["memberId"]);
		$ip = $_SERVER["HTTP_VIA"] ? $_SERVER["HTTP_X_FORWARDED_FOR"] : $_SERVER["REMOTE_ADDR"];	//IP
		
		if($web_member_id) {
			$sql4 = "UPDATE web_member set tags = :tags Where web_member_id = :web_member_id ";
			$excute4 = array(
				':tags'  				=> $tags,
				':web_member_id'  		=> $web_member_id,
			);
			//$debug = new Helper();
			//echo $test = $debug::debugPDO($sql4, $excute4);
			$pdo = $pdoDB->prepare($sql4);
			$pdo->execute($excute4);
			
			$logInfo['error'] = 0;
		} else {	
		
			$logInfo = array('error'=>'1', 'message'=> '系統維護中');
		}	
		
		die(json_encode($logInfo, JSON_UNESCAPED_UNICODE));
		
	}
	
	if($action == "firebaseToken") {
		$user_token = ($_POST["user_token"]);
		$ip = $_SERVER["HTTP_VIA"] ? $_SERVER["HTTP_X_FORWARDED_FOR"] : $_SERVER["REMOTE_ADDR"];	//IP
		$sql = "Select * From web_firebasetoken Where token = :user_token ";
		$excute = array(
			':user_token'        => $user_token,
		);
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute($excute);
		$row = $pdo->fetch(PDO::FETCH_ASSOC);
		//$rs = ConnectDB($DB, $sql);
		//if(mysql_num_rows($rs)) {
		if(!$row) {	
			$sql = "Insert into web_firebasetoken (token, ip, date) values ('".$user_token."', '".$ip."', '".date('Y-m-d H:i:s')."') ";
			//echo $sql;
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute();
			$req = array('d'=>'新增成功');
		}	
		die(json_encode($req, JSON_UNESCAPED_UNICODE));
		
	}