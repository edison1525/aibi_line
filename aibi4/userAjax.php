<?php
	include_once("./control/includes/function.php");
	
	//$chatRow = file_get_contents("./chat/chat_".date('Ymd', strtotime('-1 day')).".txt", FILE_USE_INCLUDE_PATH);
	$chatRow = '';
	for($i=7; $i>=0; $i--) {
		$chatRow .= file_get_contents("./chat/chat_".date('Ymd', strtotime('-'.$i.' day')).".txt", FILE_USE_INCLUDE_PATH);
	}
	//echo 1111;
	$chatAry = explode("============Message=========\r\n", $chatRow);
	
	$userId = $_POST[userId];
	$store = ($_POST['store']) ? $_POST['store'] : 1;
	if(!$userId) {
		return;
	}
	
	$chatAry = array_filter($chatAry);
	$chatAry2 = $chatAry;
	
	foreach ($chatAry as $key => $val) {
		$decodeVal = json_decode($val, true);
	    $volume[$key]  = $decodeVal[events][0]['timestamp'];
	}
	array_multisort($volume, SORT_DESC, $chatAry);
	
	$userAry = array();
	foreach($chatAry as $key => $val) {
		$decodeVal = json_decode($val, true);
		$storeId = ($decodeVal['events'][0]['message']['store']) ? $decodeVal['events'][0]['message']['store'] : 1;
		if($storeId != $store) {
			continue;
		}
		$userAry[$storeId][$decodeVal[events][0][source][userId]][] = $decodeVal;
	}

	$newMsgAry = array();
	foreach($chatAry2 as $key => $val) {
		$decodeVal = json_decode($val, true);
		$storeId = ($decodeVal['events'][0]['message']['store']) ? $decodeVal['events'][0]['message']['store'] : 1;
		if($storeId != $store) {
			continue;
		}
		$newMsgAry[$storeId][$decodeVal[events][0][source][userId]][] = $decodeVal;
	}
	$style = null;
	$online = "none";
	foreach($userAry[$store] as $key2 => $userInfo) {
		if($key2 == $userId) {
			$style = "-moz-animation: bg2 1.5s infinite; -webkit-animation: bg2 1.5s infinite;";
			$online = null;
		} else {
			$style = null;
			$online = "none";
		}
?>			
			<li class="clearfix user" data-id="<?php echo $key2; ?>" style="cursor:pointer;">
				<img src="<?php echo $userInfo[0][pictureUrl]; ?>" alt="<?php echo $userInfo[0][displayName]; ?>" style="border-radius:50%;" width="55" >
				<div class="about">
					<div class="name" style="<?php echo $style; ?>"><?php echo $userInfo[0][displayName]; ?></div>
					<div class="status" style="display:<?php echo $online; ?>">
						<i class="fa fa-circle online"></i> online
					</div>
				</div>
			</li>
<?php
		$firstId[] = $key2;
	}
	//echo reset($firstId);
?>