<?php
    include_once("./control/includes/function.php");
	ini_set('session.cookie_httponly', 1 );
	session_start();
	if (empty($_SESSION['expire']) || $_SESSION['expire'] < time()) $_SESSION['token'] = md5('aibi' . uniqid(microtime())); // create token (fast/sufficient) 
	$_SESSION['expire'] = time() + 900; // make session valid for next 15 mins
	$_SESSION['visitid'] = $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT'];
	
	$agent = $_SERVER['HTTP_USER_AGENT'];
	if (!preg_match("#\bLine\b#", $agent)) {
		//die('aibi');
	}
	$web_member_id = $_REQUEST['web_member_id'];
	if(!$web_member_id) {
		die('aibi');
	}
	
	$sql = "
		Select 
			SUM(a.money) as sum,
			SUM(a.usemoney) as sumUse
		From 
			web_bonus a
		Left Join 
			web_x_order b ON b.ordernum = a.web_x_order_from_ordernum
		Where 
			a.web_member_id = '".$web_member_id."'
		AND
			a.sdate <= '".date('Y-m-d')."'
		AND
			a.edate >= '".date('Y-m-d')."'
		AND
			b.states = '訂單成立'
		AND
			b.paymentstatus = '付款成功'
		order by 
			a.web_bonus_id desc 
	";
	$pdo = $pdoDB->prepare($sql);
	$pdo->execute();
	$bonusTotal = $pdo->fetch(PDO::FETCH_ASSOC);
	/*
	$sql = "
		Select 
			SUM(a.subtotal) as sum
		From 
			web_x_convert a
		Where 
			a.web_member_id = '".$web_member_id."'
		AND
			a.states = '兌換成立'
		order by 
			a.web_x_order_id desc 
	";
	$pdo = $pdoDB->prepare($sql);
	$pdo->execute();
	$useBonusTotal = $pdo->fetch(PDO::FETCH_ASSOC);
	
	$bonus = abs($bonusTotal['sum'] - $useBonusTotal['sum']);
	*/
	
	$bonus = abs($bonusTotal['sum'] - $bonusTotal['sumUse']);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $Init_WebTitle; ?> 會員專區-點數</title>
	<script src="./js/jquery-1.10.2.min.js"></script>
    <link rel="stylesheet" href="css/app.css"/>
</head>
<body>
<div class="header">
    <h1>會員專區-點數</h1> <a class="back" href="member.php"></a>
</div>
<div class="content stored">
    <div class="info">點數 <?php echo number_format($bonus); ?>點</div>
    <div class="title">點數紀錄</div>
<?php
	$sql = "
		Select 
			a.*,
			a.money as amoney,
			b.*,
			c.*,
			(select subject from web_x_class where web_x_class.web_x_class_id = b.store_id) as xClassSubject
		From 
			web_bonus a
		Left Join 
			web_x_order b ON b.ordernum = a.web_x_order_from_ordernum
		Left Join 
			web_x_bonus c ON c.web_x_bonus_id = a.web_x_bonus_id	
		Where 
			a.web_member_id = '".$web_member_id."'
		AND
			b.states = '訂單成立'
		AND
			b.paymentstatus = '付款成功'
		order by 
			a.web_bonus_id desc 
	";
	$pdo = $pdoDB->prepare($sql);
	$pdo->execute();
	$bonusRow = $pdo->fetchAll(PDO::FETCH_ASSOC);
	foreach($bonusRow as $key => $bonus) {
?>	
    <div class="row">
        <div class="col-1">
			<?php echo date('Y/m/d', strtotime($bonus['cdate'])); ?>
			</br>
			<?php
				$sql = "
					Select 
						a.web_order_id,
						a.subject,
						a.price,
						a.web_product_id,
						c.subject as csubject,
						c.web_x_product_id
					From 
						web_order a
					Left Join
						web_product b
					On
						b.web_product_id = a.web_product_id
					Left Join
						web_x_product c
					On
						c.web_x_product_id = b.web_x_product_id	
					Where 
						a.web_x_order_ordernum = :web_x_order_ordernum
				";
				$excute = array(
					':web_x_order_ordernum'        => $bonus['ordernum'],
				);
				$pdo = $pdoDB->prepare($sql);
				$pdo->execute($excute);
				$_orderRow = $pdo->fetchAll(PDO::FETCH_ASSOC);
				$prodSubjectAry = array();
				foreach($_orderRow as $_key => $_orderVal) {
					$prodSubjectAry[] = ($_orderVal['web_x_product_id'] == '1') ? substr($_orderVal['csubject'], 0, 6)."-".$_orderVal['subject']." ".$_orderVal['price'] : substr($_orderVal['csubject'], 0, 6)."-".$_orderVal['subject'];
				}
				echo implode('</br>', $prodSubjectAry);
			?>
		</div>
        <div class="col-2">
            <span>
				獲得<?php echo $bonus['amoney']; ?>點
			</span>
        </div>
        <div class="col-3">
            <span><?php echo $bonus['xClassSubject']; ?></span>
            <span><?php echo $bonus['editUser']; ?></span>
        </div>
		<div>
			<?php echo "</br>使用期限：".$bonus['sdate']."~".$bonus['edate']; ?>
		</div>	
    </div>
<?php
		if($bonus['used']) {
			$bonusAry = explode(';', $bonus['used']);
			foreach(explode(';', $bonus['used']) as $key2 => $used) {
			
				$useRow = explode('-', $used);
				$sql = "
					Select 
						a.web_x_order_id,
						a.total,
						a.cdate,
						a.editUser,
						b.subject as bsubject,
						(select subject from web_x_class where web_x_class.web_x_class_id = a.store_id) as xClassSubject
					From 
						web_x_convert a
					Left Join
						web_convert b
					on
						b.web_x_order_ordernum = a.ordernum
					Where 
						a.ordernum = :ordernum
				";
				$excute = array(
					':ordernum'        => $useRow[0],
				);
				//$debug = new Helper();
				//echo $test = $debug::debugPDO($sql, $excute);
				$pdo = $pdoDB->prepare($sql);
				$pdo->execute($excute);
				$_orderRow = $pdo->fetch(PDO::FETCH_ASSOC);
				
?>	
	<div class="row">
        <div class="col-1">
			<?php echo date('Y/m/d', strtotime($_orderRow['cdate'])); ?>
			</br>
			<?php
				echo $useRow[0]."</br>".$_orderRow['bsubject'];
			?>
		</div>
        <div class="col-2">
            <span>
				兌換<?php echo $useRow[1]; ?>點
			</span>
        </div>
        <div class="col-3">
            <span><?php echo $_orderRow['xClassSubject']; ?></span>
            <span><?php echo $_orderRow['editUser']; ?></span>
        </div>
    </div>
	
<?php
			}
		}
	}
?>	
    <button class="submit" onclick="location.href='bonus-2.php?web_member_id=<?php echo $web_member_id; ?>'">兌換</button>
</div>
</body>
</html>
