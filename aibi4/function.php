<?php

	//確認是否有註冊
	function IdCheck($from) {

		global $DB, $pdoDB;

		if(!$from) {
			return;
		}

		$sql = "SELECT loginID, web_member_id, uname, sex, mobile, lineID, ifService FROM web_member WHERE lineID like '".$from."'";

		$pdo = $pdoDB->prepare($sql);
	    $pdo->execute();
	    $row = $pdo->fetch(PDO::FETCH_ASSOC);	

		$req = (!$row[loginID] || !$row[uname] || !$row[sex] || !$row[mobile]) ? 0 : 1;

		$req = (!$row['ifService']) ? $req : 2;

		return 	$req;
	}

	function readCacheLog($filePath) {
		if(file_exists($filePath)) {
			$cacheLog = file_get_contents($filePath);
			$cacheAry = explode('#$@%^', $cacheLog);

			foreach($cacheAry as $key => $cache) {
				if(empty($cache)) {
					continue;
				}
				$unCacheAry = unserialize($cache);
				
				foreach($unCacheAry as $key2 => $unCache) {
					//過30分略過
					if($unCache[cdate]+1800 <= time()) {
						continue;
					}	
					$req[] = $unCache;
				}
			}
			
			return end($req);
		}
	}
	
	function readCacheLogAll($filePath) {
		if(file_exists($filePath)) {
			$cacheLog = file_get_contents($filePath);
			$cacheAry = explode('#$@%^', $cacheLog);

			foreach($cacheAry as $key => $cache) {
				if(empty($cache)) {
					continue;
				}
				$unCacheAry = unserialize($cache);
				
				foreach($unCacheAry as $key2 => $unCache) {
	
					$req[] = $unCache;
				}
			}
			
			return ($req);
		}
	}
	
	function readCacheLogById($filePath, $id) {
		if(file_exists($filePath)) {
			$cacheLog = file_get_contents($filePath);
			$cacheAry = explode('#$@%^', $cacheLog);

			foreach($cacheAry as $key => $cache) {
				if(empty($cache)) {
					continue;
				}
				$unCacheAry = unserialize($cache);
				
				foreach($unCacheAry as $key2 => $unCache) {
					
					if($unCache[from] !== $id) {
						continue;
					}	
					$req[] = $unCache;
				}
			}
			
			return ($req);
		}
	}
	
	function arrayRecursiveDiff($aArray1, $aArray2) {
		$aReturn = array();
		foreach ($aArray1 as $mKey => $mValue) {
			foreach($aArray2 as $mKey2 => $mValue2) {
				
				$t = array_diff($mValue, $mValue2);
				//只有cdate時為已存在
				if(count($t) == 1) {
					if($t[cdate])
						$aReturn[$mKey] = $mValue;
				}	
			}	
		}
		return $aReturn;
	}
	
	function arrayRecursiveInNotice($aArray1, $aArray2) {
		$aReturn = array();
		foreach ($aArray1 as $mKey => $mValue) {
			foreach($aArray2 as $mKey2 => $mValue2) {
				
				$t = array_intersect($mValue, $mValue2);
				
				if(count($t) == 10) {
					$aReturn[$mKey] = $mValue;
				}	
			}	
		}
		return $aReturn;
	}

	function multicast($content_type, $message) {
	 
	 	global $header, $from, $receive, $channel_access_token;
	 	
		$url = "https://api.line.me/v2/bot/message/multicast";
		
		$data = ["to" => $from, "messages" => $message];
		
		
		
		$context = stream_context_create(array(
			"http" => array("method" => "POST", "header" => implode(PHP_EOL, $header), "content" => json_encode($data), "ignore_errors" => true)
		));
		
		//file_get_contents($url, false, $context);
		//curl設定
		$headers = array(
			"Content-Type: application/json; charser=UTF-8",
			"Authorization: Bearer {" . $channel_access_token . "}"
		);
			 
		//curl
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		//如果失敗可以印出 $result 看錯誤訊息
		$result = curl_exec($ch);
		//file_put_contents("debug.txt", json_encode($result)."\r\n",FILE_APPEND);
		curl_close($ch);
	}
	
	function push($content_type, $message) {
	 
	 	global $header, $from, $receive, $channel_access_token;
	 	
		$url = "https://api.line.me/v2/bot/message/push";
		
		$data = ["to" => $from, "messages" => $message];
		
		
		$context = stream_context_create(array(
			"http" => array("method" => "POST", "header" => implode(PHP_EOL, $header), "content" => json_encode($data), "ignore_errors" => true)
		));
		
		//file_get_contents($url, false, $context);
		//curl設定
		$headers = array(
			"Content-Type: application/json; charser=UTF-8",
			"Authorization: Bearer {" . $channel_access_token . "}"
		);
			 
		//curl
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		
		//file_put_contents("debug.txt", json_encode($data)."\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", "==========================\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($headers)."\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", "==========================\r\n",FILE_APPEND);

		//如果失敗可以印出 $result 看錯誤訊息
		$result = curl_exec($ch);
		//file_put_contents("debug.txt", json_encode($result)."\r\n",FILE_APPEND);
		curl_close($ch);
	}
	
	function reply($content_type, $message) {
	 
	 	global $header, $reToken, $receive, $channel_access_token;
	 	
		$url = "https://api.line.me/v2/bot/message/reply";
		
		$data = ["replyToken" => $reToken, "messages" => $message];
		
		$context = stream_context_create(array(
		"http" => array("method" => "POST", "header" => implode(PHP_EOL, $header), "content" => json_encode($data), "ignore_errors" => true)
		));
		//file_put_contents("debug.txt", $url."\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($data)."\r\n",FILE_APPEND);
		//$log = file_get_contents($url, false, $context);
		//file_put_contents("debug.txt", json_encode($log)."\r\n",FILE_APPEND);
		
		//curl設定
		$headers = array(
			"Content-Type: application/json; charser=UTF-8",
			"Authorization: Bearer {" . $channel_access_token . "}"
		);
			 
		//curl
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		//如果失敗可以印出 $result 看錯誤訊息
		$result = curl_exec($ch);
		//file_put_contents("debug.txt", "============".date('Y-m-d H:i:s')."=========\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($result)."\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", "============Data=========\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($data)."\r\n",FILE_APPEND);
		
		curl_close($ch);
		
		return $result;
	}	
	
	function push2($content_type, $message) {
	 
	 	global $header, $from, $receive, $channel_access_token;
	 	
		$url = "https://api.line.me/v2/bot/message/push";
		
		$data = ["to" => $from, "messages" => $message];

		
		$context = stream_context_create(array(
			"http" => array(
				"method" => "POST", 
				"header" => implode(PHP_EOL, $header), 
				"content" => json_encode($data), 
				"ignore_errors" => true
			)
		));
		
		//file_get_contents($url, false, $context);
		//curl設定
		$headers = array(
			"Content-Type: application/json; charser=UTF-8",
			"Authorization: Bearer {" . $channel_access_token . "}"
		);
			 
		//curl
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		//如果失敗可以印出 $result 看錯誤訊息
		$result = curl_exec($ch);
		//file_put_contents("debug.txt", json_encode($result)."\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", date('Y-m-d H:i:s')."\r\n",FILE_APPEND);
		curl_close($ch);
	}
	
	//預約課程分類
	function reservationCategory($content_type, $message, $page) {

	 	global $header, $reToken, $receive, $channel_access_token, $DB, $pdoDB, $from;
	 	
		$url = "https://api.line.me/v2/bot/message/reply";
		
		$data = ["replyToken" => $reToken, "messages" => array(["type" => "text", "text" => $message])];
				
		$sql = "SELECT COUNT(*) as Rows FROM web_x_class where web_x_class.ifShow = '1' ORDER BY asort ASC";
		$pdo = $pdoDB->prepare($sql);
	    $pdo->execute();
	    $row = $pdo->fetch(PDO::FETCH_ASSOC);

		$Init_Page = '5';
		
		$total_pages = ceil($row[Rows]/$Init_Page);	//計算總頁數
		if ($page>$total_pages) $page = 1;		//修正輸入過大的頁數
		

	
		$sql = "SELECT SQL_CALC_FOUND_ROWS *, (SELECT COUNT(*) FROM web_class where web_x_class_id = web_x_class.web_x_class_id and web_class.ifShow = '1') as Rows FROM web_x_class where web_x_class.ifShow = '1' ORDER BY asort ASC";
		$sql .= " limit ".($page-1) * $Init_Page.", ".$Init_Page;
		$pdo = $pdoDB->prepare($sql);
	    $pdo->execute();
	    $row = $pdo->fetchAll(PDO::FETCH_ASSOC);
		
		
		end($row);
		$lastKey = key($row);
	    
	    $prodAry = array();
	    $targetKey = array(
	    	'web_x_class_id',
	    	'subject',
			'Rows',
	    	//'price_member',
	    	//'Files',

	    );
		$prodAry[0]['type'] = 'bubble';
		$prodAry[0]['styles']['body']['backgroundColor'] = '#FFFFFF';
		$prodAry[0]['styles']['footer']['backgroundColor'] = '#FFFFFF';
		$prodAry[0]['body']['type'] = 'box';
		$prodAry[0]['body']['layout'] = 'vertical';
		$prodAry[0]['body']['spacing'] = 'sm';
		$prodAry[0]['body']['contents'][0]['type'] = 'text';
		$prodAry[0]['body']['contents'][0]['text'] = '線上預約';
		$prodAry[0]['body']['contents'][0]['wrap'] = true;
		$prodAry[0]['body']['contents'][0]['weight'] = "bold";
		$prodAry[0]['body']['contents'][0]['size'] = "xl";
		$prodAry[0]['body']['contents'][1]['type'] = 'text';
		$prodAry[0]['body']['contents'][1]['text'] = "分店據點 :";
		$prodAry[0]['body']['contents'][1]['wrap'] = true;
		$prodAry[0]['body']['contents'][1]['weight'] = "bold";
		$prodAry[0]['body']['contents'][1]['size'] = "sm";
		
		$prodAry[0]['footer']['type'] = 'box';
		$prodAry[0]['footer']['layout'] = 'vertical';
		$prodAry[0]['footer']['spacing'] = 'sm';
	    foreach($row as $key => $prodInfo) {
	    	foreach($prodInfo as $key2 => $prod) {
	    		if(!in_array($key2, $targetKey)) {
	    			continue;
	    		}
	    		if($key2 == 'subject') {
	    			$subject = $prod;
	    		}
				
				if($key2 == 'web_x_class_id') {
	    			$cid = $prod;
	    		}
				
				if($key2 == 'Rows') {
	    			$Rows = $prod;
	    		}
	    		/*
	    		$prodAry[$key] = array(
	    			
					'type' => 'postback',
					'label' => $subject."(".$Rows.")",
					"data" => "action=reservationCategory&cid=".$cid,
					
	    		);
				*/
				$prodAry[0]['footer']['contents'][$key]['type'] = 'button';
				$prodAry[0]['footer']['contents'][$key]['height'] = 'sm';
				$prodAry[0]['footer']['contents'][$key]['style'] = 'primary';
				$prodAry[0]['footer']['contents'][$key]['color'] = "#444444";
				$prodAry[0]['footer']['contents'][$key]['action']['type'] = 'postback';
				$prodAry[0]['footer']['contents'][$key]['action']['label'] = $subject."(".$Rows.")";
				$prodAry[0]['footer']['contents'][$key]['action']['data'] = "action=reservationCategory&cid=".$cid;
	    	}
	    	
	    }
		
		$prodAry[0]['footer']['contents'][$key+1]['type'] = 'text';
		$prodAry[0]['footer']['contents'][$key+1]['text'] = "Design by aibitechcology";
		$prodAry[0]['footer']['contents'][$key+1]['wrap'] = true;
		$prodAry[0]['footer']['contents'][$key+1]['color'] = "#aaaaaa";
		$prodAry[0]['footer']['contents'][$key+1]['size'] = "xxs";
		$prodAry[0]['footer']['contents'][$key+1]['align'] = "end";
		/*
		$prodAry[$lastKey + 1] = array(
			'type' => 'postback',
			'label' => '全部課程',
			"data" => "action=reservationCategory&cid=all",
		);
		*/

		switch($content_type) {
		
			case "text" :
				$content_type = "文字訊息";
				
				$data = $prodAry;
				echo "<pre>";
				print_r($data);
				echo "</pre>";
				$result = array();
				foreach($data as $key => $row) {
					//if (mb_strpos($message, $row[title]) !== false) {
						array_push($result, $row);
					//}
				}				
				
				$result = (!$result[0]) ? $data : $result;
				/*
				$data = array(
					"replyToken" => $reToken, 
					"messages" =>
						array(
						
							array(
								'type' => 'template',
								'altText' => '線上預約',
								'template' => array(
									'type' => 'buttons',
									'title' => '線上預約',
									'text' => '服務據點',
									'actions' => $result
								),
							),
						),
				);
				*/
				
				$data = array(
					"replyToken" => $reToken, 
					"messages" =>
						array(
							
							array(
								'type' => 'flex',
								'altText' => '線上預約',
								'contents' => array(
									'type' => 'carousel',
									'contents' => $result
								),
							),
						),
				);
				//file_put_contents("template.txt", json_encode($data)."\r\n",FILE_APPEND);
				break;
				
			
				
			default:
				$content_type = "未知訊息";
				break;
	   	}
		
		$context = stream_context_create(array(
		"http" => array("method" => "POST", "header" => implode(PHP_EOL, $header), "content" => json_encode($data), "ignore_errors" => true)
		));
		file_put_contents("debug.txt", $url."\r\n",FILE_APPEND);
		file_put_contents("debug.txt", json_encode($data)."\r\n",FILE_APPEND);
		$log = file_get_contents($url, false, $context);
		file_put_contents("debug.txt", json_encode($log)."\r\n",FILE_APPEND);
		
		//curl設定
		$headers = array(
			"Content-Type: application/json; charser=UTF-8",
			"Authorization: Bearer {" . $channel_access_token . "}"
		);
			 
		//curl
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		//如果失敗可以印出 $result 看錯誤訊息
		$result = curl_exec($ch);
		//file_put_contents("debug.txt", "==========================\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($result)."\r\n",FILE_APPEND);
		curl_close($ch);
	}
	
	//預約課程分類
	function reservationCategoryNew($content_type, $message, $page) {

	 	global $header, $reToken, $receive, $channel_access_token, $DB, $pdoDB, $from;
	 	
		$url = "https://api.line.me/v2/bot/message/reply";
		
		$data = ["replyToken" => $reToken, "messages" => array(["type" => "text", "text" => $message])];
				
		$sql = "SELECT COUNT(*) as Rows FROM web_x_class where web_x_class.ifShow = '1' ORDER BY asort ASC";
		$pdo = $pdoDB->prepare($sql);
	    $pdo->execute();
	    $row = $pdo->fetch(PDO::FETCH_ASSOC);

		$Init_Page = '10';
		
		$total_pages = ceil($row[Rows]/$Init_Page);	//計算總頁數
		if ($page>$total_pages) $page = 1;		//修正輸入過大的頁數
		

	
		$sql = "
			SELECT 
				SQL_CALC_FOUND_ROWS *, 
				(SELECT COUNT(*) FROM web_class where web_x_class_id = web_x_class.web_x_class_id and web_class.ifShow = '1') as Rows 
			FROM 
				web_x_class 
			where 
				web_x_class.ifShow = '1' 
			ORDER BY 
				asort ASC
		";
		$sql .= " limit ".($page-1) * $Init_Page.", ".$Init_Page;
		$pdo = $pdoDB->prepare($sql);
	    $pdo->execute();
	    $row = $pdo->fetchAll(PDO::FETCH_ASSOC);
		
		
		end($row);
		$lastKey = key($row);
	    
	    $prodAry = array();
	    $targetKey = array(
	    	'web_x_class_id',
	    	'subject',
			'Rows',
			'address',
	    	//'price_member',
	    	//'Files',

	    );
		
	    foreach($row as $key => $prodInfo) {
	    	foreach($prodInfo as $key2 => $prod) {
	    		if(!in_array($key2, $targetKey)) {
	    			continue;
	    		}
	    		if($key2 == 'subject') {
	    			$subject = $prod;
	    		}
				
				if($key2 == 'address') {
	    			$address = $prod;
	    		}
				
				if($key2 == 'web_x_class_id') {
	    			$cid = $prod;
	    		}
				
				if($key2 == 'Rows') {
	    			$Rows = $prod;
	    		}
				
				
	    		/*
	    		$prodAry[$key] = array(
	    			
					'type' => 'postback',
					'label' => $subject."(".$Rows.")",
					"data" => "action=reservationCategory&cid=".$cid,
					
	    		);
				*/
				$prodAry[$key]['type'] = 'bubble';
				$prodAry[$key]['styles']['body']['backgroundColor'] = '#FFFFFF';
				$prodAry[$key]['styles']['footer']['backgroundColor'] = '#FFFFFF';
				$prodAry[$key]['hero']['type'] = 'image';
				$prodAry[$key]['hero']['size'] = 'full';
				$prodAry[$key]['hero']['aspectRatio'] = '20:13';
				$prodAry[$key]['hero']['aspectMode'] = 'cover';
				$prodAry[$key]['hero']['url'] = $_SERVER['HTTP_X_FORWARDED_PROTO'].'://'.$_SERVER['HTTP_HOST'].'/uploadfiles/l/'.str_replace('/', '', $prodInfo['Covers']);
				$prodAry[$key]['body']['type'] = 'box';
				$prodAry[$key]['body']['layout'] = 'vertical';
				$prodAry[$key]['body']['spacing'] = 'sm';
				$prodAry[$key]['body']['contents'][0]['type'] = 'text';
				$prodAry[$key]['body']['contents'][0]['text'] = $prodInfo['subject'];
				$prodAry[$key]['body']['contents'][0]['wrap'] = true;
				$prodAry[$key]['body']['contents'][0]['weight'] = "bold";
				$prodAry[$key]['body']['contents'][0]['size'] = "xl";
				$prodAry[$key]['body']['contents'][1]['type'] = 'text';
				$prodAry[$key]['body']['contents'][1]['text'] = ($prodInfo['address']) ? $prodInfo['address'] : ' ';
				$prodAry[$key]['body']['contents'][1]['wrap'] = true;
				$prodAry[$key]['body']['contents'][1]['weight'] = "bold";
				$prodAry[$key]['body']['contents'][1]['size'] = "sm";	
				
				$prodAry[$key]['footer']['type'] = 'box';
				$prodAry[$key]['footer']['layout'] = 'vertical';
				$prodAry[$key]['footer']['spacing'] = 'sm';
				
				
				
				$prodAry[$key]['footer']['contents'][0]['type'] = 'button';
				$prodAry[$key]['footer']['contents'][0]['height'] = 'sm';
				$prodAry[$key]['footer']['contents'][0]['style'] = 'primary';
				$prodAry[$key]['footer']['contents'][0]['color'] = "#444444";
				$prodAry[$key]['footer']['contents'][0]['action']['type'] = 'postback';
				$prodAry[$key]['footer']['contents'][0]['action']['label'] = "預約設計師";
				$prodAry[$key]['footer']['contents'][0]['action']['data'] = "action=reservationCategory&cid=".$cid;
				
				$prodAry[$key]['footer']['contents'][1]['type'] = 'text';
				$prodAry[$key]['footer']['contents'][1]['text'] = "Design by aibitechcology";
				$prodAry[$key]['footer']['contents'][1]['wrap'] = true;
				$prodAry[$key]['footer']['contents'][1]['color'] = "#aaaaaa";
				$prodAry[$key]['footer']['contents'][1]['size'] = "xxs";
				$prodAry[$key]['footer']['contents'][1]['align'] = "end";
	    	}
	    	
	    }
		/*
		$prodAry[0]['footer']['contents'][$key+1]['type'] = 'text';
		$prodAry[0]['footer']['contents'][$key+1]['text'] = "Design by aibitechcology";
		$prodAry[0]['footer']['contents'][$key+1]['wrap'] = true;
		$prodAry[0]['footer']['contents'][$key+1]['color'] = "#aaaaaa";
		$prodAry[0]['footer']['contents'][$key+1]['size'] = "xxs";
		$prodAry[0]['footer']['contents'][$key+1]['align'] = "end";
		*/
		/*
		$prodAry[$lastKey + 1] = array(
			'type' => 'postback',
			'label' => '全部課程',
			"data" => "action=reservationCategory&cid=all",
		);
		*/
		
		if($total_pages>1) {
			/*
			$prodAry[$key + 1] = array(
				"title" => "更多資訊",
				"text" => "更多資訊",
				"actions" => array(
					array(
						"type" => "postback",
						"label" => "想看更多請點我",
						"data" => "action=more5&page=".($page+1),
					),
					
				),
				"thumbnailImageUrl" => "https://www.edison1525.net/uploadfiles/l/7210438951824.jpeg",
			);
			*/
			$prodAry[$key + 1] = array(
				"type" => "bubble",
				"body" => array(
					"type" => "box",
					"layout" => "vertical",
					"spacing" => "sm",
					"contents" => array(
						array(
							"type" => "button",
							"flex" => 1,
							"gravity" => "center",
							"action" => array(
								"type" => "postback",
								"label" => "想看更多請點我",
								"data" => "action=more6&page=".($page+1),
								
							),
						)	
					)	
				)	
			);
		}	

		switch($content_type) {
		
			case "text" :
				$content_type = "文字訊息";
				
				$data = $prodAry;
				echo "<pre>";
				print_r($data);
				echo "</pre>";
				$result = array();
				foreach($data as $key => $row) {
					//if (mb_strpos($message, $row[title]) !== false) {
						array_push($result, $row);
					//}
				}				
				
				$result = (!$result[0]) ? $data : $result;
				/*
				$data = array(
					"replyToken" => $reToken, 
					"messages" =>
						array(
						
							array(
								'type' => 'template',
								'altText' => '線上預約',
								'template' => array(
									'type' => 'buttons',
									'title' => '線上預約',
									'text' => '服務據點',
									'actions' => $result
								),
							),
						),
				);
				*/
				
				$data = array(
					"replyToken" => $reToken, 
					"messages" =>
						array(
							
							array(
								'type' => 'flex',
								'altText' => '線上預約',
								'contents' => array(
									'type' => 'carousel',
									'contents' => $result
								),
							),
						),
				);
				//file_put_contents("template.txt", json_encode($data)."\r\n",FILE_APPEND);
				break;
				
			
				
			default:
				$content_type = "未知訊息";
				break;
	   	}
		
		$context = stream_context_create(array(
		"http" => array("method" => "POST", "header" => implode(PHP_EOL, $header), "content" => json_encode($data), "ignore_errors" => true)
		));
		file_put_contents("debug.txt", $url."\r\n",FILE_APPEND);
		file_put_contents("debug.txt", json_encode($data)."\r\n",FILE_APPEND);
		$log = file_get_contents($url, false, $context);
		file_put_contents("debug.txt", json_encode($log)."\r\n",FILE_APPEND);
		
		//curl設定
		$headers = array(
			"Content-Type: application/json; charser=UTF-8",
			"Authorization: Bearer {" . $channel_access_token . "}"
		);
			 
		//curl
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		//如果失敗可以印出 $result 看錯誤訊息
		$result = curl_exec($ch);
		curl_close($ch);
		//file_put_contents("debug.txt", "==========================\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($result)."\r\n",FILE_APPEND);
		curl_close($ch);
	}
	
	//一對一各分店
	function reservationCategoryNewStore($content_type, $message, $page) {

	 	global $header, $reToken, $receive, $channel_access_token, $DB, $pdoDB, $from;
	 	
		$url = "https://api.line.me/v2/bot/message/reply";
		
		$data = ["replyToken" => $reToken, "messages" => array(["type" => "text", "text" => $message])];
				
		$sql = "SELECT COUNT(*) as Rows FROM web_x_class where web_x_class.ifShow = '1' ORDER BY asort ASC";
		$pdo = $pdoDB->prepare($sql);
	    $pdo->execute();
	    $row = $pdo->fetch(PDO::FETCH_ASSOC);

		$Init_Page = '10';
		
		$total_pages = ceil($row[Rows]/$Init_Page);	//計算總頁數
		if ($page>$total_pages) $page = 1;		//修正輸入過大的頁數
		

	
		$sql = "
			SELECT 
				SQL_CALC_FOUND_ROWS *, 
				(SELECT COUNT(*) FROM web_class where web_x_class_id = web_x_class.web_x_class_id and web_class.ifShow = '1') as Rows 
			FROM 
				web_x_class 
			where 
				web_x_class.ifShow = '1' 
			ORDER BY 
				asort ASC
		";
		$sql .= " limit ".($page-1) * $Init_Page.", ".$Init_Page;
		$pdo = $pdoDB->prepare($sql);
	    $pdo->execute();
	    $row = $pdo->fetchAll(PDO::FETCH_ASSOC);
		
		
		end($row);
		$lastKey = key($row);
	    
	    $prodAry = array();
	    $targetKey = array(
	    	'web_x_class_id',
	    	'subject',
			'Rows',
			'address',
	    	//'price_member',
	    	//'Files',

	    );
		
	    foreach($row as $key => $prodInfo) {
	    	foreach($prodInfo as $key2 => $prod) {
	    		if(!in_array($key2, $targetKey)) {
	    			continue;
	    		}
	    		if($key2 == 'subject') {
	    			$subject = $prod;
	    		}
				
				if($key2 == 'address') {
	    			$address = $prod;
	    		}
				
				if($key2 == 'web_x_class_id') {
	    			$cid = $prod;
	    		}
				
				if($key2 == 'Rows') {
	    			$Rows = $prod;
	    		}
				
				
	    		/*
	    		$prodAry[$key] = array(
	    			
					'type' => 'postback',
					'label' => $subject."(".$Rows.")",
					"data" => "action=reservationCategory&cid=".$cid,
					
	    		);
				*/
				$prodAry[$key]['type'] = 'bubble';
				$prodAry[$key]['styles']['body']['backgroundColor'] = '#FFFFFF';
				$prodAry[$key]['styles']['footer']['backgroundColor'] = '#FFFFFF';
				$prodAry[$key]['hero']['type'] = 'image';
				$prodAry[$key]['hero']['size'] = 'full';
				$prodAry[$key]['hero']['aspectRatio'] = '20:13';
				$prodAry[$key]['hero']['aspectMode'] = 'cover';
				$prodAry[$key]['hero']['url'] = $_SERVER['HTTP_X_FORWARDED_PROTO'].'://'.$_SERVER['HTTP_HOST'].'/uploadfiles/l/'.str_replace('/', '', $prodInfo['Covers']);
				$prodAry[$key]['body']['type'] = 'box';
				$prodAry[$key]['body']['layout'] = 'vertical';
				$prodAry[$key]['body']['spacing'] = 'sm';
				$prodAry[$key]['body']['contents'][0]['type'] = 'text';
				$prodAry[$key]['body']['contents'][0]['text'] = $prodInfo['subject'];
				$prodAry[$key]['body']['contents'][0]['wrap'] = true;
				$prodAry[$key]['body']['contents'][0]['weight'] = "bold";
				$prodAry[$key]['body']['contents'][0]['size'] = "xl";
				$prodAry[$key]['body']['contents'][1]['type'] = 'text';
				$prodAry[$key]['body']['contents'][1]['text'] = ($prodInfo['address']) ? $prodInfo['address'] : ' ';
				$prodAry[$key]['body']['contents'][1]['wrap'] = true;
				$prodAry[$key]['body']['contents'][1]['weight'] = "bold";
				$prodAry[$key]['body']['contents'][1]['size'] = "sm";	
				
				$prodAry[$key]['footer']['type'] = 'box';
				$prodAry[$key]['footer']['layout'] = 'vertical';
				$prodAry[$key]['footer']['spacing'] = 'sm';
				
				
				
				$prodAry[$key]['footer']['contents'][0]['type'] = 'button';
				$prodAry[$key]['footer']['contents'][0]['height'] = 'sm';
				$prodAry[$key]['footer']['contents'][0]['style'] = 'primary';
				$prodAry[$key]['footer']['contents'][0]['color'] = "#444444";
				$prodAry[$key]['footer']['contents'][0]['action']['type'] = 'postback';
				$prodAry[$key]['footer']['contents'][0]['action']['label'] = "一對一諮詢";
				$prodAry[$key]['footer']['contents'][0]['action']['data'] = "action=chat&cid=".$cid;
				
				$prodAry[$key]['footer']['contents'][1]['type'] = 'text';
				$prodAry[$key]['footer']['contents'][1]['text'] = "Design by aibitechcology";
				$prodAry[$key]['footer']['contents'][1]['wrap'] = true;
				$prodAry[$key]['footer']['contents'][1]['color'] = "#aaaaaa";
				$prodAry[$key]['footer']['contents'][1]['size'] = "xxs";
				$prodAry[$key]['footer']['contents'][1]['align'] = "end";
	    	}
	    	
	    }
		/*
		$prodAry[0]['footer']['contents'][$key+1]['type'] = 'text';
		$prodAry[0]['footer']['contents'][$key+1]['text'] = "Design by aibitechcology";
		$prodAry[0]['footer']['contents'][$key+1]['wrap'] = true;
		$prodAry[0]['footer']['contents'][$key+1]['color'] = "#aaaaaa";
		$prodAry[0]['footer']['contents'][$key+1]['size'] = "xxs";
		$prodAry[0]['footer']['contents'][$key+1]['align'] = "end";
		*/
		/*
		$prodAry[$lastKey + 1] = array(
			'type' => 'postback',
			'label' => '全部課程',
			"data" => "action=reservationCategory&cid=all",
		);
		*/
		
		if($total_pages>1) {
			/*
			$prodAry[$key + 1] = array(
				"title" => "更多資訊",
				"text" => "更多資訊",
				"actions" => array(
					array(
						"type" => "postback",
						"label" => "想看更多請點我",
						"data" => "action=more5&page=".($page+1),
					),
					
				),
				"thumbnailImageUrl" => "https://www.edison1525.net/uploadfiles/l/7210438951824.jpeg",
			);
			*/
			$prodAry[$key + 1] = array(
				"type" => "bubble",
				"body" => array(
					"type" => "box",
					"layout" => "vertical",
					"spacing" => "sm",
					"contents" => array(
						array(
							"type" => "button",
							"flex" => 1,
							"gravity" => "center",
							"action" => array(
								"type" => "postback",
								"label" => "想看更多請點我",
								"data" => "action=more6&page=".($page+1),
								
							),
						)	
					)	
				)	
			);
		}	

		switch($content_type) {
		
			case "text" :
				$content_type = "文字訊息";
				
				$data = $prodAry;
				echo "<pre>";
				print_r($data);
				echo "</pre>";
				$result = array();
				foreach($data as $key => $row) {
					//if (mb_strpos($message, $row[title]) !== false) {
						array_push($result, $row);
					//}
				}				
				
				$result = (!$result[0]) ? $data : $result;
				/*
				$data = array(
					"replyToken" => $reToken, 
					"messages" =>
						array(
						
							array(
								'type' => 'template',
								'altText' => '線上預約',
								'template' => array(
									'type' => 'buttons',
									'title' => '線上預約',
									'text' => '服務據點',
									'actions' => $result
								),
							),
						),
				);
				*/
				
				$data = array(
					"replyToken" => $reToken, 
					"messages" =>
						array(
							
							array(
								'type' => 'flex',
								'altText' => '線上預約',
								'contents' => array(
									'type' => 'carousel',
									'contents' => $result
								),
							),
						),
				);
				//file_put_contents("template.txt", json_encode($data)."\r\n",FILE_APPEND);
				break;
				
			
				
			default:
				$content_type = "未知訊息";
				break;
	   	}
		
		$context = stream_context_create(array(
		"http" => array("method" => "POST", "header" => implode(PHP_EOL, $header), "content" => json_encode($data), "ignore_errors" => true)
		));
		file_put_contents("debug.txt", $url."\r\n",FILE_APPEND);
		file_put_contents("debug.txt", json_encode($data)."\r\n",FILE_APPEND);
		$log = file_get_contents($url, false, $context);
		file_put_contents("debug.txt", json_encode($log)."\r\n",FILE_APPEND);
		
		//curl設定
		$headers = array(
			"Content-Type: application/json; charser=UTF-8",
			"Authorization: Bearer {" . $channel_access_token . "}"
		);
			 
		//curl
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		//如果失敗可以印出 $result 看錯誤訊息
		$result = curl_exec($ch);
		curl_close($ch);
		//file_put_contents("debug.txt", "==========================\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($result)."\r\n",FILE_APPEND);
		curl_close($ch);
	}
	
	//一對一各分店
	function chatPic($content_type, $message, $page) {

	 	global $header, $reToken, $receive, $channel_access_token, $DB, $pdoDB, $from;
	 	
		$url = "https://api.line.me/v2/bot/message/reply";
		
		$data = ["replyToken" => $reToken, "messages" => array(["type" => "text", "text" => $message])];
		
		$key = 0;
		
		$prodAry[$key]['type'] = 'bubble';
		
		$prodAry[$key]['body']['type'] = 'box';
		$prodAry[$key]['body']['layout'] = 'vertical';
		$prodAry[$key]['body']['contents'][0]['type'] = 'image';
		$prodAry[$key]['body']['contents'][0]['url'] = $_SERVER['HTTP_X_FORWARDED_PROTO'].'://'.$_SERVER['HTTP_HOST'].'/img/chat.png';
		$prodAry[$key]['body']['contents'][0]['size'] = 'full';
		$prodAry[$key]['body']['contents'][0]['aspectRatio'] = '1:1';
		$prodAry[$key]['body']['contents'][0]['aspectMode'] = 'fit';
	
		$prodAry[$key]['footer']['type'] = 'box';
		$prodAry[$key]['footer']['layout'] = 'vertical';

		$prodAry[$key]['footer']['contents'][0]['type'] = 'text';
		$prodAry[$key]['footer']['contents'][0]['text'] = "Design by aibitechcology";
		$prodAry[$key]['footer']['contents'][0]['wrap'] = true;
		$prodAry[$key]['footer']['contents'][0]['color'] = "#aaaaaa";
		$prodAry[$key]['footer']['contents'][0]['size'] = "xxs";
		$prodAry[$key]['footer']['contents'][0]['align'] = "end";
		
		
		

		switch($content_type) {
		
			case "text" :
				$content_type = "文字訊息";
				
				$data = $prodAry;
				echo "<pre>";
				print_r($data);
				echo "</pre>";
				$result = array();
				foreach($data as $key => $row) {
					//if (mb_strpos($message, $row[title]) !== false) {
						array_push($result, $row);
					//}
				}				
				
				$result = (!$result[0]) ? $data : $result;
				/*
				$data = array(
					"replyToken" => $reToken, 
					"messages" =>
						array(
						
							array(
								'type' => 'template',
								'altText' => '線上預約',
								'template' => array(
									'type' => 'buttons',
									'title' => '線上預約',
									'text' => '服務據點',
									'actions' => $result
								),
							),
						),
				);
				*/
				
				$data = array(
					"replyToken" => $reToken, 
					"messages" =>
						array(
							
							array(
								'type' => 'flex',
								'altText' => '線上預約',
								'contents' => array(
									'type' => 'carousel',
									'contents' => $result
								),
							),
						),
				);
				//file_put_contents("template.txt", json_encode($data)."\r\n",FILE_APPEND);
				break;
				
			
				
			default:
				$content_type = "未知訊息";
				break;
	   	}
		
		$context = stream_context_create(array(
		"http" => array("method" => "POST", "header" => implode(PHP_EOL, $header), "content" => json_encode($data), "ignore_errors" => true)
		));
		file_put_contents("debug.txt", $url."\r\n",FILE_APPEND);
		file_put_contents("debug.txt", json_encode($data)."\r\n",FILE_APPEND);
		$log = file_get_contents($url, false, $context);
		file_put_contents("debug.txt", json_encode($log)."\r\n",FILE_APPEND);
		
		//curl設定
		$headers = array(
			"Content-Type: application/json; charser=UTF-8",
			"Authorization: Bearer {" . $channel_access_token . "}"
		);
			 
		//curl
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		//如果失敗可以印出 $result 看錯誤訊息
		$result = curl_exec($ch);
		curl_close($ch);
		//file_put_contents("debug.txt", "==========================\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($result)."\r\n",FILE_APPEND);
		curl_close($ch);
	}
	
	//預約課程分類
	function reservationCategory2($content_type, $message, $page) {

	 	global $header, $reToken, $receive, $channel_access_token, $DB, $pdoDB, $from;
	 	
		$url = "https://api.line.me/v2/bot/message/reply";
		
		$data = ["replyToken" => $reToken, "messages" => array(["type" => "text", "text" => $message])];
				
		$sql = "SELECT COUNT(*) as Rows FROM web_x_class where web_x_class.ifShow = '1' ORDER BY web_x_class_id DESC";
		$pdo = $pdoDB->prepare($sql);
	    $pdo->execute();
	    $row = $pdo->fetch(PDO::FETCH_ASSOC);

		$Init_Page = '5';
		
		$total_pages = ceil($row[Rows]/$Init_Page);	//計算總頁數
		if ($page>$total_pages) $page = 1;		//修正輸入過大的頁數
		

	
		$sql = "SELECT SQL_CALC_FOUND_ROWS *, (SELECT COUNT(*) FROM web_class where web_x_class_id = web_x_class.web_x_class_id and web_class.ifShow = '1') as Rows FROM web_x_class where web_x_class.ifShow = '1' ORDER BY web_x_class_id DESC";
		$sql .= " limit ".($page-1) * $Init_Page.", ".$Init_Page;
		$pdo = $pdoDB->prepare($sql);
	    $pdo->execute();
	    $row = $pdo->fetchAll(PDO::FETCH_ASSOC);
		
		
		end($row);
		$lastKey = key($row);
	    
	    $prodAry = array();
	    $targetKey = array(
	    	'web_x_class_id',
	    	'subject',
			'Rows',
	    	//'price_member',
	    	//'Files',

	    );
	    foreach($row as $key => $prodInfo) {
	    	foreach($prodInfo as $key2 => $prod) {
	    		if(!in_array($key2, $targetKey)) {
	    			continue;
	    		}
	    		if($key2 == 'subject') {
	    			$subject = $prod;
	    		}
				
				if($key2 == 'web_x_class_id') {
	    			$cid = $prod;
	    		}
				
				if($key2 == 'Rows') {
	    			$Rows = $prod;
	    		}
	    		
	    		$prodAry[$key] = array(
	    			
					'type' => 'postback',
					'label' => $subject."(".$Rows.")",
					"data" => "action=reservationCategory&cid=".$cid,
					
	    		);
	    	}
	    	
	    }
		/*
		$prodAry[$lastKey + 1] = array(
			'type' => 'postback',
			'label' => '全部課程',
			"data" => "action=reservationCategory&cid=all",
		);
		*/
		switch($content_type) {
		
			case "text" :
				$content_type = "文字訊息";
				
				$data = $prodAry;
				
				$result = array();
				foreach($data as $key => $row) {
					//if (mb_strpos($message, $row[title]) !== false) {
						array_push($result, $row);
					//}
				}				
				
				$result = (!$result[0]) ? $data : $result;
				
				$data = array(
					"replyToken" => $reToken, 
					"messages" =>
						array(
						
							array(
								'type' => 'template',
								'altText' => '主治項目',
								'template' => array(
									'type' => 'buttons',
									'title' => '主治項目',
									'text' => '診療項目',
									'actions' => $result
								),
							),
						),
				);
				
				//file_put_contents("template.txt", json_encode($data)."\r\n",FILE_APPEND);
				break;
				
			
				
			default:
				$content_type = "未知訊息";
				break;
	   	}
		
		$context = stream_context_create(array(
		"http" => array("method" => "POST", "header" => implode(PHP_EOL, $header), "content" => json_encode($data), "ignore_errors" => true)
		));
		//file_put_contents("debug.txt", $url."\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($data)."\r\n",FILE_APPEND);
		//$log = file_get_contents($url, false, $context);
		//file_put_contents("debug.txt", json_encode($log)."\r\n",FILE_APPEND);
		
		//curl設定
		$headers = array(
			"Content-Type: application/json; charser=UTF-8",
			"Authorization: Bearer {" . $channel_access_token . "}"
		);
			 
		//curl
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		//如果失敗可以印出 $result 看錯誤訊息
		$result = curl_exec($ch);
		//file_put_contents("debug.txt", "==========================\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($result)."\r\n",FILE_APPEND);
		
		curl_close($ch);
	}
	
	//掛號全部
	function reservationCategoryAll($content_type, $message, $page) {

	 	global $header, $reToken, $receive, $channel_access_token, $DB, $pdoDB, $from;
	 	
		$url = "https://api.line.me/v2/bot/message/reply";
		
		$data = ["replyToken" => $reToken, "messages" => array(["type" => "text", "text" => $message])];
				
		$sql = "
			SELECT 
				a.*,
				b.subject as classSubjct,
				b.Files,
				c.subject as teamSubject,
				c.restDay
			FROM 
				web_shift a
			LEFT JOIN 
				web_class b ON b.web_class_id = a.web_class_id 		
			LEFT JOIN 
				web_team c ON c.web_team_id = a.web_team_id	
			LEFT JOIN 
				web_x_class d ON d.web_x_class_id = b.web_x_class_id			
			WHERE 
				a.ifShow = '1'
			AND	
				b.ifShow = '1'
			AND
                c.ifShow = '1'
			AND
                d.ifShow = '1'		
			ORDER BY 
				a.web_class_id ASC, a.web_team_id ASC
		";
		$pdo = $pdoDB->prepare($sql);
	    $pdo->execute();
	    $row = $pdo->fetchAll(PDO::FETCH_ASSOC);
	
		
		$groupAry = array();
		foreach($row as $key => $val) {
			$groupAry[$val['web_team_id']][$val['web_class_id']] = $val;
		}

		$finalGroupAry = array();
		foreach($groupAry as $key2 => $groupRow) {
			foreach($groupRow as $key3 => $groupVal) {
				$finalGroupAry[] = $groupVal;
			}	
		}
		
		echo "<pre>";
		print_r($finalGroupAry);
		echo "</pre>";
		$Init_Page = '10';
		
		$moreFlage = 0;
		
		$total_pages = ceil(count($finalGroupAry)/$Init_Page);	//計算總頁數
		if ($page>$total_pages) $page = 1;		//修正輸入過大的頁數
		
		$moreFlage = (count($finalGroupAry) > $Init_Page) ? 1 : 0;
		
		$arrayLimit = $page * $Init_Page;
	    
	    $prodAry = array();
	    $targetKey = array(
	    	//'web_shift_id',
	    	//'subject',
	    	'classSubjct',
			//'teamSubject',
	    	'Files',

	    );
		
		echo $startKey = (ceil(ceil($arrayLimit - $Init_Page) - 1)>0) ? ceil(ceil($arrayLimit - $Init_Page)) : 0;
		echo "</br>";
		echo $endKey = ceil($arrayLimit - 1);
		
	    foreach($finalGroupAry as $key => $prodInfo) {
			if($key >= $startKey && $key <= $endKey) {
				//echo $key;
				foreach($prodInfo as $key2 => $prod) {
					if(!in_array($key2, $targetKey)) {
						continue;
					}
					if($key2 == 'subject') {
						$subject = $prod;
					}
					
					if($key2 == 'web_shift_id') {
						$cid = $prod;
					}
					
					if($key2 == 'classSubjct') {
						$classSubjct = $prod;
					}
					
					if($key2 == 'teamSubject') {
						$teamSubject = $prod;
					}
					
					if($key2 == 'Files') {
						//$prod = 'https://www.edison1525.net/uploadfiles/l/'.str_replace('/', '', $prod);
						$Files = $_SERVER['HTTP_X_FORWARDED_PROTO'].'://'.$_SERVER['HTTP_HOST'].'/uploadfiles/l/'.str_replace('/', '', $prod);
					}
					/*
					$prodAry[$key][str_replace($targetKey, array('title','thumbnailImageUrl'), $key2)] = $prod;
					$prodAry[$key][text] = $prodInfo[teamSubject]." 醫師";
					$prodAry[$key][actions] = array(
						array(
							'type' => 'postback',
							'label' => '掛號',
							"data" => "action=reservation&item=register&class=".$prodInfo[web_class_id]."&team=".$prodInfo[web_team_id],
						),
					);
					*/
					$prodAry[$key]['type'] = 'bubble';
					$prodAry[$key]['styles']['body']['backgroundColor'] = '#FFFFFF';
					$prodAry[$key]['styles']['footer']['backgroundColor'] = '#FFFFFF';
					$prodAry[$key]['hero']['type'] = 'image';
					$prodAry[$key]['hero']['size'] = 'full';
					$prodAry[$key]['hero']['aspectRatio'] = '20:13';
					$prodAry[$key]['hero']['aspectMode'] = 'cover';
					$prodAry[$key]['hero']['url'] = $Files;
					
					$prodAry[$key]['body']['type'] = 'box';
					$prodAry[$key]['body']['layout'] = 'vertical';
					$prodAry[$key]['body']['spacing'] = 'sm';
					$prodAry[$key]['body']['contents'][0]['type'] = 'text';
					$prodAry[$key]['body']['contents'][0]['text'] = $classSubjct;
					$prodAry[$key]['body']['contents'][0]['wrap'] = true;
					$prodAry[$key]['body']['contents'][0]['weight'] = "bold";
					$prodAry[$key]['body']['contents'][0]['size'] = "xl";
					$prodAry[$key]['body']['contents'][1]['type'] = 'text';
					$prodAry[$key]['body']['contents'][1]['text'] = $prodInfo[teamSubject]." 設計師";
					$prodAry[$key]['body']['contents'][1]['wrap'] = true;
					$prodAry[$key]['body']['contents'][1]['weight'] = "bold";
					$prodAry[$key]['body']['contents'][1]['size'] = "sm";
					
					$prodAry[$key]['footer']['type'] = 'box';
					$prodAry[$key]['footer']['layout'] = 'vertical';
					$prodAry[$key]['footer']['spacing'] = 'sm';
					$prodAry[$key]['footer']['contents'][0]['type'] = 'button';
					$prodAry[$key]['footer']['contents'][0]['height'] = 'sm';
					$prodAry[$key]['footer']['contents'][0]['style'] = 'primary';
					$prodAry[$key]['footer']['contents'][0]['color'] = "#444444";
					$prodAry[$key]['footer']['contents'][0]['action']['type'] = 'postback';
					$prodAry[$key]['footer']['contents'][0]['action']['label'] = '預約';
					$prodAry[$key]['footer']['contents'][0]['action']['data'] = "action=reservation&item=register&class=".$prodInfo[web_class_id]."&team=".$prodInfo[web_team_id];
					
					$prodAry[$key]['footer']['contents'][1]['type'] = 'text';
					$prodAry[$key]['footer']['contents'][1]['text'] = "Design by aibitechcology";
					$prodAry[$key]['footer']['contents'][1]['wrap'] = true;
					$prodAry[$key]['footer']['contents'][1]['color'] = "#aaaaaa";
					$prodAry[$key]['footer']['contents'][1]['size'] = "xxs";
					$prodAry[$key]['footer']['contents'][1]['align'] = "end";
				}
	    	}
	    }
		if($moreFlage) {
			/*
			$prodAry[$key + 1] = array(
				"title" => "更多資訊",
				"text" => "更多資訊",
				"actions" => array(
					array(
						"type" => "postback",
						"label" => "想看更多請點我",
						"data" => "action=more5&page=".($page+1),
					),
					
				),
				"thumbnailImageUrl" => "https://www.edison1525.net/uploadfiles/l/7210438951824.jpeg",
			);
			*/
			$prodAry[$key + 1] = array(
				"type" => "bubble",
				"body" => array(
					"type" => "box",
					"layout" => "vertical",
					"spacing" => "sm",
					"contents" => array(
						array(
							"type" => "button",
							"flex" => 1,
							"gravity" => "center",
							"action" => array(
								"type" => "postback",
								"label" => "想看更多請點我",
								"data" => "action=more5&page=".($page+1),
								
							),
						)	
					)	
				)	
			);
		}	
		echo "<pre>";
		print_r($prodAry);
		echo "</pre>";
		switch($content_type) {
		
			case "text" :
				$content_type = "文字訊息";
				
				$data = $prodAry;
				
				$result = array();
				foreach($data as $key => $row) {
					//if (mb_strpos($message, $row[title]) !== false) {
						array_push($result, $row);
					//}
				}				
				
				$result = (!$result[0]) ? $data : $result;
				/*
				$data = array(
					"replyToken" => $reToken, 
					"messages" =>
						array(
						
							array(
								'type' => 'template',
								'altText' => '線上預約',
								'template' => array(
									'type' => 'carousel',
									'columns' => $result
								),
							),
						),
				);
				*/
				$data = array(
					"replyToken" => $reToken, 
					"messages" =>
						array(
							
							array(
								'type' => 'flex',
								'altText' => '線上預約',
								'contents' => array(
									'type' => 'carousel',
									'contents' => $result
								),
							),
						),
				);
				
				//file_put_contents("template.txt", json_encode($data)."\r\n",FILE_APPEND);
				break;
				
			
				
			default:
				$content_type = "未知訊息";
				break;
	   	}
		
		$context = stream_context_create(array(
		"http" => array("method" => "POST", "header" => implode(PHP_EOL, $header), "content" => json_encode($data), "ignore_errors" => true)
		));
		//file_put_contents("debug.txt", $url."\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($data)."\r\n",FILE_APPEND);
		//$log = file_get_contents($url, false, $context);
		//file_put_contents("debug.txt", json_encode($log)."\r\n",FILE_APPEND);
		
		//curl設定
		$headers = array(
			"Content-Type: application/json; charser=UTF-8",
			"Authorization: Bearer {" . $channel_access_token . "}"
		);
			 
		//curl
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		//如果失敗可以印出 $result 看錯誤訊息
		$result = curl_exec($ch);
		//file_put_contents("debug.txt", "==========================\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($result)."\r\n",FILE_APPEND);
		
		curl_close($ch);
	}
	
	//掛號全部-叫號
	function reservationCategoryAllCall($content_type, $message, $page) {

	 	global $header, $reToken, $receive, $channel_access_token, $DB, $pdoDB, $from;
	 	
		$url = "https://api.line.me/v2/bot/message/reply";
		
		$data = ["replyToken" => $reToken, "messages" => array(["type" => "text", "text" => $message])];
				
		$sql = "
			SELECT 
				a.*,
				b.subject as classSubjct,
				b.Files,
				c.subject as teamSubject,
				c.restDay,
				e.subject as esubject,
				e.urlsubject as eurlsubject
			FROM 
				web_shift a
			LEFT JOIN 
				web_class b ON b.web_class_id = a.web_class_id 		
			LEFT JOIN 
				web_team c ON c.web_team_id = a.web_team_id	
			LEFT JOIN 
				web_x_class d ON d.web_x_class_id = b.web_x_class_id
			LEFT JOIN 
				web_time e ON e.web_time_id = a.subject	
			WHERE 
				a.ifShow = '1'
			AND	
				b.ifShow = '1'
			AND
                c.ifShow = '1'
			AND
                d.ifShow = '1'		
			ORDER BY 
				a.web_class_id ASC, a.web_team_id ASC, a.subject ASC
		";
		$pdo = $pdoDB->prepare($sql);
	    $pdo->execute();
	    $row = $pdo->fetchAll(PDO::FETCH_ASSOC);
		
		$groupAry = array();
		$groupAry2 = array();
		foreach($row as $key => $val) {
			$groupAry[$val['web_team_id']][$val['web_class_id']] = $val;
			$groupAry2[$val['web_team_id']][$val['web_class_id']][] = $val;
		}
		
		echo "<pre>";
		print_r($groupAry2);
		echo "</pre>";

		$finalGroupAry = array();
		foreach($groupAry as $key2 => $groupRow) {
			foreach($groupRow as $key3 => $groupVal) {
				$finalGroupAry[] = $groupVal;
			}	
		}
		
		
		$Init_Page = '10';
		
		$moreFlage = 0;
		
		$total_pages = ceil(count($finalGroupAry)/$Init_Page);	//計算總頁數
		if ($page>$total_pages) $page = 1;		//修正輸入過大的頁數
		
		$moreFlage = (count($finalGroupAry) > $Init_Page) ? 1 : 0;
		
		$arrayLimit = $page * $Init_Page;
	    
	    $prodAry = array();
	    $targetKey = array(
	    	//'web_shift_id',
	    	//'subject',
	    	'classSubjct',
			//'teamSubject',
	    	'Files',

	    );
		
		echo $startKey = (ceil(ceil($arrayLimit - $Init_Page) - 1)>0) ? ceil(ceil($arrayLimit - $Init_Page)) : 0;
		echo "</br>";
		echo $endKey = ceil($arrayLimit - 1);
		
	    foreach($finalGroupAry as $key => $prodInfo) {
			if($key >= $startKey && $key <= $endKey) {
				//echo $key;
				foreach($prodInfo as $key2 => $prod) {
					if(!in_array($key2, $targetKey)) {
						continue;
					}
					if($key2 == 'subject') {
						$subject = $prod;
					}
					
					if($key2 == 'web_shift_id') {
						$cid = $prod;
					}
					
					if($key2 == 'classSubjct') {
						$classSubjct = $prod;
					}
					
					if($key2 == 'teamSubject') {
						$teamSubject = $prod;
					}
					
					if($key2 == 'Files') {
						//$prod = 'https://www.edison1525.net/uploadfiles/l/'.str_replace('/', '', $prod);
						$Files = $_SERVER['HTTP_X_FORWARDED_PROTO'].'://'.$_SERVER['HTTP_HOST'].'/uploadfiles/l/'.str_replace('/', '', $prod);
					}
					
					$prodAry[$key]['type'] = 'bubble';
					$prodAry[$key]['styles']['body']['backgroundColor'] = '#FFFFFF';
					$prodAry[$key]['styles']['footer']['backgroundColor'] = '#FFFFFF';
					$prodAry[$key]['hero']['type'] = 'image';
					$prodAry[$key]['hero']['size'] = 'full';
					$prodAry[$key]['hero']['aspectRatio'] = '20:13';
					$prodAry[$key]['hero']['aspectMode'] = 'cover';
					$prodAry[$key]['hero']['url'] = $Files;
					
					$prodAry[$key]['body']['type'] = 'box';
					$prodAry[$key]['body']['layout'] = 'vertical';
					$prodAry[$key]['body']['spacing'] = 'sm';
					$prodAry[$key]['body']['contents'][0]['type'] = 'text';
					$prodAry[$key]['body']['contents'][0]['text'] = $classSubjct;
					$prodAry[$key]['body']['contents'][0]['wrap'] = true;
					$prodAry[$key]['body']['contents'][0]['weight'] = "bold";
					$prodAry[$key]['body']['contents'][0]['size'] = "xl";
					$prodAry[$key]['body']['contents'][1]['type'] = 'text';
					$prodAry[$key]['body']['contents'][1]['text'] = $prodInfo[teamSubject]." 醫師";
					$prodAry[$key]['body']['contents'][1]['wrap'] = true;
					$prodAry[$key]['body']['contents'][1]['weight'] = "bold";
					$prodAry[$key]['body']['contents'][1]['size'] = "sm";
					
					
					$prodAry[$key]['footer']['type'] = 'box';
					$prodAry[$key]['footer']['layout'] = 'vertical';
					$prodAry[$key]['footer']['spacing'] = 'sm';
					
					foreach($groupAry2[$prodInfo['web_team_id']][$prodInfo['web_class_id']] as $groupKey => $groupVal) {
						
						$logSql = "
							Select 
								ordernum 
							From 
								web_call_log 
							Where 
								web_shift_id = :web_shift_id 
							AND 
								registerDate = :registerDate 
							ORDER BY 
								ordernum DESC, cdate DESC
						";
						$excute = array(
							':web_shift_id'        => $groupVal['web_shift_id'],
							':registerDate'        => date('Y-m-d')
						);
						$pdo = $pdoDB->prepare($logSql);
						$pdo->execute($excute);
						$logRow = $pdo->fetch(PDO::FETCH_ASSOC);
						$_ordernum = ($logRow['ordernum']) ? $logRow['ordernum'] : 0;
						
						$prodAry[$key]['footer']['contents'][$groupKey]['type'] = 'button';
						$prodAry[$key]['footer']['contents'][$groupKey]['height'] = 'sm';
						$prodAry[$key]['footer']['contents'][$groupKey]['style'] = 'primary';
						$prodAry[$key]['footer']['contents'][$groupKey]['color'] = "#444444";
						$prodAry[$key]['footer']['contents'][$groupKey]['action']['type'] = 'postback';
						$prodAry[$key]['footer']['contents'][$groupKey]['action']['label'] = $groupVal['eurlsubject'].$groupVal['esubject'].' 目前叫號：'.$_ordernum.'號';
						$prodAry[$key]['footer']['contents'][$groupKey]['action']['data'] = ' ';
					}
					
					$prodAry[$key]['footer']['contents'][$groupKey + 1]['type'] = 'text';
					$prodAry[$key]['footer']['contents'][$groupKey + 1]['text'] = "Design by aibitechcology";
					$prodAry[$key]['footer']['contents'][$groupKey + 1]['wrap'] = true;
					$prodAry[$key]['footer']['contents'][$groupKey + 1]['color'] = "#aaaaaa";
					$prodAry[$key]['footer']['contents'][$groupKey + 1]['size'] = "xxs";
					$prodAry[$key]['footer']['contents'][$groupKey + 1]['align'] = "end";
					
					
					/*
					$prodAry[$key][text] = $prodInfo[teamSubject]." 醫師";
					$prodAry[$key][actions] = array(
						array(
							'type' => 'postback',
							'label' => '目前叫號：'.$key.'號',
							"data" => " ",
						),
					);
					*/
				}
	    	}
	    }
		if($moreFlage) {
			/*
			$prodAry[$key + 1] = array(
				"title" => "更多資訊",
				"text" => "更多資訊",
				"actions" => array(
					array(
						"type" => "postback",
						"label" => "想看更多請點我",
						"data" => "action=more5&page=".($page+1),
					),
					
				),
				"thumbnailImageUrl" => "https://www.edison1525.net/uploadfiles/l/7210438951824.jpeg",
			);
			*/
			$prodAry[$key + 1] = array(
				"type" => "bubble",
				"body" => array(
					"type" => "box",
					"layout" => "vertical",
					"spacing" => "sm",
					"contents" => array(
						array(
							"type" => "button",
							"flex" => 1,
							"gravity" => "center",
							"action" => array(
								"type" => "postback",
								"label" => "想看更多請點我",
								"data" => "action=more5&page=".($page+1),
								
							),
						)	
					)	
				)	
			);
		}	
		/*
		echo "<pre>";
		print_r($prodAry);
		echo "</pre>";
		*/
		switch($content_type) {
		
			case "text" :
				$content_type = "文字訊息";
				
				$data = $prodAry;
				
				$result = array();
				foreach($data as $key => $row) {
					//if (mb_strpos($message, $row[title]) !== false) {
						array_push($result, $row);
					//}
				}				
				
				$result = (!$result[0]) ? $data : $result;
				
				
				/*
				$data = array(
					"replyToken" => $reToken, 
					"messages" =>
						array(
							
							array(
								'type' => 'template',
								'altText' => '線上預約',
								'template' => array(
									'type' => 'carousel',
									'columns' => $result
								),
							),
							//json_decode($test, true)
						),
				);
				*/
				
				$data = array(
					"replyToken" => $reToken, 
					"messages" =>
						array(
							
							array(
								'type' => 'flex',
								'altText' => '線上預約',
								'contents' => array(
									'type' => 'carousel',
									'contents' => $result
								),
							),
						),
				);
				echo "<pre>";
				print_r($data);
				echo "</pre>";
				echo json_encode($data);
				//file_put_contents("template.txt", json_encode($data)."\r\n",FILE_APPEND);
				break;
				
			
				
			default:
				$content_type = "未知訊息";
				break;
	   	}
		
		$context = stream_context_create(array(
		"http" => array("method" => "POST", "header" => implode(PHP_EOL, $header), "content" => json_encode($data), "ignore_errors" => true)
		));
		//file_put_contents("debug.txt", $url."\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($data)."\r\n",FILE_APPEND);
		//$log = file_get_contents($url, false, $context);
		//file_put_contents("debug.txt", json_encode($log)."\r\n",FILE_APPEND);
		
		//curl設定
		$headers = array(
			"Content-Type: application/json; charser=UTF-8",
			"Authorization: Bearer {" . $channel_access_token . "}"
		);
			 
		//curl
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		//如果失敗可以印出 $result 看錯誤訊息
		$result = curl_exec($ch);
		//file_put_contents("debug.txt", "==========================\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($result)."\r\n",FILE_APPEND);
		
		curl_close($ch);
	}
	
	//預約課程
	function reservation($content_type, $message, $page, $cid = 0) {

	 	global $header, $reToken, $receive, $channel_access_token, $DB, $pdoDB, $from;
	 	
		$url = "https://api.line.me/v2/bot/message/reply";
		
		$data = ["replyToken" => $reToken, "messages" => array(["type" => "text", "text" => $message])];
				
		$sql = "SELECT COUNT(*) as Rows FROM web_class where web_class.ifShow = '1'";
		if($cid)
			$sql .= " AND web_class.web_x_class_id = ".$cid;
		$sql .= " ORDER BY web_class.asort DESC";
		//echo $sql;
		$pdo = $pdoDB->prepare($sql);
	    $pdo->execute();
	    $row = $pdo->fetch(PDO::FETCH_ASSOC);

		$Init_Page = '7';
		
		$total_pages = ceil($row[Rows]/$Init_Page);	//計算總頁數
		//echo "</br>";
		if ($page>$total_pages) $page = 1;		//修正輸入過大的頁數
		
		$sql = "
			SELECT 
				SQL_CALC_FOUND_ROWS web_class.*, 
				(SELECT subject FROM web_x_class where web_x_class.web_x_class_id = web_class.web_x_class_id) as csubject 
			FROM 
				web_class 
			where 
				web_class.ifShow = '1'
		";
		if($cid)
			$sql .= " AND web_class.web_x_class_id = ".$cid;
		$sql .= " ORDER BY web_class.asort ASC";
		$sql .= " limit ".($page-1) * $Init_Page.", ".$Init_Page;
		//echo $sql;
		$pdo = $pdoDB->prepare($sql);
	    $pdo->execute();
	    $row = $pdo->fetchAll(PDO::FETCH_ASSOC);

		end($row);
		$lastKey = key($row);
	    
	    $prodAry = array();
	    $targetKey = array(
	    	//'web_product_id',
			'subject',
	    	//'price_member',
	    	'Files',

	    );

	    foreach($row as $key => $prodInfo) {
			
			echo $sql2 = "
				SELECT 
					main.web_team_id as main_web_team_id,
					teams.subject as teams_subject
				FROM 
					(SELECT * from web_shift WHERE web_shift.web_class_id = '".$prodInfo['web_class_id']."' group by web_team_id) as main 
				left join 
					(SELECT * from web_team) as teams
				on 
					main.web_team_id = teams.web_team_id
				where teams.ifShow = '1'	
			";
			;
			//file_put_contents("debug.txt", $sql2."\r\n",FILE_APPEND);
			$pdo = $pdoDB->prepare($sql2);
			$pdo->execute();
			$row2 = $pdo->fetchAll(PDO::FETCH_ASSOC);
			
			
	    	foreach($prodInfo as $key2 => $prod) {
	    		if(!in_array($key2, $targetKey)) {
	    			continue;
	    		}
	    		if($key2 == 'subject') {
	    			$prod = $prod;
	    		}
	    		if($key2 == 'Files') {
	    			$prod = $_SERVER['HTTP_X_FORWARDED_PROTO'].'://'.$_SERVER['HTTP_HOST'].'/uploadfiles/l/'.str_replace('/', '', $prod);
	    		}
			
				$doctorInfoAry = array();
				if(count($row2)) {
					foreach($row2 as $key3 => $doctorInfo) {
					
						$doctorInfoAry[$key3]['type'] = "postback";
						$doctorInfoAry[$key3]['label'] = $doctorInfo['teams_subject'];
						$doctorInfoAry[$key3]['data'] = "action=reservation&item=reservationDate&prodid=".$prodInfo[web_class_id]."&csubject=".$prodInfo[csubject]."&subject=".$prodInfo[subject]."&people=".$doctorInfo['teams_subject'];
					}
				} else {
					$doctorInfoAry[0]['type'] = "message";
					$doctorInfoAry[0]['label'] = " ";
					$doctorInfoAry[0]['text'] = " ";
				}	
				
				
				file_put_contents("debug.txt", json_encode($doctorInfoAry)."\r\n",FILE_APPEND);
				//$prodAry[$key][str_replace($targetKey, array('title','thumbnailImageUrl'), $key2)] = $prod;
				//$prodAry[$key][text] = ($prodInfo[csubject]);
				//file_put_contents("debug.txt", $key."\r\n",FILE_APPEND);
				
				$prodAry[$key]['type'] = 'bubble';
				$prodAry[$key]['styles']['body']['backgroundColor'] = '#FFFFFF';
				$prodAry[$key]['styles']['footer']['backgroundColor'] = '#FFFFFF';
				$prodAry[$key]['hero']['type'] = 'image';
				$prodAry[$key]['hero']['size'] = 'full';
				$prodAry[$key]['hero']['aspectRatio'] = '20:13';
				$prodAry[$key]['hero']['aspectMode'] = 'cover';
				$prodAry[$key]['hero']['url'] = $_SERVER['HTTP_X_FORWARDED_PROTO'].'://'.$_SERVER['HTTP_HOST'].'/uploadfiles/l/'.str_replace('/', '', $prodInfo['Files']);
				$prodAry[$key]['body']['type'] = 'box';
				$prodAry[$key]['body']['layout'] = 'vertical';
				$prodAry[$key]['body']['spacing'] = 'sm';
				$prodAry[$key]['body']['contents'][0]['type'] = 'text';
				$prodAry[$key]['body']['contents'][0]['text'] = $prodInfo[subject];
				$prodAry[$key]['body']['contents'][0]['wrap'] = true;
				$prodAry[$key]['body']['contents'][0]['weight'] = "bold";
				$prodAry[$key]['body']['contents'][0]['size'] = "xl";
				$prodAry[$key]['body']['contents'][1]['type'] = 'text';
				$prodAry[$key]['body']['contents'][1]['text'] = $prodInfo[csubject];
				$prodAry[$key]['body']['contents'][1]['wrap'] = true;
				$prodAry[$key]['body']['contents'][1]['weight'] = "bold";
				$prodAry[$key]['body']['contents'][1]['size'] = "sm";	
				
				if(!count($row2) && 0) {
					/*
					$prodAry[$key][actions] = array(
						array(
							"type" => "postback",
							"label" => "今日沒看診",
							"data" => " ",
						),
						array(
							"type" => "uri",
							"label" => "查看詳情",
							"uri" => "https://tw.yahoo.com",
						),
					);
					*/
					$prodAry[$key]['footer']['type'] = 'box';
					$prodAry[$key]['footer']['layout'] = 'vertical';
					$prodAry[$key]['footer']['spacing'] = 'sm';
					$prodAry[$key]['footer']['contents'][0]['type'] = 'button';
					$prodAry[$key]['footer']['contents'][0]['height'] = 'sm';
					$prodAry[$key]['footer']['contents'][0]['style'] = 'primary';
					$prodAry[$key]['footer']['contents'][0]['color'] = "#ff0000";
					$prodAry[$key]['footer']['contents'][0]['action']['type'] = 'postback';
					$prodAry[$key]['footer']['contents'][0]['action']['label'] = '今日無服務';
					$prodAry[$key]['footer']['contents'][0]['action']['data'] = " ";
				} else {
					/*
					$prodAry[$key][actions] = array(
						array(
							"type" => "postback",
							"label" => "預約",
							"data" => "action=reservation&item=reservationDoctor&prodid=".$prodInfo[web_class_id]."&csubject=".$prodInfo[csubject]."&subject=".$prodInfo[subject],
						),
						array(
							"type" => "uri",
							"label" => "查看詳情",
							"uri" => "https://tw.yahoo.com",
						),
					);
					*/
					$prodAry[$key]['footer']['type'] = 'box';
					$prodAry[$key]['footer']['layout'] = 'vertical';
					$prodAry[$key]['footer']['spacing'] = 'sm';
					
					$sqlTeam = "SELECT SQL_CALC_FOUND_ROWS * FROM web_x_team where ifShow = '1' ORDER BY asort ASC";
					$pdoTeam = $pdoDB->prepare($sqlTeam);
					$pdoTeam->execute();
					$rowTeam = $pdoTeam->fetchAll(PDO::FETCH_ASSOC);
					foreach($rowTeam as $keyTeam => $valTeam) {
						$prodAry[$key]['footer']['contents'][$keyTeam]['type'] = 'button';
						$prodAry[$key]['footer']['contents'][$keyTeam]['height'] = 'sm';
						$prodAry[$key]['footer']['contents'][$keyTeam]['style'] = 'primary';
						$prodAry[$key]['footer']['contents'][$keyTeam]['color'] = "#444444";
						$prodAry[$key]['footer']['contents'][$keyTeam]['action']['type'] = 'postback';
						$prodAry[$key]['footer']['contents'][$keyTeam]['action']['label'] = $valTeam['subject']." - ".$valTeam['time']."HR";
						
						//$prodAry[$key]['footer']['contents'][$keyTeam]['action']['data'] = "action=reservation&item=reservationDoctor&prodid=".$prodInfo[web_class_id]."&csubject=".$prodInfo[csubject]."&subject=".$prodInfo[subject]."&xteam=".$valTeam[web_x_team_id];
						
						$prodAry[$key]['footer']['contents'][$keyTeam]['action']['data'] = "action=reservation&item=register&class=".$prodInfo[web_class_id]."&team=".$valTeam[web_x_team_id];
						
						file_put_contents("debug.txt", $prodAry[$key]['footer']['contents'][$keyTeam]['action']['data']."\r\n",FILE_APPEND);
					}	
				}	
				
				//echo count($doctorInfoAry);
				//$prodAry[$key][actions] = $doctorInfoAry;
				
	    	}
			/*
			$prodAry[$key]['footer']['contents'][count($prodAry[$key]['footer']['contents'])]['type'] = 'text';
			$prodAry[$key]['footer']['contents'][count($prodAry[$key]['footer']['contents'])]['text'] = "Design by aibitechcology";
			$prodAry[$key]['footer']['contents'][count($prodAry[$key]['footer']['contents'])]['wrap'] = true;
			$prodAry[$key]['footer']['contents'][count($prodAry[$key]['footer']['contents'])]['color'] = "#aaaaaa";
			$prodAry[$key]['footer']['contents'][count($prodAry[$key]['footer']['contents'])]['size'] = "xxs";
			$prodAry[$key]['footer']['contents'][count($prodAry[$key]['footer']['contents'])]['align'] = "end";
			*/
	    	
	    }
		
		
		if($total_pages > 1) {
			/*
			$prodAry[$lastKey + 1] = array(
				"title" => "更多產品",
				"text" => "更多產品",
				"actions" => array(
					array(
						"type" => "postback",
						"label" => "想看更多請點我",
						"data" => "action=more2&page=".($page+1),
					),
					array(
						"type" => "message",
						"label" => " ",
						"text" => " "
					),
					array(
						"type" => "message",
						"label" => " ",
						"text" => " "
					),
				),
				"thumbnailImageUrl" => $_SERVER['HTTP_X_FORWARDED_PROTO'].'://'.$_SERVER['HTTP_HOST'].'/uploadfiles/l/7210438951824.jpeg',
			);
			*/
			$prodAry[$key + 1] = array(
				"type" => "bubble",
				"body" => array(
					"type" => "box",
					"layout" => "vertical",
					"spacing" => "sm",
					"contents" => array(
						array(
							"type" => "button",
							"flex" => 1,
							"gravity" => "center",
							"action" => array(
								"type" => "postback",
								"label" => "想看更多請點我",
								"data" => "action=more2&cid=".$cid."&page=".($page+1),
								
							),
						)	
					)	
				)	
			);
		}
		echo "<pre>";
		print_r($prodAry);
		echo "</pre>";
		switch($content_type) {
		
			case "text" :
				$content_type = "文字訊息";
				
				$data = $prodAry;
				$result = array();
				foreach($data as $key => $row) {
					//if (mb_strpos($message, $row[title]) !== false) {
						array_push($result, $row);
					//}
				}
				
				$result = (!$result[0]) ? $data : $result;
				/*
				$data = array(
					"replyToken" => $reToken, 
					"messages" =>
						array(
							array(
								'type' => 'template',
								'altText' => '診療項目',
								'template' => array(
									'type' => 'carousel',
									'columns' => $result
								),
							),
						),
				);
				*/
				$data = array(
					"replyToken" => $reToken, 
					"messages" =>
						array(
							
							array(
								'type' => 'flex',
								'altText' => '服務項目',
								'contents' => array(
									'type' => 'carousel',
									'contents' => $result
								),
							),
						),
				);
				//file_put_contents("template.txt", json_encode($data)."\r\n",FILE_APPEND);
				break;
				
			
				
			default:
				$content_type = "未知訊息";
				break;
	   	}
		
		$context = stream_context_create(array(
		"http" => array("method" => "POST", "header" => implode(PHP_EOL, $header), "content" => json_encode($data), "ignore_errors" => true)
		));
		//file_put_contents("debug.txt", $url."\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($data)."\r\n",FILE_APPEND);
		//$log = file_get_contents($url, false, $context);
		//file_put_contents("debug.txt", json_encode($log)."\r\n",FILE_APPEND);
		
		//curl設定
		$headers = array(
			"Content-Type: application/json; charser=UTF-8",
			"Authorization: Bearer {" . $channel_access_token . "}"
		);
			 
		//curl
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		//如果失敗可以印出 $result 看錯誤訊息
		$result = curl_exec($ch);
		//file_put_contents("debug.txt", "==========================\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($result)."\r\n",FILE_APPEND);
		
		curl_close($ch);
	}
	
	//全部產品
	function reply2($content_type, $message, $page) {

	 	global $header, $reToken, $receive, $channel_access_token, $DB, $pdoDB, $from;
	 	
		$url = "https://api.line.me/v2/bot/message/reply";
		
		$data = ["replyToken" => $reToken, "messages" => array(["type" => "text", "text" => $message])];
		
		$sql = "SELECT COUNT(*) as Rows FROM web_product where web_product.ifShow = '1' ORDER BY web_product_id DESC";
		$pdo = $pdoDB->prepare($sql);
	    $pdo->execute();
	    $row = $pdo->fetch(PDO::FETCH_ASSOC);

		$Init_Page = '7';
		
		$total_pages = ceil($row[Rows]/$Init_Page);	//計算總頁數
		if ($page>$total_pages) $page = 1;		//修正輸入過大的頁數
		

	
		$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM web_product where web_product.ifShow = '1' ORDER BY web_product_id DESC";
		$sql .= " limit ".($page-1) * $Init_Page.", ".$Init_Page;
		$pdo = $pdoDB->prepare($sql);
	    $pdo->execute();
	    $row = $pdo->fetchAll(PDO::FETCH_ASSOC);
		
		end($row);
		$lastKey = key($row);
	    
	    $prodAry = array();
	    $targetKey = array(
	    	//'web_product_id',
	    	'subject',
	    	//'price_member',
	    	'Files',

	    );
	    foreach($row as $key => $prodInfo) {
	    	foreach($prodInfo as $key2 => $prod) {
	    		if(!in_array($key2, $targetKey)) {
	    			continue;
	    		}
	    		if($key2 == 'subject') {
	    			$prod = $prod." $".number_format($prodInfo[price_member]);
	    		}
	    		if($key2 == 'Files') {
	    			$prod = $_SERVER['HTTP_X_FORWARDED_PROTO'].'://'.$_SERVER['HTTP_HOST'].'/uploadfiles/l/'.str_replace('/', '', $prod);
	    		}
	    		$prodAry[$key][str_replace($targetKey, array('title','thumbnailImageUrl'), $key2)] = $prod;
	    		$prodAry[$key][text] = "$".number_format($prodInfo[price_member]);
	    		$prodAry[$key][actions] = array(
	    			array(
	    				'type' => 'uri',
	    				'label' => '查看詳情',
	    				'uri' => setWeb()."/product".$prodInfo[web_product_id].".html",
	    			),
					/*
	    			array(
	    				'type' => 'uri',
	    				'label' => '立即訂購',
	    				'uri' => setWeb()."/product".$prodInfo[web_product_id].".html",
	    			),
					*/
	    			array(
	    				'type' => 'uri',
	    				'label' => '加入購物車',
	    				'uri' => setWeb()."/lineApi.php?web_product_id=".$prodInfo[web_product_id]."&lineId=".$from,
	    			),
	    		);
	    	}
	    	
	    }
		$prodAry[$lastKey + 1] = array(
			"title" => "更多產品",
			"text" => "更多產品",
			"actions" => array(
				array(
					"type" => "postback",
					"label" => "想看更多請點我",
					"data" => "action=more&page=".($page+1),
				),
				/*
				array(
					"type" => "message",
					"label" => " ",
					"text" => " "
				),
				*/
				array(
					"type" => "message",
					"label" => " ",
					"text" => " "
				),
			),
			"thumbnailImageUrl" => $_SERVER['HTTP_X_FORWARDED_PROTO'].'://'.$_SERVER['HTTP_HOST'].'/uploadfiles/l/7210438951824.jpeg',
		);
		
		
		switch($content_type) {
		
			case "text" :
				$content_type = "文字訊息";
				/*
				$result = array();
				foreach ($foodData['feed']['entry'] as $item) {
					$keywords = explode(',', $item['gsx$keyword']['$t']);

					foreach ($keywords as $keyword) {
						if (mb_strpos($message, $keyword) !== false) {
							$candidate = array(
								'thumbnailImageUrl' => $item['gsx$photourl']['$t'],
								'title' => $item['gsx$title']['$t'],
								'text' => $item['gsx$title']['$t'],
								'actions' => array(
									array(
										'type' => 'uri',
										'label' => '查看詳情',
										'uri' => $item['gsx$url']['$t'],
										),
									),
								);
							array_push($result, $candidate);
						}
					}
				}
				//file_put_contents("template.txt", json_encode($result)."\r\n",FILE_APPEND);
				*/
				$data = $prodAry;
				
				$data2 = array(
					array(
						"thumbnailImageUrl" => $_SERVER['HTTP_X_FORWARDED_PROTO'].'://'.$_SERVER['HTTP_HOST'].'/uploadfiles/l/20170713160229_78724.jpg',
						"title" => "玻尿酸鎖水保濕面膜1",
						"text" => "保濕.緊緻.光澤注入多種美白元素，結合維他命B3及小分子玻尿酸，給肌膚滿滿的水潤輕薄絲面膜材質給您舒適、服貼、透氣的感受",
						"actions" => array(
							array(
								"type" => "uri",
								"label" => "查看詳情",
								"uri" => "https://www.edison1525.net/products.html"
							),
							array(
								"type" => "uri",
								"label" => "立即訂購",
								"uri" => "https://www.edison1525.net/product.html"
							),
						)
					),

					array(
						"thumbnailImageUrl" => "https://www.edison1525.net/uploadfiles/l/20170713160401_42872.jpg",
						"title" => "九胜肽美白透亮面膜2",
						"text" => "美白.透亮.緊緻九胜肽+珍珠萃取，全面煥白您的肌膚，輕薄絲面膜材質給您舒適、服貼、透氣的感受",
						"actions" => array(
							array(
								"type" => "uri",
								"label" => "查看詳情",
								"uri" => "https://www.edison1525.net/products2.html"
							),
							array(
								"type" => "uri",
								"label" => "立即訂購",
								"uri" => "https://www.edison1525.net/product2.html"
							),
						),
					),

				);
				
				
				$result = array();
				foreach($data as $key => $row) {
					//if (mb_strpos($message, $row[title]) !== false) {
						array_push($result, $row);
					//}
				}
				
				$result = (!$result[0]) ? $data : $result;
				
				$data = array(
					"replyToken" => $reToken, 
					"messages" =>
						array(
						/*	
							array(
                                'type' => 'text',
                                'text' => $message,
                            ),
						*/	
							array(
								'type' => 'template',
								'altText' => 'NH面膜',
								'template' => array(
									'type' => 'carousel',
									'columns' => $result
								),
							),
						),
				);
				//file_put_contents("template.txt", json_encode($data)."\r\n",FILE_APPEND);
				break;
				
			case "image" :
				$content_type = "圖片訊息";
				$message = getObjContent("jpeg");   // 讀取圖片內容
				$data = ["replyToken" => $reToken, "messages" => array(["type" => "image", "originalContentUrl" => $message, "previewImageUrl" => $message])];
				break;
				
			case "video" :
				$content_type = "影片訊息";
				$message = getObjContent("mp4");   // 讀取影片內容
				$data = ["replyToken" => $reToken, "messages" => array(["type" => "video", "originalContentUrl" => $message, "previewImageUrl" => $message])];
				break;
				
			case "audio" :
				$content_type = "語音訊息";
				$message = getObjContent("mp3");   // 讀取聲音內容
				$data = ["replyToken" => $reToken, "messages" => array(["type" => "audio", "originalContentUrl" => $message[0], "duration" => $message[1]])];
				break;
				
			case "location" :
				$content_type = "位置訊息";
				$title = $receive->events[0]->message->title;
				$address = $receive->events[0]->message->address;
				$latitude = $receive->events[0]->message->latitude;
				$longitude = $receive->events[0]->message->longitude;
				$data = ["replyToken" => $reToken, "messages" => array(["type" => "location", "title" => $title, "address" => $address, "latitude" => $latitude, "longitude" => $longitude])];
				break;
				
			case "sticker" :
				$content_type = "貼圖訊息";
				$packageId = $receive->events[0]->message->packageId;
				$stickerId = $receive->events[0]->message->stickerId;
				$data = ["replyToken" => $reToken, "messages" => array(["type" => "sticker", "packageId" => $packageId, "stickerId" => $stickerId])];
				break;
				
			default:
				$content_type = "未知訊息";
				break;
	   	}
		
		$context = stream_context_create(array(
		"http" => array("method" => "POST", "header" => implode(PHP_EOL, $header), "content" => json_encode($data), "ignore_errors" => true)
		));
		//file_put_contents("debug.txt", $url."\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($data)."\r\n",FILE_APPEND);
		//$log = file_get_contents($url, false, $context);
		//file_put_contents("debug.txt", json_encode($log)."\r\n",FILE_APPEND);
		
		//curl設定
		$headers = array(
			"Content-Type: application/json; charser=UTF-8",
			"Authorization: Bearer {" . $channel_access_token . "}"
		);
			 
		//curl
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		//如果失敗可以印出 $result 看錯誤訊息
		$result = curl_exec($ch);
		//file_put_contents("debug.txt", "==========================\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($result)."\r\n",FILE_APPEND);
		
		curl_close($ch);
	}
	
	//全部產品
	function reply3($content_type, $message, $page) {

	 	global $header, $reToken, $receive, $channel_access_token, $DB, $pdoDB, $from;
	 	
		$url = "https://api.line.me/v2/bot/message/reply";
		
		$data = ["replyToken" => $reToken, "messages" => array(["type" => "text", "text" => $message])];
		
		/*
		$ch = curl_init();  
		curl_setopt($ch,CURLOPT_URL,'https://spreadsheets.google.com/feeds/list/1tQCaj3LUVwH0tBuPrfBY2dOJuF-qzpYEdOqGdNvJRLc/od6/public/values?alt=json');
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		//  curl_setopt($ch,CURLOPT_HEADER, false); 
		$json = curl_exec($ch);
		curl_close($ch);
		$foodData = json_decode($json, true);
		*/
		
		$sql = "SELECT COUNT(*) as Rows FROM web_team where web_team.web_x_team_id != '3' and web_team.ifShow = '1' ORDER BY asort ASC";
		$pdo = $pdoDB->prepare($sql);
	    $pdo->execute();
	    $row = $pdo->fetch(PDO::FETCH_ASSOC);

		$Init_Page = '7';
		
		$total_pages = ceil($row[Rows]/$Init_Page);	//計算總頁數
		if ($page>$total_pages) $page = 1;		//修正輸入過大的頁數
		

	
		$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM web_team where web_team.web_x_team_id != '3' and web_team.ifShow = '1' ORDER BY asort ASC";
		$sql .= " limit ".($page-1) * $Init_Page.", ".$Init_Page;
		$pdo = $pdoDB->prepare($sql);
	    $pdo->execute();
	    $row = $pdo->fetchAll(PDO::FETCH_ASSOC);
		
		end($row);
		$lastKey = key($row);
	    
	    $prodAry = array();
	    $targetKey = array(
	    	//'web_product_id',
	    	'subject',
	    	//'price_member',
	    	'Files',

	    );
	    foreach($row as $key => $prodInfo) {
	    	foreach($prodInfo as $key2 => $prod) {
	    		if(!in_array($key2, $targetKey)) {
	    			continue;
	    		}
	    		if($key2 == 'subject') {
	    			//$prod = $prod." $".number_format($prodInfo[price_member]);
					$prod = $prod;
	    		}
	    		if($key2 == 'Files') {
	    			$prod = $_SERVER['HTTP_X_FORWARDED_PROTO'].'://'.$_SERVER['HTTP_HOST'].'/uploadfiles/l/'.str_replace('/', '', $prod);
	    		}
	    		$prodAry[$key][str_replace($targetKey, array('title','thumbnailImageUrl'), $key2)] = $prod;
	    		$prodAry[$key][text] = 'test';
	    		$prodAry[$key][actions] = array(
	    			array(
	    				'type' => 'uri',
	    				'label' => '查看詳情',
	    				'uri' => setWeb()."/product".$prodInfo[web_product_id].".html",
	    			),
					/*
	    			array(
	    				'type' => 'uri',
	    				'label' => '立即訂購',
	    				'uri' => setWeb()."/product".$prodInfo[web_product_id].".html",
	    			),
					*/
	    			array(
						"type" => "message",
						"label" => " ",
						"text" => " "
					),
	    		);
	    	}
	    	
	    }
		$prodAry[$lastKey + 1] = array(
			"title" => "更多資訊",
			"text" => "更多資訊",
			"actions" => array(
				array(
					"type" => "postback",
					"label" => "想看更多請點我",
					"data" => "action=more3&page=".($page+1),
				),
				/*
				array(
					"type" => "message",
					"label" => " ",
					"text" => " "
				),
				*/
				array(
					"type" => "message",
					"label" => " ",
					"text" => " "
				),
			),
			"thumbnailImageUrl" => $_SERVER['HTTP_X_FORWARDED_PROTO'].'://'.$_SERVER['HTTP_HOST'].'/uploadfiles/l/7210438951824.jpeg',
		);
		
		
		switch($content_type) {
		
			case "text" :
				$content_type = "文字訊息";
				/*
				$result = array();
				foreach ($foodData['feed']['entry'] as $item) {
					$keywords = explode(',', $item['gsx$keyword']['$t']);

					foreach ($keywords as $keyword) {
						if (mb_strpos($message, $keyword) !== false) {
							$candidate = array(
								'thumbnailImageUrl' => $item['gsx$photourl']['$t'],
								'title' => $item['gsx$title']['$t'],
								'text' => $item['gsx$title']['$t'],
								'actions' => array(
									array(
										'type' => 'uri',
										'label' => '查看詳情',
										'uri' => $item['gsx$url']['$t'],
										),
									),
								);
							array_push($result, $candidate);
						}
					}
				}
				//file_put_contents("template.txt", json_encode($result)."\r\n",FILE_APPEND);
				*/
				$data = $prodAry;
				
				
				$result = array();
				foreach($data as $key => $row) {
					//if (mb_strpos($message, $row[title]) !== false) {
						array_push($result, $row);
					//}
				}
				
				$result = (!$result[0]) ? $data : $result;
				
				$data = array(
					"replyToken" => $reToken, 
					"messages" =>
						array(
						/*	
							array(
                                'type' => 'text',
                                'text' => $message,
                            ),
						*/	
							array(
								'type' => 'template',
								'altText' => '醫師團隊',
								'template' => array(
									'type' => 'carousel',
									'columns' => $result
								),
							),
						),
				);
				file_put_contents("template.txt", json_encode($data)."\r\n",FILE_APPEND);
				break;
				
			default:
				$content_type = "未知訊息";
				break;
	   	}
		
		$context = stream_context_create(array(
		"http" => array("method" => "POST", "header" => implode(PHP_EOL, $header), "content" => json_encode($data), "ignore_errors" => true)
		));
		//file_put_contents("debug.txt", $url."\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($data)."\r\n",FILE_APPEND);
		//$log = file_get_contents($url, false, $context);
		//file_put_contents("debug.txt", json_encode($log)."\r\n",FILE_APPEND);
		
		//curl設定
		$headers = array(
			"Content-Type: application/json; charser=UTF-8",
			"Authorization: Bearer {" . $channel_access_token . "}"
		);
			 
		//curl
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		//如果失敗可以印出 $result 看錯誤訊息
		$result = curl_exec($ch);
		//file_put_contents("debug.txt", "==========================\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($result)."\r\n",FILE_APPEND);
		
		curl_close($ch);
	}
	
	//醫師團隊
	function reply3_2($content_type, $message, $page, $id) {

	 	global $header, $reToken, $receive, $channel_access_token, $DB, $pdoDB, $from;
	 	
		$url = "https://api.line.me/v2/bot/message/reply";
		
		$data = ["replyToken" => $reToken, "messages" => array(["type" => "text", "text" => $message])];
		
		/*
		$ch = curl_init();  
		curl_setopt($ch,CURLOPT_URL,'https://spreadsheets.google.com/feeds/list/1tQCaj3LUVwH0tBuPrfBY2dOJuF-qzpYEdOqGdNvJRLc/od6/public/values?alt=json');
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		//  curl_setopt($ch,CURLOPT_HEADER, false); 
		$json = curl_exec($ch);
		curl_close($ch);
		$foodData = json_decode($json, true);
		*/
		
		$sql = "
			SELECT 
				COUNT(*) as Rows
			FROM 
				(SELECT * from web_shift WHERE web_shift.web_class_id = '".$id."' group by web_team_id) as main 
			left join 
				(SELECT * from web_team) as teams
			on 
				main.web_team_id = teams.web_team_id
		";
		$pdo = $pdoDB->prepare($sql);
	    $pdo->execute();
	    $row = $pdo->fetch(PDO::FETCH_ASSOC);
		
		$Init_Page = '7';
		$moreFlage = 0;
		
		$total_pages = ceil($row[Rows]/$Init_Page);	//計算總頁數
		if ($page>$total_pages) $page = 1;		//修正輸入過大的頁數
		
		$moreFlage = ($row[Rows] > $Init_Page) ? 1 : 0;
	
		$sql = "
			SELECT 
				*
			FROM 
				(SELECT * from web_shift WHERE web_shift.web_class_id = '".$id."' group by web_team_id) as main 
			left join 
				(SELECT * from web_team) as teams
			on 
				main.web_team_id = teams.web_team_id	
		";
		$sql .= " limit ".($page-1) * $Init_Page.", ".$Init_Page;
		$pdo = $pdoDB->prepare($sql);
	    $pdo->execute();
	    $row = $pdo->fetchAll(PDO::FETCH_ASSOC);

		end($row);
		$lastKey = key($row);
	    
	    $prodAry = array();
	    $targetKey = array(
	    	//'web_team_id',
	    	'subject',
	    	//'price_member',
	    	'Files',

	    );
	    foreach($row as $key => $prodInfo) {
	    	foreach($prodInfo as $key2 => $prod) {
	    		if(!in_array($key2, $targetKey)) {
	    			continue;
	    		}
	    		if($key2 == 'subject') {
	    			//$prod = $prod." $".number_format($prodInfo[price_member]);
					$prod = $prod;
	    		}
	    		if($key2 == 'Files') {
	    			$prod = $_SERVER['HTTP_X_FORWARDED_PROTO'].'://'.$_SERVER['HTTP_HOST'].'/uploadfiles/l/'.str_replace('/', '', $prod);
					$Files = $_SERVER['HTTP_X_FORWARDED_PROTO'].'://'.$_SERVER['HTTP_HOST'].'/uploadfiles/l/'.str_replace('/', '', $prod);
	    		}
				
				$prodAry[$key]['type'] = 'bubble';
				$prodAry[$key]['styles']['body']['backgroundColor'] = '#FFFFFF';
				$prodAry[$key]['styles']['footer']['backgroundColor'] = '#FFFFFF';
				$prodAry[$key]['hero']['type'] = 'image';
				$prodAry[$key]['hero']['size'] = 'full';
				$prodAry[$key]['hero']['aspectRatio'] = '20:13';
				$prodAry[$key]['hero']['aspectMode'] = 'cover';
				$prodAry[$key]['hero']['url'] = $_SERVER['HTTP_X_FORWARDED_PROTO'].'://'.$_SERVER['HTTP_HOST'].'/uploadfiles/l/'.str_replace('/', '', $prodInfo['Files']);
				
				$prodAry[$key]['body']['type'] = 'box';
				$prodAry[$key]['body']['layout'] = 'vertical';
				$prodAry[$key]['body']['spacing'] = 'sm';
				$prodAry[$key]['body']['contents'][0]['type'] = 'text';
				$prodAry[$key]['body']['contents'][0]['text'] = $prodInfo['subject'];
				$prodAry[$key]['body']['contents'][0]['wrap'] = true;
				$prodAry[$key]['body']['contents'][0]['weight'] = "bold";
				$prodAry[$key]['body']['contents'][0]['size'] = "xl";
				$prodAry[$key]['body']['contents'][1]['type'] = 'text';
				$prodAry[$key]['body']['contents'][1]['text'] = "設計師";
				$prodAry[$key]['body']['contents'][1]['wrap'] = true;
				$prodAry[$key]['body']['contents'][1]['weight'] = "bold";
				$prodAry[$key]['body']['contents'][1]['size'] = "sm";
				/*
	    		$prodAry[$key][str_replace($targetKey, array('title','thumbnailImageUrl'), $key2)] = $prod;
	    		$prodAry[$key][text] = '設計師';
	    		$prodAry[$key][actions] = array(
	    			
	    			array(
	    				'type' => 'postback',
	    				'label' => '預約',
	    				"data" => "action=reservation&item=register&class=".$prodInfo[web_class_id]."&team=".$prodInfo[web_team_id],
	    			),
					
	    			array(
						"type" => "message",
						"label" => " ",
						"text" => " "
					),
	    		);
				*/
				$prodAry[$key]['footer']['type'] = 'box';
				$prodAry[$key]['footer']['layout'] = 'vertical';
				$prodAry[$key]['footer']['spacing'] = 'sm';
				$prodAry[$key]['footer']['contents'][0]['type'] = 'button';
				$prodAry[$key]['footer']['contents'][0]['height'] = 'sm';
				$prodAry[$key]['footer']['contents'][0]['style'] = 'primary';
				$prodAry[$key]['footer']['contents'][0]['color'] = "#444444";
				$prodAry[$key]['footer']['contents'][0]['action']['type'] = 'postback';
				$prodAry[$key]['footer']['contents'][0]['action']['label'] = '預約';
				$prodAry[$key]['footer']['contents'][0]['action']['data'] = "action=reservation&item=register&class=".$prodInfo[web_class_id]."&team=".$prodInfo[web_team_id];
				
				$prodAry[$key]['footer']['contents'][1]['type'] = 'text';
				$prodAry[$key]['footer']['contents'][1]['text'] = "Design by aibitechcology";
				$prodAry[$key]['footer']['contents'][1]['wrap'] = true;
				$prodAry[$key]['footer']['contents'][1]['color'] = "#aaaaaa";
				$prodAry[$key]['footer']['contents'][1]['size'] = "xxs";
				$prodAry[$key]['footer']['contents'][1]['align'] = "end";
	    	}
	    	
	    }
		
		if($moreFlage) {
			/*
			$prodAry[$lastKey + 1] = array(
				"title" => "更多資訊",
				"text" => "更多資訊",
				"actions" => array(
					array(
						"type" => "postback",
						"label" => "想看更多請點我",
						"data" => "action=more3&page=".($page+1),
					),
					
					array(
						"type" => "message",
						"label" => " ",
						"text" => " "
					),
				),
				"thumbnailImageUrl" => $_SERVER['HTTP_X_FORWARDED_PROTO'].'://'.$_SERVER['HTTP_HOST'].'/uploadfiles/l/7210438951824.jpeg',
			);
			*/
			$prodAry[$key + 1] = array(
				"type" => "bubble",
				"body" => array(
					"type" => "box",
					"layout" => "vertical",
					"spacing" => "sm",
					"contents" => array(
						array(
							"type" => "button",
							"flex" => 1,
							"gravity" => "center",
							"action" => array(
								"type" => "postback",
								"label" => "想看更多請點我",
								"data" => "action=more3&page=".($page+1),
								
							),
						)	
					)	
				)	
			);
		}
		
		switch($content_type) {
		
			case "text" :
				$content_type = "文字訊息";
				
				$data = $prodAry;
				
				
				$result = array();
				foreach($data as $key => $row) {
					//if (mb_strpos($message, $row[title]) !== false) {
						array_push($result, $row);
					//}
				}
				
				$result = (!$result[0]) ? $data : $result;
				/*
				$data = array(
					"replyToken" => $reToken, 
					"messages" =>
						array(
						
							array(
								'type' => 'template',
								'altText' => '醫師團隊',
								'template' => array(
									'type' => 'carousel',
									'columns' => $result
								),
							),
						),
				);
				*/
				$data = array(
					"replyToken" => $reToken, 
					"messages" =>
						array(
							
							array(
								'type' => 'flex',
								'altText' => '設計團隊',
								'contents' => array(
									'type' => 'carousel',
									'contents' => $result
								),
							),
						),
				);
				//file_put_contents("template.txt", json_encode($data)."\r\n",FILE_APPEND);
				break;
				
			default:
				$content_type = "未知訊息";
				break;
	   	}
		
		$context = stream_context_create(array(
		"http" => array("method" => "POST", "header" => implode(PHP_EOL, $header), "content" => json_encode($data), "ignore_errors" => true)
		));
		//file_put_contents("debug.txt", $url."\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($data)."\r\n",FILE_APPEND);
		//$log = file_get_contents($url, false, $context);
		//file_put_contents("debug.txt", json_encode($log)."\r\n",FILE_APPEND);
		
		//curl設定
		$headers = array(
			"Content-Type: application/json; charser=UTF-8",
			"Authorization: Bearer {" . $channel_access_token . "}"
		);
			 
		//curl
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		//如果失敗可以印出 $result 看錯誤訊息
		$result = curl_exec($ch);
		//file_put_contents("debug.txt", "==========================\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($result)."\r\n",FILE_APPEND);
		
		curl_close($ch);
	}
	
	//智慧沙龍
	function reply3_3($content_type, $message, $page, $class, $team) {

	 	global $header, $reToken, $receive, $channel_access_token, $DB, $pdoDB, $from;
	 	
		$url = "https://api.line.me/v2/bot/message/reply";
		
		$data = ["replyToken" => $reToken, "messages" => array(["type" => "text", "text" => $message])];
		/*
		$sql = "
			SELECT 
				COUNT(*) as Rows
			FROM 
				(SELECT * from web_shift WHERE web_shift.web_class_id = '".$class."' and web_shift.web_team_id = '".$team."') as main 
			left join 
				(SELECT * from web_team) as teams
			on 
				main.web_team_id = teams.web_team_id
		";
		$pdo = $pdoDB->prepare($sql);
	    $pdo->execute();
	    $row = $pdo->fetch(PDO::FETCH_ASSOC);
		*/
		$Init_Page = '7';
		$moreFlage = 1;
		/*
		$total_pages = ceil($row[Rows]/$Init_Page);	//計算總頁數
		if ($page>$total_pages) $page = 1;		//修正輸入過大的頁數
		
		$moreFlage = ($row[Rows] > $Init_Page) ? 1 : 0;
		*/
		$timeSql = "SELECT * from web_time WHERE web_time.ifShow = '1' ORDER BY web_time.asort ASC";
		$pdo = $pdoDB->prepare($timeSql);
	    $pdo->execute();
	    $timeRow = $pdo->fetchAll(PDO::FETCH_ASSOC);
	
		$sql = "
			SELECT 
				main.web_class_id,
				main.web_x_class_id,
				main.ifShow,
				main.lineInfo,
				main.content,
				main.subject,
				main.lineInfo as mainInfo,
				x_class.subject as web_x_class_subject,
				teams.subject as teamSubject,
				teams.web_x_team_id as teamXid
			FROM 
				web_class as main 
			Left Join
				web_x_class x_class
			ON
				x_class.web_x_class_id = main.web_x_class_id
			join 
				(SELECT * from web_x_team where web_x_team_id = '".$team."') as teams
			WHERE main.web_class_id = '".$class."'		
		";
		//$sql .= " limit ".($page-1) * $Init_Page.", ".$Init_Page;
		$pdo = $pdoDB->prepare($sql);
	    $pdo->execute();
	    $row = $pdo->fetchAll(PDO::FETCH_ASSOC);
		/*
		echo "<pre>";
		print_r($row);
		echo "</pre>";
		*/
		//echo date('N');
		$start = ($page == '1') ? '1' : ((($page - 1) * (6)) + ($page - 1)) + date('N');
		$end = ($page == '1') ? '7' : $start + 6;
		$timeAry = array();
		$test = array();
		foreach($row as $key => $timeList) {
			//$contentAry = array(1,2,3,4,5,6,7);
			//foreach($contentAry as $key2 => $contentVal) {
			for($contentVal = $start; $contentVal <= $end; $contentVal++) {
				$timeAry[$contentVal][] = $timeList;
			}
			
			$lineInfoAry = explode('#####', $timeList['lineInfo']);
			foreach($lineInfoAry as $$lineInfoKey => $lineInfoVal) {
				$lineInfoRowAry[] = explode('@#@', $lineInfoVal);
			}
		}
		
		foreach($lineInfoRowAry as $key => $lineInfoAry) {
			$lineInfo[$lineInfoAry['2']][] = $lineInfoAry['1'];
		}
	
		end($timeAry);
		$lastKey = key($timeAry);
	    $prodAry = array();
	    $targetKey = array(
	    	//'web_team_id',
	    	'subject',
	    	//'price_member',
	    	'Files',

	    );
		/*
		echo "<pre>";
		print_r($timeRow);
		echo "</pre>";
		*/
		/*
		$_timeAry = array(
			'10:30~11:30',
			'11:30~12:30',
			'12:30~13:30',
			'13:30~14:30',
			'14:30~15:30',
			'15:30~16:30',
			'16:30~17:30',
			'17:30~18:30',
			'18:30~19:30',
		);
		*/
		$_timeAry = array(
			'10:30~11:00',
			'11:00~11:30',
			'11:30~12:00',
			'12:00~12:30',
			'12:30~13:00',
			'13:00~13:30',
			'13:30~14:00',
			'14:00~14:30',
			'14:30~15:00',
			'15:00~15:30',
			'15:30~16:00',
			'16:00~16:30',
			'16:30~17:00',
			'17:00~17:30',
			'17:30~18:00',
			'18:00~18:30',
			'18:30~19:00',
			'19:00~19:30',
		);
		$registerTimeRow = array();
		$isSql = "
			SELECT 
				main.*,
				xteam.subject as xteamSubject,
				xteam.time as xteamTime
			from 
				web_x_register as main
			left join
				web_x_team xteam
			on
				xteam.web_x_team_id = main.web_x_team_id
			WHERE 
				main.registerDate >= '".date('Y-m-d')."'
			AND
				main.paymentstatus != '取消' 
			AND
				main.paymentstatus != '已報到'
			AND
				main.paymentstatus != '已服務'	
		";
		//echo $isSql;
		$pdo3 = $pdoDB->prepare($isSql);
		$pdo3->execute();
		$registerRow = $pdo3->fetchAll(PDO::FETCH_ASSOC);
		
		foreach($registerRow as $registerKey => $registerVal) {
			//echo array_search(substr($registerVal['subject'],16), $_timeAry);
			if(array_search(substr($registerVal['subject'],16), $_timeAry) >= '0') {
				if($_timeAry[array_search(substr($registerVal['subject'],16), $_timeAry)] == substr($registerVal['subject'],16)) {
					$registerTimeRow[$registerVal['web_time_id']][$registerVal['registerDate']][] = substr($registerVal['subject'],16);
					if($registerVal['xteamTime'] >= 1) {
						for($i = 1; $i <= $registerVal['xteamTime'] * 2; $i++) {
							$searchKey = array_search(substr($registerVal['subject'],16), $_timeAry) + ($i - 1);
							$registerTimeRow[$registerVal['web_time_id']][$registerVal['registerDate']][] = $_timeAry[$searchKey];
						}	
					}
				}	
				
			}
		}
		//echo date('N')."</br>";
		//for($key = 1; $key <= 7; $key++) {
	    foreach($timeAry as $key => $prodInfo) {
			$_day = ($key < date('N')) ? '+'.ceil((7 - date('N')) + $key) : '+'.ceil($key - date('N'));
			$keyDay = $key - date('N');
			$keyDay = ($keyDay < 0) ? ceil(7 - abs($keyDay)): $keyDay;
			$dayName = array(
				1 => '一', 2 => '二', 3 => '三', 4 => '四', 5 => '五', 6 => '六', 7 => '日',
			);
			
			
	    	foreach($prodInfo as $key2 => $prod) {
				//$teamRestDayAry = explode(',', $prod['teamRestDay']);
		/*	
				$teamRestDayAry = explode('@#@', $prod['mainInfo']);
				echo "<pre>";
				print_r($teamRestDayAry);
				echo "</pre>";
		*/	
				//有設休診時間
				//echo $prod['teamRestDay']."===".date('Y-m-d', strtotime($_day.'day'));
	    		if(!in_array(date('Y-m-d', strtotime($_day.'day')), $teamRestDayAry)) {
					
					/*
					$prodAry[$keyDay][title] = date('Y-m-d', strtotime($_day.'day'))."(".$dayName[date('N', strtotime($_day.'day'))].")";
					$prodAry[$keyDay][text] = '請選擇時段：';
					
					$keyT = array_search($prod['subject'], array_column($timeRow, 'subject'));
					$prodAry[$keyDay][actions][$keyT]['type'] = 'postback';
					$prodAry[$keyDay][actions][$keyT]['label'] = $prod['subject'];
					$prodAry[$keyDay][actions][$keyT]['data'] = 'action=reservation&item=checkPerson&shiftId='.$prod['web_shift_id']."&date=".$prodAry[$keyDay]['title']."-".$prod['subject']."&teamSubject=".$prod['teamSubject']."&classSubject=".$prod['classSubject'];
					*/
					
					$keyT = array_search($prod['subject'], array_column($timeRow, 'subject'));
					$prodAry[$keyDay]['type'] = 'bubble';
					$prodAry[$keyDay]['styles']['body']['backgroundColor'] = '#FFFFFF';
					$prodAry[$keyDay]['styles']['footer']['backgroundColor'] = '#FFFFFF';
					$prodAry[$keyDay]['body']['type'] = 'box';
					$prodAry[$keyDay]['body']['layout'] = 'vertical';
					$prodAry[$keyDay]['body']['spacing'] = 'sm';
					$prodAry[$keyDay]['body']['contents'][0]['type'] = 'text';
					$prodAry[$keyDay]['body']['contents'][0]['text'] = date('Y-m-d', strtotime($_day.'day'))."(".$dayName[date('N', strtotime($_day.'day'))].")";
					$prodAry[$keyDay]['body']['contents'][0]['wrap'] = true;
					$prodAry[$keyDay]['body']['contents'][0]['weight'] = "bold";
					$prodAry[$keyDay]['body']['contents'][0]['size'] = "xl";
					$prodAry[$keyDay]['body']['contents'][1]['type'] = 'text';
					$prodAry[$keyDay]['body']['contents'][1]['text'] = '請選擇預約時間：';
					$prodAry[$keyDay]['body']['contents'][1]['wrap'] = true;
					$prodAry[$keyDay]['body']['contents'][1]['weight'] = "bold";
					$prodAry[$keyDay]['body']['contents'][1]['size'] = "sm";
					
					$prodAry[$keyDay]['body']['contents'][2]['type'] = 'separator';
					$prodAry[$keyDay]['body']['contents'][2]['margin'] = 'lg';
					
					$prodAry[$keyDay]['footer']['type'] = 'box';
					$prodAry[$keyDay]['footer']['layout'] = 'vertical';
					$prodAry[$keyDay]['footer']['spacing'] = 'sm';
					foreach($_timeAry as $keyT => $_tiemVal) {
						//echo $lineInfo[date('Y-m-d', strtotime($_day.'day'))];
						if($lineInfo[date('Y-m-d', strtotime($_day.'day'))]) {
							if((in_array('1', $lineInfo[date('Y-m-d', strtotime($_day.'day'))]) && $keyT <= 9) || (in_array('3', $lineInfo[date('Y-m-d', strtotime($_day.'day'))]) && $keyT <= 9)) {
								$prodAry[$keyDay]['footer']['contents'][$keyT]['type'] = 'button';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['height'] = 'sm';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['style'] = 'primary';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['color'] = '#DCDCDC';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['type'] = 'message';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['label'] = substr($_tiemVal,0,5)."(休)";
								//$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['data'] = 'action=reservation&item=checkPerson&shiftId='.$prod['web_shift_id']."&date=".$prodAry[$keyDay]['body']['contents'][0]['text']."-".$_tiemVal."&teamSubject=".$prod['teamSubject']."&classSubject=".$prod['classSubject']."&timeId=".$prod['web_time_id'];
								
								$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['text'] = ' ';
								
							} else if((in_array('2', $lineInfo[date('Y-m-d', strtotime($_day.'day'))]) && $keyT >= 9) || (in_array('3', $lineInfo[date('Y-m-d', strtotime($_day.'day'))]) && $keyT >= 9)) {
								
								$prodAry[$keyDay]['footer']['contents'][$keyT]['type'] = 'button';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['height'] = 'sm';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['style'] = 'primary';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['color'] = '#DCDCDC';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['type'] = 'message';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['label'] = substr($_tiemVal,0,5)."(休)";
								//$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['data'] = 'action=reservation&item=checkPerson&shiftId='.$prod['web_shift_id']."&date=".$prodAry[$keyDay]['body']['contents'][0]['text']."-".$_tiemVal."&teamSubject=".$prod['teamSubject']."&classSubject=".$prod['classSubject']."&timeId=".$prod['web_time_id'];
								
								$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['text'] = ' ';
								
							} else {
								
								if(!in_array($_tiemVal, $registerTimeRow[$prod['web_class_id']][date('Y-m-d', strtotime($_day.'day'))])) {
									$prodAry[$keyDay]['footer']['contents'][$keyT]['type'] = 'button';
									$prodAry[$keyDay]['footer']['contents'][$keyT]['height'] = 'sm';
									$prodAry[$keyDay]['footer']['contents'][$keyT]['style'] = 'primary';
									$prodAry[$keyDay]['footer']['contents'][$keyT]['color'] = '#444444';
									$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['type'] = 'postback';
									$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['label'] = substr($_tiemVal,0,5);
									//$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['data'] = 'action=reservation&item=checkPerson&shiftId='.$prod['web_shift_id']."&date=".$prodAry[$keyDay]['body']['contents'][0]['text']."-".$_tiemVal."&teamSubject=".$prod['teamSubject']."&classSubject=".$prod['classSubject']."&timeId=".$prod['web_time_id'];
									
									$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['data'] = 'action=reservation&item=checkPerson&shiftId='.$prod['web_shift_id']."&date=".$prodAry[$keyDay]['body']['contents'][0]['text']."-".$_tiemVal."&teamSubject=".$prod['subject']."&classSubject=".$prod['teamSubject']."&timeId=".$prod['web_class_id']."&teamXid=".$prod['teamXid'];
									
									
								} else {
									$prodAry[$keyDay]['footer']['contents'][$keyT]['type'] = 'button';
									$prodAry[$keyDay]['footer']['contents'][$keyT]['height'] = 'sm';
									$prodAry[$keyDay]['footer']['contents'][$keyT]['style'] = 'primary';
									$prodAry[$keyDay]['footer']['contents'][$keyT]['color'] = '#ff0000';
									$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['type'] = 'message';
									$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['label'] = substr($_tiemVal,0,5)."(已有預約)";
									//$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['data'] = 'action=reservation&item=checkPerson&shiftId='.$prod['web_shift_id']."&date=".$prodAry[$keyDay]['body']['contents'][0]['text']."-".$_tiemVal."&teamSubject=".$prod['teamSubject']."&classSubject=".$prod['classSubject']."&timeId=".$prod['web_time_id'];
									
									$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['text'] = ' ';
								}
							}		
						} else {	
							if(!in_array($_tiemVal, $registerTimeRow[$prod['web_class_id']][date('Y-m-d', strtotime($_day.'day'))])) {
								$prodAry[$keyDay]['footer']['contents'][$keyT]['type'] = 'button';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['height'] = 'sm';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['style'] = 'primary';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['color'] = '#444444';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['type'] = 'postback';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['label'] = substr($_tiemVal,0,5);
								//$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['data'] = 'action=reservation&item=checkPerson&shiftId='.$prod['web_shift_id']."&date=".$prodAry[$keyDay]['body']['contents'][0]['text']."-".$_tiemVal."&teamSubject=".$prod['teamSubject']."&classSubject=".$prod['classSubject']."&timeId=".$prod['web_time_id'];
								
								$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['data'] = 'action=reservation&item=checkPerson&shiftId='.$prod['web_shift_id']."&date=".$prodAry[$keyDay]['body']['contents'][0]['text']."-".$_tiemVal."&teamSubject=".$prod['subject']."&classSubject=".$prod['teamSubject']."&timeId=".$prod['web_class_id']."&teamXid=".$prod['teamXid'];
								
								
							} else {
								$prodAry[$keyDay]['footer']['contents'][$keyT]['type'] = 'button';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['height'] = 'sm';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['style'] = 'primary';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['color'] = '#ff0000';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['type'] = 'message';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['label'] = substr($_tiemVal,0,5)."(已有預約)";
								//$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['data'] = 'action=reservation&item=checkPerson&shiftId='.$prod['web_shift_id']."&date=".$prodAry[$keyDay]['body']['contents'][0]['text']."-".$_tiemVal."&teamSubject=".$prod['teamSubject']."&classSubject=".$prod['classSubject']."&timeId=".$prod['web_time_id'];
								
								$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['text'] = ' ';
							}
						}	
					}	
					/*
					$prodAry[$keyDay]['footer']['contents'][$keyT+1]['type'] = 'text';
					$prodAry[$keyDay]['footer']['contents'][$keyT+1]['text'] = "Design by aibitechcology";
					$prodAry[$keyDay]['footer']['contents'][$keyT+1]['wrap'] = true;
					$prodAry[$keyDay]['footer']['contents'][$keyT+1]['color'] = "#aaaaaa";
					$prodAry[$keyDay]['footer']['contents'][$keyT+1]['size'] = "xxs";
					$prodAry[$keyDay]['footer']['contents'][$keyT+1]['align'] = "end";
					*/
					
					
				} else {
					/*
					$prodAry[$keyDay][title] = date('Y-m-d', strtotime($_day.'day'))."(".$dayName[date('N', strtotime($_day.'day'))].")";
					$prodAry[$keyDay][text] = '請選擇時段：';
					
					$keyT = array_search($prod['subject'], array_column($timeRow, 'subject'));
					$prodAry[$keyDay][actions][$keyT]['type'] = 'message';
					$prodAry[$keyDay][actions][$keyT]['label'] = $prod['subject']."";
					$prodAry[$keyDay][actions][$keyT]['text'] = " ";
					*/
					
					$keyT = array_search($prod['subject'], array_column($timeRow, 'subject'));
					$prodAry[$keyDay]['type'] = 'bubble';
					$prodAry[$keyDay]['styles']['body']['backgroundColor'] = '#FFFFFF';
					$prodAry[$keyDay]['styles']['footer']['backgroundColor'] = '#FFFFFF';
					$prodAry[$keyDay]['body']['type'] = 'box';
					$prodAry[$keyDay]['body']['layout'] = 'vertical';
					$prodAry[$keyDay]['body']['spacing'] = 'sm';
					$prodAry[$keyDay]['body']['contents'][0]['type'] = 'text';
					$prodAry[$keyDay]['body']['contents'][0]['text'] = date('Y-m-d', strtotime($_day.'day'))."(".$dayName[date('N', strtotime($_day.'day'))].")";
					$prodAry[$keyDay]['body']['contents'][0]['wrap'] = true;
					$prodAry[$keyDay]['body']['contents'][0]['weight'] = "bold";
					$prodAry[$keyDay]['body']['contents'][0]['size'] = "xl";
					$prodAry[$keyDay]['body']['contents'][1]['type'] = 'text';
					$prodAry[$keyDay]['body']['contents'][1]['text'] = '請選擇預約時間：';
					$prodAry[$keyDay]['body']['contents'][1]['wrap'] = true;
					$prodAry[$keyDay]['body']['contents'][1]['weight'] = "bold";
					$prodAry[$keyDay]['body']['contents'][1]['size'] = "sm";
					
					$prodAry[$keyDay]['body']['contents'][2]['type'] = 'separator';
					$prodAry[$keyDay]['body']['contents'][2]['margin'] = 'lg';
					
					$prodAry[$keyDay]['footer']['type'] = 'box';
					$prodAry[$keyDay]['footer']['layout'] = 'vertical';
					$prodAry[$keyDay]['footer']['spacing'] = 'sm';
					$prodAry[$keyDay]['footer']['contents'][$keyT]['type'] = 'button';
					$prodAry[$keyDay]['footer']['contents'][$keyT]['height'] = 'sm';
					$prodAry[$keyDay]['footer']['contents'][$keyT]['style'] = 'primary';
					$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['type'] = 'message';
					$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['label'] = $prod['urlsubject'].$prod['subject']."";
					$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['text'] = ' ';
					$prodAry[$keyDay]['footer']['contents'][$keyT]['color'] = "#ff0000";
					/*
					$prodAry[$keyDay]['footer']['contents'][$keyT+1]['type'] = 'text';
					$prodAry[$keyDay]['footer']['contents'][$keyT+1]['text'] = "Design by aibitechcology";
					$prodAry[$keyDay]['footer']['contents'][$keyT+1]['wrap'] = true;
					$prodAry[$keyDay]['footer']['contents'][$keyT+1]['color'] = "#aaaaaa";
					$prodAry[$keyDay]['footer']['contents'][$keyT+1]['size'] = "xxs";
					$prodAry[$keyDay]['footer']['contents'][$keyT+1]['align'] = "end";
					*/
				}	
				
	    	}
			//echo $keyT;
			//echo count($prodAry[$key][actions])."</br>";
			//if(count($prodAry[$keyDay][actions]) < 3 ) {
			if(count($prodAry[$keyDay]['footer']['contents']) < 3 ) {	
				foreach($timeRow as $timekey => $timeVal) {
					//if($prodAry[$keyDay][actions][$timekey]) {
					
					if($prodAry[$keyDay]['footer']['contents'][$timekey]) {	
						continue;
					}
					/*
					$keyT2 = array_search($timeVal['subject'], array_column($timeRow, 'subject'));
					$prodAry[$keyDay][actions][$keyT2]['type'] = 'message';
					$prodAry[$keyDay][actions][$keyT2]['label'] = $timeVal['subject']."(休診)";
					$prodAry[$keyDay][actions][$keyT2]['text'] = " ";
					*/
					
					$keyT2 = array_search($timeVal['subject'], array_column($timeRow, 'subject'));
					$prodAry[$keyDay]['type'] = 'bubble';
					$prodAry[$keyDay]['styles']['body']['backgroundColor'] = '#FFFFFF';
					$prodAry[$keyDay]['styles']['footer']['backgroundColor'] = '#FFFFFF';
					$prodAry[$keyDay]['body']['type'] = 'box';
					$prodAry[$keyDay]['body']['layout'] = 'vertical';
					$prodAry[$keyDay]['body']['spacing'] = 'sm';
					$prodAry[$keyDay]['body']['contents'][0]['type'] = 'text';
					$prodAry[$keyDay]['body']['contents'][0]['text'] = date('Y-m-d', strtotime($_day.'day'))."(".$dayName[date('N', strtotime($_day.'day'))].")";
					$prodAry[$keyDay]['body']['contents'][0]['wrap'] = true;
					$prodAry[$keyDay]['body']['contents'][0]['weight'] = "bold";
					$prodAry[$keyDay]['body']['contents'][0]['size'] = "xl";
					$prodAry[$keyDay]['body']['contents'][1]['type'] = 'text';
					$prodAry[$keyDay]['body']['contents'][1]['text'] = '請選擇預約時間：';
					$prodAry[$keyDay]['body']['contents'][1]['wrap'] = true;
					$prodAry[$keyDay]['body']['contents'][1]['weight'] = "bold";
					$prodAry[$keyDay]['body']['contents'][1]['size'] = "sm";
					
					$prodAry[$keyDay]['body']['contents'][2]['type'] = 'separator';
					$prodAry[$keyDay]['body']['contents'][2]['margin'] = 'lg';
					
					$prodAry[$keyDay]['footer']['type'] = 'box';
					$prodAry[$keyDay]['footer']['layout'] = 'vertical';
					$prodAry[$keyDay]['footer']['spacing'] = 'sm';
					$prodAry[$keyDay]['footer']['contents'][$keyT2]['type'] = 'button';
					$prodAry[$keyDay]['footer']['contents'][$keyT2]['height'] = 'sm';
					$prodAry[$keyDay]['footer']['contents'][$keyT2]['style'] = 'primary';
					$prodAry[$keyDay]['footer']['contents'][$keyT2]['action']['type'] = 'message';
					$prodAry[$keyDay]['footer']['contents'][$keyT2]['action']['label'] = $timeVal['subject']."";
					$prodAry[$keyDay]['footer']['contents'][$keyT2]['action']['text'] = ' ';
					$prodAry[$keyDay]['footer']['contents'][$keyT2]['color'] = "#ff0000";
				}
			}
			$infoKey = count($prodAry[$keyDay]['footer']['contents']);
			$prodAry[$keyDay]['footer']['contents'][$infoKey]['type'] = 'text';
			$prodAry[$keyDay]['footer']['contents'][$infoKey]['text'] = "Design by aibitechcology";
			$prodAry[$keyDay]['footer']['contents'][$infoKey]['wrap'] = true;
			$prodAry[$keyDay]['footer']['contents'][$infoKey]['color'] = "#aaaaaa";
			$prodAry[$keyDay]['footer']['contents'][$infoKey]['size'] = "xxs";
			$prodAry[$keyDay]['footer']['contents'][$infoKey]['align'] = "end";
			
			//ksort($prodAry[$keyDay][actions]);
			ksort($prodAry[$keyDay][footer][contents]);
			
			//20號前當月，21號當月+下個月
			if(date('d') <= '20') {
				if(date('d', strtotime($_day.'day')) == date('t')) {
					$moreFlage = 0;
					break;
				}

			} else {
				if(date('Y-m-d', strtotime($_day.'day')) >= date('Y-m-d', strtotime(date('Y-m-t', strtotime('+1 month'))))) {
					$moreFlage = 0;
					break;
				}
			}	
	    	
	    }
		ksort($prodAry);
		$lastArry = end($prodAry);

		if($moreFlage) {
			$lastKey = count($prodAry);
			$prodAry[99] = array(
				"type" => "bubble",
				"body" => array(
					"type" => "box",
					"layout" => "vertical",
					"spacing" => "sm",
					"contents" => array(
						array(
							"type" => "button",
							"flex" => 1,
							"gravity" => "center",
							"action" => array(
								"type" => "postback",
								"label" => "想看更多請點我",
								"data" => "action=more7&class=".$class."&team=".$team."&page=".($page+1),
								
							),
						)	
					)	
				)	
			);
		}
		
		//ksort($prodAry);
		
		switch($content_type) {
		
			case "text" :
				$content_type = "文字訊息";
				$data = $prodAry;
				
				$result = array();
				foreach($data as $key => $row) {
					//if (mb_strpos($message, $row[title]) !== false) {
						array_push($result, $row);
					//}
				}
				
				$result = (!$result[0]) ? $data : $result;
				/*
				$data = array(
					"replyToken" => $reToken, 
					"messages" =>
						array(
							array(
								'type' => 'template',
								'altText' => '智慧沙龍',
								'template' => array(
									'type' => 'carousel',
									'columns' => $result
								),
							),
						),
				);
				*/
				
				$data = array(
					"replyToken" => $reToken, 
					"messages" =>
						array(
							
							array(
								'type' => 'flex',
								'altText' => '智慧沙龍',
								'contents' => array(
									'type' => 'carousel',
									'contents' => $result
								),
							),
						),
				);
				//file_put_contents("template.txt", json_encode($data)."\r\n",FILE_APPEND);
				break;
				
			
			default:
				$content_type = "未知訊息";
				break;
	   	}
		/*
		echo "<pre>";
		print_r($data);
		echo "</pre>";
		*/
		$context = stream_context_create(array(
		"http" => array("method" => "POST", "header" => implode(PHP_EOL, $header), "content" => json_encode($data), "ignore_errors" => true)
		));
		//file_put_contents("debug.txt", $url."\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($data)."\r\n",FILE_APPEND);
		//$log = file_get_contents($url, false, $context);
		//file_put_contents("debug.txt", json_encode($log)."\r\n",FILE_APPEND);
		
		//curl設定
		$headers = array(
			"Content-Type: application/json; charser=UTF-8",
			"Authorization: Bearer {" . $channel_access_token . "}"
		);
			 
		//curl
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		//如果失敗可以印出 $result 看錯誤訊息
		$result = curl_exec($ch);
		//file_put_contents("debug.txt", "==========================\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($result)."\r\n",FILE_APPEND);
		
		curl_close($ch);
	}
	
	//智慧沙龍
	function _reply3_3($content_type, $message, $page, $class, $team) {

	 	global $header, $reToken, $receive, $channel_access_token, $DB, $pdoDB, $from;
	 	
		$url = "https://api.line.me/v2/bot/message/reply";
		
		$data = ["replyToken" => $reToken, "messages" => array(["type" => "text", "text" => $message])];
		/*
		$sql = "
			SELECT 
				COUNT(*) as Rows
			FROM 
				(SELECT * from web_shift WHERE web_shift.web_class_id = '".$class."' and web_shift.web_team_id = '".$team."') as main 
			left join 
				(SELECT * from web_team) as teams
			on 
				main.web_team_id = teams.web_team_id
		";
		$pdo = $pdoDB->prepare($sql);
	    $pdo->execute();
	    $row = $pdo->fetch(PDO::FETCH_ASSOC);
		*/
		$Init_Page = '7';
		$moreFlage = 1;
		/*
		$total_pages = ceil($row[Rows]/$Init_Page);	//計算總頁數
		if ($page>$total_pages) $page = 1;		//修正輸入過大的頁數
		
		$moreFlage = ($row[Rows] > $Init_Page) ? 1 : 0;
		*/
		$timeSql = "SELECT * from web_time WHERE web_time.ifShow = '1' ORDER BY web_time.asort ASC";
		$pdo = $pdoDB->prepare($timeSql);
	    $pdo->execute();
	    $timeRow = $pdo->fetchAll(PDO::FETCH_ASSOC);
	
		$sql = "
			SELECT 
				main.web_class_id,
				main.web_x_class_id,
				main.ifShow,
				main.lineInfo,
				main.content,
				main.subject,
				main.lineInfo as mainInfo,
				x_class.subject as web_x_class_subject,
				teams.subject as teamSubject,
				teams.web_x_team_id as teamXid
			FROM 
				web_class as main 
			Left Join
				web_x_class x_class
			ON
				x_class.web_x_class_id = main.web_x_class_id
			join 
				(SELECT * from web_x_team where web_x_team_id = '".$team."') as teams
			WHERE main.web_class_id = '".$class."'		
		";
		//$sql .= " limit ".($page-1) * $Init_Page.", ".$Init_Page;
		$pdo = $pdoDB->prepare($sql);
	    $pdo->execute();
	    $row = $pdo->fetchAll(PDO::FETCH_ASSOC);
		/*
		echo "<pre>";
		print_r($row);
		echo "</pre>";
		*/
		//echo date('N');
		$start = ($page == '1') ? '1' : ((($page - 1) * (6)) + ($page - 1)) + date('N');
		$end = ($page == '1') ? '7' : $start + 6;
		$timeAry = array();
		$test = array();
		foreach($row as $key => $timeList) {
			//$contentAry = array(1,2,3,4,5,6,7);
			//foreach($contentAry as $key2 => $contentVal) {
			for($contentVal = $start; $contentVal <= $end; $contentVal++) {
				$timeAry[$contentVal][] = $timeList;
			}
			
			$lineInfoAry = explode('#####', $timeList['lineInfo']);
			foreach($lineInfoAry as $$lineInfoKey => $lineInfoVal) {
				$lineInfoRowAry[] = explode('@#@', $lineInfoVal);
			}
		}
		
		foreach($lineInfoRowAry as $key => $lineInfoAry) {
			$lineInfo[$lineInfoAry['2']][] = $lineInfoAry['1'];
		}
	
		end($timeAry);
		$lastKey = key($timeAry);
	    $prodAry = array();
	    $targetKey = array(
	    	//'web_team_id',
	    	'subject',
	    	//'price_member',
	    	'Files',

	    );
		/*
		echo "<pre>";
		print_r($timeRow);
		echo "</pre>";
		*/
		/*
		$_timeAry = array(
			'10:30~11:30',
			'11:30~12:30',
			'12:30~13:30',
			'13:30~14:30',
			'14:30~15:30',
			'15:30~16:30',
			'16:30~17:30',
			'17:30~18:30',
			'18:30~19:30',
		);
		*/
		$_timeAry = array(
			'10:30~11:00',
			'11:00~11:30',
			'11:30~12:00',
			'12:00~12:30',
			'12:30~13:00',
			'13:00~13:30',
			'13:30~14:00',
			'14:00~14:30',
			'14:30~15:00',
			'15:00~15:30',
			'15:30~16:00',
			'16:00~16:30',
			'16:30~17:00',
			'17:00~17:30',
			'17:30~18:00',
			'18:00~18:30',
			'18:30~19:00',
			'19:00~19:30',
		);
		$registerTimeRow = array();
		$isSql = "
			SELECT 
				main.*,
				xteam.subject as xteamSubject,
				xteam.time as xteamTime
			from 
				web_x_register as main
			left join
				web_x_team xteam
			on
				xteam.web_x_team_id = main.web_x_team_id
			WHERE 
				main.registerDate >= '".date('Y-m-d')."'
			AND
				main.paymentstatus != '取消' 
			AND
				main.paymentstatus != '已報到'
			AND
				main.paymentstatus != '已服務'	
		";
		//echo $isSql;
		$pdo3 = $pdoDB->prepare($isSql);
		$pdo3->execute();
		$registerRow = $pdo3->fetchAll(PDO::FETCH_ASSOC);
		
		foreach($registerRow as $registerKey => $registerVal) {
			//echo array_search(substr($registerVal['subject'],16), $_timeAry);
			if(array_search(substr($registerVal['subject'],16), $_timeAry) >= '0') {
				if($_timeAry[array_search(substr($registerVal['subject'],16), $_timeAry)] == substr($registerVal['subject'],16)) {
					$registerTimeRow[$registerVal['web_time_id']][$registerVal['registerDate']][] = substr($registerVal['subject'],16);
					if($registerVal['xteamTime'] > 1) {
						for($i = 2; $i <= $registerVal['xteamTime']; $i++) {
							$searchKey = array_search(substr($registerVal['subject'],16), $_timeAry) + ($i - 1);
							$registerTimeRow[$registerVal['web_time_id']][$registerVal['registerDate']][] = $_timeAry[$searchKey];
						}	
					}
				}	
				
			}
		}
		//echo date('N')."</br>";
		//for($key = 1; $key <= 7; $key++) {
	    foreach($timeAry as $key => $prodInfo) {
			$_day = ($key < date('N')) ? '+'.ceil((7 - date('N')) + $key) : '+'.ceil($key - date('N'));
			$keyDay = $key - date('N');
			$keyDay = ($keyDay < 0) ? ceil(7 - abs($keyDay)): $keyDay;
			$dayName = array(
				1 => '一', 2 => '二', 3 => '三', 4 => '四', 5 => '五', 6 => '六', 7 => '日',
			);
			
			
	    	foreach($prodInfo as $key2 => $prod) {
				//$teamRestDayAry = explode(',', $prod['teamRestDay']);
		/*	
				$teamRestDayAry = explode('@#@', $prod['mainInfo']);
				echo "<pre>";
				print_r($teamRestDayAry);
				echo "</pre>";
		*/	
				//有設休診時間
				//echo $prod['teamRestDay']."===".date('Y-m-d', strtotime($_day.'day'));
	    		if(!in_array(date('Y-m-d', strtotime($_day.'day')), $teamRestDayAry)) {
					
					/*
					$prodAry[$keyDay][title] = date('Y-m-d', strtotime($_day.'day'))."(".$dayName[date('N', strtotime($_day.'day'))].")";
					$prodAry[$keyDay][text] = '請選擇時段：';
					
					$keyT = array_search($prod['subject'], array_column($timeRow, 'subject'));
					$prodAry[$keyDay][actions][$keyT]['type'] = 'postback';
					$prodAry[$keyDay][actions][$keyT]['label'] = $prod['subject'];
					$prodAry[$keyDay][actions][$keyT]['data'] = 'action=reservation&item=checkPerson&shiftId='.$prod['web_shift_id']."&date=".$prodAry[$keyDay]['title']."-".$prod['subject']."&teamSubject=".$prod['teamSubject']."&classSubject=".$prod['classSubject'];
					*/
					
					$keyT = array_search($prod['subject'], array_column($timeRow, 'subject'));
					$prodAry[$keyDay]['type'] = 'bubble';
					$prodAry[$keyDay]['styles']['body']['backgroundColor'] = '#FFFFFF';
					$prodAry[$keyDay]['styles']['footer']['backgroundColor'] = '#FFFFFF';
					$prodAry[$keyDay]['body']['type'] = 'box';
					$prodAry[$keyDay]['body']['layout'] = 'vertical';
					$prodAry[$keyDay]['body']['spacing'] = 'sm';
					$prodAry[$keyDay]['body']['contents'][0]['type'] = 'text';
					$prodAry[$keyDay]['body']['contents'][0]['text'] = date('Y-m-d', strtotime($_day.'day'))."(".$dayName[date('N', strtotime($_day.'day'))].")";
					$prodAry[$keyDay]['body']['contents'][0]['wrap'] = true;
					$prodAry[$keyDay]['body']['contents'][0]['weight'] = "bold";
					$prodAry[$keyDay]['body']['contents'][0]['size'] = "xl";
					$prodAry[$keyDay]['body']['contents'][1]['type'] = 'text';
					$prodAry[$keyDay]['body']['contents'][1]['text'] = '請選擇預約時間：';
					$prodAry[$keyDay]['body']['contents'][1]['wrap'] = true;
					$prodAry[$keyDay]['body']['contents'][1]['weight'] = "bold";
					$prodAry[$keyDay]['body']['contents'][1]['size'] = "sm";
					
					$prodAry[$keyDay]['body']['contents'][2]['type'] = 'separator';
					$prodAry[$keyDay]['body']['contents'][2]['margin'] = 'lg';
					
					$prodAry[$keyDay]['footer']['type'] = 'box';
					$prodAry[$keyDay]['footer']['layout'] = 'vertical';
					$prodAry[$keyDay]['footer']['spacing'] = 'sm';
					foreach($_timeAry as $keyT => $_tiemVal) {
						//echo $lineInfo[date('Y-m-d', strtotime($_day.'day'))];
						if($lineInfo[date('Y-m-d', strtotime($_day.'day'))]) {
							if((in_array('1', $lineInfo[date('Y-m-d', strtotime($_day.'day'))]) && $keyT <= 3) || (in_array('3', $lineInfo[date('Y-m-d', strtotime($_day.'day'))]) && $keyT <= 3)) {
								$prodAry[$keyDay]['footer']['contents'][$keyT]['type'] = 'button';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['height'] = 'sm';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['style'] = 'primary';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['color'] = '#DCDCDC';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['type'] = 'message';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['label'] = substr($_tiemVal,0,5)."(休)";
								//$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['data'] = 'action=reservation&item=checkPerson&shiftId='.$prod['web_shift_id']."&date=".$prodAry[$keyDay]['body']['contents'][0]['text']."-".$_tiemVal."&teamSubject=".$prod['teamSubject']."&classSubject=".$prod['classSubject']."&timeId=".$prod['web_time_id'];
								
								$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['text'] = ' ';
								
							} else if((in_array('2', $lineInfo[date('Y-m-d', strtotime($_day.'day'))]) && $keyT >= 4) || (in_array('3', $lineInfo[date('Y-m-d', strtotime($_day.'day'))]) && $keyT >= 4)) {
								
								$prodAry[$keyDay]['footer']['contents'][$keyT]['type'] = 'button';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['height'] = 'sm';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['style'] = 'primary';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['color'] = '#DCDCDC';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['type'] = 'message';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['label'] = substr($_tiemVal,0,5)."(休)";
								//$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['data'] = 'action=reservation&item=checkPerson&shiftId='.$prod['web_shift_id']."&date=".$prodAry[$keyDay]['body']['contents'][0]['text']."-".$_tiemVal."&teamSubject=".$prod['teamSubject']."&classSubject=".$prod['classSubject']."&timeId=".$prod['web_time_id'];
								
								$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['text'] = ' ';
								
							} else {
								
								if(!in_array($_tiemVal, $registerTimeRow[$prod['web_class_id']][date('Y-m-d', strtotime($_day.'day'))])) {
									$prodAry[$keyDay]['footer']['contents'][$keyT]['type'] = 'button';
									$prodAry[$keyDay]['footer']['contents'][$keyT]['height'] = 'sm';
									$prodAry[$keyDay]['footer']['contents'][$keyT]['style'] = 'primary';
									$prodAry[$keyDay]['footer']['contents'][$keyT]['color'] = '#444444';
									$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['type'] = 'postback';
									$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['label'] = substr($_tiemVal,0,5);
									//$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['data'] = 'action=reservation&item=checkPerson&shiftId='.$prod['web_shift_id']."&date=".$prodAry[$keyDay]['body']['contents'][0]['text']."-".$_tiemVal."&teamSubject=".$prod['teamSubject']."&classSubject=".$prod['classSubject']."&timeId=".$prod['web_time_id'];
									
									$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['data'] = 'action=reservation&item=checkPerson&shiftId='.$prod['web_shift_id']."&date=".$prodAry[$keyDay]['body']['contents'][0]['text']."-".$_tiemVal."&teamSubject=".$prod['subject']."&classSubject=".$prod['teamSubject']."&timeId=".$prod['web_class_id']."&teamXid=".$prod['teamXid'];
									
									
								} else {
									$prodAry[$keyDay]['footer']['contents'][$keyT]['type'] = 'button';
									$prodAry[$keyDay]['footer']['contents'][$keyT]['height'] = 'sm';
									$prodAry[$keyDay]['footer']['contents'][$keyT]['style'] = 'primary';
									$prodAry[$keyDay]['footer']['contents'][$keyT]['color'] = '#ff0000';
									$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['type'] = 'message';
									$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['label'] = substr($_tiemVal,0,5)."(已有預約)";
									//$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['data'] = 'action=reservation&item=checkPerson&shiftId='.$prod['web_shift_id']."&date=".$prodAry[$keyDay]['body']['contents'][0]['text']."-".$_tiemVal."&teamSubject=".$prod['teamSubject']."&classSubject=".$prod['classSubject']."&timeId=".$prod['web_time_id'];
									
									$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['text'] = ' ';
								}
							}		
						} else {	
							if(!in_array($_tiemVal, $registerTimeRow[$prod['web_class_id']][date('Y-m-d', strtotime($_day.'day'))])) {
								$prodAry[$keyDay]['footer']['contents'][$keyT]['type'] = 'button';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['height'] = 'sm';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['style'] = 'primary';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['color'] = '#444444';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['type'] = 'postback';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['label'] = substr($_tiemVal,0,5);
								//$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['data'] = 'action=reservation&item=checkPerson&shiftId='.$prod['web_shift_id']."&date=".$prodAry[$keyDay]['body']['contents'][0]['text']."-".$_tiemVal."&teamSubject=".$prod['teamSubject']."&classSubject=".$prod['classSubject']."&timeId=".$prod['web_time_id'];
								
								$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['data'] = 'action=reservation&item=checkPerson&shiftId='.$prod['web_shift_id']."&date=".$prodAry[$keyDay]['body']['contents'][0]['text']."-".$_tiemVal."&teamSubject=".$prod['subject']."&classSubject=".$prod['teamSubject']."&timeId=".$prod['web_class_id']."&teamXid=".$prod['teamXid'];
								
								
							} else {
								$prodAry[$keyDay]['footer']['contents'][$keyT]['type'] = 'button';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['height'] = 'sm';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['style'] = 'primary';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['color'] = '#ff0000';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['type'] = 'message';
								$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['label'] = substr($_tiemVal,0,5)."(已有預約)";
								//$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['data'] = 'action=reservation&item=checkPerson&shiftId='.$prod['web_shift_id']."&date=".$prodAry[$keyDay]['body']['contents'][0]['text']."-".$_tiemVal."&teamSubject=".$prod['teamSubject']."&classSubject=".$prod['classSubject']."&timeId=".$prod['web_time_id'];
								
								$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['text'] = ' ';
							}
						}	
					}	
					/*
					$prodAry[$keyDay]['footer']['contents'][$keyT+1]['type'] = 'text';
					$prodAry[$keyDay]['footer']['contents'][$keyT+1]['text'] = "Design by aibitechcology";
					$prodAry[$keyDay]['footer']['contents'][$keyT+1]['wrap'] = true;
					$prodAry[$keyDay]['footer']['contents'][$keyT+1]['color'] = "#aaaaaa";
					$prodAry[$keyDay]['footer']['contents'][$keyT+1]['size'] = "xxs";
					$prodAry[$keyDay]['footer']['contents'][$keyT+1]['align'] = "end";
					*/
					
					
				} else {
					/*
					$prodAry[$keyDay][title] = date('Y-m-d', strtotime($_day.'day'))."(".$dayName[date('N', strtotime($_day.'day'))].")";
					$prodAry[$keyDay][text] = '請選擇時段：';
					
					$keyT = array_search($prod['subject'], array_column($timeRow, 'subject'));
					$prodAry[$keyDay][actions][$keyT]['type'] = 'message';
					$prodAry[$keyDay][actions][$keyT]['label'] = $prod['subject']."";
					$prodAry[$keyDay][actions][$keyT]['text'] = " ";
					*/
					
					$keyT = array_search($prod['subject'], array_column($timeRow, 'subject'));
					$prodAry[$keyDay]['type'] = 'bubble';
					$prodAry[$keyDay]['styles']['body']['backgroundColor'] = '#FFFFFF';
					$prodAry[$keyDay]['styles']['footer']['backgroundColor'] = '#FFFFFF';
					$prodAry[$keyDay]['body']['type'] = 'box';
					$prodAry[$keyDay]['body']['layout'] = 'vertical';
					$prodAry[$keyDay]['body']['spacing'] = 'sm';
					$prodAry[$keyDay]['body']['contents'][0]['type'] = 'text';
					$prodAry[$keyDay]['body']['contents'][0]['text'] = date('Y-m-d', strtotime($_day.'day'))."(".$dayName[date('N', strtotime($_day.'day'))].")";
					$prodAry[$keyDay]['body']['contents'][0]['wrap'] = true;
					$prodAry[$keyDay]['body']['contents'][0]['weight'] = "bold";
					$prodAry[$keyDay]['body']['contents'][0]['size'] = "xl";
					$prodAry[$keyDay]['body']['contents'][1]['type'] = 'text';
					$prodAry[$keyDay]['body']['contents'][1]['text'] = '請選擇預約時間：';
					$prodAry[$keyDay]['body']['contents'][1]['wrap'] = true;
					$prodAry[$keyDay]['body']['contents'][1]['weight'] = "bold";
					$prodAry[$keyDay]['body']['contents'][1]['size'] = "sm";
					
					$prodAry[$keyDay]['body']['contents'][2]['type'] = 'separator';
					$prodAry[$keyDay]['body']['contents'][2]['margin'] = 'lg';
					
					$prodAry[$keyDay]['footer']['type'] = 'box';
					$prodAry[$keyDay]['footer']['layout'] = 'vertical';
					$prodAry[$keyDay]['footer']['spacing'] = 'sm';
					$prodAry[$keyDay]['footer']['contents'][$keyT]['type'] = 'button';
					$prodAry[$keyDay]['footer']['contents'][$keyT]['height'] = 'sm';
					$prodAry[$keyDay]['footer']['contents'][$keyT]['style'] = 'primary';
					$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['type'] = 'message';
					$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['label'] = $prod['urlsubject'].$prod['subject']."";
					$prodAry[$keyDay]['footer']['contents'][$keyT]['action']['text'] = ' ';
					$prodAry[$keyDay]['footer']['contents'][$keyT]['color'] = "#ff0000";
					/*
					$prodAry[$keyDay]['footer']['contents'][$keyT+1]['type'] = 'text';
					$prodAry[$keyDay]['footer']['contents'][$keyT+1]['text'] = "Design by aibitechcology";
					$prodAry[$keyDay]['footer']['contents'][$keyT+1]['wrap'] = true;
					$prodAry[$keyDay]['footer']['contents'][$keyT+1]['color'] = "#aaaaaa";
					$prodAry[$keyDay]['footer']['contents'][$keyT+1]['size'] = "xxs";
					$prodAry[$keyDay]['footer']['contents'][$keyT+1]['align'] = "end";
					*/
				}	
				
	    	}
			//echo $keyT;
			//echo count($prodAry[$key][actions])."</br>";
			//if(count($prodAry[$keyDay][actions]) < 3 ) {
			if(count($prodAry[$keyDay]['footer']['contents']) < 3 ) {	
				foreach($timeRow as $timekey => $timeVal) {
					//if($prodAry[$keyDay][actions][$timekey]) {
					
					if($prodAry[$keyDay]['footer']['contents'][$timekey]) {	
						continue;
					}
					/*
					$keyT2 = array_search($timeVal['subject'], array_column($timeRow, 'subject'));
					$prodAry[$keyDay][actions][$keyT2]['type'] = 'message';
					$prodAry[$keyDay][actions][$keyT2]['label'] = $timeVal['subject']."(休診)";
					$prodAry[$keyDay][actions][$keyT2]['text'] = " ";
					*/
					
					$keyT2 = array_search($timeVal['subject'], array_column($timeRow, 'subject'));
					$prodAry[$keyDay]['type'] = 'bubble';
					$prodAry[$keyDay]['styles']['body']['backgroundColor'] = '#FFFFFF';
					$prodAry[$keyDay]['styles']['footer']['backgroundColor'] = '#FFFFFF';
					$prodAry[$keyDay]['body']['type'] = 'box';
					$prodAry[$keyDay]['body']['layout'] = 'vertical';
					$prodAry[$keyDay]['body']['spacing'] = 'sm';
					$prodAry[$keyDay]['body']['contents'][0]['type'] = 'text';
					$prodAry[$keyDay]['body']['contents'][0]['text'] = date('Y-m-d', strtotime($_day.'day'))."(".$dayName[date('N', strtotime($_day.'day'))].")";
					$prodAry[$keyDay]['body']['contents'][0]['wrap'] = true;
					$prodAry[$keyDay]['body']['contents'][0]['weight'] = "bold";
					$prodAry[$keyDay]['body']['contents'][0]['size'] = "xl";
					$prodAry[$keyDay]['body']['contents'][1]['type'] = 'text';
					$prodAry[$keyDay]['body']['contents'][1]['text'] = '請選擇預約時間：';
					$prodAry[$keyDay]['body']['contents'][1]['wrap'] = true;
					$prodAry[$keyDay]['body']['contents'][1]['weight'] = "bold";
					$prodAry[$keyDay]['body']['contents'][1]['size'] = "sm";
					
					$prodAry[$keyDay]['body']['contents'][2]['type'] = 'separator';
					$prodAry[$keyDay]['body']['contents'][2]['margin'] = 'lg';
					
					$prodAry[$keyDay]['footer']['type'] = 'box';
					$prodAry[$keyDay]['footer']['layout'] = 'vertical';
					$prodAry[$keyDay]['footer']['spacing'] = 'sm';
					$prodAry[$keyDay]['footer']['contents'][$keyT2]['type'] = 'button';
					$prodAry[$keyDay]['footer']['contents'][$keyT2]['height'] = 'sm';
					$prodAry[$keyDay]['footer']['contents'][$keyT2]['style'] = 'primary';
					$prodAry[$keyDay]['footer']['contents'][$keyT2]['action']['type'] = 'message';
					$prodAry[$keyDay]['footer']['contents'][$keyT2]['action']['label'] = $timeVal['subject']."";
					$prodAry[$keyDay]['footer']['contents'][$keyT2]['action']['text'] = ' ';
					$prodAry[$keyDay]['footer']['contents'][$keyT2]['color'] = "#ff0000";
				}
			}
			$infoKey = count($prodAry[$keyDay]['footer']['contents']);
			$prodAry[$keyDay]['footer']['contents'][$infoKey]['type'] = 'text';
			$prodAry[$keyDay]['footer']['contents'][$infoKey]['text'] = "Design by aibitechcology";
			$prodAry[$keyDay]['footer']['contents'][$infoKey]['wrap'] = true;
			$prodAry[$keyDay]['footer']['contents'][$infoKey]['color'] = "#aaaaaa";
			$prodAry[$keyDay]['footer']['contents'][$infoKey]['size'] = "xxs";
			$prodAry[$keyDay]['footer']['contents'][$infoKey]['align'] = "end";
			
			//ksort($prodAry[$keyDay][actions]);
			ksort($prodAry[$keyDay][footer][contents]);
			
			//20號前當月，21號當月+下個月
			if(date('d') <= '20') {
				if(date('d', strtotime($_day.'day')) == date('t')) {
					$moreFlage = 0;
					break;
				}

			} else {
				if(date('Y-m-d', strtotime($_day.'day')) >= date('Y-m-d', strtotime(date('Y-m-t', strtotime('+1 month'))))) {
					$moreFlage = 0;
					break;
				}
			}	
	    	
	    }
		ksort($prodAry);
		$lastArry = end($prodAry);

		if($moreFlage) {
			$lastKey = count($prodAry);
			$prodAry[99] = array(
				"type" => "bubble",
				"body" => array(
					"type" => "box",
					"layout" => "vertical",
					"spacing" => "sm",
					"contents" => array(
						array(
							"type" => "button",
							"flex" => 1,
							"gravity" => "center",
							"action" => array(
								"type" => "postback",
								"label" => "想看更多請點我",
								"data" => "action=more7&class=".$class."&team=".$team."&page=".($page+1),
								
							),
						)	
					)	
				)	
			);
		}
		
		//ksort($prodAry);
		
		switch($content_type) {
		
			case "text" :
				$content_type = "文字訊息";
				$data = $prodAry;
				
				$result = array();
				foreach($data as $key => $row) {
					//if (mb_strpos($message, $row[title]) !== false) {
						array_push($result, $row);
					//}
				}
				
				$result = (!$result[0]) ? $data : $result;
				/*
				$data = array(
					"replyToken" => $reToken, 
					"messages" =>
						array(
							array(
								'type' => 'template',
								'altText' => '智慧沙龍',
								'template' => array(
									'type' => 'carousel',
									'columns' => $result
								),
							),
						),
				);
				*/
				
				$data = array(
					"replyToken" => $reToken, 
					"messages" =>
						array(
							
							array(
								'type' => 'flex',
								'altText' => '智慧沙龍',
								'contents' => array(
									'type' => 'carousel',
									'contents' => $result
								),
							),
						),
				);
				//file_put_contents("template.txt", json_encode($data)."\r\n",FILE_APPEND);
				break;
				
			
			default:
				$content_type = "未知訊息";
				break;
	   	}
		/*
		echo "<pre>";
		print_r($data);
		echo "</pre>";
		*/
		$context = stream_context_create(array(
		"http" => array("method" => "POST", "header" => implode(PHP_EOL, $header), "content" => json_encode($data), "ignore_errors" => true)
		));
		//file_put_contents("debug.txt", $url."\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($data)."\r\n",FILE_APPEND);
		//$log = file_get_contents($url, false, $context);
		//file_put_contents("debug.txt", json_encode($log)."\r\n",FILE_APPEND);
		
		//curl設定
		$headers = array(
			"Content-Type: application/json; charser=UTF-8",
			"Authorization: Bearer {" . $channel_access_token . "}"
		);
			 
		//curl
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		//如果失敗可以印出 $result 看錯誤訊息
		$result = curl_exec($ch);
		//file_put_contents("debug.txt", "==========================\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($result)."\r\n",FILE_APPEND);
		
		curl_close($ch);
	}
	
	//是否本人
	function booking_menu($content_type, $message, $type) {

	 	global $header, $reToken, $receive, $channel_access_token, $DB, $pdoDB, $from;
		
		$sql = "Select * From web_basicset Where web_basicset_id = '1' ";
		$pdo3 = $pdoDB->prepare($sql);
		$pdo3->execute();
		$basicset = $pdo3->fetch(PDO::FETCH_ASSOC);
	 	
		$url = "https://api.line.me/v2/bot/message/reply";
		
		$data = ["replyToken" => $reToken, "messages" => array(["type" => "text", "text" => $message])];
		if($type == '1') {
			
			$prodAry[0]['type'] = 'bubble';
			$prodAry[0]['styles']['body']['backgroundColor'] = '#FFFFFF';
			$prodAry[0]['styles']['footer']['backgroundColor'] = '#FFFFFF';
			$prodAry[0]['body']['type'] = 'box';
			$prodAry[0]['body']['layout'] = 'vertical';
			$prodAry[0]['body']['spacing'] = 'sm';
			$prodAry[0]['body']['contents'][0]['type'] = 'text';
			$prodAry[0]['body']['contents'][0]['text'] = '預約服務';
			$prodAry[0]['body']['contents'][0]['wrap'] = true;
			$prodAry[0]['body']['contents'][0]['weight'] = "bold";
			$prodAry[0]['body']['contents'][0]['size'] = "xl";
			/*
			$prodAry[0]['body']['contents'][1]['type'] = 'text';
			$prodAry[0]['body']['contents'][1]['text'] = " ";
			$prodAry[0]['body']['contents'][1]['wrap'] = true;
			$prodAry[0]['body']['contents'][1]['weight'] = "bold";
			$prodAry[0]['body']['contents'][1]['size'] = "sm";
			*/
			
			$prodAry[0]['footer']['type'] = 'box';
			$prodAry[0]['footer']['layout'] = 'vertical';
			$prodAry[0]['footer']['spacing'] = 'sm';
			
			$prodAry[0]['footer']['contents'][0]['type'] = 'button';
			$prodAry[0]['footer']['contents'][0]['height'] = 'sm';
			$prodAry[0]['footer']['contents'][0]['style'] = 'primary';
			$prodAry[0]['footer']['contents'][0]['color'] = "#444444";
			$prodAry[0]['footer']['contents'][0]['action']['type'] = 'message';
			$prodAry[0]['footer']['contents'][0]['action']['label'] = "我要預約";
			$prodAry[0]['footer']['contents'][0]['action']['text'] = "我要預約";
			
			$prodAry[0]['footer']['contents'][1]['type'] = 'button';
			$prodAry[0]['footer']['contents'][1]['height'] = 'sm';
			$prodAry[0]['footer']['contents'][1]['style'] = 'primary';
			$prodAry[0]['footer']['contents'][1]['color'] = "#444444";
			$prodAry[0]['footer']['contents'][1]['action']['type'] = 'message';
			$prodAry[0]['footer']['contents'][1]['action']['label'] = "預約查詢";
			$prodAry[0]['footer']['contents'][1]['action']['text'] = "預約查詢";
			
			$prodAry[0]['footer']['contents'][2]['type'] = 'text';
			$prodAry[0]['footer']['contents'][2]['text'] = "Design by aibitechcology";
			$prodAry[0]['footer']['contents'][2]['wrap'] = true;
			$prodAry[0]['footer']['contents'][2]['color'] = "#aaaaaa";
			$prodAry[0]['footer']['contents'][2]['size'] = "xxs";
			$prodAry[0]['footer']['contents'][2]['align'] = "end";
			
		} else if($type == '2') {
			
			$prodAry[0]['type'] = 'bubble';
			$prodAry[0]['styles']['body']['backgroundColor'] = '#FFFFFF';
			$prodAry[0]['styles']['footer']['backgroundColor'] = '#FFFFFF';
			$prodAry[0]['body']['type'] = 'box';
			$prodAry[0]['body']['layout'] = 'vertical';
			$prodAry[0]['body']['spacing'] = 'sm';
			$prodAry[0]['body']['contents'][0]['type'] = 'text';
			$prodAry[0]['body']['contents'][0]['text'] = 'IG / FB';
			$prodAry[0]['body']['contents'][0]['wrap'] = true;
			$prodAry[0]['body']['contents'][0]['weight'] = "bold";
			$prodAry[0]['body']['contents'][0]['size'] = "xl";
			/*
			$prodAry[0]['body']['contents'][1]['type'] = 'text';
			$prodAry[0]['body']['contents'][1]['text'] = " ";
			$prodAry[0]['body']['contents'][1]['wrap'] = true;
			$prodAry[0]['body']['contents'][1]['weight'] = "bold";
			$prodAry[0]['body']['contents'][1]['size'] = "sm";
			*/
			
			$prodAry[0]['footer']['type'] = 'box';
			$prodAry[0]['footer']['layout'] = 'vertical';
			$prodAry[0]['footer']['spacing'] = 'sm';
			
			$prodAry[0]['footer']['contents'][0]['type'] = 'button';
			$prodAry[0]['footer']['contents'][0]['height'] = 'sm';
			$prodAry[0]['footer']['contents'][0]['style'] = 'primary';
			$prodAry[0]['footer']['contents'][0]['color'] = "#444444";
			$prodAry[0]['footer']['contents'][0]['action']['type'] = 'uri';
			$prodAry[0]['footer']['contents'][0]['action']['label'] = "Instagram";
			$prodAry[0]['footer']['contents'][0]['action']['uri'] = $basicset['Init_Instagram'];
			
			$prodAry[0]['footer']['contents'][1]['type'] = 'button';
			$prodAry[0]['footer']['contents'][1]['height'] = 'sm';
			$prodAry[0]['footer']['contents'][1]['style'] = 'primary';
			$prodAry[0]['footer']['contents'][1]['color'] = "#444444";
			$prodAry[0]['footer']['contents'][1]['action']['type'] = 'uri';
			$prodAry[0]['footer']['contents'][1]['action']['label'] = "facebook";
			$prodAry[0]['footer']['contents'][1]['action']['uri'] = $basicset['Init_Facebook'];
			
			$prodAry[0]['footer']['contents'][2]['type'] = 'text';
			$prodAry[0]['footer']['contents'][2]['text'] = "Design by aibitechcology";
			$prodAry[0]['footer']['contents'][2]['wrap'] = true;
			$prodAry[0]['footer']['contents'][2]['color'] = "#aaaaaa";
			$prodAry[0]['footer']['contents'][2]['size'] = "xxs";
			$prodAry[0]['footer']['contents'][2]['align'] = "end";
			
		} else if($type == '3') {
			
			$prodAry[0]['type'] = 'bubble';
			$prodAry[0]['styles']['body']['backgroundColor'] = '#FFFFFF';
			$prodAry[0]['styles']['footer']['backgroundColor'] = '#FFFFFF';
			$prodAry[0]['body']['type'] = 'box';
			$prodAry[0]['body']['layout'] = 'vertical';
			$prodAry[0]['body']['spacing'] = 'sm';
			$prodAry[0]['body']['contents'][0]['type'] = 'text';
			$prodAry[0]['body']['contents'][0]['text'] = '作品集 / IG / FB';
			$prodAry[0]['body']['contents'][0]['wrap'] = true;
			$prodAry[0]['body']['contents'][0]['weight'] = "bold";
			$prodAry[0]['body']['contents'][0]['size'] = "xl";
			/*
			$prodAry[0]['body']['contents'][1]['type'] = 'text';
			$prodAry[0]['body']['contents'][1]['text'] = " ";
			$prodAry[0]['body']['contents'][1]['wrap'] = true;
			$prodAry[0]['body']['contents'][1]['weight'] = "bold";
			$prodAry[0]['body']['contents'][1]['size'] = "sm";
			*/
			
			$prodAry[0]['footer']['type'] = 'box';
			$prodAry[0]['footer']['layout'] = 'vertical';
			$prodAry[0]['footer']['spacing'] = 'sm';
			
			$prodAry[0]['footer']['contents'][0]['type'] = 'button';
			$prodAry[0]['footer']['contents'][0]['height'] = 'sm';
			$prodAry[0]['footer']['contents'][0]['style'] = 'primary';
			$prodAry[0]['footer']['contents'][0]['color'] = "#444444";
			$prodAry[0]['footer']['contents'][0]['action']['type'] = 'uri';
			$prodAry[0]['footer']['contents'][0]['action']['label'] = "Instagram";
			$prodAry[0]['footer']['contents'][0]['action']['uri'] = $basicset['Init_Instagram'];
			
			$prodAry[0]['footer']['contents'][1]['type'] = 'button';
			$prodAry[0]['footer']['contents'][1]['height'] = 'sm';
			$prodAry[0]['footer']['contents'][1]['style'] = 'primary';
			$prodAry[0]['footer']['contents'][1]['color'] = "#444444";
			$prodAry[0]['footer']['contents'][1]['action']['type'] = 'uri';
			$prodAry[0]['footer']['contents'][1]['action']['label'] = "facebook";
			$prodAry[0]['footer']['contents'][1]['action']['uri'] = $basicset['Init_Facebook'];
			
			$prodAry[0]['footer']['contents'][2]['type'] = 'button';
			$prodAry[0]['footer']['contents'][2]['height'] = 'sm';
			$prodAry[0]['footer']['contents'][2]['style'] = 'primary';
			$prodAry[0]['footer']['contents'][2]['color'] = "#444444";
			$prodAry[0]['footer']['contents'][2]['action']['type'] = 'uri';
			$prodAry[0]['footer']['contents'][2]['action']['label'] = "作品牆";
			$prodAry[0]['footer']['contents'][2]['action']['uri'] = $basicset['Init_Other_Web'];
			
			$prodAry[0]['footer']['contents'][3]['type'] = 'button';
			$prodAry[0]['footer']['contents'][3]['height'] = 'sm';
			$prodAry[0]['footer']['contents'][3]['style'] = 'primary';
			$prodAry[0]['footer']['contents'][3]['color'] = "#444444";
			$prodAry[0]['footer']['contents'][3]['action']['type'] = 'uri';
			$prodAry[0]['footer']['contents'][3]['action']['label'] = "設計團隊";
			$prodAry[0]['footer']['contents'][3]['action']['uri'] = "line://app/1653388002-RGnL10vz";
			
			$prodAry[0]['footer']['contents'][4]['type'] = 'text';
			$prodAry[0]['footer']['contents'][4]['text'] = "Design by aibitechcology";
			$prodAry[0]['footer']['contents'][4]['wrap'] = true;
			$prodAry[0]['footer']['contents'][4]['color'] = "#aaaaaa";
			$prodAry[0]['footer']['contents'][4]['size'] = "xxs";
			$prodAry[0]['footer']['contents'][4]['align'] = "end";
			
		}
		
		/*
		$prodAry[$lastKey + 1] = array(
			'type' => 'postback',
			'label' => '全部課程',
			"data" => "action=reservationCategory&cid=all",
		);
		*/

		switch($content_type) {
		
			case "text" :
				$content_type = "文字訊息";
				
				$data = $prodAry;
				echo "<pre>";
				print_r($data);
				echo "</pre>";
				$result = array();
				foreach($data as $key => $row) {
					//if (mb_strpos($message, $row[title]) !== false) {
						array_push($result, $row);
					//}
				}				
				
				$result = (!$result[0]) ? $data : $result;
				/*
				$data = array(
					"replyToken" => $reToken, 
					"messages" =>
						array(
						
							array(
								'type' => 'template',
								'altText' => '線上預約',
								'template' => array(
									'type' => 'buttons',
									'title' => '線上預約',
									'text' => '服務據點',
									'actions' => $result
								),
							),
						),
				);
				*/
				
				$data = array(
					"replyToken" => $reToken, 
					"messages" =>
						array(
							
							array(
								'type' => 'flex',
								'altText' => '線上預約',
								'contents' => array(
									'type' => 'carousel',
									'contents' => $result
								),
							),
						),
				);
				//file_put_contents("template.txt", json_encode($data)."\r\n",FILE_APPEND);
				break;
				
			
				
			default:
				$content_type = "未知訊息";
				break;
	   	}
		echo "<pre>";
		print_r($data);
		echo "</pre>";
		
		$context = stream_context_create(array(
		"http" => array("method" => "POST", "header" => implode(PHP_EOL, $header), "content" => json_encode($data), "ignore_errors" => true)
		));
		//file_put_contents("debug.txt", $url."\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($data)."\r\n",FILE_APPEND);
		//$log = file_get_contents($url, false, $context);
		//file_put_contents("debug.txt", json_encode($log)."\r\n",FILE_APPEND);
		
		//curl設定
		$headers = array(
			"Content-Type: application/json; charser=UTF-8",
			"Authorization: Bearer {" . $channel_access_token . "}"
		);
			 
		//curl
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		//如果失敗可以印出 $result 看錯誤訊息
		$result = curl_exec($ch);
		//file_put_contents("debug.txt", "==========================\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($result)."\r\n",FILE_APPEND);
		
		curl_close($ch);
	}
	
	//是否本人
	function checkPerson($content_type, $message, $page, $shiftId, $date, $team, $class, $timeId, $teamXid) {

	 	global $header, $reToken, $receive, $channel_access_token, $DB, $pdoDB, $from;
	 	
		$url = "https://api.line.me/v2/bot/message/reply";
		
		$data = ["replyToken" => $reToken, "messages" => array(["type" => "text", "text" => $message])];
		
		$today = strtotime(date('Y/m/d'));
		$registerDate = strtotime(str_replace('-', '/',substr($date,0,10)));
		$shiftRow = explode('~', substr($date,16));
		if($today == $registerDate) {
			$registerDateS = strtotime(date('Y/m/d').$shiftRow[0]);
			$registerDateE = strtotime(date('Y/m/d').$shiftRow[1]);
			if(time() + 600 >= $registerDateE) {
				if(1) { 
					$messages = array(
						array(
							'type' => 'text',
							'text' => "此時段已過~請選擇正確時段",
						),
					);
					$messages = ($text2) ? (array_merge($messages, $messages2)) : $messages;
				}
				// 回覆訊息
				reply('text', $messages);
				return;
			}
		}
		/*
		$_timeAry = array(
			'10:30~11:30',
			'11:30~12:30',
			'12:30~13:30',
			'13:30~14:30',
			'14:30~15:30',
			'15:30~16:30',
			'16:30~17:30',
			'17:30~18:30',
			'18:30~19:30',
		);
		*/
		$_timeAry = array(
			'10:30~11:00',
			'11:00~11:30',
			'11:30~12:00',
			'12:00~12:30',
			'12:30~13:00',
			'13:00~13:30',
			'13:30~14:00',
			'14:00~14:30',
			'14:30~15:00',
			'15:00~15:30',
			'15:30~16:00',
			'16:00~16:30',
			'16:30~17:00',
			'17:00~17:30',
			'17:30~18:00',
			'18:00~18:30',
			'18:30~19:00',
			'19:00~19:30',
		);
		//已預約的時段
		$registerTimeRow = array();
		$isSql = "
			SELECT 
				main.*,
				xteam.subject as xteamSubject,
				xteam.time as xteamTime
			from 
				web_x_register as main
			left join
				web_x_team xteam
			on
				xteam.web_x_team_id = main.web_x_team_id
			WHERE 
				main.registerDate >= '".date('Y-m-d', $registerDate)."'
			AND
				main.web_time_id = '".$timeId."'
			AND
				main.paymentstatus != '取消' 
			AND
				main.paymentstatus != '已報到'
			AND
				main.paymentstatus != '已服務'	
		";
		//echo $isSql;
		$pdo3 = $pdoDB->prepare($isSql);
		$pdo3->execute();
		$registerRow = $pdo3->fetchAll(PDO::FETCH_ASSOC);

		foreach($registerRow as $registerKey => $registerVal) {
			//echo array_search(substr($registerVal['subject'],16), $_timeAry);
			if(array_search(substr($registerVal['subject'],16), $_timeAry) >= '0') {
				if($_timeAry[array_search(substr($registerVal['subject'],16), $_timeAry)] == substr($registerVal['subject'],16)) {
					$registerTimeRow[$registerVal['web_time_id']][$registerVal['registerDate']][] = substr($registerVal['subject'],16);
					if($registerVal['xteamTime'] > 1) {
						for($i = 2; $i <= $registerVal['xteamTime']; $i++) {
							$searchKey = array_search(substr($registerVal['subject'],16), $_timeAry) + ($i - 1);
							$registerTimeRow[$registerVal['web_time_id']][$registerVal['registerDate']][] = $_timeAry[$searchKey];
						}	
					}
				}	
				
			}
		}
		//已預約的時段
		/*
		echo $date;
		
		echo "<pre>";
		print_r($registerTimeRow);
		echo "</pre>";
		*/
		$_teamTime = 0;
		$teamSql = "select time from web_x_team where web_x_team_id = ".$teamXid;
		$pdo4 = $pdoDB->prepare($teamSql);
		$pdo4->execute();
		$teamRow = $pdo4->fetch(PDO::FETCH_ASSOC);
		
		//看時段是否已有人預約及計算時間是否足夠
		$bookingTimeRow = array();
		$bookingTimeRow[$timeId][date('Y-m-d', $registerDate)][] = $shiftRow[0]."~".$shiftRow[1];
		if($teamRow['time'] >= 1) {
			for($i = 2; $i <= $teamRow['time'] * 2; $i++) {
				$searchKey = array_search(substr($date,16), $_timeAry) + ($i - 1);
				$bookingTimeRow[$timeId][date('Y-m-d', $registerDate)][] = $_timeAry[$searchKey];
			}	
		}
		/*
		echo "<pre>";
		print_r($bookingTimeRow);
		echo "</pre>";
		*/
		$bookingMustTimeCount = 0;
		foreach($bookingTimeRow[$timeId][date('Y-m-d', $registerDate)] as $key => $bookingMustTime) {
			//找不到時段可能不已關店
			if(!$bookingMustTime) {
				$bookingMustTimeCount ++;	
			}	
			if(in_array($bookingMustTime, $registerTimeRow[$timeId][date('Y-m-d', $registerDate)])) {
				$bookingMustTimeCount ++;
			}
		}
		//echo $bookingMustTimeCount;
		//看時段是否已有人預約及計算時間是否足夠
		
		
		if(!$bookingMustTimeCount) {		//沒衝突時段可約
			$prodAry[0] = array(
				"title" => "請問您是：",
				"text" => " ",
				"actions" => array(
					array(
						"type" => "uri",
						"label" => "本人要預約",
						"uri" => "line://app/1653388002-015aNXZ2?person=1&shiftId=".$shiftId."&date=".$date."&team=".$team."&class=".$class."&timeId=".$timeId."&teamXid=".$teamXid,
					),
					array(
						"type" => "uri",
						"label" => "幫別人預約",
						"uri" => "line://app/1653388002-015aNXZ2?person=0&shiftId=".$shiftId."&date=".$date."&team=".$team."&class=".$class."&timeId=".$timeId."&teamXid=".$teamXid,
					),
				),
			);	
		} else {
			$prodAry[0] = array(
				"title" => "你預約的時段不足",
				"text" => " ",
				"actions" => array(
					array(
						"type" => "message",
						"label" => "重新預約",
						"text" => "我要預約",
					)
				),
			);	
		}
		
		/*
		$prodAry[0]['type'] = 'bubble';
		$prodAry[0]['body']['type'] = 'box';
		$prodAry[0]['body']['layout'] = 'vertical';
		$prodAry[0]['body']['spacing'] = 'sm';
		$prodAry[0]['body']['contents'][0]['type'] = 'text';
		$prodAry[0]['body']['contents'][0]['text'] = '請問您是：';
		$prodAry[0]['body']['contents'][0]['wrap'] = true;
		$prodAry[0]['body']['contents'][0]['weight'] = "bold";
		$prodAry[0]['body']['contents'][0]['size'] = "xl";
		$prodAry[0]['body']['contents'][0]['color'] = '#905c44';
		
		$prodAry[0]['body']['contents'][1]['type'] = 'separator';
		$prodAry[0]['body']['contents'][1]['margin'] = 'lg';
		
		$prodAry[0]['body']['contents'][2]['type'] = 'box';
		$prodAry[0]['body']['contents'][2]['layout'] = 'vertical';
		$prodAry[0]['body']['contents'][2]['spacing'] = 'sm';
		$prodAry[0]['body']['contents'][2]['contents'][0]['type'] = 'button';
		$prodAry[0]['body']['contents'][2]['contents'][0]['style'] = 'link';
		$prodAry[0]['body']['contents'][2]['contents'][0]['action']['type'] = 'uri';
		$prodAry[0]['body']['contents'][2]['contents'][0]['action']['label'] = '本人要看診';
		$prodAry[0]['body']['contents'][2]['contents'][0]['action']['uri'] = "line://app/1567441648-vk5le0d2?person=1&shiftId=".$shiftId."&date=".$date."&team=".$team."&class=".$class;

		
		$prodAry[0]['footer']['type'] = 'box';
		$prodAry[0]['footer']['layout'] = 'horizontal';
		$prodAry[0]['footer']['spacing'] = 'sm';
		$prodAry[0]['footer']['contents'][0]['type'] = 'button';
		$prodAry[0]['footer']['contents'][0]['style'] = 'primary';
		$prodAry[0]['footer']['contents'][0]['action']['type'] = 'uri';
		$prodAry[0]['footer']['contents'][0]['action']['label'] = '本人要看診';
		$prodAry[0]['footer']['contents'][0]['action']['uri'] = "line://app/1567441648-vk5le0d2?person=1&shiftId=".$shiftId."&date=".$date."&team=".$team."&class=".$class;
		
		$prodAry[0]['footer']['contents'][1]['type'] = 'button';
		$prodAry[0]['footer']['contents'][1]['style'] = 'primary';
		$prodAry[0]['footer']['contents'][1]['color'] = '#905c44';
		$prodAry[0]['footer']['contents'][1]['action']['type'] = 'uri';
		$prodAry[0]['footer']['contents'][1]['action']['label'] = '幫別人看診';
		$prodAry[0]['footer']['contents'][1]['action']['uri'] = "line://app/1567441648-vk5le0d2?person=0&shiftId=".$shiftId."&date=".$date."&team=".$team."&class=".$class;
		*/
		switch($content_type) {
		
			case "text" :
				$content_type = "文字訊息";
				$data = $prodAry;
				
				
				$result = array();
				foreach($data as $key => $row) {
					//if (mb_strpos($message, $row[title]) !== false) {
						array_push($result, $row);
					//}
				}
				
				$result = (!$result[0]) ? $data : $result;
				
				$data = array(
					"replyToken" => $reToken, 
					"messages" =>
						array(
							array(
								'type' => 'template',
								'altText' => '智慧沙龍',
								'template' => array(
									'type' => 'carousel',
									'columns' => $result
								),
							),
						),
				);
				
				/*
				$data = array(
					"replyToken" => $reToken, 
					"messages" =>
						array(
							
							array(
								'type' => 'flex',
								'altText' => '智慧沙龍',
								'contents' => array(
									'type' => 'carousel',
									'contents' => $result
								),
							),
						),
				);
				*/
				//file_put_contents("template.txt", json_encode($data)."\r\n",FILE_APPEND);
				break;
				
			default:
				$content_type = "未知訊息";
				break;
	   	}
		
		$context = stream_context_create(array(
		"http" => array("method" => "POST", "header" => implode(PHP_EOL, $header), "content" => json_encode($data), "ignore_errors" => true)
		));
		//file_put_contents("debug.txt", $url."\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($data)."\r\n",FILE_APPEND);
		//$log = file_get_contents($url, false, $context);
		//file_put_contents("debug.txt", json_encode($log)."\r\n",FILE_APPEND);
		
		//curl設定
		$headers = array(
			"Content-Type: application/json; charser=UTF-8",
			"Authorization: Bearer {" . $channel_access_token . "}"
		);
			 
		//curl
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		//如果失敗可以印出 $result 看錯誤訊息
		$result = curl_exec($ch);
		//file_put_contents("debug.txt", "==========================\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($result)."\r\n",FILE_APPEND);
		
		curl_close($ch);
	}
	
	function getObjContent($filenameExtension){
		
		global $channel_access_token, $receive, $myURL;
		
		$objID = $receive->events[0]->message->id;
		$url = 'https://api.line.me/v2/bot/message/'.$objID.'/content';
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer {' . $channel_access_token . '}',
		));
		
		$json_content = curl_exec($ch);
		curl_close($ch);
		
		if (!$json_content) {
			return false;
		}
		
		$fileURL = './uploadfiles/l/'.$objID.'.'.$filenameExtension;
		//file_put_contents("template.txt", $fileURL."\r\n",FILE_APPEND);
		$fp = fopen($fileURL, 'w');
		fwrite($fp, $json_content);
		fclose($fp);
		
		$fileURL = './uploadfiles/m/'.$objID.'.'.$filenameExtension;
		$fp = fopen($fileURL, 'w');
		fwrite($fp, $json_content);
		fclose($fp);
		
		$fileURL = './uploadfiles/s/'.$objID.'.'.$filenameExtension;
		$fp = fopen($fileURL, 'w');
		fwrite($fp, $json_content);
		fclose($fp);
		/*	
		if ($filenameExtension=="mp3"){
			//使用getID3套件分析mp3資訊
			require_once("getID3/getid3/getid3.php");
			$getID3 = new getID3;
			$fileData = $getID3->analyze($fileURL);
			//$audioInfo = var_dump($fileData);
			$playSec = floor($fileData["playtime_seconds"]);
			$re = array($myURL.$objID.'.'.$filenameExtension, $playSec*1000);
			return $re;
		}
		*/
		//file_put_contents("template.txt", $myURL.$objID.'.'.$filenameExtension."\r\n",FILE_APPEND);
		return $myURL.$objID.'.'.$filenameExtension;
	}
	
	function profile($userId) {
		global $channel_access_token, $receive;
		
		$url = 'https://api.line.me/v2/bot/profile/'.$userId;
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer {' . $channel_access_token . '}',
		));
		
		$json_content = curl_exec($ch);
		curl_close($ch);
		
		if (!$json_content) {
			return false;
		}
		

		file_put_contents("./log/debug.txt", "==========================\r\n",FILE_APPEND);
		file_put_contents("./log/debug.txt", json_encode($json_content)."\r\n",FILE_APPEND);
		
		return $json_content;
	}
	
	function rich_menu_user($richMenuId, $userId) {
		global $channel_access_token, $receive;
		
		$url = 'https://api.line.me/v2/bot/user/'.$userId.'/richmenu/'.$richMenuId;
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			//'Content-Length: 32',
			'Authorization: Bearer '.$channel_access_token,
		));
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		$json_content = curl_exec($ch);
		curl_close($ch);

		
		//file_put_contents("./log/debug/debug_".date('Ymd').".txt", "==========================\r\n",FILE_APPEND);
		//file_put_contents("./log/debug/debug_".date('Ymd').".txt", $url."\r\n",FILE_APPEND);
		//file_put_contents("./log/debug/debug_".date('Ymd').".txt", json_encode($json_content)."\r\n",FILE_APPEND);
		
		
		return $json_content;
		
	}
	
	function rich_menu_user_cancel($userId) {
		global $channel_access_token, $receive;
		
		$url = 'https://api.line.me/v2/bot/user/'.$userId.'/richmenu';
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer {' . $channel_access_token . '}',
		));
		
		$json_content = curl_exec($ch);
		curl_close($ch);
		
		//file_put_contents("./log/debug/debug_".date('Ymd').".txt", "==========================\r\n",FILE_APPEND);
		//file_put_contents("./log/debug/debug_".date('Ymd').".txt", $url."\r\n",FILE_APPEND);
		//file_put_contents("./log/debug/debug_".date('Ymd').".txt", json_encode($json_content)."\r\n",FILE_APPEND);
		return $json_content;
		
	}
	
	function rich_menu_all_default($richMenuId) {
		global $channel_access_token, $receive;
		
		$url = 'https://api.line.me/v2/bot/user/all/richmenu/'.$richMenuId;
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer {' . $channel_access_token . '}',
		));
		
		$json_content = curl_exec($ch);
		curl_close($ch);
		
		return $json_content;
		
	}
	
	function rich_menu_all_default_cancel() {
		global $channel_access_token, $receive;
		
		$url = 'https://api.line.me/v2/bot/user/all/richmenu/';
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer {' . $channel_access_token . '}',
		));
		
		$json_content = curl_exec($ch);
		curl_close($ch);
		
		return $json_content;
		
	}
	
	function rich_menu_list() {
		global $channel_access_token, $receive;
		
		$url = 'https://api.line.me/v2/bot/richmenu/list';
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer {' . $channel_access_token . '}',
		));
		
		$json_content = curl_exec($ch);
		curl_close($ch);
		
		return $json_content;
		
	}
	
	//掛號查詢
	function register_search($content_type, $message, $page, $userId) {

	 	global $header, $reToken, $receive, $channel_access_token, $DB, $pdoDB, $from;
	 	
		$url = "https://api.line.me/v2/bot/message/reply";
		
		$data = ["replyToken" => $reToken, "messages" => array(["type" => "text", "text" => $message])];
		
		$sql = "SELECT web_x_register_id FROM web_x_register where web_x_register.lineID = '".$userId."' AND registerDate >= '".date('Y-m-d')."' AND paymentstatus = '未報到' and concat(SUBSTRING(subject,1,10),' ',SUBSTRING(subject, 21)) > '".date('Y-m-d H:i')."' GROUP BY registerDate ORDER BY subject ASC, registerDate ASC";
		$pdo = $pdoDB->prepare($sql);
	    $pdo->execute();
	    $allrow3 = $pdo->fetchAll(PDO::FETCH_ASSOC);
		//echo $allrow3['Rows'];
		$Init_Page2 = '7';
		$moreFlage2 = 1;
		
		$total_pages2 = ceil(count($allrow3)/$Init_Page2);	//計算總頁數
		if ($page>$total_pages2) {
			$page = 1;		//修正輸入過大的頁數
			$moreFlage2 = 0;
			echo 11111;
		}	
		//$moreFlage2 = ($row['Rows'] > $Init_Page) ? 1 : 0;
		
		$sql = "SELECT registerDate FROM web_x_register where web_x_register.lineID = '".$userId."' AND registerDate >= '".date('Y-m-d')."' AND paymentstatus = '未報到' and concat(SUBSTRING(subject,1,10),' ',SUBSTRING(subject, 21)) > '".date('Y-m-d H:i')."' GROUP BY registerDate ORDER BY subject ASC, registerDate ASC";
		$sql .= " limit ".($page-1) * $Init_Page2.", ".$Init_Page2;
		
		$pdo = $pdoDB->prepare($sql);
	    $pdo->execute();
	    $row3 = $pdo->fetchAll(PDO::FETCH_ASSOC);
		//echo count($row3);
		if(!count($row3)) {
			if(1) { 
				$messages = array(
					array(
						'type' => 'text',
						'text' => "近期無預約資料",
					),
				);
				$messages = ($text2) ? (array_merge($messages, $messages2)) : $messages;
			}
			// 回覆訊息
			reply('text', $messages);
		} else {
		
			$prodAry = array();
			foreach($row3 as $key3 => $registerDate) {
				
				if($key3 > 7) {
					continue;
				}

				$sql = "SELECT COUNT(*) as Rows FROM web_x_register where web_x_register.lineID = '".$userId."' AND registerDate = '".$registerDate['registerDate']."' AND paymentstatus = '未報到' and concat(SUBSTRING(subject,1,10),' ',SUBSTRING(subject, 21)) > '".date('Y-m-d H:i')."' ORDER BY subject ASC, registerDate ASC";
				$pdo = $pdoDB->prepare($sql);
				$pdo->execute();
				$row = $pdo->fetch(PDO::FETCH_ASSOC);
				
				//echo '1111:'.$row['Rows']."</br>";
				
				$Init_Page = '5';
				$moreFlage = 0;
				
				$total_pages = ceil($row['Rows']/$Init_Page);	//計算總頁數
				if ($page>$total_pages) $page = 1;		//修正輸入過大的頁數
				
				$moreFlage = ($row['Rows'] > $Init_Page) ? 1 : 0;
			
				$sql = "
					SELECT 
						SQL_CALC_FOUND_ROWS a.*,
						c.subject as classSubject,
						d.subject as teamSubject,
						d.time as teamTime,
						e.subject as esubject
					FROM 
						web_x_register a
					Left Join 
						web_class c ON c.web_class_id = a.web_time_id
					Left Join 
						web_x_class e ON e.web_x_class_id = c.web_x_class_id	
					Left Join 
						web_x_team d ON d.web_x_team_id = a.web_x_team_id			
					where 
						a.lineID = '".$userId."' 
					AND 
						a.registerDate = '".$registerDate['registerDate']."' 
					AND 
						a.paymentstatus = '未報到' 
					AND
						concat(SUBSTRING(a.subject,1,10),' ',SUBSTRING(a.subject, 21)) > '".date('Y-m-d H:i')."'	
					ORDER BY 
						a.subject ASC,
						a.registerDate ASC
				";
				$sql .= " limit ".($page-1) * $Init_Page.", ".$Init_Page;
				//echo $sql;
				//echo "</br>";
				$pdo = $pdoDB->prepare($sql);
				$pdo->execute();
				$row = $pdo->fetchAll(PDO::FETCH_ASSOC);
				//echo date('N');
				
				$timeAry = array();
				foreach($row as $key => $timeList) {

					$timeAry[$registerDate['registerDate']][] = $timeList;
				}		
				
				end($timeAry);
				$lastKey = key($timeAry);
				/*
				echo "<pre>";
				print_r($timeAry);
				echo "</pre>";
				*/
				$targetKey = array(
					//'web_team_id',
					'subject',
					//'price_member',
					'Files',

				);
				foreach($timeAry as $key => $prodInfo) {
					/*
					$_day = ($key < date('N')) ? '+'.ceil((7 - date('N')) + $key) : '+'.ceil($key - date('N'));
					$keyDay = $key - date('N');
					$keyDay = ($keyDay < 0) ? ceil(7 - abs($keyDay)): $keyDay;
					*/
					$dayName = array(
						1 => '一', 2 => '二', 3 => '三', 4 => '四', 5 => '五', 6 => '六', 7 => '日',
					);
					
					$prodAry[$key]['type'] = 'bubble';
					$prodAry[$key]['styles']['body']['backgroundColor'] = '#FFFFFF';
					$prodAry[$key]['styles']['footer']['backgroundColor'] = '#FFFFFF';
					$prodAry[$key]['body']['type'] = 'box';
					$prodAry[$key]['body']['layout'] = 'vertical';
					$prodAry[$key]['body']['spacing'] = 'sm';
					$prodAry[$key]['body']['contents'][0]['type'] = 'text';
					$prodAry[$key]['body']['contents'][0]['text'] = "近期預約資料 :";
					$prodAry[$key]['body']['contents'][0]['wrap'] = true;
					$prodAry[$key]['body']['contents'][0]['weight'] = "bold";
					$prodAry[$key]['body']['contents'][0]['size'] = "xl";
					$prodAry[$key]['body']['contents'][1]['type'] = 'text';
					$prodAry[$key]['body']['contents'][1]['text'] = $key."(".$dayName[date('N', strtotime($key))].")";
					$prodAry[$key]['body']['contents'][1]['wrap'] = true;
					$prodAry[$key]['body']['contents'][1]['weight'] = "bold";
					$prodAry[$key]['body']['contents'][1]['size'] = "sm";
					
					$prodAry[$key]['body']['contents'][2]['type'] = 'separator';
					
					//$j = count($prodAry[$key]['body']['contents']);
					foreach($prodInfo as $key2 => $prod) {
						/*
						$prodAry[$key][title] = "近期掛號資料 :";
						$prodAry[$key][text] = $key."(".$dayName[date('N', strtotime($key))].")";
						$prodAry[$key][actions][$key2]['type'] = 'postback';
						$prodAry[$key][actions][$key2]['label'] = $prod['classSubjct']." ".$prod['teamSubject']." 醫師";
						$prodAry[$key][actions][$key2]['data'] = "action=reservation&item=registerSearch&Id=".$prod['web_x_register_id'];
						*/

						
						$j = count($prodAry[$key]['body']['contents']);

						$prodAry[$key]['body']['contents'][$j]['type'] = 'box';
						$prodAry[$key]['body']['contents'][$j]['layout'] = 'horizontal';
						$prodAry[$key]['body']['contents'][$j]['spacing'] = 'sm';
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['type'] = 'box';
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['layout'] = 'vertical';
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['flex'] = 5;
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][0]['type'] = 'text';
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][0]['size'] = 'sm';
						//$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][0]['text'] = $prod['teamSubject']." ".$prod['teamTime']."HR";
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][0]['text'] = $prod['teamSubject'];
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][0]['align'] = 'start';
						
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][1]['type'] = 'box';
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][1]['layout'] = 'baseline';
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][1]['spacing'] = 'sm';
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][1]['contents'][0]['type'] = 'icon';
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][1]['contents'][0]['url'] = 'https://scdn.line-apps.com/n/channel_devcenter/img/fx/review_gold_star_28.png';
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][1]['contents'][0]['size'] = 'sm';
						
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][1]['contents'][1]['type'] = 'text';
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][1]['contents'][1]['text'] = "門市 : ";
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][1]['contents'][2]['size'] = 'sm';
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][1]['contents'][1]['flex'] = 2;
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][1]['contents'][1]['align'] = 'start';
						
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][1]['contents'][2]['type'] = 'text';
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][1]['contents'][2]['size'] = 'sm';
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][1]['contents'][2]['text'] = $prod['esubject'];
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][1]['contents'][2]['flex'] = 4;
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][1]['contents'][2]['align'] = 'start';
						
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][2]['type'] = 'box';
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][2]['layout'] = 'baseline';
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][2]['spacing'] = 'sm';
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][2]['contents'][0]['type'] = 'icon';
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][2]['contents'][0]['url'] = 'https://scdn.line-apps.com/n/channel_devcenter/img/fx/review_gold_star_28.png';
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][2]['contents'][0]['size'] = 'sm';
						
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][2]['contents'][1]['type'] = 'text';
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][2]['contents'][1]['text'] = "到店時間 : ";
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][2]['contents'][2]['size'] = 'sm';
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][2]['contents'][1]['flex'] = 4;
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][2]['contents'][1]['align'] = 'start';
						
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][2]['contents'][2]['type'] = 'text';
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][2]['contents'][2]['size'] = 'sm';
						//會根據時段加時間
						if($prod['teamTime'] > 1 && 0) {
							$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][2]['contents'][2]['text'] = substr($prod['subject'],16, 6).ceil(substr($prod['subject'],22, 2)+($prod['teamTime']-1)).substr($prod['subject'],24);
						} else {
							$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][2]['contents'][2]['text'] = substr($prod['subject'],16,5);
						}	
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][2]['contents'][2]['flex'] = 3;
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][2]['contents'][2]['align'] = 'start';

						
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][3]['type'] = 'box';
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][3]['layout'] = 'baseline';
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][3]['spacing'] = 'sm';
						
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][3]['contents'][0]['type'] = 'icon';
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][3]['contents'][0]['url'] = 'https://scdn.line-apps.com/n/channel_devcenter/img/fx/review_gold_star_28.png';
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][3]['contents'][0]['size'] = 'sm';
						
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][3]['contents'][1]['type'] = 'text';
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][3]['contents'][1]['text'] = "設計師 : ";
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][3]['contents'][1]['flex'] = 3;
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][3]['contents'][1]['align'] = 'start';
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][3]['contents'][2]['type'] = 'text';
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][3]['contents'][2]['color'] = "#ff0000";
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][3]['contents'][2]['text'] = $prod['classSubject'];
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][3]['contents'][2]['flex'] = 4;
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][3]['contents'][2]['size'] = 'sm';
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][3]['contents'][2]['weight'] = "bold";
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][3]['contents'][2]['align'] = 'start';
						
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][4]['type'] = 'box';
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][4]['layout'] = 'baseline';
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][4]['spacing'] = 'sm';
						
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][4]['contents'][0]['type'] = 'icon';
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][4]['contents'][0]['url'] = 'https://scdn.line-apps.com/n/channel_devcenter/img/fx/review_gold_star_28.png';
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][4]['contents'][0]['size'] = 'sm';
						
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][4]['contents'][1]['type'] = 'text';
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][4]['contents'][1]['text'] = "消費人 : ";
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][4]['contents'][1]['flex'] = 4;
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][4]['contents'][1]['align'] = 'start';
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][4]['contents'][2]['type'] = 'text';
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][4]['contents'][2]['color'] = "#ff0000";
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][4]['contents'][2]['text'] = $prod['accept_name'];
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][4]['contents'][2]['flex'] = 6;
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][4]['contents'][2]['size'] = 'sm';
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][4]['contents'][2]['weight'] = "bold";
						$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][4]['contents'][2]['align'] = 'start';
						
						
						$prodAry[$key]['body']['contents'][$j]['contents'][1]['type'] = 'separator';
						
						$prodAry[$key]['body']['contents'][$j]['contents'][2]['type'] = 'button';
						$prodAry[$key]['body']['contents'][$j]['contents'][2]['flex'] = 3;
						$prodAry[$key]['body']['contents'][$j]['contents'][2]['height'] = 'sm';
						$prodAry[$key]['body']['contents'][$j]['contents'][2]['gravity'] = 'center';
						$prodAry[$key]['body']['contents'][$j]['contents'][2]['style'] = 'primary';
						$prodAry[$key]['body']['contents'][$j]['contents'][2]['color'] = '#444444';
						$prodAry[$key]['body']['contents'][$j]['contents'][2]['action']['type'] = 'postback';
						$prodAry[$key]['body']['contents'][$j]['contents'][2]['action']['label'] = "取消";
						//$prodAry[$key]['body']['contents'][$j]['contents'][2]['action']['data'] = "action=reservation&item=registerSearch&Id=".$prod['web_x_register_id'];
						$prodAry[$key]['body']['contents'][$j]['contents'][2]['action']['data'] ="action=reservation&item=registerCancel&Id=".$prod['web_x_register_id'];
						$prodAry[$key]['body']['contents'][$j]['contents'][2]['action']['displayText'] = '取消';
						//$j;
						$prodAry[$key]['body']['contents'][$j + 1]['type'] = 'separator';
						
						
						$prodAry[$key]['footer']['type'] = 'box';
						$prodAry[$key]['footer']['layout'] = 'vertical';
						$prodAry[$key]['footer']['spacing'] = 'sm';
						/*
						$prodAry[$key]['footer']['contents'][$key2]['type'] = 'button';
						$prodAry[$key]['footer']['contents'][$key2]['style'] = 'primary';
						$prodAry[$key]['footer']['contents'][$key2]['gravity'] = 'center';
						$prodAry[$key]['footer']['contents'][$key2]['action']['type'] = 'postback';
						$prodAry[$key]['footer']['contents'][$key2]['action']['label'] = $prod['classSubjct']." ".$prod['teamSubject']." 醫師";
						$prodAry[$key]['footer']['contents'][$key2]['action']['data'] = "action=reservation&item=registerSearch&Id=".$prod['web_x_register_id'];
						*/
						
						
						
					}
					
					
					$footerKey = 0;
					if(1){
						if($moreFlage) {
							/*
							$prodAry[$key][actions][$key2+1]['type'] = 'postback';
							$prodAry[$key][actions][$key2+1]['label'] = "more";
							$prodAry[$key][actions][$key2+1]['data'] = "action=reservation&itme=registerSearchMore4&date=".$key."&page=".($page+1);
							*/
							
							$prodAry[$key]['footer']['contents'][$footerKey]['type'] = 'button';
							$prodAry[$key]['footer']['contents'][$footerKey]['height'] = 'sm';
							$prodAry[$key]['footer']['contents'][$footerKey]['style'] = 'primary';
							$prodAry[$key]['footer']['contents'][$footerKey]['color'] = '#444444';
							$prodAry[$key]['footer']['contents'][$footerKey]['gravity'] = 'center';
							$prodAry[$key]['footer']['contents'][$footerKey]['action']['type'] = 'postback';
							$prodAry[$key]['footer']['contents'][$footerKey]['action']['label'] = "more";
							$prodAry[$key]['footer']['contents'][$footerKey]['action']['data'] = "action=reservation&itme=registerSearchMore4&date=".$key."&page=".($page+1);
							$footerKey ++;
						} else {
							if(0) {
								//for($j=count($prodAry[$key][actions]); $j<3; $j++) {
								for($j=count($prodAry[$key]['footer']['contents']); $j<3; $j++) {	
									/*
									$prodAry[$key][actions][$j]['type'] = 'message';
									$prodAry[$key][actions][$j]['label'] = " ";
									$prodAry[$key][actions][$j]['text'] = " ";
									*/
									$prodAry[$key]['footer']['contents'][$j]['type'] = 'button';
									$prodAry[$key]['footer']['contents'][$j]['height'] = 'sm';
									$prodAry[$key]['footer']['contents'][$j]['style'] = 'primary';
									$prodAry[$key]['footer']['contents'][$j]['color'] = '#ffffff';
									$prodAry[$key]['footer']['contents'][$j]['action']['type'] = 'message';
									$prodAry[$key]['footer']['contents'][$j]['action']['label'] = " ";
									$prodAry[$key]['footer']['contents'][$j]['action']['text'] = " ";
								}
							}	
						}
					}	

					$prodAry[$key]['footer']['contents'][$footerKey]['type'] = 'text';
					$prodAry[$key]['footer']['contents'][$footerKey]['text'] = "Design by aibitechcology";
					$prodAry[$key]['footer']['contents'][$footerKey]['wrap'] = true;
					$prodAry[$key]['footer']['contents'][$footerKey]['color'] = "#aaaaaa";
					$prodAry[$key]['footer']['contents'][$footerKey]['size'] = "xxs";
					$prodAry[$key]['footer']['contents'][$footerKey]['align'] = "end";	
					
					
				}

				//ksort($prodAry);
				
			}	
		}

		if(($total_pages2 > 1)) {
			//echo $page."==".$total_pages2."==".$moreFlage2;
			//if($moreFlage2 != '0') {
				$prodAry[$key + 1] = array(
					"type" => "bubble",
					"body" => array(
						"type" => "box",
						"layout" => "vertical",
						"spacing" => "sm",
						"contents" => array(
							array(
								"type" => "button",
								"flex" => 1,
								"gravity" => "center",
								"action" => array(
									"type" => "postback",
									"label" => "想看更多請點我",
									"data" => "action=more8&userId=".$userId."&page=".($page+1),
									
								),
							)	
						)	
					)	
				);
			//}	
		}		
		
		//echo json_encode($prodAry);
		switch($content_type) {
		
			case "text" :
				$content_type = "文字訊息";
				$data = $prodAry;
				
				
				$result = array();
				foreach($data as $key => $row) {
					//if (mb_strpos($message, $row[title]) !== false) {
						array_push($result, $row);
					//}
				}
				
				$result = (!$result[0]) ? $data : $result;
				/*
				$data = array(
					"replyToken" => $reToken, 
					"messages" =>
						array(
							array(
								'type' => 'template',
								'altText' => '智慧沙龍',
								'template' => array(
									'type' => 'carousel',
									'columns' => $result
								),
							),
						),
				);
				*/
				$data = array(
					"replyToken" => $reToken, 
					"messages" =>
						array(
							
							array(
								'type' => 'flex',
								'altText' => '智慧沙龍',
								'contents' => array(
									'type' => 'carousel',
									'contents' => $result
								),
							),
						),
				);
				//file_put_contents("template.txt", json_encode($data)."\r\n",FILE_APPEND);
				break;
				
			
			default:
				$content_type = "未知訊息";
				break;
	   	}
		
		$context = stream_context_create(array(
		"http" => array("method" => "POST", "header" => implode(PHP_EOL, $header), "content" => json_encode($data), "ignore_errors" => true)
		));
		//file_put_contents("debug.txt", $url."\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($data)."\r\n",FILE_APPEND);
		//$log = file_get_contents($url, false, $context);
		//file_put_contents("debug.txt", json_encode($log)."\r\n",FILE_APPEND);
		
		//curl設定
		$headers = array(
			"Content-Type: application/json; charser=UTF-8",
			"Authorization: Bearer {" . $channel_access_token . "}"
		);
			 
		//curl
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		//如果失敗可以印出 $result 看錯誤訊息
		$result = curl_exec($ch);
		//file_put_contents("debug.txt", "==========================\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($result)."\r\n",FILE_APPEND);
		
		curl_close($ch);
	}
	
	//掛號查詢-more
	function register_search_more($content_type, $message, $page, $date, $userId) {

	 	global $header, $reToken, $receive, $channel_access_token, $DB, $pdoDB, $from;
		
		//file_put_contents("debug.txt", "***".$userId."\r\n",FILE_APPEND);
	 	
		$url = "https://api.line.me/v2/bot/message/reply";
		
		$data = ["replyToken" => $reToken, "messages" => array(["type" => "text", "text" => $message])];
				
		$sql = "
			SELECT 
				COUNT(*) as Rows 
			FROM 
				web_x_register 
			where 
				web_x_register.lineID = '".$userId."' 
			AND 
				registerDate = '".$date."' 
			AND 
				paymentstatus = '未報到' 
			AND 
				concat(SUBSTRING(subject,1,10),' ',SUBSTRING(subject, 21)) > '".date('Y-m-d H:i')."' 
			ORDER BY 
				subject ASC, registerDate ASC
		";
		$pdo = $pdoDB->prepare($sql);
	    $pdo->execute();
	    $row = $pdo->fetch(PDO::FETCH_ASSOC);

		$Init_Page = '5';
		$moreFlage = 0;
		
		$total_pages = ceil($row[Rows]/$Init_Page);	//計算總頁數
		if ($page>$total_pages) $page = 1;		//修正輸入過大的頁數
		
		//$moreFlage = ($row[Rows] > $Init_Page) ? 1 : 0;
	
		$sql = "
			SELECT 
				SQL_CALC_FOUND_ROWS a.*,
				c.subject as classSubject,
				d.subject as teamSubject,
				d.time as teamTime,
				e.subject as esubject
			FROM 
				web_x_register a
			Left Join 
				web_class c ON c.web_class_id = a.web_time_id
			Left Join 
				web_x_class e ON e.web_x_class_id = c.web_x_class_id		
			Left Join 
				web_x_team d ON d.web_x_team_id = a.web_x_team_id			
			where 
				a.lineID = '".$userId."' 
			AND 
				a.registerDate = '".$date."' 
			AND 
				a.paymentstatus = '未報到' 
			AND
				concat(SUBSTRING(a.subject,1,10),' ',SUBSTRING(a.subject, 21)) > '".date('Y-m-d H:i')."'	
			ORDER BY 
				a.subject ASC,
				a.registerDate ASC
		";
		$sql .= " limit ".($page-1) * $Init_Page.", ".$Init_Page;
		//echo $sql;
		$pdo = $pdoDB->prepare($sql);
	    $pdo->execute();
	    $row = $pdo->fetchAll(PDO::FETCH_ASSOC);
		
		end($row);
		$lastKey = key($row);
	    
	    $prodAry = array();
	    $targetKey = array(
	    	'web_x_register_id',
	    	'subject',
			'Rows',
			//'classSubject',
			//'teamSubject',
			'ordernum',
	    	//'price_member',
	    	//'Files',

	    );
		$dayName = array(
			1 => '一', 2 => '二', 3 => '三', 4 => '四', 5 => '五', 6 => '六', 7 => '日',
		);
		$key = 0;
		$prodAry[$key]['type'] = 'bubble';
		$prodAry[$key]['styles']['body']['backgroundColor'] = '#FFFFFF';
		$prodAry[$key]['styles']['footer']['backgroundColor'] = '#FFFFFF';
		$prodAry[$key]['body']['type'] = 'box';
		$prodAry[$key]['body']['layout'] = 'vertical';
		$prodAry[$key]['body']['spacing'] = 'sm';
		$prodAry[$key]['body']['contents'][0]['type'] = 'text';
		$prodAry[$key]['body']['contents'][0]['text'] = "近期預約資料 :";
		$prodAry[$key]['body']['contents'][0]['wrap'] = true;
		$prodAry[$key]['body']['contents'][0]['weight'] = "bold";
		$prodAry[$key]['body']['contents'][0]['size'] = "xl";
		$prodAry[$key]['body']['contents'][1]['type'] = 'text';
		$prodAry[$key]['body']['contents'][1]['text'] = $date."(".$dayName[date('N', strtotime(str_replace('-', '/', $date)))].")";
		$prodAry[$key]['body']['contents'][1]['wrap'] = true;
		$prodAry[$key]['body']['contents'][1]['weight'] = "bold";
		$prodAry[$key]['body']['contents'][1]['size'] = "sm";
		
		$prodAry[$key]['body']['contents'][2]['type'] = 'separator';

	    foreach($row as $key3 => $prodInfo) {
						
	    	
	    		
				/*
	    		$prodAry[$key] = array(
	    			
					'type' => 'postback',
					'label' => $classSubject." ".$teamSubject." 醫師",
					"data" => "action=reservation&item=registerSearch&Id=".$cid,
					
	    		);
				*/
				
				
				$j = count($prodAry[$key]['body']['contents']);

				$prodAry[$key]['body']['contents'][$j]['type'] = 'box';
				$prodAry[$key]['body']['contents'][$j]['layout'] = 'horizontal';
				$prodAry[$key]['body']['contents'][$j]['spacing'] = 'sm';
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['type'] = 'box';
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['layout'] = 'vertical';
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['flex'] = 5;
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][0]['type'] = 'text';
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][0]['size'] = 'sm';
				//$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][0]['text'] = $prod['teamSubject']." ".$prod['teamTime']."HR";
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][0]['text'] = $prodInfo['teamSubject'];
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][0]['align'] = 'start';
				
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][1]['type'] = 'box';
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][1]['layout'] = 'baseline';
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][1]['spacing'] = 'sm';
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][1]['contents'][0]['type'] = 'icon';
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][1]['contents'][0]['url'] = 'https://scdn.line-apps.com/n/channel_devcenter/img/fx/review_gold_star_28.png';
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][1]['contents'][0]['size'] = 'sm';
				
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][1]['contents'][1]['type'] = 'text';
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][1]['contents'][1]['text'] = "門市 : ";
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][1]['contents'][2]['size'] = 'sm';
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][1]['contents'][1]['flex'] = 2;
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][1]['contents'][1]['align'] = 'start';
				
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][1]['contents'][2]['type'] = 'text';
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][1]['contents'][2]['size'] = 'sm';
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][1]['contents'][2]['text'] = $prodInfo['esubject'];
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][1]['contents'][2]['flex'] = 4;
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][1]['contents'][2]['align'] = 'start';
				
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][2]['type'] = 'box';
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][2]['layout'] = 'baseline';
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][2]['spacing'] = 'sm';
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][2]['contents'][0]['type'] = 'icon';
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][2]['contents'][0]['url'] = 'https://scdn.line-apps.com/n/channel_devcenter/img/fx/review_gold_star_28.png';
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][2]['contents'][0]['size'] = 'sm';
				
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][2]['contents'][1]['type'] = 'text';
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][2]['contents'][1]['text'] = "到店時間 : ";
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][2]['contents'][2]['size'] = 'sm';
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][2]['contents'][1]['flex'] = 4;
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][2]['contents'][1]['align'] = 'start';
				
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][2]['contents'][2]['type'] = 'text';
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][2]['contents'][2]['size'] = 'sm';
				if($prodInfo['teamTime'] > 1 && 0) {
					$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][2]['contents'][2]['text'] = substr($prodInfo['subject'],16, 6).ceil(substr($prodInfo['subject'],22, 2)+($prodInfo['teamTime']-1)).substr($prodInfo['subject'],24);
				} else {
					$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][2]['contents'][2]['text'] = substr($prodInfo['subject'],16, 5);
				}	
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][2]['contents'][2]['flex'] = 3;
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][2]['contents'][2]['align'] = 'start';

				
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][3]['type'] = 'box';
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][3]['layout'] = 'baseline';
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][3]['spacing'] = 'sm';
				
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][3]['contents'][0]['type'] = 'icon';
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][3]['contents'][0]['url'] = 'https://scdn.line-apps.com/n/channel_devcenter/img/fx/review_gold_star_28.png';
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][3]['contents'][0]['size'] = 'sm';
				
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][3]['contents'][1]['type'] = 'text';
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][3]['contents'][1]['text'] = "設計師 : ";
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][3]['contents'][1]['flex'] = 3;
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][3]['contents'][1]['align'] = 'start';
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][3]['contents'][2]['type'] = 'text';
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][3]['contents'][2]['color'] = "#ff0000";
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][3]['contents'][2]['text'] = $prodInfo['classSubject'];
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][3]['contents'][2]['flex'] = 4;
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][3]['contents'][2]['size'] = 'sm';
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][3]['contents'][2]['weight'] = "bold";
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][3]['contents'][2]['align'] = 'start';
				
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][4]['type'] = 'box';
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][4]['layout'] = 'baseline';
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][4]['spacing'] = 'sm';
				
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][4]['contents'][0]['type'] = 'icon';
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][4]['contents'][0]['url'] = 'https://scdn.line-apps.com/n/channel_devcenter/img/fx/review_gold_star_28.png';
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][4]['contents'][0]['size'] = 'sm';
				
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][4]['contents'][1]['type'] = 'text';
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][4]['contents'][1]['text'] = "消費人 : ";
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][4]['contents'][1]['flex'] = 4;
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][4]['contents'][1]['align'] = 'start';
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][4]['contents'][2]['type'] = 'text';
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][4]['contents'][2]['color'] = "#ff0000";
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][4]['contents'][2]['text'] = $prodInfo['accept_name'];
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][4]['contents'][2]['flex'] = 6;
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][4]['contents'][2]['size'] = 'sm';
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][4]['contents'][2]['weight'] = "bold";
				$prodAry[$key]['body']['contents'][$j]['contents'][0]['contents'][4]['contents'][2]['align'] = 'start';
				
				$prodAry[$key]['body']['contents'][$j]['contents'][1]['type'] = 'separator';
				
				$prodAry[$key]['body']['contents'][$j]['contents'][2]['type'] = 'button';
				$prodAry[$key]['body']['contents'][$j]['contents'][2]['flex'] = 3;
				$prodAry[$key]['body']['contents'][$j]['contents'][2]['height'] = 'sm';
				$prodAry[$key]['body']['contents'][$j]['contents'][2]['gravity'] = 'center';
				$prodAry[$key]['body']['contents'][$j]['contents'][2]['style'] = 'primary';
				$prodAry[$key]['body']['contents'][$j]['contents'][2]['color'] = '#444444';
				$prodAry[$key]['body']['contents'][$j]['contents'][2]['action']['type'] = 'postback';
				$prodAry[$key]['body']['contents'][$j]['contents'][2]['action']['label'] = "取消";
				//$prodAry[$key]['body']['contents'][$j]['contents'][2]['action']['data'] = "action=reservation&item=registerSearch&Id=".$prod['web_x_register_id'];
				$prodAry[$key]['body']['contents'][$j]['contents'][2]['action']['data'] ="action=reservation&item=registerCancel&Id=".$prodInfo['web_x_register_id'];
				$prodAry[$key]['body']['contents'][$j]['contents'][2]['action']['displayText'] = '取消';
				//$j;
				$prodAry[$key]['body']['contents'][$j + 1]['type'] = 'separator';
				
				/*
				$prodAry[$key]['footer']['type'] = 'box';
				$prodAry[$key]['footer']['layout'] = 'vertical';
				$prodAry[$key]['footer']['spacing'] = 'sm';
				*/
	    	
	    }

		$footerKey = 0;
		
		if($total_pages > 1) {
			if($page < $total_pages) {
				/*
				$prodAry[$lastKey + 1] = array(
					'type' => 'postback',
					'label' => 'more',
					"data" => "action=reservation&itme=registerSearchMore4&date=".$date."&page=".($page+1),
				);
				*/
				$prodAry[$key]['footer']['contents'][$footerKey]['type'] = 'button';
				$prodAry[$key]['footer']['contents'][$footerKey]['height'] = 'sm';
				$prodAry[$key]['footer']['contents'][$footerKey]['style'] = 'primary';
				$prodAry[$key]['footer']['contents'][$footerKey]['color'] = '#444444';
				$prodAry[$key]['footer']['contents'][$footerKey]['gravity'] = 'center';
				$prodAry[$key]['footer']['contents'][$footerKey]['action']['type'] = 'postback';
				$prodAry[$key]['footer']['contents'][$footerKey]['action']['label'] = "more";
				$prodAry[$key]['footer']['contents'][$footerKey]['action']['data'] = "action=reservation&itme=registerSearchMore4&date=".$date."&page=".($page+1);
				$footerKey ++;
			}	
			
		}	
		
		$prodAry[$key]['footer']['type'] = 'box';
		$prodAry[$key]['footer']['layout'] = 'vertical';
		$prodAry[$key]['footer']['spacing'] = 'sm';
		$prodAry[$key]['footer']['contents'][$footerKey]['type'] = 'text';
		$prodAry[$key]['footer']['contents'][$footerKey]['text'] = "Design by aibitechcology";
		$prodAry[$key]['footer']['contents'][$footerKey]['wrap'] = true;
		$prodAry[$key]['footer']['contents'][$footerKey]['color'] = "#aaaaaa";
		$prodAry[$key]['footer']['contents'][$footerKey]['size'] = "xxs";
		$prodAry[$key]['footer']['contents'][$footerKey]['align'] = "end";
		echo "<pre>";
		print_r($prodAry);
		echo "</pre>";
		switch($content_type) {
		
			case "text" :
				$content_type = "文字訊息";
				
				$data = $prodAry;
				
				$result = array();
				foreach($data as $key => $row) {
					//if (mb_strpos($message, $row[title]) !== false) {
						array_push($result, $row);
					//}
				}				
				
				$result = (!$result[0]) ? $data : $result;
				
				//echo json_encode($result);
				/*
				$data = array(
					"replyToken" => $reToken, 
					"messages" =>
						array(
						
							array(
								'type' => 'template',
								'altText' => '掛號查詢',
								'template' => array(
									'type' => 'buttons',
									'title' => '近期掛號資料 :',
									'text' => $date,
									'actions' => $result
								),
							),
						),
				);
				*/
				
				$data = array(
					"replyToken" => $reToken, 
					"messages" =>
						array(
							
							array(
								'type' => 'flex',
								'altText' => '預約查詢',
								'contents' => array(
									'type' => 'carousel',
									'contents' => $result
								),
							),
						),
				);
				
				
				//file_put_contents("template.txt", json_encode($data)."\r\n",FILE_APPEND);
				break;
				
			
				
			default:
				$content_type = "未知訊息";
				break;
	   	}

		$context = stream_context_create(array(
		"http" => array("method" => "POST", "header" => implode(PHP_EOL, $header), "content" => json_encode($data), "ignore_errors" => true)
		));
		//file_put_contents("debug.txt", $url."\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($data)."\r\n",FILE_APPEND);
		//$log = file_get_contents($url, false, $context);
		//file_put_contents("debug.txt", json_encode($log)."\r\n",FILE_APPEND);
		
		//curl設定
		$headers = array(
			"Content-Type: application/json; charser=UTF-8",
			"Authorization: Bearer {" . $channel_access_token . "}"
		);
			 
		//curl
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		//如果失敗可以印出 $result 看錯誤訊息
		$result = curl_exec($ch);
		//file_put_contents("debug.txt", "==========================\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($result)."\r\n",FILE_APPEND);
		
		curl_close($ch);
	}
	
	//預約課程分類
	function registerSerachById($content_type, $message, $page, $id) {
		
	 	global $header, $reToken, $receive, $channel_access_token, $DB, $pdoDB, $from;
		
		//file_put_contents("debug.txt", $from."---".$id."\r\n",FILE_APPEND);
	 	
		$url = "https://api.line.me/v2/bot/message/reply";
		
		$data = ["replyToken" => $reToken, "messages" => array(["type" => "text", "text" => $message])];
				

		$sql = "
			Select 
				a.*,
				b.web_shift_id,
				b.web_class_id,
				b.web_team_id,
				c.subject as classSubjct,
				d.subject as teamSubject
			From 
				web_x_register a
			Left Join 
				web_shift b ON b.web_shift_id = a.web_shift_id
			Left Join 
				web_class c ON c.web_class_id = b.web_class_id 		
			Left Join 
				web_team d ON d.web_team_id = b.web_team_id
			Where
				a.lineID = '".$from."'
			And	
				a.web_x_register_id = '".$id."'
			order by 
				a.registerDate ASC, b.web_class_id ASC, a.ordernum ASC
		";
		
		$pdo = $pdoDB->prepare($sql);
	    $pdo->execute();
	    $row = $pdo->fetchAll(PDO::FETCH_ASSOC);
		
		
		end($row);
		$lastKey = key($row);
	    
	    $prodAry = array();
	    $targetKey = array(
	    	'web_x_register_id',
	    	'subject',
			'Rows',
			'classSubjct',
			'teamSubject',
			'ordernum',
	    	//'price_member',
	    	//'Files',

	    );
	    foreach($row as $key => $prodInfo) {
	    	foreach($prodInfo as $key2 => $prod) {
	    		if(!in_array($key2, $targetKey)) {
	    			continue;
	    		}
	    		if($key2 == 'subject') {
	    			$subject = $prod;
	    		}
				
				if($key2 == 'web_x_register_id') {
	    			$cid = $prod;
	    		}
				
				if($key2 == 'Rows') {
	    			$Rows = $prod;
	    		}
				
				if($key2 == 'classSubjct') {
	    			$classSubjct = $prod;
	    		}
				
				if($key2 == 'teamSubject') {
	    			$teamSubject = $prod;
	    		}
				
				if($key2 == 'ordernum') {
	    			$ordernum = $prod;
	    		}
	    		
	    		
	    	}
	    	
	    }
		
		$prodAry[0] = array(
	    			
			'type' => 'postback',
			'label' => '我要取消',
			"data" => "action=reservation&item=registerCancel&Id=".$cid,
			
		);
		
		switch($content_type) {
		
			case "text" :
				$content_type = "文字訊息";
				
				$data = $prodAry;
				
				$result = array();
				foreach($data as $key => $row) {
					//if (mb_strpos($message, $row[title]) !== false) {
						array_push($result, $row);
					//}
				}				
				
				$result = (!$result[0]) ? $data : $result;
				
				$data = array(
					"replyToken" => $reToken, 
					"messages" =>
						array(
						
							array(
								'type' => 'template',
								'altText' => '預約查詢',
								'template' => array(
									'type' => 'buttons',
									'title' => '預約內容',
									'text' => $classSubjct."-".$teamSubject." 設計師\n".$subject."\n預約號碼: ".$ordernum,
									'actions' => $result
								),
							),
						),
				);
				
				
				//file_put_contents("template.txt", json_encode($data)."\r\n",FILE_APPEND);
				break;
				
			
				
			default:
				$content_type = "未知訊息";
				break;
	   	}
		
		$context = stream_context_create(array(
		"http" => array("method" => "POST", "header" => implode(PHP_EOL, $header), "content" => json_encode($data), "ignore_errors" => true)
		));
		//file_put_contents("debug.txt", $url."\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($data)."\r\n",FILE_APPEND);
		//$log = file_get_contents($url, false, $context);
		//file_put_contents("debug.txt", json_encode($log)."\r\n",FILE_APPEND);
		
		//curl設定
		$headers = array(
			"Content-Type: application/json; charser=UTF-8",
			"Authorization: Bearer {" . $channel_access_token . "}"
		);
			 
		//curl
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		//如果失敗可以印出 $result 看錯誤訊息
		$result = curl_exec($ch);
		//file_put_contents("debug.txt", "==========================\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($result)."\r\n",FILE_APPEND);
		
		curl_close($ch);
	}
	
	//預約取消
	function registerCancel($content_type, $message, $page, $id) {
		
	 	global $header, $reToken, $receive, $channel_access_token, $DB, $pdoDB, $from;
		
		//file_put_contents("debug.txt", $from."####".$id."\r\n",FILE_APPEND);
	 	
		$url = "https://api.line.me/v2/bot/message/reply";
		
		$data = ["replyToken" => $reToken, "messages" => array(["type" => "text", "text" => $message])];
		
		
		$sql = "
			Select 
				a.*,
				c.subject as classSubject,
				d.subject as teamSubject
			From 
				web_x_register a
			Left Join 
				web_class c ON c.web_class_id = a.web_time_id 		
			Left Join 
				web_x_team d ON d.web_x_team_id = a.web_x_team_id
			Where
				a.lineID = '".$from."'
			And	
				a.web_x_register_id = '".$id."'
			order by 
				a.registerDate ASC, a.ordernum ASC
		";
		
		$pdo = $pdoDB->prepare($sql);
	    $pdo->execute();
	    $row = $pdo->fetchAll(PDO::FETCH_ASSOC);
		
		
		end($row);
		$lastKey = key($row);
	    
	    $prodAry = array();
	    $targetKey = array(
	    	'web_x_register_id',
	    	'subject',
			'Rows',
			'classSubject',
			'teamSubject',
			'ordernum',
			'accept_name',
	    	//'price_member',
	    	//'Files',

	    );
	    foreach($row as $key => $prodInfo) {
	    	foreach($prodInfo as $key2 => $prod) {
	    		if(!in_array($key2, $targetKey)) {
	    			continue;
	    		}
	    		if($key2 == 'subject') {
	    			$subject = $prod;
	    		}
				
				if($key2 == 'web_x_register_id') {
	    			$cid = $prod;
	    		}
				
				if($key2 == 'Rows') {
	    			$Rows = $prod;
	    		}
				
				if($key2 == 'classSubject') {
	    			$classSubject = $prod;
	    		}
				
				if($key2 == 'teamSubject') {
	    			$teamSubject = $prod;
	    		}
				
				if($key2 == 'ordernum') {
	    			$ordernum = $prod;
	    		}
				
				if($key2 == 'accept_name') {
	    			$accept_name = $prod;
	    		}
	    		
	    		
	    	}
	    	
	    }
		
		switch($content_type) {
		
			case "text" :
				$content_type = "文字訊息";
				
				$prodAry[0]['type'] = 'bubble';
				$prodAry[$key]['styles']['body']['backgroundColor'] = '#FFFFFF';
				$prodAry[$key]['styles']['footer']['backgroundColor'] = '#FFFFFF';
				$prodAry[0]['body']['type'] = 'box';
				$prodAry[0]['body']['layout'] = 'vertical';
				$prodAry[0]['body']['spacing'] = 'sm';
				$prodAry[0]['body']['contents'][0]['type'] = 'text';
				$prodAry[0]['body']['contents'][0]['text'] = '取消成功：';
				$prodAry[0]['body']['contents'][0]['wrap'] = true;
				$prodAry[0]['body']['contents'][0]['weight'] = "bold";
				$prodAry[0]['body']['contents'][0]['size'] = "xl";
				$prodAry[0]['body']['contents'][0]['color'] = '#905c44';
				
				$prodAry[0]['body']['contents'][1]['type'] = 'separator';
				$prodAry[0]['body']['contents'][1]['margin'] = 'lg';
				
				$prodAry[0]['body']['contents'][2]['type'] = 'text';
				$prodAry[0]['body']['contents'][2]['text'] = $teamSubject;
				$prodAry[0]['body']['contents'][2]['wrap'] = true;
				$prodAry[0]['body']['contents'][2]['weight'] = "bold";
				$prodAry[0]['body']['contents'][2]['size'] = "sm";
				$prodAry[0]['body']['contents'][2]['color'] = '#000000';
				
				$prodAry[0]['body']['contents'][3]['type'] = 'text';
				$prodAry[0]['body']['contents'][3]['text'] = $subject;
				$prodAry[0]['body']['contents'][3]['wrap'] = true;
				$prodAry[0]['body']['contents'][3]['weight'] = "bold";
				$prodAry[0]['body']['contents'][3]['size'] = "sm";
				$prodAry[0]['body']['contents'][3]['color'] = '#000000';
				
				$prodAry[0]['body']['contents'][4]['type'] = 'text';
				$prodAry[0]['body']['contents'][4]['text'] = '設計師 : '.$classSubject;
				$prodAry[0]['body']['contents'][4]['wrap'] = true;
				$prodAry[0]['body']['contents'][4]['weight'] = "bold";
				$prodAry[0]['body']['contents'][4]['size'] = "sm";
				$prodAry[0]['body']['contents'][4]['color'] = '#000000';
				
				$prodAry[0]['body']['contents'][5]['type'] = 'text';
				$prodAry[0]['body']['contents'][5]['text'] = '顧客姓名 : '.$accept_name;
				$prodAry[0]['body']['contents'][5]['wrap'] = true;
				$prodAry[0]['body']['contents'][5]['weight'] = "bold";
				$prodAry[0]['body']['contents'][5]['size'] = "sm";
				$prodAry[0]['body']['contents'][5]['color'] = '#000000';
				
				$prodAry[0]['footer']['type'] = 'box';
				$prodAry[0]['footer']['layout'] = 'vertical';
				$prodAry[0]['footer']['spacing'] = 'sm';
				$prodAry[0]['footer']['contents'][0]['type'] = 'text';
				$prodAry[0]['footer']['contents'][0]['text'] = "Design by aibitechcology";
				$prodAry[0]['footer']['contents'][0]['wrap'] = true;
				$prodAry[0]['footer']['contents'][0]['color'] = "#aaaaaa";
				$prodAry[0]['footer']['contents'][0]['size'] = "xxs";
				$prodAry[0]['footer']['contents'][0]['align'] = "end";
				

				
				$data = $prodAry;
				
				$result = array();
				foreach($data as $key => $row) {
					//if (mb_strpos($message, $row[title]) !== false) {
						array_push($result, $row);
					//}
				}				
				
				$result = (!$result[0]) ? $data : $result;
				/*
				$data = array(
					"replyToken" => $reToken, 
					"messages" =>
						array(
						
							array(
								'type' => 'text',
								'text' => "取消成功\n\n".$classSubjct."-".$teamSubject." 醫師\n\n".$subject."\n\n掛號號碼: ".$ordernum,
							),
						),
				);
				*/
				$data = array(
					"replyToken" => $reToken, 
					"messages" =>
						array(
							
							array(
								'type' => 'flex',
								'altText' => '取消成功',
								'contents' => array(
									'type' => 'carousel',
									'contents' => $result
								),
							),
						),
				);
				
				
				//file_put_contents("template.txt", json_encode($data)."\r\n",FILE_APPEND);
				break;
				
			
				
			default:
				$content_type = "未知訊息";
				break;
	   	}
		
		$sql = "Update web_x_register set paymentstatus = '取消', cancelUsr = '".$order_name."', cancelTime = '".date('Y-m-d H:i:s')."' where web_x_register_id = '".$id."'";
		$pdo = $pdoDB->prepare($sql);
	    $pdo->execute();
		
		$context = stream_context_create(array(
		"http" => array("method" => "POST", "header" => implode(PHP_EOL, $header), "content" => json_encode($data), "ignore_errors" => true)
		));
		//file_put_contents("debug.txt", $url."\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($data)."\r\n",FILE_APPEND);
		//$log = file_get_contents($url, false, $context);
		//file_put_contents("debug.txt", json_encode($log)."\r\n",FILE_APPEND);
		
		//curl設定
		$headers = array(
			"Content-Type: application/json; charser=UTF-8",
			"Authorization: Bearer {" . $channel_access_token . "}"
		);
			 
		//curl
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		//如果失敗可以印出 $result 看錯誤訊息
		$result = curl_exec($ch);
		//file_put_contents("debug.txt", "==========================\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($result)."\r\n",FILE_APPEND);
		
		curl_close($ch);
		
		//預約取消推播
		if(1) {
			$sql4 = "
				Select 
					a.*,
					c.subject as classSubject,
					c.web_x_class_id as store_id,
					d.subject as teamSubject,
					e.subject as storeSubject
				From 
					web_x_register a
				Left Join 
					web_class c ON c.web_class_id = a.web_time_id 		
				Left Join 
					web_x_team d ON d.web_x_team_id = a.web_x_team_id
				Left Join 
					web_x_class e ON e.web_x_class_id = c.web_x_class_id	
				Where
					a.web_x_register_id = :web_x_register_id
				order by 
					a.registerDate ASC, a.ordernum ASC
			";
			$excute = array(
				':web_x_register_id'        => $id,
			);
			$pdo = $pdoDB->prepare($sql4);
			$pdo->execute($excute);
			$row4 = $pdo->fetchAll(PDO::FETCH_ASSOC);
			
			foreach($row4 as $key4 => $val4) {
				$logInfo['error'] = 0;
				$logInfo['ordernum'] = $val4['ordernum'];
				$logInfo['registerId'] = $val4['web_x_register_id'];
				$logInfo['subject'] = $val4['subject'];
				$logInfo['classSubject'] = $val4['classSubject'];
				$logInfo['teamSubject'] = $val4['teamSubject'];
				$logInfo['store_id'] = $val4['store_id'];
				$logInfo['storeSubject'] = $val4['storeSubject'];
				$logInfo['accept_name'] = $val4['accept_name'];
				$logInfo['web_time_id'] = $val4['web_time_id'];
			}
			//file_put_contents("debug.txt", json_encode($logInfo, JSON_UNESCAPED_UNICODE)."\r\n", FILE_APPEND);
			
			$sql2 = "Select Init_Register_Push From web_basicset Where web_basicset_id = '1' ";
			$pdo2 = $pdoDB->prepare($sql2);
			$pdo2->execute();
			$Init_Register_Push = $pdo2->fetch(PDO::FETCH_ASSOC);
			/*
			$sql = "SELECT GROUP_CONCAT(DISTINCT lineID SEPARATOR ',') AS lineIDAry FROM web_member WHERE lineID != '' AND (ifService IN(1) AND ifMessage IN(1) AND store_id = '".$logInfo['store_id']."') OR (store_id = '-1' AND ifService IN(1) AND ifMessage IN(1))";
			*/
			$sql = "SELECT GROUP_CONCAT(DISTINCT lineID SEPARATOR ',') AS lineIDAry FROM web_member WHERE lineID != '' AND pusher_id = '".$logInfo['web_time_id']."' OR((ifService IN(1) AND ifMessage IN(1) AND store_id = '".$logInfo['store_id']."') OR (store_id = '-1' AND ifService IN(1) AND ifMessage IN(1)))";
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute();
			$lineIDAry = $pdo->fetch(PDO::FETCH_ASSOC);
			//file_put_contents("debug.txt", json_encode($lineIDAry, JSON_UNESCAPED_UNICODE)."\r\n", FILE_APPEND);
			if($Init_Register_Push['Init_Register_Push']) {
				if(count($lineIDAry) && 1) {
					//$text = "客戶【".$logInfo['accept_name']."】預約".$productRow['csubject'].$productRow['subject'];
					$text = "【預約取消通知】\n門市：".$logInfo['storeSubject']."\n項目：".$logInfo['teamSubject']."\n設計師：".$logInfo['classSubject']."\n預約時間：".substr($logInfo['subject'],0,15)."\n到店時間：".substr($logInfo['subject'],16,5)."\n消費人：".$logInfo['accept_name'];
					//file_put_contents("debug.txt", $text."\r\n", FILE_APPEND);	
					$_REQUEST['ID'] = $lineIDAry['lineIDAry'];
					$_REQUEST['contnet'] = $text;
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, "https://".$_SERVER['HTTP_HOST']."/api.php");
					curl_setopt($ch, CURLOPT_HEADER, false);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
					$PostData = $_REQUEST;
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $PostData);
					$temp = curl_exec($ch);
					// 關閉CURL連線
					curl_close($ch);
				}
			}
		}
		
	}
	
	
	
	function getDistance($longitude1, $latitude1, $longitude2, $latitude2, $unit=2, $decimal=2){

		$EARTH_RADIUS = 6370.996; // 地球半径系数
		$PI = 3.1415926;

		$radLat1 = $latitude1 * $PI / 180.0;
		$radLat2 = $latitude2 * $PI / 180.0;

		$radLng1 = $longitude1 * $PI / 180.0;
		$radLng2 = $longitude2 * $PI /180.0;

		$a = $radLat1 - $radLat2;
		$b = $radLng1 - $radLng2;

		$distance = 2 * asin(sqrt(pow(sin($a/2),2) + cos($radLat1) * cos($radLat2) * pow(sin($b/2),2)));
		$distance = $distance * $EARTH_RADIUS * 1000;

		if($unit==2){
			$distance = $distance / 1000;
		}

		return round($distance, $decimal);

	}
	
	function modify_stroe_user($storeTag, $userId) {
		global $DB, $pdoDB;
		
		$sql = "SELECT loginID, web_member_id, uname, sex, mobile, lineID, ifService FROM web_member WHERE lineID like '".$userId."'";
		$pdo = $pdoDB->prepare($sql);
	    $pdo->execute();
	    $row = $pdo->fetch(PDO::FETCH_ASSOC);
		
		if($row) {
			switch($storeTag) {
				case 'A1':
					$sql = "Update web_member set store_id = '3', ifService = '1' where web_member_id = '".$row['web_member_id']."'";
				break;
				
				case 'A2':
					$sql = "Update web_member set store_id = '4', ifService = '1' where web_member_id = '".$row['web_member_id']."'";
				break;
				
				case 'A3':
					$sql = "Update web_member set store_id = '8', ifService = '1' where web_member_id = '".$row['web_member_id']."'";
				break;
				
				case 'A5':
					$sql = "Update web_member set store_id = '5', ifService = '1' where web_member_id = '".$row['web_member_id']."'";
				break;
				
				case 'A6':
					$sql = "Update web_member set store_id = '6', ifService = '1' where web_member_id = '".$row['web_member_id']."'";
				break;
				
				case 'A7':
					$sql = "Update web_member set store_id = '7', ifService = '1' where web_member_id = '".$row['web_member_id']."'";
				break;
				
				case 'A9':
					$sql = "Update web_member set store_id = '9', ifService = '1' where web_member_id = '".$row['web_member_id']."'";
				break;
				
				case 'AT1':
					$sql = "Update web_member set store_id = '1', ifService = '1' where web_member_id = '".$row['web_member_id']."'";
				break;
				
				case 'AT3':
					$sql = "Update web_member set store_id = '2', ifService = '1' where web_member_id = '".$row['web_member_id']."'";
				break;
			}
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute();	
		}
		
	}
