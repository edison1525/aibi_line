<?php
    include_once("./control/includes/function.php");
	ini_set('session.cookie_httponly', 1 );
	session_start();
	//unset($_SESSION);
	if (empty($_SESSION['expire']) || $_SESSION['expire'] < time()) $_SESSION['token'] = md5('aibi' . uniqid(microtime())); // create token (fast/sufficient) 
	$_SESSION['expire'] = time() + 900; // make session valid for next 15 mins
	$_SESSION['visitid'] = $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT'];
	
	$agent = $_SERVER['HTTP_USER_AGENT'];
	if (!preg_match("#\bLine\b#", $agent)) {
		//die('aibi');
	}
	/*
	if($_GET['store']) {
		echo "<script>alert('".$_GET['store']."');</script>";
	}
	*/
	/*
	echo "<pre>";
	print_r($_SESSION);
	echo "</pre>";
	*/
?>	
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo $Init_WebTitle; ?> 會員專區</title>
    <script src="https://d.line-scdn.net/liff/1.0/sdk.js"></script>
    <script src="./js/jquery-1.10.2.min.js"></script>
	<script>
		//init LIFF
        function initializeApp(data) {
            //取得QueryString
            let urlParams = new URLSearchParams(window.location.search);
            //顯示QueryString
            $('#QueryString').val(urlParams.toString());
            //顯示UserId
            $('.userid').val(data.context.userId);
			if(data.context.userId) {
				$('.popup_group, .popup_alert').fadeIn(400).find('h2').text('資料讀取中');
				setTimeout(function(){
					$('.popup_group .popup, .popup_group').fadeOut(400);
				},1000);
				var userId = data.context.userId;
				$.ajax({ 
					url: "./action", 
					type: "POST",
					data: {action: 'userInfo', lineId: userId, token: '<?php echo $_SESSION['token']; ?>'}, 
					success: function(e){
						var obj = jQuery.parseJSON(e);
						/*
						var output = '';
						$.each(obj, function(k,v) {
							output += k + ': ' + v +'; ';
						});
						alert(output);
						*/
						if(obj.error == '0') {
							/*
							$('#username').attr({'readonly': 'readonly'}).val(obj.uname);
							if($('#formContent').find("[name='person']").val() == '0') {	
								$('#birthday').val(obj.birthday);
							} else {
								$('#birthday').attr({'readonly': 'readonly'}).val(obj.birthday);
							}
							*/
							if(!obj.uname || !obj.birthday || !obj.mobile || !obj.loginID || !obj.ifAccept) {
								$('.popup_group, .popup_alert').fadeIn(400).find('h2').text('請完成會員資料');
								setTimeout(function(){
									$('.popup_group .popup, .popup_group').fadeOut(400);
									location.href = 'register.php?web_member_id='+obj.web_member_id+'';
								},1000);
								return;
							}
							$('.name').text(obj.uname);
							$('#memberBirthday').text(obj.birthday);
							$('#mobile').text(obj.mobile);
							$('#balance').text(obj.balance);
							$('#bonus').text(obj.bonus);
							$('#birthdayMoney').text(obj.birthdayMoney);
							
							$('.registerClick').attr('onclick', 'location.href="register.php?web_member_id='+obj.web_member_id+'"');
							$('.stored').attr('href', 'stored.php?web_member_id='+obj.web_member_id+'');
							$('.coupon').attr('href', 'coupon.php?web_member_id='+obj.web_member_id+'');
							$('.bonus').attr('href', 'bonus.php?web_member_id='+obj.web_member_id+'');
							
							if(obj.memberMoneyKind == '3') {
								$('.birthdayMoney').hide();
							} else {
								if(obj.memberMoneyKind == '1') {
									$('.birthdayMoney').attr({'data-memberid': +obj.web_member_id}).show();
									$('.birthdayMoneyMsg').text(' 兌換成功');
									$('.birthdayMoney').attr('data-click', '2');
								} else if(obj.memberMoneyKind == '2') {
									$('.birthdayMoney').attr({'data-memberid': +obj.web_member_id}).show();
									$('.birthdayMoneyMsg').text(' 兌換取消');
								} else if(obj.showMemberMoneyTag) {
									$('.birthdayMoney').attr({'data-memberid': +obj.web_member_id}).show();
								}
							}
							
							//$('.birthdayMoney').attr({'data-memberid': +obj.web_member_id}).show();
							
							liff.getProfile().then(
								profile=> {
									//顯示在text box中
									//$('#username').val(profile.displayName);
									//alert(profile.pictureUrl);
									$('.avatar').css("background-image","url("+profile.pictureUrl+")"); 
								}
							);
						} else {
							location.href = 'register2.php';
						}
						
					}
				});	
			}
			
			
			
        }

        //ready
        $(function () {
            //init LIFF
            liff.init(function (data) {
                initializeApp(data);
				//alert(data.context.userId);
            });
			/*
			document.getElementById('scanQrCodeButton').addEventListener('click', function() {
				alert(liff.isInClient());
				if (!liff.isInClient()) {
					sendAlertIfNotInClient();
				} else {
					liff.scanCode().then(result => {
						// e.g. result = { value: "Hello LIFF app!" }
						const stringifiedResult = JSON.stringify(result);
						document.getElementById('scanQrField').textContent = stringifiedResult;
						toggleQrCodeReader();
					}).catch(err => {
						document.getElementById('scanQrField').textContent = "scanCode failed!";
					});
				}
			});

            //ButtonGetProfile
            $('#ButtonGetProfile').click(function () {
                //取得User Proile
                liff.getProfile().then(
                    profile=> {
                        //顯示在text box中
                        $('#UserInfo').val(profile.displayName);
                        //居然可以alert
                        alert('done');
                    }
                );
            });

            //ButtonSendMsg
            $('#ButtonSendMsg').click(function () {
                liff.sendMessages([
                 {
                     type: 'text',
                     text: $('#msg').val()
                 }
                ])
               .then(() => {
                   alert('done');
               })
            });
			*/
			
			$('.birthdayMoney').click(function() {
				if(!$(this).attr('data-click')) {
					$('.popup_group, .popup_stored_confirm').fadeIn(400).find('h2').text('此次消費是否要兌換？');
					$('.popup_stored_confirm .submit').attr('data-memberid', $(this).attr('data-memberid'));
				} else if($(this).attr('data-click') == '1') {
					$('.popup_group, .popup_alert').fadeIn(400).find('h2').text('領取已送取，等待管理人員確認');
					setTimeout(function(){
						$('.popup_group .popup, .popup_group').fadeOut(400);
					},1000);
				} else if($(this).attr('data-click') == '2') {
					$('.popup_group, .popup_alert').fadeIn(400).find('h2').text('已兌換成功');
					setTimeout(function(){
						$('.popup_group .popup, .popup_group').fadeOut(400);
					},1000);
				}	
			});
			
			$('.popup_stored_confirm .submit').click(function () {
				$('.popup_stored_confirm').hide();
				$('input[name="web_member_id"]').val($(this).attr('data-memberid'));
				$('.popup_group, .popup_stored_confirm2').show().find('h2').text('請選擇使用店家：');
			});	
			
			$('.popup_stored_confirm2 .submit').click(function () {
				var store_id = $('#store').find('option:selected').val();
				if($('input[name="web_member_id"]').val() && store_id) {
					//alert($('input[name="web_member_id"]').val());
					$.ajax({ 
						url: "./action", 
						type: "POST",
						data: $("#searchForm").serialize(), 
						success: function(e){
							
							var obj = jQuery.parseJSON(e);
							/*
							var output = '';
							$.each(obj, function(k,v) {
								output += k + ': ' + v +'; ';
							});
							alert(output);
							*/
							if(obj.error != '0') {
								//alert(obj.message);
								$('.popup_group, .popup_alert').fadeIn(400).find('h2').text(obj.message);
								setTimeout(function(){
									$('.popup_group .popup, .popup_group').fadeOut(400);
								},1000);
								return;
							} else if(obj.error == '0') {
								$('.popup_group .popup, .popup_group').fadeOut(400);
								$('.popup_group, .popup_alert').fadeIn(400).find('h2').text('領取已送取，等待管理人員確認');
								$('.birthdayMoney').attr('data-click', '1');
								setTimeout(function(){
									$('.popup_group .popup, .popup_group').fadeOut(400);
								},1000);
							}
						}
					});
					$('.popup_group .popup, .popup_group').fadeOut(400);
					
				} else {
					$('.popup_group .popup, .popup_group').fadeOut(400);
					$('.popup_group, .popup_alert').fadeIn(400).find('h2').text('操作錯誤');
					setTimeout(function(){
						$('.popup_group .popup, .popup_group').fadeOut(400);
					},1000);
				}
			});
			
			$('.popup_stored_confirm .cancel, .popup_stored_confirm2 .cancel, .popup_stored_complete2 .submit').click(function () {
				$('.popup_group .popup, .popup_group').fadeOut(400);
			});
        });
	</script>
	<link rel="stylesheet" href="./css/app.css"/>
</head>
<body>
<div class="header">
    <h1>會員專區</h1>

</div>
<div class="content member">
    <div class="info">
        <div class="avatar" style="background-image: url('')">
        </div>
        <div class="right">
            <div class="name">
                <?php echo $_SESSION[$_SESSION['token']]['uname']; ?>
            </div>
            <div class="row">
                電話：<span id="mobile"><?php echo $_SESSION[$_SESSION['token']]['mobile']; ?><span>
            </div>
            <div class="row">
                生日：<span id="memberBirthday"><?php echo $_SESSION[$_SESSION['token']]['birthday']; ?><span>
            </div>
            <button class="registerClick" onclick="location.href='register.php'">編輯會員資料</button>
        </div>
    </div>
		
	<a class="btn birthdayMoney" style="display:none;" data-click="">生日禮 消費折<span id="birthdayMoney"><?php echo $_SESSION[$_SESSION['token']]['birthdayMoney']; ?></span>元<span class="birthdayMoneyMsg"></span></a>

    <a class="btn stored" href="stored.php">我的儲值 <span id="balance"><?php echo $_SESSION[$_SESSION['token']]['balance']; ?></span>元</a>
    <a class="btn coupon" href="coupon.php">療程體驗券</a>
	<a class="btn bonus" href="bonus.php">我的點數 <span id="bonus"><?php echo $_SESSION[$_SESSION['token']]['bonus']; ?></span>點</a>
	
</div>
<div class="popup_group">
	<div class="popup popup_alert">
		<!--<a class="close" href="#"></a>-->
		<h2>預約管理</h2>
		<!--<a href="#" class="btn submit disable">確認</a>-->
	</div>
	
	<div class="popup popup_stored_confirm">
        <h2>確認購買？</h2>
        <div class="popup_content">
			
			<div class="text">
				注意：兌換後無法取消
			</div>
			<div class="btns">
				<a href="#" class="cancel">取消</a>
				<a href="#" data-memberid="" class="submit">確認</a>
			</div>
			
        </div>
    </div>
	
	<div class="popup popup_stored_confirm2" style="display:none;">
        <h2>請選擇使用店家：</h2>
        <div class="popup_content" style="padding-top:15px;">
			<form name="searchForm" id="searchForm" method="POST">
				<div class="row">
					<select id="store" name="store_id">
						<option value="">請選擇</option>
				<?php
					foreach($xClassRow as $xClassKey => $xClass) {
						$selected = ($xClass['web_x_class_id'] == $cid) ? "selected='selected'" : null;
						if($xClass['web_x_class_id'] == $cid) {
							$service = ($id) ? $xClass['service'] : $xClass['content'];
						}
				?>		
						<option value="<?php echo $xClass['web_x_class_id']; ?>" <?php echo $selected; ?> data-id="<?php echo $id; ?>"><?php echo $xClass['subject']; ?></option>
				<?php
					}
				?>	
					</select>
				</div>
				<div class="btns">
					<a href="#" class="cancel">取消</a>
					<a href="#" data-memberid="" class="submit">確認</a>
				</div>
				<input class="form-control" type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>" />
				<input class="form-control" type="hidden" name="web_member_id"/>
				<input class="form-control" type="hidden" name="action" value="birthdayMoney" />
			</form>		
        </div>
    </div>
</div>	
</body>
</html>
