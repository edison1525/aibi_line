<?php
    include_once("./control/includes/function.php");
	ini_set('session.cookie_httponly', 1 );
	session_start();

	if (empty($_SESSION['expire']) || $_SESSION['expire'] < time()) $_SESSION['token'] = md5('aibi' . uniqid(microtime())); // create token (fast/sufficient) 
	$_SESSION['expire'] = time() + 900; // make session valid for next 15 mins
	$_SESSION['visitid'] = $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT'];
	
	$agent = $_SERVER['HTTP_USER_AGENT'];
	if (!preg_match("#\bLine\b#", $agent)) {
		die('aibi');
	}
	$collectArr = json_decode($_REQUEST['collect'], true);

	$sql = "
		Select 
			web_qrcode_id,
			cdate,
			edate
		From 
			web_qrcode a
		Where 
			a.web_qrcode_id = '".$collectArr['web_qrcode_id']."'
		AND
			a.ifShow = '1'
	";
	$pdo = $pdoDB->prepare($sql);
	$pdo->execute();
	$web_qrcode_info = $pdo->fetch(PDO::FETCH_ASSOC);
	$statusFlag = 0;
	
	//echo date('Y-m-d')."==".$web_qrcode_info['cdate']."==".$web_qrcode_info['edate']."</br>";
	if(date('Y-m-d') >= $web_qrcode_info['cdate'] && $web_qrcode_info['edate'] >= date('Y-m-d')) {
		$statusFlag = 1;
		$sql = "Update web_qrcode a set a.hits = a.hits +1 Where a.web_qrcode_id = '".$collectArr['web_qrcode_id']."'";
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute();
	}
	//echo $statusFlag;
	//exit;
?>	
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo $Init_WebTitle; ?> 會員專區</title>
    <script src="https://d.line-scdn.net/liff/1.0/sdk.js"></script>
    <script src="./js/jquery-1.10.2.min.js"></script>
	<script>
		//init LIFF
        function initializeApp(data) {
			var statusFlag = "<?php echo $statusFlag; ?>";
            //取得QueryString
            let urlParams = new URLSearchParams(window.location.search);
            //顯示QueryString
            $('#QueryString').val(urlParams.toString());
            //顯示UserId
            $('.userid').val(data.context.userId);
			if(statusFlag == '0') {
				//alert('此QR-Code已過期');
				$('.popup_group, .popup_alert').fadeIn(400).find('h2').text('此QR-Code已過期');
				setTimeout(function(){
					liff.closeWindow();
				},1000);
				return false;
			}
			if(data.context.userId) {
				$('.popup_group, .popup_alert').fadeIn(400).find('h2').text('資料讀取中');
				setTimeout(function(){
					$('.popup_group .popup, .popup_group').fadeOut(400);
				},1000);
				var userId = data.context.userId;
				//alert(userId);
				$.ajax({ 
					url: "./action", 
					type: "POST",
					data: {
						action: 'userInfo', 
						lineId: userId, 
						token: '<?php echo $_SESSION['token']; ?>', 
						request: '<?php echo $_REQUEST['collect']; ?>'
					}, 
					success: function(e){
						var obj = jQuery.parseJSON(e);
						/*
						var output = '';
						$.each(obj, function(k,v) {
							output += k + ': ' + v +'; ';
						});
						alert(output);
						*/
						if(obj.error == '0') {
							location.href = 'line://ti/p/@905ogpbp';
							setTimeout(function(){
								window.parent.close();
								liff.closeWindow();
							},6000);

						} else {
							
							$.ajax({ 
								url: "./action", 
								type: "POST",
								data: {
									action: 'collectRegister', 
									lineId: userId, 
									token: '<?php echo $_SESSION['token']; ?>', 
									request: '<?php echo $_REQUEST['collect']; ?>'
								}, 
								success: function(e){
									location.href = 'line://ti/p/@905ogpbp';
									setTimeout(function(){
										window.parent.close();
										//liff.closeWindow();
									},6000);
								}
							});	
							
						}
						
					}
				});
			} else {
				setTimeout(function(){
					liff.closeWindow();
				},2000);

			}
			
        }

        //ready
        $(function () {
            //init LIFF
            liff.init(function (data) {
                initializeApp(data);
				//alert(data.context.userId);
            });
			
        });
	</script>
	<link rel="stylesheet" href="./css/app.css"/>
</head>
<body>
<div class="popup_group">
	<div class="popup popup_alert">
		<!--<a class="close" href="#"></a>-->
		<h2>預約管理</h2>
		<!--<a href="#" class="btn submit disable">確認</a>-->
	</div>
</div>	
</body>
</html>
