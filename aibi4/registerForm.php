<?php
	include_once("./control/includes/function.php");
	ini_set('session.cookie_httponly', 1 );
	session_start();

	if (empty($_SESSION['expire']) || $_SESSION['expire'] < time()) $_SESSION['token'] = md5('aibi' . uniqid(microtime())); // create token (fast/sufficient) 
	$_SESSION['expire'] = time() + 900; // make session valid for next 15 mins
	$_SESSION['visitid'] = $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT'];
	
	$agent = $_SERVER['HTTP_USER_AGENT'];
	if (!preg_match("#\bLine\b#", $agent)) {
		//die('aibi');
	}
	
	//$action = ($_REQUEST['action']);
	$person = intval($_REQUEST['person']);
	$personTitle = ($person) ? '本人' : '非本人';
	$shiftId = intval($_REQUEST['shiftId']);
	$date = $_REQUEST['date'];
	$class = $_REQUEST['class'];
	$team = $_REQUEST['team'];
	$timeId = $_REQUEST['timeId'];
	$teamXid = $_REQUEST['teamXid'];
	$error = 0;
	
	if(!$date || !$class || !$team) {
		$error = 1;
		echo "<script>alert('操作錯誤')</script>";
	}
	$registerDateLimit = strtotime(substr($date, 0, 10)." ".substr($date, 22));
	if(time() >= $registerDateLimit) {
		echo "<script>window.alert('此預約時段已過')</script>";
		$error = 1;
		//die();
	}
	
	$sql = "
		SELECT 
			class.subject as classSubject,
			class.lineInfo as classLineInfo,
			class.web_x_class_id as class_x_class_id,
			xclass.subject as xclassSubject
		from 
			web_class class
		left join
			web_x_class xclass
		on
			xclass.web_x_class_id = class.web_x_class_id	
		WHERE 
			class.web_class_id = :web_class_id
	";
	$excute = array(
		':web_class_id'		=> $timeId,
	);
	
	$pdo = $pdoDB->prepare($sql);
	$pdo->execute($excute);
	$row3 = $pdo->fetch(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html>
<head> 
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo $Init_WebTitle; ?> 線上預約-<?php echo $personTitle; ?></title>
    <script src="https://d.line-scdn.net/liff/1.0/sdk.js"></script>
	
    <script src="../js/jquery-1.10.2.min.js"></script>
    <link rel="stylesheet" href="../css/app_old.css"/>

	
	<script src="../js/jquery.validate.js" type="text/javascript"></script>
	<script src="../js/additional-methods.js"></script>
	<style>
		.error {
			margin-top:-10px;
			font-size:14px;
			color:red;
		}
		#birthday{
			width: 100%;
			height: 30px;
		}
		.popup_product2 {
			margin: 80px 0;
			top: 0;
			left: 50%;
			transform: translate(40%, 0); 
			width: 50.5%;
			max-width: 845px;
			border: 1px solid #ffdd00;
			background: #FFF;
			padding: 20px 20px 0 0;
		}
		.popup_product2 .closebtn {
			width: 40px;
			color: #D3D3D3;
			font-size: 26px;
			padding: 10px;
			margin: 5px;
			height: 40px;
			line-height: 0;
			position: absolute;
			right: 20px;
			top: 20px;
			cursor: pointer;
		}
		@media (max-width: 800px) {
			.popup_product2 {
				padding: 20px;
			}
		}
		@media (max-width: 500px) {
			.popup_group .popup.popup_product2 {
				padding: 0;
				width: 100%;
				border: 0;
				margin: 50px 0 0 0;
			}
			.popup_product2 .row{
				margin: 0;
			}
			.popup_product2 .item{
				position: fixed;
				top: 0;
				left: 0;
				right: 0;
				box-shadow: 1px 1px 8px 0 rgba(0, 0, 0, 0.1);
				max-width: none;
				margin: 0;
				padding: 10px 15px;
			}
			.popup_product2 .col-img{
				width: 100%;
				margin: 15px 0;
			}
			.popup_product2 .closebtn{
				display: none;
			}
		}
	</style>
    <script>
        //init LIFF
        function initializeApp(data) {
            //取得QueryString
            let urlParams = new URLSearchParams(window.location.search);
            //顯示QueryString
            $('#QueryString').val(urlParams.toString());
            //顯示UserId
            $('#userid').val(data.context.userId);
			if(data.context.userId) {
				var userId = data.context.userId;
				$.ajax({ 
					url: "./action", 
					type: "POST",
					data: {action: 'userInfo', lineId: userId, token: '<?php echo $_SESSION['token']; ?>'}, 
					success: function(e){
						var obj = jQuery.parseJSON(e);
						/*
						var output = '';
						$.each(obj, function(k,v) {
							output += k + ': ' + v +'; ';
						});
						alert(output);
						*/
						if(obj.error == '0') {
							$('#username').attr({'readonly': 'readonly'}).val(obj.uname);
							if($('#formContent').find("[name='person']").val() == '0') {	
								//$('#birthday').val();
								$('#birthday').attr({'readonly': 'readonly'}).val(obj.birthday);
							} else {
								$('#birthday').attr({'readonly': 'readonly'}).val(obj.birthday);
							}	
							$('#memberBirthday').val(obj.birthday);
							$('#mobile').val(obj.mobile);
						}
						
					}
				});	
			}
			
			liff.getProfile().then(
				profile=> {
					//顯示在text box中
					//$('#username').val(profile.displayName);
					//alert('done');
				}
			);
        }

        //ready
        $(function () {
			
			/*
			$('#birthday').datepick({
				maxDate: 0
			});
			*/
			var errorFlag = <?php echo $error; ?>;
			
            //init LIFF
            liff.init(function (data) {
				var url = new URL(location.href);
				var action = url.searchParams.get("action");
				if(errorFlag)
					liff.closeWindow();
				if(action == 'confirmRegister') {		//確認
				
					var registerId = url.searchParams.get("registerId");
					var classSubjct = url.searchParams.get("class");
					var teamSubject = url.searchParams.get("team");
					var subject = url.searchParams.get("subject");
					var ordernum = url.searchParams.get("ordernum");
					var timeId = url.searchParams.get("timeId");
					
					
					$.ajax({ 
						url: "./action", 
						type: "POST",
						data: {action: 'confirm', id: registerId, token: '<?php echo $_SESSION['token']; ?>'}, 
						success: function(e){
							var obj = jQuery.parseJSON(e);
							if(!obj.error) {
								//alert(obj.error);
								liff.sendMessages([
									{	
									/*
										"type": "template",
										"altText": "預約確認",
										"template": {
											"type": "carousel",
											"columns": [{
												"title": "掛號成功	",
												"text": classSubjct +"-"+ teamSubject +" 醫師\n"+ subject +"\n掛號號碼: "+ordernum,
												"actions": [
													{
														"type": "uri",
														"label": " ",
														"uri": "line://app/1567441648-vk5le0d2?action=close"
													}
												],
											}]
										}
									*/	
										"type": "flex",
										"altText": "預約確認",
										"contents": {
											"type": "carousel",
											"contents": [{
												  "type": "bubble",
												  "hero": {
													"type": "image",
													"url": "https://scdn.line-apps.com/n/channel_devcenter/img/fx/01_2_restaurant.png",
													"size": "full",
													"aspectRatio": "20:13",
													"aspectMode": "cover",
													"action": {
													  "type": "uri",
													  "uri": "https://linecorp.com"
													}
												  },
												  "body": {
													"type": "box",
													"layout": "vertical",
													"spacing": "md",
													"action": {
													  "type": "uri",
													  "uri": "https://linecorp.com"
													},
													"contents": [
													  {
														"type": "text",
														"text": "Brown's Burger",
														"size": "xl",
														"weight": "bold"
													  },
													  {
														"type": "box",
														"layout": "vertical",
														"spacing": "sm",
														"contents": [
														  {
															"type": "box",
															"layout": "baseline",
															"contents": [
															  {
																"type": "icon",
																"url": "https://scdn.line-apps.com/n/channel_devcenter/img/fx/restaurant_regular_32.png"
															  },
															  {
																"type": "text",
																"text": "$10.5",
																"weight": "bold",
																"margin": "sm",
																"flex": 0
															  },
															  {
																"type": "text",
																"text": "400kcl",
																"size": "sm",
																"align": "end",
																"color": "#aaaaaa"
															  }
															]
														  },
														  {
															"type": "box",
															"layout": "baseline",
															"contents": [
															  {
																"type": "icon",
																"url": "https://scdn.line-apps.com/n/channel_devcenter/img/fx/restaurant_large_32.png"
															  },
															  {
																"type": "text",
																"text": "$15.5",
																"weight": "bold",
																"margin": "sm",
																"flex": 0
															  },
															  {
																"type": "text",
																"text": "550kcl",
																"size": "sm",
																"align": "end",
																"color": "#aaaaaa"
															  }
															]
														  }
														]
													  },
													  {
														"type": "text",
														"text": "Sauce, Onions, Pickles, Lettuce & Cheese",
														"wrap": true,
														"color": "#aaaaaa",
														"size": "xxs"
													  }
													]
												  },
												  "footer": {
													"type": "box",
													"layout": "vertical",
													"contents": [
													  {
														"type": "spacer",
														"size": "xxl"
													  },
													  {
														"type": "button",
														"style": "primary",
														"color": "#905c44",
														"action": {
														  "type": "uri",
														  "label": "Add to Cart",
														  "uri": "https://linecorp.com"
														}
													  }
													]
												  }
												}]
										}
									}	
								]).then(() => {
									//建立成功，關閉視窗
									liff.closeWindow();
								}).catch(function (error) {
									window.alert('Error sending message: ' + error.message);
								});
								
							} else {
								liff.sendMessages([
									{
										type: 'text',
										text: obj.message
										
									},	
								]).then(() => {
									//建立成功，關閉視窗
									liff.closeWindow();
								}).catch(function (error) {
									window.alert('Error sending message: ' + error.message);
								});	
							}	
						}
					});
					
				} else if(action == 'removeRegister') { 	//刪除
					var registerId = url.searchParams.get("registerId");
					
					$.ajax({ 
						url: "./action", 
						type: "POST",
						data: {action: 'remove', id: registerId, token: '<?php echo $_SESSION['token']; ?>'}, 
						success: function(e){
							var obj = jQuery.parseJSON(e);
							//alert(obj.message);
							liff.sendMessages([
								{
									type: 'text',
									text: obj.message
									
								},	
								/*{
									type: 'sticker',
									packageId: '2',
									stickerId: '144'
								}*/
							]).then(() => {
								//建立成功，關閉視窗
								liff.closeWindow();
							}).catch(function (error) {
								window.alert('Error sending message: ' + error.message);
							});
								
							
						}
					});
					
					
				} else if(action == 'close') {
					liff.closeWindow();
				}
                initializeApp(data);
            });

            //ButtonGetProfile
            $('#ButtonGetProfile').click(function () {
                //取得User Proile
                liff.getProfile().then(
                    profile=> {
                        //顯示在text box中
                        $('#UserInfo').val(profile.displayName);
                        //居然可以alert
                        alert('done');
                    }
                );
            });

            //ButtonSendMsg
            $('#ButtonSendMsg').click(function () {
				
				//liff.closeWindow();
				var color = '#' + parseInt(Math.random() * 0xffffff).toString(16);
                liff.sendMessages([
					{
						"type": "template",
						"altText": "預約確認",
						"template": {
							"type": "carousel",
							"columns": [{
								"title": "你的掛號資訊",
								"text": "感謝您透過線上預約課程!\r\n您預約：【test】- test\r\n詳情如下：",
								"actions": [
									{
										"type": "uri",
										"label": "LIFF APP",
										"uri": "line://app/1508489527-8gy7JjJM?v=2"
									}
								],
							}]
						}
					},	
					
					/*{
						type: 'sticker',
						packageId: '2',
						stickerId: '144'
					}*/
                ]).then(() => {
                    //建立成功，關閉視窗
                    liff.closeWindow();
                }).catch(function (error) {
					window.alert('Error sending message: ' + error.message);
				});
            });
			
        });
    </script>
</head>
	<body>
		<div class="registerForm row">
			<form name="form" action="contact.html" class="row" id="formContent" method="POST">
				<div class="content appointment">
					<label>到店時間</label>
					<input name="date2" id="date2" value="<?php echo substr($date,0,21); ?>" readonly="readonly"/>
					<input type="hidden" name="date" id="date" value="<?php echo $date; ?>" readonly="readonly"/>
					<input class="form-control" type="hidden" name="lineID" id="userid" />
					<input class="form-control" type="hidden" name="class" id="class" value="<?php echo $class; ?>" />
					<input class="form-control" type="hidden" name="team" id="team" value="<?php echo $team; ?>" />
					<input class="form-control" type="hidden" name="timeId" id="timeId" value="<?php echo $timeId; ?>" />
					<input class="form-control" type="hidden" name="teamXid" id="teamXid" value="<?php echo $teamXid; ?>" />
					<input class="form-control" type="hidden" name="storeName" id="storeName" value="<?php echo $row3['xclassSubject']; ?>" />
			<?php
				if($person) {
			?>
					<label>預約人姓名</label>
					<input name="uname" id="username" required="" placeholder="請輸入預約人姓名？"/><br />
					<label>性別</label></br></br>
					<input type="radio" id="r32" name="sex" value="先生" checked>
					<label for="r32">先生</label>
					
					<input type="radio" id="r22" name="sex" value="小姐">
					<label for="r22">小姐</label>
					</br>
					<label style="display:none;">預約人生日</label>
			<?php
				} else {
			?>	
					<label>消費人姓名</label>
					<input name="uname2" id="username2" required="" placeholder="請輸入預約人姓名？"/><br />
					<label>性別</label></br></br>
					<input type="radio" id="r32_2" name="sex2" value="先生" checked>
					<label for="r32_2">先生</label>
					
					<input type="radio" id="r22_2" name="sex2" value="小姐">
					<label for="r22_2">小姐</label>
					</br>
					<label style="display:none;">消費人生日</label>
			<?php
				}
			?>
					
					<!--</br>
					<input type="date" name="birthday" id="birthday" min="1929-01-01" max="<?php echo date('Y-m-d', strtotime('-1 day')); ?>" data-role="datebox" data-options='{"mode": "datebox", "useNewStyle":true}' required="" placeholder="請輸入預約人生日"></br>-->
					<input type="hidden" name="birthday" id="birthday" min="1929-01-01" max="<?php echo date('Y-m-d', strtotime('-1 day')); ?>" data-role="datebox" data-options='{"mode": "datebox", "useNewStyle":true}' required="" placeholder="請輸入預約人生日"></br>
			<?php
				if(!$person) {
			?>		
					<label style="display:none;">預約人姓名</label>
					<input type="hidden" name="uname" id="username" placeholder="請輸入預約人姓名"></br>
					<label style="display:none;">性別</label></br></br>
					<input type="hidden" id="r32" name="sex" value="先生" checked>
					<label style="display:none;" for="r32">先生</label>
					
					<input type="hidden" id="r22" name="sex" value="小姐">
					<label style="display:none;" for="r22">小姐</label>
					</br>
			<?php
				}
			?>	
					<label style="display:none;">預約人手機號碼</label>
					<input type="hidden" name="mobile" id="mobile" required="" isMobile="1" maxlength="10" minlength="10" placeholder="請輸入預約人手機號碼">
					<input class="form-control" type="hidden" name="shiftId" value="<?php echo $shiftId; ?>" />
					<input class="form-control" type="hidden" name="person" value="<?php echo $person; ?>" />
					<input class="form-control" type="hidden" id="memberBirthday" name="memberBirthday" value="<?php echo $shiftId; ?>" />
					<input class="form-control" type="hidden" name="action" value="register" />
					<input class="form-control" type="hidden" name="registerType" value="1" />
					<input class="form-control" type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>" />
					<button type="submit" id="contnetBtn">確認</button>
				</div>
			</form>	
		</div>
		<div class="registerInfo" style="display:none;">
			<div class="content appointment">
				<div class="registerInfoList"></div>
				</br>
				<button class="confirm" data-status="enable">確認</button>
			</div>
		</div>
		<div class="popup_group">
			<div class="popup popup_alert2">
				<!--<a class="close" href="#"></a>-->
				<h2>預約管理</h2>
				<!--<a href="#" class="btn submit disable">確認</a>-->
			</div>
		</div>		
	</body>
</html>
<script>
	$(function() {
		
		var tpl;
		jQuery.validator.addMethod("string", function (value, element) {
			return this.optional(element) || /^[\u0391-\uFFE5\w]+$/.test(value);
		}, "不允許包含特殊符号!"); 
		jQuery.validator.addMethod("isMobile", function(value, element) {  
			var length = value.length;  
			var mobile = /^09[0-9]{8}$/;  
			return this.optional(element) || (length == 10 && mobile.test(value));  
		}, "請正確填寫手機號碼");  
		$('#formContent').validate({
			errorElement: 'p',
			submitHandler: function(form) {
				
				var registerDate = $(form).find('#date').val();
				
				tpl = "<div class=\"info\">";
				tpl += "	<div class=\"row\">";
			/*	
			if($(form).find("[name='person']").val() == '0') {	
				tpl += "		<div class=\"name\">"+$(form).find('#username2').val()+"</div>";
			} else {
				tpl += "		<div class=\"name\">"+$(form).find('#username').val()+"</div>";
			}
			*/	
				tpl += "		<div class=\"name\">"+$(form).find('#username').val()+"</div>";
				
				tpl += "		<div class=\"value\">電話："+$(form).find('#mobile').val()+" <br> 生日："+$(form).find('#birthday').val()+"</div>";
				tpl += "	</div>";
				tpl += "</div>";
				tpl += "<div class=\"row\">";
				tpl += "	<div class=\"name\">門市</div>";
				tpl += "	<div class=\"value\">"+$(form).find('#storeName').val()+"</div>";
				tpl += "</div>";
				tpl += "<div class=\"row\">";
				tpl += "	<div class=\"name\">類型</div>";
			if($(form).find("[name='person']").val() == '0') {
				tpl += "	<div class=\"value\">非本人預約</div>";
			} else {
				tpl += "	<div class=\"value\">本人預約</div>";
			}	
				tpl += "</div>";
				tpl += "<div class=\"row\">";
				tpl += "	<div class=\"name\">項目</div>";
				tpl += "	<div class=\"value\">"+$(form).find('#class').val()+"</div>";
				tpl += "</div>";
				tpl += "<div class=\"row\">";
				tpl += "	<div class=\"name\">設計師</div>";
				tpl += "	<div class=\"value\">"+$(form).find('#team').val()+"</div>";
				tpl += "</div>";
				tpl += "<div class=\"row\">";
				tpl += "	<div class=\"name\">日期</div>";
				tpl += "	<div class=\"value\">"+registerDate.substr(0, 13)+"</div>";
				tpl += "</div>";
				tpl += "<div class=\"row\">";
				tpl += "	<div class=\"name\">到店時間</div>";
				tpl += "	<div class=\"value\">"+registerDate.substr(14, 5)+"</div>";
				tpl += "</div>";
			if($(form).find("[name='person']").val() == '0') {	
				tpl += "<div class=\"row\">";
				tpl += "	<div class=\"name\">消費人姓名</div>";
				tpl += "	<div class=\"value\">"+$(form).find('#username2').val()+"</div>";
				tpl += "</div>";
				tpl += "<div class=\"row\" style=\"display:none;\">";
				tpl += "	<div class=\"name\">消費人性別</div>";
				tpl += "	<div class=\"value\">"+$(form).find('input[name="sex2"]:checked').val()+"</div>";
				tpl += "</div>";
				tpl += "<div class=\"row\" style=\"display:none;\">";
				tpl += "	<div class=\"name\">消費人生日</div>";
				tpl += "	<div class=\"value\">"+$(form).find('#birthday').val()+"</div>";
				tpl += "</div>";
			} else {
				tpl += "<div class=\"row\">";
				tpl += "	<div class=\"name\">消費人姓名</div>";
				tpl += "	<div class=\"value\">"+$(form).find('#username').val()+"</div>";
				tpl += "</div>";
				tpl += "<div class=\"row\" style=\"display:none;\">";
				tpl += "	<div class=\"name\">消費人性別</div>";
				tpl += "	<div class=\"value\">"+$(form).find('input[name="sex"]:checked').val()+"</div>";
				tpl += "</div>";
			}
				
				$('.registerForm').hide();
				$('.registerInfoList').append(tpl);
				//$('.registerInfo').show();
				$('.registerInfo').fadeIn(400);
			},
			rules: {
				code: {
					required: true,
					minlength: 4,
					remote: {
						url: 'verifycode.php',
						type: "post",
						data: {
							type: 1,
							code: function() {
								return $('#code').val();
							}
						}
					}
				},
				uname: {
					required: true,
					minlength: 2,
					remote: {
						url: 'chekChiness.php',
						type: "post",
						data: {
							type: 1,
							username: function() {
								return $('#username').val();
							}
						}
					}
				},
				uname2: {
					required: true,
					minlength: 2,
					remote: {
						url: 'chekChiness.php',
						type: "post",
						data: {
							type: 1,
							username: function() {
								return $('#username2').val();
							}
						}
					}
				}
			},
			messages: {
				codeLogin: {
					remote: '請輸入正確的驗証碼'
				},
				uname: {
					remote: '請輸入中文姓名'
				},
				uname2: {
					remote: '請輸入中文姓名'
				}	
			}   
		});

		$("#contnetBtn").click(function(e) {
			e.preventDefault();
			$("#contnetBtn").attr('disable', true);
			$('#formContent').submit();
			return false;
		});
		
		$(".confirm").click(function(e) {
			if($(this).attr('data-status') == 'enable') {
				$(this).attr({'data-status': 'disable'});
				$.ajax({ 
					url: "./action", 
					type: "POST",
					data: $("#formContent").serialize(), 
					success: function(e){
						var obj = jQuery.parseJSON(e);
						var output = '';
						$.each(obj, function(k,v) {
							output += k + ': ' + v +'; ';
						});
						//alert(output);
						if(obj.error != '0') {
							//alert(obj.message);
							$('.registerInfo').fadeOut(400);
							$('.popup_group, .popup_alert2').fadeIn(400).find('h2').text(obj.message);
							//liff.closeWindow();
							setTimeout(function(){
								liff.closeWindow();
							},1000);
							
						}	

						if(obj.error == '0') {
							
							if(obj.registerId) {
								$.ajax({ 
									url: "./action", 
									type: "POST",
									data: {action: 'confirm', id: obj.registerId, token: '<?php echo $_SESSION['token']; ?>'}, 
									success: function(e){
										var obj2 = jQuery.parseJSON(e);
										var output2 = '';
										$.each(obj2, function(k,v) {
											output2 += k + ': ' + v +'; ';
										});
										//alert(output2);
										if(obj2.error == '0') {
											//alert(obj.error);
											liff.sendMessages([
												{
													/*
													"type": "template",
													"altText": "預約確認",
													"template": {
														"type": "carousel",
														"columns": [{
															"title": "掛號成功	",
															"text": obj.classSubjct +"-"+ obj.teamSubject +" 醫師\n"+ obj.subject +"\n掛號號碼: "+obj.ordernum,
															"actions": [
																{
																	"type": "uri",
																	"label": " ",
																	"uri": "line://app/1567441648-vk5le0d2?action=close"
																}
															],
														}]
													}
													*/
													"type": "flex",
													"altText": "預約確認",
													"contents": {
														"type": "carousel",
														"contents": [
															{
																"type": "bubble",
																"body": {
																	"type": "box",
																	"layout": "vertical",
																	"spacing": "md",
																	"contents": [
																	{
																		"type": "text",
																		"text": "預約成功",
																		"size": "xl",
																		"weight": "bold",
																		"color": "#3A71BF"
																	},
																	{
																		"type": "separator",
																		"margin": "lg"
																	},
																	{
																		"type": "box",
																		"layout": "vertical",
																		"spacing": "sm",
																		"contents": [
																		{
																			"type": "box",
																			"layout": "baseline",
																			"contents": [
																			  
																				{
																					"type": "text",
																					"text": "門市 : ",
																					"weight": "bold",
																					"margin": "sm",
																				},
																				{
																					"type": "text",
																					"text": $("#formContent").find('#storeName').val(),
																					"weight": "bold",
																					"flex": 4,
																					"color": "#905c44",
																					"align": "start",
																					//"size": "xl",
																				}
																			]
																		},
																		{
																			"type": "box",
																			"layout": "baseline",
																			"contents": [
																				{
																					"type": "text",
																					"text": obj.teamSubject +"-"+ obj.classSubject +" 設計師",
																					"weight": "bold",
																					"margin": "sm",
																				}
																			]
																		},
																		{
																			"type": "box",
																			"layout": "baseline",
																			"contents": [
																			  
																				{
																					"type": "text",
																					"text": "預約時間 : ",
																					"weight": "bold",
																					"margin": "sm",
																				},
																				{
																					"type": "text",
																					"text": obj.subject.substr(0, 13),
																					"weight": "bold",
																					"flex": 2,
																					"color": "#905c44",
																					"align": "start",
																					//"size": "xl",
																				}
																			]
																		},
																		{
																			"type": "box",
																			"layout": "baseline",
																			"contents": [
																			  
																				{
																					"type": "text",
																					"text": "到店時間 : ",
																					"weight": "bold",
																					"margin": "sm",
																				},
																				{
																					"type": "text",
																					"text": obj.subject.substr(14, 5),
																					"weight": "bold",
																					"flex": 2,
																					"color": "#905c44",
																					"align": "start",
																					//"size": "xl",
																				}
																			]
																		},
																		{
																			"type": "box",
																			"layout": "baseline",
																			"contents": [
																			  
																				{
																					"type": "text",
																					"text": "消費人 : ",
																					"weight": "bold",
																					"margin": "sm",
																				},
																				{
																					"type": "text",
																					"text": ($("#formContent").find("[name='person']").val() == '0') ? $("#formContent").find('#username2').val() : $("#formContent").find('#username').val(),
																					"weight": "bold",
																					"flex": 3,
																					"color": "#905c44",
																					"align": "start",
																					//"size": "xl",
																				}
																			]
																		}
																		/*,
																		{
																			"type": "box",
																			"layout": "baseline",
																			"contents": [
																				{
																					"type": "text",
																					"text": "預約號碼 : ",
																					"weight": "bold",
																					"margin": "sm",
																				},
																				{
																					"type": "text",
																					"text": obj.ordernum,
																					"weight": "bold",
																					"flex": 2,
																					"color": "#905c44",
																					"align": "start",
																					"size": "xl",
																				}
																			]
																		}
																		*/
																		]
																	  },
																	  
																	  {
																		"type": "text",
																		"text": "Design by aibitechcology",
																		"wrap": true,
																		"color": "#aaaaaa",
																		"size": "xxs",
																		"align": "end"
																	  }
																	  
																	]
																 }
															}
														]
													}
												}	
											]).then(() => {
												//建立成功，關閉視窗
												liff.closeWindow();
											}).catch(function (error) {
												window.alert('Error sending message: ' + error.message);
												liff.closeWindow();
											});
											
										} else {
											liff.sendMessages([
												{
													type: 'text',
													text: obj.message
													
												},	
											]).then(() => {
												//建立成功，關閉視窗
												liff.closeWindow();
											}).catch(function (error) {
												window.alert('Error sending message: ' + error.message);
												liff.closeWindow();
											});	
										}	
									}
								});
							}	
								
							if(0){
								liff.sendMessages([
									{
										"type": "template",
										"altText": "預約確認",
										"template": {
											"type": "carousel",
											"columns": [{
												"title": "你的掛號資訊",
												"text": "診療項目："+ obj.classSubjct +"【"+ obj.teamSubject +"】\n"+ obj.subject,
												"actions": [
													{
														"type": "uri",
														"label": "資料錯誤，重新預約",
														"uri" : "line://app/1653388002-015aNXZ2?action=removeRegister&registerId="+obj.registerId,
														//"uri" : "https://tw.yahoo.com",
													},
													{
														"type": "uri",
														"label": "正確",
														"uri" : "line://app/1653388002-015aNXZ2?action=confirmRegister&registerId="+obj.registerId+"&class="+obj.classSubjct+"&team="+obj.teamSubject+"&subject="+obj.subject+"&ordernum="+obj.ordernum,
													}
												],
											}]
										}
									},	
									
									/*{
										type: 'sticker',
										packageId: '2',
										stickerId: '144'
									}*/
								]).then(() => {
									//建立成功，關閉視窗
									liff.closeWindow();
								}).catch(function (error) {
									window.alert('Error sending message: ' + error.message);
									liff.closeWindow();
								});
							}
						}	
						
					}
				});
			}	
		});	
	})
</script>