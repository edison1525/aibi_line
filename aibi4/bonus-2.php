<?php
    include_once("./control/includes/function.php");
	ini_set('session.cookie_httponly', 1 );
	session_start();

	if (empty($_SESSION['expire']) || $_SESSION['expire'] < time()) $_SESSION['token'] = md5('aibi' . uniqid(microtime())); // create token (fast/sufficient) 
	$_SESSION['expire'] = time() + 900; // make session valid for next 15 mins
	$_SESSION['visitid'] = $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT'];
	
	$agent = $_SERVER['HTTP_USER_AGENT'];
	if (!preg_match("#\bLine\b#", $agent)) {
		//die('aibi');
	}
	$web_member_id = $_REQUEST['web_member_id'];
	if(!$web_member_id) {
		die('aibi');
	}
	
	$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM web_product where web_product.ifShow = :ifShow and web_product.web_x_product_id = :web_x_product_id and useEdate >= '".date('Y-m-d')."' ORDER BY web_product.displayorder ASC";
	$excute = array(
		':ifShow'        			=> 1,
		':web_x_product_id'       	=> 4,
	);
	$pdo = $pdoDB->prepare($sql);
	$pdo->execute($excute);
	$row = $pdo->fetchAll(PDO::FETCH_ASSOC);
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $Init_WebTitle; ?> 會員專區-點數兌換</title>
	<script src="./js/jquery-1.10.2.min.js"></script>
	<script src="../js/jquery.cookie.js" type="text/javascript"></script>
    <link rel="stylesheet" href="css/app.css"/>
</head>
<body>
<div class="header">
    <h1>會員專區-點數兌換</h1> <a class="back" href="bonus.php?web_member_id=<?php echo $web_member_id; ?>"></a>
</div>
<div class="content stored-2">
	<div class="row" style="padding:20px;">
		<select id="store">
			<option value="">請選擇</option>
	<?php
		foreach($xClassRow as $xClassKey => $xClass) {
			$selected = ($xClass['web_x_class_id'] == $cid) ? "selected='selected'" : null;
			if($xClass['web_x_class_id'] == $cid) {
				$service = ($id) ? $xClass['service'] : $xClass['content'];
			}
	?>		
			<option value="<?php echo $xClass['web_x_class_id']; ?>" <?php echo $selected; ?> data-id="<?php echo $id; ?>"><?php echo $xClass['subject']; ?></option>
	<?php
		}
	?>	
		</select>
	</div>
	<div class="info" style="display:none;">
		<div class="title">請選擇兌換商品 (注意：需到店現場兌換)</div>
	<?php
		foreach($row as $key => $storedVal) {
			$pic = ShowPic($storedVal['Covers'], "./uploadfiles/l/", "./uploadfiles/no_image.jpg");
	?>    
		<div class="row" data-prodid="<?php echo $storedVal['web_product_id']; ?>" data-pricemember="<?php echo number_format($storedVal['price_member']); ?>" data-pricecost="<?php echo number_format($storedVal['price_cost']); ?>">
			<div class="col-1" style="padding:5px; background-color:white;">
				<img src="<?php echo $pic; ?>" width="80%" />
			</div>
			<div class="col-2" style="font-size:10px;"><?php echo $storedVal['subject']; ?></br><?php echo number_format($storedVal['price_cost']); ?>點</div>
			<div class="col-3">
				<span>兌換</span>
			</div>
		</div>
	<?php
		}
	?>	
	</div>
</div>
<div class="popup_group">
    <div class="popup popup_stored_confirm">
        <h2>確認兌換？</h2>
        <div class="popup_content">
			<form name="searchForm" id="searchForm" method="POST">
				<div class="text" style="display:none;">
					點數 <span id="cash">4,000</span>點
				</div>
				<div class="btns">
					<a href="#" class="cancel">取消</a>
					<a href="#" class="submit">確認</a>
				</div>
				<input class="form-control" type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>" />
				<input class="form-control" type="hidden" name="web_member_id" value="<?php echo $web_member_id; ?>" />
				<input class="form-control" type="hidden" name="web_product_id" />
				<input class="form-control" type="hidden" name="store_id" />
				<input class="form-control" type="hidden" name="action" value="useBonus" />
			</form>	
        </div>
    </div>
    <div class="popup popup_stored_complete">
        <h2>已送出</h2>
        <div class="popup_content">
            <div class="text">
                請店家確認付款事宜
            </div>
            <div class="btns">
                <a href="#" class="submit">關閉</a>
            </div>
        </div>
    </div>
	<div class="popup popup_alert" style="display:none;">
		<h2>兌換管理</h2>
		<div class="btns remark">
			<a href="#" class="btn submit checkUser">確認</a>
		</div>
	</div>
</div>
<script>
	$(function () {
		
		$('.row').click(function () {
			var prodid = ($(this).attr('data-prodid')) ? $(this).attr('data-prodid') : 0;
			var price_member = ($(this).attr('data-pricemember')) ? $(this).attr('data-pricemember') : 0;
			var price_cost = ($(this).attr('data-pricecost')) ? $(this).attr('data-pricecost') : 0;
			var store_id = $('#store').find('option:selected').val();
			if(!prodid || !store_id){
				return;
			}
			$('#cash').text(price_member);
			$('#finalStored').text(price_cost);
			$('input[name="web_product_id"]').val(prodid);
			$('input[name="store_id"]').val(store_id);
			$('.popup_group, .popup_stored_confirm').fadeIn(400).find('h2').text('確認兌換？');
		});
		
		$('.popup_stored_confirm .submit').click(function () {
			if($('input[name="web_product_id"]').val() && $('input[name="web_member_id"]').val() && $('input[name="store_id"]').val()) {
				$.ajax({ 
					url: "./action", 
					type: "POST",
					data: $("#searchForm").serialize(), 
					success: function(e){
						var obj = jQuery.parseJSON(e);
						var output = '';
						$.each(obj, function(k,v) {
							output += k + ': ' + v +'; ';
						});
						//alert(output);
						if(obj.error != '0') {
							//alert(obj.message);
							$('.popup_group, .popup_alert').fadeIn(400).find('h2').text(obj.message);
							setTimeout(function(){
								$('.popup_group .popup, .popup_group').fadeOut(400);
							},1000);
							return;
						} else if(obj.error == '0') {
							$('.popup_stored_complete').show();
						}
					}
				});
				$('.popup_stored_confirm').hide();
			} else {
				alert('操作錯誤');
			}	
		});
		
		$('.popup_stored_confirm .cancel, .popup_stored_complete .submit').click(function () {
			$('.popup_group .popup, .popup_group').fadeOut(400);
		});
		
		if($.cookie('storeIdCookie')) {
			$('#store').find('option[value="'+$.cookie('storeIdCookie')+'"]').prop('selected', true);
			$('.info').fadeIn(400);
		}
		
		$('#store').on('change', function() {
			var storeId = $(this).find('option:selected').val();
			$.cookie('storeIdCookie', storeId, {
				expires:7, 
				path: '/'
			});
			var id = $(this).find('option:selected').attr('data-id');
			//console.log(storeId);
			if(storeId) 
				$('.info').fadeIn(400);
			else 
				$('.info').fadeOut(400);
		});
	})
</script>
</body>
</html>
