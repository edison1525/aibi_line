<?php
	include_once("./control/includes/function.php");
	if (empty($_SESSION['expire']) || $_SESSION['expire'] < time()) {
		unset($_SESSION[$_SESSION['token']]);
		$_SESSION['token'] = md5('aibi' . uniqid(microtime())); // create token (fast/sufficient) 
	}	
	$_SESSION['expire'] = time() + 900; // make session valid for next 15 mins
	$_SESSION['visitid'] = $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT'];
	
	$store = ($_GET['store']) ? $_GET['store'] : 1;
	
	//$chatRow = file_get_contents("./chat/chat_".date('Ymd', strtotime('-1 day')).".txt", FILE_USE_INCLUDE_PATH);
	$chatRow = '';
	for($i=7; $i>=0; $i--) {
		$chatRow .= file_get_contents("./chat/chat_".date('Ymd', strtotime('-'.$i.' day')).".txt", FILE_USE_INCLUDE_PATH);
	}
	//echo 1111;
	$chatAry = explode("============Message=========\r\n", $chatRow);
	
	$chatAry = array_filter($chatAry);
	$chatAry2 = $chatAry;
	
	foreach ($chatAry as $key => $val) {
		$decodeVal = json_decode($val, true);
	    $volume[$key]  = $decodeVal[events][0]['timestamp'];
	}
	array_multisort($volume, SORT_DESC, $chatAry);
	
	$userAry = array();
	foreach($chatAry as $key => $val) {
		$decodeVal = json_decode($val, true);
		$storeId = ($decodeVal['events'][0]['message']['store']) ? $decodeVal['events'][0]['message']['store'] : 1;
		if($storeId != $store) {
			continue;
		}
		$userAry[$storeId][$decodeVal[events][0][source][userId]][] = $decodeVal;
		//$userAry[$decodeVal[events][0][source][userId]][] = $decodeVal;
	}

	$newMsgAry = array();
	foreach($chatAry2 as $key => $val) {
		$decodeVal = json_decode($val, true);
		$storeId = ($decodeVal['events'][0]['message']['store']) ? $decodeVal['events'][0]['message']['store'] : 1;
		if($storeId != $store) {
			continue;
		}
		$newMsgAry[$storeId][$decodeVal[events][0][source][userId]][] = $decodeVal;
		//$newMsgAry[$decodeVal[events][0][source][userId]][] = $decodeVal;
	}
?>
<html class="">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet prefetch" href="./js/chat/reset.min.css">
	<link rel="stylesheet prefetch" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css"  href="./css/chat.css">
	<link rel="manifest" href="./manifest.json">
	<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.8.1/firebase-app.js"></script>
	<script src="https://www.gstatic.com/firebasejs/4.8.1/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.8.1/firebase-messaging.js"></script>
</head>
<body>
	<span id="log" style="color: white; display:none;"></span>
	<div class="container clearfix">
		<div class="people-list" id="people-list" style="display:">
			
			<div class="search">
				<input placeholder="search" type="text">
				<i class="fa fa-search"></i>
			</div>
			<ul class="list" style="height:720px; overflow-y:scroll;">
	<?php
		foreach($userAry[$store] as $key2 => $userInfo) {
			/*
			if($key2 == $userId) {
				$style = "-moz-animation: bg2 1.5s infinite; -webkit-animation: bg2 1.5s infinite;";
				$online = null;
			}
			*/
	?>		
				<li class="clearfix user" data-id="<?php echo $key2; ?>" style="cursor:pointer">
					<img src="<?php echo $userInfo[0][pictureUrl]; ?>" alt="<?php echo $userInfo[0][displayName]; ?>" style="border-radius:50%;" width="55" >
					<div class="about">
						<div class="name"><?php echo $userInfo[0][displayName]; ?></div>
						<div class="status" style="display:none">
							<i class="fa fa-circle online"></i> online
						</div>
					</div>
				</li>
	<?php
			$firstId[] = $key2;
		}
		//echo reset($firstId);
	?>	
			</ul>
		</div>
		<div id="chatAjax">
		<?php
			$i = 0;
			foreach($newMsgAry[$store] as $key2 => $msgInfo) {
				if(reset($firstId) != $key2) {
					continue;
				}
		?>    
			<div class="chat">
				<div class="chat-header clearfix">
					<img src="<?php echo $msgInfo[0][pictureUrl]; ?>" alt="<?php echo $msgInfo[0][displayName]; ?>" style="border-radius:50%;" width="55" >

					<div class="chat-about" data-userId="<?php echo $key2; ?>">
					  <div class="chat-with"><?php echo $msgInfo[0][displayName]; ?></div>
					  <div class="chat-num-messages"><?php echo count($msgInfo); ?>則訊息</div>
					</div>
					<!--<i class="fa fa-star"></i>-->
				</div> <!-- end chat-header -->
			  
				<div class="chat-history">
					<ul>
				<?php
					foreach($msgInfo as $key3 => $msg) {
						
						$sql2 = "SELECT * FROM web_lineChatlog WHERE messageId = '' and uname like '".$key2."' and date >= '".date('Y-m-d 00:00:00', strtotime('-7 day'))."'";
						$rs2 = ConnectDB($DB, $sql2);
						if (mysql_num_rows($rs2)) {
							for ($i2=0; $i2<mysql_num_rows($rs2); $i2++) {
								$row2 = mysql_fetch_assoc($rs2);
				?>	
							<li class="clearfix">
								<div class="message-data align-right">
									<span class="message-data-time"><?php echo date('H:i A Y-m-d', strtotime($row2[date])); ?></span> &nbsp; &nbsp;
									<span class="message-data-name"></span> <i class="fa fa-circle me"></i>

								</div>
								<div class="message other-message float-right">
									<?php echo $row2[log]; ?>
								</div>
							</li>
				<?php

							}
						}
				?>		
						<li class="msg" data-id="<?php echo $msg[events][0][message][id]; ?>" data-reply="<?php echo $msg[events][0][replyToken]; ?>">
							<div class="message-data">
								<span class="message-data-name"><i class="fa fa-circle online"></i> <?php echo $msgInfo[0][displayName]; ?></span> 
								<span class="message-data-time"><?php echo date('H:i A Y-m-d', ($msg[events][0][timestamp]/1000)); ?></span>
							</div>
							<div class="message my-message">
								<?php 
									if($msg[events][0][message][type] == "text") {
										echo $msg[events][0][message][text];
									} else if($msg[events][0][message][type] == "image") {
										if($msg[events][0][message][text]) {
											echo "<center><a href=\"".$msg[events][0][message][text]."\" data-lightbox=\"image-1\"><img class=\"chatPic\" src=\"".$msg[events][0][message][text]."\" width=\"80%\" /></a></center>";
										} else {
											echo "圖片無法顯示";
										}	
									} else if($msg[events][0][message][type] == "sticker") {
										echo "貼圖無法顯示";
									}		
								?>
							</div>
						</li>
				<?php
						$sql2 = "SELECT * FROM web_lineChatlog WHERE messageId like '".$msg[events][0][message][id]."' and uname like '".$key2."' and date >= '".date('Y-m-d 00:00:00', strtotime('-7 day'))."'";
						$rs2 = ConnectDB($DB, $sql2);
						if (mysql_num_rows($rs2)) {
							for ($i2=0; $i2<mysql_num_rows($rs2); $i2++) {
								$row2 = mysql_fetch_assoc($rs2);
				?>
							<li class="clearfix">
								<div class="message-data align-right">
									<span class="message-data-time"><?php echo date('H:i A Y-m-d', strtotime($row2[date])); ?></span> &nbsp; &nbsp;
									<span class="message-data-name"></span> <i class="fa fa-circle me"></i>

								</div>
								<div class="message other-message float-right">
									<?php 
										if($row2[type] == 'images') {
											echo "<center><a href=\"".$row2[log]."\" data-lightbox=\"image-1\"><img class=\"chatPic\" src=\"".$row2[log]."\" width=\"80%\" /></a></center>";
										} else {
											echo nl2br($row2[log]);
										}	
									?>
								</div>
							</li>
				
				<?php
							}
						}
					}
				?>	

					  
					</ul>
					
					
				
				</div> <!-- end chat-history -->
				
				
			  
				<div class="chat-message clearfix">
					<textarea name="message-to-send" id="message-to-send" placeholder="輸入你的訊息" rows="3"></textarea>
					<!--		
					<i class="fa fa-file-o"></i> &nbsp;&nbsp;&nbsp;
					-->
					
					<!--
					<input type="file" name="fileInput" id="fileInput" style="display:none ;" />
					<i class="fa fa-file-image-o"></i>
					-->
					<button>送出</button>

				</div> <!-- end chat-message -->
			  
			</div> <!-- end chat -->
		<?php
			 $i++;
			}
		?>
		</div>	
	</div> <!-- end container -->

	<script id="message-template" type="text/x-handlebars-template">
		<li class="clearfix">
			<div class="message-data align-right">
				<span class="message-data-time" >{{time}}, Today</span> &nbsp; &nbsp;
				<span class="message-data-name" ></span> <i class="fa fa-circle me"></i>
			</div>
			<div class="message other-message float-right">
				{{messageOutput}}
			</div>
		</li>
	</script>

	<script id="message-response-template" type="text/x-handlebars-template">
		<li>
			<div class="message-data">
				<span class="message-data-name"><i class="fa fa-circle online"></i> Vincent</span>
				<span class="message-data-time">{{time}}, Today</span>
			</div>
			<div class="message my-message">
				{{response}}
			</div>
		</li>
	</script>
	
	<script id="messageImg-template" type="text/x-handlebars-template">
		<li class="clearfix">
			<div class="message-data align-right">
				<span class="message-data-time" >{{time}}, Today</span> &nbsp; &nbsp;
				<span class="message-data-name" ></span> <i class="fa fa-circle me"></i>
			</div>
			<div class="message other-message float-right">
				<img src="{{{imgOutput}}}" width="80%"/>
			</div>
		</li>
	</script>

	<!--<script src="//static.codepen.io/assets/common/stopExecutionOnTimeout-b2a7b3fe212eaa732349046d8416e00a9dec26eb7fd347590fbced3ab38af52e.js"></script>
	-->
	<script src="./js/chat/handlebars.min.js"></script>
	<script src="./js/chat/list.min.js"></script>
	<link rel="stylesheet" href="./js/lightbox/lightbox.min.css">
	<script src="./js/lightbox/lightbox-plus-jquery.min.js"></script>
	<script>
		var messaging = null;
		
		var notification = null;
        function ShowNotification(title, body) {
            notification = new Notification(title, {
                //icon: '/icon/ms-icon-310x310.png',
                body: body,
                onclick: function () {
                    parent.focus();
                    window.focus(); //just in case, older browsers
                    this.close();
                }
            })
        }
		(function(){
			if (Notification && Notification.permission === 'default') {
                Notification.requestPermission(function (permission) {
                    if (!('permission' in Notification)) {
                        Notification.permission = permission;
                    }
                });
            }
            else if (Notification.permission === 'granted') {
                //alert('已經有取得權限了!');
            }
            else {
                alert('請檢查是否你的瀏覽器支援');
            }
			
			var showAlert = true;
			$(window).on("blur focus", function(e) {
				var prevType = $(this).data("prevType");

				if (prevType != e.type) {   //  reduce double fire issues
					switch (e.type) {
						case "blur":
							showAlert = true;
							break;
						case "focus":
							showAlert = false;
							break;
					}
				}

				$(this).data("prevType", e.type);
			})
			
			
			var config = {
                apiKey: "AIzaSyAoR8p0dcPP5_4krh4-B4n31BKfg1gF7To",
                authDomain: "aibi4-62739.firebaseapp.com",
                databaseURL: "https://aibi4-62739.firebaseio.com",
                projectId: "aibi4-62739",
                storageBucket: "aibi4-62739.appspot.com",
                messagingSenderId: "768333202033",
				appId: "1:768333202033:web:69ad8dd0983388c2bc11d0",
				measurementId: "G-NL6M5ET3SX"

            };
            firebase.initializeApp(config);
			
            messaging = firebase.messaging();
			
			messaging.getToken().then(function (currentToken) {
				$('#log').append("TOKEN: " + currentToken + "<br><br>")
				if (currentToken) {
					RegistUserTokenToSelfServer(currentToken);
				} 
			}).catch(function (err) {
				//$('#log').prepend("跟 Server 註冊失敗 原因:" + err + "<br>");
				alert("跟 Server 註冊失敗 原因:" + err + "<br>");
			});
			
            //收到訊息後的處理
			
            messaging.onMessage(function (payload) {
                
                //如果可以顯示通知就做顯示通知
				
				if (Notification.permission === 'granted') {
					ShowNotification(payload.notification.title, payload.notification.body);
					//三秒後自動關閉
					setTimeout(notification.close.bind(notification), 3000);
				}
				
				notification.addEventListener('click', function(e) {
					e.preventDefault();
					location.href = 'https://aibi4.aibitechcology.com/chat.php';
				});
				
				if(showAlert) {
					alert('你有新訊息');
					//location.reload();	
				}
				
				//console.log(payload.notification, JSON.stringify(payload.data), payload.data['gcm.notification.data']);
				//var obj = $.parseJSON(payload.notification.body);
				//console.log(obj);
				if(payload.data['gcm.notification.data']) {
					
					if(payload.data['gcm.notification.data'] == $('.chat-about').attr('data-userid')) {
						$.ajax({
							method: "POST",
							url: "chatAjax.php",
							data: { userId: payload.data['gcm.notification.data'], store: '<?php echo $store; ?>', token: '<?php echo $_SESSION['token']; ?>'}
						}).done(function( msg ) {
							
							$('#chatAjax').html(msg);
							chat.cacheDOM();
							chat.scrollToBottom();
							chat.bindEvents();
							
						});
					} 
					
					$.ajax({
						method: "POST",
						url: "userAjax.php",
						data: { userId: payload.data['gcm.notification.data'], store: '<?php echo $store; ?>'}
					}).done(function( msg ) {
						
						$('.list').html(msg);
						user.click();
					});
				}
				
            });
			
			// Enable pusher logging - don't include this in production
			/*
			Pusher.logToConsole = true;

			var pusher = new Pusher('039734996a5a25e144b5', {
				cluster: 'ap1',
				encrypted: true
			});
			*/		
			//});
			
		  
			var chat = {
				messageToSend: '',
				messageResponses: [
					'Why did the web developer leave the restaurant? Because of the table layout.',
					'How do you comfort a JavaScript bug? You console it.',
					'An SQL query enters a bar, approaches two tables and asks: "May I join you?"',
					'What is the most used language in programming? Profanity.',
					'What is the object-oriented way to become wealthy? Inheritance.',
					'An SEO expert walks into a bar, bars, pub, tavern, public house, Irish pub, drinks, beer, alcohol'
				],
				init: function() {
					this.cacheDOM();
					this.bindEvents();
					this.render();
				},
				cacheDOM: function() {
					this.$chatHistory = ($('.chat-history').length) ? $('.chat-history') : null;
					this.$button = $('button');
					this.$textarea = $('#message-to-send');
					this.$chatHistoryList = ($('.chat-history').length) ? this.$chatHistory.find('ul') : null;
				},
				bindEvents: function() {
					this.$button.on('click', this.addMessage.bind(this));
					this.$textarea.on('keyup', this.addMessageEnter.bind(this));
				},
				render: function() {
					this.scrollToBottom();
					if (this.messageToSend.trim() !== '') {
						var template = Handlebars.compile( $("#message-template").html());
						var context = { 
							messageOutput: this.messageToSend.replace('\n', ''),
							time: this.getCurrentTime(),
							userId: $('.chat-about').attr('data-userId'),
							msgId: $('.msg:last').attr('data-id'),
							replyToken: $('.msg:last').attr('data-reply'),
						};
						//console.log(JSON.stringify(context), context.userId);

						$.ajax({
							method: "POST",
							url: "./api.php",
							data: { 
								ID: context.userId,
								replyToken: context.replyToken,
								contnet: context.messageOutput, 
								msgId: context.msgId, 
								time: context.time, 
								action: 'chat' 
							}
						});

						this.$chatHistoryList.append(template(context));
						this.scrollToBottom();
						this.$textarea.val('');
						/*
						// responses
						var templateResponse = Handlebars.compile( $("#message-response-template").html());
						var contextResponse = { 
							response: this.getRandomItem(this.messageResponses),
							time: this.getCurrentTime(),
							userId: $('.chat-about').attr('data-userId'),
							msgId: $('.msg:last').attr('data-id'),
						};
						
						setTimeout(function() {
							this.$chatHistoryList.append(templateResponse(contextResponse));
							this.scrollToBottom();
						}.bind(this), 1500);
						*/
					}
				  
				},
				
				addMessage: function() {
					this.messageToSend = this.$textarea.val()
					this.render();         
				},
				addMessageEnter: function(event) {
					// enter was pressed
					if (event.keyCode === 13 && !event.shiftKey) {
						this.addMessage();
					}
				},
				addImage: function(img) {
					var template = Handlebars.compile( $("#messageImg-template").html());
					var context = { 
						imgOutput: img,
						time: this.getCurrentTime(),
						userId: $('.chat-about').attr('data-userId'),
						msgId: $('.msg:last').attr('data-id'),
					};
					
					//console.log(JSON.stringify(context), context.userId);
					
					
					$.ajax({
						method: "POST",
						url: "./api.php",
						data: { 
							ID: context.userId, 
							contnet: context.imgOutput, 
							msgId: context.msgId, 
							time: context.time, 
							action: 'img' 
						}
					});
					

					this.$chatHistoryList.append(template(context));
					this.scrollToBottom();
					this.$textarea.val('');
				},
				scrollToBottom: function() {
					if(this.$chatHistory) {
						this.$chatHistory.scrollTop(this.$chatHistory[0].scrollHeight);
					}	
				},
				getCurrentTime: function() {
					return new Date().toLocaleTimeString().
					replace(/([\d]+:[\d]{2})(:[\d]{2})(.*)/, "$1$3");
				},
				getRandomItem: function(arr) {
					return arr[Math.floor(Math.random()*arr.length)];
				}
				
			};
				
			chat.init();
			  
			var searchFilter = {
				options: { valueNames: ['name'] },
				init: function() {
					var userList = new List('people-list', this.options);
					var noItems = $('<li id="no-items-found">No items found</li>');

					userList.on('updated', function(list) {
					if (list.matchingItems.length === 0) {
							$(list.list).append(noItems);
						} else {
							noItems.detach();
						}
					});
				}
			};
			  
			searchFilter.init();
			  
			var user = {

				init: function() {
					this.click();
					var firstUser = $('.user').eq(0).attr('data-id');
					$('[data-id="'+firstUser+'"]').trigger('click');
					
				},
				click: function() {
					$('.user').on('click', function() {
						//console.log($(this).attr('data-id'));
						if(!$(this).attr('data-id')) {
							return;
						}
						$.ajax({
							method: "POST",
							url: "chatAjax.php",
							data: { userId: $(this).attr('data-id'), store: '<?php echo $store; ?>', token: '<?php echo $_SESSION['token']; ?>'}
						}).done(function( msg ) {
							$('#chatAjax').html(msg);
							chat.cacheDOM();
							chat.scrollToBottom();
							chat.bindEvents();
							//user.click();
							//images.click();
							images.init();
						});
					});
				}
			};
			user.init();
			
			var images = {
				
				init: function() {
					this.click();
					//prevent browsers from opening the file when its dragged and dropped
					$(document).on('drop dragover', function (e) {
						e.preventDefault();
					});
					
					//call a function to handle file upload on select file
					$('input[type=file]').on('change', this.fileUpload);
					
				},
				click: function() {
					//file input field trigger when the drop box is clicked
					$(".fa-file-image-o").click(function(){
						$("#fileInput").click();
					});
				},
				fileUpload: function(event) {
					//notify user about the file upload status
					//$("#dropBox").html(event.target.value+" uploading...");
					//alert(11111);
					//get selected file
					files = event.target.files;
					
					//form data check the above bullet for what it is  
					var data = new FormData();                                   

					//file data is presented as an array
					for (var i = 0; i < files.length; i++) {
						var file = files[i];
						if(!file.type.match('image.*')) {              
							//check file type
							//$("#dropBox").html("Please choose an images file.");
							alert("Please choose an images file.");
						}else if(file.size > 1048576){
							//check file size (in bytes)
							//$("#dropBox").html("Sorry, your file is too large (>1 MB)");
							alert("Sorry, your file is too large (>1 MB)");
						}else{
							//append the uploadable file to FormData object
							data.append('file', file, file.name);
							console.log(file);
							//create a new XMLHttpRequest
							var xhr = new XMLHttpRequest();     
							
							//post file data for upload
							xhr.open('POST', 'lineUpload.php', true);  
							xhr.send(data);
							xhr.onload = function () {
								//get response and show the uploading status
								var response = JSON.parse(xhr.responseText);
								if(xhr.status === 200 && response.status == 'ok'){
									//$("#dropBox").html("File has been uploaded successfully. Click to upload another.");
									alert("檔案上傳成功");
									chat.addImage(""+response.filePath+"");
								}else if(response.status == 'type_err'){
									//$("#dropBox").html("Please choose an images file. Click to upload another.");
									alert("只能上傳圖片");
								}else{
									//$("#dropBox").html("Some problem occured, please try again.");
									alert("檔案上傳失敗，請重新上傳");
								}
							};
						}
					}
				}
			};
			images.init();			
			
		})();
		
		function RegistUserTokenToSelfServer(user_token, successFunc, errorFunc) {
            var $res = '';
            $.ajax({
                type: "POST",
                url: "./action",
                contentType: 'application/x-www-form-urlencoded',
                async: true,
                cache: false,
                dataType: 'text',
                data: { user_token: user_token, action: "firebaseToken", token: '<?php echo $_SESSION['token']; ?>' },
                success: function (data) {
					console.log(data);
                    if (data.hasOwnProperty("d")) {
                        $res = data.d;
                        if (successFunc != null)
                            successFunc(data.d);
                    }
                    else {
                        $res = data;
                        if (successFunc != null)
                            successFunc(data);
                    }
                },
                error: function (e) {
                    if (errorFunc != null)
                        errorFunc(e);
                }
            });
            return $res;
        }
		//# sourceURL=pen.js
	</script>
</body>
</html>