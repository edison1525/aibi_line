<?php
	include_once("../control/includes/function.php");
	$web_member_id = $_GET['web_member_id'];
	$store = $_GET['store'];
	$id = $_GET['id'];
	if(!$web_member_id) {
		return;
	}
	
	$storedInfoSql = "
		Select 
			SQL_CALC_FOUND_ROWS a.*,
			b.subject as bsubject,
			b.web_product_id as bproductId,
			b.price as bprice,
			b.num as bnum,
			b.dimension as bdimension,
			c.subject as csubject,
			c.price_cost as cprice_cost,
			c.price_member as cprice_member,
			c.totalCount as ctotalCount,
			d.web_xx_product_id as dweb_xx_product_id,
			d.subject as dsubject,
			e.subject as esubject,
			(select subject from web_x_class where web_x_class.web_x_class_id = a.store_id) as xClassSubject
		From 
			web_x_order a
		Left Join 
			web_order b ON b.web_x_order_ordernum = a.ordernum
		Left Join 
			web_product c ON c.web_product_id = b.web_product_id
		Left Join 
			web_x_product d ON d.web_x_product_id = c.web_x_product_id
		Left Join 
			web_xx_product e ON e.web_xx_product_id = d.web_xx_product_id	
		Where 
			a.web_member_id = :web_member_id 
		AND
			a.states = '訂單成立'
		AND
			a.paymentstatus = '付款成功'
		AND
			d.web_xx_product_id IN (2)
		AND
			a.order_type = 0	
		order by 
			a.web_x_order_id desc
	";
	$excute = array(
		':web_member_id'	=> $web_member_id,
	);

	//$debug = new Helper();
	//echo $test = $debug::debugPDO($sql, $excute);
	$pdo = $pdoDB->prepare($storedInfoSql);
	$pdo->execute($excute);
	$storedInfoRow = $pdo->fetchAll(PDO::FETCH_ASSOC);
	
	$useStoredInfoSql = "
		Select 
			SQL_CALC_FOUND_ROWS a.*,
			(select subject from web_x_class where web_x_class.web_x_class_id = a.store_id) as xClassSubject
		From 
			web_x_order a
		Where 
			a.web_member_id = :web_member_id
		AND
			a.states = '訂單成立'
		AND
			a.paymentstatus = '付款成功'
		AND
			a.order_type = 2	
		order by 
			a.web_x_order_id desc 
	";
	$excute = array(
		':web_member_id'	=> $web_member_id,
	);

	//$debug = new Helper();
	//echo $test = $debug::debugPDO($sql, $excute);
	$pdo = $pdoDB->prepare($useStoredInfoSql);
	$pdo->execute($excute);
	$useStoredInfoRow = $pdo->fetchAll(PDO::FETCH_ASSOC);
?>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet prefetch" href="./js/chat/reset.min.css">
    <link rel="stylesheet" type="text/css" href="./css/chat_0203.css">
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
</head>
<body>
    <div class="t-wrapper">
        <div class="t-container">
            <div class="table">
                <div class="table-header">
                    <div class="table-header__back" onclick="location.href='./chat.php?store=<?php echo $store; ?>&id=<?php echo $id; ?>'">
                        <i class="icon icon-back"></i>
                        <span>返回</span>
                    </div>
                    <div class="table-header__title">
                        <h3>儲值紀錄</h3>
                    </div>
                </div>
                <div class="table-inner" style="height:70%; overflow-y:auto;">
                    <table>
                        <tr>
                            <th>類別</th>
                            <th>儲值時間</th>
                            <th>館別</th>
                            <th>金額</th>
                            <th>經手人</th>
                            <th>開通時間</th>
                            <th>狀態</th>
                        </tr>
			<?php
				foreach($storedInfoRow as $key => $storedInfo) {
					/*
					$sql3 = "Select subject From web_order Where web_x_order_ordernum like '".$storedInfo['ordernum']."'";
					$rs3 = ConnectDB($DB, $sql3);
					$prodSubject = mysql_result($rs3, 0, "subject");
					*/
					switch($storedInfo['order_type']) {
						case '0' :
						  $orderTypeText = "一般";
						  break;
						case '1' :
						  $orderTypeText = "票券使用";
						  break;
						case '2' :
						  $orderTypeText = "儲值使用";
						  break;
								
					}
			?>		
                        <tr>
                            <td><?php echo $orderTypeText; ?></td>
                            <td><?php echo $storedInfo['cdate']; ?></td>
                            <td><?php echo $storedInfo['xClassSubject']; ?></td>
                            <td align="right"><?php echo number_format($storedInfo["cprice_cost"]); ?></td>
                            <td>
								<?php 
									if($storedInfo['states'] == '取消') {
										echo $storedInfo['cancelUser']; 
									} else {	
										echo $storedInfo['editUser']; 
									}	
								?>
							</td>
                            <td>
								<?php 
									if($storedInfo['states'] == '取消') {
										echo $storedInfo['cancelDateTime']; 
									} else {	
										echo $storedInfo['editDateTime'];
									}	
								?>
							</td>
                            <td><?php echo $storedInfo['states']."</br>".$storedInfo['paymentstatus']; ?></td>
                        </tr>
            <?php
					$storedTotal += $storedInfo["cprice_cost"];
				}
			?>
			<?php
				foreach($useStoredInfoRow as $key => $useStoredInfo) {
					/*
					$sql3 = "Select subject From web_order Where web_x_order_ordernum like '".$useStoredInfo['ordernum']."'";
					$rs3 = ConnectDB($DB, $sql3);
					$prodSubject = mysql_result($rs3, 0, "subject");
					*/
					switch($useStoredInfo['order_type']) {
						case '0' :
						  $orderTypeText = "一般";
						  break;
						case '1' :
						  $orderTypeText = "票券使用";
						  break;
						case '2' :
						  $orderTypeText = "儲值使用";
						  break;
								
					}
			?>		
                        <tr>
                            <td><?php echo $orderTypeText; ?></td>
                            <td><?php echo $useStoredInfo['cdate']; ?></td>
                            <td><?php echo $useStoredInfo['xClassSubject']; ?></td>
                            <td align="right">- <?php echo number_format($useStoredInfo["total"]); ?></td>
                            <td>
								<?php 
									if($useStoredInfo['states'] == '取消') {
										echo $useStoredInfo['cancelUser']; 
									} else {	
										echo $useStoredInfo['editUser']; 
									}	
								?>
							</td>
                            <td>
								<?php 
									if($useStoredInfo['states'] == '取消') {
										echo $useStoredInfo['cancelDateTime']; 
									} else {	
										echo $useStoredInfo['editDateTime'];
									}	
								?>
							</td>
                            <td><?php echo $useStoredInfo['states']."</br>".$useStoredInfo['paymentstatus']; ?></td>
                        </tr>
            <?php
					$useTotal += $useStoredInfo["total"];
				}
				
				$balance = abs($storedTotal - $useTotal);
			?>	
						<tr>
						  <td colspan="6" class="Title">總計</td>
						  <td colspan="1" align="right"><span class="font_grayred"><?php echo "$".number_format($balance); ?></span></td>
						</tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</body>
</html>