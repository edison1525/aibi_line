// 開啟 tag-selector
$('.tag-selector__overlay, .tag-selected__btn').on('click', function () {
    $('.tag-selector').fadeOut();
});

// 關閉 tag-selector
$('.tag-btn, .tag-inner li').on('click', function () {
    $('.tag-selector').fadeIn();
});

//tag dropdown
$('.tag-dropdown__title').on('click', function () {
    $(this).siblings('.tag-dropdown__inner').slideToggle();
    $(this).toggleClass('-isClose');
});

// 刪 已選擇的標籤
$('.tag-selected__inner li').on('click', removeTag);

// 刪標籤
function removeTag() {
    var tagId = $(this).attr('data-tagId');
    $(this).remove();
    $('.tag-dropdown__inner li[data-tagId="' + tagId + '"]').removeClass('-selected');
}

// 加標籤
$('.tag-dropdown__inner li').on('click', function () {
    if ($(this).hasClass('-selected') == false) {
        var tagColor = '#BFECBF';
        var tagId = $(this).attr('data-tagId');
        var tagContent = $(this).text();
        var newTag = $('<li data-tagId="' + tagId + '" style="background-color: ' + tagColor + '"></li>');
        
        // 新增、綁 刪標籤
        newTag.append(tagContent).on('click', removeTag);
        $('.tag-selected__inner ul').append(newTag);
        $('.tag-dropdown__inner li[data-tagId="' + tagId + '"]').addClass('-selected');

    }
});


// <1024 sidebar開合
$('.chat-btn__info').on('click', function () {
    $('.chat').addClass('-overlay');
    $('.sidebar').addClass('-active');
    
});
$('.chat-overlay').on('click', function () {
    $('.chat').removeClass('-overlay');
    $('.sidebar').removeClass('-active');
});

// <640 chat開合
$('.people-list li.user').on('click',function(){
    $('#chatAjax').addClass('-active');
})

$('.chat-btn__back').on('click',function(){
    $('#chatAjax').removeClass('-active');
})