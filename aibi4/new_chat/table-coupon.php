<?php
	include_once("../control/includes/function.php");
	$web_member_id = $_GET['web_member_id'];
	$store = $_GET['store'];
	$id = $_GET['id'];
	if(!$web_member_id) {
		return;
	}
	$couponInfoSql = "
		Select 
			SQL_CALC_FOUND_ROWS a.*,
			b.subject as bsubject,
			b.web_product_id as bproductId,
			b.price as bprice,
			b.num as bnum,
			b.dimension as bdimension,
			c.subject as csubject,
			c.price_cost as cprice_cost,
			c.price_member as cprice_member,
			c.totalCount as ctotalCount,
			d.web_xx_product_id as dweb_xx_product_id,
			d.subject as dsubject,
			e.subject as esubject,
			(select subject from web_x_class where web_x_class.web_x_class_id = a.store_id) as xClassSubject
		From 
			web_x_order a
		Left Join 
			web_order b ON b.web_x_order_ordernum = a.ordernum
		Left Join 
			web_product c ON c.web_product_id = b.web_product_id
		Left Join 
			web_x_product d ON d.web_x_product_id = c.web_x_product_id
		Left Join 
			web_xx_product e ON e.web_xx_product_id = d.web_xx_product_id	
		Where 
			a.web_member_id = '".$web_member_id."' 
		AND
			a.states = '訂單成立'
		AND
			a.paymentstatus = '付款成功'
		AND
			d.web_xx_product_id IN (1)
		AND
			a.order_type = 0	
		order by 
			a.web_x_order_id desc
	";
	$pdo = $pdoDB->prepare($couponInfoSql);
	$pdo->execute();
	$couponInfoRow = $pdo->fetchAll(PDO::FETCH_ASSOC);
?>	
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet prefetch" href="./js/chat/reset.min.css">
    <link rel="stylesheet" type="text/css" href="./css/chat_0203.css">
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
</head>
<body>
    <div class="t-wrapper">
        <div class="t-container">
            <div class="table">
                <div class="table-header">
                    <div class="table-header__back" onclick="location.href='./chat.php?store=<?php echo $store; ?>&id=<?php echo $id; ?>'">
                        <i class="icon icon-back"></i>
                        <span>返回</span>
                    </div>
                    <div class="table-header__title">
                        <h3>票券紀錄</h3>
                    </div>
                </div>
                <div class="table-inner" style="height:70%; overflow-y:auto;">
                    <table>
                        <tr>
                            <th>類別</th>
                            <th>品名</th>
                            <th>館別</th>
                            <th>金額</th>
                            <th>經手人</th>
                            <th>開通時間</th>
                            <th>狀態</th>
                        </tr>
				<?php
					foreach($couponInfoRow as $key => $couponInfo) {
						switch($couponInfo['order_type']) {
							case '0' :
							  $orderTypeText = "一般";
							  break;
							case '1' :
							  $orderTypeText = "票券使用";
							  break;
							case '2' :
							  $orderTypeText = "儲值使用";
							  break;
									
						}
				?>	
                        <tr>
                            <td><?php echo $orderTypeText; ?></td>
                            <td><?php echo $couponInfo["bsubject"]."/可用:".$couponInfo["ctotalCount"]."次"; ?></td>
                            <td><?php echo $couponInfo["xClassSubject"]; ?></td>
                            <td align="right"><?php echo number_format($couponInfo["cprice_cost"]); ?></td>
                            <td>
								<?php 
									if($couponInfo['states'] == '取消') {
										echo $couponInfo['cancelUser']; 
									} else {	
										echo $couponInfo['editUser']; 
									}	
								?>
							</td>
                            <td>
								<?php 
									if($couponInfo['states'] == '取消') {
										echo $couponInfo['cancelDateTime']; 
									} else {	
										echo $couponInfo['editDateTime'];
									}	
								?>
							</td>
                            <td><?php echo $couponInfo["states"]."</br>".$couponInfo["paymentstatus"]; ?></td>
                        </tr>
				<?php
						$useCouponInfoSql = "
							Select 
								SQL_CALC_FOUND_ROWS a.*,
								b.subject as bsubject,
								b.web_product_id as bproductId,
								b.price as bprice,
								b.num as bnum,
								b.dimension as bdimension,
								c.subject as csubject,
								c.price_cost as cprice_cost,
								c.price_member as cprice_member,
								c.totalCount as ctotalCount,
								d.web_xx_product_id as dweb_xx_product_id,
								d.subject as dsubject,
								e.subject as esubject,
								(select subject from web_x_class where web_x_class.web_x_class_id = a.store_id) as xClassSubject
							From 
								web_x_order a
							Left Join 
								web_order b ON b.web_x_order_ordernum = a.ordernum
							Left Join 
								web_product c ON c.web_product_id = b.web_product_id
							Left Join 
								web_x_product d ON d.web_x_product_id = c.web_x_product_id
							Left Join 
								web_xx_product e ON e.web_xx_product_id = d.web_xx_product_id	
							Where 
								a.from_ordernum = '".$couponInfo['ordernum']."' 
							AND
								a.states = '訂單成立'
							AND
								a.paymentstatus = '付款成功'
							AND
								d.web_xx_product_id IN (1)
							AND
								a.order_type = 1	
							order by 
								a.web_x_order_id desc 
						";
						$pdo = $pdoDB->prepare($useCouponInfoSql);
						$pdo->execute();
						$useCouponInfoRow = $pdo->fetchAll(PDO::FETCH_ASSOC);
						foreach($useCouponInfoRow as $key => $useCouponInfo) {
							switch($useCouponInfo['order_type']) {
								case '0' :
								  $orderTypeText = "一般";
								  break;
								case '1' :
								  $orderTypeText = "票券使用";
								  break;
								case '2' :
								  $orderTypeText = "儲值使用";
								  break;
										
							}
				?>	
							<tr style="background:#F4E1E1; color:#666666">
								<td><?php echo $orderTypeText; ?></td>
								<td><?php echo $useCouponInfo["bsubject"]."/已用:1次"; ?></td>
								<td><?php echo $useCouponInfo["xClassSubject"]; ?></td>
								<td align="right"></td>
								<td>
									<?php 
										if($useCouponInfo['states'] == '取消') {
											echo $useCouponInfo['cancelUser']; 
										} else {	
											echo $useCouponInfo['editUser']; 
										}	
									?>
								</td>
								<td>
									<?php 
										if($useCouponInfo['states'] == '取消') {
											echo $useCouponInfo['cancelDateTime']; 
										} else {	
											echo $useCouponInfo['editDateTime'];
										}	
									?>
								</td>
								<td><?php echo $useCouponInfo["states"]."</br>".$useCouponInfo["paymentstatus"]; ?></td>
							</tr>
                <?php
						}
					}
				?>	
                    </table>
                </div>
            </div>
        </div>
    </div>
</body>
</html>