<?php
	include_once("../control/includes/function.php");
	$web_member_id = $_GET['web_member_id'];
	$store = $_GET['store'];
	$id = $_GET['id'];
	if(!$web_member_id) {
		return;
	}
	
	$registerInfoSql = "
		Select 
				SQL_CALC_FOUND_ROWS 
				a.*,
				c.subject as classSubject,
				d.subject as teamSubject,
				d.time as teamTime,
				e.subject as xClassSubject
			From 
				web_x_register a
			Left Join 
				web_class c ON c.web_class_id = a.web_time_id 		
			Left Join 
				web_x_team d ON d.web_x_team_id = a.web_x_team_id
			Left Join 
				web_x_class e ON e.web_x_class_id = c.web_x_class_id 	
			Where 
				a.web_member_id = :web_member_id 
			order by 
				a.registerDate DESC, c.web_class_id ASC, a.ordernum ASC
	";
	$excute = array(
		':web_member_id'	=> $web_member_id,
	);

	//$debug = new Helper();
	//echo $test = $debug::debugPDO($sql, $excute);
	$pdo = $pdoDB->prepare($registerInfoSql);
	$pdo->execute($excute);
	$registerInfoRow = $pdo->fetchAll(PDO::FETCH_ASSOC);
?>	
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet prefetch" href="./js/chat/reset.min.css">
    <link rel="stylesheet" type="text/css" href="./css/chat_0203.css">
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
</head>
<body>
    <div class="t-wrapper">
        <div class="t-container">
            <div class="table">
                <div class="table-header">
					<div class="table-header__back" onclick="location.href='./chat.php?store=<?php echo $store; ?>&id=<?php echo $id; ?>'">
                        <i class="icon icon-back"></i>
                        <span>返回</span>
                    </div>
                    <div class="table-header__title">
                        <h3>預約紀錄</h3>
                    </div>
                </div>
                <div class="table-inner" style="height:70%; overflow-y:auto;">
                    <table>
                        <tr>
                            <th>預約時間</th>
                            <th>服務項目</th>
                            <th>館別</th>
                            <th>設計師</th>
                            <th>消費人</th>
                            <th>消費時間</th>
                            <th>狀態</th>
                        </tr>
				<?php
					foreach($registerInfoRow as $key => $registerInfo) {
				?>	
                        <tr>
                            <td><?php echo $registerInfo['cdate']; ?></td>
                            <td><?php echo $registerInfo['teamSubject']; ?>-<?php echo $registerInfo['teamTime']; ?>HR</td>
                            <td><?php echo $registerInfo['xClassSubject']; ?></td>
                            <td><?php echo $registerInfo['classSubject']; ?></td>
                            <td><?php echo $registerInfo['accept_name']; ?></td>
                            <td><?php echo $registerInfo['subject']; ?></td>
                            <td><?php echo $registerInfo['paymentstatus']; ?></td>
                        </tr>
                <?php
					}
				?>	
                    </table>
                </div>
            </div>
        </div>
    </div>
</body>
</html>