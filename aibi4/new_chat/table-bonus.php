<?php
	include_once("../control/includes/function.php");
	$web_member_id = $_GET['web_member_id'];
	$store = $_GET['store'];
	$id = $_GET['id'];
	if(!$web_member_id) {
		return;
	}
	$bonusInfosql = "
		Select 
			a.*,
			a.money as amoney,
			a.sdate as asdate,
			a.edate as aedate,
			b.*,
			c.*,
			(select subject from web_x_class where web_x_class.web_x_class_id = b.store_id) as xClassSubject
		From 
			web_bonus a
		Left Join 
			web_x_order b ON b.ordernum = a.web_x_order_from_ordernum
		Left Join 
			web_x_bonus c ON c.web_x_bonus_id = a.web_x_bonus_id	
		Where 
			a.web_member_id = '".$web_member_id."'
		AND
			b.states = '訂單成立'
		AND
			b.paymentstatus = '付款成功'
		order by 
			a.web_bonus_id desc 
	";
	$pdo = $pdoDB->prepare($bonusInfosql);
	$pdo->execute();
	$bonusInfoRow = $pdo->fetchAll(PDO::FETCH_ASSOC);
	
	$useBonusInfoSql = "
		SELECT 
			SQL_CALC_FOUND_ROWS a.*,
			b.web_member_id as bweb_member_id,
			b.uname as buname,
			b.birthday as bbirthday,
			b.mobile as bmobile,
			b.lineID as blineID,
			web_convert.subject as prodSubjects,
			web_convert.web_order_id as web_order_id,
			web_convert.web_x_order_ordernum as web_x_order_ordernum,
			web_convert.web_product_id as web_order_product_id,
			web_product.web_product_id as web_product_product_id,
			web_product.web_x_product_id as web_product_x_product_id,
			web_product.totalCount as web_product_totalCount,
			web_x_product.web_x_product_id as web_x_product_x_product_id,
			web_x_product.subject as web_x_product_subject,
			web_x_product.web_xx_product_id as web_x_product_xx_product_id,
			(select subject from web_x_class where web_x_class.web_x_class_id = a.store_id) as xClassSubject
		FROM 
			web_x_convert a
		Left Join 
			web_member b 
		ON 
			b.web_member_id = a.web_member_id
		Left Join
			web_convert as web_convert
		On
			web_convert.web_x_order_ordernum = a.ordernum
		Left Join
			web_product
		On
			web_product.web_product_id = web_convert.web_product_id
		Left Join
			web_x_product
		On
			web_x_product.web_x_product_id = web_product.web_x_product_id	
		where 
			a.web_member_id = '".$web_member_id."'
		Order By
			a.web_x_order_id DESC
	";
	$pdo = $pdoDB->prepare($useBonusInfoSql);
	$pdo->execute();
	$useBonusInfoRow = $pdo->fetchAll(PDO::FETCH_ASSOC);
?>	
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet prefetch" href="./js/chat/reset.min.css">
    <link rel="stylesheet" type="text/css" href="./css/chat_0203.css">
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
</head>
<body>
    <div class="t-wrapper">
        <div class="t-container">
            <div class="table">
                <div class="table-header">
                    <div class="table-header__back" onclick="location.href='./chat.php?store=<?php echo $store; ?>&id=<?php echo $id; ?>'">
                        <i class="icon icon-back"></i>
                        <span>返回</span>
                    </div>
                    <div class="table-header__title">
                        <h3>兌點紀錄</h3>
                    </div>
                </div>
                <div class="table-inner" style="height:70%; overflow-y:auto;">
                    <table>
                        <tr>
                            <th>類別</th>
                            <th>訂單時間</th>
                            <th>館別</th>
                            <th>商品</th>
                            <th>點數</th>
                            <th>經手人</th>
                            <th>開通時間</th>
                            <th>狀態</th>
							<th>期限</th>
                        </tr>
				<?php
					foreach($bonusInfoRow as $key => $bonusInfo) {
						$sql = "
							Select 
								a.web_order_id,
								a.subject,
								a.price,
								a.web_product_id,
								c.subject as csubject,
								c.web_x_product_id
							From 
								web_order a
							Left Join
								web_product b
							On
								b.web_product_id = a.web_product_id
							Left Join
								web_x_product c
							On
								c.web_x_product_id = b.web_x_product_id	
							Where 
								a.web_x_order_ordernum = :web_x_order_ordernum
						";
						$excute = array(
							':web_x_order_ordernum'        => $bonusInfo['ordernum'],
						);
						$pdo = $pdoDB->prepare($sql);
						$pdo->execute($excute);
						$_orderRow = $pdo->fetchAll(PDO::FETCH_ASSOC);
						$prodSubjectAry = array();
						foreach($_orderRow as $_key => $_orderVal) {
							$prodSubjectAry[] = ($_orderVal['web_x_product_id'] == '1') ? substr($_orderVal['csubject'], 0, 6)."-".$_orderVal['subject']." ".$_orderVal['price'] : substr($_orderVal['csubject'], 0, 6)."-".$_orderVal['subject'];
						}
				?>	
                        <tr>
                            <td>獲得</td>
                            <td><?php echo $bonusInfo["cdate"]; ?></td>
                            <td><?php echo $bonusInfo['xClassSubject']; ?></td>
                            <td><?php echo implode('</br>', $prodSubjectAry); ?></td>
                            <td><?php echo $bonusInfo['money']; ?></td>
                            <td>
								<?php 
									if($bonusInfo['states'] == '取消') {
										echo $bonusInfo['cancelUser']; 
									} else {	
										echo $bonusInfo['editUser']; 
									}	
								?>
							</td>
                            <td>
								<?php 
									if($bonusInfo['states'] == '取消') {
										echo $bonusInfo['cancelDateTime']; 
									} else {	
										echo $bonusInfo['editDateTime'];
									}	
								?>
							</td>
                            <td><?php echo $bonusInfo["states"]."</br>".$bonusInfo["paymentstatus"]; ?></td>
							<td><?php echo $bonusInfo["asdate"]." - ".$bonusInfo["aedate"]; ?></td>
                        </tr>
                <?php
					}
				?>
				<?php
					foreach($useBonusInfoRow as $key => $useBonusInfo) {
						
				?>	
                        <tr>
                            <td>兌換</td>
                            <td><?php echo $useBonusInfo["cdate"]; ?></td>
                            <td><?php echo $useBonusInfo['xClassSubject']; ?></td>
                            <td><?php echo $useBonusInfo['prodSubjects']; ?></td>
                            <td><?php echo $useBonusInfo['total']; ?></td>
                            <td>
								<?php 
									if($useBonusInfo['states'] == '取消') {
										echo $useBonusInfo['cancelUser']; 
									} else {	
										echo $useBonusInfo['editUser']; 
									}	
								?>
							</td>
                            <td>
								<?php 
									if($useBonusInfo['states'] == '取消') {
										echo $useBonusInfo['cancelDateTime']; 
									} else {	
										echo $useBonusInfo['editDateTime'];
									}	
								?>
							</td>
                            <td><?php echo $useBonusInfo["states"]."</br>".$useBonusInfo["paymentstatus"]; ?></td>
							<td></td>
                        </tr>
                <?php
					}
					
					$sql = "
						Select 
							SUM(a.money) as sum,
							SUM(a.usemoney) as sumUse
						From 
							web_bonus a
						Left Join 
							web_x_order b ON b.ordernum = a.web_x_order_from_ordernum
						Where 
							a.web_member_id = '".$web_member_id."'
						AND
							a.sdate <= '".date('Y-m-d')."'
						AND
							a.edate >= '".date('Y-m-d')."'
						AND
							b.states = '訂單成立'
						AND
							b.paymentstatus = '付款成功'
						order by 
							a.web_bonus_id desc 
					";
					$pdo = $pdoDB->prepare($sql);
					$pdo->execute();
					$bonusTotal = $pdo->fetch(PDO::FETCH_ASSOC);
					$bonus = abs($bonusTotal['sum'] - $bonusTotal['sumUse']);
				?>	
						<tr>
						  <td colspan="8" class="Title">總計</td>
						  <td colspan="1" align="right"><span class="font_grayred">
							<?php echo "".number_format($bonus); ?>點</span>
						  </td>
						</tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</body>
</html>