<?php
	include_once("./control/includes/function.php");

	require_once('function.php');		//line function

    $channel_id = $Channel_ID;
    $channel_secret = $Channel_Secret;
    $channel_access_token = $Channel_Token;
    $myURL = "https://".$_SERVER['HTTP_HOST']."/uploadfiles/";
	
	$action = ($_POST['action']);
	$content_type = ($_POST['action'] == 'img') ? "images" : "text";
	$from = explode(',', $_POST['ID']);

	$from = (count($from) >= 2) ? $from : $from[0];
	$message = $_POST['contnet'];
	$reToken = $_POST['replyToken'];
	
	file_put_contents("./log/debug/api_".date("Ymd").".log", date("Y-m-d H:i:s")."\r\n", FILE_APPEND | LOCK_EX);
	foreach($_REQUEST as $_key => $_value) {
		$row2[$_key] = urlencode($_REQUEST[$_key]);
		$$_key = str_front($_REQUEST[$_key]);
		file_put_contents("./log/debug/api_".date("Ymd").".log", $_key.":".$$_key."\r\n", FILE_APPEND | LOCK_EX);
		//echo $_key.":".$$_key."\r\n";
	}
	file_put_contents("./log/debug/api_".date("Ymd").".log", "-------------------------\r\n", FILE_APPEND | LOCK_EX);
	
	$msgId = ($_POST['msgId']) ? $_POST['msgId'] : '';
	
	if($action == 'chat' || $action == 'img') {
		$msgTime = date('Y-m-d H:i:s');
		$sql = "Insert into web_lineChatlog (messageId, type, log, uname, date) values ('$msgId', '$content_type', '$message', '$from', '$msgTime') ";
		$rs = ConnectDB($DB, $sql);
	}	
	
	switch($content_type) {
        
        case "text" :
            $content_type = "文字訊息";
            $messages = array(
                array(
                    'type' => 'text',
                    'text' => $message,
                ),
            );    
            break;
			
		case "images" :
            $content_type = "圖片訊息";
            $messages = array(
                array(
                    'type' => 'image',
                    'originalContentUrl' => "https://".$_SERVER['HTTP_HOST']."/".substr($message, 2),
					'previewImageUrl' => "https://".$_SERVER['HTTP_HOST']."/".substr($message, 2),
                ),
            );   		
            break;	 

        case "sticker" :
            $content_type = "貼圖訊息";
            $messages = array(
                        
                array(
                    'type' => 'sticker',
                    'packageId' => '1',
                    'stickerId' => '2',
                ),
            );      
            break;  

        case "image" :
            $content_type = "圖片訊息";
            $message = getObjContent("jpeg");   // 讀取圖片內容
            $data = ["to" => $from, "messages" => array(["type" => "image", "originalContentUrl" => $message, "previewImageUrl" => $message])];
            break;
            
        case "video" :
            $content_type = "影片訊息";
            $message = getObjContent("mp4");   // 讀取影片內容
            $data = ["to" => $from, "messages" => array(["type" => "video", "originalContentUrl" => $message, "previewImageUrl" => $message])];
            break;
            
        case "audio" :
            $content_type = "語音訊息";
            $message = getObjContent("mp3");   // 讀取聲音內容
            $data = ["to" => $from, "messages" => array(["type" => "audio", "originalContentUrl" => $message[0], "duration" => $message[1]])];
            break;
            
        case "location" :
            $content_type = "位置訊息";
            $title = $receive->events[0]->message->title;
            $address = $receive->events[0]->message->address;
            $latitude = $receive->events[0]->message->latitude;
            $longitude = $receive->events[0]->message->longitude;
            $data = ["to" => $from, "messages" => array(["type" => "location", "title" => $title, "address" => $address, "latitude" => $latitude, "longitude" => $longitude])];
            break;
            
        default:
            $content_type = "未知訊息";
            break;
    }
	
	if(count($from) >= 2) {
		$request = multicast($content_type, $messages);
	} else {
		//$request = push($content_type, $messages);
		if($reToken) {
			$request = reply($content_type, $messages);
			$requestDecode = json_decode($request, true);
			if($requestDecode['message']) {
				$request = push($content_type, $messages);
			}
		} else {
			$request = push($content_type, $messages);
		}	
	}	
