<?php
    include_once("./control/includes/function.php");
	ini_set('session.cookie_httponly', 1 );
	session_start();

	if (empty($_SESSION['expire']) || $_SESSION['expire'] < time()) $_SESSION['token'] = md5('aibi' . uniqid(microtime())); // create token (fast/sufficient) 
	$_SESSION['expire'] = time() + 900; // make session valid for next 15 mins
	$_SESSION['visitid'] = $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT'];
	
	$agent = $_SERVER['HTTP_USER_AGENT'];
	if (!preg_match("#\bLine\b#", $agent)) {
		//die('aibi');
	}
	$web_member_id = $_REQUEST['web_member_id'];
	if(!$web_member_id) {
		die('aibi');
	}
	
	$sql = "
		SELECT 
			a.*
		FROM 
			web_about as a		
		WHERE 
			a.ifShow = '1'
		AND
			a.web_about_id = '1'
	";
	//echo $sql;
    //$rs = ConnectDB($DB, $sql);
	$pdo = $pdoDB->prepare($sql);
    $pdo->execute();
    $termsRow = $pdo->fetch(PDO::FETCH_ASSOC);
	
	$sql = "
		SELECT 
			a.*
		FROM 
			web_about as a		
		WHERE 
			a.ifShow = '1'
		AND
			a.web_about_id = '6'
	";
	//echo $sql;
    //$rs = ConnectDB($DB, $sql);
	$pdo = $pdoDB->prepare($sql);
    $pdo->execute();
    $privacyPolicyRow = $pdo->fetch(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $Init_WebTitle; ?> 會員專區</title>
	<script src="https://d.line-scdn.net/liff/1.0/sdk.js"></script>
	
    <script src="./js/jquery-1.10.2.min.js"></script>
	<script src="./js/jquery.validate.js" type="text/javascript"></script>
	<script src="./js/additional-methods.js"></script>
	<script>
		//init LIFF
        function initializeApp(data) {
            //取得QueryString
            let urlParams = new URLSearchParams(window.location.search);
            //顯示QueryString
            $('#QueryString').val(urlParams.toString());
            //顯示UserId
            $('.userid').val(data.context.userId);
			if(data.context.userId) {
				var userId = data.context.userId;
				$.ajax({ 
					url: "./action", 
					type: "POST",
					data: {action: 'userInfo', lineId: userId, token: '<?php echo $_SESSION['token']; ?>'}, 
					success: function(e){
						var obj = jQuery.parseJSON(e);
						/*
						var output = '';
						$.each(obj, function(k,v) {
							output += k + ': ' + v +'; ';
						});
						alert(output);
						*/
						if(obj.error == '0') {
							$('#username').attr({'readonly': 'readonly'}).val(obj.uname);
							if(obj.loginID) {
								$('input[name="ifOverseas"][value="'+obj.ifOverseas+'"]').trigger('click');
								$('input[name="ifOverseas"]').attr({'disabled': true});
								$('#loginID').attr({'readonly': 'readonly'}).val(obj.loginID);
							}	
							$('input[name="sex"][value="'+obj.sex+'"]').trigger('click');
							$('#memberBirthday').attr({'readonly': 'readonly'}).val(obj.birthday).blur();
							$('#mobile').val(obj.mobile);
							if(!obj.ifAccept) {
								$('.ifAccept').show();
							}
							if(!obj.ifUseSelf) {
								$('#ifUseSelf').attr({'checked': false});
							} else {
								$('#ifUseSelf').attr({'checked': true});
							}
						}
						
					}
				});	
			}
			
			liff.getProfile().then(
				profile=> {
					//顯示在text box中
					//$('#username').val(profile.displayName);
					//alert(profile.pictureUrl);
					$('.avatar').css("background-image","url("+profile.pictureUrl+")"); 
				}
			);
			
        }

        //ready
        $(function () {
            //init LIFF
            liff.init(function (data) {
                initializeApp(data);
				//alert(data.context.userId);
            });

            //ButtonGetProfile
            $('#ButtonGetProfile').click(function () {
                //取得User Proile
                liff.getProfile().then(
                    profile=> {
                        //顯示在text box中
                        $('#UserInfo').val(profile.displayName);
                        //居然可以alert
                        alert('done');
                    }
                );
            });

            //ButtonSendMsg
            $('#ButtonSendMsg').click(function () {
                liff.sendMessages([
                 {
                     type: 'text',
                     text: $('#msg').val()
                 }
                ])
               .then(() => {
                   alert('done');
               })
            });
        });
	</script>
    <link rel="stylesheet" href="css/app.css"/>
</head>
<body>
<div class="header">
    <h1>會員專區</h1> <a class="back" href="member.php"></a>
</div>
<div class="content register">
    <div class="avatar" style="background-image: url('')">
    </div>
	<form name="searchForm" id="searchForm" method="POST">
		<div class="row">
			<label>姓名</label>
			<input id="username" name="uname" value="陳建志" placeholder="請輸入姓名" required="">
		</div>
		<div class="row">
			<label>國籍</label></br></br>
			<input type="radio" id="r42" name="ifOverseas" value="0" checked>
			<label for="r42">本國</label>
			
			<input type="radio" id="r12" name="ifOverseas" value="1">
			<label for="r12">外國</label>
		</div>
		<div class="row">
			<label id="ifOverseasText">身份證字號</label>
			<input id="loginID" name="loginID" placeholder="請輸入身份證字號" required="" TWIDCheck="1">
		</div>
		<div class="row">
			<label>性別</label></br></br>
			<input type="radio" id="r32" name="sex" value="先生" checked>
			<label for="r32">先生</label>
			
			<input type="radio" id="r22" name="sex" value="小姐">
			<label for="r22">小姐</label>
		</div>
		<div class="row">
			<label>生日</label></br>
			<input type="text" id="memberBirthday" name="birthday" value="1987/05/27" placeholder="請輸入生日" min="1929-01-01" max="<?php echo date('Y-m-d', strtotime('-1 day')); ?>" data-role="datebox" data-options='{"mode": "datebox", "useNewStyle":true}' required="">
		</div>
		<div class="row">
			<label>手機</label>
			<input id="mobile" name="mobile" value="0922358666" placeholder="請輸入手機" isMobile="1" required="">
		</div>
		<div class="row ifUseSelf" style="display:;">
			<input type="checkbox" id="ifUseSelf" name="ifUseSelf" value="1" style="width:15px;">
			限本人使用
		</div>
		<div class="row ifAccept" style="display:none;">
			<input type="checkbox" id="ifAccept" name="ifAccept" value="1" style="width:15px;" required="">
			我同意<a class="terms" style="cursor:pointer; text-decoration:underline;">會員條款</a>與<a class="privacy-policy" style="cursor:pointer; text-decoration:underline;">隱私權政策</a>，請打V
			<span id="span_mself"></span>
		</div>
		<input class="form-control" type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>" />
		<input class="form-control" type="hidden" name="web_member_id" value="<?php echo $web_member_id; ?>" />
		<input class="form-control" type="hidden" name="action" value="infoEditMember" />
		<button class="submit">確認</button>
	</form>	
</div>
<div class="popup_group">
	<div class="popup popup_alert" style="width:70%; height:70%; overflow-y: scroll;">
		<div class="header">
			<h1 style="font-weight:normal;">會員條款</h1> <a class="close" style="cursor:pointer;"></a>
		</div>
		<div class="content team terms" style="display:none;">
			<?php echo str_front($termsRow['content']); ?>
		</div>
		<div class="content team privacy-policy" style="display:none;">
			<?php echo str_front($privacyPolicyRow['content']); ?>
		</div>
	</div>
	<div class="popup popup_alert2">
		<!--<a class="close" href="#"></a>-->
		<h2>預約管理</h2>
		<!--<a href="#" class="btn submit disable">確認</a>-->
	</div>
</div>	
<script>
	$(function() {
		jQuery.validator.addMethod("string", function (value, element) {
			return this.optional(element) || /^[\u0391-\uFFE5\w]+$/.test(value);
		}, "不允許包含特殊符号!"); 
		jQuery.validator.addMethod("isMobile", function(value, element) {  
			var length = value.length;  
			var mobile = /^09[0-9]{8}$/;  
			return this.optional(element) || (length == 10 && mobile.test(value));  
		}, "請正確填寫手機號碼");  
		// 台灣身份證字號格式檢查程式
		jQuery.validator.addMethod("TWIDCheck", function(value, element, param)
		{
			var a = new Array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'X', 'Y', 'W', 'Z', 'I', 'O');
			var b = new Array(1, 9, 8, 7, 6, 5, 4, 3, 2, 1);
			var c = new Array(2);
			var d;
			var e;
			var f;
			var g = 0;
			var h = /^[a-z](1|2)\d{8}$/i;
			if (value.search(h) == -1)
			{
				return false;
			}
			else
			{
				d = value.charAt(0).toUpperCase();
				f = value.charAt(9);
			}
			for (var i = 0; i < 26; i++)
			{
				if (d == a[i])//a==a
				{
					e = i + 10; //10
					c[0] = Math.floor(e / 10); //1
					c[1] = e - (c[0] * 10); //10-(1*10)
					break;
				}
			}
			for (var i = 0; i < b.length; i++)
			{
				if (i < 2)
				{
					g += c[i] * b[i];
				}
				else
				{
					g += parseInt(value.charAt(i - 1)) * b[i];
				}
			}
			if ((g % 10) == f)
			{
				return true;
			}
			if ((10 - (g % 10)) != f)
			{
				return false;
			}
			return true;
		}, "請輸入有效的身份證字號!");
		$('#searchForm').validate({
			errorElement: 'p',
			submitHandler: function(form) {
				$.ajax({ 
					url: "./action", 
					type: "POST",
					data: $("#searchForm").serialize(), 
					success: function(e){
						var obj = jQuery.parseJSON(e);
						var output = '';
						$.each(obj, function(k,v) {
							output += k + ': ' + v +'; ';
						});
						//alert(output);
						if(obj.error != '0') {
							//alert(obj.message);
							$('.popup_group, .popup_alert').hide();
							$('.popup_group, .popup_alert2').fadeIn(400).find('h2').text(obj.message);
							setTimeout(function(){
								$('.popup_group .popup, .popup_group').fadeOut(400);
							},1000);
							return;
						} else if(obj.error == '0') {
							//alert('修改成功');
							$('.popup_group, .popup_alert').hide();
							$('.popup_group, .popup_alert2').fadeIn(400).find('h2').text('修改成功');
							setTimeout(function(){
								$('.popup_group .popup, .popup_group').fadeOut(400);
								location.href = 'member.php';
							},1000);
						}
					}
				});		
			},
			rules: {
				uname: {
					required: true,
					minlength: 2,
					remote: {
						url: '../chekChiness.php',
						type: "post",
						data: {
							type: 1,
							username: function() {
								return $('#username').val();
							}
						}
					}
				}
			},
			messages: {
				uname: {
					remote: '請輸入中文姓名'
				}
			},
			errorPlacement: function(error, element) {

				 if (element.attr("id") == "ifAccept") {//需要特殊处理的验证元素

					   error.insertAfter("#span_mself");  //控制显示位置

				 }

				 else{

					   error.insertAfter(element); //默认位置，可灵活改动

				 }

			}    
		});

		$('.submit').on('click', function(e) {
			e.preventDefault();
			$('#searchForm').submit();
			return false;	
			
		});
		
		$('.terms, .privacy-policy').on('click', function() {
			$('.content.team').hide();
			$('.popup_group, .popup_alert2').hide();
			$('.popup_group, .popup_alert').fadeIn(400).find('h1').text($(this).text());
			$('.'+$(this).attr('class')).show();
		});
		
		$('input[name="ifOverseas"]').on('click', function() {
			if($(this).val() == '1') {
				$('#ifOverseasText').text('護照號碼');
				$('input[name="loginID"]').attr({'placeholder': '請輸入護照號碼'});
				$('input[name="loginID"]').removeAttr('TWIDCheck')
			} else {
				$('#ifOverseasText').text('身份證字號');
				$('input[name="loginID"]').attr({'placeholder': '請輸入身份證字號', 'TWIDCheck': '1'});
			}
		});
		
		$('.popup_group, .popup_alert .close').on('click', function() {
			$('.popup_group .popup, .popup_group').fadeOut(400);
		});
		
		$('input[name="loginID"]').keyup(function(){			
			$(this).val($(this).val().toUpperCase());
		});
		
	})
</script>
</body>
</html>
