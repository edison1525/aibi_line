<?php
    include_once("./control/includes/function.php");
	ini_set('session.cookie_httponly', 1 );
	session_start();
	if (empty($_SESSION['expire']) || $_SESSION['expire'] < time()) $_SESSION['token'] = md5('aibi' . uniqid(microtime())); // create token (fast/sufficient) 
	$_SESSION['expire'] = time() + 900; // make session valid for next 15 mins
	$_SESSION['visitid'] = $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT'];
	
	$agent = $_SERVER['HTTP_USER_AGENT'];
	if (!preg_match("#\bLine\b#", $agent)) {
		//die('aibi');
	}
	$web_product_id = $_REQUEST['web_product_id'];
	$web_member_id = $_REQUEST['web_member_id'];
	if(!$web_member_id || !$web_product_id) {
		die('aibi');
	}
	
	$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM web_product where web_product.ifShow = :ifShow and web_product.web_x_product_id = :web_x_product_id and web_product.web_product_id = :web_product_id ORDER BY web_product.displayorder ASC";
	$excute = array(
		':ifShow'        			=> 1,
		':web_x_product_id'       	=> 1,
		':web_product_id'       	=> $web_product_id,
	);
	$pdo = $pdoDB->prepare($sql);
	$pdo->execute($excute);
	$row = $pdo->fetch(PDO::FETCH_ASSOC);
	
	$pic = ShowPic($row['Covers'], "./uploadfiles/l/", "./uploadfiles/no_image.jpg");
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $Init_WebTitle; ?> 會員專區-療程體驗券</title>
	<script src="./js/jquery-1.10.2.min.js"></script>
    <link rel="stylesheet" href="css/app.css"/>
</head>
<body>
<div class="header">
    <h1>購買療程體驗券</h1> <a class="back" href="coupon-buy.php?web_member_id=<?php echo $web_member_id; ?>"></a>
</div>
<div class="content coupon-info">
    <div class="row">
        <div class="title"><?php echo $row['subject']; ?></div>
        <img src="<?php echo $pic; ?>">
        <div class="desc">
            <div class="number">次數：<?php echo ($row['totalCount']); ?> 次</br>有效日期：<?php echo $row['useEdate'];?></div>
            <div class="price"><span style="color:black;"><S>定價：<?php echo number_format($row['price_cost']); ?> 元</S></span></br>特價：<?php echo number_format($row['price_member']); ?> 元</div>
        </div>
        <div class="btns">
            <a href="#" class="buy-btn" data-prodid="<?php echo $row['web_product_id']; ?>" data-pricemember="<?php echo number_format($row['price_member']); ?>" data-pricecost="<?php echo number_format($row['price_cost']); ?>"data-totalcount="<?php echo ($row['totalCount']); ?>">購買</a>
        </div>
    </div>
    <div class="row">
        <div class="title">療程介紹</div>
        <div class="content">
            <?php echo $row['content']; ?>
        </div>
    </div>
</div>
<div class="popup_group">
    <div class="popup popup_stored_confirm">
        <h2>確認購買？</h2>
        <div class="popup_content">
			<form name="searchForm" id="searchForm" method="POST">
				<div class="text">
					現金$<span id="cash">6,000</span>，可得<span id="totalCount">4</span>次使用券
				</div>
				<div class="btns">
					<a href="#" class="cancel">取消</a>
					<a href="#" class="submit">確認</a>
				</div>
				<input class="form-control" type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>" />
				<input class="form-control" type="hidden" name="web_member_id" value="<?php echo $web_member_id; ?>" />
				<input class="form-control" type="hidden" name="web_product_id" />
				<input class="form-control" type="hidden" name="store_id" value="<?php echo $_GET['store_id']; ?>" />
				<input class="form-control" type="hidden" name="action" value="addOrder" />
			</form>	
        </div>
    </div>
    <div class="popup popup_stored_complete">
        <h2>已送出</h2>
        <div class="popup_content">
            <div class="text">
                請店家確認付款事宜
            </div>
            <div class="btns">
                <a href="#" class="submit">關閉</a>
            </div>
        </div>
    </div>
</div>
<script>
	$(function () {
		$('.buy-btn').click(function () {
			var prodid = ($(this).attr('data-prodid')) ? $(this).attr('data-prodid') : 0;
			var price_member = ($(this).attr('data-pricemember')) ? $(this).attr('data-pricemember') : 0;
			var price_cost = ($(this).attr('data-pricecost')) ? $(this).attr('data-pricecost') : 0;
			var totalCount = ($(this).attr('data-totalcount')) ? $(this).attr('data-totalcount') : 0;
			
			$('#cash').text(price_member);
			$('#totalCount').text(totalCount);
			$('input[name="web_product_id"]').val(prodid);
			$('.popup_group, .popup_stored_confirm').fadeIn(400);
		});
		$('.popup_stored_confirm .submit').click(function () {
			if($('input[name="web_product_id"]').val() && $('input[name="web_member_id"]').val() && $('input[name="store_id"]').val()) {
				$.ajax({ 
					url: "./action", 
					type: "POST",
					data: $("#searchForm").serialize(), 
					success: function(e){
						var obj = jQuery.parseJSON(e);
						var output = '';
						$.each(obj, function(k,v) {
							output += k + ': ' + v +'; ';
						});
						//alert(output);
						if(obj.error != '0') {
							alert(obj.message);
							return;
						} else if(obj.error == '0') {
							$('.popup_stored_complete').show();
						}
					}
				});
				$('.popup_stored_confirm').hide();
			} else {
				alert('操作錯誤');
			}
		});
		$('.popup_stored_confirm .cancel, .popup_stored_complete .submit').click(function () {
			$('.popup_group .popup, .popup_group').fadeOut(400);
		});
	})
</script>
</body>
</html>
