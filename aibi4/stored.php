<?php
    include_once("./control/includes/function.php");
	ini_set('session.cookie_httponly', 1 );
	session_start();
	if (empty($_SESSION['expire']) || $_SESSION['expire'] < time()) $_SESSION['token'] = md5('aibi' . uniqid(microtime())); // create token (fast/sufficient) 
	$_SESSION['expire'] = time() + 900; // make session valid for next 15 mins
	$_SESSION['visitid'] = $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT'];
	
	$agent = $_SERVER['HTTP_USER_AGENT'];
	if (!preg_match("#\bLine\b#", $agent)) {
		//die('aibi');
	}
	$web_member_id = $_REQUEST['web_member_id'];
	if(!$web_member_id) {
		die('aibi');
	}
	
	$sql = "
		Select 
			SQL_CALC_FOUND_ROWS a.*,
			b.subject as bsubject,
			b.web_product_id as bproductId,
			b.price as bprice,
			b.num as bnum,
			b.dimension as bdimension,
			c.subject as csubject,
			c.price_cost as cprice_cost,
			c.price_member as cprice_member,
			c.totalCount as ctotalCount,
			d.web_xx_product_id as dweb_xx_product_id,
			d.subject as dsubject,
			e.subject as esubject,
			(select subject from web_x_class where web_x_class.web_x_class_id = a.store_id) as xClassSubject
		From 
			web_x_order a
		Left Join 
			web_order b ON b.web_x_order_ordernum = a.ordernum
		Left Join 
			web_product c ON c.web_product_id = b.web_product_id
		Left Join 
			web_x_product d ON d.web_x_product_id = c.web_x_product_id
		Left Join 
			web_xx_product e ON e.web_xx_product_id = d.web_xx_product_id
		Where 
			a.web_member_id = '".$web_member_id."' 
		AND
			a.states = '訂單成立'
		AND
			a.paymentstatus = '付款成功'
		AND
			d.web_xx_product_id IN (2,3)
		Group by
			a.web_x_order_id
		order by 
			a.web_x_order_id desc 
	";
	$pdo = $pdoDB->prepare($sql);
	$pdo->execute();
	$orderRow = $pdo->fetchAll(PDO::FETCH_ASSOC);
	
	
	$sql = "
		Select 
			SUM(b.dimension) as sum
		From 
			web_x_order a
		Left Join 
			web_order b ON b.web_x_order_ordernum = a.ordernum
		Left Join 
			web_product c ON c.web_product_id = b.web_product_id
		Left Join 
			web_x_product d ON d.web_x_product_id = c.web_x_product_id
		Left Join 
			web_xx_product e ON e.web_xx_product_id = d.web_xx_product_id	
		Where 
			a.web_member_id = '".$web_member_id."' 
		AND
			a.states = '訂單成立'
		AND
			a.paymentstatus = '付款成功'
		AND
			d.web_xx_product_id IN (2)
		order by 
			a.web_x_order_id desc 
	";
	$pdo = $pdoDB->prepare($sql);
	$pdo->execute();
	$storedTotal = $pdo->fetch(PDO::FETCH_ASSOC);
	
	$sql = "
		Select 
			a.ordernum,
			a.total,
			b.price,
			a.web_birthday_money,
			a.all_discount
		From 
			web_x_order a
		Left Join 
			web_order b ON b.web_x_order_ordernum = a.ordernum
		Left Join 
			web_product c ON c.web_product_id = b.web_product_id
		Left Join 
			web_x_product d ON d.web_x_product_id = c.web_x_product_id
		Left Join 
			web_xx_product e ON e.web_xx_product_id = d.web_xx_product_id	
		Where 
			a.web_member_id = '".$web_member_id."' 
		AND
			a.states = '訂單成立'
		AND
			a.paymentstatus = '付款成功'
		AND
			d.web_xx_product_id IN (3)
		order by 
			a.web_x_order_id desc 
	";
	$pdo = $pdoDB->prepare($sql);
	$pdo->execute();
	$useTotalRow = $pdo->fetchAll(PDO::FETCH_ASSOC);
	$useTotalArr = array();
	foreach($useTotalRow as $key => $useTotal) {
		$useTotalArr[$useTotal['ordernum']] = $useTotal['total'];
	}
	$balance = abs($storedTotal['sum'] - array_sum($useTotalArr));
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $Init_WebTitle; ?> 會員專區-儲值</title>
	<script src="./js/jquery-1.10.2.min.js"></script>
    <link rel="stylesheet" href="css/app.css"/>
</head>
<body>
<div class="header">
    <h1>會員專區-儲值</h1> <a class="back" href="member.php"></a>
</div>
<div class="content stored">
    <div class="info">儲值金 <?php echo number_format($balance); ?>元</div>
    <div class="title">儲值/使用 紀錄</div>
<?php
	foreach($orderRow as $key => $orderVal) {
		$orangeClass = ($orderVal['dweb_xx_product_id'] == '2') ? 'orange' : null;
		$typeText = ($orderVal['dweb_xx_product_id'] == '2') ? $orderVal['esubject'] : $orderVal['csubject'];
		$truePrice = ($orderVal['dweb_xx_product_id'] == '2') ? $orderVal['bdimension'] : $orderVal['total'];
		$paymentText = ($orderVal['payment'] == 'cash') ? ' 現金-付款' : ' 信用卡-付款';
?>	
    <div class="row">
        <div class="col-1"><?php echo date('Y/m/d', strtotime($orderVal['cdate'])); ?></div>
        <div class="col-2 <?php echo $orangeClass; ?>">
            <span>
				<?php 
					if($orderVal['dweb_xx_product_id'] == '2') {
						echo $typeText.$paymentText;
					} else {
						
						$sql = "
							Select 
								a.web_order_id,
								a.subject,
								a.price,
								a.web_product_id,
								c.subject as csubject
							From 
								web_order a
							Left Join
								web_product b
							On
								b.web_product_id = a.web_product_id
							Left Join
								web_x_product c
							On
								c.web_x_product_id = b.web_x_product_id	
							Where 
								a.web_x_order_ordernum = :web_x_order_ordernum
						";
						$excute = array(
							':web_x_order_ordernum'        => $orderVal['ordernum'],
						);
						$pdo = $pdoDB->prepare($sql);
						$pdo->execute($excute);
						$_orderRow = $pdo->fetchAll(PDO::FETCH_ASSOC);
						$prodSubjectAry = array();
						foreach($_orderRow as $_key => $_orderVal) {
							$prodSubjectAry[] = $_orderVal['csubject']."-".$_orderVal['subject']." ".$_orderVal['price'];
						}
						echo implode('</br>', $prodSubjectAry);
					}	
				?>
			</span>
            <span><?php echo number_format($truePrice); ?>元</span>
        </div>
        <div class="col-3">
            <span><?php echo $orderVal['xClassSubject']; ?></span>
            <span><?php echo $orderVal['editUser']; ?></span>
        </div>
    </div>
<?php
	}
?>	
    <button class="submit" onclick="location.href='stored-2.php?web_member_id=<?php echo $web_member_id; ?>'">儲值</button>
</div>
</body>
</html>
