<?php
	//session_start();
	//date_default_timezone_set("Asia/Taipei");
	include_once("./control/includes/function.php");
	require_once('function.php');
	require_once ('./control/class/Mustache/Autoloader.php');
	include_once ('./pusher/curl.php');
	
	/* 輸入申請的Line Developers 資料  */
	$channel_id = $Channel_ID;
	$channel_secret = $Channel_Secret;
	$channel_access_token = $Channel_Token;

	$myURL = $_SERVER['HTTP_X_FORWARDED_PROTO']."://".$_SERVER['HTTP_HOST']."/uploadfiles/l/";
	/*
	$sql = "Select lineID, loginID, web_member_id From web_member Where lineID != ''";
	$rs = ConnectDB($DB, $sql);
	for ($i=0; $i<mysql_num_rows($rs); $i++) {
		$row = mysql_fetch_assoc($rs);
		foreach($row as $_key=>$_value) $$_key = $row[$_key];
		
		$fromAry[$loginID] = $lineID;
		
		if(in_array($lineID, $fromAry) && !$loginID) {
			$sql23 = "DELETE FROM web_member WHERE web_member_id = '".$web_member_id."'";
			$rs23 = ConnectDB($DB, $sql23);
		} 
		//$fromAry[$loginID] = $lineID;
		
	}
	*/
	
	$fromAry = array_filter($fromAry);
	
	// 將收到的資料整理至變數
	$receive = json_decode(file_get_contents("php://input"));
	$receive2 = (file_get_contents("php://input"));
	//file_put_contents("debug.txt", file_get_contents("php://input")."\r\n",FILE_APPEND);
	
	// 讀取收到的訊息內容
	$receiveType = $receive->events[0]->type;
	//file_put_contents("./log/debug/debug_".date('Ymd').".txt", $receive2."\r\n",FILE_APPEND);
	/*
	$sql = "Insert into web_orderlog (log, date) values ('".json_encode($receive2)."', '".date('Y-m-d H:i:s')."') ";
	$rs = ConnectDB($DB, $sql);
	*/
	// 讀取訊息來源的類型 	[user, group, room]
	$type = $receive->events[0]->source->type;
	
	// 由於新版的Messaging Api可以讓Bot帳號加入多人聊天和群組當中
	// 所以在這裡先判斷訊息的來源
	if ($type == "room") {
		// 多人聊天 讀取房間id
		$from = $receive->events[0]->source->roomId;
		$reToken = $receive->events[0]->replyToken;
	} else if ($type == "group") {
		// 群組 讀取群組id
		$from = $receive->events[0]->source->groupId;
		$reToken = $receive->events[0]->replyToken;
	} else {
		// 一對一聊天 讀取使用者id
		$from = $receive->events[0]->source->userId;
		$reToken = $receive->events[0]->replyToken;
	}	

	$userAction = file_get_contents("./user/user_".$from.".txt", FILE_USE_INCLUDE_PATH);
	$userActionAry = explode('|', $userAction);
	$nowAction = $userActionAry[0];
	$chatStore = $userActionAry[1];
	$idType = IdCheck($from);
	//file_put_contents("./debug.txt", $idType."\r\n",FILE_APPEND);
	if($idType) {

		switch($idType) {
			case 0:
				$text = '新使用者';
			break;
			case 1:
				$text = '已註冊';
			break;
			case 2:
				$text = '管理者';
			break;
		}

		$messages = array(
			array(
				'type' => 'text',
				'text' => $text,
			),
		);
		$messages = ($text2) ? (array_merge($messages, $messages2)) : $messages;
		// 回覆訊息
		//$content_type = 1;
		//reply($content_type, $messages);
	}
	
	
	//$userAction = file_get_contents("./user/user_".$from.".txt", FILE_USE_INCLUDE_PATH);
	//$nowAction = $userAction;
	//$nowAction = null;
	
	//file_put_contents("./log/debug/debug2.txt", $receiveType."\r\n",FILE_APPEND);
	
	//文字訊息
	if($receiveType == 'message') {
		
		$text = $receive->events[0]->message->text;
		$userId = $receive->events[0]->source->userId;
		$content_type = $receive->events[0]->message->type;
		//file_put_contents("./log/debug/debug.txt", $content_type."\r\n",FILE_APPEND);
		if($text == '快速註冊') {
			$content_type = 'templateButtons';
			include_once("./confirmPush.php");
			return;
		} else if($text == '產品分類') {
			/*
			if($idType != '2') {
				//return;
			}	
			$content_type = 'templateProductAdd';
			include_once("./confirmPush.php");
			return;
			*/
		} else if($text == '預約服務') {
			$messages = $text;
			booking_menu('text', $messages, 1);
			$nowAction = 'menu';
			file_put_contents("./user/user_".$from.".txt", $nowAction);
		} else if($text == 'IG/FB') {
			$messages = $text;
			booking_menu('text', $messages, 2);
			$nowAction = 'menu';
			file_put_contents("./user/user_".$from.".txt", $nowAction);	
		} else if($text == '作品集') {
			$messages = $text;
			booking_menu('text', $messages, 3);
			$nowAction = 'menu';
			file_put_contents("./user/user_".$from.".txt", $nowAction);	
		} else if($text == '我要預約') {
			$messages = $text;
			//有各店
			reservationCategoryNew('text', $messages, 1);
			$nowAction = 'menu';
			file_put_contents("./user/user_".$from.".txt", $nowAction);
			//先第一間店
			//reservation('text', $messages, 1, 1);
			//reservationCategory('text', $messages, 1);
			//reservationCategoryAll('text', $messages, 1);
			return;
		/*	
		} else if($text == '今日叫號') {
			$messages = $text;
			//reservationCategory('text', $messages, 1);
			reservationCategoryAllCall('text', $messages, 1);
			$nowAction = 'menu';
			file_put_contents("./user/user_".$from.".txt", $nowAction);
			return;
		} else if($text == '主治項目') {
			$messages = $text;
			reservationCategory2('text', $messages, 1);
			$nowAction = 'menu';
			file_put_contents("./user/user_".$from.".txt", $nowAction);
			return;	
		*/	
		} else if($text == '預約查詢') {
			$messages = $text;
			register_search('text', $messages, 1, $userId);
			$nowAction = 'menu';
			file_put_contents("./user/user_".$from.".txt", $nowAction);
			return;	
		} else if($text == '各館諮詢') {
			$messages = $text;
			reservationCategoryNewStore('text', $messages, 1);
			$nowAction = 'menu';
			file_put_contents("./user/user_".$from.".txt", $nowAction);
			return;
		} else if($text == '一對一') {
			$content_type = "text";
			$messages = array(
				array(
					'type' => 'text',
					'text' => '請提出你的問題，稍後由專人為你解答!!',
				),
			);
			reply($content_type, $messages);
			$nowAction = 'chat';
			file_put_contents("./user/user_".$from.".txt", $nowAction);
			return;
		}
		/*
		if($text == '上傳產品成功') {
			
			$messages = $text;
			reply2('text', $messages, 1);
			return;
		}
		*/
	} else if($receiveType == 'postback') {
		
		
		$text = $receive->events[0]->postback->data;
		
		$passRow1 = array(
			'reservationDate',
			'reservationTime',
			'reservationDoctor',
			'register',
			'checkPerson',
			'registerSearch',
			'registerCancel',
		);
		
		if(!in_array($text, $passRow1)) {
			//if($text == 'action=more&page=2') {
			if(preg_match("/\more\b/i", $text)) {
				
				parse_str($text, $output);
				if($output[page]) {
					$messages = $text;
					reply2('text', $messages, $output[page]);
					return;
				}	
			}
			
			if(preg_match("/\more2\b/i", $text)) {
				
				parse_str($text, $output);
				if($output[page]) {
					$messages = $text;
					reservation('text', $messages, $output[page], $output[cid]);
					return;
				}	
			}
			
			if(preg_match("/\more3\b/i", $text)) {
				
				parse_str($text, $output);
				if($output[page]) {
					$messages = $text;
					reply3('text', $messages, $output[page]);
					return;
				}	
			}
			
			if(preg_match("/\more4\b/i", $text)) {
				$userId = $receive->events[0]->source->userId;
				parse_str($text, $output);
				if($output[page]) {
					$messages = $text;
					//reply3('text', $messages, $output[page]);
					register_search_more('text', $messages, $output['page'], $output['date'], $userId);
					return;
				}	
			}
			
			if(preg_match("/\more5\b/i", $text)) {
				
				parse_str($text, $output);
				if($output[page]) {
					$messages = $text;
					//reply3('text', $messages, $output[page]);
					reservationCategoryAll('text', $messages, $output[page]);
					return;
				}	
			}
			
			if(preg_match("/\more6\b/i", $text)) {
				
				parse_str($text, $output);
				if($output[page]) {
					$messages = $text;
					//reply3('text', $messages, $output[page]);
					reservationCategoryNew('text', $messages, $output[page]);
					return;
				}	
			}
			
			if(preg_match("/\more7\b/i", $text)) {
				
				parse_str($text, $output);
				if($output[page]) {
					$messages = $text;
					//reply3('text', $messages, $output[page]);
					reply3_3('text', $messages, $output[page], $output['class'], $output['team']);
					return;
				}	
			}
			
			if(preg_match("/\more8\b/i", $text)) {
				
				parse_str($text, $output);
				if($output[page]) {
					$messages = $text;
					//reply3('text', $messages, $output[page]);
					//reply3_3('text', $messages, $output[page], $output['class'], $output['team']);
					register_search('text', $messages, $output[page], $output['userId']);
					return;
				}	
			}
			
			if(preg_match("/reservationCategory/i", $text)) {
				//file_put_contents("./log/debug/debug2.txt", $text."\r\n",FILE_APPEND);
				parse_str($text, $output);
				if($output[cid] == 'all') {
					$messages = $text;
					reservation('text', $messages, 1);
					return;
				} else {
					$messages = $text;
					reservation('text', $messages, 1, $output[cid]);
					return;
				}	
			}
			
			if(preg_match("/reservation/i", $text)) {
				//file_put_contents("./log/debug/debug2.txt", $text."\r\n",FILE_APPEND);
				parse_str($text, $output);
				if($output[people]) {
					$messages = $text;
					//reservation('text', $messages, 1);
					//return;

				}	
			}
			
			if(preg_match("/chat/i", $text)) {
				//file_put_contents("./log/debug/debug2.txt", $text."\r\n",FILE_APPEND);
				parse_str($text, $output);
				if($output['cid']) {
					$content_type = "images";
					$messages = array(
						array(
							'type' => 'text',
							'text' => '請提出你的問題，稍後由專人為你解答!!'.$output['cid'],
						),
					);
					chatPic('text', $messages, 1);
					$nowAction = 'chat|'.$output['cid'];
					file_put_contents("./user/user_".$from.".txt", $nowAction);
					return;

				}	
			}
			
			$idArray = explode('&',$text);
			foreach ($idArray as $index => $avPair){
				list($ignore, $value) = explode("=", $avPair);
				$id[$ignore] = $value;
			}
			//file_put_contents("./log/debug/debug2.txt", $id['action']."\r\n",FILE_APPEND);
			if(count($id)) {
				switch($id['action']) {
					case "register":
						switch($id['item']) {
							case "profile":
								$content_type = "text";
								$profileJson = profile($from);
								$profileAry = json_decode($profileJson, true);
								$text = "LINE個人資訊\r\n".$profileAry[displayName]."\r\n<img src='".$profileAry[pictureUrl]."' />";	
								$nowAction = "profile";
								break;
								
							case "email":
								$content_type = "text";
								$text = "請輸入正確email";	
								$nowAction = "email";
								break;
							
							case "uname":
								$content_type = "text";
								$text = "請輸入正確姓名";
								$nowAction = "uname";							
								break;
							
							case "sex":
								$content_type = "text";
								$text = "請輸入正確性別";      
								$nowAction = "sex";							
								break;
								
							case "mobile":
								$content_type = "text";
								$text = "請輸入手機號碼";
								$nowAction = "mobile";							
								break;	
							
						}
						
						break;
					
					case "add":	
						switch($id['item']) {
							case "productName":
								$content_type = "text";
								$text = "請輸入產品名稱";	
								$nowAction = "productName";
								break;

							case "productNum":
								$content_type = "text";
								$text = "請輸入產品數量";	
								$nowAction = "productNum";
								break;

							case "productPrice":
								$content_type = "text";
								$text = "請輸入產品售價";	
								$nowAction = "productPrice";
								break;	
									
							case "productImage":
								$content_type = "text";
								$text = "請上傳產品圖";	
								$nowAction = "productImage";
								break;

							case "confirm":
								$content_type = "text";
								$text = "確定新增";      
								$nowAction = "confirm";							
								break;	
							
						}

						break;
					
					case "reservation":	
						//file_put_contents("./log/debug/debug2.txt", $id['item']."\r\n",FILE_APPEND);
						switch($id['item']) {
							case "reservationDoctor":
								$content_type = 'text';
								$messages = $text;
								reply3_2($content_type, $messages, 1, $id['prodid']);
								return;
								break;
							
							case "register":
								$content_type = 'text';
								$messages = $text;
								
								reply3_3($content_type, $messages, 1, $id['class'], $id['team']);
								
								file_put_contents("./debug.txt", $id['class']."|".$id['team']."\r\n",FILE_APPEND);
								//reply3_2($content_type, $messages, 1, $id['class']);
								return;
								break;

							case "checkPerson":
								$content_type = 'text';
								$messages = $text;
								checkPerson($content_type, $messages, 1, $id['shiftId'], $id['date'], $id['teamSubject'], $id['classSubject'], $id['timeId'], $id['teamXid']);
								//reply3_2($content_type, $messages, 1, $id['class']);
								return;
								break;	
								
							case "registerSearch":
								$content_type = 'text';
								$messages = $text;
								registerSerachById($content_type, $messages, 1, $id['Id']);
								//reply3_2($content_type, $messages, 1, $id['class']);
								return;
								break;	
								
							case "registerCancel":
								$content_type = 'text';
								if(!$id['data']) {
									$messages2 = array(
								
										array(
											'type' => 'template',
											'altText' => '線上掛號',
											'template' => array(
												'type' => 'confirm',
												'text' => "取消確認：\n小提醒：\n取消次數超過5次\n無法使用線上預約服務\n如要預約，請到門市現場預約",
												'actions' => array(
													array(
														"type" => "message",
														"label" => "不小心按錯",
														"text" => "預約查詢",
													),
													array(
														"type" => "postback",
														"label" => "確認取消",
														"data" => "action=reservation&item=registerCancel&Id=".$id['Id']."&data=y",
													)
												),
											),
										),
											
									);
									
									$prodAry[0]['type'] = 'bubble';
									$prodAry[0]['body']['type'] = 'box';
									$prodAry[0]['body']['layout'] = 'vertical';
									$prodAry[0]['body']['spacing'] = 'sm';
									$prodAry[0]['body']['contents'][0]['type'] = 'text';
									$prodAry[0]['body']['contents'][0]['text'] = '取消確認：';
									$prodAry[0]['body']['contents'][0]['wrap'] = true;
									$prodAry[0]['body']['contents'][0]['weight'] = "bold";
									$prodAry[0]['body']['contents'][0]['size'] = "xl";
									$prodAry[0]['body']['contents'][0]['color'] = '#905c44';
									
									$prodAry[0]['body']['contents'][1]['type'] = 'separator';
									$prodAry[0]['body']['contents'][1]['margin'] = 'lg';
									
									$prodAry[0]['body']['contents'][2]['type'] = 'text';
									$prodAry[0]['body']['contents'][2]['text'] = '小提醒：';
									$prodAry[0]['body']['contents'][2]['wrap'] = true;
									$prodAry[0]['body']['contents'][2]['weight'] = "bold";
									$prodAry[0]['body']['contents'][2]['size'] = "sm";
									$prodAry[0]['body']['contents'][2]['color'] = '#000000';
									
									$prodAry[0]['body']['contents'][3]['type'] = 'text';
									$prodAry[0]['body']['contents'][3]['text'] = '取消次數超過5次';
									$prodAry[0]['body']['contents'][3]['wrap'] = true;
									$prodAry[0]['body']['contents'][3]['weight'] = "bold";
									$prodAry[0]['body']['contents'][3]['size'] = "sm";
									$prodAry[0]['body']['contents'][3]['color'] = '#000000';
									
									$prodAry[0]['body']['contents'][4]['type'] = 'text';
									$prodAry[0]['body']['contents'][4]['text'] = '無法使用線上預約服務';
									$prodAry[0]['body']['contents'][4]['wrap'] = true;
									$prodAry[0]['body']['contents'][4]['weight'] = "bold";
									$prodAry[0]['body']['contents'][4]['size'] = "sm";
									$prodAry[0]['body']['contents'][4]['color'] = '#000000';
									
									$prodAry[0]['body']['contents'][5]['type'] = 'text';
									$prodAry[0]['body']['contents'][5]['text'] = '如要預約，請到門市現場預約';
									$prodAry[0]['body']['contents'][5]['wrap'] = true;
									$prodAry[0]['body']['contents'][5]['weight'] = "bold";
									$prodAry[0]['body']['contents'][5]['size'] = "sm";
									$prodAry[0]['body']['contents'][5]['color'] = '#000000';
									
									$prodAry[0]['body']['contents'][6]['type'] = 'box';
									$prodAry[0]['body']['contents'][6]['layout'] = 'horizontal';
									$prodAry[0]['body']['contents'][6]['spacing'] = 'sm';
									$prodAry[0]['body']['contents'][6]['margin'] = 'lg';
									
									$prodAry[0]['body']['contents'][6]['contents'][0]['type'] = 'button';
									$prodAry[0]['body']['contents'][6]['contents'][0]['style'] = 'primary';
									$prodAry[0]['body']['contents'][6]['contents'][0]['color'] = '#665750';
									$prodAry[0]['body']['contents'][6]['contents'][0]['action']['type'] = 'message';
									$prodAry[0]['body']['contents'][6]['contents'][0]['action']['label'] = '不小心按錯';
									$prodAry[0]['body']['contents'][6]['contents'][0]['action']['text'] = "預約查詢";
									
									$prodAry[0]['body']['contents'][6]['contents'][1]['type'] = 'button';
									$prodAry[0]['body']['contents'][6]['contents'][1]['style'] = 'primary';
									$prodAry[0]['body']['contents'][6]['contents'][1]['color'] = '#905c44';
									$prodAry[0]['body']['contents'][6]['contents'][1]['action']['type'] = 'postback';
									$prodAry[0]['body']['contents'][6]['contents'][1]['action']['label'] = '確認取消';
									$prodAry[0]['body']['contents'][6]['contents'][1]['action']['data'] = "action=reservation&item=registerCancel&Id=".$id['Id']."&data=y";

									$prodAry[0]['footer']['type'] = 'box';
									$prodAry[0]['footer']['layout'] = 'vertical';
									$prodAry[0]['footer']['spacing'] = 'sm';
									$prodAry[0]['footer']['contents'][0]['type'] = 'text';
									$prodAry[0]['footer']['contents'][0]['text'] = "Design by aibitechcology";
									$prodAry[0]['footer']['contents'][0]['wrap'] = true;
									$prodAry[0]['footer']['contents'][0]['color'] = "#aaaaaa";
									$prodAry[0]['footer']['contents'][0]['size'] = "xxs";
									$prodAry[0]['footer']['contents'][0]['align'] = "end";
									
									
									$data = $prodAry;
				
									$result = array();
									foreach($data as $key => $row) {
										//if (mb_strpos($message, $row[title]) !== false) {
											array_push($result, $row);
										//}
									}
									
									$result = (!$result[0]) ? $data : $result;
									$messages2 = 
										array(
											
											array(
												'type' => 'flex',
												'altText' => '線上掛號',
												'contents' => array(
													'type' => 'carousel',
													'contents' => $result
												),
											),
										);

									
									reply($content_type, $messages2);
								
									return;
								} else {
									if($id['data'] == 'y') {
										registerCancel($content_type, $messages, 1, $id['Id']);	
									} else {
										return;
									}
								}	
								break;	
							
							case "reservationDate":
								$content_type = 'templateConfirmDatePicker';
								include_once("./confirmPush.php");
								$nowAction = "reservationDate";
								file_put_contents("./user/user_".$from.".txt", $nowAction);
								$_SESSION[$from][uuid] = uniqid(20);
								$_SESSION[$from][from] = $from;
								$_SESSION[$from][cdate] = time();
								$_SESSION[$from][info] = $text;
								$_SESSION[$from][prodid] = $id['prodid'];
								$_SESSION[$from][csubject] = $id['csubject'];
								$_SESSION[$from][subject] = $id['subject'];
								$_SESSION[$from][people] = $id['people'];
								file_put_contents("./log/debug/cache_".$from."_".date('Y-m-d').".log", serialize($_SESSION)."#$@%^",FILE_APPEND);
								return;	
								break;
								
							case "reservationTime":
								$content_type = 'templateConfirmTimePicker';
								include_once("./confirmPush.php");
								$nowAction = "reservationTime";
								file_put_contents("./user/user_".$from.".txt", $nowAction);
								return;	
								break;	

							case "confirm":
								$content_type = "text";
								$text = "確定新增";      
								$nowAction = "confirm";		
								//file_put_contents("./log/debug/debug2.txt", $id['data']."\r\n",FILE_APPEND);
								
								$file_path = "./log/debug/cache_".$from."_".date('Y-m-d').".log";
								$_SESSION[$from] = readCacheLog($file_path);
								$reqAry = readCacheLogAll($file_path);
								
								//驗証是否有重覆
								$file_path = "./log/debug/reservation.log";
								
								$reqAry2 = readCacheLogById($file_path, $from);
								$NewArray1 = arrayRecursiveDiff($reqAry, $reqAry2);
								if(!$NewArray1) {
									file_put_contents($file_path, serialize($_SESSION)."#$@%^",FILE_APPEND);
									$text = "感謝您透過線上預約課程!\r\n您預約：【".$_SESSION[$from][csubject]."】- ".$_SESSION[$from][subject]."\r\n詳情如下：\r\n".$_SESSION[$from][people]." | ".$_SESSION[$from][reservationDate]." | ".$_SESSION[$from][reservationTime]."\r\n服務系統已接受預約，並於當天預留時段\r\n歡迎您來店。\r\n\r\n*當天預約課程前2小時，服務系統會貼心小提醒，主動發訊息給您，知會此次預約課程。";
								} else {
									$text = "此為已預約訊息\r\n您預約：【".$_SESSION[$from][csubject]."】- ".$_SESSION[$from][subject]."\r\n詳情如下：\r\n".$_SESSION[$from][people]." | ".$_SESSION[$from][reservationDate]." | ".$_SESSION[$from][reservationTime];
								}
								unset($_SESSION[$from]);
								break;	
							
						}

						break;	
					
					default:
					
						break;
				}
				
				
				
				
				file_put_contents("./user/user_".$from.".txt", $nowAction);
				//$_SESSION[$from][nowAction] = ($nowAction) ? $nowAction : null;
			}
		} else {
			
			//file_put_contents("./log/debug/debug2.txt", $text."\r\n",FILE_APPEND);
			
			switch($text) {
				
				case "reservationDoctor":
					$content_type = 'text';
					$messages = $text;
					reply3($content_type, $messages, 1);
					return;
				break;
				
				case "register":
					$content_type = 'text';
					$messages = $text;
					reply3($content_type, $messages, 1);
					return;
				break;
				
				case "reservationDate":
					if (!$text) {	
						$text = "請輸入正確預約日期";
					} else {
						//$sql = "Update web_member set uname = '".$text."' Where lineID = '".$from."' ";
						//$rs = ConnectDB($DB, $sql);
						$date = $receive->events[0]->postback->params->date;
						//file_put_contents("./log/debug/debug2.txt", $date."\r\n",FILE_APPEND);
						$_text = $date;
						$text = "預約日期是 :".$_text;
						$nextAction = 'reservationTime';
						file_put_contents("./user/user_".$from.".txt", $nextAction);
						
						$file_path = "./log/debug/cache_".$from."_".date('Y-m-d').".log";
						$_SESSION[$from] = readCacheLog($file_path);
						$_SESSION[$from][reservationDate] = $_text;
						
						file_put_contents($file_path, serialize($_SESSION)."#$@%^", FILE_APPEND);
						
						$content_type = 'templateConfirmTimePicker';
						include_once("./confirmPush.php");
						return;
					}
				break;
				
				case "reservationTime":
					if (!$text) {	
						$text = "請輸入正確預約時間";
					} else {
						//$sql = "Update web_member set uname = '".$text."' Where lineID = '".$from."' ";
						//$rs = ConnectDB($DB, $sql);
						$time = $receive->events[0]->postback->params->time;
						//file_put_contents("./log/debug/debug2.txt", $time."\r\n",FILE_APPEND);
						$_text = $time;
						$text = "預約時間是 :".$_text;
						//$nextAction = 'reservationConfirm';
						//file_put_contents("./user/user_".$from.".txt", $nextAction);
						
						$file_path = "./log/debug/cache_".$from."_".date('Y-m-d').".log";
						$_SESSION[$from] = readCacheLog($file_path);
						$_SESSION[$from][reservationTime] = $_text;
						
						file_put_contents($file_path, serialize($_SESSION)."#$@%^");
						//$text = null;
						$text = "此為已預約訊息\r\n您預約：【".$_SESSION[$from][csubject]."】- ".$_SESSION[$from][subject]."\r\n詳情如下：\r\n".$_SESSION[$from][people]." | ".$_SESSION[$from][reservationDate]." | ".$_SESSION[$from][reservationTime];
						
						$content_type = "text";
						//$text2 = "確定預約";
						
						$messages2 = array(
							
							array(
								'type' => 'template',
								'altText' => 'LINE@',
								'template' => array(
									'type' => 'confirm',
									'text' => $text,
									'actions' => array(
										array(
											"type" => "postback",
											"label" => "確認預約",
											"data" => "action=reservation&item=confirm&data=y",
										),
										array(
											"type" => "postback",
											"label" => "取約預約",
											"data" => "action=reservation&item=confirm&data=n",
										)
									),
								),
							),
								
						);
						
						reply($content_type, $messages2);
						return;
					}
				break;
				
				default:
				break;
					
			}		
		}
		
		
	}	

	//file_put_contents("receive.txt", $receive2."\r\n",FILE_APPEND);
	//file_put_contents("receive.txt", date('Y-m-d H:i:s')."\r\n",FILE_APPEND);
	
	
	if(!in_array($from, $fromAry)) {
		$content_type = "text";
		//$text = "您好，謝謝您的加入:現在時間為".date('Y/m/d H:i:s');
		//push($content_type, $text);
		if($from) {
			//$sql = "Insert into web_member (lineID) values ('$from') ";
			//$rs = ConnectDB($DB, $sql);
		}	
		
		//file_put_contents("userId.txt", $from.",",FILE_APPEND);
	}	
	
	// 讀取訊息的型態 [Text, Image, Video, Audio, Location, Sticker]
	$content_type = $receive->events[0]->message->type;
	
	// 準備Post回Line伺服器的資料 
	$header = ["Content-Type: application/json", "Authorization: Bearer {" . $channel_access_token . "}"];
	
	$textPassAry = array(
		'新品活動',
		'醫師團隊',
		'menu',
		'menu off',
		'admin',
		'line id'
		/*
		'A1',
		'A2',
		'A3',
		'A5',
		'A6',
		'A7',
		'A9',
		'AT1',
		'AT3',
		*/
	);
	if($receiveType != 'postback') { 
		if(!in_array($text, $textPassAry)) {
			$nowActionLog = file_get_contents("./user/user_".$from.".txt", FILE_USE_INCLUDE_PATH);
			if($nowActionLog) {
				switch($nowActionLog) {
					case "profile":
							$text = $from;
							$nextAction = 'email';
						break;
					case "email":
						if (!preg_match("/^[a-zA-Z0-9_-]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/", $text)) {
							$text = "請輸入正確email";
						} else {
							$sql = "SELECT web_member_id, uname, sex, mobile, lineID FROM web_member WHERE loginID like '".$text."'";
							$rs = ConnectDB($DB, $sql);
							if (mysql_num_rows($rs) != 0) {
								$lineID = mysql_result($rs, 0, "lineID");
								$web_member_id = mysql_result($rs, 0, "web_member_id");
								$uname = mysql_result($rs, 0, "uname");
								$sex = mysql_result($rs, 0, "sex");
								$mobile = mysql_result($rs, 0, "mobile");
								
								if($lineID) {
									if($uname && $sex && $mobile) {
										$text = "此帳號已註冊過";						
										$nextAction = 'Doned';
										file_put_contents("./user/user_".$from.".txt", $nextAction);
									} else {
										$text = "您的Email是 :".$text;
										$nextAction = 'uname';
										file_put_contents("./user/user_".$from.".txt", $nextAction);
									}
									
									
								} else {
									
									$sql = "Update web_member set lineID = '".$from."' Where web_member_id = '".$web_member_id."' ";
									$rs = ConnectDB($DB, $sql);
									$text = "您的Email是 :".$text;
									if($uname && $sex && $mobile) {
										$nextAction = 'Done';
										file_put_contents("./user/user_".$from.".txt", $nextAction);
									} else {
										$nextAction = 'uname';
										file_put_contents("./user/user_".$from.".txt", $nextAction);
									}	
									
								}

								
							} else {
								
								$sql = "SELECT web_member_id, nickname, wishlist, lineID FROM web_member WHERE lineID like '".$from."' and loginID != ''";
								$rs = ConnectDB($DB, $sql);
								if (mysql_num_rows($rs) == 0) {
									if(!$text) {
										continue;
									}
									$cdate = date('Y-m-d H:i:s');
									$sql = "Insert into web_member (loginID, lineID, email, cdate) values ('$text', '$from', '$text', '$cdate') ";
									//file_put_contents("debug.txt", $sql."\r\n",FILE_APPEND);
									$rs = ConnectDB($DB, $sql);
									$text = "您的Email是 :".$text;
									
									$nextAction = 'uname';
									file_put_contents("./user/user_".$from.".txt", $nextAction);
									
								} else {
									$text = "您的LINE識別碼已註冊過";
									$nextAction = 'Doned';
									file_put_contents("./user/user_".$from.".txt", $nextAction);
								}	
			
							}	
						}
						
						break;
						
					case "uname":
						//if (!preg_match('/^(([\xe4-\xe9][\x80-\xbf]{2}){2,4}|[a-z]{1,}|[A-Z]{1,})$/', $text)) {
						if (!$text) {	
							$text = "請輸入正確會員名稱";
						} else {
							$sql = "Update web_member set uname = '".$text."' Where lineID = '".$from."' ";
							$rs = ConnectDB($DB, $sql);
							$text = "您的名稱是 :".$text;
							
							$nextAction = 'sex';
							file_put_contents("./user/user_".$from.".txt", $nextAction);
						}	
						
						break;
						
					case "sex":
					
						$sql = "Update web_member set sex = '".$text."' Where lineID = '".$from."' ";
						$rs = ConnectDB($DB, $sql);
						$text = "您的性別是 :".$text;
						$nextAction = 'mobile';
						file_put_contents("./user/user_".$from.".txt", $nextAction);
						
						break;
						
					case "mobile":
						if (!preg_match("/^09[0-9]{8}$/", $text)) {
							$text = "請輸入正確手機號碼(ex.0919000123)";
						} else {
							$sql = "Update web_member set mobile = '".$text."' Where lineID = '".$from."' ";
							$rs = ConnectDB($DB, $sql);
							$text = "您的手機號碼是 :".$text;						
							$nextAction = 'Done';
							file_put_contents("./user/user_".$from.".txt", $nextAction);
						}
						break;	

					case "productName":
						if (!$text) {	
							$text = "請輸入正確產品名稱";
						} else {
							//$sql = "Update web_member set uname = '".$text."' Where lineID = '".$from."' ";
							//$rs = ConnectDB($DB, $sql);
							$_text = $text;
							$text = "產品名稱是 :".$text;
							$nextAction = 'productNum';
							file_put_contents("./user/user_".$from.".txt", $nextAction);
							$_SESSION[$from][productName] = $_text;
							file_put_contents("./log/debug/prod.txt", serialize($_SESSION));
						}
						break;	

					case "productNum":
						if (!is_numeric($text)) {	
							$text = "只能輸入數字";
						} else {
							if($text > '9999') {
								$text = "產品數量最大為9999";
							} else {
								//$sql = "Update web_member set uname = '".$text."' Where lineID = '".$from."' ";
								//$rs = ConnectDB($DB, $sql);
								$_text = $text;
								$text = "產品數量 :".$text;
								
								$nextAction = 'productPrice';
								file_put_contents("./user/user_".$from.".txt", $nextAction);

								//讀取暫存
								$prodLog = file_get_contents("./log/debug/prod.txt");
								$prodLogAry = unserialize($prodLog);
								$prodLogAry[key($prodLogAry)][productNum] = $_text;
								file_put_contents("./log/debug/prod.txt", serialize($prodLogAry));
							}	
						}
						break;		

					case "productPrice":
						if (!is_numeric($text)) {	
							$text = "只能輸入數字";
						} else {
							if($text > '9999') {
								$text = "產品售價最大為$9,999";
							} else {
								//$sql = "Update web_member set uname = '".$text."' Where lineID = '".$from."' ";
								//$rs = ConnectDB($DB, $sql);
								$_text = $text;
								$text = "產品售價 : $".number_format($text);
								
								$nextAction = 'productImage';
								file_put_contents("./user/user_".$from.".txt", $nextAction);

								//讀取暫存
								$prodLog = file_get_contents("./log/debug/prod.txt");
								$prodLogAry = unserialize($prodLog);
								$prodLogAry[key($prodLogAry)][productPrice] = $_text;
								file_put_contents("./log/debug/prod.txt", serialize($prodLogAry));
							}	
						}
						break;	

					case "productImage":
						$content_type = $receive->events[0]->message->type;
						if ($content_type != 'image') {
							$text = "請上傳產品圖";
						} else {
							//先寫入暫存檔(完成全部填寫在寫入)
							//$sql = "Insert into web_member (loginID, lineID, email, cdate) values ('$text', '$from', '$text', '$cdate') ";
							//$rs = ConnectDB($DB, $sql);
							$content_type = "圖片訊息";
							$message = getObjContent("jpeg");   // 讀取圖片內容
							/*
							$data = ["replyToken" => $reToken, "messages" => array(["type" => "image", "originalContentUrl" => $message, "previewImageUrl" => $message])];

							file_put_contents("./log/debug/debug.txt", json_encode($data)."\r\n",FILE_APPEND);
							*/
							//讀取暫存
							$prodLog = file_get_contents("./log/debug/prod.txt");
							$prodLogAry = unserialize($prodLog);
							$prodLogAry[key($prodLogAry)][productImage][] = $message;
							file_put_contents("./log/debug/prod.txt", serialize($prodLogAry));
							//reply($content_type, $data[messages]);
							//$text = null;
							$nextAction = 'confirm';
							file_put_contents("./user/user_".$from.".txt", $nextAction);
						}
						break;	

					case "confirm":
					
						if($text == '確認') {
							$nextAction = 'addProductDone';
							file_put_contents("./user/user_".$from.".txt", $nextAction);
						}	
						
						break;	
						
						
					case "reservationDate":
						if (!$text) {	
							$text = "請輸入正確產品名稱";
						} else {
							//$sql = "Update web_member set uname = '".$text."' Where lineID = '".$from."' ";
							//$rs = ConnectDB($DB, $sql);
							$_text = $text;
							$text = "產品名稱是 :".$text;
							$nextAction = 'reservationTime';
							file_put_contents("./user/user_".$from.".txt", $nextAction);
							$_SESSION[$from][productName] = $_text;
							file_put_contents("./log/debug/prod.txt", serialize($_SESSION));
						}
						break;

					case "reservationTime":
						$nextAction = 'reservationConfirm';
						file_put_contents("./user/user_".$from.".txt", $nextAction);
						break;		
					
				}
				
			}
		
			if($nextAction) {
				switch($nextAction) {
					case "email":
					
						$content_type = "text";
						$text2 = "請輸入正確email";	
						$messages2 = array(
							array(
								'type' => 'text',
								'text' => $text2,
							),
						);
						//$nowAction = "email";
						break;
					
					case "uname":
					
						$content_type = "text";
						$text2 = "請輸入正確姓名";
						$messages2 = array(
							array(
								'type' => 'text',
								'text' => $text2,
							),
						);
						//$nowAction = "uname";							
						break;
					
					case "sex":
					
						$content_type = "text";
						$text2 = "請輸入正確性別";
						
						$messages2 = array(
							
							array(
								'type' => 'template',
								'altText' => 'LINE@',
								'template' => array(
									'type' => 'confirm',
									'text' => '請輸入正確性別?',
									'actions' => array(
										array(
											"type" => "message",
											"label" => "先生",
											"text" => "先生",
										),
										array(
											"type" => "message",
											"label" => "小姐",
											"text" => "小姐",
										)
									),
								),
							),
								
						);
						//$nowAction = "address";							
						break;
						
					case "mobile":
					
						$content_type = "text";
						$text2 = "請輸入手機號碼";
						$messages2 = array(
							array(
								'type' => 'text',
								'text' => $text2,
							),
						);
						//$nowAction = "address";							
						break;	
					
					case "Done";
					
						$text2 = "感謝你的註冊, 請至註冊信箱收取網站密碼";
							
						
						//寄送成功信件
						$sql = "Select loginID, web_member_id, uname, sex, email, password From web_member Where lineID like '".$from."' ";
						$rs = ConnectDB($DB, $sql);

						for ($i=0; $i<mysql_num_rows($rs); $i++) {
							$row = mysql_fetch_assoc($rs);
							foreach($row as $_key=>$_value) $$_key = $row[$_key];
							
							//發送優惠代碼
							$sqlCode = "Select web_code_id, subject, srange, money, sdate, edate From web_code Where web_x_code_id = '2' and sdate <= '".date("Y-m-d")."' and edate >= '".date("Y-m-d")."' and usedate = '0000-00-00 00:00:00' and web_member_id = '0' order by web_code_id ";
							$rsCode = ConnectDB($DB, $sqlCode);
							if(mysql_num_rows($rsCode)) {
								$web_code_id = mysql_result($rsCode, 0, "web_code_id");
								$codeSubject = mysql_result($rsCode, 0, "subject");
								$srange = mysql_result($rsCode, 0, "srange");
								$money = mysql_result($rsCode, 0, "money");
								$sdate = mysql_result($rsCode, 0, "sdate");
								$edate = mysql_result($rsCode, 0, "edate");
								
								$sql2 = "Update web_code set web_member_id = '".$web_member_id."' Where web_code_id = '".$web_code_id."' ";
								$rs2 = ConnectDB($DB, $sql2);
								
								$text2 = "感謝你的註冊!! 你的優惠代碼:【".$codeSubject."】使用條件為「結帳時滿$".number_format($srange)."元可使用，優惠金額為$".number_format($money)."元，活動時間為".$sdate." ~ ".$edate."」,  並請至註冊信箱收取網站密碼";
								
							}
							
							if(!$password) {
							
								//重設密碼
								$password = getPwd();
								$sql2 = "Update web_member set password = '".md5($password)."' Where web_member_id = '".$web_member_id."' ";
								//$sql2 = "Update web_member set password = '".md5($password)."' Where lineID = '".$from."' ";
								$rs2 = ConnectDB($DB, $sql2);


								$mail_file_location = '../Templates/mail_Line.html';
								if(file_exists($mail_file_location)) {
									
									
									$tpl = file_get_contents($mail_file_location);

									$contactInfo = explode('<span>', str_replace(array('<p>', '</p>', '</span>'), null, $Init_Contact));
									array_filter($contactInfo);
							
									$info_array[2] = preg_replace('/([\x80-\xff]*)/i','',$contactInfo[2]);
									$info_array[1] = preg_replace('/([\x80-\xff]*)/i','',$contactInfo[3]);

									$mailInfo = array(
										'WEBURL' 			=> "http://".$_SERVER['HTTP_HOST'],
										'SENDDATE'			=> date("Y-m-j h:i:s a"),
										'IP'				=> getenv('REMOTE_ADDR'),

										'PWD'				=> $password,
										'EMAIL'				=> $email,
										'TEXTAREA'			=> ($content) ? nl2br($content) : '',

										'COMPANYNAME'		=> $Init_WebTitle,
										'COMPANYTEL'		=> $info_array[2],
										'COMPANYADDRESS'	=> $info_array[0],
										'COMPANYMAIL'		=> $info_array[1],
										'COMPANYFAX'		=> $info_array[3],
										
									);


									$mailSubject = $uname."於".$Init_WebTitle."的LINE@快速註冊";

									Mustache_Autoloader::register();
									$m = new Mustache_Engine;

									//array_push($options['email'], $email);
									//render the template with the set values
									$mailTemplate = $m->render($tpl, $mailInfo);
									$Init_Email_array = explode(',', $Init_Email);
									
									//寄信
									$MailInfo[root] = "../control/";								//路徑
									$MailInfo[subject] = $mailSubject;							//信件主旨
									$MailInfo[body] = $mailTemplate;							//信件內容
									$MailInfo[FromMail] = $Init_Email_array[0];					//寄件人信箱(無作用)
									$MailInfo[FromName] = $Init_WebTitle;						//寄件人名稱
									$MailInfo[ToMail] = $email;									//收件人信箱
									$MailInfo[ToName] = $uname;									//收件人名稱
									$MailInfo[BccMail] = $Init_Email;							//密件副本
									$MailInfo[ReplyMail] = $email;								//回信人信箱
									$MailInfo[ReplyName] = $uname;								//回信人名稱
									
									//file_put_contents("mailInfo.txt", "==============================\r\n",FILE_APPEND);
									//file_put_contents("mailInfo.txt", json_encode($MailInfo)."\r\n",FILE_APPEND);
								
									$message = SendMail($MailInfo);

									//RunJs("login.html", "密碼已寄出，請至您的電子信箱收信");

								}
								//$text2 = "感謝你的註冊, 請至註冊信箱收取網站密碼";
							}
						}	
						
						$text2 = $text2;
						$messages2 = array(
							array(
								'type' => 'text',
								'text' => $text2,
							),
						);
						
						break;
					
					case "productName":
					
						$content_type = "text";
						$text2 = "請輸入產品名稱";	
						$messages2 = array(
							array(
								'type' => 'text',
								'text' => $text2,
							),
						);
						//$nowAction = "email";
						break;
					
					case "productNum":
					
						$content_type = "text";
						$text2 = "請輸入產品數量";
						$messages2 = array(
							array(
								'type' => 'text',
								'text' => $text2,
							),
						);
						//$nowAction = "uname";							
						break;

					case "productPrice":
					
						$content_type = "text";
						$text2 = "請輸入產品售價";
						$messages2 = array(
							array(
								'type' => 'text',
								'text' => $text2,
							),
						);
						//$nowAction = "uname";							
						break;	

					case "productImage":
					
						$content_type = "text";
						$text2 = "請上傳產品圖";
						$messages2 = array(
							array(
								'type' => 'text',
								'text' => $text2,
							),
						);
						//$nowAction = "uname";							
						break;		

					
					case "confirm":
					
						$content_type = "text";
						$text2 = "確定新增產品";
						
						$messages = array(
							
							array(
								'type' => 'template',
								'altText' => 'LINE@',
								'template' => array(
									'type' => 'confirm',
									'text' => '確定新增產品?',
									'actions' => array(
										array(
											"type" => "message",
											"label" => "確認",
											"text" => "確認",
										),
										array(
											"type" => "message",
											"label" => "取消",
											"text" => "取消",
										)
									),
								),
							),
								
						);
						//$nowAction = "address";							
						break;


					case "addProductDone":

						$content_type = "text";
						$text2 = "上傳產品成功";
						$messages2 = array(
							array(
								'type' => 'text',
								'text' => $text2,
							),
						);
							
						$prodLog = file_get_contents("./log/debug/prod.txt");
						$prodLogAry = unserialize($prodLog);

						//
						if(!empty($prodLogAry)) {
							
							$TableName = "web_product";
							$AccurateAction = "Get";

							$_POST['web_x_product_id'] = 1;
							$_POST['ifShow'] = 1;
							$_POST['ifGeneral'] = 1;
							$_POST['subject'] = $prodLogAry[key($prodLogAry)][productName];
							$_POST['stock'] = $prodLogAry[key($prodLogAry)][productNum];
							$_POST['price_cost'] = $prodLogAry[key($prodLogAry)][productPrice];
							$_POST['price_public'] = $prodLogAry[key($prodLogAry)][productPrice];
							$_POST['price_member'] = $prodLogAry[key($prodLogAry)][productPrice];
							$_POST['Covers'] = str_replace('https://aibi.edison1525.net/uploadfiles/l/', '', $prodLogAry[key($prodLogAry)][productImage][0]);
							$_POST['Files'] = str_replace('https://aibi.edison1525.net/uploadfiles/l/', '', $prodLogAry[key($prodLogAry)][productImage][0]);
							$_POST['hits'] = 0;	//瀏覽人數
							$_POST['cdate'] = date("Y-m-d H:i:s");	//建立時間
							$_POST['sdate'] = date("Y-m-d H:i:s");	//建立時間
							$_POST['edate'] = date("Y-m-d H:i:s", strtotime("+1 year"));	//建立時間


							require("./control/includes/accurate.php");
							
							$AccurateAction = "Insert";
							require("./control/includes/accurate.php");	

							reply2('text', $messages, 1);
							return;	
						}	

						break;
					
					case "reservationConfirm":
					
						$content_type = "text";
						$text2 = "確定預約";
						
						$messages2 = array(
							
							array(
								'type' => 'template',
								'altText' => 'LINE@',
								'template' => array(
									'type' => 'confirm',
									'text' => $text,
									'actions' => array(
										array(
											"type" => "message",
											"label" => "Y",
											"text" => "確認預約",
										),
										array(
											"type" => "message",
											"label" => "N",
											"text" => "取約預約",
										)
									),
								),
							),
								
						);
						//$nowAction = "address";							
						break;
				}
				
				
				// 回覆訊息
				//reply($content_type, $messages);
			}
		} else {
			switch($text) {
				/*
				case '新品活動':
					$messages = $text;
					reply2($content_type, $messages, 1);
				break;
				
				case '醫師團隊':
					$messages = $text;
					reply3($content_type, $messages, 1);
				break;
				*/
				case 'admin':
					if($idType == '2')
						//reply($content_type, $messages);
						rich_menu_user('richmenu-3be7e9d9da46299ed0ba6831c33d9b5f', $from);
						//rich_menu_user('richmenu-436bede59fe1abf36539e1b67f5d03d1', $from);
					return;
				break;
				
				case 'menu':
					if($idType == '2')
						//reply($content_type, $messages);
						//rich_menu_user('richmenu-0bd4e2d802043e429c51cc6c29f07397', $from);
						rich_menu_user('richmenu-481b36854c9ac555d78fc5f656a5a07e', $from);
					return;
				break;
				
				case 'menu off':
					rich_menu_user_cancel($from);
					return;
				break;
				
				case 'line id':
					$messages = array(
						array(
							'type' => 'text',
							'text' => $from,
						),
					);
					$messages = ($text2) ? (array_merge($messages, $messages2)) : $messages;
					$content_type = 1;
					reply($content_type, $messages);
					return;
				break;
				
				case 'A1':
					modify_stroe_user('A1', $from);
					//rich_menu_user_cancel($from);
					return;
				break;
				
				case 'A2':
					modify_stroe_user('A2', $from);
					//rich_menu_user_cancel($from);
					return;
				break;
				
				case 'A3':
					modify_stroe_user('A3', $from);
					//rich_menu_user_cancel($from);
					return;
				break;
				
				case 'A5':
					modify_stroe_user('A5', $from);
					//rich_menu_user_cancel($from);
					return;
				break;
				
				case 'A6':
					modify_stroe_user('A6', $from);
					//rich_menu_user_cancel($from);
					return;
				break;
				
				case 'A7':
					modify_stroe_user('A7', $from);
					//rich_menu_user_cancel($from);
					return;
				break;
				
				case 'A9':
					modify_stroe_user('A9', $from);
					//rich_menu_user_cancel($from);
					return;
				break;
				
				case 'AT1':
					modify_stroe_user('AT1', $from);
					//rich_menu_user_cancel($from);
					return;
				break;
				
				case 'AT3':
					modify_stroe_user('AT3', $from);
					//rich_menu_user_cancel($from);
					return;
				break;
				
				default:
					if($text) {
						$messages = array(
							array(
								'type' => 'text',
								'text' => $text,
							),
						);
						$messages = ($text2) ? (array_merge($messages, $messages2)) : $messages;
						// 回覆訊息
						$content_type = 1;
						reply($content_type, $messages);
					}
				break;
			}
			/*
			if($text == '新品活動' || $text == '醫師團隊') {
				if($text == '新品活動') {
					$messages = $text;
					reply2($content_type, $messages, 1);
				} else {
					$messages = $text;
					reply3($content_type, $messages, 1);
				}	
				return;
			} else if($text == 'menu') {
				if($idType == '2')
					//reply($content_type, $messages);
					rich_menu_user('richmenu-0bd4e2d802043e429c51cc6c29f07397', $from);
				return;
			} else if($text == 'menu off') {
				//if($idType == '2')
					rich_menu_user_cancel($from);
				return;
			} else {
				if($text) {
					$messages = array(
						array(
							'type' => 'text',
							'text' => $text,
						),
					);
					$messages = ($text2) ? (array_merge($messages, $messages2)) : $messages;
					// 回覆訊息
					$content_type = 1;
					reply($content_type, $messages);
				}	
			}	
			*/
		}	
	}	
	
	//if(in_array($from, $fromAry)) {
	if(1){	
		if($from === 'U1c491541c08997d9f147026e0762584d' && 0) {
			if($text === 'M') {
				$_SESSION[module] = 'multicast';
				$content_type = "text";
				$message = "你好:現在時間為".date('Y/m/d H:i:s')."模式 : ".$_SESSION[module];
				$messages = array(
					array(
						'type' => 'text',
						'text' => $message,
					),
				);
				push($content_type, $messages);
				file_put_contents("Gate.cfg", "multicast");
				return;
			} else if($text === 'Q') {
				$_SESSION[module] = 'basic';
				$content_type = "text";
				$message = "你好:現在時間為".date('Y/m/d H:i:s')."模式 : ".$_SESSION[module];
				$messages = array(
					array(
						'type' => 'text',
						'text' => $message,
					),
				);
				push($content_type, $messages);
				file_put_contents("Gate.cfg", "basic");
				return;
			}
			//file_put_contents("debug.txt", $_SESSION[module]."\r\n",FILE_APPEND);
			switch(trim(file_get_contents("Gate.cfg"))) {
				case 'multicast':
					$from = array(
						"U1c491541c08997d9f147026e0762584d",
						//"U19f39f5daedb6fa79fd54358b0c86d06",
					);
					$messages = array(
						array(
							'type' => 'text',
							'text' => $text,
						),
					);
					multicast($content_type, $messages);
				break;
				
				case 'basic':
					if($text) { 
						$messages = array(
							array(
								'type' => 'text',
								'text' => $text,
							),
						);
						$messages = ($text2) ? (array_merge($messages, $messages2)) : $messages;
					}
					$messages = ($text2) ? (array_merge($messages, $messages2)) : $messages;
					// 回覆訊息
					reply($content_type, $messages);
				break;
				
				default:
					if($text) { 
						$messages = array(
							array(
								'type' => 'text',
								'text' => $text,
							),
						);
						$messages = ($text2) ? (array_merge($messages, $messages2)) : $messages;
					}
					// 回覆訊息
					reply($content_type, $messages);
				break;
			}
			
		} else {
			//file_put_contents("./debug.txt", $nowAction."\r\n",FILE_APPEND);
			if($nowAction != 'chat') {
				if($text) { 
					$messages = array(
						array(
							'type' => 'text',
							'text' => "機器人回覆:".$text,
						),
					);
					$messages = ($text2) ? (array_merge($messages, $messages2)) : $messages;
				}
				
				//file_put_contents("debug.txt", "============To Data=========\r\n",FILE_APPEND);
				//file_put_contents("debug.txt", json_encode($messages)."\r\n",FILE_APPEND);
				// 回覆訊息
				//reply($content_type, $messages);
			} else {
				//if($text != '預約服務' && $text != 'IG/FB' && $text != '優惠訊息' && $text != '作品集') {
					$content_type = $receive->events[0]->message->type;
					$chatInfoAry = json_decode($receive2, true);
					
					if ($content_type == 'image') {						
						$content_type = "圖片訊息";
						$message = getObjContent("jpeg");   // 讀取圖片內容
						$chatInfoAry[events][0][message][text] = $message;
					}	
					
					$chatInfoAry[events][0][message][store] = $chatStore;
					$receive2 = json_encode($chatInfoAry);
					
					$profileJson = profile($from);
					$chatInfo = json_encode(array_merge(json_decode($receive2, true),json_decode($profileJson, true)));
					file_put_contents("./chat/chat_".date('Ymd').".txt", "============Message=========\r\n",FILE_APPEND);
					file_put_contents("./chat/chat_".date('Ymd').".txt", $chatInfo."\r\n",FILE_APPEND);
					//file_put_contents("debug.txt", $profileJson."\r\n",FILE_APPEND);
					$profileAry = json_decode($profileJson, true);
					
					//curlPusher($receive2);
					$text = "顧客【".$profileAry['displayName']."】諮詢 : ".$text;
					pushFirebase($from,$text,$profileAry['pictureUrl'], $chatStore);
					
					$sql = "SELECT GROUP_CONCAT(DISTINCT lineID SEPARATOR ',') AS lineIDAry FROM web_member WHERE lineID != '' AND ((ifService IN(1) AND ifMessage IN(1) AND store_id = '".$chatStore."') OR (store_id = '-1' AND ifService IN(1) AND ifMessage IN(1)))";
					$pdo = $pdoDB->prepare($sql);
					$pdo->execute();
					$lineIDAry = $pdo->fetch(PDO::FETCH_ASSOC);
					
					if($Init_Chat_Push) {
						if($lineIDAry['lineIDAry'] && 1) {
							
							$from = explode(',', $lineIDAry['lineIDAry']);
							$from = (count($from) >= 2) ? $from : $from[0];
							
							$content_type = "文字訊息";
							$messages = array(
								array(
									'type' => 'text',
									'text' => $text,
								),
							);
							
							if(count($from) >= 2) {
								$request = multicast($content_type, $messages);
							} else {
								//$request = push($content_type, $messages);
								if($reToken) {
									$request = reply($content_type, $messages);
									$requestDecode = json_decode($request, true);
									if($requestDecode['message']) {
										$request = push($content_type, $messages);
									}
								} else {
									$request = push($content_type, $messages);
								}	
							}	
						
						}	
					}
				//}	
				
			}	
		}
		
	} else {
		$content_type = $receive->events[0]->message->type;
		
		//file_put_contents("debug.txt", "============To Data=========\r\n",FILE_APPEND);
		//file_put_contents("debug.txt", json_encode($receive)."\r\n",FILE_APPEND);
		if ($content_type != 'location') {
			if(0) {
				if($text) { 
					$messages = array(
						array(
							'type' => 'text',
							'text' => $text,
							//'text' => '我要預約',
						),
					);
					$messages = ($text2) ? (array_merge($messages, $messages2)) : $messages;
				}
				//file_put_contents("debug.txt", "============To Data=========\r\n",FILE_APPEND);
				//file_put_contents("debug.txt", json_encode($messages)."\r\n",FILE_APPEND);
				// 回覆訊息
				reply($content_type, $messages);	
			}
			//reservation('text', $text, 1, 1);	
		} else {
			$latitude = $receive->events[0]->message->latitude;
			$longitude = $receive->events[0]->message->longitude;
			
			$latitude2 = '24.159108';
			$longitude2 = '120.640425';
			$howLong = getDistance($longitude, $latitude, $longitude2, $latitude2, 2);
			$messages = array(
				array(
					'type' => 'text',
					'text' => $content_type = $receive->events[0]->message->address."\r\n Latitude:".$receive->events[0]->message->latitude."\r\n Longitude:".$receive->events[0]->message->longitude."\r\n 距離:".$howLong."Km",
				),
			);
			$messages = ($text2) ? (array_merge($messages, $messages2)) : $messages;
			//file_put_contents("debug.txt", "============To Data=========\r\n",FILE_APPEND);
			//file_put_contents("debug.txt", json_encode($messages)."\r\n",FILE_APPEND);
			// 回覆訊息
			reply($content_type, $messages);	
		}
	}
	
