<?php
	include_once("../control/includes/function.php");
    //unset($_SESSION['MyShopping']);
	include_once("./checkAgent.php");
	
	if (empty($_SESSION['expire']) || $_SESSION['expire'] < time()) {
		unset($_SESSION[$_SESSION['token']]);
		$_SESSION['token'] = md5('aibi' . uniqid(microtime())); // create token (fast/sufficient) 
	}// create token (fast/sufficient) 
	$_SESSION['expire'] = time() + 900; // make session valid for next 15 mins
	$_SESSION['visitid'] = $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT'];
	
	$_SESSION["session_accept_name"] = null;
	$_SESSION["session_accept_birthday"] = null;
	$_SESSION["session_accept_mobile"] = null;
	
	//if($_SESSION['Member2']['ID'] || $_SESSION['store_id'] == '-1') {
	if($_SESSION['store_id'] == '-1') {	
		$store_id = ($_POST['store_id']) ? $_POST['store_id'] : $xClassRow[0]['web_x_class_id'];	
		//$_SESSION['store_id'] = $store_id;
		$storeRange = 'all';
	} else if($_SESSION['store_id']) {
		$store_id = $_SESSION['store_id'];
		$storeRange = $store_id;
	} else {
		$store_id = $_SESSION['store_id'];
		$storeRange = $store_id;
	}
?>	
<!DOCTYPE html>
<html lang="en">
<?php include_once('head.php'); ?>
<body>
<div class="main">
    <?php include_once('header.php'); ?>
    <div class="content booking2">
		<form name="searchForm" action="booking3.php" id="searchForm" method="POST">
			<a href="booking.php" class="back"></a>
			<ul class="menu">
				<li class="active">我要預約</li>
			</ul>
			<div>
				<div class="row">
					<div class="name">姓名</div>
					<div class="value">
						<input name="accept_name" id="accept_name" type="text" placeholder="請輸入姓名">
					</div>
				</div>
				<div class="row">
					<div class="name">生日</div>
					<div class="value">
						<input type="date" name="accept_birthday" min="1929-01-01" max="<?php echo date('Y-m-d', strtotime('-1 day')); ?>" data-role="datebox" data-options='{"mode": "datebox", "useNewStyle":true}' placeholder="請輸入生日">
					</div>
				</div>
				<div class="row">
					<div class="name">手機</div>
					<div class="value">
						<input type="number" name="accept_mobile" isMobile="1" placeholder="請輸入手機號碼">
					</div>
				</div>
			</div>
			<input type="hidden" name="search" value="1">
			<a href="#" class="btn submit disable search">查詢</a>
		</form>	
    </div>
</div>
<div class="popup_group">
	<div class="popup popup_alert">
		<!--<a class="close" href="#"></a>-->
		<h2>預約管理</h2>
		<!--<a href="#" class="btn submit disable">確認</a>-->
	</div>
</div>	
<script>
	$(function () {
		$('input[name="name"]').on('change', function() {
			if($(this).val() != ''){
				$('.content .btn.submit').removeClass('disable');
			}
		});
		/*
		$('.content .btn.submit').click(function () {
			location.href = 'booking3.html';
		});
		*/
		jQuery.validator.addMethod("string", function (value, element) {
			return this.optional(element) || /^[\u0391-\uFFE5\w]+$/.test(value);
		}, "不允許包含特殊符号!"); 
		jQuery.validator.addMethod("isMobile", function(value, element) {  
			var length = value.length;  
			var mobile = /^09[0-9]{8}$/;  
			return this.optional(element) || (length == 10 && mobile.test(value));  
		}, "請正確填寫手機號碼");  
		$('#searchForm').validate({
			errorElement: 'p',
			submitHandler: function(form) {
				form.submit();
			},
			rules: {
				accept_name: {
					required: false,
					minlength: 2,
					remote: {
						url: '../chekChiness.php',
						type: "post",
						data: {
							type: 1,
							username: function() {
								return $('#accept_name').val();
							}
						}
					}
				}
			},
			messages: {
				accept_name: {
					remote: '請輸入中文姓名'
				}
			}   
		});

		$('.search').on('click', function(e) {
		
			if(!$('input[name="accept_name"]').val() && !$('input[name="accept_birthday"]').val() && !$('input[name="accept_mobile"]').val()) {
				//alert('請至少輸入一個搜尋條件');
				$('.popup_group .popup, .popup_group').fadeOut(100);
				$('.popup_group, .popup_alert').fadeIn(400).find('h2').text('請至少輸入一個搜尋條件');
				setTimeout(function(){
					$('.popup_group .popup, .popup_group').fadeOut(400);
				},1000);
				return;
			}
			e.preventDefault();
			$('#searchForm').submit();
			return false;	
			
		});
		
		$('input').on('change', function() {
			if($(this).val() != ''){
				$('.content .btn.submit').removeClass('disable');
			} else {
				$('.content .btn.submit').addClass('disable');
			}
		});
	})
</script>
</body>
</html>
