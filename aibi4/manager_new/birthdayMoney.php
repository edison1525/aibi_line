<?php
	include_once("../control/includes/function.php");
    //unset($_SESSION['MyShopping']);
	include_once("./checkAgent.php");
	
	if (empty($_SESSION['expire']) || $_SESSION['expire'] < time()) {
		unset($_SESSION[$_SESSION['token']]);
		$_SESSION['token'] = md5('aibi' . uniqid(microtime())); // create token (fast/sufficient) 
	}// create token (fast/sufficient) 
	$_SESSION['expire'] = time() + 900; // make session valid for next 15 mins
	$_SESSION['visitid'] = $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT'];
	
	$todayDay = date('N');
	$endDay = strtotime('+'.ceil(14-14).' day');
	$startDay = strtotime('-2 day', $endDay);
	
	$searchDay = ($_POST['searchDay']) ? strtotime($_POST['searchDay']) :  strtotime(date('Y-m-d'));
	$status = ($_POST['status']) ? $_POST['status'] : '全部';
	$mobile = ($_POST['mobile']) ? $_POST['mobile'] : null;
	
	//unset($_SESSION['store_id']);
	
	//if($_SESSION['Member2']['ID'] || $_SESSION['store_id'] == '-1') {
	if($_SESSION['store_id'] == '-1') {	
		$store_id = ($_POST['store_id']) ? $_POST['store_id'] : $xClassRow[0]['web_x_class_id'];	
		//$_SESSION['store_id'] = $store_id;
		$storeRange = 'all';
	} else if($_SESSION['store_id']) {
		$store_id = $_SESSION['store_id'];
		$storeRange = $store_id;
	} else {
		$store_id = $_SESSION['store_id'];
		$storeRange = $store_id;
	}
	
	$storeSubject = SearchMultidimensionalAry($xClassRow, 'web_x_class_id', 'subject', $store_id);
	
	$sql = "
		SELECT 
			web_member_money.*,
			web_member.uname,
			web_member.mobile,
			web_member.birthday
			
		from 
			web_member_money
		Left Join
			web_member
		On
			web_member_money.web_member_id = web_member.web_member_id
		WHERE 
			1
		AND
			MONTH(web_member.birthday) = '".date('m')."'
		AND
			YEAR(web_member_money.cdate) = '".date('Y')."'
	";
	$sql .= "
		AND 
			web_member_money.store_id = :store_id
	";
	$sql .= "
		Order by
			web_member_money.cdate DESC
	";
	
	$excute = array(
		':store_id'				=> $store_id,
	);
	
	//$debug = new Helper();
	//echo $test = $debug::debugPDO($sql, $excute);
	$pdo = $pdoDB->prepare($sql);
	$pdo->execute($excute);
	$row3 = $pdo->fetchAll(PDO::FETCH_ASSOC);
	/*
	echo "<pre>";
	print_r($row3);
	echo "</pre>";
	*/
	
?>
<!DOCTYPE html>
<html lang="en">
<?php include_once('head.php'); ?>
<script src="https://d.line-scdn.net/liff/1.0/sdk.js"></script>
<script>
	//init LIFF
	function initializeApp(data) {
		//取得QueryString
		let urlParams = new URLSearchParams(window.location.search);
		//顯示QueryString
		if(data.context.userId) {
			var userId = data.context.userId;
			$.ajax({ 
				url: "../action", 
				type: "POST",
				data: {action: 'userInfo', lineId: userId, token: '<?php echo $_SESSION['token']; ?>'}, 
				success: function(e){
					var obj = jQuery.parseJSON(e);
					
					if(obj.error == '0') {
						/*
						var output = '';
						$.each(obj, function(k,v) {
							output += k + ': ' + v +'; ';
						});
						alert(output);
						*/
						$('.popup_group, .popup_login').attr('data-adminid', obj.web_member_id);
						//alert(obj.store_id);
					} 
					
				}
			});	
		}	
		
	}
</script>
<body>
<div class="main">
    <?php include_once('header.php'); ?>
    <div class="content coupon">
        <a class="close" style="display:none;"></a>
        <h2>生日禮兑換</h2>
        <ul class="menu">
            <li><a href="bonus.php">兌換管理</a></li>
            <li class="active">生日禮兑換</li>
        </ul>
        <div class="table">
			<div>
				<form name="form" id="searchForm" action="birthdayMoney.php" method="POST">
					<ul>
						<li style="" class="active">
							<div class="row">
								<div class="value" style="display:inline;">
									<select class="popSelect2" name="store_id" style="display:inline-block;">
							<?php
								foreach($xClassRow as $xClassKey => $xClass) {
									if($storeRange != 'all') {
										if($xClass['web_x_class_id'] != $storeRange) {
											continue;
										}
									}	
									$selected = ($xClass['web_x_class_id'] == $store_id) ? "selected='selected'" : null;
							?>		
										<option value="<?php echo $xClass['web_x_class_id']; ?>" <?php echo $selected; ?>><?php echo $xClass['subject']; ?></option>
							<?php
								}
							?>	
									</select>	
								</div>
							</div>
						</li>
					</ul>
				</form>	
			</div>
	<?php
		foreach($row3 as $key3 => $val3) {

	?>	
            <div class="tr">
                <div>
                    <div><span class="name"><?php echo $val3['uname']; ?> <?php echo "【".$val3['birthday']."】"; ?></span><span><?php echo $val3['mobile']; ?></span></div>
                    <div class="color"><span>生日禮金：</span><span><?php echo number_format($val3['money']); ?>元</span></div>
                    <div class="color2">
						<?php 
							switch($val3['kind']) {
								case 0:
									$status = '未兌換';
								break;
								case 1:
									$status = '已兌換';
								break;
								case 2:
									$status = '已取消';
								break;
								case 3:
									$status = '已使用';
								break;
							}	
							echo $status;
						?>
					</div>
                </div>
                <div class="right">
                    <a href="birthdayMoney-info1.php?web_member_money_id=<?php echo $val3['web_member_money_id']; ?>" class="btn info">詳情</a>
			<?PHP
				if($val3['kind'] == '0') {
					//if($val3['paymentstatus'] == '未兌換') { 
			?>		
                    <a href="#" class="btn manager" data-id="<?php echo $val3['web_member_money_id']; ?>">管理</a>
			<?php
					//}
				}	
			?>		
                </div>
            </div>
	<?php
		}
	?>		
        </div>
    </div>
</div>
<div class="popup_group">
    <div class="popup popup_login" data-id="" data-adminid="<?php echo $_SESSION['Member2']['ID']; ?>">
        <a class="close" href="#"></a>
        <h2>兌換管理</h2>
        <a href="#" class="btn items" data-status="pay">已兌換</a>
        <a href="#" class="btn items" data-status="cancel">兌換取消</a>
        <a href="#" class="btn submit disable">確認</a>
    </div>
	
	<div class="popup popup_alert">
		<h2>消費管理</h2>
		<div class="btns remark">
			<a href="#" class="btn submit checkUser">確認</a>
		</div>
	</div>
</div>
<div class="popup_filter">
    <div class="popup">
        <div class="content">
            <form name="form" id="searchForm" action="bonus.php" method="POST">
				<div class="row">
					<div class="name">店別</div>
					<div class="value">
						<select id="store" name="store_id">
				<?php
					foreach($xClassRow as $xClassKey => $xClass) {
						if($storeRange != 'all') {
							if($xClass['web_x_class_id'] != $storeRange) {
								continue;
							}
						}	
						$selected = ($xClass['web_x_class_id'] == $store_id) ? "selected='selected'" : null;
				?>		
							<option value="<?php echo $xClass['web_x_class_id']; ?>" <?php echo $selected; ?>><?php echo $xClass['subject']; ?></option>
				<?php
					}
				?>	
						</select>
					</div>
				</div>
				<div class="row">
					<div class="name">狀態</div>
					<div class="value">
						<select name="status">
							<option>全部</option>
							<option value="尚未收款">未收款</option>
							<option value="付款成功">已收款</option>
							<option value="取消">已取消</option>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="name">日期</div>
					<div class="value">
						<select name="searchDay">
					<?php
						for($dayS=$startDay; $dayS<=$endDay; $dayS+=86400) {
							$selected = (date('Y/m/d', $dayS) == date('Y/m/d')) ? 'selected="selected"' : null;
					?>		
							<option <?php echo $selected; ?> value="<?php echo date('Y-m-d', $dayS);?>"><?php echo date('Y/m/d', $dayS);?> 週<?php echo $dayName[date('N', $dayS)];?><?php if(date('Y/m/d', $dayS) == date('Y/m/d')) { ?>(今天)<?php } ?></option>
					<?php
						}
					?>		
						</select>
					</div>
				</div>
				<div class="row">
					<div class="name">手機</div>
					<div class="value">
						<input type="text" name="mobile" id="mobile" isMobile="1" maxlength="10" minlength="10" placeholder="請輸入手機號碼">
					</div>
				</div>
				<input class="form-control" type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>" />
				<a href="#" class="btn submit">搜尋</a>
			</form>	
        </div>
    </div>
</div>

<script>
	$(function () {
		liff.init(function (data) {
			initializeApp(data);
		});
		
<?php
	$agent = $_SERVER['HTTP_USER_AGENT'];
	if (preg_match("#\bLine\b#", $agent)) {
?>		
		if(!$('input[name="storeId"]').val()) {
			liff.init(function (data) {
				initializeApp(data);
			});
			$('.popup_group, .popup_alert').fadeIn(400).find('h2').text('身份讀取中');
			/*
			setTimeout(function(){
				$('.popup_group .popup, .popup_group').fadeOut(400);
			},1000);
			*/
		}
		
		$('.checkUser').on('click', function(){ 
			window.location.reload();
		});
<?php
	}
?>
		
		if($.cookie('storeIdCookie')) {
			//alert($.cookie('storeIdCookie'));
			if($('#store').find('option[value="'+$.cookie('storeIdCookie')+'"]').length) {
				$('#store').find('option[value="'+$.cookie('storeIdCookie')+'"]').prop('selected', true);
				$('#storeName').text($('#store').find('option[value="'+$.cookie('storeIdCookie')+'"]').text());
			} else {
				var _storeId = $('input[name="storeId"]').val();
				$('#store').find('option[value="'+_storeId+'"]').prop('selected', true);
				$('#storeName').text($('#store').find('option[value="'+_storeId+'"]').text());
			}
			
			if($.cookie('storeIdCookie') != _storeId) {
				//$('#searchForm').submit();
			}
		} 
		/*
		var _storeId = $('input[name="storeId"]').val();
		//alert($('#store').find('option[value="'+_storeId+'"]').text()+"="+$.cookie('storeIdCookie'));
		$('#store').find('option[value="'+_storeId+'"]').prop('selected', true);
		$('#storeName').text($('#store').find('option[value="'+_storeId+'"]').text());
		*/
		$('#store').on('change', function() {
			var storeId = $(this).find('option:selected').val();
			$.cookie('storeIdCookie', storeId, {
				expires:7, 
				path: '/'
			});
		});
		
		$('.popup_login .btn.items').click(function () {
			$('.popup_login .btn.items').removeClass('selected');
			$(this).addClass('selected');
			$('.popup_login .btn.submit').removeClass('disable').addClass('active');
		});
		$('.btn.manager').click(function () {
			var web_member_money_id = $(this).attr('data-id');
			if(!web_member_money_id) {
				return false;
			}
			$('.popup_group, .popup_login').attr('data-id', web_member_money_id).fadeIn(400);
		});
		$('.popup .close, .popup .back').on('click', function () {
			$('.popup_group .popup, .popup_group').fadeOut(400);
		});
		
		$('.popup_login .submit').on('click', function () {
			
			if($(this).is('.active')) {
				
				var id = $('.popup_login').attr('data-id');
				var status = $('.popup_login .btn.items.selected').attr('data-status');
				var admin = $('.popup_login').attr('data-adminid');
				//alert(id+"=="+status+"==="+admin);
				if(!id || !status || !admin) {
					return false;
				}
					
				$.ajax({ 
					url: "../action", 
					type: "POST",
					data: {
						action: 'birthdayMoneyPay', 
						id: id,
						status: status,
						admin: admin,
						token: '<?php echo $_SESSION['token']; ?>'
					}, 
					success: function(e){
						var obj = jQuery.parseJSON(e);
						
						if(obj.error == '0') {
							$('.popup_group .popup, .popup_group').fadeOut(400);
							$('[data-id="'+id+'"]').hide();
							location.reload();
						}
						
					}
				});
				
			}	
		});
		
		$('.filter .icon').click(function () {
			$('.popup_filter, .popup').fadeIn(400);
		});
		
		jQuery.validator.addMethod("string", function (value, element) {
			return this.optional(element) || /^[\u0391-\uFFE5\w]+$/.test(value);
		}, "不允許包含特殊符号!"); 
		jQuery.validator.addMethod("isMobile", function(value, element) {  
			var length = value.length;  
			var mobile = /^09[0-9]{8}$/;  
			return this.optional(element) || (length == 10 && mobile.test(value));  
		}, "請正確填寫手機號碼");  
		$('#searchForm').validate({
			errorElement: 'p',
			submitHandler: function(form) {
				form.submit();
			},
			rules: {
				accept_name: {
					required: false,
					minlength: 2,
					remote: {
						url: '../chekChiness.php',
						type: "post",
						data: {
							type: 1,
							username: function() {
								return $('#accept_name').val();
							}
						}
					}
				}
			},
			messages: {
				accept_name: {
					remote: '請輸入中文姓名'
				}
			}   
		});
		
		$('.popup_filter .submit').click(function (e) {
			e.preventDefault();
			$('#searchForm').submit();
			$('.popup_filter, .popup').hide();
			return false;
		});
		
		$('.popSelect2').on('change', function() {
			$('#searchForm').submit();
		});
	})
</script>
</body>
</html>
