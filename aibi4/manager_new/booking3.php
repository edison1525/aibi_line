<?php
	include_once("../control/includes/function.php");
    //unset($_SESSION['MyShopping']);
	include_once("./checkAgent.php");
	
	if (empty($_SESSION['expire']) || $_SESSION['expire'] < time()) {
		unset($_SESSION[$_SESSION['token']]);
		$_SESSION['token'] = md5('aibi' . uniqid(microtime())); // create token (fast/sufficient) 
	}// create token (fast/sufficient) 
	$_SESSION['expire'] = time() + 900; // make session valid for next 15 mins
	$_SESSION['visitid'] = $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT'];
	
	$accept_name = (!$_SESSION["session_accept_name"]) ? trim($_POST["accept_name"]) : $_SESSION["session_accept_name"];
	$accept_birthday = (!$_SESSION["session_accept_birthday"]) ?  trim($_POST["accept_birthday"]) : $_SESSION["session_accept_birthday"];
	$accept_mobile = (!$_SESSION["session_accept_mobile"]) ?  trim($_POST["accept_mobile"]) : $_SESSION["session_accept_mobile"];
	
	$searchType = ($_POST["search"]) ?  trim($_POST["search"]) : 0;
	
	$_SESSION["session_accept_name"] = $accept_name;
	$_SESSION["session_accept_birthday"] = $accept_birthday;
	$_SESSION["session_accept_mobile"] = $accept_mobile;
	
	$_POST["accept_name"] = $accept_name;
	$_POST["accept_birthday"] = $accept_birthday;
	$_POST["accept_mobile"] = $accept_mobile;
	
	//if($_SESSION['Member2']['ID'] || $_SESSION['store_id'] == '-1') {
	if($_SESSION['store_id'] == '-1') {	
		$store_id = ($_POST['store_id']) ? $_POST['store_id'] : $xClassRow[0]['web_x_class_id'];	
		//$_SESSION['store_id'] = $store_id;
		$storeRange = 'all';
	} else if($_SESSION['store_id']) {
		$store_id = $_SESSION['store_id'];
		$storeRange = $store_id;
	} else {
		$store_id = $_SESSION['store_id'];
		$storeRange = $store_id;
	}
	
	if(!$accept_name && !$accept_birthday && !$accept_mobile) {
		die('系統維護中');
	}
	
	$sql = "
		SELECT 
			SQL_CALC_FOUND_ROWS a.*,
			b.web_shift_id,
			b.web_class_id,
			b.web_team_id,
			c.subject as classSubjct,
			d.subject as teamSubject,
			e.subject as teamXsubject,
			f.uname as funame,
			f.birthday as fbirthday,
			f.mobile as fmobile	
		FROM 
			web_x_register a
		Left Join 
			web_shift b ON b.web_shift_id = a.web_shift_id
		Left Join 
			web_class c ON c.web_class_id = b.web_class_id 		
		Left Join 
			web_team d ON d.web_team_id = b.web_team_id
		Left Join 
			web_x_team e ON e.web_x_team_id = d.web_x_team_id
		Left Join 
			web_member f ON f.uname = a.order_name AND f.mobile = a.order_mobile		
		where 
			1
	";
if($accept_name) {	
	$sql .= "
		AND 
			a.accept_name LIKE :accept_name
	";	
}
if($accept_birthday) {	
	$sql .= "
		AND 
			a.accept_birthday = :accept_birthday
	";	
}
if($accept_mobile) {	
	$sql .= "
		AND 
			a.order_mobile = :order_mobile
	";	
}
	$sql .= "Group BY";
if(!$accept_name) {		
	$sql .= " a.accept_name";
} else {
	$sql .= " a.order_mobile";
}	
	$sql .= "
		ORDER BY 
			a.subject ASC,
			a.registerDate ASC
	";
	$excute = array();
	
	$excute = ($accept_name) ? $excute+array(':accept_name' => '%'.$accept_name.'%') : $excute;
	$excute = ($accept_birthday) ? $excute+array(':accept_birthday' => $accept_birthday) : $excute;
	$excute = ($accept_mobile) ? $excute+array(':order_mobile' => $accept_mobile) : $excute;
	
	
	//$debug = new Helper();
	//echo $test = $debug::debugPDO($sql, $excute);
	$pdo = $pdoDB->prepare($sql);
	$pdo->execute($excute);
	$row3 = $pdo->fetchAll(PDO::FETCH_ASSOC);
	/*
	echo "<pre>";
	print_r($row3);
	echo "</pre>";
	*/
?>	
<!DOCTYPE html>
<html lang="en">
<?php include_once('head.php'); ?>
<body>
<div class="main">
    <?php include_once('header.php'); ?>
    <div class="content appointment"><a href="booking2.php" class="back"></a>
        <ul class="menu">
            <li>預約管理</li>
        </ul>
        <ul class="filter">
	<?php
		foreach($_POST as $postKey => $_postVal) {
			if(!$_postVal || $postKey == 'search') {
				continue;
			}
	?>
			<li><a><?php echo $_postVal; ?></a></li>
	<?php
		}
	?>	
		</ul>
        <div class="table">
	<?php
	if(count($row3)) {
		$regsiterMember = array();
		foreach($row3 as $rowKey3 => $rowVal3) {
			$regsiterMember[] = $rowVal3['web_member_id'];
	?>	
            <div class="tr">
                <div class="td"><span class="accept_name"><?php echo $rowVal3['accept_name']; ?></span></div>
                <div class="td color2">電話：<span class="accept_mobile"><?php echo $rowVal3['order_mobile']; ?></span></div>
                <div class="td color2">生日：<span class="accept_birthday"><?php echo $rowVal3['accept_birthday']; ?></span></div>
                <div class="td right">
                    <a href="#" class="btn manager" data-lineid="<?php echo $rowVal3['lineID']; ?>">預約</a>
                    <a href="booking-info.php?id=<?php echo $rowVal3['web_x_register_id']; ?>&search=1" class="btn info">詳情</a>
                </div>
            </div>
    <?php
		}
		$regsiterMember = array_unique($regsiterMember);
	}
		$sql = "
			SELECT 
				SQL_CALC_FOUND_ROWS a.*
			FROM 
				web_member a

			where 
				1
		";
	if($accept_name) {	
		$sql .= "
			AND 
				a.uname LIKE :accept_name
		";	
	}
	if($accept_birthday) {	
		$sql .= "
			AND 
				a.birthday = :accept_birthday
		";	
	}
	if($accept_mobile) {	
		$sql .= "
			AND 
				a.mobile = :order_mobile
		";	
	}
	if(count($regsiterMember)) {
		$sql .= "
			AND 
				a.web_member_id NOT IN (".implode(',', $regsiterMember).")
		";
	}
		$sql .= "
			ORDER BY 
				a.web_member_id ASC
		";
		$excute2 = array();
		
		$excute2 = ($accept_name) ? $excute2+array(':accept_name' => '%'.$accept_name.'%') : $excute2;
		$excute2 = ($accept_birthday) ? $excute2+array(':accept_birthday' => $accept_birthday) : $excute2;
		$excute2 = ($accept_mobile) ? $excute2+array(':order_mobile' => $accept_mobile) : $excute2;
		
		
		//$debug = new Helper();
		//echo $test = $debug::debugPDO($sql, $excute);
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute($excute);
		$row4 = $pdo->fetchAll(PDO::FETCH_ASSOC);
	if(count($row4)) {	
		foreach($row4 as $rowKey4 => $rowVal4) {
	?>	
			<div class="tr">
                <div class="td"><span class="accept_name"><?php echo $rowVal4['uname']; ?></span></div>
                <div class="td color2">電話：<span class="accept_mobile"><?php echo $rowVal4['mobile']; ?></span></div>
                <div class="td color2">生日：<span class="accept_birthday"><?php echo $rowVal4['birthday']; ?></span></div>
                <div class="td right">
                    <a href="#" class="btn manager" data-lineid="<?php echo $rowVal4['lineID']; ?>">預約</a>
                </div>
            </div>
	<?php
		}
	}	
	?>	
        </div>
<?php
	if(count($row3) || count($row4)) {
?>		
		<a href="#" class="btn submit">新增預約</a>
<?php
	} else {
?>		
        <div class="table no_data">
            <div class="tr">
                <div class="td">查無此結果</div>
            </div>
        </div>
		<a href="#" class="btn submit">新增預約</a>
<?php
	}
?>	
    </div>
</div>
<div class="popup_group">
	<div class="popup popup_alert">
		<!--<a class="close" href="#"></a>-->
		<h2>報到管理</h2>
		<div></div>
		<a href="#" class="btn submit cofirm active">確認</a>
	</div>
    <div class="popup popup_info">
        <!--<a class="close" href="#"></a>-->
        <h2>預約成功</h2>
        <div class="content registerInfoList">
            <div class="info">
                <div class="row">
                    <div class="name">陳小春</div>
                    <div class="value">電話：0933888222 <br> 生日：1987/05/27</div>
                </div>
            </div>
            <div class="row">
                <div class="name">類型</div>
                <div class="value">本人預約</div>
            </div>
            <div class="row">
                <div class="name">項目</div>
                <div class="value">內科</div>
            </div>
            <div class="row">
                <div class="name">設計師</div>
                <div class="value">2診王淑貞醫師</div>
            </div>
            <div class="row">
                <div class="name">日期</div>
                <div class="value">2019/05/27 (一)</div>
            </div>
            <div class="row">
                <div class="name">時間</div>
                <div class="value">上午診09:30-13:00 11號</div>
            </div>
            <a href="#" class="btn submit">確認</a>
        </div>
    </div>
    <div class="popup popup_appointment">
        <h2><a href="#" class="back"></a> <div class="tit">預約管理</div> <a class="close"></a></h2>
        <div class="content">
			<div style="float:right; height:5px;">* 為必填</div>
			<form name="form" id="formContent" method="POST">
				<div class="row">
					<div class="name">姓名 *</div>
					<div class="value">
						<input name="uname" id="username" type="text" placeholder="請輸入姓名" value="<?php echo $accept_name; ?>" required="">
					</div>
				</div>
				<div class="row">
					<div class="name">生日</div>
					<div class="value">
						<input name="birthday" id="birthday" type="date" min="1929-01-01" max="<?php echo date('Y-m-d', strtotime('-1 day')); ?>" placeholder="請輸入生日" value="<?php echo $accept_birthday; ?>">
					</div>
				</div>
				<div class="row">
					<div class="name">手機</div>
					<div class="value">
						<input name="mobile" id="mobile" type="text" placeholder="請輸入手機" isMobile="1" maxlength="10" minlength="10" value="<?php echo $accept_mobile; ?>">
					</div>
				</div>
				<div class="row">
					<div class="name">分店</div>
					<div class="value">
				<?php
					foreach($xClassRow as $xClassKey => $xClassVal) {
						if($storeRange != 'all') {
							if($xClassVal['web_x_class_id'] != $store_id) {
								continue;
							}
							$defaultChecked = 'checked="checked"';
						} else {	
							$defaultChecked = ($xClassKey == 0) ? 'checked="checked"' : null;
							if($xClassKey) {
								//continue;
							}
						}	
				?>		
						<input type="radio" id="r2-<?php echo $xClassKey; ?>" value="<?php echo $xClassVal['web_x_class_id']; ?>" name="xClass" <?php echo $defaultChecked; ?> data-text="<?php echo $xClassVal['subject']; ?>">
						<label for="r2-<?php echo $xClassKey; ?>"><?php echo $xClassVal['subject']; ?></label>
				<?php
					}
				?>	
					</div>
				</div>
				<div class="row">
					<div class="name">設計師</div>
					<div class="classList value">
				<?php
					foreach($classRow as $classKey => $classVal) {
						$defaultChecked = ($classKey == 0) ? 'checked="checked"' : null;
				?>		
						<input type="radio" id="r<?php echo $classKey; ?>" value="<?php echo $classVal['web_class_id']; ?>" name="class" <?php echo $defaultChecked; ?> data-text="<?php echo $classVal['subject']; ?>">
						<label for="r<?php echo $classKey; ?>"><?php echo $classVal['subject']; ?></label>
				<?php
					}
				?>
					</div>
				</div>
				<div class="row">
					<div class="name">項目</div>
					<div class="teamList value">
				<?php
					$teamHaveClass = 0;
					foreach($teamRow as $teamKey => $teamVal) {
						/*
						$lineInfoAry = explode('#####', $teamVal['lineInfo']);
						
						foreach($lineInfoAry as $$lineInfoKey => $lineInfoVal) {
							$lineInfoRowAry[$teamVal['web_team_id']][] = explode('@#@', $lineInfoVal);
							//$listTimeByTeam[$teamVal][]
						}	
						
						$teamHaveClass = SearchMultidimensionalArray($lineInfoRowAry[$teamVal['web_team_id']], 2, $classRow[0]['web_class_id']);
						if(!$teamHaveClass) {
							continue;
						}
						*/
						$defaultChecked = ($teamKey == 0) ? 'checked="checked"' : null;
						
				?>	
						<input type="radio" id="r1-<?php echo $teamKey; ?>" value="<?php echo $teamVal['web_x_team_id']; ?>" name="team" <?php echo $defaultChecked; ?> data-text="<?php echo $teamVal['subject'].$teamVal['web_x_team_subject']; ?>">
						<label for="r1-<?php echo $teamKey; ?>"><?php echo $teamVal['subject'].$teamVal['web_x_team_subject']; ?></label>
				<?php
					}
				?>
					</div>
				</div>
				<div class="row">
					<div class="name">預約日期/到店時間 *</div>
					<div class="value">
						<input type="text" name="date" id="date" placeholder="選擇預約日期/到店時間" required="">
						<input type="hidden" name="timeId" id="timeId">
					</div>
				</div>
				<div class="row">
					<div class="name">預約方式</div>
					<div class="value">
				<?php
					if($isLine && 0) {
				?>	
						<input type="radio" id="t32" name="registerType" value="1" checked>
						<label for="t32">LINE</label>
						
						<input type="radio" id="t22" name="registerType" value="2">
						<label for="t22">電話</label>
						
						<input type="radio" id="t12" name="registerType" value="3">
						<label for="t12">現場</label>
				<?php
					} else {
				?>	
						<input type="radio" id="t22" name="registerType" value="2" checked>
						<label for="t22">電話</label>
						
						<input type="radio" id="t12" name="registerType" value="3">
						<label for="t12">現場</label>
				<?php
					}
				?>	
					</div>
				</div>
				
				<div class="row">
					<div class="name">備註</div>
					<div class="value">
						<textarea name="remark" placeholder="輸入文字"></textarea>
					</div>
				</div>
				
				<input class="form-control" type="hidden" name="teamXid" id="teamXid" value="" />
				<input class="form-control" type="hidden" name="lineID" value="" />
				<input class="form-control" type="hidden" name="uname2" id="username2" value="" />
				<input class="form-control" type="hidden" name="shiftId" value="" />
				<input class="form-control" type="hidden" name="person" value="0" />
				<input class="form-control" type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>" />
				<input class="form-control" type="hidden" name="action" value="registerManager" />
				<a href="#" class="btn submit confirm">確認預約</a>
			</form>
        </div>
    </div>
    <div class="popup popup_appointment_date">
        <a class="close" data-type="date" href="#"></a>
        <h2>
            <a href="#" data-type="date" class="back"></a>
            <div class="tit">篩選設定<span class="showText"></span></div>
            <div class="row" style="display:none;">
                <div class="name">顯示區間</div>
                <div class="value">
                    <input type="text" placeholder="" value="2019/05/13"> <span>~</span>  <input type="text" placeholder="" value="2019/05/13">
                </div>
            </div>
        </h2>
        <div class="content">
            <table class="pc">
                <tr>
                    <td></td>
                    <td>週一 05/13</td>
                    <td>週二 05/13</td>
                    <td>週三 05/13</td>
                    <td>週四 05/13</td>
                    <td>週五 05/13</td>
                    <td>週六 05/13</td>
                    <td>週日 05/13</td>
                </tr>
                <tr>
                    <td><span>上午</span><span>09:30</span><span class="line">|</span><span>13:00</span></td>
                    <td class="items"><span>預約人數</span><span>10 人</span></td>
                    <td>-</td>
                    <td class="items"><span>預約人數</span><span>6 人</span></td>
                    <td>-</td>
                    <td>-</td>
                    <td class="items"><span>預約人數</span><span>10 人</span></td>
                    <td class="items"><span>預約人數</span><span>6 人</span></td>
                </tr>
                <tr>
                    <td><span>中午</span><span>15:00</span><span class="line">|</span><span>18:00</span></td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td class="items"><span>預約人數</span><span>6 人</span></td>
                </tr>
                <tr>
                    <td><span>晚上</span><span>19:00</span><span class="line">|</span><span>21:30</span></td>
                    <td class="items"><span>預約人數</span><span class="red">10 人</span></td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td class="items"><span>預約人數</span><span class="blue">6 人</span></td>
                </tr>
            </table>
            <div style="width:700px; overflow:scroll;">
				<table class="mobile">
					<tr>
						<td></td>
						<td><span>上午</span><span>09:30-13:00</span></td>
						<td><span>中午</span><span>15:00-18:00</span></td>
						<td><span>晚上</span><span>19:00-21:30</span></td>
					</tr>
					<tr>
						<td><span>週一</span><span>05/13</span></td>
						<td class="items"><span>預約人數</span><span>10 人</span></td>
						<td>-</td>
						<td class="items"><span>預約人數</span><span>6 人</span></td>
					</tr>
					<tr>
						<td><span>週一</span><span>05/13</span></td>
						<td>-</td>
						<td>-</td>
						<td class="items"><span>預約人數</span><span>6 人</span></td>
					</tr>
					<tr>
						<td><span>週一</span><span>05/13</span></td>
						<td class="items"><span>預約人數</span><span class="red">10 人</span></td>
						<td>-</td>

						<td class="items"><span>預約人數</span><span class="blue">6 人</span></td>
					</tr>
				</table>
			</div>
            <a href="#" class="btn submit disable calendar">確認選擇</a>
        </div>
    </div>
</div>
<script>
	function cookSelected() {
		var hasXclassVal = ($.cookie('xClassVal')) ? $.cookie('xClassVal') : null;
		var hasClassVal = ($.cookie('classVal')) ? $.cookie('classVal') : null;
		var hasTeamVal = ($.cookie('teamVal')) ? $.cookie('teamVal') : null;
		
		console.log(hasClassVal, hasTeamVal);
		
		if(hasXclassVal && hasClassVal && hasTeamVal) {
			//$('input[name="class"][value="'+hasClassVal+'"]').trigger('click');
			
			$.each($('input[name="team"]'), function(k,e) {
				if($(e).val() == hasTeamVal) {
					$(e).trigger('click');
					return false;
				}
			})
		}
	
	}
	
	function btnInitForIcon() {
		$('.popup .close, .popup .back').on('click', function () {
			if($(this).attr('data-type') != 'date') {
				alert('2222');
				$('.popup_group .popup, .popup_group').fadeOut(400);
			}	
		});
		
		$('.content .btn.submit').click(function () {
			$('.popup_group, .popup_appointment').fadeIn(400);
		});
		/*
		$('.popup_appointment .btn.submit').click(function () {
			$('.popup_appointment').hide();
			$('.popup_group, .popup_info').fadeIn(400);
		});
		*/
		$('.popup_info .btn.submit').click(function () {
			$('.popup_group .popup_info, .popup_group').fadeOut(400);
		});
	
		$('.popup_appointment_date .items').click(function () {
			$('.popup_appointment_date .items').removeClass('selected');
			$(this).addClass('selected');
			$('.popup_appointment_date .btn.submit').removeClass('disable');
			var shiftid = $(this).find('.shiftInfo').attr('data-shiftid');
			var shifttext = $(this).find('.shiftInfo').attr('data-shifttext');
			var date = $(this).find('.shiftInfo').attr('data-date');
			var timeId = $(this).find('.shiftInfo').attr('data-timeid');
			
			$('.popup_appointment_date .btn.submit').removeClass('disable').attr({
				'data-shiftid': shiftid,
				'data-shifttext': shifttext,
				'data-date': date,
				'data-timeid': timeId
			});
		});
		
		$('input[name="team"], input[name="class"]').on('click', function() {
			$('input[name="date"]').val(null);
			
			$('input[name="timeId"]').val($('input[name="class"]:checked').val());
			$('input[name="teamXid"]').val($('input[name="team"]:checked').val());
		})
		
		$('input[name="class"]').on('click', function() {
			var classVal = $(this).val();
			if(!classVal) {
				//alert('系統維護中');
				$('.popup_group, .popup_alert').fadeIn(400).find('h2').text('系統維護中');
				liff.closeWindow();
				setTimeout(function(){
					liff.closeWindow();
				},3000);
			} else {	
				$.ajax({ 
					url: "../action", 
					type: "POST",
					data: {
						action: 'classClickBySearch', 
						classVal: classVal,
						token: '<?php echo $_SESSION['token']; ?>'
					}, 
					success: function(e){
						var obj = jQuery.parseJSON(e);
						
						if(obj.error == '0') {
							$('.teamList').html(obj.tpl);
						}
						
					}
				});
			}	
		});
	}
	
	function btnInit() {
		$('.popup .close, .popup .back').on('click', function () {
			if($(this).attr('data-type') != 'date') {
				$('.popup_group .popup, .popup_group').fadeOut(400);
			}
		});
		
		$('.content .btn.submit').click(function () {
			$('.popup_group, .popup_appointment').fadeIn(400);
		});
		/*
		$('.popup_appointment .btn.submit').click(function () {
			$('.popup_appointment').hide();
			$('.popup_group, .popup_info').fadeIn(400);
		});
		*/
		$('.popup_info .btn.submit').click(function () {
			$('.popup_group .popup_info, .popup_group').fadeOut(400);
		});
	
		$('.popup_appointment_date .items').click(function () {
			$('.popup_appointment_date .items').removeClass('selected');
			$(this).addClass('selected');
			$('.popup_appointment_date .btn.submit').removeClass('disable');
			var shiftid = $(this).find('.shiftInfo').attr('data-shiftid');
			var shifttext = $(this).find('.shiftInfo').attr('data-shifttext');
			var date = $(this).find('.shiftInfo').attr('data-date');
			var timeId = $(this).find('.shiftInfo').attr('data-timeid');
			
			$('.popup_appointment_date .btn.submit').removeClass('disable').attr({
				'data-shiftid': shiftid,
				'data-shifttext': shifttext,
				'data-date': date,
				'data-timeid': timeId
			});
		});
		
		$('input[name="team"], input[name="class"]').on('click', function() {
			$('input[name="date"]').val(null);
		})
		
		$('input[name="class"]').on('click', function() {
			var classVal = $(this).val();
			if(!classVal) {
				//alert('系統維護中');
				$('.popup_group, .popup_alert').fadeIn(400).find('h2').text('系統維護中');
				liff.closeWindow();
				setTimeout(function(){
					liff.closeWindow();
				},3000);
			} else {	
				$.ajax({ 
					url: "../action", 
					type: "POST",
					data: {
						action: 'classClickBySearch', 
						classVal: classVal,
						token: '<?php echo $_SESSION['token']; ?>'
					}, 
					success: function(e){
						var obj = jQuery.parseJSON(e);
						
						if(obj.error == '0') {
							$('.teamList').html(obj.tpl);
						}
						
					}
				});
			}	
		});
	}
	
	$(function () {
		btnInit();
		$('.popup .close, .popup .back').on('click', function () {
			
			if($(this).attr('data-type') != 'date') {
				$('.popup_group .popup, .popup_group').fadeOut(400);
			}
		});
		$('.content .btn.submit').click(function () {
			//console.log($('.popup_group, .popup_appointment').find('h2'));
			$('.popup_group, .popup_appointment').find('h2').html('<a href="#" class="back"></a> <div class="tit">新增預約</div>').fadeIn(400);
			
			if(!$(this).hasClass('calendar') && !$(this).hasClass('confirm')) {
				var hasXclassVal = ($.cookie('xClassVal')) ? $.cookie('xClassVal') : null;
				var hasClassVal = ($.cookie('classVal')) ? $.cookie('classVal') : null;
				var hasTeamVal = ($.cookie('teamVal')) ? $.cookie('teamVal') : null;
				//console.log(hasClassVal);
				/*
				if(hasClassVal) {
					$('input[name="class"][value="'+hasClassVal+'"]').trigger('click');
				} else {
					$('input[name="class"]:checked').trigger('click');
				}
				*/
				if(hasXclassVal) 
					$('input[name="xClass"][value="'+hasXclassVal+'"]').attr({'data-iconflag':'true'}).trigger('click');
				else
					$('input[name="xClass"]:checked').trigger('click');
			}	
			
			//$('input[name="class"]:checked').trigger('click');
		});
		/*
		$('.popup_appointment .btn.submit').click(function () {
			$('.popup_appointment').hide();
			$('.popup_group, .popup_info').fadeIn(400);
		});
		*/
		$('.popup_info .btn.submit').click(function () {
			$('.popup_group .popup_info, .popup_group').fadeOut(400);
		});
	
		$('.popup_appointment_date .items').click(function () {
			$('.popup_appointment_date .items').removeClass('selected');
			$(this).addClass('selected');
			var shiftid = $(this).find('.shiftInfo').attr('data-shiftid');
			var shifttext = $(this).find('.shiftInfo').attr('data-shifttext');
			var date = $(this).find('.shiftInfo').attr('data-date');
			var timeId = $(this).find('.shiftInfo').attr('data-timeid');
			
			$('.popup_appointment_date .btn.submit').removeClass('disable').attr({
				'data-shiftid': shiftid,
				'data-shifttext': shifttext,
				'data-date': date,
				'data-timeid': timeId
			});	
			
		});
		
		$('input[name="xClass"]').on('click', function() {
			var xClassVal = $(this).val();
			var xClassText = $(this).attr('data-text');
			var xClassIconFlag = $(this).attr('data-iconflag');
			
			var hasXclassVal = ($.cookie('xClassVal')) ? $.cookie('xClassVal') : null;
			var hasClassVal = ($.cookie('classVal')) ? $.cookie('classVal') : null;
			var hasTeamVal = ($.cookie('teamVal')) ? $.cookie('teamVal') : null;
			
			if(!xClassVal) {
				//alert('系統維護中');
				$('.popup_group, .popup_alert').fadeIn(400).find('h2').text('系統維護中');
				liff.closeWindow();
				setTimeout(function(){
					liff.closeWindow();
				},3000);
			} else {	
				$.ajax({ 
					url: "../action", 
					type: "POST",
					data: {
						action: 'xClassClickBySearch2', 
						xClassVal: xClassVal,
						token: '<?php echo $_SESSION['token']; ?>'
					}, 
					success: function(e){
						var obj = jQuery.parseJSON(e);
						
						if(obj.error == '0') {
							$('.classList').html(obj.tpl);
						}
						
						//cookSelected();
						if(!xClassIconFlag) {
							btnInit();
							$('input[name="class"]:checked').trigger('click');
						} else {
							btnInitForIcon();
							if(hasClassVal) 
								$('input[name="class"][value="'+hasClassVal+'"]').trigger('click');
							else
								$('input[name="class"]:checked').trigger('click');
							$(this).attr({'data-iconflag':false});
						}
						
					}
				});
			}	
		});
		
		//科目->團隊
		$('input[name="class"]').on('click', function() {
			$('.popup_appointment input[name="subject"]').val(null);
			var classVal = $(this).val();
			if(!classVal) {
				alert('系統維護中');
				liff.closeWindow();
			} else {	
				$.ajax({ 
					url: "../action", 
					type: "POST",
					data: {
						action: 'classClickBySearch', 
						classVal: classVal,
						token: '<?php echo $_SESSION['token']; ?>'
					}, 
					success: function(e){
						var obj = jQuery.parseJSON(e);
						
						if(obj.error == '0') {
							$('.teamList').html(obj.tpl);
							btnInit();
							cookSelected();
						}
						
					}
				});
			}	
		});
		
		//時段
		$('.popup_appointment input[name="date"]').click(function () {
			if(!$('.popup_appointment_date .btn.submit').hasClass('disable')) {
				$('.popup_appointment_date .btn.submit').addClass('disable');
			}
			$('.popup_appointment').hide();
			//console.log($('input[name="class"]:checked').val(), $('input[name="team"]:checked').val());
			var classId = $('input[name="class"]:checked').val();
			var teamId = $('input[name="team"]:checked').val();
			
			var classText = $('input[name="class"]:checked').attr('data-text');
			var teamText = $('input[name="team"]:checked').attr('data-text');
			if(!classId && !teamId) {
				return;
			}
			
			$.ajax({ 
				url: "../action", 
				type: "POST",
				data: {
					action: 'calendar', 
					classVal: classId,
					teamVal: teamId,
					token: '<?php echo $_SESSION['token']; ?>'
				}, 
				success: function(e){
					var obj = jQuery.parseJSON(e);
					
					if(obj.error == '0') {
						//console.log($('.popup_appointment_date').find('h2 .tit'));
						$('.popup_appointment_date').find('h2 .tit .showText').html(" "+classText+" "+teamText+"</span>");
						$('.content .mobile').html(obj.tpl).show();
						btnInit();
					}
					
				}
			});
			
			
			$('.popup_appointment_date').fadeIn(400);
		});
		
		//掛號按鈕
		$('.content .btn.manager').click(function () {
			var accept_name = $(this).parents('.tr').find('.accept_name').text();
			var accept_mobile = $(this).parents('.tr').find('.accept_mobile').text();
			var accept_birthday = $(this).parents('.tr').find('.accept_birthday').text();
			var lineid = $(this).attr('data-lineid');
			//alert($(this).attr('data-lineid'));
			$('.popup.popup_appointment').find('input[name="lineID"]').attr({'readonly': true}).val(lineid);
			//console.log(accept_name, accept_mobile, accept_birthday);
			$('.popup.popup_appointment').find('input[name="uname"]').attr({'readonly': true}).val(accept_name);
			$('.popup.popup_appointment').find('input[name="uname2"]').attr({'readonly': true}).val(accept_name);
			if(accept_birthday != '0000-00-00')
				$('.popup.popup_appointment').find('input[name="birthday"]').val(accept_birthday);
			$('.popup.popup_appointment').find('input[name="mobile"]').val(accept_mobile);
			
			$('.popup_group, .popup_appointment').fadeIn(400);
			
			var hasXclassVal = ($.cookie('xClassVal')) ? $.cookie('xClassVal') : null;
			var hasClassVal = ($.cookie('classVal')) ? $.cookie('classVal') : null;
			var hasTeamVal = ($.cookie('teamVal')) ? $.cookie('teamVal') : null;
			//console.log(hasClassVal);
			/*
			if(hasClassVal) {
				$('input[name="class"][value="'+hasClassVal+'"]').trigger('click');
			} else {
				$('input[name="class"]:checked').trigger('click');
			}
			*/
			if(hasXclassVal) 
				$('input[name="xClass"][value="'+hasXclassVal+'"]').attr({'data-iconflag':'true'}).trigger('click');
			else
				$('input[name="xClass"]:checked').trigger('click');
		});
		
		$('.popup_appointment_date .btn.submit').click(function () {
			
			var shiftid = $(this).attr('data-shiftid');
			var shifttext = $(this).attr('data-shifttext');
			var date = $(this).attr('data-date');
			var timeid = $(this).attr('data-timeid');
			
			if($(this).hasClass('disable') || (!shiftid && !shifttext && !date && !timeid)) {
				return;
			}
			$('.popup_appointment_date').hide();
			//console.log();
			$('.popup_appointment input[name="date"]').val(shifttext);
			//$('.popup_appointment input[name="date"]').val(date);
			$('.popup_appointment input[name="shiftId"]').val(shiftid);
			$('.popup_appointment input[name="timeId"]').val(timeid);
		});
		var tpl;
		jQuery.validator.addMethod("string", function (value, element) {
			return this.optional(element) || /^[\u0391-\uFFE5\w]+$/.test(value);
		}, "不允許包含特殊符号!"); 
		jQuery.validator.addMethod("isMobile", function(value, element) {  
			var length = value.length;  
			var mobile = /^09[0-9]{8}$/;  
			return this.optional(element) || (length == 10 && mobile.test(value));  
		}, "請正確填寫手機號碼");  
		$('#formContent').validate({
			errorElement: 'p',
			submitHandler: function(form) {
				$.ajax({ 
					url: "../action", 
					type: "POST",
					data: $("#formContent").serialize(), 
					success: function(e){
						var obj = jQuery.parseJSON(e);
						var output = '';
						$.each(obj, function(k,v) {
							output += k + ': ' + v +'; ';
						});
						if(obj.error != '0') {
							//alert(obj.message);
							$('.popup_appointment').hide();
							$('.popup_group, .popup_alert').fadeIn(400).find('h2').text(obj.message);
							setTimeout(function(){
								$('.popup_group .popup, .popup_group').fadeOut(400);
							},1000);
							return;
						}
						if(obj.error == '0') {
							console.log(obj);
							var registerDate = obj.subject;
				
							tpl = "<div class=\"info\">";
							tpl += "	<div class=\"row\">";
							
						if($(form).find("[name='person']").val() == '0') {	
							tpl += "		<div class=\"name\">"+$(form).find('#username2').val()+"</div>";
						} else {
							tpl += "		<div class=\"name\">"+$(form).find('#username').val()+"</div>";
						}
							
							//tpl += "		<div class=\"name\">"+$(form).find('#username').val()+"</div>";
							tpl += "		<div class=\"value\">電話："+$(form).find('#mobile').val()+" <br> 生日："+$(form).find('#birthday').val()+"</div>";
							tpl += "	</div>";
							tpl += "</div>";
							tpl += "<div class=\"row\">";
							tpl += "	<div class=\"name\">類型</div>";
						if($(form).find("[name='person']").val() == '0') {
							tpl += "	<div class=\"value\">非本人預約</div>";
						} else {
							tpl += "	<div class=\"value\">本人預約</div>";
						}	
							tpl += "</div>";
							tpl += "<div class=\"row\">";
							tpl += "	<div class=\"name\">項目</div>";
							tpl += "	<div class=\"value\">"+obj.teamSubject+"</div>";
							tpl += "</div>";
							tpl += "<div class=\"row\">";
							tpl += "	<div class=\"name\">設計師</div>";
							tpl += "	<div class=\"value\">"+obj.classSubject+"</div>";
							tpl += "</div>";
							tpl += "<div class=\"row\">";
							tpl += "	<div class=\"name\">日期</div>";
							tpl += "	<div class=\"value\">"+registerDate.substr(0, 13)+"</div>";
							tpl += "</div>";
							tpl += "<div class=\"row\">";
							tpl += "	<div class=\"name\">到店時間</div>";
							//tpl += "	<div class=\"value\">"+registerDate.substr(14)+" "+obj.ordernum+"號</div>";
							tpl += "	<div class=\"value\">"+registerDate.substr(14, 5)+"</div>";
							tpl += "</div>";
							tpl += "<a href=\"./booking.php\" class=\"btn submit confirm\">確認</a>";
							//console.log(tpl);
							
							$('.popup_appointment').hide();
							$('.popup_group, .popup_info').find('.content.registerInfoList').html(tpl).fadeIn(400);
							$('.popup_group, .popup_info').fadeIn(400);
							
						}	
					}
				});		
			},
			rules: {
				uname: {
					required: true,
					minlength: 2,
					remote: {
						url: '../chekChiness.php',
						type: "post",
						data: {
							type: 1,
							username: function() {
								return $('#username').val();
							}
						}
					}
				}
			},
			messages: {
				uname: {
					remote: '請輸入中文姓名'
				}
			}   
		});

		$('.popup_appointment .btn.submit.confirm').on('click', function(e) {
			var uname = $('.popup.popup_appointment').find('input[name="uname"]').val();
			
			$('input[name="timeId"]').val($('input[name="class"]:checked').val());
			$('input[name="teamXid"]').val($('input[name="team"]:checked').val());
			//alert($('input[name="team"]:checked').val());
			//var uname2_check = $('.popup.popup_appointment').find('input[name="uname2"]').val();
			//if(!uname2_check)
			$('.popup.popup_appointment').find('input[name="uname2"]').val(uname);
			e.preventDefault();
			$('#formContent').submit();
			return false;	
			
		});
		
		$('input[name="team"], input[name="class"]').on('click', function() {
			$('input[name="date"]').val(null);
		});
		
		$('.popup_appointment_date .close, .popup_appointment_date .back').click(function (e) {
			e.preventDefault();
			$('.popup_group, .popup_appointment_date').hide();
			$('.popup_group, .popup_appointment').fadeIn(100);
		});	
	})
</script>
</body>
</html>