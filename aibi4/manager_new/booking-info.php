<?php
	include_once("../control/includes/function.php");
    //unset($_SESSION['MyShopping']);
	include_once("./checkAgent.php");

	$urlInfo = parse_url($_SERVER['HTTP_REFERER']);
	if (empty($_SESSION['expire']) || $_SESSION['expire'] < time()) {
		unset($_SESSION[$_SESSION['token']]);
		$_SESSION['token'] = md5('aibi' . uniqid(microtime())); // create token (fast/sufficient) 
	}// create token (fast/sufficient) 
	$_SESSION['expire'] = time() + 900; // make session valid for next 15 mins
	$_SESSION['visitid'] = $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT'];
	$web_x_register_id = $_GET['id'];
	$searchType = ($_GET['search']) ? trim($_GET['search']) : 0;
	if(!$web_x_register_id) {
		die('aibi');
	}
	
	//if($_SESSION['Member2']['ID'] || $_SESSION['store_id'] == '-1') {
	if($_SESSION['store_id'] == '-1') {	
		$store_id = ($_POST['store_id']) ? $_POST['store_id'] : $xClassRow[0]['web_x_class_id'];	
		//$_SESSION['store_id'] = $store_id;
		$storeRange = 'all';
	} else if($_SESSION['store_id']) {
		$store_id = $_SESSION['store_id'];
		$storeRange = $store_id;
	} else {
		$store_id = $_SESSION['store_id'];
		$storeRange = $store_id;
	}
	
	$accept_name = (!$_SESSION["session_accept_name"]) ? trim($_POST["accept_name"]) : $_SESSION["session_accept_name"];
	$accept_birthday = (!$_SESSION["session_accept_birthday"]) ?  trim($_POST["accept_birthday"]) : $_SESSION["session_accept_birthday"];
	$accept_mobile = (!$_SESSION["session_accept_mobile"]) ?  trim($_POST["accept_mobile"]) : $_SESSION["session_accept_mobile"];
	
	$registerInfoSql = "
		SELECT 
			SQL_CALC_FOUND_ROWS a.*,
			b.web_shift_id,
			b.web_class_id,
			b.web_team_id,
			c.subject as classSubjct,
			d.subject as teamSubject,
			e.subject as teamXsubject,
			f.uname as funame,
			f.birthday as fbirthday,
			f.mobile as fmobile
		FROM 
			web_x_register a
		Left Join 
			web_shift b ON b.web_shift_id = a.web_shift_id
		Left Join 
			web_class c ON c.web_class_id = b.web_class_id 		
		Left Join 
			web_team d ON d.web_team_id = b.web_team_id
		Left Join 
			web_x_team e ON e.web_x_team_id = d.web_x_team_id
		Left Join 
			web_member f ON f.uname = a.order_name AND f.mobile = a.order_mobile	
		where 
			a.web_x_register_id = :web_x_register_id	
		Group BY
			a.web_x_register_id	
		ORDER BY 
			a.subject ASC,
			a.registerDate ASC
	";
	$excute = array(
		':web_x_register_id'	=> $web_x_register_id,
	);

	//$debug = new Helper();
	//echo $test = $debug::debugPDO($sql, $excute);
	$pdo = $pdoDB->prepare($registerInfoSql);
	$pdo->execute($excute);
	$registerInfoRow = $pdo->fetch(PDO::FETCH_ASSOC);
	
	//近期預約記錄
	$sql = "
		SELECT 
			SQL_CALC_FOUND_ROWS a.*,
			c.subject as classSubject,
			e.subject as teamXsubject,
			e.time as teamXtime,
			f.uname as funame,
			f.birthday as fbirthday,
			f.mobile as fmobile
		FROM 
			web_x_register a
		Left Join 
			web_class c ON c.web_class_id = a.web_time_id		
		Left Join 
			web_x_team e ON e.web_x_team_id = a.web_x_team_id
		Left Join 
			web_member f ON f.uname = a.order_name AND f.mobile = a.order_mobile	
		where 
			a.registerDate >= :registerDate 
		AND
			a.accept_name Like :accept_name
		AND
			a.lineID = :lineID
		AND
			a.order_mobile = :order_mobile
		Group BY
			a.web_x_register_id
		ORDER BY 
			a.subject ASC,
			a.registerDate ASC
		Limit 0, 5	
	";
	$excute = array(
		':registerDate'			=> date('Y-m-d'),
		':accept_name'        	=> '%'.$registerInfoRow['accept_name'].'%',
		':lineID'				=> $registerInfoRow['lineID'],
		':order_mobile'				=> $registerInfoRow['order_mobile'],
	);

	//$debug = new Helper();
	//echo $test = $debug::debugPDO($sql, $excute);
	$pdo = $pdoDB->prepare($sql);
	$pdo->execute($excute);
	$row3 = $pdo->fetchAll(PDO::FETCH_ASSOC);
	
	//過往看診紀錄
	$sql = "
		SELECT 
			SQL_CALC_FOUND_ROWS a.*,
			c.subject as classSubject,
			e.subject as teamXsubject,
			e.time as teamXtime,
			f.uname as funame,
			f.birthday as fbirthday,
			f.mobile as fmobile
		FROM 
			web_x_register a
		Left Join 
			web_class c ON c.web_class_id = a.web_time_id		
		Left Join 
			web_x_team e ON e.web_x_team_id = a.web_x_team_id
		Left Join 
			web_member f ON f.uname = a.order_name AND f.mobile = a.order_mobile	
		where 
			a.registerDate < :registerDate 
		AND
			a.accept_name Like :accept_name
		AND
			a.lineID = :lineID
		AND
			a.order_mobile = :order_mobile	
		ORDER BY 
			a.subject DESC,
			a.registerDate DESC
		Limit 0, 5	
	";
	$excute = array(
		':registerDate'			=> date('Y-m-d'),
		':accept_name'        	=> '%'.$registerInfoRow['accept_name'].'%',
		':lineID'				=> $registerInfoRow['lineID'],
		':order_mobile'				=> $registerInfoRow['order_mobile'],
	);

	//$debug = new Helper();
	//echo $test = $debug::debugPDO($sql, $excute);
	$pdo = $pdoDB->prepare($sql);
	$pdo->execute($excute);
	$row4 = $pdo->fetchAll(PDO::FETCH_ASSOC);
	
	$dayName = array(
		1 => '一', 2 => '二', 3 => '三', 4 => '四', 5 => '五', 6 => '六', 7 => '日',
	);
	
	$registerTypeText = array(1 => 'LINE', 2 => '電話', 3 => '現場');
?>
<!DOCTYPE html>
<html lang="en">
<?php include_once('head.php'); ?>
<body>
<div class="main">
    <?php include_once('header.php'); ?>
    <div class="content appointment_info">
        <ul class="menu">
			<li class="active">個人詳情</li>
        </ul>
        <div class="content">
            <div class="info">
                <!--<a href="appointment5.php?id=<?php echo $registerInfoRow['web_x_register_id'];?>&search=<?php echo $searchType; ?>" class="btn">編輯</a>-->
				<a href="#" class="btn manager">預約</a>
                <div class="row">
                    <div class="name"><span class="accept_name"><?php echo $registerInfoRow['accept_name']; ?></span></div>
                    <div class="value">電話：<span class="accept_mobile"><?php echo $registerInfoRow['order_mobile']; ?></span> <br> 生日：<span class="accept_birthday"><?php echo $registerInfoRow['accept_birthday']; ?></span></div>
                </div>
            </div>
            <h3>近期預約記錄</h3>
	<?php
		foreach($row3 as $rowKey3 => $rowVal3) {
			$person = ($rowVal3['self']) ? '本人預約' : '非本人預約';
	?>
            <div class="info2">
		<?php
			if($rowVal3['paymentstatus'] === '未報到') {
		?>	
                <!--<a href="#" class="btn manager" data-id="<?php echo $rowVal3['web_x_register_id']; ?>">管理</a>-->
		<?php
			}
		?>	
                <div class="flex">
                    <div>
                        <div class="row">
                            <div class="name">類型</div>
                            <div class="value"><?php echo $person; ?></div>
                        </div>
                        <div class="row">
                            <div class="name">項目</div>
                            <div class="value"><?php echo $rowVal3['teamXsubject']." ".$rowVal3['teamXtime']."HR"; ?></div>
                        </div>
                        <div class="row">
                            <div class="name">設計師</div>
                            <div class="value"><?php echo $rowVal3['classSubject']; ?></div>
                        </div>
                        <div class="row">
                            <div class="name">日期</div>
                            <div class="value"><?php echo $rowVal3['registerDate']; ?> (<?php echo $dayName[date('N', strtotime(str_replace('-', '/', $rowVal3['registerDate'])))]; ?>)</div>
                        </div>
                    </div>
                    <div>
                        <div class="row">
                            <div class="name">時間</div>
                            <div class="value">
								<?php 
									if($rowVal3['teamXtime'] > 1) {
										echo substr($rowVal3['subject'],16, 6).ceil(substr($rowVal3['subject'],22, 2)+($rowVal3['teamXtime']-1)).substr($rowVal3['subject'],24);
									} else {
										echo substr($rowVal3['subject'], 16);
									}	
								?>
							</div>
                        </div>
                        <div class="row">
                            <div class="name">預約方式</div>
                            <div class="value"><?php echo $registerTypeText[$rowVal3['registerType']]; ?></div>
                        </div>
                        <div class="row">
                            <div class="name">狀態</div>
                            <div class="value status"><?php echo $rowVal3['paymentstatus']; ?></div>
                        </div>
                    </div>
                </div>
            </div>
	<?php
		}
	?>	
            <h3>過往服務紀錄</h3>
	<?php
		foreach($row4 as $rowKey4 => $rowVal4) {
			$person = ($rowVal4['self']) ? '本人預約' : '非本人預約';
	?>		
            <div class="info2">
                <div class="flex">
                    <div>
                        <div class="row">
                            <div class="name">類型</div>
                            <div class="value"><?php echo $person; ?></div>
                        </div>
                        <div class="row">
                            <div class="name">項目</div>
                            <div class="value"><?php echo $rowVal4['teamXsubject']." ".$rowVal4['teamXtime']."HR"; ?></div>
                        </div>
                        <div class="row">
                            <div class="name">設計師</div>
                            <div class="value"><?php echo $rowVal4['classSubject']; ?></div>
                        </div>
                        <div class="row">
                            <div class="name">日期</div>
                            <div class="value"><?php echo $rowVal4['registerDate']; ?> (<?php echo $dayName[date('N', strtotime(str_replace('-', '/', $rowVal4['registerDate'])))]; ?>)</div>
                        </div>
                    </div>
                    <div>
                        <div class="row">
                            <div class="name">時間</div>
                            <div class="value">
							<?php 
								if($rowVal4['teamXtime'] > 1) {
									echo substr($rowVal4['subject'],16, 6).ceil(substr($rowVal4['subject'],22, 2)+($rowVal4['teamXtime']-1)).substr($rowVal4['subject'],24);
								} else {
									echo substr($rowVal4['subject'], 16);
								}	
							?>
							</div>
                        </div>
                        <div class="row">
                            <div class="name">預約方式</div>
                            <div class="value"><?php echo $registerTypeText[$rowVal4['registerType']]; ?></div>
                        </div>
                        <div class="row">
                            <div class="name">狀態</div>
                            <div class="value"><?php echo $rowVal4['paymentstatus']; ?></div>
                        </div>
                    </div>
                </div>
            </div>
	<?php
		}
	?>	
        </div>
    </div>
</div>
<div class="popup_group">

	<div class="popup popup_alert">
		<!--<a class="close" href="#"></a>-->
		<h2>報到管理</h2>
		<div></div>
		<a href="#" class="btn submit cofirm active">確認</a>
	</div>
	<div class="popup popup_info">
        <!--<a class="close" href="#"></a>-->
        <h2>預約成功</h2>
        <div class="content registerInfoList">
            <div class="info">
                <div class="row">
                    <div class="name">陳小春</div>
                    <div class="value">電話：0933888222 <br> 生日：1987/05/27</div>
                </div>
            </div>
            <div class="row">
                <div class="name">類型</div>
                <div class="value">本人預約</div>
            </div>
            <div class="row">
                <div class="name">項目</div>
                <div class="value">內科</div>
            </div>
            <div class="row">
                <div class="name">設計師</div>
                <div class="value">2診王淑貞醫師</div>
            </div>
            <div class="row">
                <div class="name">日期</div>
                <div class="value">2019/05/27 (一)</div>
            </div>
            <div class="row">
                <div class="name">時間</div>
                <div class="value">上午診09:30-13:00 11號</div>
            </div>
            <a href="#" class="btn submit">確認</a>
        </div>
    </div>
	<div class="popup popup_appointment">
		<h2><a href="#" class="back"></a> 新增預約 <a class="close"></a></h2>
		<div class="content">
			<form name="form" id="formContent" method="POST">
				<div class="row">
					<div class="name">姓名</div>
					<div class="value">
						<input name="uname" id="username" type="text" placeholder="請輸入姓名" value="<?php echo $registerInfoRow['accept_name']; ?>" required="">
					</div>
				</div>
				<div class="row">
					<div class="name">生日</div>
					<div class="value">
						<input name="birthday" id="birthday" type="date" min="1929-01-01" max="<?php echo date('Y-m-d', strtotime('-1 day')); ?>" placeholder="請輸入生日" value="<?php echo $registerInfoRow['accept_birthday']; ?>" required="">
					</div>
				</div>
				<div class="row">
					<div class="name">手機</div>
					<div class="value">
						<input name="mobile" id="mobile" type="text" placeholder="請輸入手機" required="" isMobile="1" maxlength="10" minlength="10" value="<?php echo $registerInfoRow['order_mobile']; ?>">
					</div>
				</div>
				<div class="row">
					<div class="name">分店</div>
					<div class="value">
				<?php
					foreach($xClassRow as $xClassKey => $xClassVal) {
						if($storeRange != 'all') {
							if($xClassVal['web_x_class_id'] != $store_id) {
								continue;
							}
							$defaultChecked = 'checked="checked"';
						} else {	
							$defaultChecked = ($xClassKey == 0) ? 'checked="checked"' : null;
							if($xClassKey) {
								//continue;
							}
						}
				?>		
						<input type="radio" id="r2-<?php echo $xClassKey; ?>" value="<?php echo $xClassVal['web_x_class_id']; ?>" name="xClass" <?php echo $defaultChecked; ?> data-text="<?php echo $xClassVal['subject']; ?>">
						<label for="r2-<?php echo $xClassKey; ?>"><?php echo $xClassVal['subject']; ?></label>
				<?php
					}
				?>	
					</div>
				</div>
				<div class="row">
					<div class="name">設計師</div>
					<div class="classList value">
				<?php
					foreach($classRow as $classKey => $classVal) {
						$defaultChecked = ($classKey == 0) ? 'checked="checked"' : null;
				?>		
						<input type="radio" id="r<?php echo $classKey; ?>" value="<?php echo $classVal['web_class_id']; ?>" name="class" <?php echo $defaultChecked; ?> data-text="<?php echo $classVal['subject']; ?>">
						<label for="r<?php echo $classKey; ?>"><?php echo $classVal['subject']; ?></label>
				<?php
					}
				?>
					</div>
				</div>
				<div class="row">
					<div class="name">項目</div>
					<div class="teamList value">
				<?php
					$teamHaveClass = 0;
					foreach($teamRow as $teamKey => $teamVal) {
						/*
						$lineInfoAry = explode('#####', $teamVal['lineInfo']);
						
						foreach($lineInfoAry as $$lineInfoKey => $lineInfoVal) {
							$lineInfoRowAry[$teamVal['web_team_id']][] = explode('@#@', $lineInfoVal);
							//$listTimeByTeam[$teamVal][]
						}	
						
						$teamHaveClass = SearchMultidimensionalArray($lineInfoRowAry[$teamVal['web_team_id']], 2, $classRow[0]['web_class_id']);
						if(!$teamHaveClass) {
							continue;
						}
						*/
						$defaultChecked = ($teamKey == 0) ? 'checked="checked"' : null;
						
				?>	
						<input type="radio" id="r1-<?php echo $teamKey; ?>" value="<?php echo $teamVal['web_x_team_id']; ?>" name="team" <?php echo $defaultChecked; ?> data-text="<?php echo $teamVal['subject'].$teamVal['web_x_team_subject']; ?>">
						<label for="r1-<?php echo $teamKey; ?>"><?php echo $teamVal['subject'].$teamVal['web_x_team_subject']; ?></label>
				<?php
					}
				?>
					</div>
				</div>
				<div class="row">
					<div class="name">預約日期/到店時間</div>
					<div class="value">
						<input type="text" name="date" id="date" placeholder="選擇預約日期/到店時間" required="">
						<input type="hidden" name="timeId" id="timeId">
					</div>
				</div>
				<div class="row">
					<div class="name">預約方式</div>
					<div class="value">
				<?php
					if($isLine && 0) {
				?>	
						<input type="radio" id="t32" name="registerType" value="1" checked>
						<label for="t32">LINE</label>
						
						<input type="radio" id="t22" name="registerType" value="2">
						<label for="t22">電話</label>
						
						<input type="radio" id="t12" name="registerType" value="3">
						<label for="t12">現場</label>
				<?php
					} else {
				?>	
						<input type="radio" id="t22" name="registerType" value="2" checked>
						<label for="t22">電話</label>
						
						<input type="radio" id="t12" name="registerType" value="3">
						<label for="t12">現場</label>
				<?php
					}
				?>	
					</div>
				</div>
				
				<div class="row">
					<div class="name">備註</div>
					<div class="value">
						<textarea name="remark" placeholder="輸入文字"></textarea>
					</div>
				</div>
				
				<input class="form-control" type="hidden" name="teamXid" id="teamXid" value="" />
				<input class="form-control" type="hidden" name="lineID" value="<?php echo $row3[0]["lineID"]; ?>" />
				<input class="form-control" type="hidden" name="uname2" id="username2" value="" />
				<input class="form-control" type="hidden" name="shiftId" value="" />
				<input class="form-control" type="hidden" name="person" value="0" />
				<input class="form-control" type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>" />
				<input class="form-control" type="hidden" name="action" value="register" />
				<a href="#" class="btn submit confirm">確認預約</a>
			</form>
		</div>
	</div>
	<div class="popup popup_appointment_date">
        <a class="close" data-type="date" href="#"></a>
        <h2>
            <a href="#" data-type="date" class="back"></a>
            <div class="tit">篩選設定<span class="showText"></span></div>
            <div class="row" style="display:none;">
                <div class="name">顯示區間</div>
                <div class="value">
                    <input type="text" placeholder="" value="2019/05/13"> <span>~</span>  <input type="text" placeholder="" value="2019/05/13">
                </div>
            </div>
        </h2>
        <div class="content">
            <table class="pc">
                <tr>
                    <td></td>
                    <td>週一 05/13</td>
                    <td>週二 05/13</td>
                    <td>週三 05/13</td>
                    <td>週四 05/13</td>
                    <td>週五 05/13</td>
                    <td>週六 05/13</td>
                    <td>週日 05/13</td>
                </tr>
                <tr>
                    <td><span>上午</span><span>09:30</span><span class="line">|</span><span>13:00</span></td>
                    <td class="items"><span>預約人數</span><span>10 人</span></td>
                    <td>-</td>
                    <td class="items"><span>預約人數</span><span>6 人</span></td>
                    <td>-</td>
                    <td>-</td>
                    <td class="items"><span>預約人數</span><span>10 人</span></td>
                    <td class="items"><span>預約人數</span><span>6 人</span></td>
                </tr>
                <tr>
                    <td><span>中午</span><span>15:00</span><span class="line">|</span><span>18:00</span></td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td class="items"><span>預約人數</span><span>6 人</span></td>
                </tr>
                <tr>
                    <td><span>晚上</span><span>19:00</span><span class="line">|</span><span>21:30</span></td>
                    <td class="items"><span>預約人數</span><span class="red">10 人</span></td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td class="items"><span>預約人數</span><span class="blue">6 人</span></td>
                </tr>
            </table>
            <div style="width:700px; overflow:scroll;">
				<table class="mobile">
					<tr>
						<td></td>
						<td><span>上午</span><span>09:30-13:00</span></td>
						<td><span>中午</span><span>15:00-18:00</span></td>
						<td><span>晚上</span><span>19:00-21:30</span></td>
					</tr>
					<tr>
						<td><span>週一</span><span>05/13</span></td>
						<td class="items"><span>預約人數</span><span>10 人</span></td>
						<td>-</td>
						<td class="items"><span>預約人數</span><span>6 人</span></td>
					</tr>
					<tr>
						<td><span>週一</span><span>05/13</span></td>
						<td>-</td>
						<td>-</td>
						<td class="items"><span>預約人數</span><span>6 人</span></td>
					</tr>
					<tr>
						<td><span>週一</span><span>05/13</span></td>
						<td class="items"><span>預約人數</span><span class="red">10 人</span></td>
						<td>-</td>

						<td class="items"><span>預約人數</span><span class="blue">6 人</span></td>
					</tr>
				</table>
			</div>
            <a href="#" class="btn submit disable calendar">確認選擇</a>
        </div>
    </div>
</div>
<script>
	function active(obj) {
		if($(obj).hasClass('active')) {
			//alert($(obj).find('.popup_login').data('id'));
			console.log($(obj).parent().data('id'), $(obj).siblings('.selected').attr('data-status'));

			var id = $(obj).parent().data('id');
			var status = $(obj).siblings('.selected').attr('data-status');
			
			if(!id || !status) {
				return true;
			}
			
			//console.log($('.btn.manager[data-id='+id+']').hide(), $('.btn.manager[data-id='+id+']').siblings('.flex').find('.status').text(status));

			$.ajax({ 
				url: "../action", 
				type: "POST",
				data: {
					action: 'todayManager', 
					id: id,
					status: status,
					token: '<?php echo $_SESSION['token']; ?>'
				}, 
				success: function(e){
					var obj = jQuery.parseJSON(e);
					
					if(obj.error == '0') {
						
						$('.popup_group .popup, .popup_group').fadeOut(100);
						$('.popup_group, .popup_alert').fadeIn(400).find('h2').text(obj.message);
						setTimeout(function(){
							$('.btn.manager[data-id='+id+']').hide();
							$('.btn.manager[data-id='+id+']').siblings('.flex').find('.status').text(status)
							$('.popup_group .popup, .popup_group').fadeOut(400);
						},1000);

					}
					
				}
			});
			
			
		}
	}
	
	function btnInit() {
		$('.popup .close, .popup .back').on('click', function () {
			if($(this).attr('data-type') != 'date') {
				$('.popup_group .popup, .popup_group').fadeOut(400);
			}	
		});
		
		$('.content .btn.submit').click(function () {
			$('.popup_group, .popup_appointment').fadeIn(400);
		});
		/*
		$('.popup_appointment .btn.submit').click(function () {
			$('.popup_appointment').hide();
			$('.popup_group, .popup_info').fadeIn(400);
		});
		*/
		$('.popup_info .btn.submit').click(function () {
			$('.popup_group .popup_info, .popup_group').fadeOut(400);
		});
	
		$('.popup_appointment_date .items').click(function () {
			$('.popup_appointment_date .items').removeClass('selected');
			$(this).addClass('selected');
			$('.popup_appointment_date .btn.submit').removeClass('disable');
			var shiftid = $(this).find('.shiftInfo').attr('data-shiftid');
			var shifttext = $(this).find('.shiftInfo').attr('data-shifttext');
			var date = $(this).find('.shiftInfo').attr('data-date');
			var timeId = $(this).find('.shiftInfo').attr('data-timeid');
			
			$('.popup_appointment_date .btn.submit').removeClass('disable').attr({
				'data-shiftid': shiftid,
				'data-shifttext': shifttext,
				'data-date': date,
				'data-timeid': timeId
			});
		});
		
		$('input[name="team"], input[name="class"]').on('click', function() {
			$('input[name="date"]').val(null);
		})
		
		$('input[name="class"]').on('click', function() {
			var classVal = $(this).val();
			if(!classVal) {
				//alert('系統維護中');
				$('.popup_group, .popup_alert').fadeIn(400).find('h2').text('系統維護中');
				liff.closeWindow();
				setTimeout(function(){
					liff.closeWindow();
				},3000);
			} else {	
				$.ajax({ 
					url: "../action", 
					type: "POST",
					data: {
						action: 'classClickBySearch', 
						classVal: classVal,
						token: '<?php echo $_SESSION['token']; ?>'
					}, 
					success: function(e){
						var obj = jQuery.parseJSON(e);
						
						if(obj.error == '0') {
							$('.teamList').html(obj.tpl);
						}
						
					}
				});
			}	
		});
	}

	$(function () {
		$('.manager').click(function () {
			
			if(!$(this).hasClass('calendar') && !$(this).hasClass('confirm')) {
				var hasXclassVal = ($.cookie('xClassVal')) ? $.cookie('xClassVal') : null;
				var hasClassVal = ($.cookie('classVal')) ? $.cookie('classVal') : null;
				var hasTeamVal = ($.cookie('teamVal')) ? $.cookie('teamVal') : null;
				//console.log(hasClassVal);
				/*
				if(hasClassVal) {
					$('input[name="class"][value="'+hasClassVal+'"]').trigger('click');
				} else {
					$('input[name="class"]:checked').trigger('click');
				}
				*/
				if(hasXclassVal) 
					$('input[name="xClass"][value="'+hasXclassVal+'"]').attr({'data-iconflag':'true'}).trigger('click');
				else
					$('input[name="xClass"]:checked').trigger('click');
			}	
			
			$('.popup_group, .popup_appointment').fadeIn(400);
		});
		
		$('.popup_appointment_date .items').click(function () {
			$('.popup_appointment_date .items').removeClass('selected');
			$(this).addClass('selected');
			var shiftid = $(this).find('.shiftInfo').attr('data-shiftid');
			var shifttext = $(this).find('.shiftInfo').attr('data-shifttext');
			var date = $(this).find('.shiftInfo').attr('data-date');
			var timeId = $(this).find('.shiftInfo').attr('data-timeid');
			
			$('.popup_appointment_date .btn.submit').removeClass('disable').attr({
				'data-shiftid': shiftid,
				'data-shifttext': shifttext,
				'data-date': date,
				'data-timeid': timeId
			});	
			
		});
		
		$('input[name="xClass"]').on('click', function() {
			var xClassVal = $(this).val();
			var xClassText = $(this).attr('data-text');
			var xClassIconFlag = $(this).attr('data-iconflag');
			
			var hasXclassVal = ($.cookie('xClassVal')) ? $.cookie('xClassVal') : null;
			var hasClassVal = ($.cookie('classVal')) ? $.cookie('classVal') : null;
			var hasTeamVal = ($.cookie('teamVal')) ? $.cookie('teamVal') : null;
			
			if(!xClassVal) {
				//alert('系統維護中');
				$('.popup_group, .popup_alert').fadeIn(400).find('h2').text('系統維護中');
				liff.closeWindow();
				setTimeout(function(){
					liff.closeWindow();
				},3000);
			} else {	
				$.ajax({ 
					url: "../action", 
					type: "POST",
					data: {
						action: 'xClassClickBySearch2', 
						xClassVal: xClassVal,
						token: '<?php echo $_SESSION['token']; ?>'
					}, 
					success: function(e){
						var obj = jQuery.parseJSON(e);
						
						if(obj.error == '0') {
							$('.classList').html(obj.tpl);
						}
						
						//cookSelected();
						if(!xClassIconFlag) {
							btnInit();
							$('input[name="class"]:checked').trigger('click');
						} else {
							btnInitForIcon();
							if(hasClassVal) 
								$('input[name="class"][value="'+hasClassVal+'"]').trigger('click');
							else
								$('input[name="class"]:checked').trigger('click');
							$(this).attr({'data-iconflag':false});
						}
						
					}
				});
			}	
		});
		
		//科目->團隊
		$('input[name="class"]').on('click', function() {
			$('.popup_appointment input[name="subject"]').val(null);
			var classVal = $(this).val();
			if(!classVal) {
				$('.popup_group, .popup_alert').fadeIn(400).find('h2').text('系統維護中');
				liff.closeWindow();
				setTimeout(function(){
					liff.closeWindow();
				},3000);
			} else {	
				$.ajax({ 
					url: "../action", 
					type: "POST",
					data: {
						action: 'classClickBySearch', 
						classVal: classVal,
						token: '<?php echo $_SESSION['token']; ?>'
					}, 
					success: function(e){
						var obj = jQuery.parseJSON(e);
						
						if(obj.error == '0') {
							$('.teamList').html(obj.tpl);
							btnInit();
							cookSelected();
						}
						
					}
				});
			}	
		});
		
		//時段
		$('.popup_appointment input[name="date"]').click(function () {
			if(!$('.popup_appointment_date .btn.submit').hasClass('disable')) {
				$('.popup_appointment_date .btn.submit').addClass('disable');
			}
			$('.popup_appointment').hide();
			//console.log($('input[name="class"]:checked').val(), $('input[name="team"]:checked').val());
			var classId = $('input[name="class"]:checked').val();
			var teamId = $('input[name="team"]:checked').val();
			
			var classText = $('input[name="class"]:checked').attr('data-text');
			var teamText = $('input[name="team"]:checked').attr('data-text');
			if(!classId && !teamId) {
				return;
			}
			
			$.ajax({ 
				url: "../action", 
				type: "POST",
				data: {
					action: 'calendar', 
					classVal: classId,
					teamVal: teamId,
					token: '<?php echo $_SESSION['token']; ?>'
				}, 
				success: function(e){
					var obj = jQuery.parseJSON(e);
					
					if(obj.error == '0') {
						//console.log($('.popup_appointment_date').find('h2 .tit'));
						$('.popup_appointment_date').find('h2 .tit .showText').html(" "+classText+" "+teamText+"</span>");
						$('.content .mobile').html(obj.tpl).show();
						btnInit();
					}
					
				}
			});
			
			
			$('.popup_appointment_date').fadeIn(400);
		});
		
		//掛號按鈕
		$('.content .btn.manager').click(function () {
			var accept_name = $(this).parents('.info').find('.accept_name').text();
			var accept_mobile = $(this).parents('.info').find('.accept_mobile').text();
			var accept_birthday = $(this).parents('.info').find('.accept_birthday').text();
			
			//console.log(accept_name, accept_mobile, accept_birthday);
			$('.popup.popup_appointment').find('input[name="uname"]').attr({'readonly': true}).val(accept_name);
			$('.popup.popup_appointment').find('input[name="uname2"]').attr({'readonly': true}).val(accept_name);
			if(accept_birthday != '0000-00-00')
				$('.popup.popup_appointment').find('input[name="birthday"]').val(accept_birthday);
			$('.popup.popup_appointment').find('input[name="mobile"]').val(accept_mobile);
			
			$('.popup_group, .popup_appointment').fadeIn(400);
			
			var hasXclassVal = ($.cookie('xClassVal')) ? $.cookie('xClassVal') : null;
			var hasClassVal = ($.cookie('classVal')) ? $.cookie('classVal') : null;
			var hasTeamVal = ($.cookie('teamVal')) ? $.cookie('teamVal') : null;
			//console.log(hasClassVal);
			/*
			if(hasClassVal) {
				$('input[name="class"][value="'+hasClassVal+'"]').trigger('click');
			} else {
				$('input[name="class"]:checked').trigger('click');
			}
			*/
			if(hasXclassVal) 
				$('input[name="xClass"][value="'+hasXclassVal+'"]').attr({'data-iconflag':'true'}).trigger('click');
			else
				$('input[name="xClass"]:checked').trigger('click');
		});
		
		$('.popup_appointment_date .btn.submit').click(function () {
			var shiftid = $(this).attr('data-shiftid');
			var shifttext = $(this).attr('data-shifttext');
			var date = $(this).attr('data-date');
			var timeid = $(this).attr('data-timeid');
			
			if($(this).hasClass('disable') || (!shiftid && !shifttext && !date && !timeid)) {
				return;
			}
			$('.popup_appointment_date').hide();
			//console.log();
			$('.popup_appointment input[name="date"]').val(shifttext);
			//$('.popup_appointment input[name="date"]').val(date);
			$('.popup_appointment input[name="shiftId"]').val(shiftid);
			$('.popup_appointment input[name="timeId"]').val(timeid);
		});
		var tpl;
		jQuery.validator.addMethod("string", function (value, element) {
			return this.optional(element) || /^[\u0391-\uFFE5\w]+$/.test(value);
		}, "不允許包含特殊符号!"); 
		jQuery.validator.addMethod("isMobile", function(value, element) {  
			var length = value.length;  
			var mobile = /^09[0-9]{8}$/;  
			return this.optional(element) || (length == 10 && mobile.test(value));  
		}, "請正確填寫手機號碼");  
		$('#formContent').validate({
			errorElement: 'p',
			submitHandler: function(form) {
				$.ajax({ 
					url: "../action", 
					type: "POST",
					data: $("#formContent").serialize(), 
					success: function(e){
						var obj = jQuery.parseJSON(e);
						var output = '';
						$.each(obj, function(k,v) {
							output += k + ': ' + v +'; ';
						});
						if(obj.error != '0') {
							//alert(obj.message);
							$('.popup_group .popup, .popup_group').fadeOut(100);
							$('.popup_group, .popup_alert').fadeIn(400).find('h2').text(obj.message);
							setTimeout(function(){
								$('.popup_group, .popup_alert').find('h2').text('報到管理');
								$('.popup_group .popup, .popup_group').fadeOut(400);
							},1000);
							return;
						}
						if(obj.error == '0') {
							console.log(obj);
							var registerDate = obj.subject;
				
							tpl = "<div class=\"info\">";
							tpl += "	<div class=\"row\">";
							
						if($(form).find("[name='person']").val() == '0') {	
							tpl += "		<div class=\"name\">"+$(form).find('#username2').val()+"</div>";
						} else {
							tpl += "		<div class=\"name\">"+$(form).find('#username').val()+"</div>";
						}
							
							//tpl += "		<div class=\"name\">"+$(form).find('#username').val()+"</div>";
							tpl += "		<div class=\"value\">電話："+$(form).find('#mobile').val()+" <br> 生日："+$(form).find('#birthday').val()+"</div>";
							tpl += "	</div>";
							tpl += "</div>";
							tpl += "<div class=\"row\">";
							tpl += "	<div class=\"name\">類型</div>";
						if($(form).find("[name='person']").val() == '0') {
							tpl += "	<div class=\"value\">非本人預約</div>";
						} else {
							tpl += "	<div class=\"value\">本人預約</div>";
						}	
							tpl += "</div>";
							tpl += "<div class=\"row\">";
							tpl += "	<div class=\"name\">項目</div>";
							tpl += "	<div class=\"value\">"+obj.teamSubject+"</div>";
							tpl += "</div>";
							tpl += "<div class=\"row\">";
							tpl += "	<div class=\"name\">設計師</div>";
							tpl += "	<div class=\"value\">"+obj.classSubject+"</div>";
							tpl += "</div>";
							tpl += "<div class=\"row\">";
							tpl += "	<div class=\"name\">日期</div>";
							tpl += "	<div class=\"value\">"+registerDate.substr(0, 13)+"</div>";
							tpl += "</div>";
							tpl += "<div class=\"row\">";
							tpl += "	<div class=\"name\">到店時間</div>";
							tpl += "	<div class=\"value\">"+registerDate.substr(14, 5)+"</div>";
							tpl += "</div>";
							tpl += "<a href=\"./booking.php\" class=\"btn submit confirm\">確認</a>";
							//console.log(tpl);
							
							$('.popup_appointment').hide();
							$('.popup_group, .popup_info').find('.content.registerInfoList').html(tpl).fadeIn(400);
							$('.popup_group, .popup_info').fadeIn(400);
							
						}	
					}
				});		
			},
			rules: {
				uname: {
					required: true,
					minlength: 2,
					remote: {
						url: '../chekChiness.php',
						type: "post",
						data: {
							type: 1,
							username: function() {
								return $('#username').val();
							}
						}
					}
				}
			},
			messages: {
				uname: {
					remote: '請輸入中文姓名'
				}
			}   
		});

		$('.popup_appointment .btn.submit.confirm').on('click', function(e) {
			var uname = $('.popup.popup_appointment').find('input[name="uname"]').val();
			
			$('input[name="timeId"]').val($('input[name="class"]:checked').val());
			$('input[name="teamXid"]').val($('input[name="team"]:checked').val());
			//alert($('input[name="team"]:checked').val());
			//var uname2_check = $('.popup.popup_appointment').find('input[name="uname2"]').val();
			//if(!uname2_check)
				$('.popup.popup_appointment').find('input[name="uname2"]').val(uname);
			e.preventDefault();
			$('#formContent').submit();
			return false;	
			
		});
		
		$('input[name="team"], input[name="class"]').on('click', function() {
			$('input[name="date"]').val(null);
		});
		
		
		$('.popup .close').on('click', function () {
			$('.popup_group .popup, .popup_group').fadeOut(400);
			$('.popup_login .btn.items').removeClass('selected');
			$('.popup_login .btn.submit').removeClass('active').addClass('disable');
		});

		$('.btn.manager').click(function () {
			$('.popup_group, .popup_login').fadeIn(400);
		});

		$('.popup_login .btn.items').click(function () {
			$('.popup_login .btn.items').removeClass('selected');
			$(this).addClass('selected');
			$('.popup_login .btn.submit').removeClass('disable').addClass('active');
		});
		
		$('.popup_appointment_date .close, .popup_appointment_date .back').click(function (e) {
			e.preventDefault();
			$('.popup_group, .popup_appointment_date').hide();
			$('.popup_group, .popup_appointment').fadeIn(100);
		});	
	})
</script>
</body>
</html>