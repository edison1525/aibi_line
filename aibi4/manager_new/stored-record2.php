<?php
	include_once("../control/includes/function.php");
    //unset($_SESSION['MyShopping']);
	include_once("./checkAgent.php");
	
	if (empty($_SESSION['expire']) || $_SESSION['expire'] < time()) {
		unset($_SESSION[$_SESSION['token']]);
		$_SESSION['token'] = md5('aibi' . uniqid(microtime())); // create token (fast/sufficient) 
	}// create token (fast/sufficient) 
	$_SESSION['expire'] = time() + 900; // make session valid for next 15 mins
	$_SESSION['visitid'] = $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT'];
	
	$web_member_id= intval($_GET["web_member_id"]) ? intval($_GET["web_member_id"]) : null;
	
	$refererInfo = parse_url($_SERVER['HTTP_REFERER']);
	
	if(!$refererInfo || ($refererInfo['path']) != '/manager_new/stored-record.php') {
		RunJs("./stored-search.php");
	}
	
	if(!$web_member_id) {
		RunJs("./stored-search.php");
	}
	
	//if($_SESSION['Member2']['ID'] || $_SESSION['store_id'] == '-1') {
	if($_SESSION['store_id'] == '-1') {	
		$store_id = ($_POST['store_id']) ? $_POST['store_id'] : $xClassRow[0]['web_x_class_id'];	
		//$_SESSION['store_id'] = $store_id;
		$storeRange = 'all';
	} else if($_SESSION['store_id']) {
		$store_id = $_SESSION['store_id'];
		$storeRange = $store_id;
	} else {
		$store_id = $_SESSION['store_id'];
		$storeRange = $store_id;
	}
	
	$sql = "
		SELECT 
			web_x_order.*,
			web_order.subject as prodSubjects,
			web_order.web_order_id as web_order_id,
			web_order.web_x_order_ordernum as web_x_order_ordernum,
			web_order.web_product_id as web_order_product_id,
			web_order.dimension as web_order_dimension,
			web_product.web_product_id as web_product_product_id,
			web_product.web_x_product_id as web_product_x_product_id,
			web_product.totalCount as web_product_totalCount,
			web_x_product.web_x_product_id as web_x_product_x_product_id,
			web_x_product.web_xx_product_id as web_x_product_xx_product_id,
			(select subject from web_x_class where web_x_class.web_x_class_id = web_x_order.store_id) as xClassSubject
		from 
			web_x_order
		Left Join
			web_order as web_order
		On
			web_order.web_x_order_ordernum = web_x_order.ordernum
		Left Join
			web_product
		On
			web_product.web_product_id = web_order.web_product_id
		Left Join
			web_x_product
		On
			web_x_product.web_x_product_id = web_product.web_x_product_id
		WHERE 
			web_x_order.web_member_id = :web_member_id
		AND
			web_x_order.paymentstatus = '付款成功'	
		AND
			web_x_product.web_xx_product_id IN (2,3)
		GROUP BY
			web_x_order.web_x_order_id
		ORDER BY
			web_x_order.cdate DESC	
	";
	
	$excute = array(
		':web_member_id'		=> $web_member_id,
	);
	//$debug = new Helper();
	//echo $test = $debug::debugPDO($sql, $excute);
	$pdo = $pdoDB->prepare($sql);
	$pdo->execute($excute);
	$row3 = $pdo->fetchAll(PDO::FETCH_ASSOC);

	/*
	$sql = "
		SELECT 
			web_x_order.*,
			web_order.subject as prodSubjects,
			web_order.web_order_id as web_order_id,
			web_order.web_x_order_ordernum as web_x_order_ordernum,
			web_order.web_product_id as web_order_product_id,
			web_order.dimension as web_order_dimension,
			web_product.web_product_id as web_product_product_id,
			web_product.web_x_product_id as web_product_x_product_id,
			web_product.totalCount as web_product_totalCount,
			web_product.Covers as web_product_Covers,
			web_product.useEdate as web_product_useEdate,
			web_x_product.web_x_product_id as web_x_product_x_product_id,
			web_x_product.web_xx_product_id as web_x_product_xx_product_id,
			(select subject from web_x_class where web_x_class.web_x_class_id = web_x_order.store_id) as xClassSubject
		from 
			web_x_order
		Left Join
			web_order as web_order
		On
			web_order.web_x_order_ordernum = web_x_order.ordernum
		Left Join
			web_product
		On
			web_product.web_product_id = web_order.web_product_id
		Left Join
			web_x_product
		On
			web_x_product.web_x_product_id = web_product.web_x_product_id
		WHERE 
			web_x_order.from_ordernum = :ordernum
		AND
			web_x_order.paymentstatus = '付款成功'	
		AND
			web_x_order.order_type = 2
		AND
			web_x_product.web_xx_product_id = 3	
		ORDER BY
			web_x_order.cdate DESC
	";
	
	$excute = array(
		':ordernum'		=> $web_member_id,
	);
	//$debug = new Helper();
	//echo $test = $debug::debugPDO($sql, $excute);
	$pdo = $pdoDB->prepare($sql);
	$pdo->execute($excute);
	$row4 = $pdo->fetchAll(PDO::FETCH_ASSOC);
	*/
?>
<!DOCTYPE html>
<html lang="en">
<?php include_once('head.php'); ?>
<body>
<div class="main">
    <?php include_once('header.php'); ?>
    <div class="content coupon-record2">
        <a class="back" style="cursor:pointer;"></a>
		<form name="form" id="backForm" action="stored-record.php" method="POST">
			<input type="hidden" value="<?php echo $_SESSION["session_uname"]; ?>" name="uname" />
			<input type="hidden" value="<?php echo $_SESSION["session_birthday"]; ?>" name="birthday" />
			<input type="hidden" value="<?php echo $_SESSION["session_mobile"]; ?>" name="mobile" />
		</form>
        <h2>消費管理</h2>
        <ul class="menu">
            <li><a href="stored.php">收款管理</a></li>
            <li class="active">消費管理</li>
        </ul>
        <div class="filter">
            <ul>
                <li><a>購買/使用 紀錄</a></li>
            </ul>
        </div>
        <div class="table">
	<?php
		foreach($row3 as $key3 => $val3) {
			if($val3['order_type'] != '2') {
			$buyState = ($val3['states'] == '取消') ? '已取消' : $val3['paymentstatus'];
	?>	
            <div class="tr">
                <!--<a href="#" class="btn btn-popup">取消</a>-->
                <span class="buy">買</span>
                <div class="color2">狀態：<?php echo $buyState; ?></div>
                <div class="row">
                    <div class="name">日期</div>
                    <div class=""><?php echo $val3['cdate']; ?></div>
                </div>
                <div class="row">
                    <div class="name">儲值金額</div>
                    <div class=""><?php echo number_format($val3['web_order_dimension']); ?>元</div>
                </div>
                <div class="row">
                    <div class="name">收款金額</div>
                    <div class=""><?php echo number_format($val3['total']); ?>元</div>
                </div>
                <div class="row">
                    <div class="name">店別</div>
                    <div class=""><?php echo $val3['xClassSubject']; ?></div>
                </div>
                <div class="row">
                    <div class="name">經手人</div>
                    <div class=""><?php echo $val3['editUser']; ?></div>
                </div>
		<?php
			if($buyState == '已取消') {
		?>		
				<div class="row">
                    <div class="name">取消人員</div>
                    <div class=""><?php echo $val3['cancelUser']; ?></div>
                </div>
				<div class="row">
                    <div class="name">取消時間</div>
                    <div class=""><?php echo $val3['cancelDateTime']; ?></div>
                </div>
				<div class="row">
                    <div class="name">取消原因</div>
                    <div class=""><?php echo $val3['cancelReason']; ?></div>
                </div>
				<div class="row">
                    <div class="name">備註</div>
                    <div class=""><?php echo $val3['cancelRemark']; ?></div>
                </div>
		<?php
			}
		?>		
            </div>
	<?php
		//}
	?>	
	<?php
			} else {
		//foreach($row4 as $key4 => $val4) {
			$useState = ($val3['states'] == '取消') ? '已取消' : '已使用';
	?>		
            <div class="tr">
			<?php
				if($useState == '已使用') {
			?>
                <a href="#" data-xorderid="<?php echo $val3['web_x_order_id'];?>" class="btn btn-popup">取消</a>
			<?php
				}
			?>	
                <span class="use">用</span>
                <div class="color">狀態：<?php echo $useState; ?></div>
                <div class="row">
                    <div class="name">日期</div>
                    <div class=""><?php echo $val3['cdate']; ?></div>
                </div>
                <div class="row">
                    <div class="name">消費項目</div>
                    <div class="">
						<?php 
							//echo $val3['prodSubjects']; 
							$sql = "
								Select 
									a.web_order_id,
									a.subject,
									a.price,
									a.web_product_id,
									c.subject as csubject
								From 
									web_order a
								Left Join
									web_product b
								On
									b.web_product_id = a.web_product_id
								Left Join
									web_x_product c
								On
									c.web_x_product_id = b.web_x_product_id	
								Where 
									a.web_x_order_ordernum = :web_x_order_ordernum
							";
							$excute = array(
								':web_x_order_ordernum'        => $val3['web_x_order_ordernum'],
							);
							$pdo = $pdoDB->prepare($sql);
							$pdo->execute($excute);
							$orderRow = $pdo->fetchAll(PDO::FETCH_ASSOC);
							$prodSubjectAry = array();
							foreach($orderRow as $key => $orderVal) {
								$prodSubjectAry[] = $orderVal['csubject']."-".$orderVal['subject']." ".$orderVal['price'];
							}
							echo implode('，', $prodSubjectAry);
						?>
					</div>
                </div>
                <div class="row">
                    <div class="name">收款金額</div>
                    <div class=""><?php echo $val3['total']; ?>元</div>
                </div>
                <div class="row">
                    <div class="name">店別</div>
                    <div class=""><?php echo $val3['xClassSubject']; ?></div>
                </div>
                <div class="row">
                    <div class="name">經手人</div>
                    <div class=""><?php echo $val3['editUser']; ?></div>
                </div>
		<?php
			if($useState == '已取消') {
		?>		
				<div class="row">
                    <div class="name">取消人員</div>
                    <div class=""><?php echo $val3['cancelUser']; ?></div>
                </div>
				<div class="row">
                    <div class="name">取消時間</div>
                    <div class=""><?php echo $val3['cancelDateTime']; ?></div>
                </div>
				<div class="row">
                    <div class="name">取消原因</div>
                    <div class=""><?php echo $val3['cancelReason']; ?></div>
                </div>
				<div class="row">
                    <div class="name">備註</div>
                    <div class=""><?php echo $val3['cancelRemark']; ?></div>
                </div>
		<?php
			}
		?>	
            </div>
	<?php
			}
		}
	?>		
        </div>
    </div>
</div>
<div class="popup_group">
    <div class="popup popup_password">
        <a class="close" href="#"></a>
        <h2>請輸入密碼</h2>
		<form name="form" id="cancelForm" action="stored-record-cancel.php" method="POST">
			<input id="cancelPwd" name="cancelPwd" type="password" required="">
			<input class="form-control" type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>" />
			<input class="form-control" type="hidden" name="web_x_order_id" value="" />
			<input class="form-control" type="hidden" name="action" value="cancelPwdCheck" />
			<a class="btn submit">確認</a>
		</form>
    </div>
	<div class="popup popup_alert">
		<!--<a class="close" href="#"></a>-->
		<h2>票券管理</h2>
		<!--<a href="#" class="btn submit disable">確認</a>-->
	</div>
</div>
<script>
	$(function () {
		$('.btn-popup').click(function () {
			var web_x_order_id = $(this).attr('data-xorderid');
			if(web_x_order_id)  {
				$('#cancelPwd').val('');
				$('.popup_group, .popup_password').fadeIn(400).find('h2').text('請輸入密碼');
				$('.popup_group, .popup_group .popup').fadeIn(400);
				$('input[name="web_x_order_id"]').val(web_x_order_id);
			}	
		});
		
		$('#cancelForm').validate({
			errorElement: 'p',
			submitHandler: function(form) {
				$.ajax({ 
					url: "../action", 
					type: "POST",
					data: $("#cancelForm").serialize(), 
					success: function(e){
						var obj = jQuery.parseJSON(e);
						var output = '';
						$.each(obj, function(k,v) {
							output += k + ': ' + v +'; ';
						});
						
						if(obj.error != '0') {
							
							$('.popup_group .popup, .popup_group').hide();
							$('.popup_group, .popup_alert').fadeIn(400).find('h2').text(obj.message);
							
							setTimeout(function(){
								$('.popup_group .popup, .popup_group').fadeOut(400);
							},1000);
							
							return false;
						}
						if(obj.error == '0') {
							form.submit();
							$('.popup_group .popup, .popup_group').fadeOut(400);
						}
												
					}
				});
			}
		});
		
		$('.popup .close').on('click', function () {
			$('.popup_group .popup, .popup_group').fadeOut(400);
		});
		
		$('.popup_password .submit').click(function (e) {
			e.preventDefault();
			$('#cancelForm').submit();
			return false;
		});
		
		$('.back').click(function(e) {
			e.preventDefault();
			$('#backForm').submit();
			return false;
		});
		
	})
</script>
</body>
</html>
