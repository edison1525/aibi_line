<?php
	$agent = $_SERVER['HTTP_USER_AGENT'];
?>
	<div class="header">
        <h1><img src="../img/home_ligo_white@3x.png" height="28"></h1>
        <ul>
            <li><a href="booking.php">預約管理</a></li>
<?php
	if(!preg_match("#\bLine\b#", $agent)) {
?>		
            <li><a href="coupon.php">票券管理</a></li>
            <li><a href="stored.php">儲值管理</a></li>
			<li><a href="bonus.php">兌換管理</a></li>
			<li><a href="chat.php">顧客諮詢</a></li>
			<li><a href="logout.php">登出</a></li>
<?php
	}
?>	
        </ul>
    </div>	
	<script>
		$(function() {
			var l = new URL(window.location.href);
			//console.log(l.pathname.split("/"));
			var l_ary = l.pathname.split("/");
			
			$('.header').find('a').each(function(k,e) {
				if($(e).attr('href') == l_ary[l_ary.length - 1]) {
					$(e).parent().addClass('active').text($(e).text());
					$(e).remove('a');
					//console.log($(e).parent().addClass('active').text($(e).text()), $(e).remove('a'));
				}
			})
		})
	</script>