<?php
	include_once("../control/includes/function.php");
    //unset($_SESSION['MyShopping']);
	include_once("./checkAgent.php");
	
	if (empty($_SESSION['expire']) || $_SESSION['expire'] < time()) {
		unset($_SESSION[$_SESSION['token']]);
		$_SESSION['token'] = md5('aibi' . uniqid(microtime())); // create token (fast/sufficient) 
	}	
	$_SESSION['expire'] = time() + 900; // make session valid for next 15 mins
	$_SESSION['visitid'] = $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT'];
	
	$todayDay = date('N');
	$endDay = strtotime('+'.ceil(14-14).' day');
	$startDay = strtotime('-2 day', $endDay);
	
	$searchDay = ($_POST['searchDay']) ? strtotime($_POST['searchDay']) :  strtotime(date('Y-m-d'));
	$status = ($_POST['status']) ? $_POST['status'] : '全部';
	$mobile = ($_POST['mobile']) ? $_POST['mobile'] : null;
	
	//unset($_SESSION['store_id']);
	
	//if($_SESSION['Member2']['ID'] || $_SESSION['store_id'] == '-1') {
	if($_SESSION['store_id'] == '-1') {	
		$store_id = ($_POST['store_id']) ? $_POST['store_id'] : $xClassRow[0]['web_x_class_id'];	
		//$_SESSION['store_id'] = $store_id;
		$storeRange = 'all';
	} else if($_SESSION['store_id']) {
		$store_id = $_SESSION['store_id'];
		$storeRange = $store_id;
	} else {
		$store_id = $_SESSION['store_id'];
		$storeRange = $store_id;
	}
	
	$storeSubject = SearchMultidimensionalAry($xClassRow, 'web_x_class_id', 'subject', $store_id);

	$sql = "
		SELECT 
			web_x_order.*
		from 
			web_x_order
		WHERE 
			web_x_order.cdate >= :registerDateS
		AND
			web_x_order.cdate <= :registerDateE
		AND
			web_x_order.order_type = :order_type
	";
	$sql .= "
		AND 
			web_x_order.store_id = :store_id
	";	
if($status == '全部') {	
	$sql .= "
		AND 1
	";	
} else if($status == '取消') {
	$sql .= "
		AND 
			web_x_order.states = :status
	";
} else {
	$sql .= "
		AND 
			web_x_order.paymentstatus = :status
	";
}
if($mobile) {	
	$sql .= "
		AND 
			web_x_order.order_mobile = :order_mobile
	";	
}	
	$sql .= "
		Order by
			web_x_order.cdate DESC, web_x_order.web_x_order_id ASC
	";
	
	$excute = array(
		':order_type'			=> 0,
		':registerDateS'		=> date('Y-m-d 00:00:00', $searchDay),
		':registerDateE'		=> date('Y-m-d 23:59:59', $searchDay),
	);
	if($status != '全部') {	
		$excute = ($status) ? $excute+array(':status' => $status) : $excute;
	}	
	$excute = ($mobile) ? $excute+array(':order_mobile' => $mobile) : $excute;
	$excute = ($store_id) ? $excute+array(':store_id' => $store_id) : $excute;	
	//$debug = new Helper();
	//echo $test = $debug::debugPDO($sql, $excute);
	$pdo = $pdoDB->prepare($sql);
	$pdo->execute($excute);
	$row3 = $pdo->fetchAll(PDO::FETCH_ASSOC);
/*	
	echo "<pre>";
	print_r($row3);
	echo "</pre>";
*/	
?>	
<!DOCTYPE html>
<html lang="en">
<?php include_once('head.php'); ?>
<script src="https://d.line-scdn.net/liff/1.0/sdk.js"></script>
<script>
	//init LIFF
	function initializeApp(data) {
		//取得QueryString
		let urlParams = new URLSearchParams(window.location.search);
		//顯示QueryString
		if(data.context.userId) {
			var userId = data.context.userId;
			$.ajax({ 
				url: "../action", 
				type: "POST",
				data: {action: 'userInfo', lineId: userId, token: '<?php echo $_SESSION['token']; ?>'}, 
				success: function(e){
					var obj = jQuery.parseJSON(e);
					
					if(obj.error == '0') {
						/*
						var output = '';
						$.each(obj, function(k,v) {
							output += k + ': ' + v +'; ';
						});
						alert(output);
						*/
						$('.popup_group, .popup_login').attr('data-adminid', obj.web_member_id);
					
					} 
					
				}
			});	
		}	
		
	}
</script>
<body>
<div class="main">
    <?php include_once('header.php'); ?>
    <div class="content coupon">
        <!--<a class="close"></a>-->
        <h2>票券管理</h2>
        <ul class="menu">
            <li class="active">收款管理</li>
            <li><a href="coupon-search.php">使用管理</a></li>
        </ul>
        <div class="filter">
			<input type="hidden" value="<?php echo $store_id; ?>" name="storeId" />
            <ul>
				<li><a id="storeName"><?php echo $storeSubject[$store_id]; ?></a></li>
                <li style="padding-left:40px;"><a><?php echo $status; ?></a></li>
                <li><a><?php echo date('Y/m/d', $searchDay); ?> <?php if(date('Y/m/d', $searchDay) == date('Y/m/d')) { ?>(今天)<?php } ?></a><!--<a><?php echo $mobile; ?></a>--></li>
                <li class="right"><a href="#" class="icon"></a></li>
            </ul>
        </div>
        <div class="table">
	<?php
		foreach($row3 as $key3 => $val3) {
			//GROUP_CONCAT(DISTINCT web_order.subject SEPARATOR ',') AS prodSubjects
			$sql = "
				SELECT 
					web_order.subject as prodSubjects,
					web_order.web_order_id as web_order_id,
					web_order.web_x_order_ordernum as web_x_order_ordernum,
					web_order.web_product_id as web_order_product_id,
					web_product.web_product_id as web_product_product_id,
					web_product.web_x_product_id as web_product_x_product_id,
					web_x_product.web_x_product_id as web_x_product_x_product_id,
					web_x_product.web_xx_product_id as web_x_product_xx_product_id
				FROM 
					web_order
				Left Join
					web_product
				On
					web_product.web_product_id = web_order.web_product_id
				Left Join
					web_x_product
				On
					web_x_product.web_x_product_id = web_product.web_x_product_id
				WHERE 
					web_order.web_x_order_ordernum = '".$val3['ordernum']."'
				AND
					web_x_product.web_xx_product_id = 1
			";
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute();
			$prodAry = $pdo->fetch(PDO::FETCH_ASSOC);
			if(!isset($prodAry['prodSubjects'])) {
				continue;
			}
			
			
	?>	
            <div class="tr">
                <div>
                    <div><span class="name"><?php echo $val3['order_name']; ?></span><span><?php echo $val3['order_mobile']; ?></span><span style="display:none;">【<?php echo $prodAry['web_x_order_ordernum']; ?>】</span></div>
                    <div class="color"><span><?php echo $prodAry['prodSubjects']; ?></span><span>$<?php echo number_format($val3['total']); ?></span></div>
                    <div class="color2"><?php echo $val3['states'];?> - <?php echo $val3['paymentstatus'];?></div>
                </div>
                <div class="right">
                    <a href="coupon-info1.php?web_x_order_id=<?php echo $val3['web_x_order_id']; ?>" class="btn info">詳情</a>
			<?PHP
				if($val3['states'] == '訂單成立') {
					if($val3['paymentstatus'] == '尚未收款') { 
			?>	
                    <a href="#" class="btn manager" data-id="<?php echo $val3['web_x_order_id']; ?>">管理</a>
			<?php
					}
				}	
			?>	
                </div>
            </div>
	<?php
		}
	?>	
        </div>
    </div>
</div>
<div class="popup_group">
    <div class="popup popup_login" data-id="" data-adminid="<?php echo $_SESSION['Member2']['ID']; ?>">
        <a class="close" href="#"></a>
        <h2>收款管理</h2>
        <a href="#" class="btn items" data-status="pay">已收款</a>
        <a href="#" class="btn items" data-status="cancel">訂單取消</a>
        <a href="#" class="btn submit disable">確認</a>
    </div>
	<div class="popup popup_alert">
		<h2>消費管理</h2>
		<div class="btns remark">
			<a href="#" class="btn submit checkUser">確認</a>
		</div>
	</div>
</div>
<div class="popup_filter">
    <div class="popup">
        <div class="content">
            <form name="form" id="searchForm" action="coupon.php" method="POST">
				<div class="row">
					<div class="name">店別</div>
					<div class="value">
						<select id="store" name="store_id">
				<?php
					foreach($xClassRow as $xClassKey => $xClass) {
						if($storeRange != 'all') {
							if($xClass['web_x_class_id'] != $storeRange) {
								continue;
							}
						}	
						$selected = ($xClass['web_x_class_id'] == $store_id) ? "selected='selected'" : null;
				?>		
							<option value="<?php echo $xClass['web_x_class_id']; ?>" <?php echo $selected; ?>><?php echo $xClass['subject']; ?></option>
				<?php
					}
				?>	
						</select>
					</div>
				</div>
				<div class="row">
					<div class="name">狀態</div>
					<div class="value">
						<select name="status">
							<option>全部</option>
							<option value="尚未收款">未收款</option>
							<option value="付款成功">已收款</option>
							<option value="取消">已取消</option>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="name">日期</div>
					<div class="value">
						<select name="searchDay">
					<?php
						for($dayS=$startDay; $dayS<=$endDay; $dayS+=86400) {
							$selected = (date('Y/m/d', $dayS) == date('Y/m/d')) ? 'selected="selected"' : null;
					?>		
							<option <?php echo $selected; ?> value="<?php echo date('Y-m-d', $dayS);?>"><?php echo date('Y/m/d', $dayS);?> 週<?php echo $dayName[date('N', $dayS)];?><?php if(date('Y/m/d', $dayS) == date('Y/m/d')) { ?>(今天)<?php } ?></option>
					<?php
						}
					?>		
						</select>
					</div>
				</div>
				<div class="row">
					<div class="name">手機</div>
					<div class="value">
						<input type="text" name="mobile" id="mobile" isMobile="1" maxlength="10" minlength="10" placeholder="請輸入手機號碼">
					</div>
				</div>
				<input class="form-control" type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>" />
				<a href="#" class="btn submit">搜尋</a>
			</form>	
        </div>
    </div>
</div>
<script>
	$(function () {
		
		liff.init(function (data) {
			initializeApp(data);
		});
		
<?php
	$agent = $_SERVER['HTTP_USER_AGENT'];
	if (preg_match("#\bLine\b#", $agent)) {
?>		
		if(!$('input[name="storeId"]').val()) {
			liff.init(function (data) {
				initializeApp(data);
			});
			$('.popup_group, .popup_alert').fadeIn(400).find('h2').text('身份讀取中');
			/*
			setTimeout(function(){
				$('.popup_group .popup, .popup_group').fadeOut(400);
			},1000);
			*/
		}
		
		$('.checkUser').on('click', function(){ 
			window.location.reload();
		});
<?php
	}
?>

		if($.cookie('storeIdCookie')) {
			//alert($.cookie('storeIdCookie'));
			if($('#store').find('option[value="'+$.cookie('storeIdCookie')+'"]').length) {
				$('#store').find('option[value="'+$.cookie('storeIdCookie')+'"]').prop('selected', true);
				$('#storeName').text($('#store').find('option[value="'+$.cookie('storeIdCookie')+'"]').text());
			} else {
				var _storeId = $('input[name="storeId"]').val();
				$('#store').find('option[value="'+_storeId+'"]').prop('selected', true);
				$('#storeName').text($('#store').find('option[value="'+_storeId+'"]').text());
			}
			
			if($.cookie('storeIdCookie') != _storeId) {
				//$('#searchForm').submit();
			}
		}  
		/*
		var _storeId = $('input[name="storeId"]').val();
		//alert($('#store').find('option[value="'+_storeId+'"]').text()+"="+$.cookie('storeIdCookie'));
		$('#store').find('option[value="'+_storeId+'"]').prop('selected', true);
		$('#storeName').text($('#store').find('option[value="'+_storeId+'"]').text());
		*/
		$('#store').on('change', function() {
			var storeId = $(this).find('option:selected').val();
			$.cookie('storeIdCookie', storeId, {
				expires:7, 
				path: '/'
			});
		});
		
		$('.popup_login .btn.items').click(function () {
			$('.popup_login .btn.items').removeClass('selected');
			$(this).addClass('selected');
			$('.popup_login .btn.submit').removeClass('disable').addClass('active');
		});
		$('.btn.manager').click(function () {
			
			var web_x_order_id = $(this).attr('data-id');
			if(!web_x_order_id) {
				return false;
			}
			$('.popup_group, .popup_login').attr('data-id', web_x_order_id).fadeIn(400);
		});
		$('.popup .close, .popup .back').on('click', function () {
			$('.popup_group .popup, .popup_group').fadeOut(400);
		});
		
		$('.popup_login .submit').on('click', function () {
			
			if($(this).is('.active')) {
				
				var id = $('.popup_login').attr('data-id');
				var status = $('.popup_login .btn.items.selected').attr('data-status');
				var admin = $('.popup_login').attr('data-adminid');
				//alert(id+"=="+status+"==="+admin);
				if(!id || !status || !admin) {
					return false;
				}
					
				$.ajax({ 
					url: "../action", 
					type: "POST",
					data: {
						action: 'orderPay', 
						id: id,
						status: status,
						admin: admin,
						token: '<?php echo $_SESSION['token']; ?>'
					}, 
					success: function(e){
						var obj = jQuery.parseJSON(e);
						
						if(obj.error == '0') {
							$('.popup_group .popup, .popup_group').fadeOut(400);
							$('[data-id="'+id+'"]').hide();
							window.location.reload();
						}
						
					}
				});
				
			}	
		});
		
		$('.filter .icon').click(function () {
			$('.popup_filter, .popup').fadeIn(400);
		});
		
		jQuery.validator.addMethod("string", function (value, element) {
			return this.optional(element) || /^[\u0391-\uFFE5\w]+$/.test(value);
		}, "不允許包含特殊符号!"); 
		jQuery.validator.addMethod("isMobile", function(value, element) {  
			var length = value.length;  
			var mobile = /^09[0-9]{8}$/;  
			return this.optional(element) || (length == 10 && mobile.test(value));  
		}, "請正確填寫手機號碼");  
		$('#searchForm').validate({
			errorElement: 'p',
			submitHandler: function(form) {
				form.submit();
			},
			rules: {
				accept_name: {
					required: false,
					minlength: 2,
					remote: {
						url: '../chekChiness.php',
						type: "post",
						data: {
							type: 1,
							username: function() {
								return $('#accept_name').val();
							}
						}
					}
				}
			},
			messages: {
				accept_name: {
					remote: '請輸入中文姓名'
				}
			}   
		});
		
		$('.popup_filter .submit').click(function (e) {
			e.preventDefault();
			$('#searchForm').submit();
			$('.popup_filter, .popup').hide();
			return false;
		});
	})
</script>
</body>
</html>
