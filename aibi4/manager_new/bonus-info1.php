<?php
	include_once("../control/includes/function.php");
    //unset($_SESSION['MyShopping']);
	include_once("./checkAgent.php");
	
	if (empty($_SESSION['expire']) || $_SESSION['expire'] < time()) {
		unset($_SESSION[$_SESSION['token']]);
		$_SESSION['token'] = md5('aibi' . uniqid(microtime())); // create token (fast/sufficient) 
	}// create token (fast/sufficient) 
	$_SESSION['expire'] = time() + 900; // make session valid for next 15 mins
	$_SESSION['visitid'] = $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT'];
	
	$web_x_order_id= intval($_GET["web_x_order_id"]) ? intval($_GET["web_x_order_id"]) : null;
	
	if(!$web_x_order_id) {
		RunJs("./stored.php");
	}
	
	//if($_SESSION['Member2']['ID'] || $_SESSION['store_id'] == '-1') {
	if($_SESSION['store_id'] == '-1') {	
		$store_id = ($_POST['store_id']) ? $_POST['store_id'] : $xClassRow[0]['web_x_class_id'];	
		//$_SESSION['store_id'] = $store_id;
		$storeRange = 'all';
	} else if($_SESSION['store_id']) {
		$store_id = $_SESSION['store_id'];
		$storeRange = $store_id;
	} else {
		$store_id = $_SESSION['store_id'];
		$storeRange = $store_id;
	}
	
	$sql = "
		SELECT 
			web_x_convert.*,
			web_convert.subject as prodSubjects,
			web_convert.web_order_id as web_order_id,
			web_convert.web_x_order_ordernum as web_x_order_ordernum,
			web_convert.web_product_id as web_order_product_id,
			web_convert.dimension as web_order_dimension,
			web_product.web_product_id as web_product_product_id,
			web_product.web_x_product_id as web_product_x_product_id,
			web_product.totalCount as web_product_totalCount,
			web_x_product.web_x_product_id as web_x_product_x_product_id,
			web_x_product.web_xx_product_id as web_x_product_xx_product_id,
			(select subject from web_x_class where web_x_class.web_x_class_id = web_x_convert.store_id) as xClassSubject
		from 
			web_x_convert
		Left Join
			web_convert as web_convert
		On
			web_convert.web_x_order_ordernum = web_x_convert.ordernum
		Left Join
			web_product
		On
			web_product.web_product_id = web_convert.web_product_id
		Left Join
			web_x_product
		On
			web_x_product.web_x_product_id = web_product.web_x_product_id
		WHERE 
			web_x_convert.web_x_order_id = :web_x_order_id
		AND
			web_x_product.web_xx_product_id = 4	
		AND
			web_x_convert.order_type = 0		
	";
	
	$excute = array(
		':web_x_order_id'		=> $web_x_order_id,
	);
	//$debug = new Helper();
	//echo $test = $debug::debugPDO($sql, $excute);
	$pdo = $pdoDB->prepare($sql);
	$pdo->execute($excute);
	$row3 = $pdo->fetchAll(PDO::FETCH_ASSOC);
	
	$sql = "
		Select 
			SUM(b.price) as sum
		From 
			web_x_order a
		Left Join 
			web_order b ON b.web_x_order_ordernum = a.ordernum
		Left Join 
			web_product c ON c.web_product_id = b.web_product_id
		Left Join 
			web_x_product d ON d.web_x_product_id = c.web_x_product_id
		Left Join 
			web_xx_product e ON e.web_xx_product_id = d.web_xx_product_id	
		Where 
			a.web_member_id = '".$row3[0]['web_member_id']."'		
		AND
			a.states = '訂單成立'
		AND
			a.paymentstatus = '付款成功'
		AND
			d.web_xx_product_id IN (3)
		order by 
			a.web_x_order_id desc 
	";
	$pdo = $pdoDB->prepare($sql);
	$pdo->execute();
	$useTotal = $pdo->fetch(PDO::FETCH_ASSOC);
	
?>	
<!DOCTYPE html>
<html lang="en">
<?php include_once('head.php'); ?>
<body>
<div class="main">
    <?php include_once('header.php'); ?>
    <div class="content coupon-info">
        <a href="bonus.php" class="back"></a>
        <ul class="menu">
            <li class="active">兌換詳情</li>
        </ul>
        <div class="content">
            <div class="info">
	<?php
		if($row3[0]['states'] == '兌換成立') {
			if($row3[0]['paymentstatus'] == '已兌換') {
				//if(!$useTotal['sum']) { 
	?>	
                <a href="#" class="btn btn-popup">取消</a>	
	<?php
				//}
			}
		}
	?>
                <div class="row">
                    <span class="name"><?php echo $row3[0]['order_name']; ?></span>
                    <span class="color"><?php echo $row3[0]['order_mobile']; ?></span>
                </div>
                <div class="row color2">
                    狀態：
					<?php 
						if($row3[0]['states'] == '取消') {
							echo $row3[0]['states']; 
						} else {
							echo $row3[0]['paymentstatus']; 
						}	
					?>
                </div>
            </div>
	<?php
		foreach($row3 as $key3 => $val3) {
	?>		
            <div class="info2">
                <div class="row">
                    <div class="name">兌換門市</div>
                    <div class="value"><?php echo ($val3['xClassSubject']); ?></div>
                </div>
                <div class="row">
                    <div class="name">兌換商品</div>
                    <div class="value"><?php echo ($val3['prodSubjects']); ?></div>
                </div>
                <div class="row">
                    <div class="name">點數</div>
                    <div class="value">
			<?php if($val3['paymentstatus'] == '已兌換') {  echo number_format($val3['total']); } ?>		
					</div>
                </div>
				<div class="row">
					<div class="name">下單時間</div>
					<div class="value"><?php echo $val3['cdate']; ?></div>
				</div>
            </div>
	<?php
		//if($val3['states'] == '訂單成立') {
			if($val3['paymentstatus'] == '已兌換') {
	?>
			<div class="info2">
                <div class="row">
                    <div class="name">兌換人員</div>
                    <div class="value"><?php echo $val3['editUser'];?></div>
                </div>
                <div class="row">
                    <div class="name">兌換時間</div>
                    <div class="value"><?php echo date('Y/m/d H:i:s', strtotime($val3['editDateTime']));?></div>
                </div>
            </div>
	<?php
			}
		//}	
	?>	
	<?php
		if($val3['states'] == '取消') {
	?>
			<div class="info2">
                <div class="row">
                    <div class="name">取消人員</div>
                    <div class="value"><?php echo $val3['cancelUser'];?></div>
                </div>
                <div class="row">
                    <div class="name">取消時間</div>
                    <div class="value"><?php echo date('Y/m/d H:i:s', strtotime($val3['cancelDateTime']));?></div>
                </div>
                <div class="row">
                    <div class="name">取消原因</div>
                    <div class="value"><?php echo $val3['cancelReason'];?></div>
                </div>
                <div class="row">
                    <div class="name">備註</div>
                    <div class="value"><?php echo $val3['cancelRemark'];?></div>
                </div>
            </div>
	<?php
		}	
	?>	
	<?php
		}
	?>		
        </div>
    </div>
</div>
<div class="popup_group">
    <div class="popup popup_password">
        <a class="close" href="#"></a>
        <h2>請輸入密碼</h2>
		<form name="form" id="cancelForm" action="bonus-cancel.php" method="POST">
			<input id="cancelPwd" name="cancelPwd" type="password" required="">
			<input class="form-control" type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>" />
			<input class="form-control" type="hidden" name="web_x_order_id" value="<?php echo $web_x_order_id; ?>" />
			<input class="form-control" type="hidden" name="action" value="cancelPwdCheck" />
			<a class="btn submit">確認</a>
		</form>
    </div>
	<div class="popup popup_alert">
		<!--<a class="close" href="#"></a>-->
		<h2>票券管理</h2>
		<!--<a href="#" class="btn submit disable">確認</a>-->
	</div>
</div>
<script>
	$(function() {
		$('.btn-popup').click(function () {
			$('#cancelPwd').val('');
			$('.popup_group, .popup_password').fadeIn(400).find('h2').text('請輸入密碼');
			$('.popup_group, .popup_group .popup').fadeIn(400);
		});
		
		$('#cancelForm').validate({
			errorElement: 'p',
			submitHandler: function(form) {
				$.ajax({ 
					url: "../action", 
					type: "POST",
					data: $("#cancelForm").serialize(), 
					success: function(e){
						var obj = jQuery.parseJSON(e);
						var output = '';
						$.each(obj, function(k,v) {
							output += k + ': ' + v +'; ';
						});
						
						if(obj.error != '0') {
							
							$('.popup_group .popup, .popup_group').hide();
							$('.popup_group, .popup_alert').fadeIn(400).find('h2').text(obj.message);
							
							setTimeout(function(){
								$('.popup_group .popup, .popup_group').fadeOut(400);
							},1000);
							
							return false;
						}
						if(obj.error == '0') {
							form.submit();
							$('.popup_group .popup, .popup_group').fadeOut(400);
						}
												
					}
				});
			}
		});
		$('.popup_password .submit').click(function (e) {
			e.preventDefault();
			$('#cancelForm').submit();
			return false;
		});
	})
</script>
</body>
</html>
