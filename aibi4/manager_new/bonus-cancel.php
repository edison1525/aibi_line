<?php
	include_once("../control/includes/function.php");
    //unset($_SESSION['MyShopping']);
	include_once("./checkAgent.php");
	
	if (empty($_SESSION['expire']) || $_SESSION['expire'] < time()) {
		unset($_SESSION[$_SESSION['token']]);
		$_SESSION['token'] = md5('aibi' . uniqid(microtime())); // create token (fast/sufficient) 
	}// create token (fast/sufficient) 
	$_SESSION['expire'] = time() + 900; // make session valid for next 15 mins
	$_SESSION['visitid'] = $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT'];
	
	$web_x_order_id= intval($_POST["web_x_order_id"]) ? intval($_POST["web_x_order_id"]) : null;
	
	$refererInfo = parse_url($_SERVER['HTTP_REFERER']);
	
	if(!$refererInfo || (($refererInfo['path']."?".$refererInfo['query']) != '/manager_new/bonus-info1.php?web_x_order_id='.$web_x_order_id)) {
		RunJs("./bonus.php");
	}
	
	//if($_SESSION['Member2']['ID'] || $_SESSION['store_id'] == '-1') {
	if($_SESSION['store_id'] == '-1') {	
		$store_id = ($_POST['store_id']) ? $_POST['store_id'] : $xClassRow[0]['web_x_class_id'];	
		//$_SESSION['store_id'] = $store_id;
		$storeRange = 'all';
	} else if($_SESSION['store_id']) {
		$store_id = $_SESSION['store_id'];
		$storeRange = $store_id;
	} else {
		$store_id = $_SESSION['store_id'];
		$storeRange = $store_id;
	}
	/*
	echo "<pre>";
	print_r($_SESSION);
	echo "</pre>";
	*/
	
?>	
<!DOCTYPE html>
<html lang="en">
<?php include_once('head.php'); ?>
<script src="https://d.line-scdn.net/liff/1.0/sdk.js"></script>
<script>
	//init LIFF
	function initializeApp(data) {
		//取得QueryString
		let urlParams = new URLSearchParams(window.location.search);
		//顯示QueryString
		if(data.context.userId) {
			var userId = data.context.userId;
			$.ajax({ 
				url: "../action", 
				type: "POST",
				data: {action: 'userInfo', lineId: userId, token: '<?php echo $_SESSION['token']; ?>'}, 
				success: function(e){
					var obj = jQuery.parseJSON(e);
					
					if(obj.error == '0') {
						/*
						var output = '';
						$.each(obj, function(k,v) {
							output += k + ': ' + v +'; ';
						});
						alert(output);
						*/
						$('input[name="admin"]').val(obj.web_member_id);
						
					} 
					
				}
			});	
		}	
		
	}
</script>
<body>
<div class="main">
    <?php include_once('header.php'); ?>
    <div class="content coupon-cancel">
        <a href="bonus.php" class="back"></a>
        <ul class="menu">
            <li class="active">兌換詳情</li>
        </ul>
        <form name="form" id="cancelForm" method="POST">
			<div>
				<div class="row">
					<div class="name">請選擇取消原因：</div>
					<div class="value">
						<label><input name="cancelReason" type="radio" value="客戶取消" required=""> <span>客戶取消</span></label>
						<label><input name="cancelReason" type="radio" value="操作錯誤" required=""> <span>操作錯誤</span></label>
						<label><input name="cancelReason" type="radio" value="其他" required=""> <span>其他(請於備註說明)</span></label>
					</div>
					<label for="cancelReason" class="error"></label>
				</div>
				<div class="row">
					<div class="name">備註</div>
					<div class="value">
						<textarea name="cancelRemark" placeholder=""></textarea>
					</div>
				</div>
			</div>
			<input class="form-control" type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>" />
			<input class="form-control" type="hidden" name="web_x_order_id" value="<?php echo $web_x_order_id; ?>" />
			<input class="form-control" type="hidden" name="action" value="cancelBonus" />
			<input class="form-control" type="hidden" name="admin" value="<?php echo $_SESSION['Member2']['ID']; ?>" />
			<a class="btn submit">送出</a>
		</form>	
    </div>
</div>
<div class="popup_group">
	<div class="popup popup_alert">
		<!--<a class="close" href="#"></a>-->
		<h2>預約管理</h2>
		<!--<a href="#" class="btn submit disable">確認</a>-->
	</div>
</div>	
<script>
	$(function() {
		
		liff.init(function (data) {
			initializeApp(data);
		});
		
		$('input[name="cancelReason"]').on('click', function(){
			//alert($('input[name="cancelReason"]:checked').val());
			if($('input[name="cancelReason"]:checked').val() == '其他') {
				$('textarea[name="cancelRemark"]').attr('required', true);
			} else {
				$('textarea[name="cancelRemark"]').attr('required', false);
			}
		});
		$('#cancelForm').validate({
			//errorElement: 'p',
			submitHandler: function(form) {
				
				$.ajax({ 
					url: "../action", 
					type: "POST",
					data: $("#cancelForm").serialize(), 
					success: function(e){
						var obj = jQuery.parseJSON(e);
						/*
						var output = '';
						$.each(obj, function(k,v) {
							output += k + ': ' + v +'; ';
						});
						return false;
						*/
						if(obj.error != '0') {
							
							$('.popup_group .popup, .popup_group').hide();
							$('.popup_group, .popup_alert').fadeIn(400).find('h2').text(obj.message);
							
							setTimeout(function(){
								$('.popup_group .popup, .popup_group').fadeOut(400);
							},1000);
							
							return false;
						}
						if(obj.error == '0') {
							//form.submit();
							location.href = './bonus.php';
						}
												
					}
				});
				
			}
		});
		$('.submit').click(function (e) {
			e.preventDefault();
			$('#cancelForm').submit();
			return false;
		});
	})
</script>
</body>
</html>
