<?php
	include_once("../control/includes/function.php");
    //unset($_SESSION['MyShopping']);
	include_once("./checkAgent.php");
	
	if (empty($_SESSION['expire']) || $_SESSION['expire'] < time()) {
		unset($_SESSION[$_SESSION['token']]);
		$_SESSION['token'] = md5('aibi' . uniqid(microtime())); // create token (fast/sufficient) 
	}// create token (fast/sufficient) 
	$_SESSION['expire'] = time() + 900; // make session valid for next 15 mins
	$_SESSION['visitid'] = $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT'];
	
	$todayDay = date('N');
	//$endDay = strtotime('+'.ceil(14-$todayDay).' day');
	//$startDay = strtotime('-13 day', $endDay);
	//20號前當月，21號當月+下個月
	if(date('d') <= '20') {
		$_endDay = ceil((date('t')- date('d')) + $todayDay); 
		$endDay = strtotime('+'.ceil($_endDay-$todayDay).' day');
		//$startDay = strtotime('-'.ceil($_endDay - 1).' day', $endDay);
		$startDay = strtotime('-'.ceil($_endDay + 2).' day', $endDay);

	} else {
		$nextMonthTotal = date('d', strtotime(date('Y-m-t', strtotime('+1 month'))));
		$_endDay = ceil((date('t')- date('d')) + $todayDay + $nextMonthTotal); 
		$endDay = strtotime('+'.ceil($_endDay-$todayDay).' day');
		//$startDay = strtotime('-'.ceil($_endDay - 1).' day', $endDay);
		$startDay = strtotime('-'.ceil($_endDay + 2).' day', $endDay);
	}
	
	$selectDay = ($_POST['selectDay']) ? $_POST['selectDay'] : date('Y-m-d');
	//unset($_SESSION['store_id']);
	//if($_SESSION['Member2']['ID'] || $_SESSION['store_id'] == '-1') {
	if($_SESSION['store_id'] == '-1') {	
		$store_id = ($_POST['store_id']) ? $_POST['store_id'] : $xClassRow[0]['web_x_class_id'];	
		//$_SESSION['store_id'] = $store_id;
		$storeRange = 'all';
	} else if($_SESSION['store_id']) {
		$store_id = $_SESSION['store_id'];
		$storeRange = $store_id;
	} else {
		$store_id = $_SESSION['store_id'];
		$storeRange = $store_id;
	}
	
	$web_x_class_id = $store_id;
	$sql = "
		SELECT 
			main.*,
			xteam.subject as xteamSubject,
			xteam.time as xteamTime,
			class.subject as classSubject,
			class.lineInfo as classLineInfo,
			class.web_x_class_id as class_x_class_id,
			xclass.subject as xclassSubject
		from 
			web_x_register as main
		left join
			web_x_team xteam
		on
			xteam.web_x_team_id = main.web_x_team_id
		left join
			web_class class
		on
			class.web_class_id = main.web_time_id	
		left join
			web_x_class xclass
		on
			xclass.web_x_class_id = class.web_x_class_id	
		WHERE 
			main.registerDate >= :registerDateS
		AND
			main.registerDate <= :registerDateE
		AND
			xclass.web_x_class_id = :web_x_class_id
	";
	$sql .= "		
		Order by
			main.registerDate ASC, main.subject ASC
	";
	$excute = array(
		':registerDateS'		=> ($selectDay) ? $selectDay : date('Y-m-d', $startDay),
		':registerDateE'		=> ($selectDay) ? $selectDay : date('Y-m-d', $endDay),
		//':paymentstatus'		=> '取消',
		':web_x_class_id'		=> $store_id,
		//':subject'        		=> $timeRow[0]['web_time_id'],
		//':web_team_id'			=> $teamRow[0]['web_team_id'],
		//':web_class_id'			=> $classRow[0]['web_class_id'],
	);
	//$excute = ($store_id) ? $excute+array(':store_id' => $store_id) : $excute;	
	//$debug = new Helper();
	//echo $test = $debug::debugPDO($sql, $excute);
	$pdo = $pdoDB->prepare($sql);
	$pdo->execute($excute);
	$row3 = $pdo->fetchAll(PDO::FETCH_ASSOC);
	$bookingInfo = array();
	/*
	foreach($row3 as $key3 => $val3) {
		$dayAry[$val3['registerDate']][date('N', strtotime($val3['registerDate']))][] = $val3;
	}
	*/
	$registerTimeRow = array();
	$restDateRow = array();
	foreach($row3 as $registerKey => $registerVal) {
		if(array_search(substr($registerVal['subject'],16), $_timeAry) >= '0') {
			if($_timeAry[array_search(substr($registerVal['subject'],16), $_timeAry)] == substr($registerVal['subject'],16)) {
				//$registerTimeRow[$registerVal['web_time_id']][$registerVal['registerDate']][] = substr($registerVal['subject'],16);
				$registerTimeRow[$registerVal['registerDate']][$registerVal['web_time_id']][substr($registerVal['subject'],16)][] = $registerVal;
				if($registerVal['xteamTime'] >= 1) {
					for($i = 2; $i <= $registerVal['xteamTime'] * 2; $i++) {
						$searchKey = array_search(substr($registerVal['subject'],16), $_timeAry) + ($i - 1);
						//$registerTimeRow[$registerVal['web_time_id']][$registerVal['registerDate']][] = $_timeAry[$searchKey];
						$registerTimeRow[$registerVal['registerDate']][$registerVal['web_time_id']][$_timeAry[$searchKey]][] = $registerVal; 
					}	
				}
			}	
			
		}
		$restDateRow[$registerVal['web_time_id']] = $registerVal['classLineInfo'];
	}
	
	$registerTypeText = array(1 => 'LINE', 2 => '電話', 3 => '現場');
	
?>
<!DOCTYPE html>
<html lang="en">
<?php include_once('head.php'); ?>
<script src="https://d.line-scdn.net/liff/1.0/sdk.js"></script>
<script>
	//init LIFF
	function initializeApp(data) {
		//取得QueryString
		let urlParams = new URLSearchParams(window.location.search);
		//顯示QueryString
		if(data.context.userId) {
			var userId = data.context.userId;
			$.ajax({ 
				url: "../action", 
				type: "POST",
				data: {action: 'userInfo', lineId: userId, token: '<?php echo $_SESSION['token']; ?>'}, 
				success: function(e){
					var obj = jQuery.parseJSON(e);
					
					if(obj.error == '0') {
						/*
						var output = '';
						$.each(obj, function(k,v) {
							output += k + ': ' + v +'; ';
						});
						alert(output);
						*/
						$('.popup_group, .popup_login').attr('data-adminid', obj.web_member_id);
						$('input[name="adminUsr"]').val(obj.uname);
					} 
					
				}
			});	
		}	
		
	}
</script>
<body>
<div class="main">
    <?php include_once('header.php'); ?>
    <div class="content booking">
        <a class="close" style="display:none;"></a>
		<input type="hidden" value="<?php echo $_SESSION['store_id']; ?>" name="storeId" />
		<input type="hidden" value="管理員" name="adminUsr" />
		<form name="form" id="searchForm" action="booking.php" method="POST">
			<ul class="menu">
				<li style="padding-right:15px;">店別:</li>
				<li class="active">
					<div class="row">
						<div class="value" style="display:inline;">
							<select class="popSelect2" name="store_id" style="display:inline-block;">
					<?php
						foreach($xClassRow as $xClassKey => $xClass) {
							if($storeRange != 'all') {
								if($xClass['web_x_class_id'] != $storeRange) {
									continue;
								}
							}	
							$selected = ($xClass['web_x_class_id'] == $store_id) ? "selected='selected'" : null;
					?>		
								<option value="<?php echo $xClass['web_x_class_id']; ?>" <?php echo $selected; ?>><?php echo $xClass['subject']; ?></option>
					<?php
						}
					?>	
							</select>	
						</div>
					</div>
				</li>
			</ul>
			
			<ul class="filter" data-date="<?php echo $selectDay; //date('Y-m-d'); ?>" data-sdate="<?php echo date('Y-m-d', $startDay); ?>" data-edate="<?php echo date('Y-m-d', $endDay); ?>">
				<li><a id="prevDay">前一日</a></li>
				<li><a id="nextDay">後一日</a></li>
				<li>
					<select id="selectDay" name="selectDay">
				<?php
					for($dayS=$startDay; $dayS<=$endDay; $dayS+=86400) {
						//$selected = (date('Y/m/d', $dayS) == date('Y/m/d')) ? 'selected="selected"' : null;
						$selected = (date('Y-m-d', $dayS) == $selectDay) ? 'selected="selected"' : null;
				?>	
						<option <?php echo $selected; ?> value="<?php echo date('Y-m-d', $dayS);?>"><?php echo date('Y/m/d', $dayS);?> 週<?php echo $dayName[date('N', $dayS)];?><?php if(date('Y/m/d', $dayS) == date('Y/m/d')) { ?>(今天)<?php } ?></option>
						
				<?php
					}
				?>
					</select>
				</li>
			</ul>
		</form>
<?php
	for($dayS=$startDay; $dayS<=$endDay; $dayS+=86400) {
		if(isset($registerTimeRow[date('Y-m-d', $dayS)])) {
?>	
	<?php
		//foreach($dayAry as $dayKey => $dayInfo) {
			//echo $dayKey;
	?>		
			<div class="table" data-date="<?php echo date('Y-m-d', $dayS);?>" style="display:none;">
				<div>
					<table>
						<tr>
							<td><div>時段</div></td>
					<?php
						$lineInfoRowAry = array();
						$lineInfo = array();
						foreach($classRow as $classKey => $classVal) {
							//echo $classVal['web_x_class_id'];
							if($web_x_class_id != $classVal['web_x_class_id']) {
								continue;
							}
							
							$lineInfoAry = ($restDateRow[$classVal['web_class_id']]) ? (explode('#####', $restDateRow[$classVal['web_class_id']])) : explode('#####', $classVal['lineInfo']);
							foreach($lineInfoAry as $lineInfoKey => $lineInfoVal) {
								$lineInfoRowAry[$classVal['web_class_id']][] = explode('@#@', $lineInfoVal);
							}
							foreach($lineInfoRowAry[$classVal['web_class_id']] as $key => $lineInfoAry) {
								$lineInfo[$classVal['web_class_id']][$lineInfoAry['2']][] = $lineInfoAry['1'];
							}
					?>		
							<td><div class="register" data-type="0"><?php echo $classVal['subject']; ?></div></td>
					<?php
						}
					?>	
						</tr>
				<?php
					foreach($_timeAry as $_timeKey => $_timeVal) {
						$searchKey = date('Y-m-d', $dayS)."(".$dayName[date('N', strtotime(date('Y-m-d', $dayS)))].")-".$_timeVal;
						$_timeValAry = explode('~', $_timeVal);
						
						//$searchRow = SearchMultidimensionalAry_5($dayAry[date('Y-m-d', $dayS)][date('N', strtotime(date('Y-m-d', $dayS)))], 'subject', $searchKey);
				?>		
						<tr>
							<td>
								<div><span><?php echo $_timeValAry[0]; ?></span><span><?php echo $_timeValAry[1]; ?></span></div>
							</td>
					<?php
						foreach($classRow as $classKey => $classVal) {
							if($web_x_class_id != $classVal['web_x_class_id']) {
								continue;
							}
							
							//$searchRow2 = SearchMultidimensionalAry_5($searchRow, 'web_time_id', $classVal['web_class_id']);
							
							$searchRow2 = $registerTimeRow[date('Y-m-d', $dayS)][$classVal['web_class_id']][$_timeVal];
							
					?>	
							<td>
						<?php
							if((in_array('1', $lineInfo[$classVal['web_class_id']][date('Y-m-d', $dayS)]) && $_timeKey <= 9) || (in_array('3', $lineInfo[$classVal['web_class_id']][date('Y-m-d', $dayS)]) && $_timeKey <= 9) && !count($searchRow2)) {
						?>	
							<div class="register" data-type="0">休</div>
						<?php
							} else if((in_array('2', $lineInfo[$classVal['web_class_id']][date('Y-m-d', $dayS)]) && $_timeKey  >= 9) || (in_array('3', $lineInfo[$classVal['web_class_id']][date('Y-m-d', $dayS)]) && $_timeKey  >= 9) && !count($searchRow2)) {
						?>
							<div class="register" data-type="0">休</div>
						<?php
							} else if(count($searchRow2)) {
								foreach($searchRow2 as $searchkey2 => $searchVal2) { 
							
									if(isset($searchVal2['paymentstatus'])) {
										switch($searchVal2['paymentstatus']) {
											case '未報到':
												$dataType = 1;
												$classColor = '';
											break;
											case '已報到':
												$dataType = 2;
												$classColor = '';
											break;
											case '取消':
												$dataType = 3;
												$searchVal2['paymentstatus'] = '已取消';
												$classColor = 'color';
											break;
										}
									} else {
										$dataType = 0;
									}	
						?>	
								<div data-registerid="<?php echo $searchVal2['web_x_register_id']; ?>" data-type="<?php echo $dataType; ?>">
								<span><?php echo $searchVal2['accept_name']."-".($searchVal2['xteamSubject']); ?></span><span class="<?php echo $classColor;?>"><?php echo $searchVal2['paymentstatus']; ?></span>
								<input type="hidden" name="order_name" value="<?php echo $searchVal2['order_name']; ?>" />
								<input type="hidden" name="order_mobile" value="<?php echo $searchVal2['order_mobile']; ?>" />
								<input type="hidden" name="classSubjct" value="<?php echo $searchVal2['classSubject']; ?>" />
								<input type="hidden" name="teamXsubject" value="<?php echo $searchVal2['xteamSubject']; ?>" />
								<input type="hidden" name="accept_name" value="<?php echo $searchVal2['accept_name']; ?>" />
								<input type="hidden" name="time" value="<?php echo substr($searchVal2['subject'], 16); ?>" />
								<input type="hidden" name="remark" value="<?php echo $searchVal2['remark']; ?>" />
								<input type="hidden" name="total" value="<?php echo $searchVal2['total']; ?>" />
								<input type="hidden" name="checkInUsr" value="<?php echo $searchVal2['checkInUsr']; ?>" />
								<input type="hidden" name="checkInTime" value="<?php echo $searchVal2['checkInTime']; ?>" />
								<input type="hidden" name="cancelUsr" value="<?php echo $searchVal2['cancelUsr']; ?>" />
								<input type="hidden" name="cancelTime" value="<?php echo $searchVal2['cancelTime']; ?>" />
								</div>
						<?php
									if(count($searchRow2) != ($searchkey2+1)) {
						?>
								<span class="line"></span>
						<?php
									}
								}
							
							} else {	
						?>
								<div class="register" data-type="0"></div>
						<?php
							}
						?>
							</td>
					<?php
						}
					?>	
						</tr>
				<?php
					}
				?>	

					</table>
				</div>
			</div>
	<?php
		//}
	?>
<?php
		} else {
?>
			<div class="table" data-date="<?php echo date('Y-m-d', $dayS);?>" style="display:none;">
				<div>
					<table>
						<tr>
							<td><div>時段</div></td>
					<?php
						
						$lineInfoRowAry = array();
						$lineInfo = array();
						foreach($classRow as $classKey => $classVal) {
							if($web_x_class_id != $classVal['web_x_class_id']) {
								continue;
							}
							$lineInfoAry = explode('#####', $classVal['lineInfo']);
							foreach($lineInfoAry as $lineInfoKey => $lineInfoVal) {
								$lineInfoRowAry[$classVal['web_class_id']][] = explode('@#@', $lineInfoVal);
							}
							foreach($lineInfoRowAry[$classVal['web_class_id']] as $key => $lineInfoAry) {
								$lineInfo[$classVal['web_class_id']][$lineInfoAry['2']][] = $lineInfoAry['1'];
							}
					?>		
							<td><div class="register" data-type="0"><?php echo $classVal['subject']; ?></div></td>
					<?php
						}
					?>	
						</tr>
				<?php
					foreach($_timeAry as $_timeKey => $_timeVal) {
						$searchKey = $dayKey."(".$dayName[date('N', strtotime($dayKey))].")-".$_timeVal;
						$_timeValAry = explode('~', $_timeVal);
						
						$searchRow = SearchMultidimensionalAry_5($dayInfo[date('N', strtotime($dayKey))], 'subject', $searchKey);
				?>		
						<tr>
							<td>
								<div><span><?php echo $_timeValAry[0]; ?></span><span><?php echo $_timeValAry[1]; ?></span></div>
							</td>
					<?php
						foreach($classRow as $classKey => $classVal) {
							if($web_x_class_id != $classVal['web_x_class_id']) {
								continue;
							}
							
							$searchRow2 = SearchMultidimensionalAry_5($searchRow, 'web_time_id', $classVal['web_class_id']);
							if(isset($searchRow2[0]['paymentstatus'])) {
								switch($searchRow2[0]['paymentstatus']) {
									case '未報到':
										$dataType = 1;
									break;
									case '已報到':
										$dataType = 2;
									break;
									case '已取消':
										$dataType = 3;
									break;
								}
							} else {
								$dataType = 0;
							}	
							
					?>	
							<td>
					<?php
						if((in_array('1', $lineInfo[$classVal['web_class_id']][date('Y-m-d', $dayS)]) && $_timeKey < 9) || (in_array('3', $lineInfo[$classVal['web_class_id']][date('Y-m-d', $dayS)]) && $_timeKey < 9)) {
					?>	
						<div class="register" data-type="0">休</div>
					<?php
						} else if((in_array('2', $lineInfo[$classVal['web_class_id']][date('Y-m-d', $dayS)]) && $_timeKey  >= 9) || (in_array('3', $lineInfo[$classVal['web_class_id']][date('Y-m-d', $dayS)]) && $_timeKey  >= 9)) {
					?>
						<div class="register" data-type="0">休</div>		
					<?php
						} else {
					?>	
								<div class="register" data-type="<?php echo $dataType; ?>"><span></span><span><?php echo $searchRow2[0]['paymentstatus']; ?></span></div>
					<?php
						}
					?>	
							</td>
					<?php
						}
					?>	
						</tr>
				<?php
					}
				?>	
					</table>
				</div>
			</div>
<?php			
		}
	}
?>	
    </div>
</div>
<div class="popup_group">
    <div class="popup popup_info" data-type="1" data-id="">
        <a class="close"></a>
        <h2>未報到</h2>
        <div class="content">
            <div class="row">
                <div class="name">預約人</div>
                <div class="value">李佳靜</div>
            </div>
            <div class="row">
                <div class="name">電話</div>
                <div class="value">0933888222</div>
            </div>
            <div class="row">
                <div class="name">時段</div>
                <div class="value">13:30-14:30</div>
            </div>
            <div class="row">
                <div class="name">設計師</div>
                <div class="value">Vicky</div>
            </div>
            <div class="row">
                <div class="name">服務項目</div>
                <div class="value">染髮</div>
            </div>
            <div class="row notes">
                <div class="name">備註</div>
                <div class="value">
                    <span></span>
                    <input type="text" name="remark" value="">
                    <a href="#" class="btn2">編輯</a>
                </div>
            </div>
			<div class="row total">
                <div class="name">金額</div>
                <div class="value">
					<span></span>
                    <input type="text" name="total" value="">
					<a href="#" class="btn3">編輯</a>
                </div>
            </div>
            <div class="btns">
                <a href="#" class="btn items" data-status="取消">預約取消</a>
                <a href="#" class="btn items" data-status="已報到">預約報到</a>
                <a href="#" class="btn submit disable">確認</a>
            </div>
        </div>
    </div>
    <div class="popup popup_info" data-type="2" data-id="">
        <a class="close"></a>
        <h2>已報到<span id="checkInMsg">10:35 by Sam</span></h2>
        <div class="content">
            <div class="row">
                <div class="name">預約人</div>
                <div class="value">李佳靜</div>
            </div>
            <div class="row">
                <div class="name">電話</div>
                <div class="value">0933888222</div>
            </div>
            <div class="row">
                <div class="name">時段</div>
                <div class="value">13:30-14:30</div>
            </div>
            <div class="row">
                <div class="name">設計師</div>
                <div class="value">Vicky</div>
            </div>
            <div class="row">
                <div class="name">服務項目</div>
                <div class="value">染髮</div>
            </div>
            <div class="row notes">
                <div class="name">備註</div>
                <div class="value">
                    <span></span>
                    <input type="text" name="remark" value="">
                    <a href="#" class="btn2">編輯</a>
                </div>
            </div>
			<div class="row total">
                <div class="name">金額</div>
                <div class="value">
					<span></span>
                    <input type="text" name="total" value="">
					<a href="#" class="btn3">編輯</a>
                </div>
            </div>
			<div class="btns remark">
                <a href="#" class="btn submit disable">確認</a>
            </div>
        </div>
    </div>
    <div class="popup popup_info" data-type="3" data-id="">
        <a class="close"></a>
        <h2>已取消<span id="cancelMsg">10:35 by Sam</span></h2>
        <div class="content">
            <div class="row">
                <div class="name">預約人</div>
                <div class="value">李佳靜</div>
            </div>
            <div class="row">
                <div class="name">電話</div>
                <div class="value">0933888222</div>
            </div>
            <div class="row">
                <div class="name">時段</div>
                <div class="value">13:30-14:30</div>
            </div>
            <div class="row">
                <div class="name">設計師</div>
                <div class="value">Vicky</div>
            </div>
            <div class="row">
                <div class="name">服務項目</div>
                <div class="value">染髮</div>
            </div>
            <div class="row notes">
                <div class="name">備註</div>
                <div class="value">
                    <span></span>
                    <input type="text" name="remark" value="">
                    <a href="#" class="btn2">編輯</a>
                </div>
            </div>
			<div class="row total">
                <div class="name">金額</div>
                <div class="value">
					<span></span>
                    <input type="text" name="total" value="">
					<a href="#" class="btn3">編輯</a>
                </div>
            </div>
			<div class="btns remark">
                <a href="#" class="btn submit disable">確認</a>
            </div>
        </div>
    </div>
	<div class="popup popup_alert">
		<h2>消費管理</h2>
		<div class="btns remark">
			<a href="#" class="btn submit checkUser">確認</a>
		</div>
	</div>
</div>
<script>
	function calendar() {
		$('.table').each(function () {
			//console.log($(this).attr('data-date'), $('#selectDay option:selected').val());
			if($(this).attr('data-date') == $('.filter').attr('data-date')) {
				$(this).show();
			} else {
				$(this).hide();	
			}
		});	
	}
	
	$(function () {
		
		liff.init(function (data) {
			initializeApp(data);
		});
<?php
	$agent = $_SERVER['HTTP_USER_AGENT'];
	if (preg_match("#\bLine\b#", $agent)) {
?>		
		if(!$('input[name="storeId"]').val()) {
			liff.init(function (data) {
				initializeApp(data);
			});
			$('.popup_group, .popup_alert').fadeIn(400).find('h2').text('身份讀取中');
		}
		
		$('.checkUser').on('click', function(){ 
			window.location.reload();
		});
<?php
	}
?>	
		calendar();
		
		$('#selectDay').on('change', function(){
			$('.filter').attr('data-date', $(this).val());
			calendar();		
		});
		
		$('#prevDay').click(function() {
			$('#selectDay option').attr('selected', false);
			var Element = $('.filter').attr('data-date');
			var sdate = $('.filter').attr('data-sdate');
			if(Element != sdate) {
				$('#selectDay option[value="'+Element+'"]').prop('selected', false);
				var prevElement = $('#selectDay option[value="'+Element+'"]').prev('option');
				if (prevElement.length > 0) {
					$('#selectDay option[value="'+Element+'"]').prev('option').prop('selected', true).trigger('change');
					$('.filter').attr('data-date', $('#selectDay option[value="'+Element+'"]').prev('option').val());
				}
			} else {
				$('#selectDay option[value="'+Element+'"]').prop('selected', false);
				$('#selectDay option[value="'+Element+'"]').prev('option').prop('selected', true);
			}	
		});
		
		$('#nextDay').click(function() {
			$('#selectDay option').prop('selected', false);
			var Element = $('.filter').attr('data-date');
			var edate = $('.filter').attr('data-edate');
			if(Element != edate) {
				$('#selectDay option[value="'+Element+'"]').prop('selected', false);
				var nextElement = $('#selectDay option[value="'+Element+'"]').next('option');
				if (nextElement.length > 0) {
					$('#selectDay option[value="'+Element+'"]').next('option').prop('selected', true).trigger('change');
					$('.filter').attr('data-date', $('#selectDay option[value="'+Element+'"]').next('option').val());
				}
			} else {
				$('#selectDay option[value="'+Element+'"]').prop('selected', true);
				//$('#selectDay option[value="'+Element+'"]').next('option').prop('selected', true);
			}	
		});
		
		$('.table div').click(function () {
			var type = $(this).attr('data-type');
			var dom  = $('.popup_info[data-type="' + type + '"]');
			var id = $(this).attr('data-registerid');
			
			if (type == '0') {
				location.href = 'booking2.php';
				return false;
			}
			
			if(type && id) {
				//console.log(type, id);
				var accept_name = $(this).find('input[name="accept_name"]').val();
				var order_name = $(this).find('input[name="order_name"]').val();
				var order_mobile = $(this).find('input[name="order_mobile"]').val();
				var classSubjct = $(this).find('input[name="classSubjct"]').val();
				var teamXsubject = $(this).find('input[name="teamXsubject"]').val();
				var time = $(this).find('input[name="time"]').val();
				var remark = $(this).find('input[name="remark"]').val();
				var total = $(this).find('input[name="total"]').val();
				var checkInUsr = $(this).find('input[name="checkInUsr"]').val();
				var checkInTime = $(this).find('input[name="checkInTime"]').val();
				var cancelUsr = $(this).find('input[name="cancelUsr"]').val();
				var cancelTime = $(this).find('input[name="cancelTime"]').val();
				console.log(total);
				if (type === 0) location.href = 'booking2.php';
				if (!dom.get(0)) return false;
				dom.attr('data-id', id);
				if(type == '2')
					dom.find('#checkInMsg').text(checkInTime+' by '+checkInUsr);
				else if(type == '3')
					dom.find('#cancelMsg').text(cancelTime+' by '+cancelUsr);
				dom.find('.value').eq(0).text(accept_name);
				dom.find('.value').eq(1).text(order_mobile);
				dom.find('.value').eq(2).text(time);
				dom.find('.value').eq(3).text(classSubjct);
				dom.find('.value').eq(4).text(teamXsubject);
				dom.find('.value').eq(5).find('span').text(remark);
				dom.find('.value').eq(5).find('input[name="remark"]').val(remark);
				dom.find('.value').eq(6).find('span').text(total);
				dom.find('.value').eq(6).find('input[name="total"]').val(total);
				/*
				if(type != '1') {
					if(total) {
						dom.find('.value').eq(6).text(total);
					} else {
						dom.find('.value').eq(6).text(null);
					}
				}
				*/
				dom.fadeIn(400);
				
				dom.find('.btn.items').removeClass('selected');
				dom.find('.btn.submit').removeClass('active');
				$('.popup_group').fadeIn(400);
				

			}	
		});

		$('.popup_info[data-type="1"] .btn.items').click(function () {
			$('.popup_info[data-type="1"] .btn.items').removeClass('selected');
			$(this).addClass('selected');
			$('.popup_info[data-type="1"] .btn.submit').removeClass('disable').addClass('active');
		});
		
		$('.popup .close').click(function () {
			$('.popup_group .popup, .popup_group').fadeOut(400);
			$('.btn2, .notes .value span').show();
			$('.notes .value input').hide();
			$('.btn3, .total .value span').show();
			$('.total .value input').hide();
		});
		
		$('.popup_info .btn.submit').click(function () {

			if($(this).is('.active')) {
				var id = $(this).parent().parent().parent().attr('data-id');
				var remark = $(this).parent().parent().parent().find('input[name="remark"]').val();
				var total = $(this).parent().parent().parent().find('input[name="total"]').val();
				var status = $(this).siblings('.selected').attr('data-status');
				//console.log(id, remark, total, $(this).is('.remark'), $(this).is('.total'));
				//return;
				if(($(this).is('.remark') || $(this).is('.total')) && !status) {
					if(!id || (!remark && !total)) {
						return true;
					}
					if(remark) {
						$.ajax({ 
							url: "../action", 
							type: "POST",
							data: {
								action: 'todayManagerRemark', 
								id: id,
								remark: remark,
								token: '<?php echo $_SESSION['token']; ?>'
							}, 
							success: function(e){
								var obj = jQuery.parseJSON(e);
								
								if(obj.error == '0') {
									$('.popup_group .popup, .popup_group').fadeOut(400);
									$('.table div[data-registerid="' + id + '"]').each(function() {
										$(this).find('input[name="remark"]').val(remark);
									});
								}
								
							}
						});
					}	
					if(total) {
						$.ajax({ 
							url: "../action", 
							type: "POST",
							data: {
								action: 'todayManagerTotal', 
								id: id,
								total: total,
								token: '<?php echo $_SESSION['token']; ?>'
							}, 
							success: function(e){
								var obj = jQuery.parseJSON(e);
								
								if(obj.error == '0') {
									$('.popup_group .popup, .popup_group').fadeOut(400);
									$('.table div[data-registerid="' + id + '"]').each(function() {
										$(this).find('input[name="total"]').val(total);
									});
								}
								
							}
						});
					}
				
				} else {
					
					var typeid = 0;
					var adminUsr = $('input[name="adminUsr"]').val();
					if(!id || !status) {
						return true;
					}
					
					if(status == '已報到') {
						typeid = '2';
					} else if(status == '取消') {
						typeid = '3';
					}
					
					$.ajax({ 
						url: "../action", 
						type: "POST",
						data: {
							action: 'todayManager', 
							id: id,
							status: status,
							remark: remark,
							adminUsr: adminUsr,
							total: total,
							token: '<?php echo $_SESSION['token']; ?>'
						}, 
						success: function(e){
							var obj = jQuery.parseJSON(e);
							
							if(obj.error == '0') {
								$('.popup_group .popup, .popup_group').fadeOut(400);				
								$('.table div[data-registerid="' + id + '"]').each(function() {
									$(this).attr('data-type', typeid);
									$(this).find('span').eq(1).text(status);
									$(this).find('input[name="remark"]').val(remark);
									$(this).find('input[name="total"]').val(total);
								});
								
							}
							window.location.reload();
						}
					});
				}
			}
		});
		
		$('.notes input[name="remark"]').blur(function(){
			if($(this).val()) {
				//console.log($(this).parent().parent().parent().parent().attr('data-type'));
				if($(this).parent().parent().parent().parent().attr('data-type') != '1') {
					$(this).parent().parent().siblings().find('.btn.submit').removeClass('disable').addClass('active remark');
				}
			}	
		});
		
		$('.total input[name="total"]').blur(function(){
			if($(this).val()) {
				//console.log($(this).parent().parent().parent().parent().attr('data-type'));
				if($(this).parent().parent().parent().parent().attr('data-type') != '1') {
					$(this).parent().parent().siblings().find('.btn.submit').removeClass('disable').addClass('active total');
				}
			}	
		});
		
		$('.btn2').click(function () {
			$('.btn2, .notes .value span').hide();
			$('.notes .value input').show();
		});
		
		$('.btn3').click(function () {
			$('.btn3, .total .value span').hide();
			$('.total .value input').show();
		});
		
		$('.popSelect2, #selectDay').on('change', function() {
			$('#searchForm').submit();
		});
		
		$('.popup_info[data-type="1"]').find('input[name="remark"]').on('input propertychange', function() {
			if($(this).val()) {
				$('.popup_info[data-type="1"] .btn.submit').removeClass('disable').addClass('active remark');
			}
		});
		
		$('.popup_info[data-type="1"]').find('input[name="total"]').on('input propertychange', function() {
			if($(this).val()) {
				$('.popup_info[data-type="1"] .btn.submit').removeClass('disable').addClass('active total');
			}
		});
	})
</script>
</body>
</html>
