<?php
	include_once("../control/includes/function.php");
    //unset($_SESSION['MyShopping']);
	include_once("./checkAgent.php");
	
	if (empty($_SESSION['expire']) || $_SESSION['expire'] < time()) {
		unset($_SESSION[$_SESSION['token']]);
		$_SESSION['token'] = md5('aibi' . uniqid(microtime())); // create token (fast/sufficient) 
	}// create token (fast/sufficient) 
	$_SESSION['expire'] = time() + 900; // make session valid for next 15 mins
	$_SESSION['visitid'] = $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT'];
	
	$uname = (!$_SESSION["session_uname"] || $_SESSION["session_uname"] != trim($_POST["uname"])) ? trim($_POST["uname"]) : $_SESSION["session_uname"];
	$birthday = (!$_SESSION["session_birthday"] || $_SESSION["session_birthday"] != trim($_POST["birthday"])) ?  trim($_POST["birthday"]) : $_SESSION["session_birthday"];
	$mobile = (!$_SESSION["session_mobile"] || $_SESSION["session_mobile"] != trim($_POST["mobile"])) ?  trim($_POST["mobile"]) : $_SESSION["session_mobile"];
	
	$searchType = ($_POST["search"]) ?  trim($_POST["search"]) : 0;
	
	$_SESSION["session_uname"] = $uname;
	$_SESSION["session_birthday"] = $birthday;
	$_SESSION["session_mobile"] = $mobile;
	
	$_POST["uname"] = $uname;
	$_POST["birthday"] = $birthday;
	$_POST["mobile"] = $mobile;
	
	//if($_SESSION['Member2']['ID'] || $_SESSION['store_id'] == '-1') {
	if($_SESSION['store_id'] == '-1') {	
		$store_id = ($_POST['store_id']) ? $_POST['store_id'] : $xClassRow[0]['web_x_class_id'];	
		//$_SESSION['store_id'] = $store_id;
		$storeRange = 'all';
	} else if($_SESSION['store_id']) {
		$store_id = $_SESSION['store_id'];
		$storeRange = $store_id;
	} else {
		$store_id = $_SESSION['store_id'];
		$storeRange = $store_id;
	}
	
	if(!$uname && !$birthday && !$mobile) {
		die('系統維護中');
	}
	
	$sql = "
		SELECT 
			GROUP_CONCAT(DISTINCT web_x_order.web_member_id SEPARATOR ',') AS web_member_id
		FROM 
			web_x_order
		LEFT Join
			web_member
		ON
			web_member.web_member_id = web_x_order.web_member_id
		where		
			1
	";
/*	
if($store_id) {	
	$sql .= "
		AND 
			web_x_order.store_id = :store_id
	";
}
*/	
if($uname) {	
	$sql .= "
		AND 
			web_x_order.order_name LIKE :uname
	";	
}
if($birthday) {	
	$sql .= "
		AND 
			web_member.birthday = :birthday
	";	
}
if($mobile) {	
	$sql .= "
		AND 
			web_x_order.order_mobile = :mobile
	";	
}
	$excute = array();
	
	$excute = ($uname) ? $excute+array(':uname' => '%'.$uname.'%') : $excute;
	$excute = ($birthday) ? $excute+array(':birthday' => $birthday) : $excute;
	$excute = ($mobile) ? $excute+array(':mobile' => $mobile) : $excute;
	//$excute = ($store_id) ? $excute+array(':store_id' => $store_id) : $excute;
	$pdo = $pdoDB->prepare($sql);
	$pdo->execute($excute);
	$memberRow = $pdo->fetch(PDO::FETCH_ASSOC);
	$memberRowArr = explode(',', $memberRow['web_member_id']);
	$sql = "
		SELECT 
			SQL_CALC_FOUND_ROWS a.*,
			b.web_member_id as bweb_member_id,
			b.uname as buname,
			b.birthday as bbirthday,
			b.mobile as bmobile,
			b.ifUseSelf as bifUseSelf,
			web_order.subject as prodSubjects,
			web_order.web_order_id as web_order_id,
			web_order.web_x_order_ordernum as web_x_order_ordernum,
			web_order.web_product_id as web_order_product_id,
			web_order.dimension as web_order_dimension,
			SUM(web_order.dimension) as storeTotal,
			web_product.web_product_id as web_product_product_id,
			web_product.web_x_product_id as web_product_x_product_id,
			web_product.totalCount as web_product_totalCount,
			web_x_product.web_x_product_id as web_x_product_x_product_id,
			web_x_product.web_xx_product_id as web_x_product_xx_product_id
		FROM 
			web_x_order a
		Left Join 
			web_member b 
		ON 
			b.web_member_id = a.web_member_id
		Left Join
			web_order as web_order
		On
			web_order.web_x_order_ordernum = a.ordernum
		Left Join
			web_product
		On
			web_product.web_product_id = web_order.web_product_id
		Left Join
			web_x_product
		On
			web_x_product.web_x_product_id = web_product.web_x_product_id
		where 
			web_x_product.web_xx_product_id = 2	
		AND
			a.order_type = '0'
		AND
			a.states = '訂單成立'
		AND
			a.paymentstatus = '付款成功'
	";	
	if(count($memberRowArr) > 1) {
		$sql .= "
			AND 
				a.web_member_id IN (".$memberRow['web_member_id'].")
		";	
	} else {
		$sql .= "	
			AND 
				a.web_member_id IN ('".$memberRow['web_member_id']."')
		";
	}
	$sql .= "
		GROUP BY 
			a.web_member_id
		ORDER BY 
			a.cdate ASC
	";
	/*
	$excute = array(
		':memberId'		=> $memberRow['web_member_id'],
	);
	*/
	
	//$debug = new Helper();
	//echo $test = $debug::debugPDO($sql, $excute);
	$pdo = $pdoDB->prepare($sql);
	$pdo->execute();
	$row3 = $pdo->fetchAll(PDO::FETCH_ASSOC);
	
	//echo $Init_All_Discount;
	
?>	
<!DOCTYPE html>
<html lang="en">
<?php include_once('head.php'); ?>
<script src="https://d.line-scdn.net/liff/1.0/sdk.js"></script>
<script>
	//init LIFF
	function initializeApp(data) {
		//取得QueryString
		let urlParams = new URLSearchParams(window.location.search);
		//顯示QueryString
		if(data.context.userId) {
			var userId = data.context.userId;
			$.ajax({ 
				url: "../action", 
				type: "POST",
				data: {action: 'userInfo', lineId: userId, token: '<?php echo $_SESSION['token']; ?>'}, 
				success: function(e){
					var obj = jQuery.parseJSON(e);
					
					if(obj.error == '0') {
						/*
						var output = '';
						$.each(obj, function(k,v) {
							output += k + ': ' + v +'; ';
						});
						alert(output);
						*/
						$('input[name="admin"]').val(obj.web_member_id);
						
					} 
					
				}
			});	
		}	
		
	}
</script>
<style>
._confirm {
	background-color:#665750;
	color:#FFFFFF;
	border-radius: 24px;
	width: 375px;
	height: 48px;
	color: #FFFFFF;
	font-size: 17px;
	font-weight: 500;
	letter-spacing: 1.13px;
	line-height: 48px;
	text-align: center;
	margin: 70px auto 0;
	display: block;
}	
</style>
<body>
<div class="main">
    <?php include_once('header.php'); ?>
    <div class="content coupon-record">
        <a href="stored-search.php" class="back"></a>
        <h2>消費管理</h2>
        <ul class="menu">
            <li><a href="stored.php">收款管理</a></li>
            <li class="active">消費管理</li>
        </ul>
        <div class="filter">
            <ul>
                <li><a><?php echo $uname; ?><?php echo $mobile; ?><?php echo $birthday; ?></a></li>
            </ul>
        </div>
        <div class="table">
	<?php
		foreach($row3 as $key3 => $val3) {
			$sql = "select SUM(total) as useTotal from web_x_order where from_ordernum = '".$val3['bweb_member_id']."' and order_type = 2 AND states = '訂單成立' AND paymentstatus = '付款成功'";
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute();
			$useCountRow = $pdo->fetch(PDO::FETCH_ASSOC);
			
			$finalStoreTotal = abs($val3['storeTotal'] - $useCountRow['useTotal']);
			
			$ifUseSelfText = ($val3['bifUseSelf']) ? "</br>(限本人使用)" : null;
			
			//生日禮
			$sql2 = "select web_member_money_id, money, kind, sdate, edate from web_member_money where web_member_id = '".$val3['bweb_member_id']."' and kind = '1' AND sdate >= '".date('Y-m-01')."' AND edate <= '".date('Y-m-t')."'";
			$pdo2 = $pdoDB->prepare($sql2);
			$pdo2->execute();
			$memberMoneyRow = $pdo2->fetch(PDO::FETCH_ASSOC);
			$memberMoneyText = ($memberMoneyRow['kind'] == '1') ? ' 【本次消費可使用生日禮折抵 $'.number_format($memberMoneyRow['money'])."元】" : '';
	?>		
            <div class="tr">
                <div>
                    <div><span class="name"><?php echo $val3['buname']; ?></span><span class="color">儲值金額 $<span id="balance"><?php echo number_format($finalStoreTotal); ?></span></span><span style="color:red;"><?php echo $ifUseSelfText.$memberMoneyText; ?></span></div>
                    <div class="color">電話：<?php echo $val3['order_mobile']; ?></div>
                    <div class="color">生日：<?php echo $val3['bbirthday']; ?></div>
                </div>
                <div class="right">
                    <a href="stored-record2.php?web_member_id=<?php echo $val3['bweb_member_id']; ?>" class="btn info">紀錄</a>
			<?php
				if($finalStoreTotal > 0) {
			?>	
                    <a href="#" class="btn manager" data-memberid="<?php echo $val3['bweb_member_id']; ?>" data-shipdate="<?php echo $val3['shipdate']; ?>" data-membermoneyid="<?php echo $memberMoneyRow['web_member_money_id']; ?>" data-membermoney="<?php echo $memberMoneyRow['money']; ?>">使用</a>
			<?php
				}
			?>	
                </div>
            </div>
	<?php
		}
	?>		
            
        </div>
    </div>
</div>
<div class="popup_group">
    <div class="popup popup_use2">
        <h2>
            消費管理 <a class="close" style="display:;"></a>
        </h2>
        <div class="content custom">
			<form name="form" id="useForm" method="POST">
				<div class="row">
					<div class="name">店別</div>
					<div class="value">
						<select class="popSelect2" name="store_id">
				<?php
					foreach($xClassRow as $xClassKey => $xClass) {
						if($storeRange != 'all') {
							if($xClass['web_x_class_id'] != $storeRange) {
								continue;
							}
						}	
						$selected = ($xClass['web_x_class_id'] == $store_id) ? "selected='selected'" : null;
				?>		
							<option value="<?php echo $xClass['web_x_class_id']; ?>" <?php echo $selected; ?>><?php echo $xClass['subject']; ?></option>
				<?php
					}
				?>	
						</select>
					</div>
				</div>
		<?php
			$sql5 = "
				SELECT 
					SQL_CALC_FOUND_ROWS
					web_x_product.subject as web_x_product_subject,
					web_x_product.web_x_product_id as web_x_product_x_product_id,
					web_x_product.web_xx_product_id as web_x_product_xx_product_id
				FROM 
					web_x_product
				where 
					web_x_product.web_xx_product_id = 3	
				AND
					web_x_product.ifShow = 1
				ORDER BY
					web_x_product.asort ASC
			";
			$pdo = $pdoDB->prepare($sql5);
			$pdo->execute();
			$row5 = $pdo->fetchAll(PDO::FETCH_ASSOC);
			foreach($row5 as $key5 => $val5) {
				$sql2 = "
					SELECT 
						SQL_CALC_FOUND_ROWS
						web_product.subject as web_product_subject,
						web_product.web_product_id as web_product_product_id,
						web_product.web_x_product_id as web_product_x_product_id,
						web_product.totalCount as web_product_totalCount,
						web_product.price_cost as web_product_price_cost,
						web_product.price_member as web_product_price_member,
						web_x_product.subject as web_x_product_subject,
						web_x_product.web_x_product_id as web_x_product_x_product_id,
						web_x_product.web_xx_product_id as web_x_product_xx_product_id
					FROM 
						web_product
					Left Join
						web_x_product
					On
						web_x_product.web_x_product_id = web_product.web_x_product_id	
					where 
						web_x_product.web_x_product_id = '".$val5['web_x_product_x_product_id']."'	
					AND
						web_x_product.ifShow = 1
					AND
						web_product.ifShow = 1
					ORDER BY
						web_product.displayorder ASC, web_product.price_member DESC	
				";
				//$debug = new Helper();
				//echo $test = $debug::debugPDO($sql2, $excute);
				$pdo = $pdoDB->prepare($sql2);
				$pdo->execute();
				$row4 = $pdo->fetchAll(PDO::FETCH_ASSOC);
				if($val5['web_x_product_x_product_id'] != '8') {
				
		?>	
				<div class="row">
					<div class="name"><?php echo $val5['web_x_product_subject'];?></div>
					<div class="value">
			<?php
				foreach($row4 as $key4 => $val4) {	
			?>	
						<input type="checkbox" id="r<?php echo $val4['web_product_product_id']; ?>" name="web_product_id[]" value="<?php echo $val4['web_product_product_id']; ?>" data-prodsubject="<?php echo $val5['web_x_product_subject']." - ".$val4['web_product_subject'];?>" data-prodprice="<?php echo $val4['web_product_price_member']; ?>">
						<label for="r<?php echo $val4['web_product_product_id']; ?>"><?php echo $val4['web_product_subject']; ?><br><?php echo $val4['web_product_price_member']; ?></label>			
			<?php
				}
			?>	
					</div>
				</div>
			<?php } else { ?>	
				<div class="row">
					<div class="name">自訂</div>
					<div class="row2">
						<div>
							<div class="name">名稱</div>
							<div class="value">
								<input type="text" name="customName" placeholder="請輸入名稱">
							</div>
						</div>
						<div>
							<div class="name">金額</div>
							<div class="value">
								<input type="text" name="customNamePrice" placeholder="請輸入金額">
							</div>
						</div>
					</div>
				</div>
				
		<?php
				}
			}
		?>	
				<a href="#" class="btn submit">確認</a>
				<input class="form-control" type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>" />
				<input class="form-control" type="hidden" name="shipdate" value="" />
				<input class="form-control" type="hidden" name="ordernum" value="" />
				<input class="form-control" type="hidden" name="web_member_id" value="" />
				<input class="form-control" type="hidden" name="action" value="useOrder2" />
				<input class="form-control" type="hidden" name="web_member_money_id" value="" />
				<input class="form-control" type="hidden" name="web_member_money" value="" />
				<input class="form-control" type="hidden" name="Init_All_Discount" value="<?php echo $Init_All_Discount; ?>" />
				<input class="form-control" type="hidden" name="admin" value="<?php echo $_SESSION['Member2']['ID']; ?>" />
			</form>	
		</div>
    </div>
	
	<div class="popup popup_alert">
		<h2>消費管理</h2>
	</div>
	
	
	<div class="popup popup_alert2" style="width: 35%; display:none;">
		<h2>
			消費明細 
			<a class="close" style="display:;"></a>
		</h2>
		<div class="list" style="padding-top:15px;"></div>
		<a href="#" class="_confirm">確認</a>
	</div>
	
</div>
<script>
	$(function () {
		liff.init(function (data) {
			initializeApp(data);
		});
		
		$('.popup .back, .popup_use2 .submit').on('click', function () {
			var prodItems = [];
			var prodTotal = 0;
			var prodList = '';
			var store_id = $('select[name="store_id"]').find('option:selected').val();
			var memberMoney = $('input[name="web_member_money"]').val();
			var Init_All_Discount = $('input[name="Init_All_Discount"]').val();
			if(!store_id) {
				$('.popup_group .popup, .popup_group').hide();
				$('.popup_group, .popup_alert').fadeIn(400).find('h2').text('請選店別');
				setTimeout(function(){
					$('.popup_group .popup, .popup_group').fadeOut(400);
				},1000);
				return false;
			}
			
			$('.popup_group .popup, .popup_group').hide();
			
			$.each($('input[type="checkbox"]:checked'),function(e,v){
				//console.log(e,v,$(v).attr('data-prodsubject'),$(v).attr('data-prodprice'));
				prodItems.push($(v).attr('data-prodsubject')+' '+$(v).attr('data-prodprice'));
				prodTotal += parseInt($(v).attr('data-prodprice'));
				prodList += '<span style=\"font-size:18px; line-height:32px;\">'+$(v).attr('data-prodsubject')+'<span style=\"font-size:18px; float:right;\">'+$(v).attr('data-prodprice')+'</span></span></br>';
			});
			
			if($('input[name="customName"]').val() && $('input[name="customNamePrice"]').val()) {
				prodList += '<span style=\"font-size:18px; line-height: 32px;\">自訂 - '+$('input[name="customName"]').val()+'<span style=\"font-size:18px; float:right;\">'+$('input[name="customNamePrice"]').val()+'</span></span></br>';
				prodTotal += parseInt($('input[name="customNamePrice"]').val());
			}
			
			if(memberMoney) {
				prodList += '<span style=\"font-size:18px; line-height: 32px;\">生日禮 '+$('input[name="customName"]').val()+'<span style=\"font-size:18px; float:right;\">-'+memberMoney+'</span></span></br>';
				
				if(memberMoney >= prodTotal) {
					prodTotal = prodTotal - parseInt(prodTotal);
				} else {
					prodTotal = prodTotal - parseInt(memberMoney);
				}	
			}
			
			prodList += "<hr><span style=\"font-size:18px;\"><span style='float:right;font-size:18px;'>總計："+prodTotal+"</span>";
			
			if(Init_All_Discount != '0') {
				prodTotal = Math.round(prodTotal * (parseInt(Init_All_Discount) / 100));
				prodList += "<br><span style='float:right;font-size:18px;'>儲值"+(parseInt(Init_All_Discount) / 100) * 10 +" 折："+prodTotal+"</span>";
			}
			
			prodList += "</span>";
			
			$('.popup_group, .popup_alert2').fadeIn(400).find('.list').html(prodList);
			
		});
		
		$('._confirm').on('click', function () {
			var prodItems = [];
			var prodTotal = 0;
			var prodList = '';
			var store_id = $('select[name="store_id"]').find('option:selected').val();
			if(!store_id) {
				$('.popup_group .popup, .popup_group').hide();
				$('.popup_group, .popup_alert').fadeIn(400).find('h2').text('請選店別');
				setTimeout(function(){
					$('.popup_group .popup, .popup_group').fadeOut(400);
				},1000);
				return false;
			}
			
			$.ajax({ 
				url: "../action", 
				type: "POST",
				data: $("#useForm").serialize(), 
				success: function(e){
					var obj = jQuery.parseJSON(e);
					/*
					var output = '';
					$.each(obj, function(k,v) {
						output += k + ': ' + v +'; ';
					});
					return false;
					*/
					if(obj.error != '0') {
						
						$('.popup_group .popup, .popup_group').hide();
						$('.popup_group, .popup_alert').fadeIn(400).find('h2').text(obj.message);
						
						setTimeout(function(){
							$('.popup_group .popup, .popup_group').fadeOut(400);
							$('.popup_group, .popup.popup_use2').fadeIn(400);
						},1000);
						
						return false;
					}
					if(obj.error == '0') {
						
						$('.popup_group .popup, .popup_group').hide();
						$('.popup_group, .popup_alert').fadeIn(400).find('h2').text('使用成功');
						$('#balance').text(obj.balance);
						setTimeout(function(){
							$('.popup_group .popup, .popup_group').fadeOut(400);
							location.reload();
						},2000);
						//$('.tr').eq(dataId).hide();
						//$("#canUseRow").text(parseInt($("#canUseRow").text() - 1));
					}
											
				}
			});
		});
		
		$('.popup .close').click(function () {
			$('.popup_group .popup, .popup_group').fadeOut(400);
		});
		
		$('.btn.manager').click(function () {
			$('.popup_group, .popup.popup_use2').fadeIn(400);
			var web_member_id = $(this).attr('data-memberid');
			var shipdate = $(this).attr('data-shipdate');
			var web_member_money_id = $(this).attr('data-membermoneyid');
			var web_member_money = $(this).attr('data-membermoney');
			$('input[name="web_member_id"], input[name="ordernum"]').val(web_member_id);
			$('input[name="web_member_money_id"]').val(web_member_money_id);
			$('input[name="web_member_money"]').val(web_member_money);
			$('input[name="shipdate"]').val(shipdate);
		});
	})
</script>
</body>
</html>
