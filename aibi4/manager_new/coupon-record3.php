<?php
	include_once("../control/includes/function.php");
    //unset($_SESSION['MyShopping']);
	include_once("./checkAgent.php");
	
	if (empty($_SESSION['expire']) || $_SESSION['expire'] < time()) {
		unset($_SESSION[$_SESSION['token']]);
		$_SESSION['token'] = md5('aibi' . uniqid(microtime())); // create token (fast/sufficient) 
	}// create token (fast/sufficient) 
	$_SESSION['expire'] = time() + 900; // make session valid for next 15 mins
	$_SESSION['visitid'] = $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT'];
	
	$web_x_order_id= intval($_GET["web_x_order_id"]) ? intval($_GET["web_x_order_id"]) : null;
	
	$refererInfo = parse_url($_SERVER['HTTP_REFERER']);
	
	if(!$refererInfo || ($refererInfo['path']) != '/manager_new/coupon-record.php' || !$web_x_order_id) {
		RunJs("./coupon-search.php");
	}
	
	//if($_SESSION['Member2']['ID'] || $_SESSION['store_id'] == '-1') {
	if($_SESSION['store_id'] == '-1') {	
		$store_id = ($_POST['store_id']) ? $_POST['store_id'] : $xClassRow[0]['web_x_class_id'];	
		//$_SESSION['store_id'] = $store_id;
		$storeRange = 'all';
	} else if($_SESSION['store_id']) {
		$store_id = $_SESSION['store_id'];
		$storeRange = $store_id;
	} else {
		$store_id = $_SESSION['store_id'];
		$storeRange = $store_id;
	}
		
	$sql = "
		SELECT 
			web_x_order.*,
			web_order.subject as prodSubjects,
			web_order.web_order_id as web_order_id,
			web_order.web_x_order_ordernum as web_x_order_ordernum,
			web_order.web_product_id as web_order_product_id,
			web_product.web_product_id as web_product_product_id,
			web_product.web_x_product_id as web_product_x_product_id,
			web_product.totalCount as web_product_totalCount,
			web_product.Covers as web_product_Covers,
			web_product.useEdate as web_product_useEdate,
			web_x_product.web_x_product_id as web_x_product_x_product_id,
			web_x_product.web_xx_product_id as web_x_product_xx_product_id
		from 
			web_x_order
		Left Join
			web_order as web_order
		On
			web_order.web_x_order_ordernum = web_x_order.ordernum
		Left Join
			web_product
		On
			web_product.web_product_id = web_order.web_product_id
		Left Join
			web_x_product
		On
			web_x_product.web_x_product_id = web_product.web_x_product_id
		WHERE 
			web_x_order.web_x_order_id = :web_x_order_id
		AND
			web_x_order.states = '訂單成立'
		AND
			web_x_order.paymentstatus = '付款成功'
		AND
			web_x_order.order_type = 0
		AND
			web_x_product.web_xx_product_id = 1	
	";
	
	$excute = array(
		':web_x_order_id'		=> $web_x_order_id,
	);
	//$debug = new Helper();
	//echo $test = $debug::debugPDO($sql, $excute);
	$pdo = $pdoDB->prepare($sql);
	$pdo->execute($excute);
	$row3 = $pdo->fetch(PDO::FETCH_ASSOC);
	
	$sql = "
		SELECT 
			web_x_order.*,
			web_order.subject as prodSubjects,
			web_order.web_order_id as web_order_id,
			web_order.web_x_order_ordernum as web_x_order_ordernum,
			web_order.web_product_id as web_order_product_id,
			web_product.web_product_id as web_product_product_id,
			web_product.web_x_product_id as web_product_x_product_id,
			web_product.totalCount as web_product_totalCount,
			web_product.Covers as web_product_Covers,
			web_product.useEdate as web_product_useEdate,
			web_x_product.web_x_product_id as web_x_product_x_product_id,
			web_x_product.web_xx_product_id as web_x_product_xx_product_id
		from 
			web_x_order
		Left Join
			web_order as web_order
		On
			web_order.web_x_order_ordernum = web_x_order.ordernum
		Left Join
			web_product
		On
			web_product.web_product_id = web_order.web_product_id
		Left Join
			web_x_product
		On
			web_x_product.web_x_product_id = web_product.web_x_product_id
		WHERE 
			web_x_order.from_ordernum = :ordernum
		AND
			web_x_order.states = '訂單成立'
		AND
			web_x_order.paymentstatus = '付款成功'	
		AND
			web_x_order.order_type = 1
		AND
			web_x_product.web_xx_product_id = 1		
	";
	
	$excute = array(
		':ordernum'		=> $row3['ordernum'],
	);
	//$debug = new Helper();
	//echo $test = $debug::debugPDO($sql, $excute);
	$pdo = $pdoDB->prepare($sql);
	$pdo->execute($excute);
	$row4 = $pdo->fetchAll(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html lang="en">
<?php include_once('head.php'); ?>
<script src="https://d.line-scdn.net/liff/1.0/sdk.js"></script>
<script>
	//init LIFF
	function initializeApp(data) {
		//取得QueryString
		let urlParams = new URLSearchParams(window.location.search);
		//顯示QueryString
		if(data.context.userId) {
			var userId = data.context.userId;
			$.ajax({ 
				url: "../action", 
				type: "POST",
				data: {action: 'userInfo', lineId: userId, token: '<?php echo $_SESSION['token']; ?>'}, 
				success: function(e){
					var obj = jQuery.parseJSON(e);
					
					if(obj.error == '0') {
						/*
						var output = '';
						$.each(obj, function(k,v) {
							output += k + ': ' + v +'; ';
						});
						alert(output);
						*/
						$('input[name="admin"]').val(obj.web_member_id);
						
					} 
					
				}
			});	
		}	
		
	}
</script>
<body>
<div class="main">
    <?php include_once('header.php'); ?>
    <div class="content coupon-record3">
        <a class="back" style="cursor:pointer;"></a>
		<form name="form" id="backForm" action="coupon-record.php" method="POST">
			<input type="hidden" value="<?php echo $_SESSION["session_uname"]; ?>" name="uname" />
			<input type="hidden" value="<?php echo $_SESSION["session_birthday"]; ?>" name="birthday" />
			<input type="hidden" value="<?php echo $_SESSION["session_mobile"]; ?>" name="mobile" />
		</form>
        <h2>使用管理</h2>
        <ul class="menu">
            <li><a href="coupon.php">收款管理</a></li>
            <li class="active">使用管理</li>
        </ul>
        <h3>可使用票券：<span id="canUseRow"><?php echo ceil($row3['web_product_totalCount'] - count($row4)); ?></span></h3>
        <div class="table">
	<?php
		for($i=0; $i<ceil($row3['web_product_totalCount'] - count($row4)); $i++) {
			$pic = ShowPic($row3['web_product_Covers'], "../uploadfiles/l/", "../uploadfiles/no_image.jpg");
			$avgPrice = abs($row3['total'] / $row3['web_product_totalCount']);
	?>	
            <div class="tr">
                <div class="pic" style="background-image: url('<?php echo $pic; ?>')">
                </div>
                <div class="info">
                    <div>
                        <div class="name"><?php echo $row3['prodSubjects']; ?></div>
                        <div class="">使用期限：<?php echo date('Y/m/d', strtotime($row3['web_product_useEdate'])); ?></div>
						<div class="">單次費用：<?php echo number_format($avgPrice); ?></div>
                    </div>
		<?php
			//使用期限設定
			if(strtotime($row3['web_product_useEdate']) > 1) {
				if(time() <= (strtotime($row3['web_product_useEdate']) + 86399)) {
		?>	
                    <a href="#" class="btn" data-count="<?php echo $i; ?>">使用</a>
		<?php
				}
			}
		?>	
                </div>
            </div>
    <?php
		}	
	?>	
        </div>
    </div>
</div>
<div class="popup_group">
    <div class="popup popup_use" data-id="">
		<form name="form" id="useForm" method="POST">
			<h2>確認使用嗎？</h2>
			<div class="btns">
				<div class="value" style="padding-top:20px;">
					<select class="popSelect" name="store_id">
			<?php
				foreach($xClassRow as $xClassKey => $xClass) {
					if($storeRange != 'all') {
						if($xClass['web_x_class_id'] != $storeRange) {
							continue;
						}
					}	
					$selected = ($xClass['web_x_class_id'] == $store_id) ? "selected='selected'" : null;
			?>		
						<option value="<?php echo $xClass['web_x_class_id']; ?>" <?php echo $selected; ?>><?php echo $xClass['subject']; ?></option>
			<?php
				}
			?>	
					</select>
				</div>
				<a href="#" class="btn cancel">取消</a>
				<a href="#" class="btn submit">確認</a>
			</div>
			<input class="form-control" type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>" />
			<input class="form-control" type="hidden" name="shipdate" value="<?php echo $row3['shipdate']; ?>" />
			<input class="form-control" type="hidden" name="ordernum" value="<?php echo $row3['ordernum']; ?>" />
			<input class="form-control" type="hidden" name="web_member_id" value="<?php echo $row3['web_member_id']; ?>" />
			<input class="form-control" type="hidden" name="web_product_id" value="<?php echo $row3['web_product_product_id']; ?>" />
			<input class="form-control" type="hidden" name="action" value="useOrder" />
			<input class="form-control" type="hidden" name="admin" value="<?php echo $_SESSION['Member2']['ID']; ?>" />
		</form>	
    </div>
	
	<div class="popup popup_alert">
		<!--<a class="close" href="#"></a>-->
		<h2>預約管理</h2>
		<!--<a href="#" class="btn submit disable">確認</a>-->
	</div>
</div>
<script>
	$(function () {
		liff.init(function (data) {
			initializeApp(data);
		});
		
		$('.info .btn').click(function () {
			$('.popup_group, .popup_use').attr('data-id', $(this).attr('data-count')).fadeIn(400);
			//alert($('.tr').length);
		});
		$('.popup .cancel, .popup_use .submit').on('click', function () {
			if($(this).is('.submit')) {
				var dataId = $('.popup .cancel, .popup_use').attr('data-id');
				if(!dataId) return false;
				var store_id = $('select[name="store_id"]').find('option:selected').val();
				if(!store_id) {
					$('.popup_group .popup, .popup_group').hide();
					$('.popup_group, .popup_alert').fadeIn(400).find('h2').text('請選店別');
					setTimeout(function(){
						$('.popup_group .popup, .popup_group').fadeOut(400);
					},1000);
					return false;
				}
				$.ajax({ 
					url: "../action", 
					type: "POST",
					data: $("#useForm").serialize(), 
					success: function(e){
						var obj = jQuery.parseJSON(e);
						/*
						var output = '';
						$.each(obj, function(k,v) {
							output += k + ': ' + v +'; ';
						});
						return false;
						*/
						if(obj.error != '0') {
							
							$('.popup_group .popup, .popup_group').hide();
							$('.popup_group, .popup_alert').fadeIn(400).find('h2').text(obj.message);
							
							setTimeout(function(){
								$('.popup_group .popup, .popup_group').fadeOut(400);
							},1000);
							
							return false;
						}
						if(obj.error == '0') {
							
							$('.popup_group .popup, .popup_group').hide();
							$('.popup_group, .popup_alert').fadeIn(400).find('h2').text('使用成功');
							
							setTimeout(function(){
								$('.popup_group .popup, .popup_group').fadeOut(400);
							},1000);
							$('.tr').eq(dataId).hide();
							$("#canUseRow").text(parseInt($("#canUseRow").text() - 1));
						}
												
					}
				});
				
			}
			$('.popup_group .popup, .popup_group').fadeOut(400);
		});
		
		$('.back').click(function(e) {
			e.preventDefault();
			$('#backForm').submit();
			return false;
		});
	})
</script>
</body>
</html>
