<?php
	$agent = $_SERVER['HTTP_USER_AGENT'];
	if (!preg_match("#\bLine\b#", $agent)) {
		if(!isset($_SESSION['Member2'])) {
			RunJs("./logout.php");
		}
		
	}
?>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $Init_WebTitle; ?> 管理端</title>
	<script src="https://d.line-scdn.net/liff/1.0/sdk.js"></script>
    <link rel="stylesheet" href="../css/manager.css"/>
	<link rel="stylesheet" href="../css/manager2.css"/>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="../js/jquery.cookie.js" type="text/javascript"></script>
	<script src="../js/jquery.validate.js" type="text/javascript"></script>
	<script src="../js/additional-methods.js"></script>
	<script src="../js/jquery.cookie.js" type="text/javascript"></script>
	<style>
		.error {
			/*margin-top:-10px;*/
			font-size:14px;
			color:red;
		}
	</style>
	<link rel="manifest" href="../manifest.json">
	<script src="https://www.gstatic.com/firebasejs/4.8.1/firebase-app.js"></script>
	<script src="https://www.gstatic.com/firebasejs/4.8.1/firebase-auth.js"></script>
	<script src="https://www.gstatic.com/firebasejs/4.8.1/firebase-messaging.js"></script>
	<script>
		var messaging = null;
			
		var notification = null;
		function ShowNotification(title, body) {
			notification = new Notification(title, {
				//icon: '/icon/ms-icon-310x310.png',
				body: body,
				onclick: function (event) {
					//event.preventDefault(); // prevent the browser from focusing the Notification's tab
					//window.open('https://airconcept.aibitechcology.com/admin/web_chat/web_chat_list.php', '_blank');
					parent.focus();
					window.focus(); //just in case, older browsers
					this.close();
				}
			})
		}
		
		var config = {
			apiKey: "AIzaSyAoR8p0dcPP5_4krh4-B4n31BKfg1gF7To",
			authDomain: "aibi4-62739.firebaseapp.com",
			databaseURL: "https://aibi4-62739.firebaseio.com",
			projectId: "aibi4-62739",
			storageBucket: "aibi4-62739.appspot.com",
			messagingSenderId: "768333202033",
			appId: "1:768333202033:web:69ad8dd0983388c2bc11d0",
			measurementId: "G-NL6M5ET3SX"

		};
		firebase.initializeApp(config);
		
		messaging = firebase.messaging();
		
		//收到訊息後的處理
		messaging.onMessage(function (payload) {
			
			//如果可以顯示通知就做顯示通知
			if (Notification.permission === 'granted') {
				ShowNotification(payload.notification.title, payload.notification.body);
				//三秒後自動關閉
				setTimeout(notification.close.bind(notification), 3000);
			}
		});	
	</script>		
</head>