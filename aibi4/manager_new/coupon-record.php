<?php
	include_once("../control/includes/function.php");
    //unset($_SESSION['MyShopping']);
	include_once("./checkAgent.php");
	
	if (empty($_SESSION['expire']) || $_SESSION['expire'] < time()) {
		unset($_SESSION[$_SESSION['token']]);
		$_SESSION['token'] = md5('aibi' . uniqid(microtime())); // create token (fast/sufficient) 
	}// create token (fast/sufficient) 
	$_SESSION['expire'] = time() + 900; // make session valid for next 15 mins
	$_SESSION['visitid'] = $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT'];
	
	$uname = (!$_SESSION["session_uname"] || $_SESSION["session_uname"] != trim($_POST["uname"])) ? trim($_POST["uname"]) : $_SESSION["session_uname"];
	$birthday = (!$_SESSION["session_birthday"] || $_SESSION["session_birthday"] != trim($_POST["birthday"])) ?  trim($_POST["birthday"]) : $_SESSION["session_birthday"];
	$mobile = (!$_SESSION["session_mobile"] || $_SESSION["session_mobile"] != trim($_POST["mobile"])) ?  trim($_POST["mobile"]) : $_SESSION["session_mobile"];
	
	$searchType = ($_POST["search"]) ?  trim($_POST["search"]) : 0;
	
	$_SESSION["session_uname"] = $uname;
	$_SESSION["session_birthday"] = $birthday;
	$_SESSION["session_mobile"] = $mobile;
	
	$_POST["uname"] = $uname;
	$_POST["birthday"] = $birthday;
	$_POST["mobile"] = $mobile;
	
	//if($_SESSION['Member2']['ID'] || $_SESSION['store_id'] == '-1') {
	if($_SESSION['store_id'] == '-1') {	
		$store_id = ($_POST['store_id']) ? $_POST['store_id'] : $xClassRow[0]['web_x_class_id'];	
		//$_SESSION['store_id'] = $store_id;
		$storeRange = 'all';
	} else if($_SESSION['store_id']) {
		$store_id = $_SESSION['store_id'];
		$storeRange = $store_id;
	} else {
		$store_id = $_SESSION['store_id'];
		$storeRange = $store_id;
	}
	
	if(!$uname && !$birthday && !$mobile) {
		die('系統維護中');
	}
	
	$sql = "
		SELECT 
			GROUP_CONCAT(DISTINCT web_x_order.web_member_id SEPARATOR ',') AS web_member_id
		FROM 
			web_x_order
		LEFT Join
			web_member
		ON
			web_member.web_member_id = web_x_order.web_member_id
		where		
			1
	";
/*	
if($store_id) {	
	$sql .= "
		AND 
			web_x_order.store_id = :store_id
	";
}
*/	
if($uname) {	
	$sql .= "
		AND 
			web_x_order.order_name LIKE :uname
	";	
}
if($birthday) {	
	$sql .= "
		AND 
			web_member.birthday = :birthday
	";	
}
if($mobile) {	
	$sql .= "
		AND 
			web_x_order.order_mobile = :mobile
	";	
}
	$excute = array();
	
	$excute = ($uname) ? $excute+array(':uname' => '%'.$uname.'%') : $excute;
	$excute = ($birthday) ? $excute+array(':birthday' => $birthday) : $excute;
	$excute = ($mobile) ? $excute+array(':mobile' => $mobile) : $excute;
	//$excute = ($store_id) ? $excute+array(':store_id' => $store_id) : $excute;	
	$pdo = $pdoDB->prepare($sql);
	$pdo->execute($excute);
	$memberRow = $pdo->fetch(PDO::FETCH_ASSOC);
	$memberRowArr = explode(',', $memberRow['web_member_id']);
	$sql = "
		SELECT 
			SQL_CALC_FOUND_ROWS a.*,
			b.web_member_id as bweb_member_id,
			b.uname as buname,
			b.birthday as bbirthday,
			b.mobile as bmobile,
			b.ifUseSelf as bifUseSelf,
			web_order.subject as prodSubjects,
			web_order.web_order_id as web_order_id,
			web_order.web_x_order_ordernum as web_x_order_ordernum,
			web_order.web_product_id as web_order_product_id,
			web_product.web_product_id as web_product_product_id,
			web_product.web_x_product_id as web_product_x_product_id,
			web_product.totalCount as web_product_totalCount,
			web_x_product.web_x_product_id as web_x_product_x_product_id,
			web_x_product.web_xx_product_id as web_x_product_xx_product_id
		FROM 
			web_x_order a
		Left Join 
			web_member b 
		ON 
			b.web_member_id = a.web_member_id
		Left Join
			web_order as web_order
		On
			web_order.web_x_order_ordernum = a.ordernum
		Left Join
			web_product
		On
			web_product.web_product_id = web_order.web_product_id
		Left Join
			web_x_product
		On
			web_x_product.web_x_product_id = web_product.web_x_product_id	
		where 
			web_x_product.web_xx_product_id = 1	
		AND
			a.order_type = '0'
		AND
			a.states = '訂單成立'
		AND
			a.paymentstatus = '付款成功'
	";		
	if(count($memberRowArr) > 1) {
		$sql .= "
			AND 
				a.web_member_id IN (".$memberRow['web_member_id'].")
		";	
	} else {
		$sql .= "	
			AND 
				a.web_member_id IN ('".$memberRow['web_member_id']."')
		";
	}
	$sql .= "
		ORDER BY 
			a.cdate DESC
	";
	/*
	$excute = array(
		':memberId'		=> $memberRow['web_member_id'],
	);
	*/
	//$debug = new Helper();
	//echo $test = $debug::debugPDO($sql, $excute);
	$pdo = $pdoDB->prepare($sql);
	$pdo->execute();
	$row3 = $pdo->fetchAll(PDO::FETCH_ASSOC);
	
?>	
<!DOCTYPE html>
<html lang="en">
<?php include_once('head.php'); ?>
<body>
<div class="main">
    <?php include_once('header.php'); ?>
    <div class="content coupon-record">
        <a href="coupon-search.php" class="back"></a>
        <h2>使用管理</h2>
        <ul class="menu">
            <li><a href="coupon.php">收款管理</a></li>
            <li class="active">使用管理</li>
        </ul>
        <div class="filter">
            <ul>
                <li><a><?php echo $uname; ?><?php echo $mobile; ?><?php echo $birthday; ?></a></li>
            </ul>
        </div>
        <div class="table">
	<?php
		foreach($row3 as $key3 => $val3) {
			$sql = "select COUNT(web_x_order_id) as useCount from web_x_order where from_ordernum = '".$val3['ordernum']."' and order_type = 1 AND states = '訂單成立' AND paymentstatus = '付款成功'";
			$pdo = $pdoDB->prepare($sql);
			$pdo->execute();
			$useCountRow = $pdo->fetch(PDO::FETCH_ASSOC);
			
			$ifUseSelfText = ($val3['bifUseSelf']) ? "(限本人使用)" : null;
	?>		
            <div class="tr">
                <div>
                    <div><span class="name"><?php echo $val3['buname']." 【".$val3['prodSubjects']."】"; ?></span><span class="color2">可使用票券：<?php echo ceil($val3['web_product_totalCount'] - $useCountRow['useCount']); ?></span><span style="color:red;"><?php echo $ifUseSelfText; ?></span></div>
                    <div class="color">電話：<?php echo $val3['order_mobile']; ?></div>
                    <div class="color">生日：<?php echo $val3['bbirthday']; ?></div>
                </div>
                <div class="right">
                    <a href="coupon-record2.php?web_x_order_id=<?php echo $val3['web_x_order_id']; ?>" class="btn info">紀錄</a>
                    <a href="coupon-record3.php?web_x_order_id=<?php echo $val3['web_x_order_id']; ?>" class="btn manager">使用</a>
                </div>
            </div>
    <?php
		}
	?>	
        </div>
    </div>
</div>
</body>
</html>
