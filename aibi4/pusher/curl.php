<?php
	
	function array_implode($glue, $separator, $array) {
        if (!is_array($array)) {
            return $array;
        }

        $string = array();
        foreach ($array as $key => $val) {
            if (is_array($val)) {
                $val = implode(',', $val);
            }
            $string[] = "{$key}{$glue}{$val}";
        }

        return implode($separator, $string);
    }
	
	function curlPusher($message) {
		
		if(!$message) {
			return false;
		}
	
		$app_id = "907557";
		$cluster = "ap1";
		$auth_key = '039734996a5a25e144b5';
		$auth_secret = 'd13a659ae1a2967e82ff';
		$request_method = "POST";
		$auth_version = '1.0';
		$auth_timestamp = null;
		$query_params = array();
		$s_url = '/apps/'.$app_id.'/events';
		$request_path = $s_url;
		
		$ch = curl_init();
		$_header = array(
			'Content-Type: application/json',
			'Expect:',
			'X-Pusher-Library: pusher-http-php 3.0.0',
		);
		$channels = array('my-channel');
		$data['message'] = $message;
		$post_params = array();
		$post_params['name'] = "my-event";
		$post_params['data'] = json_encode($data);
		$post_params['channels'] = array_values($channels);
		
		$post_value = json_encode($post_params);
		$query_params['body_md5'] = md5($post_value);
		
		$params = array();
		$params['auth_key'] = $auth_key;
		$params['auth_timestamp'] = (is_null($auth_timestamp) ? time() : $auth_timestamp);
		$params['auth_version'] = $auth_version;

		$params = array_merge($params, $query_params);
		ksort($params);
		
		$string_to_sign = "$request_method\n".$request_path."\n".array_implode('=', '&', $params);
		
		$auth_signature = hash_hmac('sha256', $string_to_sign, $auth_secret, false);

		$params['auth_signature'] = $auth_signature;
		ksort($params);
		
		$auth_query_string = array_implode('=', '&', $params);

		
		$full_url = "https://api-".$cluster.".pusher.com:443".$request_path.'?'.$auth_query_string;
		
		$full_url = $full_url."&body_md5=".$query_params['body_md5'];
		
		curl_setopt($ch, CURLOPT_URL, $full_url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $_header);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_value);
		$result = curl_exec($ch);
		curl_close($ch);
	}	
	
	
	function pushFirebase($from, $message, $image, $store) {
		
		global $DB, $pdoDB;
		
		define( 'API_SERVER_ACCESS_KEY', 'AAAAsuQ6QnE:APA91bHE5pDPt1-2oyjOmfmWGf9ZmZ1jDSFi7xvZ0MbFVAmS_GtoJXL3psbAnNNl8FbwXydDFW5ZP-ceWRvVLi1J3MF-zXu4qrjvrk9yZUu3UDvMYPM9_rR2W6ymE2x4LIE8wHSYgGup' );
		
		$sql = "SELECT * from (SELECT * FROM web_firebasetoken ORDER BY date DESC LIMIT 9999) e GROUP BY ip";
		$pdo = $pdoDB->prepare($sql);
	    $pdo->execute();
	    $tokenRow = $pdo->fetchAll(PDO::FETCH_ASSOC);
		
		$sql2 = "SELECT * from web_x_class Where web_x_class_id = '".$store."'";
		$pdo2 = $pdoDB->prepare($sql2);
	    $pdo2->execute();
	    $stroeSubject = $pdo2->fetch(PDO::FETCH_ASSOC);
	
		foreach($tokenRow as $tokenKey => $token) {
			
			$token 	 = $token['token'];/*FCM 接收端的token*/
			//$message = $from;/*要接收的內容*/
			//$title 	 = 'AiBI智慧沙龍示範店';  /*要接收的標題*/
			$title = $stroeSubject['subject'];
			/*
			notification組成格式 官方範例
			{
			  "message":{
				"token":"bk3RNwTe3H0:CI2k_HHwgIpoDKCIZvvDMExUdFQ3P1...",
				"notification":{
				  "title":"Portugal vs. Denmark",
				  "body":"great match!"
				}
			  }
			}
			*/
			
			$content = array
			(
				'title'	=> $title,
				'body' 	=> $message,
				//'image'	=> $image,
				'data'	=> $from
			);
			
			$webpush = array
			(
				'fcm_options'	=> array(
					'link'	=> 'https://airconcept.aibitechcology.com/chat.php'
				),
				
			);
			
			$fields = array
			(
				'to'		    => $token,
				'notification'	=> $content,
				'webpush' 		=> $webpush,

			);
			
			//firebase認證 與 傳送格式
			$headers = array
			(
				'Authorization: key='. API_SERVER_ACCESS_KEY,
				'Content-Type: application/json'
			);
			
			/*curl至firebase server發送到接收端*/
			$ch = curl_init();//建立CURL連線
			curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
			curl_setopt( $ch,CURLOPT_POST, true );
			curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
			$result = curl_exec($ch );
			curl_close( $ch );//關閉CURL連線
			//result 是firebase server的結果
			echo $result;
			$resAry = json_decode($result, true);
			if($resAry['results'][0]['error'] == 'NotRegistered') {
				$sql = "Delete From web_firebasetoken Where token = :token ";
				$excute = array(
					':token'        => $token,
				);
				$pdo = $pdoDB->prepare($sql);
				$pdo->execute($excute);
			}
			file_put_contents("./debug.txt", "============To Data=========\r\n",FILE_APPEND);
			file_put_contents("./debug.txt", $token."\n".($result)."\r\n",FILE_APPEND);
			//file_put_contents("./debug.txt", $resAry['results'][0]['error']."\r\n",FILE_APPEND);
		}
	}
	
	//curlPusher(date('Y-m-d H:i:s'));