<?php
    include_once("./control/includes/function.php");
	ini_set('session.cookie_httponly', 1 );
	session_start();

	if (empty($_SESSION['expire']) || $_SESSION['expire'] < time()) $_SESSION['token'] = md5('aibi' . uniqid(microtime())); // create token (fast/sufficient) 
	$_SESSION['expire'] = time() + 900; // make session valid for next 15 mins
	$_SESSION['visitid'] = $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT'];
	
	$agent = $_SERVER['HTTP_USER_AGENT'];
	if (!preg_match("#\bLine\b#", $agent)) {
		//die('aibi');
	}
	$web_member_id = $_REQUEST['web_member_id'];
	if(!$web_member_id) {
		die('aibi');
	}
	
	$sql = "
		Select 
			SQL_CALC_FOUND_ROWS a.*,
			b.subject as bsubject,
			b.web_product_id as bproductId,
			b.price as bprice,
			b.num as bnum,
			b.dimension as bdimension,
			c.subject as csubject,
			c.price_cost as cprice_cost,
			c.price_member as cprice_member,
			c.totalCount as ctotalCount,
			c.Covers as cCovers,
			c.useEdate as cuseEdate,
			d.web_xx_product_id as dweb_xx_product_id,
			d.subject as dsubject,
			e.subject as esubject
		From 
			web_x_order a
		Left Join 
			web_order b ON b.web_x_order_ordernum = a.ordernum
		Left Join 
			web_product c ON c.web_product_id = b.web_product_id
		Left Join 
			web_x_product d ON d.web_x_product_id = c.web_x_product_id
		Left Join 
			web_xx_product e ON e.web_xx_product_id = d.web_xx_product_id	
		Where 
			a.web_member_id = '".$web_member_id."' 
		AND
			a.states = '訂單成立'
		AND
			a.paymentstatus = '付款成功'
		AND
			d.web_xx_product_id IN (1)
		AND
			a.order_type = 0
		order by 
			a.web_x_order_id desc 
	";
	$pdo = $pdoDB->prepare($sql);
	$pdo->execute();
	$orderRow = $pdo->fetchAll(PDO::FETCH_ASSOC);
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $Init_WebTitle; ?> 會員專區-療程體驗券</title>
	<script src="./js/jquery-1.10.2.min.js"></script>
    <link rel="stylesheet" href="css/app.css"/>
	<style>
		.coupon .name {
			margin-bottom: 15px;
		}

		.coupon .row {
			color: #4A4A4A;
			font-size: 14px;
			font-weight: 400;
			border-bottom: 1px solid #ECECEC;
			overflow: hidden;
			display: flex;
			background-image: url("../img/icon_next@3x.png");
			background-position: right;
			background-size: 38px;
			background-repeat: no-repeat;
		}

		.coupon .row .col-1,
		.coupon .row .col-2 {
			line-height: 180%;
			padding: 30px 15px;
			box-sizing: border-box;
		}

		.coupon .row .col-1 {
			width: 25%;
			background-color: #F9F6F5;
			background-size: cover;
		}

		.coupon .row span {
			color: #FF5F15;
		}
		.submit {
			position: fixed;
			bottom: 0;
			left: 0;
			border: 0;
			width: 100%;
			padding: 12px 0;
			color: #FFFFFF;
			text-align: center;
			background: #665750;
			font-size: 18px;
			font-weight: 500;
		}
	</style>
</head>
<body>
<div class="header">
    <h1>會員專區-療程體驗券</h1> <a class="back" href="member.php"></a>
</div>
<div class="content coupon">
<?php
	foreach($orderRow as $key => $orderVal) {
		$pic = ShowPic($orderVal['cCovers'], "./uploadfiles/l/", "./uploadfiles/no_image.jpg");
		
		$sql = "
			SELECT 
				web_x_order.*,
				web_order.subject as prodSubjects,
				web_order.web_order_id as web_order_id,
				web_order.web_x_order_ordernum as web_x_order_ordernum,
				web_order.web_product_id as web_order_product_id,
				web_product.web_product_id as web_product_product_id,
				web_product.web_x_product_id as web_product_x_product_id,
				web_product.totalCount as web_product_totalCount,
				web_product.Covers as web_product_Covers,
				web_product.useEdate as web_product_useEdate,
				web_x_product.web_x_product_id as web_x_product_x_product_id,
				web_x_product.web_xx_product_id as web_x_product_xx_product_id
			from 
				web_x_order
			Left Join
				web_order as web_order
			On
				web_order.web_x_order_ordernum = web_x_order.ordernum
			Left Join
				web_product
			On
				web_product.web_product_id = web_order.web_product_id
			Left Join
				web_x_product
			On
				web_x_product.web_x_product_id = web_product.web_x_product_id
			WHERE 
				web_x_order.from_ordernum = :ordernum
			AND
				web_x_order.states = '訂單成立'	
			AND
				web_x_order.paymentstatus = '付款成功'	
			AND
				web_x_order.order_type = 1
			AND
				web_x_product.web_xx_product_id = 1		
		";
		
		$excute = array(
			':ordernum'		=> $orderVal['ordernum'],
		);
		//$debug = new Helper();
		//echo $test = $debug::debugPDO($sql, $excute);
		$pdo = $pdoDB->prepare($sql);
		$pdo->execute($excute);
		$row4 = $pdo->fetchAll(PDO::FETCH_ASSOC);
?>
    <div class="row" onclick="location.href='coupon-record.php?web_member_id=<?php echo $web_member_id; ?>&web_product_id=<?php echo $orderVal['bproductId']; ?>&web_x_order_id=<?php echo $orderVal['web_x_order_id']; ?>'">
        <div class="col-1" style="background-image: url('<?php echo $pic; ?>'); display:none;"></div>
        <div class="col-2">
            <div class="name"><?php echo $orderVal['bsubject']; ?></div>
            <div>使用狀況：<span><?php echo count($row4); ?> / <?php echo $orderVal['ctotalCount']; ?></span>  (已使用/總次數)</div>
            <?php
				if(strtotime($orderVal['cuseEdate'])>1) {
			?>		
			<div>使用期限：<?php echo date('Y/m/d', strtotime($orderVal['cuseEdate'])); ?></div>
			<?php
				}
			?>	
        </div>
    </div>
<?php
	}
?>	
	<!--
    <div class="row" onclick="location.href='coupon-record.html'">
        <div class="col-1" style="background-image: url('http://www.sarkarinaukrisearch.in/wp-content/uploads/2019/05/173-I-MISS-YOU-PICS-PICTURES-PHOTOS-WALLPAPER-HD-FREE-DOWNLOAD-1-500x415.gif')"></div>
        <div class="col-2">
            <div class="name">頭皮活化疏導療程</div>
            <div>使用狀況：<span>3 / 4</span>  (已使用/總次數)</div>
            <div>使用期限：2019/12/31</div>
        </div>
    </div>
	-->
    <button class="submit" onclick="location.href='coupon-buy.php?web_member_id=<?php echo $web_member_id; ?>'">我要買券</button>
</div>
</body>
</html>
